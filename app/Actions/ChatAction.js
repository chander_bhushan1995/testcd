import { connect } from "react-redux";
import * as Actions from "../Constants/ActionTypes";
import ChatComponent from "../Components/ChatComponent";
import { SendMessageStatus } from "../Constants/SendMessageStatus";
import ChatbotResponseParser from "../ChatbotComponent/ChatbotResponseParser";
import { documentDataFromDocUrl, isDocURL, isSupportedDoc, isJson } from "../Chat/ChatUtils";
import * as ChatAPI from "../Chat/ChatAPIHelper";
import { chatManager } from "../Navigation/index";
import { APIData } from "../../index";

// Defualt data for Components
// dataItem:List of data
// type:it is type of item eg:send message,receiveMessage etc
// isLoading: stand for showing loader
// bottomViewType:to keep what type of bottom view or not keep any.
// ratingData:rating data
const mapStateToProps = state => ({
    animatedIndex: state.ChatReducer.animatedIndex,
    dataItem: state.ChatReducer.dataItem,
    type: state.ChatReducer.type,
    isLoading: state.ChatReducer.isLoading,
    isInternetConnected: state.ChatReducer.isInternetConnected,
    showInternetBar: state.ChatReducer.showInternetBar,
    bottomViewType: state.ChatReducer.bottomViewType,
    ratingData: state.ChatReducer.ratingData,
    errorMesage: state.ChatReducer.errorMesage,
    BottomSheetCloseChatData: state.ChatReducer.BottomSheetCloseChatData,
    bottomViewData: state.ChatReducer.bottomViewData,
    routeList: state.nav.routes,
    autoSuggestionData: state.ChatReducer.autoSuggestionData,
    selectedSuggestionData: state.ChatReducer.selectedSuggestionData,
    inputTextForAutoSuggestions: state.ChatReducer.inputTextForAutoSuggestions,
    suggestionViewMarginFromBottom: state.ChatReducer.suggestionViewMarginFromBottom,
    shouldClearInput: state.ChatReducer.shouldClearInput,
    autoSuggestionEnabled: state.ChatReducer.autoSuggestionEnabled,
    autoSuggestionVisible: state.ChatReducer.autoSuggestionVisible,
    isDateTimePickerVisible: state.ChatReducer.isDateTimePickerVisible,
    dateTimePickerType: state.ChatReducer.dateTimePickerType,
    selectedTagData: state.ChatReducer.selectedTagData,
    otherTagData: state.ChatReducer.otherTagData,
    translatedData: state.ChatReducer.translatedData
});

const mapDispatchToProps = dispatch => ({
    receiveViewAnimationDone: (index) => dispatch({type: Actions.ANIMATION_DONE, index: index}),
    // reset the state of whole chat compoent
    resetState: (isLoading) => dispatch({ type: Actions.RESET, isLoading: isLoading }),
    // to go back
    back: NavigationActions => dispatch(NavigationActions.back()),
    //to remove caht botview from particular index
    removeChatBotView: (index, displayMessage) => dispatch({ type: Actions.REMOVE_CHAT_BOT_VIEW_FROM_LIST, index: index, displayMessage: displayMessage }),
    // to reset data and show start new chat button.
    resetDataAndShowStartChat: () => dispatch({ type: Actions.SHOW_START_CHAT }),
    // to call the api for  translate
    getDataForCloseChat: () => callTranslateAPI(dispatch),
    // to call the api for  capture rating
    callForCaptureRating: (stars) => callCaptureRating(stars, dispatch),
    // to call the api for  capture rating
    callForCaptureFeedback: (comment, questionsArr, completion) => callCaptureFeedback(comment, questionsArr, completion, dispatch),
    // to call the api for start chat api
    startChatApi: (autoSuggestionEnabled) => callStartChatAPI(autoSuggestionEnabled, dispatch),
    // to call the api for  capture rating
    getAutosuggestions: (inputText, inputData) => executeAutosuggestionsApi(inputText, inputData, dispatch),
    resetSuggestionView: () => dispatch({ type: Actions.AUTO_SUGGESTIONS_CLOSE }),
    onSelectionSuggestion: (suggestionObj) => dispatch({ type: Actions.SUGGESTIONS_SELECTION, selectedSuggestionObj: suggestionObj }),
    resetSelectedSuggestion: () => dispatch({ type: Actions.SUGGESTIONS_SELECTION_RESET }),
    showAutoSuggestionView: (visible) => dispatch({ type: Actions.AUTO_SUGGESTION_VISIBLE, visible: visible }),
    updateBottomMarginAutoSuggestionView: (newMargin) => dispatch({ type: Actions.SUGGESTIONS_VIEW_MARGIN_BOTTOM_UPDATE, newMargin: newMargin }),
    // to call the api for ssend message and pass data to componet to sho the send message text
    sendMessage: (message, index) => callSendMessageApi(message, index, dispatch),
    // to call the api for ssend message and pass data to componet to sho the send message text
    updateBottomView: () => dispatch({ type: Actions.UPDATE_BOTTOM_VIEW }),
    addSendMessageView: (sendData, displayData, type) => dispatch({ type: Actions.SEND_MESSAGE, sendData: sendData, displayData: displayData, componentType: type }),
    addSendMessageStaticView: (message) => dispatch({ type: Actions.ADD_SEND_RECEIVE_MESSAGE, message: message, isSent: true, animationCompleted: false }),
    //to remove view from particular index
    removeViewFromList: (index) => dispatch({ type: Actions.REMOVE_VIEW_FROM_LIST, index: index }),
    addDocumentUploadView: (data) => dispatch({ type: Actions.DOCUMENT, data: data }),
    uploadDocument: (index, docUrl, pickedFrom) => uploadDoc(index, docUrl, pickedFrom, dispatch),
    defaultInputType: () => dispatch({ type: Actions.INPUT_ENABLED }),
    updateConnectionStatus: (isConnected, showInternetBar) => dispatch({ type: Actions.UPDATE_CONNECTION_STATUS, isConnected: isConnected, showInternetBar: showInternetBar }),
    showDateTimePicker: (dateTimePickerType) => dispatch({ type: Actions.SHOW_DATETIME_PICKER, dateTimePickerType: dateTimePickerType }),
    hideDateTimePicker: () => dispatch({ type: Actions.HIDE_DATETIME_PICKER }),
    callApiForImageUpload: (uploadData) => callApiForImageUpload(uploadData),
    callDraftDetailApi: (draftRequest) => callDraftDetailApi(draftRequest),
    callDownloadAlreadyUploadedImage: (downloadRequest) => callDownloadAlreadyUploadedImage(downloadRequest),
    callDownloadTemplate: (downloadRequest) => callDownloadTemplate(downloadRequest),
    createSocketConnection: () => createSocketConnection(dispatch).done(),
    callDispatch: (action) => dispatch(action),
    //to remove chat botview from particular index
    updateAutoSuggestions: (isAutoSuggestionEnabled) => dispatch({ type: Actions.UPDATE_AUTO_SUGGESTION_ENABLED, isAutoSuggestionEnabled: isAutoSuggestionEnabled }),
    receivedMessage: (params) => receivedMessageFromSocket(params, dispatch),
    receivedStatus: (params) => receivedStatusFromSocket(params, dispatch),
    resetBottomView: () => resetBottomView(dispatch),
    showStartChatButton: () => showStartChatButton(dispatch)

});
function resetBottomView( dispatch) {
    dispatch({ type: Actions.RESET_BOTTOM_VIEW });
}
function showStartChatButton( dispatch) {
    dispatch({ type: Actions.START_CHAT_BUTTON });
}

async function uploadDoc(index, docUrl, pickedFrom, dispatch) {
    dispatch({ type: Actions.SEND_MESSAGE_STATUS, index: index, status: SendMessageStatus.Sending });
    chatManager.uploadDocument(docUrl, pickedFrom, (uploaded) => {
        dispatch({ type: Actions.SEND_MESSAGE_STATUS, index: index, status: uploaded ? SendMessageStatus.Success : SendMessageStatus.Failure });
    })
}

async function receivedMessageFromSocket(data, dispatch) {
    if (chatManager.showMessage(data.msgId, data.chatId)) {
        chatManager.setFirstSocketMessageShown(true);
        let messageText = data.message
        if (isDocURL(messageText) && isSupportedDoc(messageText)) {
            let docData = documentDataFromDocUrl(messageText);
            dispatch({
                type: Actions.AGENT_SIDE_DOCUMENT,
                docUrl: messageText,
                docType: docData.docType,
                fileType: docData.docFormat,
                fileName: docData.docName
            });
        } else if (isJson(messageText)) {
            var chatbotResponseParser = new ChatbotResponseParser();
            var chatbotData = chatbotResponseParser.chatbotDataFromResponse(messageText);
            dispatch({ type: Actions.TYPING_PAUSED });
            dispatch({ type: Actions.BOT, data: chatbotData });
            checkForCloseChat(chatbotData.ratingView, dispatch);
        } else {
            dispatch({ type: Actions.RECEIVE_MESSAGE, message: messageText.trim() })
        }
        chatManager.checkAndShowLocalNotification(messageText)
        chatManager.checkAgentMismatch(data.from)
    }
}

async function receivedStatusFromSocket(data, dispatch) {
    dispatch({ type: (data?.status?.toLowerCase() === "composing") ? Actions.TYPING : Actions.TYPING_PAUSED });
    chatManager.checkAgentMismatch(data.from)
}

async function checkForCloseChat(ratingData, dispatch) {
    if (ratingData) {
        if (ratingData?.showRating) {
            // ratingData = {
            //     cardType: "rating",
            //     showRating: true,
            //     options: [
            //         {
            //             id: 566769,
            //             value: {
            //                 text: "कृपया अपने चैट अनुभव पर हमें रेट करें और हमें बेहतर बनाने में मदद करें"
            //             },
            //             params: {
            //                 chatid: 566769
            //             }
            //         }
            //     ],
            //     ratingCardText: {
            //         heading1: "आपका चैट अनुभव कैसा रहा?",
            //         heading2: "हमें 1 से 5 के पैमाने पर रेट करें",
            //         submitText: "प्रस्तुत",
            //         notNowText: "ఇప్పుడు కాదు",
            //         loaderText: "रेटिंग जमा किया जा रहा"
            //     },
            //     afterRatingText: {
            //         heading1: "आपकी प्रतिक्रिया के लिए धन्यवाद",
            //         heading2: {
            //             web: "कृपया गूगल रेविएवस पर हमें रेट करने के लिए कुछ समय लें",
            //             android: "कृपया हमें गूगल प्ले स्टोर पर रेट करने के लिए कुछ समय दें",
            //             ios: "कृपया हमें एप्पल प्ले स्टोर पर रेट करने के लिए कुछ समय दें"
            //         },
            //         sureText: "ज़रूर",
            //         toastMsg: "रेटिंग सफलतापूर्वक सबमिट की गई"
            //     }
            // };
            chatManager.setRatingData(ratingData);
            chatManager.resetActiveDeptID()
            chatManager.resetHash()
            dispatch({ type: Actions.RATING, value: ratingData});
        } else {
            chatManager.clearPropsAndCloseConnection();
            dispatch({ type: Actions.START_CHAT_BUTTON});
            //callStartChatAPI(false, true, 0, dispatch).done()
        }
    }
}

function commonErrorHandler(error, dispatch) {
    dispatch({ type: Actions.TECHNICAL_ERROR, message: Actions.TECHNICAL_ERROR_MESSAGE, isLoading: false });
}

// API call for sending rating
async function callCaptureRating(stars, dispatch) {
    dispatch({ type: Actions.TYPING });
    chatManager.submitRating(stars, (_response, error) => {
        if (error) {
            commonErrorHandler(error, dispatch)
        } else {
            if (stars > 3) {
                dispatch({ type: Actions.RATING_SUBMIT_SUCCESS});
                dispatch({ type: Actions.REVIEW, value: chatManager.getRatingData() });
                chatManager.setRatingData(null);
            } else {
                chatManager.feedbackQuestions((response, error) => {
                    if (error) {
                        commonErrorHandler(error, dispatch)
                    } else {
                        dispatch({ type: Actions.NEAGATIVE_RATING_QUESTIONS, response: response });
                    }
                })
            }
        }
    })
}

async function callCaptureFeedback(comment, questionsArr, completion, dispatch) {
    chatManager.submitFeedback(questionsArr, comment, (response, error) => {
        if (error) {
            commonErrorHandler(error, dispatch)
        } else {
            dispatch({ type: Actions.RATING_SUBMIT_SUCCESS});
        }
        completion();
    })
}

//  API call for get history
async function callHistory(dispatch) {
    chatManager.getHistoryData((historyData, error, isChatClosed) => {
        if (isChatClosed) {
            callStartChatAPI(true, dispatch);
        } else if (error) {
            commonErrorHandler(error, dispatch)
        } else {
            chatManager.setFirstSocketMessageShown(true);
            dispatch({ type: Actions.HISTORY, message: historyData });
        }
    })
}

//  API call for get translated text
async function callTranslateAPI(dispatch) {
    dispatch({ type: Actions.GET_DATA_FOR_CLOSE_CHAT});
    // TODO: check translations
    chatManager.getTranslations((response, error) => {
        if (error) {
            commonErrorHandler(error, dispatch)
        } else {
            dispatch({ type: Actions.GET_TRANSLATIONS, translatedData: response?.data});
        }
    })
}

//  API call for start chat
async function callStartChatAPI(autoSuggestionEnabled, dispatch) {
    dispatch({ type: Actions.TYPING });
    chatManager.startChat((response, error) => {
        if (error) {
            commonErrorHandler(error, dispatch)
        } else {
            dispatch({ type: Actions.START_CHAT, message: response, autoSuggestionEnabled: autoSuggestionEnabled, isLoading: false });
        }
    })
}

//  API call for send message
async function callSendMessageApi(message, index, dispatch) {
    dispatch({ type: Actions.SEND_MESSAGE_STATUS, index: index, status: SendMessageStatus.Sending });
    chatManager.sendMessage(message, (response, error) => {
        if (error) {
            dispatch({ type: Actions.SEND_MESSAGE_STATUS, index: index, status: error.message === "10001" ? SendMessageStatus.Failure : SendMessageStatus.NoInternet });
        } else {
            dispatch({ type: Actions.SEND_MESSAGE_STATUS, index: index, status: SendMessageStatus.Success });
        }
    })
}

function executeAutosuggestionsApi(inputText, inputData, dispatch) {
    if (inputText !== "") {
        chatManager.getAutoSuggestions(inputText, inputData, (response, error) => {
            if (response?.suggestions) {
                dispatch({ type: Actions.AUTO_SUGGESTIONS_API_SUCCESS, autoSuggestionData: response?.suggestions, inputText: inputText });
            } else {
                dispatch({ type: Actions.AUTO_SUGGESTIONS_API_FAILURE, autoSuggestionData: error });
            }
        })
    } else {
        dispatch({ type: Actions.AUTO_SUGGESTIONS_CLOSE });
    }
}

//  API call for image upload of a document
async function callApiForImageUpload(uploadData) {
    chatManager.spDocUpload(uploadData, (response, error) => {
        if (response?.status === "success") {
            uploadData.callback(uploadData.index, response.data.documentId, "");
        } else {
            uploadData.callback(uploadData.index, "", response?.message ?? "");
        }
    })
}

//  API call for draft detail of all documents
async function callDraftDetailApi(draftRequest) {
    chatManager.srDraftDetail(draftRequest.draftId, (response, error) => {
        if (!error) {
            draftRequest.callback(response);
        }
    })
}

//  API call for download already uploaded documents
async function callDownloadAlreadyUploadedImage(downloadRequest) {
    chatManager.spDocumentDownload(downloadRequest)
}

//  API call for download template docs
async function callDownloadTemplate(downloadRequest) {
    chatManager.downloadTemplate(downloadRequest)
}

//  Call new start chat api after visitor api success
async function callNewStartChatAPI(dispatch) {
    chatManager.startChatFTR((response, error) => {
        if (error) {
            if (error?.errors && error?.errors.length) {
                dispatch({ type: Actions.TECHNICAL_ERROR, message: Actions.TECHNICAL_ERROR_MESSAGE, isLoading: false });
                dispatch({ type: Actions.SEND_MESSAGE_STATUS, index: index, status: SendMessageStatus.Failure });
            } else {
                chatManager.startChatFTR((response, error) => {
                    if (error) {
                        dispatch({ type: Actions.TECHNICAL_ERROR, message: Actions.TECHNICAL_ERROR_MESSAGE, isLoading: false });
                        dispatch({ type: Actions.SEND_MESSAGE_STATUS, index: index, status: SendMessageStatus.Failure });
                    } else {
                        dispatch({ type: Actions.START_CHAT, message: response, autoSuggestionEnabled: false, isLoading: true });
                    }
                })
            }
        } else {
            dispatch({ type: Actions.START_CHAT, message: response, autoSuggestionEnabled: false, isLoading: true });
        }
    })
}

async function createSocketConnection(dispatch) {
    chatManager.createSocketConnection(isConnected => {
        if (isConnected) {
            // JWT Auth token for autosuggestion api
            chatManager.getAuthToken()
            if (APIData?.membership_id?.length) {
                callNewStartChatAPI(dispatch);
            } else {
                callHistory(dispatch);
            }
        } else {
            dispatch({ type: Actions.TECHNICAL_ERROR, message: Actions.TECHNICAL_ERROR_MESSAGE, isLoading: false });
        }
    })
}

//  it is redux function which is responsible to connect the component to store
export default connect(mapStateToProps, mapDispatchToProps)(ChatComponent);
