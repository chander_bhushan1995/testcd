import React, { useEffect } from 'react';
import {
    Dimensions,
    Image,
    NativeModules,
    SafeAreaView,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import ScreenFooterComponent from '../../Catalyst/components/ScreenFooterComponent';
import { AppForAppFlowStrings } from '../Constant/AppForAllConstants'
import colors from "../../Constants/colors";
import spacing from "../../Constants/Spacing";
import { TextStyle, SafeAreaStyle } from "../../Constants/CommonStyle";
import { isWalkThroughShow } from "../../commonUtil/AppUtils"
import { CustType } from "../../Constants/AsyncStorageConstant"
import codePush from "react-native-code-push";
import { APIData } from '../../../index';
import {PLATFORM_OS} from "../../Constants/AppConstants";


const chatBrigeRef = NativeModules.ChatBridge;

const TopComponent = props => {
    return (
        <View style={styles.topView}>
            <Image style={styles.imageStyle} source={require('../../images/icon_oalogo.webp')} />
            <Text style={styles.topHeading}>{AppForAppFlowStrings.splashTopHeading1}</Text>
            <Text style={styles.topDescirption}>{AppForAppFlowStrings.splashTopDescription}</Text>
        </View>
    )
};

const SplashScreen: () => React$Node = props => {

    const { navigation } = props;

    useEffect(() => {
        let time = (Platform.OS === PLATFORM_OS.android) ? 3000 : 2000;
        let timer1 = setTimeout(moveToMainScreen, time)
        return () => {
            clearTimeout(timer1)
        }
    }, [])

    const moveToMainScreen = () => {
        const { customer_id, custType } = APIData ?? {}

        if (customer_id && customer_id != "0") { //user has verified his mobile number
            chatBrigeRef.gotoTabbar(false, false, false)
        } else if (custType == CustType.notVerified) { // go to dashboard without verification
            chatBrigeRef.gotoTabbar(true, false, false)
        } else {
            isWalkThroughShow({
                onTrue: () => chatBrigeRef.openLoginAsRoot(),
                onFalse: () => navigation?.navigate("WalkThrough")
            });
        }
    };

    return (
        <SafeAreaView style={SafeAreaStyle.safeArea}>
            <TopComponent />
            <ScreenFooterComponent data={AppForAppFlowStrings.screenFooterComponent} style={styles.footerView} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    topView: {
        marginTop: Dimensions.get("screen").height * 0.22,
        flexDirection: 'column',
        alignItems: 'center'
    },
    imageStyle: {
        width: 150,
        height: 50
    },
    topHeading: {
        ...TextStyle.text_16_normal,
        color: colors.color_004890,
        marginTop: spacing.spacing32
    },
    topDescirption: {
        ...TextStyle.text_18_bold,
        color: colors.color_004890,
        marginTop: spacing.spacing20
    },
    footerView: {
        position: 'absolute',
        bottom: Dimensions.get("screen").height * 0.06,
        alignSelf: 'center',
        marginHorizontal: 0
    }
});

export default codePush(SplashScreen);
