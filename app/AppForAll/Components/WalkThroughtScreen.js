import React, {useEffect} from 'react';
import {
    StyleSheet,
    Text,
    View,
    SafeAreaView,
    Dimensions,
    Image, NativeModules, Platform, BackHandler,
} from 'react-native';
import {AppForAppFlowStrings} from '../Constant/AppForAllConstants'
import colors from "../../Constants/colors";
import spacing from "../../Constants/Spacing";
import {TextStyle, SafeAreaStyle, ButtonViewStyle} from '../../Constants/CommonStyle';
import CarouselContainer from "../../Catalyst/components/common/CarouselContainer";
import LinkButton from "../../CustomComponent/LinkButton";
import {PLATFORM_OS} from "../../Constants/AppConstants";
import {WALKTHROUGH_GET_STARTED, WALKTHROUGHT_LOGIN, WALKTHROUGH} from "../../Constants/WebengageEvents";
import {logWebEnageEvent, trackAppScreen} from '../../commonUtil/WebengageTrackingUtils';
import * as Store from '../../commonUtil/Store'

const chatBrigeRef = NativeModules.ChatBridge;

const CardComponent = props => {
    return (
        <View style={[styles.singleCard, props.style]}>
            <Text style={styles.topHeading}>{props.data.title}</Text>
            <Text style={styles.topDescription}>{props.data.paragraph}</Text>
            <View style={styles.imageContainer}>
                <Image style={styles.imageStyle} source={props.data.image}/>
            </View>
        </View>
    )
}

const _renderItem = ({item, index}) => {
    return <CardComponent
        style={{paddingHorizontal: 0, marginTop: 0, marginHorizontal: 0}}
        data={item}
    />
}

const WalkThrough = props => {

    const {navigation} = props;

    useEffect(() => {
        trackAppScreen(WALKTHROUGH)
    }, [])

    const onBack = () => {
        chatBrigeRef.goBack();
    }


    function onPressCustomerLogin() {
        Store.setWalkthroughAction("true")
        if (Platform.OS === PLATFORM_OS.android) {
            chatBrigeRef.walkthroughVisited();
        }
        chatBrigeRef.openLogin(false);
        logWebEnageEvent(WALKTHROUGHT_LOGIN, new Object());
    };

    function onPressLetsStartAction() {
        Store.setWalkthroughAction("true")
        if (Platform.OS === PLATFORM_OS.android) {
            chatBrigeRef.walkthroughVisited();
        }
        navigation?.navigate("WelcomeScreen", {
            headerData: AppForAppFlowStrings.welcomeHeaderComponent,
            cardData: AppForAppFlowStrings.verifyNumberContent
        })
        logWebEnageEvent(WALKTHROUGH_GET_STARTED, new Object());
    };

    function navigateToTnCScreen() {
        navigation?.navigate('TnCWebViewComponent', {
            productTermAndCUrl: AppForAppFlowStrings.TnCComponent.url,
            otherParam: 'T&C',
            enableNativeBack: false,
        });
    }

    return (
        <SafeAreaView style={SafeAreaStyle.safeArea}>
            <View style={{flexGrow: 1}}>
                <CarouselContainer
                    dotStyle={styles.activeDotStyle}
                    inactiveDotStyle={styles.inactiveDotStyle}
                    dotColor={colors.color_212121}
                    inactiveDotColor={colors.color_E0E0E0}
                    _renderItem={_renderItem}
                    dotContainerStyle={styles.dotContainerStyle}
                    dataSource={AppForAppFlowStrings.tutorialComponent}
                    carouselContainerStyle={styles.carouselContainerStyle}
                    isProgressVisible={false}
                    isPaginationVisible={true}
                    autoplay={false}
                    loop={false}
                    autoplayDelay={0}
                    containerWidth={Dimensions.get('screen').width}
                />
            </View>
            <View style={styles.footerView}>
                <LinkButton
                    data={{
                        action: AppForAppFlowStrings.letsStartButtonTitle
                    }}
                    style={ButtonViewStyle.splashBlueButton}
                    onPress={onPressLetsStartAction}
                />
                <LinkButton
                    data={{
                        action: AppForAppFlowStrings.customerLoginButtonTitle
                    }}
                    style={ButtonViewStyle.splashWhiteButton}
                    onPress={onPressCustomerLogin}
                />
            </View>
            <View style={styles.tncViewStyle}>
                <Text>
                    <Text style={[TextStyle.text_12_normal]}>{AppForAppFlowStrings.TnCComponent.firstText} </Text>
                    <Text style={[TextStyle.text_12_bold, styles.tncTextStyle]}
                          onPress={navigateToTnCScreen}>{AppForAppFlowStrings.TnCComponent.privacyTxt} </Text>
                </Text>
            </View>
        </SafeAreaView>
    )
};

const styles = StyleSheet.create({
    carouselContainerStyle: {
        borderRadius: 0,
        elevation: 0,
        shadowColor: colors.black,
        shadowOpacity: 0.0,
        shadowRadius: 0,
        shadowOffset: {
            height: 0,
            width: 0
        },
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        margin: spacing.spacing0,
    },
    singleCard: {
        padding: spacing.spacing20,
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
    },
    topHeading: {
        ...TextStyle.text_20_bold,
        color: colors.color_212121,
        marginHorizontal: spacing.spacing36,
        marginTop: spacing.spacing32,
        textAlign: 'center'
    },
    topDescription: {
        ...TextStyle.text_14_normal,
        color: colors.color_888F97,
        marginHorizontal: spacing.spacing36,
        marginTop: spacing.spacing14,
        textAlign: 'center'
    },
    imageContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin: spacing.spacing10,
        flexDirection: 'column'
    },
    imageStyle: {
        flex: 1,
        margin: 0,
        resizeMode: "contain"
    },
    footerView: {
        bottom: spacing.spacing0,
        width: "100%",
        height: 110,
    },
    dotContainerStyle: {
        height: 0,
        padding: 0,
        margin: 0,
        marginHorizontal: 3
    },
    inactiveDotStyle: {
        height: 6,
        width: 6,
        borderRadius: 3
    },
    activeDotStyle: {
        height: 6,
        width: 18,
        borderRadius: 3
    },
    tncViewStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        margin: spacing.spacing20
    },
    tncTextStyle: {
        marginLeft: spacing.spacing4,
        textDecorationLine: 'underline',
    }
});

export default WalkThrough;
