import React, {useEffect} from 'react';
import {
    StyleSheet,
    Text,
    View,
    SafeAreaView,
    Image,
    TouchableOpacity,
    ImageBackground, NativeModules, Platform, ScrollView,
} from 'react-native';
import {AppForAppFlowStrings} from '../Constant/AppForAllConstants';
import colors from '../../Constants/colors';
import spacing from '../../Constants/Spacing';
import {ButtonViewStyle, TextStyle, SafeAreaStyle, CommonStyle, margin} from '../../Constants/CommonStyle';
import LinkButton from '../../CustomComponent/LinkButton';
import PersonalizedMessageComponent from '../../Catalyst/components/PersonalizedMessageComponent';
import ImagesIndex from '../../images/index.image';
import DialogView from '../../Components/DialogView';
import {LOGIN_SKIP} from '../../Constants/WebengageEvents';
import {trackAppScreen} from '../../commonUtil/WebengageTrackingUtils';
import {useBackButton} from "../../BuyPlan/Helpers/CustomHooks";
import {PLATFORM_OS} from "../../Constants/AppConstants";

const chatBrigeRef = NativeModules.ChatBridge;

const HeaderView = props => {

    const {
        onPressBackAction = () => {
        }, onPressSkipAction = () => {
        }
    } = props;

    return (
        <View style={styles.headerView}>
            <TouchableOpacity onPress={onPressBackAction}>
                <Image source={ImagesIndex.backImage} style={styles.backButton}/>
            </TouchableOpacity>
            <TouchableOpacity style={styles.skipButtonContainer} onPress={onPressSkipAction}>
                <Text style={styles.skipButton}>SKIP</Text>
            </TouchableOpacity>
        </View>
    );
};

const CardView = props => {

    const data = props.data || {};
    const headerText = data.header || ""
    const paragraphText = data.paragraph || ""
    const buttonAction = data.button || {}
    const buttonTitle = buttonAction.text || ""
    const action = buttonAction.action || ""

    function onPressCardAction() {
        chatBrigeRef.openLogin(true);
    }

    return (
        <View style={[CommonStyle.cardContainerWithShadow, props.style]}>
            <View style={cardStyle.imageContainer}>
                <View style={cardStyle.imageStyle}>
                    <Image source={require('../../images/verify_imei_icon.webp')}
                           style={{alignSelf: "center", marginTop: spacing.spacing32, width: 72, height: 72}}/>
                </View>
            </View>
            <View style={cardStyle.textContainer}>
                <Text style={cardStyle.headerTextStyle}>{headerText}</Text>
                <Text style={cardStyle.paragraphTextStyle}>{paragraphText}</Text>
                <View style={{marginTop: spacing.spacing40}}>
                    <LinkButton
                        data={{action: buttonTitle}}
                        style={ButtonViewStyle.defaultBlueButton}
                        onPress={onPressCardAction}
                    />
                </View>
            </View>
        </View>
    );
};


const WelcomeScreen = props => {
    thisProp = props
    const {navigation} = props;
    const headerData = navigation.getParam('headerData');
    const cardData = navigation.getParam('cardData');
    useBackButton(() => {
        return onPressBackAction();
    })

    useEffect(() => {
        if (Platform.OS == PLATFORM_OS.android) {
            chatBrigeRef.enableNativeBack(false)
        }
        return () => {
            if (Platform.OS == PLATFORM_OS.android) {
                chatBrigeRef.enableNativeBack(true)
            }
        }
    }, [])

    function onPressBackAction() {
        navigation.pop();
        return true;
    }

    function onPressSkipAction() {
        showAlert(AppForAppFlowStrings.skipAlertTitle, AppForAppFlowStrings.skipLoginMessage, AppForAppFlowStrings.skipAlertButtonText)
    }

    function showAlert(title, alertMessage, buttonText) {
        if (this.DialogViewRef) {
            this.DialogViewRef.showDailog({
                title: title,
                message: alertMessage,
                primaryButton: {
                    text: buttonText, onClick: () => {
                        trackAppScreen(LOGIN_SKIP)
                        chatBrigeRef.gotoTabbar(true, false, false)
                    },
                },
                cancelable: true,
                onClose: () => {
                },
            });
        }
    }

    return (
        <SafeAreaView style={styles.safeArea}>
            <HeaderView {...props} onPressBackAction={onPressBackAction} onPressSkipAction={onPressSkipAction}/>
            <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom: spacing.spacing16}} bounces={false}>
                <PersonalizedMessageComponent data={headerData} headerStyle={styles.primaryText}
                                              paragraphStyle={styles.secondaryText}/>
                <CardView data={cardData} style={styles.cardContainer}/>
            </ScrollView>
            <DialogView ref={ref => (this.DialogViewRef = ref)}/>
        </SafeAreaView>
    );
};


const cardStyle = StyleSheet.create({
    imageContainer: {
        justifyContent: 'flex-start',
        height: 150
    },
    imageStyle: {
        width: '100%',
        height: '100%',
    },
    textContainer: {
        marginTop: spacing.spacing20,
    },
    headerTextStyle: {
        ...TextStyle.text_20_bold,
        textAlign: 'center',
        marginHorizontal: spacing.spacing16
    },
    paragraphTextStyle: {
        ...TextStyle.text_16_normal,
        textAlign: 'center',
        marginHorizontal: spacing.spacing16,
        marginTop: spacing.spacing16
    },
    footerButtonView: {
        width: '100%'
    }
});


const styles = StyleSheet.create({
    safeArea: {
        ...SafeAreaStyle.safeArea,
        backgroundColor: colors.blue,
        justifyContent: 'flex-start'
    },
    headerView: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    backButton: {
        height: 25,
        width: 30,
        tintColor: colors.white,
        marginLeft: 9,
        marginRight: 12,
        marginVertical: 12,
        resizeMode: 'contain'
    },
    skipButtonContainer: {
        marginTop: 16,
        marginRight: 16
    },
    skipButton: {
        ...TextStyle.text_12_semibold,
        marginLeft: spacing.spacing6,
        textAlign: 'center',
        color: colors.white
    },
    primaryText: {
        ...TextStyle.text_20_bold,
        color: colors.white,
        marginTop: spacing.spacing0
    },
    secondaryText: {
        color: colors.white,
        marginTop: spacing.spacing12
    },
    cardContainer: {
        marginTop: "15%",
        marginHorizontal: spacing.spacing12,
        bottom: spacing.spacing12,
        borderRadius: 4,
    },
});

export default WelcomeScreen;
