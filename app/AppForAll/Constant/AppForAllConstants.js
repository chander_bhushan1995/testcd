import {Text} from 'react-native';
import React from 'react';
import {buybackPartnerType} from '../../Constants/BuybackConstants';

export const SelectedActionType = {
    CHAT: "CHAT",
    TABSWITHCHED: "TABSWITHCHED",
}

export const RaisedServiceRequestsStatus = {
    IP: 'IP',           // in progress
    P: 'P',             // pending
    CO: 'CO',            // completed
    OH: 'OH',            // On hold
};

export const SODServiceRequestTypes = {
    breakdown: 'HA_BD',
    pms: 'PMS',
};

export const PaymentStatus = {
    success: 'S',
    inProgress: 'I',
};

export const PossibleOnHoldStatuses = {
    SN: 'SN', //Spare Needed
    TCCA: 'TCCA', // Technician Cancelled-Customer Not Available
    TCO: 'TCO', // Technician Cancelled-Other
    AP: 'AP', // Approved
    AA: 'AA', //Awaiting approval
    SPA: 'SPA', // SparePart Available
    TR: 'TR', // Transport
};

export const ReportTypes = {
    jobsheet: "JOBSHEET"
}

export const AppForAppFlowStrings = {

    downloadInvoice: 'Downloading Invoice',
    splashTopHeading1: 'With you at every step for your',
    splashTopDescription: 'Gadgets | Finances | Appliances',
    letsStartButtonTitle: 'Let’s get started',
    customerLoginButtonTitle: 'Already a customer? Log in',
    skipAlertTitle: 'Are you sure you want to skip?',
    skipAlertMessage: 'You can unlock your rewards later from Account Tab at the bottom of home screen',
    skipLoginMessage: 'You can login/create account by verifying your mobile number from Account section.',
    skipAlertButtonText: 'Skip to home',
    checkInternetConnection: 'Please check your internet connection',

    //SOD Constants
    onHold: 'On Hold',
    scheduledFor: 'Scheduled for',
    time: 'Time',
    viewDetails: 'View Details',
    completedOn: 'Completed on',
    rateOurService: 'Rate our service',
    requestPaymentDone: ' request payment completed',
    rescheduleNow: 'Reschedule Now',
    requestPlaced: ' request placed',
    slotsExhausted: 'Sorry, your slot has been exhausted. You can easily reschedule.',
    paymentUnderProcess: 'Payment is under process. We will let you know once the payment is completed.',

    hint_search_box: 'Search your bank',
    textCompleteProfile: 'Save changes',
    textUnlockRewards: 'Unlock Rewards',
    textEdit: 'Save changes',
    viewAll: 'View all',
    okGotIt: 'Ok Got It',
    ok: 'ok',
    pleaseWait: 'Please wait...',
    cardOffers: 'CARD OFFERS',

    cardTitle: 'Card Title',
    cardType: 'Card Type',
    position: 'Position',
    location: 'Location',
    id: 'ID',
    commId: "CommId",
    componentId: "ComponentId",
    product: 'Product',
    service: 'Service',
    commCat: 'CommCat',
    commSubcat :'CommSubcat',
    group: 'Group',
    homeBigCard: 'Home Big Card',

    accountTabScreen: 'Account Tab screen',
    homeScreen: 'Home Screen',
    myAccountScreen: 'My Account',
    bookMarkOffer: 'Bookmark Offer',
    generalCompleteProfile: 'GeneralCompleteProfile',
    completeProfile: 'CompleteProfile',
    GOTO_ACTIVATE_VOUCHER_FROM: 'Membership',
    explore: 'Explore',
    payBill: 'Pay Bill',
    buyback: 'Buyback',
    home: 'Home',
    activateVoucher: 'Activate Voucher',
    chat: 'Chat',
    serviceRequests: 'My Service requests',
    gadgetAppliancesAndBanks: 'My Gadgets, Applicances and Bank Accounts',

    yourTextHere: 'YOUR TEXT WILL BE HERE...',
    renewal: 'renewal',
    completePurchase: 'complete_purchase',

    somethingWentWrong: 'Something went wrong!',
    getUpTo: (price) => {
        return `GET UPTO ₹ ${price}.`;
    },
    bestsellingPriceFor: (model) => {
        return `Unlock the best offer price for your ${model}.`;
    },
    resaleOffer: (model) => {
        return `An amazing resale offer for your ${model} is waiting.`;
    },
    finishHealthTest: (price) => {
        return `Finish your mobile health test now. Get upto ₹ ${price}.`;
    },
    lastStepPending: (model) => {
        return `Only last step is pending. Find ${model} selling price.`;
    },
    fewQuestions: (price) => {
        return `Answer a few questions to get exact price. Get upto ₹ ${price}.`;
    },
    finalOffer: (price) => {
        return `Final offer of ₹ ${price} is running out.`;
    },
    resaleRequest: (brandModel) => {
        return `Resale request for ${brandModel} has been placed.`;
    },
    finalSellingPrice: (price) => {
        return `Final selling price is ₹ ${price}.`;
    },
    activeRequests: (number) => {
        return `You have ${number} active requests.`;
    },
    resalePriceIs: (model, price) => {
        return `For ${model} resale price is ₹ ${price}.`;
    },
    moreText: (number) => `+${number} more`,
    exchangeRequest: (brandModel) => {
        return `Exchange request for ${brandModel} has been placed.`;
    },
    finalExchangePrice: (price, flow) => {
        let offerText = flow === buybackPartnerType.PARTNER_XIAOMI ? 'Assured Buyback Value' : 'Exchange Value';
        return `Visit to partner store is confirmed. ${offerText} is ₹ ${price}.`;
    },
    seeYourDevice: () => {
        return `See your device resale offer`;
    },

    verifyEmailText: (email) => `Your email ${email} is not verified. Verify it now to receive important communication related to your membership in future.`,

    saleOldMobile: 'We do not support this model',
    brandNotSupported: 'Please download the app on the phone you wish to sell',
    schedulePickup: (days)=>{return `Schedule pickup within ${days} days or you may lose this offer.`},

    myAppliances: 'My Appliances',
    myGadgets: 'My Gadgets',
    banksTransactWith: 'Banks I transact with',

    assetHAQC: 'ASSET_HA',
    assetPEQC: 'ASSET_PE',
    assetFQC: 'ASSET_F',

    // My account screen used Text
    rewards: 'Rewards',
    bookmarkOffers: 'Bookmarked offers',
    myGadgetsAppliancesAndBankCards: 'My gadgets, appliances and bank cards',
    myServiceRequest: 'Quick Repair Bookings',
    rateOnGoogle: 'Rate on Google Play',
    rateOnAppStore: 'Rate on App Store',
    shareText: 'Share our App with your friends & family. Spread the love.',
    relationshipNo: 'Relationship No. ',
    otherOptions: 'OTHER OPTIONS',
    myProfileText: 'MY PROFILE',
    personalizedExperience: 'For a personalized experience, complete your profile now.',
    unlockReward: 'To unlock rewards, complete your profile now!',
    completeProfileText: 'Complete Profile',
    activateMembership: 'Activate your membership in 3 easy steps.',
    haveVoucher: 'Have a voucher code?',
    shareApp: 'Share App',
    resendLink: 'Resend Link',
    chatWithUs: 'Chat with us',
    privacyPolicy: 'Privacy Policy',
    settings: 'Settings',
    notifications: 'Notifications',
    rewardsWaitingForYou: 'Verify your mobile number for a personalized app experience.',
    completeProfileAndVerifyMobile: 'This’ll help us to save your app activity and show relevant information when you visit us next time.',
    verifyMobile: 'Verify mobile number',
    newBadge: 'NEW',
    logout: 'Logout',
    stepsLeft: (value) => {
        return value == 1 ? `Only ${value} step left` : `Only ${value} steps left`;
    },

    // My Profile screen used Text
    myProfileTitle: 'My gadgets, appliances and bank cards',
    settingsTitle: 'Settings',
    personalDetails: 'Personal details',
    otherDetails: 'Other details',
    email: 'Email',
    mobileNumber: 'Mobile number',
    footerText: 'If your details do not match with our records, your claim will be rejected. To update your details.',
    chatSupport: ' Chat with customer care.',
    changeSaved: 'Your changes have been saved.',

    myRewardsTitle: 'My Rewards',

    partnerBUCode: '2185',
    partnerCode: '139',
    initiatingSystem: '8',
    idFenceDashboardNotSetup: 'Please wait while we setup your ID Fence dashboard. This may take few minutes.',
    idFenceDashboardCustomerDoesntExist: 'We couldn\'t find your IDFence Membership.',
    idFenceDashboardSetupInProgress: 'We are setting up your Dashboard. You will get notified via SMS/Email when it is ready',
    close: 'CLOSE',

    noRewardsToUnlock: 'Looks like there are no rewards to unlock. Come back later.',
    unlockedRewards: 'UNLOCKED REWARDS',
    usedExpiredRewards: 'USED & EXPIRED REWARDS',

    buybackDeeplinkUrl: (quoteId) => {
        return `https://www.oneassist.in/buyback/schedulepickup?quoteId=${quoteId ?? 0}`;
    },

    screenFooterComponent: {
        'images': [
            'sslImage',
            'pciImage',
            'nortonImage',
        ],
        'header': {
            'value': '100% Safe and Secure | PCI-DSS Compliant',
        },
        'isLocalImages': true,
    },
    tutorialComponent: [
        {
            'title': 'With you for your gadgets',
            'paragraph': 'To protect your gadgets against damages and offer best selling price for old mobile.',
            'image': require('../../images/ic_gadgets.webp'),
        },
        {
            'title': 'With you for your finances',
            'paragraph': 'To help you save money with card offers, pay credit card bills on time and monitor them against fraud and misuse.',
            'image': require('../../images/ic_finance.webp'),
        },
        {
            'title': 'With you for your appliances',
            'paragraph': 'To protect TV, AC, Fridge, Washing Machine and more with quick doorstep service.',
            'image': require('../../images/ic_appliances.webp'),
        },
        {
            'title': 'With you for quick repairs',
            'paragraph': 'To service, repair or fix your AC, washing machine, water purifier as & when required on-the-spot.',
            'image': require('../../images/ic_quickrepair.webp'),
        },
    ],
    welcomeHeaderComponent: {
        'header': {
            'value': 'Hi! Welcome to OneAssist.',
            'isHTML': false,
        },
        'paragraph': {
            'value': 'Explore our app and get the most out of your Gadgets, Finances & Appliances.',
            'isHTML': false,
        },
    },
    TnCComponent: {
        'firstText': 'By continuing, you agree to OneAssist',
        'privacyTxt': 'Privacy Policy',
        'url': 'https://oneassist.in/privacypolicy/app/',
    },


    verifyNumberContent: {
        'header': 'Verify mobile number for personalized recommendations',
        'paragraph': 'This’ll help you us show unique recommendations each time you open app',
        'button': {
            'text': 'Verify Mobile Number',
            'action': 'login',
        },
    },

    myAccount: {
        'myDetails': 'My _ details',
        'gedgetsAppliancesAndBankCard': 'Gadgets, Appliances and Bank Cards.',
        'stepOnly': ' Steps only.',
        'logoutTitle': 'Logout',
        'logoutmessage': 'Do you wish to Logout?',
        'logoutCTAText': 'Yes',
        'networkReqFailed': 'Network request failed',
    },

    bookmarkLoginAlert: {
        title: 'To see your Bookmarks, please verify mobile number.',
    },
    buybackLoginAlert: {
        title: 'To see your Buyback status, please verify mobile number.',
    },
    newByBankATMAlert: {
        title: 'To see your near by ATMs or Banks, please verify mobile number.',
    },
    CreditCardBillLoginAlert: {
        title: 'To pay credit card bill, please verify mobile number',
    },
    activateVoucherLoginAlert: {
        title: 'Have a voucher code? To activate, please verify mobile number.',
    },
    cardOffersLoginAlert: {
        title: 'To see offers on you cards, please verify moile number',
    },

    chatLoginAlert: {
        title: 'To chat with customer care, please verify mobile number.',
    },

    serviceRequestAlert: {
        title: 'To see your service requests, please verify mobile number.',
    },

    gadgetAppliancesAndBanksAlert: {
        title: 'To see your gadgets, appliances and bank accounts, please verify mobile number.',
    },

    toolTipAlert: {
        rewardAlert: 'We have some amazing rewards for you. To unlock, Go to account & complete your profile.',
        // nonVerifiedAlert: 'To unlock rewards, Go to accounts tab > Verify mobile number > Complete your profile.',
        nonVerifiedAlert: 'Verify your mobile number for a personalized app experience. This’ll help us to save your app activity and show relevant information.',
        pendingRewards: 'All rewards will be available here. You may access rewards by going to Account > My Rewards.',
        personalizedExperience: 'For a personalized experience. Go to accounts tab and complete your profile.',
    },

    onBoardingQuestionsCode: {
        haAssets: 'ASSET_HA',
        peAssets: 'ASSET_PE',
        financeAssets: 'ASSET_F',
    },

    onboardingQuestions: {
        nameQuestion: 'What is your name?',
        fullNameOnID: 'Full name (As per govt. issued ID Card)',
        fullName: 'Full Name',
        andEmail: 'And, your Email ID?',
        email: 'Email',
        enterEmail: 'Enter email',
        emailAddress: 'email-address',
        enterValidEmail: 'Please enter a valid Email Id',
        otherAppliance: 'OTHER APPLIANCES',
        others: 'OTHERS',

        wantSaveChanges: 'Do you want to save the changes?',
        pressSaveToUpdate: 'Press save changes to update your records.',
        saveChanges: 'Save changes',

        wantToSkip: 'Are you sure you want to skip?',
        unlockRewardsLater: 'You can unlock your rewards later from Account Tab at the bottom of home screen.',
        skipToHome: 'Skip to home',
    },
};
