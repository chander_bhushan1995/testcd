import images from "../../../images/index.image"
import colors from "../../../Constants/colors"
import { getRandomMHCCategory } from './MHCData'
import { ButtonsAction } from '../../HomeScreen/Helpers/ButtonActions'
import { Tags } from '../../HomeScreen/Components/ComponentsFactory'

// const MHCData = getRandomMHCCategory()
//
// export const prepareWhiteBgImgCompData = () => {
//     var objectArr = []
//
//     MHCData.viewModel.forEach(viewModel => {
//         const compDataObject = {
//             "type": "WhiteBackgroundImageComponent",
//             "image": viewModel.icon,
//             "title": viewModel.name,
//             "mhcCategory": MHCData.name,
//             "buttonAction": ButtonsAction.START_SECTIONAL_MHC
//         }
//         objectArr.push(compDataObject)
//     })
//
//     return objectArr
// }

export const removeObjHomeScreenDataArray = (arr, attr, value) => {
    var i = arr.length;
    while (i--) {
        if (arr[i]
            && arr[i].hasOwnProperty(attr)
            && (arguments.length > 2 && arr[i][attr] === value)) {

            arr.splice(i, 1);

        }
    }
    return arr;
}


const HomeScreenData = [
    {
        "type": "SectionalViewComponent",
        "title": "FOR YOU",
        "bgColor": "#FFFFFF",
        "compTag": 100,
        "isShadowEnabled": true,
        "components": [
            {
                "type": "CarouselVariation2",
                "compTag": 101,
                "components": [
                    {
                        "type": "SpotLightComponent",
                        "compTag": 102,
                        "isShowSkeleton": true
                    }
                ]
            }
        ]
    },
    {
        "type": "ActivateVoucher",
        "title": "Have a membership activation code?",
        "compTag": 120,
        "button": {
            "text": "Activate Now",
            "action": "ACTIVATE_VOUCHER"
        },
        "dismissImage": {
            "uri": "https://ws.oneassist.in/static/oaapp/home/ic_blackClose.png"
        }
    },
    {
        "type": "ScheduledUIComponent",
        "compTag": 126
    },
    {
        "type": "SectionalViewComponent",
        "title": "QUICK REPAIRS",
        "isTagEnabled": true,
        "tagText": "NEW",
        "bgColor": null,
        "compTag": 121,
        "components": [
            {
                "type": "CardContainerComponent",
                "compTag": 122,
                "components": [
                    {
                        "type": "LeftAlignedComponent",
                        "title": "AC SERVICE",
                        "description": "Just book and relax. AC service @ &#8377;599 <n><strike>&#8377;699</strike></n>",
                        "imageBackground": "#6475DF",
                        "image": {
                            "uri": "https://ws.oneassist.in/static/oaapp/home/ac_service.png"
                        },
                        "data": "https://oneassist.in/buy?category=QR&product=AC",
                        "buttonAction": "START_SOD_FLOW",
                        "compTag": 123
                    },
                    {
                        "type": "LeftAlignedComponent",
                        "title": "WASHING MACHINE REPAIR",
                        "description": "Upto 15% off on genuine spares (Limited Period Offer) ",
                        "imageBackground": "#6475DF",
                        "image": {
                            "uri": "https://ws.oneassist.in/static/oaapp/home/wm_repair.png"
                        },
                        "data": "https://oneassist.in/buy?category=QR&product=WM",
                        "buttonAction": "START_SOD_FLOW",
                        "compTag": 124
                    }
                ]
            }
        ]
    },
    {
        "type": "TitleDescriptionListContainer",
        "title": "Book a Service or Repair",
        "description": "40,000+ appliances repaired successfully in 2019",
        "bgColor": "#6475DF",
        "bgImage": {
            "uri": "https://ws.oneassist.in/static/oaapp/home/repairing.png"
        },
        "compTag": 125,
        "components": [
            {
                "type": "WhiteBackgroundImageComponent",
                "image": {
                    "uri": "https://ws.oneassist.in/static/oaapp/home/air_conditioner.png"
                },
                "title": "AC",
                "data": "https://oneassist.in/buy?category=QR&product=AC",
                "buttonAction": "START_SOD_FLOW"
            },
            {
                "type": "WhiteBackgroundImageComponent",
                "image": {
                    "uri": "https://ws.oneassist.in/static/oaapp/home/washing_machine.png"
                },
                "title": "Washing Machine",
                "data": "https://oneassist.in/buy?category=QR&product=WM",
                "buttonAction": "START_SOD_FLOW"
            },
            {
                "type": "WhiteBackgroundImageComponent",
                "image": {
                    "uri": "https://ws.oneassist.in/static/oaapp/home/Refrigerator.png"
                },
                "title": "Refrigerator",
                "data": "https://oneassist.in/buy?category=QR&product=REF",
                "buttonAction": "START_SOD_FLOW"
            },
            {
                "type": "WhiteBackgroundImageComponent",
                "image": {
                    "uri": "https://ws.oneassist.in/static/oaapp/home/television.png"
                },
                "title": "Television",
                "data": "https://oneassist.in/buy?category=QR&product=TV",
                "buttonAction": "START_SOD_FLOW"
            }
        ]
    },
    {
        "type": "SectionalViewComponent",
        "title": "FOR YOUR MOBILE",
        "bgColor": null,
        "compTag": 103,
        "components": [
            {
                "type": "CardContainerComponent",
                "compTag": 104,
                "components": [
                    {
                        "type": "LeftAlignedComponent",
                        "compTag": 105,
                        "isShowSkeleton": true
                    },
                    {
                        "type": "LeftAlignedComponent",
                        "title": "MOBILE PERFORMANCE",
                        "description": "Check Samsung mobile health within minutes.",
                        "imageBackground": "#0282F0",
                        "image": {
                            "uri": "https://ws.oneassist.in/static/oaapp/home/ic_mhc.png"
                        },
                        "buttonAction": "START_COMPLETE_MHC",
                        "compTag": 106
                    }
                ]
            }
        ]
    },
    {
        "type": "TitleDescriptionListContainer",
        "compTag": 117
    },
    {
        "type": "SectionalViewComponent",
        "title": "FOR YOUR FINANCES",
        "compTag": 107,
        "components": [
            {
                "type": "CardContainerComponent",
                "compTag": 108,
                "components": [
                    {
                        "type": "LeftAlignedComponent",
                        "title": "CREDIT CARD BILL",
                        "description": "Pay Credit Card Bills easily and get rewards.",
                        "imageBackground": "#00AFAF",
                        "image": {
                            "uri": "https://ws.oneassist.in/static/oaapp/home/ic_managePayCard.png"
                        },
                        "tag": "NEW",
                        "buttonAction": "PAY_BILL",
                        "compTag": 109
                    },
                    {
                        "type": "LeftAlignedComponent",
                        "title": "CARD OFFERS",
                        "description": "Find offers on your bank cards and more.",
                        "imageBackground": "#00AFAF",
                        "image": {
                            "uri": "https://ws.oneassist.in/static/oaapp/home/ic_offers.png"
                        },
                        "buttonAction": "CARD_OFFERS",
                        "compTag": 110
                    },
                    {
                        "type": "LeftAlignedComponent",
                        "title": "NEARBY ATMS",
                        "description": "Low on cash? Find nearby ATMs on the go.",
                        "imageBackground": "#00AFAF",
                        "image": {
                            "uri": "https://ws.oneassist.in/static/oaapp/home/ic_atmFind.png"
                        },
                        "buttonAction": "SHOW_ATM",
                        "compTag": 111
                    },
                    {
                        "type": "LeftAlignedComponent",
                        "title": "NEARBY BANKS",
                        "description": "Find bank branch, see contact info and address.",
                        "imageBackground": "#00AFAF",
                        "image": {
                            "uri": "https://ws.oneassist.in/static/oaapp/home/ic_bankFind.png"
                        },
                        "buttonAction": "SHOW_BANK",
                        "compTag": 112
                    }
                ]
            }
        ]
    },
    {
        "type": "TitleDescriptionListContainer",
        "title": "Offers for you",
        "description": "Save money on daily expenses with card offers.",
        "bgColor": "#00AFAF",
        "button": {
            "text": "See all",
            "action": "SEE_ALL_OFFERS"
        },
        "compTag": 118,
        "components": [
            {
                "type": "OfferCardSkeleton"
            }
        ]
    },
    {
        "type": "SectionalViewComponent",
        "title": "FOR YOUR APPLIANCES",
        "compTag": 113,
        "components": [
            {
                "type": "CardContainerComponent",
                "compTag": 114,
                "components": [
                    {
                        "type": "LeftAlignedComponent",
                        "title": "EXTENDED WARRANTY",
                        "description": "Cover your appliances against malfunctions and defects.",
                        "imageBackground": "#00AFAF",
                        "image": {
                            "uri": "https://ws.oneassist.in/static/oaapp/home/ic_HomeEW.png"
                        },
                        "buttonAction": "EXTENDED_WARRANTY",
                        "compTag": 115
                    },
                    {
                        "type": "LeftAlignedComponent",
                        "title": "HOMEASSIST",
                        "description": "Protect ALL your appliances with just ONE plan.",
                        "imageBackground": "#00AFAF",
                        "image": {
                            "uri": "https://ws.oneassist.in/static/oaapp/home/ic_homeAssist.png"
                        },
                        "buttonAction": "HOME_ASSIST",
                        "compTag": 116
                    }
                ]
            }
        ]
    }
]

export default HomeScreenData

