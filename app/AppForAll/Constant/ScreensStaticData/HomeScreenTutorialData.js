import images from '../../../images/index.image';

export const HomeScreenTutorialData = [
    {
        "image": images.homeSmall,
        "header": "Home",
        "description": "This tab will have features for your gadgets, finances and appliances.",
        "tag": ""
    },
    {
        "image": images.buySmall,
        "header": "Buy plan or book a service",
        "description": "Need a quick repair? Need a rotection plan? Find repair services and plans for Electronics, Bank Cards, Digital Identity and Home Appliances. #StayProtected",
        "tag": ""
    },
    {
        "image": images.memSmall,
        "header": "Memberships",
        "description": "View all your memberships, repair requests, service bookings and claim requests under My Purchases.",
        "tag": ""
    },
    {
        "image": images.accountSmall,
        "header": "Account",
        "description": "You can find your account details here, activate voucher code and also chat with our customer care if you have any queries.",
        "tag": ""
    }
]

export const HomeScreenTutorialDataNew = [
    {
        "image": images.homeSmall,
        "header": "Home - Today & Explore",
        "description": "From daily recommendations to app features, All under ‘Home’ tab. See today’s unique recommendations or explore app features.",
        "tag": "NEW"
    },
    {
        "image": images.buySmall,
        "header": "Buy plan or book a service",
        "description": "Need a quick repair? Need a rotection plan? Find repair services and plans for Electronics, Bank Cards, Digital Identity and Home Appliances. \n#StayProtected",
        "tag": ""
    },
    {
        "image": images.memSmall,
        "header": "Memberships",
        "description": "View all your memberships, repair requests, service bookings and claim requests under My Purchases.",
        "tag": ""
    },
    {
        "image": images.accountSmall,
        "header": "Account",
        "description": "You can find your account details here, activate voucher code and also chat with our customer care if you have any queries.",
        "tag": ""
    }
]