
import { ButtonsAction } from "../../MyRewards/Helpers/ButtonActions";

export const RewardStats = { //states of rewards
    LOCKED: "LOCKED",
    EARNED: "EARNED",
    UNLOCKED: "UNLOCKED",
    USED: "USED",
    EXPIRED: "EXPIRED",
    CLAIMED: "CLAIMED"
}
