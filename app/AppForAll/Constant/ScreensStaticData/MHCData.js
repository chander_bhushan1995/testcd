import images from '../../../images/index.image'


export const MHCTestCategories = [
    {
        "name": "screen",
        "title": "Test your mobile touch screen",
        "description": "Did you know? Screen lag is the most common type of issue. Test touch screen now",
        "viewModel": [
            {
                "name": "Touch Test",
                "icon": images.ic_touch
            },
        ],
        "subcategories": [
            {
                "testname": "multitouch",
                "name": "Multi Touch",
            },
        ]
    },
    {
        "name": "camera",
        "title": "Test your mobile camera",
        "description": "Tired of blurry pictures and selfies? Test your front & back camera, flash and auto-focus.",
        "viewModel": [
            {
                "name": "Front/Back Camera Test",
                "icon": images.ic_camera,
            }
        ],
        "subcategories": [
            {
                "testname": "frontCamera",
                "name": "Front Camera",
            },
            {
                "testname": "backCamera",
                "name": "Back Camera",
            },
            {
                "testname": "flash",
                "name": "Flash",
            },
            {
                "testname": "autoFocus",
                "name": "Auto Focus",
            },
        ]
    },
    {
        "name": "sound",
        "title": "Test your mobile sound",
        "description": "Most common causes in sound component is due to water spills and dust. Test them now.",
        "viewModel": [
            {
                "name": "Sound Test",
                "icon": images.ic_speaker,
            }
        ],
        "subcategories": [
            {
                "testname": "speaker",
                "name": "Speaker",
            },
            {
                "testname": "mic",
                "name": "Microphone",
            },
            {
                "testname": "vibration",
                "name": "Vibration",
            },
        ]
    },
    {
        "name": "connectivity",
        "title": "Test your mobile connectivity",
        "description": "Did you know? 1 in 5 mobile users face connectivity issues. Test connectivity components now.",
        "viewModel": [
            {
                "name": "GPS Test",
                "icon": images.ic_gps
            },
            {
                "name": "NFC Test",
                "icon": images.ic_nfc
            }
        ],
        "subcategories": [
            {
                "testname": "nfc",
                "name": "Near-field communication",
            },
            {
                "testname": "gps",
                "name": "GPS",
            },
        ]
    },
    {
        "name": "buttons",
        "title": "Test your mobile buttons",
        "description": "Most volume buttons tend to get loose after some time. Test and be sure they’re in working condition.",
        "viewModel": [
            {
                "name": "Buttons Test",
                "icon": images.ic_buttons,
            }
        ],
        "subcategories": [
            {
                "testname": "volumeUpButton",
                "name": "Volume Up Button",
            },
            {
                "testname": "volumeDownButton",
                "name": "Volume Down Button",
            },
        ]
    },
    {
        "name": "sensors",
        "title": "Test your mobile sensors",
        "description": "Sensors are mostly used in gaming apps, fitness apps. Test and be sure they’re in working condition.",
        "viewModel": [
            {
                "name": "Gyroscope Test",
                "icon": images.ic_sensors
            },
            {
                "name": "Accelerometer & Magnetometer",
                "icon": images.ic_accel_magne,
            },
            {
                "name": "Proximity Test",
                "icon": images.ic_proximity,
            },
            {
                "name": "Biometric Test",
                "icon": images.ic_finger
            },
        ],
        "subcategories": [
            {
                "testname": "gyroscope",
                "name": "Gyroscope",
            },
            {
                "testname": "accelerometer",
                "name": "Accelerometer",
            },
            {
                "testname": "magnetometer",
                "name": "Magnetometer",
            },
            {
                "testname": "proximity",
                "name": "Proximity Sensor",
            },
            {
                "testname": "fingerprint",
                "name": "Fingerprint Sensor",
            },
        ]
    },
    {
        "name": "hardware",
        "title": "Test your mobile hardware",
        "description": "Earphone/charging cables tend to fit loosely after some time which prevent them from working. Test now.",
        "viewModel": [
            {
                "name": "Earphones Jack Test",
                "icon": images.ic_earphones,
            },
            {
                "name": "Charging Port Test",
                "icon": images.ic_usb,
            },
        ],
        "subcategories": [
            {
                "testname": "headphoneJack",
                "name": "Headphone Jack",
            },
            {
                "testname": "chargingPort",
                "name": "Charging Port",
            },
        ]
    },
    {
        "name": "other",
        "title": "Do a quick mobile test",
        "description": "Quick test include WiFi, Bluetooth, Cellular, NFC, RAM, Battery & Storage. It’ll only take seconds.",
        "viewModel": [
            {
                "name": "Wi-Fi",
                "icon": images.ic_wiFi
            },
            {
                "name": "Bluetooth",
                "icon": images.ic_bluetooth
            },
            {
                "name": "RAM",
                "icon": images.ic_ram
            },
            {
                "name": "Storage",
                "icon": images.ic_sdCard
            },
            {
                "name": "Antenna Test",
                "icon": images.ic_network
            },
            {
                "name": "Battery",
                "icon": images.ic_battery
            },
        ],
        "subcategories": [
            {
                "testname": "wifi",
                "name": "Wi-Fi",
            },
            {
                "testname": "bluetooth",
                "name": "Bluetooth",
            },
            {
                "testname": "ram",
                "name": "RAM",
            },
            {
                "testname": "storage",
                "name": "Storage",
            },
            {
                "testname": "antenna",
                "name": "GSM Test",
            },
            {
                "testname": "battery",
                "name": "Battery",
            },
        ]
    },
]


export const getRandomMHCCategory = () => {
    return MHCTestCategories[Math.floor(Math.random() * MHCTestCategories.length)]
}
