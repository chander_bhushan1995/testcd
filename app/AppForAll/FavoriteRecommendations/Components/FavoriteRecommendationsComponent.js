import React, { useEffect, useContext } from 'react';
import { NativeModules, Platform, ScrollView, Text, View } from 'react-native';
import { HeaderBackButton } from 'react-navigation';
import colors from '../../../Constants/colors';
import useBackHandler from '../../../CardManagement/CustomHooks/useBackHandler';
import { TextStyle } from "../../../Constants/CommonStyle";
import { stringConstants } from '../DataLayer/StringConstants';
import { FavoriteRecommendationsContext } from '../Navigation/index.favoriterecommendations';
import { StyleSheet } from "react-native";
import spacing from "../../../Constants/Spacing";
import RecommendationComponent from "../../../Catalyst/components/RecommendationComponent";
import { PLATFORM_OS } from '../../../Constants/AppConstants';
import * as FavoriteRecommendationsAPI from '../DataLayer/FavoriteRecommendationsAPIHelper';
import * as Actions from '../DataLayer/Actions';
import Loader from '../../../Components/Loader';
import { AppForAppFlowStrings } from '../../Constant/AppForAllConstants';
import { CATALYST_CARD_CLICK, CATALYST_CARD_VIEW } from '../../../Constants/WebengageEvents';
import { logWebEnageEvent, trackAppScreen } from '../../../commonUtil/WebengageTrackingUtils';
import { CATALYST_FAVOURITE_SCREENVIEW, HOME_TAB, CATALYST_FAVOURITE_SCREEN } from "../../../Constants/WebengageEvents";

const bridgeRef = NativeModules.ChatBridge;

export default function FavoriteRecommendationsComponent(props) {
    const { loader, favoriteRecommendationsState, favoriteRecommendationsDispatch } = useContext(FavoriteRecommendationsContext);
    const { isLoading, loaderMessage } = loader
    const { favoriteRecommendations } = favoriteRecommendationsState;
    useBackHandler(() => bridgeRef.goBack());

    useEffect(() => {
        let eventData = new Object();
        logWebEnageEvent(CATALYST_FAVOURITE_SCREENVIEW, eventData);
    }, [])

    const updateFavoriteRecommendationStatus = (rcId, rId, uiComponentId, isFavorite) => {
        favoriteRecommendationsDispatch({ type: Actions.REMOVE_FAVORITE_RECOMMENDATIONS, data: { rId: rId, uiComponentId: uiComponentId } });
        FavoriteRecommendationsAPI.updateFavoriteRecommendation(rcId, [{ rId: rId, uiComponentId: uiComponentId, isFavorite: isFavorite }], (response, error) => {
            if (response) {
                bridgeRef.updateFavoriteRecommendation(rId, uiComponentId, isFavorite);
            }
        })
    }

    return (
        <View style={style.container}>
            <Loader isLoading={isLoading} loadingMessage={loaderMessage} />
            <ScrollView
                contentContainerStyle={style.scrollViewContainerStyle}
                horizontal={false}
                nestedScrollEnabled={true}
                bounces={Platform.OS === "ios"}
                style={{ flex: 1, backgroundColor: colors.color_F3F2F7 }}>
                <RecommendationComponent isFromFav={true} data={favoriteRecommendations} markFavorite={(index, data) => {
                    updateFavoriteRecommendationStatus(favoriteRecommendations.rcId, data.rid, data.uiComponentId, !(data.favorite?.value ?? false));
                }}
                    componentVisible={(index, data) => {
                        let eventData = new Object();
                        eventData[AppForAppFlowStrings.componentId] = data?.uiComponentId
                        eventData[AppForAppFlowStrings.id] = data?.rid
                        eventData[AppForAppFlowStrings.commId] = favoriteRecommendations.rcId
                        eventData[AppForAppFlowStrings.product] = data?.product
                        eventData[AppForAppFlowStrings.service] = data?.service
                        eventData[AppForAppFlowStrings.commCat] = data?.commCategory
                        eventData[AppForAppFlowStrings.commSubcat] = data?.commSubcategory
                        eventData[AppForAppFlowStrings.position] = index
                        eventData[AppForAppFlowStrings.location] = CATALYST_FAVOURITE_SCREEN
                        logWebEnageEvent(CATALYST_CARD_VIEW, eventData);
                    }}
                    onClick={(index, data, actionUrl) => {
                        let eventData = new Object();
                        eventData[AppForAppFlowStrings.componentId] = data?.uiComponentId?.toString()
                        eventData[AppForAppFlowStrings.id] = data?.rid?.toString()
                        eventData[AppForAppFlowStrings.commId] = favoriteRecommendations.rcId?.toString()
                        eventData[AppForAppFlowStrings.product] = data?.product?.toString()
                        eventData[AppForAppFlowStrings.service] = data?.service?.toString()
                        eventData[AppForAppFlowStrings.commCat] = data?.commCategory?.toString()
                        eventData[AppForAppFlowStrings.commSubcat] = data?.commSubcategory?.toString()
                        eventData[AppForAppFlowStrings.position] = index
                        eventData[AppForAppFlowStrings.location] = CATALYST_FAVOURITE_SCREEN
                        logWebEnageEvent(CATALYST_CARD_CLICK, eventData);
                        bridgeRef.processDeeplink(actionUrl, eventData);
                    }}
                    onExploreViewClick={() => {
                        let eventData = new Object();
                        eventData[AppForAppFlowStrings.location] = CATALYST_FAVOURITE_SCREEN
                        logWebEnageEvent(HOME_TAB, eventData);
                        bridgeRef.goBack();
                     }}
                     />
            </ScrollView>
        </View>
    );
}

FavoriteRecommendationsComponent.navigationOptions = () => {
    return {
        headerTitle: <View style={Platform.OS === 'ios' ? { alignItems: 'center' } : { alignItems: 'flex-start' }}>
            <Text style={TextStyle.text_16_bold}>{stringConstants.favoriteRecommendatinsScreenTitle}</Text>
        </View>,
        headerLeft: <HeaderBackButton tintColor={colors.black} onPress={() => bridgeRef.goBack()} />,
        headerTitleStyle: { color: colors.white },
        headerTintColor: colors.white,
    };
};


export const style = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: colors.white
    },
    scrollViewContainerStyle: {
        backgroundColor: colors.color_F3F2F7,
        paddingBottom: Platform.OS == PLATFORM_OS.ios ? spacing.spacing40 : 0
    },
    headerTitle: {
        fontSize: 16,
        fontFamily: "Lato",
        color: colors.charcoalGrey
    },
    tabBarTextStyle: {
        fontSize: 13,
        fontFamily: "Lato-Semibold",
        lineHeight: 20
    },
    tabBarUnderlineView: {
        backgroundColor: colors.blue028,
        height: spacing.spacing4,
        borderRadius: spacing.spacing2
    },
    tabContainerView: {
        borderBottomColor: colors.grey,
    }
});
