export const GET_FAVORITE_RECOMMENDATIONS = 'GET_FAVORITE_RECOMMENDATIONS';
export const REMOVE_FAVORITE_RECOMMENDATIONS = 'REMOVE_FAVORITE_RECOMMENDATIONS';
export const MARK_FAVORITE = 'MARK_FAVORITE';
export const ERROR = 'ERROR';