import { HTTP_GET, HTTP_POST, executeApi } from "../../../network/ApiManager";

import {APIData} from '../../../../index';

import { GET_CATALYST_RECOMMENDED_UI_COMPONENTS_SUB_URL, UPDATE_FAVORITE_RECOMMENDATIONS_URL } from '../../../Constants/ApiUrls';

const getDataFromApiGateway = (apiParams) => {
    let params = APIData;
    apiParams.apiProps = {baseUrl: params?.api_gateway_base_url, header: params?.apiHeader};
    return apiParams;
};

const custId = () => {
    return APIData?.customer_id;
};

export const getCatalystRecommendedUIComponents = (isOnlyFavorite, callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: GET_CATALYST_RECOMMENDED_UI_COMPONENTS_SUB_URL(custId(), isOnlyFavorite),
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams));
};

export const updateFavoriteRecommendation = (rcId, favoriteData, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: UPDATE_FAVORITE_RECOMMENDATIONS_URL(custId()),
        requestBody: {
            rcId: rcId,
            components: favoriteData,
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
}