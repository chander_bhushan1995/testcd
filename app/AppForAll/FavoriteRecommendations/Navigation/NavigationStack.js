import {createStackNavigator} from 'react-navigation';

import FavoriteRecommendationsComponent from '../Components/FavoriteRecommendationsComponent';
import Dialog from '../../../Components/DialogView';

const navigator = createStackNavigator({
    FavoriteRecommendationsComponent: {
        screen: FavoriteRecommendationsComponent,
    },
}, {});

const navigator1 = createStackNavigator({
    Main: {
        screen: navigator,
    },
    Dialog: {
        screen: Dialog,
    },
}, {
    mode: 'modal',
    headerMode: 'none',
    initialRouteName: 'Main',
});

export default navigator1;
