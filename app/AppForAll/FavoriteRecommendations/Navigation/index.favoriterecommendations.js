import React, { useRef, useReducer, useEffect } from 'react';
import AppNavigator from './NavigationStack';
import useLoader from '../../../CardManagement/CustomHooks/useLoader';
import DialogView from '../../../Components/DialogView';
import * as FavoriteRecommendationsAPI from '../DataLayer/FavoriteRecommendationsAPIHelper';
import * as Actions from '../DataLayer/Actions';
import codePush from "react-native-code-push";
import {setAPIData, updateAPIData} from "../../../../index";

export const FavoriteRecommendationsContext = React.createContext();

const initialCardsState = {
    favoriteRecommendations: {},
    error: null,
};

const reducer = (state = initialCardsState, action) => {
    switch (action.type) {
        case Actions.GET_FAVORITE_RECOMMENDATIONS:
            return {
                ...state,
                favoriteRecommendations: action.data
            };
        case Actions.REMOVE_FAVORITE_RECOMMENDATIONS:
            return {
                ...state,
                favoriteRecommendations: {
                    ...state.favoriteRecommendations,
                    components: state.favoriteRecommendations.components?.filter((value) => !(value.rid === action.data?.rId && value.uiComponentId === action.data?.uiComponentId))
                },
            };
        default:
            return state;
    }
};

function Root(props) {
    setAPIData(props);

    console.ignoredYellowBox = ['Warning:'];
    console.disableYellowBox = true;

    const [isLoading, loaderMessage, startLoading, stopLoader] = useLoader();

    const alertRef = useRef(null);

    const showAlert = (title, alertMessage, primaryButton = { text: 'Yes', onClick: () => { } }, secondaryButton, checkBox) => {
        alertRef.current.showDailog({
            title: title,
            message: alertMessage,
            primaryButton: primaryButton,
            secondaryButton: secondaryButton,
            checkBox: checkBox,
            cancelable: true,
            onClose: () => { },
        });
    };

    const [favoriteRecommendationsState, favoriteRecommendationsDispatch] = useReducer(reducer, initialCardsState);

    useEffect(() => {
        updateAPIData(props)
    }, [props])

    useEffect(() => {
        startLoading('Loading Favorite Recommendations');
        getFavoriteRecommendations();
    }, []);

    const getFavoriteRecommendations = () => {
        FavoriteRecommendationsAPI.getCatalystRecommendedUIComponents(true, (response, error) => {
            stopLoader();
            if (response?.data) {
                favoriteRecommendationsDispatch({ type: Actions.GET_FAVORITE_RECOMMENDATIONS, data: response?.data });
            } else {
                showAlert("Unable to get favorite recommendations", "Please check your internet connection", { text: "Ok" })
            }
        });
    };

    return (
        <FavoriteRecommendationsContext.Provider value={{
            loader: { isLoading: isLoading, loaderMessage: loaderMessage },
            favoriteRecommendationsState: favoriteRecommendationsState,
            favoriteRecommendationsDispatch: favoriteRecommendationsDispatch
        }}>
            <AppNavigator />
            <DialogView ref={alertRef} />
        </FavoriteRecommendationsContext.Provider>
    );
}

export default codePush(Root);
