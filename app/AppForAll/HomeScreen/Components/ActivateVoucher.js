

import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableWithoutFeedback, Image } from 'react-native'
import spacing from '../../../Constants/Spacing'
import { CommonStyle, TextStyle } from '../../../Constants/CommonStyle'
import colors from '../../../Constants/colors'
import { handleHomeScreenActions } from '../Helpers/ButtonActions'

const ActivateVoucher = props => {

    const data = props.data
    const title = data.title
    const button = data.button
    const dismissImage = data.dismissImage
    const [dismiss, setdismiss] = useState(false)

    const handleOnDismiss = () => {
        setdismiss(true)
    }

    return (
        <View>
            {!dismiss
                ? <View style={[CommonStyle.cardContainerWithShadow, styles.rootView]}>
                    <View>
                        <Text style={styles.title}>{title}</Text>
                        <TouchableWithoutFeedback style={{ marginTop: spacing.spacing4 }} onPress={() => { handleHomeScreenActions(button.action, null) }}>
                            <Text style={styles.buttonText}>{button.text}</Text>
                        </TouchableWithoutFeedback>
                    </View>
                    <TouchableWithoutFeedback style={styles.rightImageBtn} onPress={handleOnDismiss}>
                        <Image source={dismissImage} style={styles.image} />
                    </TouchableWithoutFeedback>
                </View>
                : null
            }
        </View>
    )
}

export default ActivateVoucher

const styles = StyleSheet.create({

    rootView: {
        flexDirection: 'row',
        borderRadius: spacing.spacing4,
        padding: spacing.spacing12,
        marginHorizontal: spacing.spacing16,
        marginTop: spacing.spacing24,
        justifyContent: 'space-between',
        alignItems: 'center'
    },

    title: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
    },

    image: {
        height: 24,
        width: 24,
    },
    buttonText:{
        ...TextStyle.text_12_bold,
        color: colors.blue028
    },

    rightImageBtn:{
        alignSelf: 'center'
    }
})
