import React from 'react'
import { StyleSheet, Text, View, FlatList } from 'react-native'
import spacing from '../../../Constants/Spacing'
import { getComponent } from './ComponentsFactory'
import { TextStyle } from '../../../Constants/CommonStyle'
import { CommonStyle } from '../../../Constants/CommonStyle'
import colors from '../../../Constants/colors'

const CardContainerComponent = props => {
    const components = props.data?.components


    const _renderItem = ({ item }) => {
        const Component = getComponent(item.type)
        return <Component data={item} />
    }

    return (
        <View style={[CommonStyle.cardContainerWithShadow, { borderRadius: spacing.spacing8 }]}>
            <FlatList
                showsVerticalScrollIndicator={false}
                ItemSeparatorComponent={props => { return <View style={styles.itemSeprator} /> }}
                data={components}
                renderItem={_renderItem}
                keyExtractor={(item, index) => { return index.toString() }}
            />
        </View>
    )
}

export default CardContainerComponent

const styles = StyleSheet.create({
    itemSeprator: {
        height: spacing.spacing1,
        backgroundColor: colors.greyF1F2F3
    }
})
