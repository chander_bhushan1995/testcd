

import React, { useRef, useState } from 'react'
import {StyleSheet, Text, View, Dimensions, Alert} from 'react-native';
import { getComponent } from './ComponentsFactory'
import spacing from '../../../Constants/Spacing'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import {isResetSpotlightPagination, resetStateOf, setCurrentSpotliteScrollIndex, addEventPriority, eventSendForPriority} from '../Screens/HomeScreen';
import { SPOTLIGHT_VIEW, RECO_SCREEN } from '../../../Constants/WebengageEvents';
import { AppForAppFlowStrings } from '../../Constant/AppForAllConstants';
import {logWebEnageEvent} from '../../../commonUtil/WebengageTrackingUtils';

const CarouselVariation2 = props => {

    const data = props.data || {};
    const components = data.components || [];
    const isSingleItem = (props.data.components.length == 1) || 0
    const [activeDotIndex,setActiveIndex] = useState(0)
    const carouselRef = useRef(null)

    if(resetStateOf.isResetSpotlightPagination){
        resetStateOf.isResetSpotlightPagination = false
        setActiveIndex(0)
    }

    return (
        <View>
            <Carousel layout={'default'}
                inactiveSlideScale={0.95}
                containerCustomStyle={{marginVertical: -15}}
                ref={carouselRef}
                itemWidth={Dimensions.get('screen').width - 40}
                sliderWidth={Dimensions.get('screen').width}
                data={components}
                onSnapToItem={(index)=>{
                    let data = components[index];
                    if (!eventSendForPriority.includes(data?.priority)) {
                        let eventData = new Object();
                        eventData[AppForAppFlowStrings.cardTitle] = data?.header?.value
                        eventData[AppForAppFlowStrings.cardType] = data?.cardType
                        eventData[AppForAppFlowStrings.position] = index + 1
                        eventData[AppForAppFlowStrings.location] = "Homepage"
                        logWebEnageEvent(SPOTLIGHT_VIEW, eventData);
                        addEventPriority(data?.priority)
                    }
                    setCurrentSpotliteScrollIndex(index)
                    setActiveIndex(index)
                }}
                renderItem={({ item,index }) => {
                    const Component = getComponent(item.type);
                    return <Component style={{ marginHorizontal: 0 }} data={item}
                     index={index}/>
                }}
            />
            {!isSingleItem
                ? <Pagination
                    dotsLength={components.length}
                    containerStyle={styles.paginationContainerStyle}
                    dotStyle={styles.activeDotStyle}
                    dotContainerStyle={styles.dotContainerStyle}
                    inactiveDotStyle={styles.inactiveDotStyle}
                    activeDotIndex={activeDotIndex}
                />
                : null}
        </View>
    )
}

export default CarouselVariation2

const styles = StyleSheet.create({
    carouselContainerStyle: {
        borderRadius: 0,
        elevation: 0,
        shadowOpacity: 0.0,
        shadowRadius: 0,
        shadowOffset: {
            height: 0,
            width: 0
        },
        margin: spacing.spacing0,
    },
    activeDotStyle: {
        height: 6,
        width: 18,
        borderRadius: 3
    },
    dotContainerStyle: {
        height: 0,
        padding: 0,
        margin: 0,
        marginHorizontal: 3
    },
    inactiveDotStyle: {
        height: 6,
        width: 6,
        borderRadius: 3
    },
    paginationContainerStyle: {// default styles for paginationContainer view
        height: 30,
        paddingVertical: 10,
        marginHorizontal: 0,
        alignItems: "center",
    },
})
