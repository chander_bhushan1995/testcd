import SpotLightComponent from './SpotLightComponent'
import OfferCardComponent from './OfferCardComponent'
import TitleDescriptionListContainer from './TitleDescriptionListContainer'
import LeftAlignedComponent from './LeftAlignedComponent'
import WhiteBackgroundImageComponent from './WhiteBackgroundImageComponent'
import SectionalViewComponent from './SectionalViewComponent'
import CarouselVariation2 from './CarouselVariation2'
import CardContainerComponent from './CardContainerComponent'
import ActivateVoucher from './ActivateVoucher'
import OfferCardSkeleton from './OfferCardSkeleton'
import ScheduledUIComponent from './ScheduledUIComponent';
import RecommendationComponent from "../../../Catalyst/components/RecommendationComponent";

export const Components = { //All Components for Home Screen should register here to show on home screen
    SpotLightComponent: SpotLightComponent,
    OfferCardComponent: OfferCardComponent,
    TitleDescriptionListContainer: TitleDescriptionListContainer,
    LeftAlignedComponent: LeftAlignedComponent,
    WhiteBackgroundImageComponent: WhiteBackgroundImageComponent,
    SectionalViewComponent: SectionalViewComponent,
    CarouselVariation2: CarouselVariation2,
    CardContainerComponent: CardContainerComponent,
    ActivateVoucher: ActivateVoucher,
    OfferCardSkeleton:OfferCardSkeleton,
    ScheduledUIComponent: ScheduledUIComponent,
    RecommendationComponent:RecommendationComponent
}

export const getComponent = (componentName) => { // a method to get component reference of given name in parameter
    return Components[componentName];
}


export const Tags = {   //These are the tags used to identify each level of component hierarchy on home screen
    SpotLightSectionalViewComponent: 100,
    SpotLightCarouselV2: 101,
    SpotLightComponent: 102,

    ForMobileSectionalViewComponent: 103,
    ForMobileCardContainerComponent: 104,
    BuyBackLeftAlignedComponent: 105,
    MobilePerformanceLeftAlignedComponent: 106,

    ForFinanceSectionalViewComponent: 107,
    ForFinanceCardContainerComponent: 108,
    CardBillLeftAlignedComponent: 109,
    CardOffersLeftAlignedComponent: 110,
    NearByATMLeftAlignedComponent: 111,
    NearByBankLeftAlignedComponent: 112,

    ForAppliancesSectionalViewComponent: 113,
    ForAppliancesCardContainerComponent: 114,
    ExtendedWarrantyLeftAlignedComponent: 115,
    HomeAssistLeftAlignedComponent: 116,

    MHCTitleDescriptionListContainer: 117,

    OffersTitleDescriptionListContainer: 118,
    OffersOfferCardComponent: 119,

    ActivateVoucherCard: 120,

    QuickRepairsSectionalViewComponent: 121,
    QuickRepairsCardContainerComponent: 122,
    ACServiceLeftAlignedComponent: 123,
    WashingMachineLeftAlignedComponent: 124,

    QuickRepairsTitleDescriptionListContainer: 125,
    ScheduledUIComponentContainer: 126,
    RecommendationComponent: 127,

}

/*
* This is a method to update a key's value within JSON and return the complete JSON after update
* */
export const updateObjects = (obj, key, val, newVal) => {
    let newValue = newVal;
    let objects = [];
    for (let i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(updateObjects(obj[i], key, val, newValue));
        } else if (i == key && obj[key] == val) {
            for (const property in newVal) {
                obj[property] = newVal[property]
            }
        }
    }
    return obj;
}
