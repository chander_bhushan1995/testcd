

import React, { useContext } from 'react'
import { StyleSheet, Text, View, Image, TouchableWithoutFeedback } from 'react-native'
import spacing from '../../../Constants/Spacing'
import Dimen from '../../../Constants/Dimens'
import { TextStyle } from '../../../Constants/CommonStyle'
import colors from '../../../Constants/colors'
import TagComponent from '../../../Catalyst/components/common/TagsComponent'
import { handleHomeScreenActions } from '../Helpers/ButtonActions'
import SkeletonLeftImage from './Skeletons/SkeletonsLeftImage'
import { HomeScreenContext } from '../Screens/HomeScreen'
import { AppForAppFlowStrings } from '../../Constant/AppForAllConstants'
import TextView from '../../../Catalyst/components/common/TextView';

const LeftAlignedComponent = props => {

    const { homeScreenState, schedulePickupSheet} = useContext(HomeScreenContext)
    const image = props.data?.image
    const title = props.data?.title
    const description = props.data?.description
    const tag = props.data?.tag
    const imageBackground = props.data?.imageBackground
    const isBadgeEnabled = props.data?.isBadgeEnabled
    const note = props.data?.note
    const subDescription = props.data?.subDescription
    const cardAction = props.data?.buttonAction
    const isShowSkeleton = props.data?.isShowSkeleton
    let data = props.data?.data     //this variable is representing the data which needs to be send while handling card action
    data = title == AppForAppFlowStrings.cardOffers ? homeScreenState.cardsResponse : data

    return (
        <TouchableWithoutFeedback onPress={() => {
            handleHomeScreenActions(cardAction, data,{location: AppForAppFlowStrings.home}, schedulePickupSheet)
        }}>
            <View>
                {isShowSkeleton
                    ? <SkeletonLeftImage />
                    : <View style={styles.container}>
                        <View style={[styles.imageContainer, { backgroundColor: imageBackground }]}>
                            <Image source={image} style={styles.image} />
                            {tag        //if tag is available
                                ? <View style={styles.tagContainer}>
                                    <TagComponent tags={[tag]}
                                        singleTagStyle={styles.singleTagStyle}
                                        textStyle={{ ...TextStyle.text_10_bold, color: colors.white }}
                                    />
                                </View>
                                : null
                            }
                            {isBadgeEnabled ? <View style={styles.badge} /> : null}
                        </View>
                        <View style={styles.textContainer}>
                            {title ? <Text style={styles.title}>{title}</Text> : null}
                            {description ? <TextView style={styles.description} texts={{"value": description,"isHTML":true}}/> : null}
                            {note ? <Text style={styles.note}>{note}</Text> : null}
                            {subDescription ? <Text style={styles.subDescription}>{subDescription}</Text> : null}
                        </View>
                    </View>
                }
            </View>
        </TouchableWithoutFeedback>
    )
}

export default LeftAlignedComponent

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        paddingHorizontal: spacing.spacing12,
        paddingVertical: spacing.spacing20,
    },
    title: {
        ...TextStyle.text_12_bold,
        color: colors.color_888F97
    },
    description: {
        ...TextStyle.text_16_bold,
        marginTop: spacing.spacing4,
    },
    textContainer: {
        marginHorizontal: spacing.spacing16,
        flex: 1,
        // flexWrap: 'wrap',
    },
    imageContainer: {
        borderRadius: 8,
        // overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'center',
        height: Dimen.dimen64,
        width: Dimen.dimen64,
    },
    image: {
        resizeMode: 'contain',
        height: Dimen.dimen64,
        width: Dimen.dimen64,
    },
    tagContainer: {
        position: 'absolute',
        bottom: -10,
    },
    singleTagStyle: {
        backgroundColor: colors.saffronYellow,
        borderWidth: 2,
        paddingHorizontal: spacing.spacing8,
        paddingVertical: spacing.spacing0,
        borderColor: colors.transparent,
        marginLeft: 0,
        borderRadius: 4,
    },
    badge: {
        position: 'absolute',
        top: -5,
        right: -5,
        height: 14,
        width: 14,
        borderRadius: 7,
        backgroundColor: colors.redBF0,
    }, note: {
        ...TextStyle.text_12_normal,
        color: colors.redBF0,
        marginTop: spacing.spacing8,
    }, subDescription: {
        ...TextStyle.text_12_normal,
        color: colors.color_888F97,
        marginTop: spacing.spacing8,
    }
})
