import React, { useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableWithoutFeedback } from 'react-native';
import spacing from '../../../Constants/Spacing'
import { CommonStyle, TextStyle } from '../../../Constants/CommonStyle';
import colors from '../../../Constants/colors'
import { handleHomeScreenActions } from '../Helpers/ButtonActions'
import SkeletonsRightImage from './Skeletons/SkeletonsRightImage'
import images from '../../../images/index.image';
const OfferCardComponent = props => {

    const title = props.data?.title
    const description = props.data?.description
    const cardAction = props.data?.buttonAction
    const isShowSkeleton = props.data?.isShowSkeleton
    const tagging = props.data?.tagging
    const moreText = props.data?.moreText

    const [imageSource, setImageSource] = useState(props.data?.image?.length ? { uri: props.data?.image } : images.defaultOfferImage)
    return (
        <TouchableWithoutFeedback onPress={() => { handleHomeScreenActions(cardAction, props.data.offerData) }}>
            <View style={styles.container}>
                {
                    isShowSkeleton
                        ? <SkeletonsRightImage />
                        : <View style={styles.textImageContainer}>
                            <View style={styles.textContainer}>
                                {
                                    tagging
                                        ? <View style={{ flexDirection: "row" }}>
                                            <View style={styles.badgeStyle}>
                                                <Text style={styles.badgeTextStyle}>{tagging}</Text>
                                            </View><Text style={styles.extraText}>{moreText}</Text>
                                        </View>
                                        : null
                                }
                                <Text style={styles.title} numberOfLines={1} adjustsFontSizeToFit={true}>{title}</Text>
                                <Text style={styles.description} numberOfLines={2} adjustsFontSizeToFit={true}>{description}</Text>
                            </View>
                            <Image source={imageSource} style={styles.image} onError={() => setImageSource(images.defaultOfferImage)} />
                        </View>
                }
            </View>
        </TouchableWithoutFeedback>
    )
}

export default OfferCardComponent

const styles = StyleSheet.create({
    container: {
        ...CommonStyle.cardContainerWithShadow,
        borderRadius: 4,
        width: 280,
    },
    textImageContainer: {
        width: 280,
        height: 120,
        flexDirection: 'row',
    },
    textContainer: {
        paddingVertical: spacing.spacing22,
        paddingHorizontal: spacing.spacing12,
        flex: 1,
    },
    title: {
        ...TextStyle.text_16_bold,
        marginTop: 10,
    },
    description: {
        ...TextStyle.text_14_normal,
    },
    image: {
        width: 120,
        height: 120,
        alignSelf: 'flex-end',
        resizeMode: 'contain',
    },
    badgeStyle: {
        backgroundColor: colors.white,
        paddingHorizontal: spacing.spacing4,
        borderRadius: 2,
        borderWidth: 1,
        borderColor: colors.color_0282F0,
        flexWrap: "wrap",
    },
    badgeTextStyle: {
        ...TextStyle.text_10_bold,
        color: colors.color_0282F0,
        // letterSpacing: 2,
    },
    extraText: {
        ...TextStyle.text_10_normal,
        marginLeft: spacing.spacing2
    }
})
