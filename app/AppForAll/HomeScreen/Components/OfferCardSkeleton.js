import React from 'react';
import { StyleSheet, View } from 'react-native';
import { CommonStyle } from '../../../Constants/CommonStyle';
import SkeletonsRightImage from './Skeletons/SkeletonsRightImage'


const OfferCardSkeleton = props => {
    return (
        <View style={styles.container}>
            <SkeletonsRightImage />
        </View>
    )
}

export default OfferCardSkeleton

const styles = StyleSheet.create({
    container: {
        ...CommonStyle.cardContainerWithShadow,
        borderRadius: 4,
        width: 280,
    },
})
