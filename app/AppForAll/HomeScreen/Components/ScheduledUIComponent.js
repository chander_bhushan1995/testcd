import React, {useEffect, useState, useRef} from 'react';
import {
    View,
    Text,
    StyleSheet,
    FlatList,
    Dimensions,
    PanResponder,
    Animated,
    Platform,
} from 'react-native';
import {CommonStyle, TextStyle} from '../../../Constants/CommonStyle';
import colors from '../../../Constants/colors';
import dimens from '../../../Constants/Dimens';
import RightArrowButton from '../../../CommonComponents/RightArrowButton';
import spacing from '../../../Constants/Spacing';
import * as Progress from 'react-native-progress';
import {handleHomeScreenActions} from '../Helpers/ButtonActions';
import TagComponent from '../../../Catalyst/components/common/TagsComponent';
import {PLATFORM_OS} from '../../../Constants/AppConstants';

let activeProgress = 0;
// let value = new Animated.Value(0)
const ScheduledUIComponent = React.memo(props => {

    let data = props.data;
    let cardsData = data.cardsData ?? [];
    // const [cardsDataState,setCardsDataState] = useState(props.data?.cardsData ?? [])
    const cardsCountRef = useRef(0)
    // let cardsData = dataArray;
    //ANIMATION CONFIG
    const [activeProgressValue, setProgressValue] = useState(0);
    const value = useRef(new Animated.Value(0));
    const PROGRESSBAR_ANIMATION = 3000;
    const PROGRESSBAR_MAX_WIDTH = Dimensions.get('screen').width/10

    useEffect(() => {
        return () =>
        {
            activeProgress = 0
            value.current.stopAnimation();
            value.current.removeAllListeners()
        }


    },[]);
    useEffect(() => {
        cardsCountRef.current = cardsData.length
        if (cardsData.length <= 0){
            setProgressValue(0)
            activeProgress = 0
            value.current.stopAnimation();
            value.current.removeAllListeners()
            // value.current = new Animated.Value(0)
        } else if (cardsData.length > 1) {
            setProgressValue(0)
            activeProgress = 0;
            value.current.stopAnimation();
            value.current.removeAllListeners()
            // value.current = new Animated.Value(0)
            value.current.addListener(({value}) => {
                setProgressValue(value);
            });
            // @Nishant I have added one sec more sec for android prevent crash i.e view has been
            if (Platform.OS === PLATFORM_OS.ios) {
                startAnimation();
            }
        }
    }, [props.data]);


    const panResponder = React.useRef(
        PanResponder.create({
            // Ask to be the responder:
            onStartShouldSetPanResponder: (evt, gestureState) => {
                return true;
            },
            onStartShouldSetPanResponderCapture: (evt, gestureState) => {
                return false;
            },
            onMoveShouldSetPanResponder: (evt, gestureState) => {
                return !(gestureState.dx === 0 && gestureState.dy === 0);
            },
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => {
                return false;
            },
            onPanResponderGrant: (evt, gestureState) => {
                value.current.stopAnimation();
            },
            onPanResponderMove: (evt, gestureState) => {
            },
            onPanResponderTerminationRequest: (evt, gestureState) => {
                return true;
            },
            onPanResponderRelease: (evt, gestureState) => {
                startAnimation();
            },
            onShouldBlockNativeResponder: (evt, gestureState) => {
                return false;
            },
            onPanResponderTerminate: ()=>{
                startAnimation();
            }
        })).current;


    const startAnimation = () => {
        Animated.timing(value.current, {
            toValue: 1,
            duration: PROGRESSBAR_ANIMATION,
            useNativeDriver: false,
        }).start((status) => {
            if (status.finished) {
                activeProgress = activeProgress == (cardsCountRef.current - 1) ? 0 : ++activeProgress;
                value.current.setValue(0);
                startAnimation();
            }
        });
    };

    //cards data length is 1 then default opacity will be 1 otherwise opacity will be varied on animation value
    const opacity = cardsData.length > 1 ? value.current.interpolate({
        inputRange: [0, 0.10, 0.90, 1],
        outputRange: [1, 1, 1, 1],
    }) : 1;

    if (cardsData.length <= 0) { //cards data empty means no data to show
        return null;
    }

    return (
        <View {...panResponder.panHandlers}
              style={[CommonStyle.cardContainerWithShadow, styles.component, props.style]}>

            <View style={styles.backgroundBottomImgContainer}>
                <Animated.Image style={[styles.backgroundBottomImg, {opacity: opacity}]}
                                source={cardsData[activeProgress]?.image ?? {'url': ''}}/>
            </View>

            <Animated.View style={{flexDirection: 'row', opacity: opacity}}>
                <Text style={styles.headerText} numberOfLines={2}>{cardsData[activeProgress]?.header ?? ''}</Text>
                {cardsData.length > 1 && cardsData[activeProgress]?.button // if there are card and current card having button obj
                    ? <RightArrowButton text={cardsData[activeProgress]?.button?.text ?? ''}
                                        style={styles.rightArrowContainer}
                                        textStyle={styles.buttonText}
                                        imageStyle={styles.rightArrowStyle} onPress={() => {
                        // Alert.alert('button clicked widt data', cardsData[activeProgress]?.data);
                        handleHomeScreenActions(cardsData[activeProgress]?.button.action, cardsData[activeProgress]?.data);
                    }}/>
                    : null
                }
            </Animated.View>
            { cardsData[activeProgress]?.tag
                ?  <TagComponent tags={[cardsData[activeProgress]?.tag]}
                                 style = {{marginTop: 0}}
                                 singleTagStyle={styles.singleTagStyle}
                                 textStyle={{...TextStyle.text_12_bold, color: colors.white}}
                />
                : null
            }
            <Animated.View
                style={{
                    flexDirection: 'row',
                    marginHorizontal: spacing.spacing12,
                    marginTop: spacing.spacing8,
                    opacity: opacity,
                    maxWidth: (Dimensions.get('screen').width-32-136)
                }}>
                <View>
                    <Text style={styles.noteText} numberOfLines={2}>{cardsData[activeProgress]?.firstNote ?? ''}</Text>
                    {cardsData[activeProgress]?.firstSubNote
                        ? <Text style={styles.dateOrTime}
                                numberOfLines={1}>{cardsData[activeProgress]?.firstSubNote ?? ''}</Text>
                        : null
                    }
                </View>

                {cardsData[activeProgress]?.secondNote
                    ? <View style={{marginLeft: spacing.spacing32}}>
                        <Text style={styles.noteText}
                              numberOfLines={1}>{cardsData[activeProgress]?.secondNote ?? ''}</Text>
                        <Text style={styles.dateOrTime}
                              numberOfLines={1}>{cardsData[activeProgress]?.secondSubNote ?? ''}</Text>
                    </View>
                    : null
                }

            </Animated.View>

            {cardsData.length > 1 /*bottom progress bars*/
                ? <View style={{position: 'absolute', bottom: spacing.spacing12, left: spacing.spacing12}}>
                    <FlatList
                        keyExtractor={(item, index) => {
                            return index.toString();
                        }}
                        horizontal={true}
                        data={cardsData}
                        ItemSeparatorComponent={() => <View style={{backgroundColor: 'transparent', width: 10}}/>}
                        renderItem={({item, index}) => {
                            let progress = index < activeProgress ? 1 : 0;
                            return <Progress.Bar borderWidth={0} width={Math.min((Dimensions.get('screen').width-24-108-(cardsData.length*10))/(cardsData.length),PROGRESSBAR_MAX_WIDTH)} height={2}
                                                 progress={index == activeProgress ? activeProgressValue : progress}
                                                 color={colors.white}
                                                 animated={false} unfilledColor='rgba(255, 255, 255, 0.16)'
                                                 // style= {{maxWidth: Dimensions.get('screen').width/10}}
                            />;
                        }}
                    />
                </View>
                : /*bottom action button*/
                <View style={{position: 'absolute', bottom: spacing.spacing12, left: spacing.spacing12}}>
                    <View>
                        <RightArrowButton text={cardsData[activeProgress]?.button?.text ?? ''}
                                          style={{marginTop: 0, marginBottom: 0}}
                                          textStyle={styles.buttonText} imageStyle={styles.rightArrowStyle}
                                          onPress={() => {
                                              handleHomeScreenActions(cardsData[activeProgress]?.button.action, cardsData[activeProgress]?.data);
                                          }}/>
                    </View>
                </View>
            }
        </View>
    );
});

const styles = StyleSheet.create({
    component: {
        borderRadius: dimens.dimen4,
        backgroundColor: colors.darkblue64,
        marginHorizontal: spacing.spacing16,
        marginTop: spacing.spacing32,
        height: dimens.dimen170,
        overflow: 'hidden',
    },
    headerText: {
        ...TextStyle.text_16_bold,
        color: colors.white,
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing12,
        flex: 1,
    },
    buttonText: {
        ...TextStyle.text_16_bold,
        color: colors.white,
    },
    rightArrowContainer: {
        marginTop: spacing.spacing16,
        marginBottom: spacing.spacing0,
         alignSelf: 'flex-start',
        // justifyContent: 'flex-end',
    },
    rightArrowStyle: {
        width: 8,
        height: 12,
        marginLeft: spacing.spacing4,
        marginRight: spacing.spacing16,
        resizeMode: 'stretch',
        tintColor: 'white',
    },
    noteText: {
        ...TextStyle.text_12_normal,
        color: colors.white,
    },
    dateOrTime: {
        ...TextStyle.text_14_bold,
        color: colors.white,
        marginTop: spacing.spacing4,
    },
    backgroundBottomImgContainer: {
        width: dimens.dimen96,
        height: dimens.dimen96,
        borderRadius: dimens.dimen96 / 2,
        position: 'absolute',
        bottom: -(spacing.spacing22),
        right: -(spacing.spacing10),
        backgroundColor: colors.white,
        alignItems: 'center',
        justifyContent: 'center',
    },
    backgroundBottomImg: {
        width: dimens.dimen60,
        height: dimens.dimen60,
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    singleTagStyle: {
        backgroundColor: colors.whiteOpacity_point_1,
        borderWidth: 1,
        paddingHorizontal: spacing.spacing4,
        paddingVertical: spacing.spacing0,
        borderColor: colors.white,
        marginLeft: 12,
        marginTop: spacing.spacing8,
        alignSelf: 'center',
        borderRadius: 4,
    },
});

export default ScheduledUIComponent;
