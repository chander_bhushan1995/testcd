import {StyleSheet, Text, View, FlatList, Alert} from 'react-native';
import React from "react";
import {CommonStyle, TextStyle} from '../../../Constants/CommonStyle';
import colors from "../../../Constants/colors";
import { getComponent, Components } from './ComponentsFactory'
import spacing from '../../../Constants/Spacing';
import TagComponent from '../../../Catalyst/components/common/TagsComponent';

const SectionalViewComponent = (props) => {
    const { data } = props;
    const isTagEnabled = data.isTagEnabled ?? false
    const tagText = data.tagText ?? "NEW"
    const components = data.components
    const _renderItem = ({ item }) => {
        const Component = getComponent(item.type)
        return <Component data={item} />
    }

    return (
        <View style={[SectionViewStyle.rootViewStyle,
            data.isShadowEnabled ? {...CommonStyle.cardContainerWithShadow,shadowRadius: 0, shadowOffset: { width: 0, height: 2 }, marginTop: -2}: null
            ,{backgroundColor: data.bgColor},
        ]}>
            <View style={{flexDirection: "row"}}>
                <Text style={[TextStyle.text_12_bold, { marginLeft: 16 }]}>{data.title}</Text>
            { isTagEnabled
                ?  <TagComponent tags={[tagText]}
                                 style = {{marginTop: 0}}
                              singleTagStyle={SectionViewStyle.singleTagStyle}
                              textStyle={{...TextStyle.text_10_bold, color: colors.white}}
                    />
                : null
            }
            </View>
            <View style={SectionViewStyle.containerStyle}>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{
                        paddingHorizontal: components[0]?.type != "CarouselVariation2" ? spacing.spacing16 : spacing.spacing0,
                        paddingVertical: spacing.spacing2,
                    }}
                    horizontal={false}
                    data={components}
                    renderItem={_renderItem}
                    keyExtractor={(item, index) => { return index.toString() }}
                />
            </View>
        </View>
    );
};

export const SectionViewStyle = StyleSheet.create({
    rootViewStyle: {
        width: "100%",
        backgroundColor: colors.transparent,
        paddingTop: 40,
        paddingBottom: 4,
        marginRight: spacing.spacing16,
    },

    containerStyle: {
        width: "100%",
        marginTop: 12,
    },

    singleTagStyle: {
        backgroundColor: colors.saffronYellow,
        borderWidth: 2,
        paddingHorizontal: spacing.spacing8,
        paddingVertical: spacing.spacing0,
        borderColor: colors.transparent,
        marginLeft: 10,
        marginTop: 0,
        alignSelf: 'center',
        borderRadius: 4,
    },
});

export default SectionalViewComponent;



