import {StyleSheet, Text, View, FlatList, Alert} from 'react-native';
import React from "react";
import {CommonStyle, TextStyle} from '../../../Constants/CommonStyle';
import colors from "../../../Constants/colors";
import {getComponent, Components} from './ComponentsFactory'
import spacing from '../../../Constants/Spacing';
import TagComponent from '../../../Catalyst/components/common/TagsComponent';
import CarouselVariation3 from "./CarouselVariation3";
import Pagination from "../../../Catalyst/components/common/pagination/Pagination";

const SectionalViewComponentV2 = (props) => {
    const {data} = props;
    const components = data.components

    const _renderItem = ({item}) => {
        return <CarouselVariation3 data={item}/>
    }

    return (
        <View style={{marginVertical: 16, marginHorizontal: 16}}>
            <View style={{flexDirection: "row",marginVertical:16}}>
                <Text style={[TextStyle.text_16_regular]}>{data.title}</Text>
            </View>
            <View >

                <View style={SectionViewStyle.containerStyle}>
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{

                        }}
                        horizontal={false}
                        data={components}
                        renderItem={_renderItem}
                        keyExtractor={(item, index) => {
                            return index.toString()
                        }}
                    />
                </View>
            </View>

        </View>
    );
};

export const SectionViewStyle = StyleSheet.create({
    rootViewStyle: {
        width: "100%",
        backgroundColor: colors.transparent,
        paddingBottom: 4,
        marginRight: spacing.spacing16,
    },

    containerStyle: {
        width: "100%",

    }
});

export default React.memo(SectionalViewComponentV2);



