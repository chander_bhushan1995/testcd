

import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import SkeletonContent from '../../../../lib/src/index'
const SkeletonLeftImage = props => {

    const [show, setshow] = useState(false)

    useEffect(()=>{
        setshow(true)
    },[])

    return (
        <View style={{ margin: 10 }}>
            <SkeletonContent
                containerStyle={{ flex: 1, width: 200, flexDirection: 'row' }}
                isLoading={show}
            >
                <View style={{ height: 66, width: 66 }}>
                </View>
                <View style={{ height: 20, width: 200, marginLeft: 10 }}>
                    <View style={{ height: 10, width: 100, marginLeft: 10 }}></View>
                </View>
            </SkeletonContent>
            <SkeletonContent
                containerStyle={{ flex: 1, width: 200, flexDirection: 'row' }}
                isLoading={show}
            >
                <View style={{ height: 10, width: 200, marginLeft: 76, marginTop: -30 }}>
                </View>
            </SkeletonContent>
        </View>
    )
}

export default SkeletonLeftImage

const styles = StyleSheet.create({})
