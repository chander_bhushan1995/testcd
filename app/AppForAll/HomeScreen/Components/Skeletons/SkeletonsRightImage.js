

import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import SkeletonContent from '../../../../lib/src/index'
const SkeletonsRightImage = props => {

    const [show, setshow] = useState(false)
    useEffect(() => {
        setshow(true)
    }, [])

    return (
        <View style={{ margin: 10 }}>
            <SkeletonContent
                containerStyle={{ flex: 1, width: 200, flexDirection: 'row' }}
                isLoading={show}
            >
                <View style={{ height: 10, width: 180, marginLeft: 10, marginRight: 10 }}></View>
                <View style={{ height: 66, width: 66 }}>
                </View>
            </SkeletonContent>
            <SkeletonContent
                containerStyle={{ flex: 1, width: 180, flexDirection: 'row' }}
                isLoading={show}
            >
                <View style={{ height: 10, width: 180, marginLeft: 10, marginRight: 10, marginTop: -40 }}>
                </View>
            </SkeletonContent>
        </View>
    )
}

export default SkeletonsRightImage

const styles = StyleSheet.create({})
