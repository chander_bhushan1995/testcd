import React from 'react'
import {StyleSheet, View, Image, TouchableWithoutFeedback} from 'react-native';
import spacing from "../../../Constants/Spacing";
import colors from '../../../Constants/colors';
import TagComponent from '../../../Catalyst/components/common/TagsComponent'
import { TextStyle } from '../../../Constants/CommonStyle';
import TextView from '../../../Catalyst/components/common/TextView'
import SkeletonsRightImage from './Skeletons/SkeletonsRightImage';
import { handleHomeScreenActions, ButtonsAction } from '../Helpers/ButtonActions'
import { SPOTLIGHT_CLICK } from '../../../Constants/WebengageEvents';
import { AppForAppFlowStrings } from '../../Constant/AppForAllConstants';
import {logWebEnageEvent} from '../../../commonUtil/WebengageTrackingUtils';

const SpotLightComponent = props => {
    const data = props?.data
    const containerStyle = props?.style
    const headerTextObj = data?.header
    const paragraphTextObj = data?.paragraph
    const image = data?.image
    const tags = data?.tag_text ?? ""
    const isTagEnabled = tags?.length > 0
    const bgColor = data?.bgColor ?? ""
    const isShowSkeleton = data?.isShowSkeleton
    const buttonObject = data?.button
    const isOpenWebView = buttonObject?.openWebView ?? false
    const webViewTitle = buttonObject?.webviewTitle ?? ""
    const actionDeeplinkURL = buttonObject?.action ?? ""
    const cardAction = ButtonsAction.SPOTLIGHT_ACTION
    const position = Number(props.index ?? "0") + 1

    return (
        <TouchableWithoutFeedback onPress={() => {
            let eventData = new Object();
            eventData[AppForAppFlowStrings.cardTitle] = headerTextObj?.value
            eventData[AppForAppFlowStrings.cardType] = data?.cardType
            eventData[AppForAppFlowStrings.position] = position
            eventData[AppForAppFlowStrings.location] = "Homepage"
            logWebEnageEvent(SPOTLIGHT_CLICK,eventData)
            handleHomeScreenActions(cardAction, { url: actionDeeplinkURL,isOpenWebView: isOpenWebView,webViewTitle: webViewTitle})
        }}>
            <View style={[styles.container, containerStyle, bgColor.length ? { backgroundColor: bgColor } : null]}>
                {isShowSkeleton
                    ? <SkeletonsRightImage />
                    : <View style={{ flex: 1 }}>
                        <View style={styles.textContainer}>
                            <TextView texts={headerTextObj} style={styles.headerTextStyle} />
                            <TextView texts={paragraphTextObj} style={styles.paragraphText} />
                        </View>
                    </View>
                }
                <Image />
                <Image source={{ uri: image }} style={styles.image} />
                {isTagEnabled
                    ? <TagComponent tags={[tags]}
                        style={styles.tagContainerStyle}
                        singleTagStyle={styles.singleTagStyle}
                        textStyle={{ ...TextStyle.text_10_bold, color: colors.white }} />
                    : null
                }
            </View>
        </TouchableWithoutFeedback>
    )
}

export default SpotLightComponent

const styles = StyleSheet.create({
    container: {
        borderRadius: 8,
        backgroundColor: colors.green5DBD98,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: spacing.spacing16,
        marginVertical: spacing.spacing16,
        paddingBottom: spacing.spacing8
    },
    singleTagStyle: {
        backgroundColor: colors.saffronYellow,
        borderWidth: 2,
        paddingHorizontal: spacing.spacing8,
        paddingVertical: spacing.spacing0,
        borderColor: colors.transparent,
        borderRadius: 4,
    },
    tagContainerStyle: {
        position: 'absolute',
        marginTop: 0,
        top: 0,
        right: 0
    },
    textContainer: {
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing12
    },
    headerTextStyle: {
        ...TextStyle.text_16_normal,
        flex: 1,
        flexWrap: 'wrap',
    },
    image: {
        height: 104,
        width: 104,
        resizeMode: "contain",
        marginTop: spacing.spacing8,
        marginHorizontal: spacing.spacing8
    },
    paragraphText: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        marginTop: spacing.spacing8,
    }
})
