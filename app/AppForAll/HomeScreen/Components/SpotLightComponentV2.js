import React from 'react'
import {StyleSheet, View, Image, TouchableWithoutFeedback} from 'react-native';
import spacing from "../../../Constants/Spacing";
import colors from '../../../Constants/colors';
import { TextStyle } from '../../../Constants/CommonStyle';
import TextView from '../../../Catalyst/components/common/TextView'
import SkeletonsRightImage from './Skeletons/SkeletonsRightImage';
import { handleHomeScreenActions, ButtonsAction } from '../Helpers/ButtonActions'
import { SPOTLIGHT_CLICK, RECO_SCREEN } from '../../../Constants/WebengageEvents';
import { AppForAppFlowStrings } from '../../Constant/AppForAllConstants';
import {logWebEnageEvent} from '../../../commonUtil/WebengageTrackingUtils';

const SpotLightComponentV2 = props => {
    const data = props?.data
    const containerStyle = props?.style
    const headerTextObj = data?.header
    const paragraphTextObj = data?.paragraph
    const image = data?.image
    const bgColor = data?.bgColor ?? ""
    const isShowSkeleton = data?.isShowSkeleton
    const buttonObject = data?.button
    const isOpenWebView = buttonObject?.openWebView ?? false
    const webViewTitle = buttonObject?.webviewTitle ?? ""
    const actionDeeplinkURL = buttonObject?.action ?? ""
    const cardAction = ButtonsAction.SPOTLIGHT_ACTION
    const position = Number(props.index ?? "0") + 1

    return (

        <TouchableWithoutFeedback onPress={() => {
            let eventData = new Object();
            eventData[AppForAppFlowStrings.cardTitle] = headerTextObj?.value
            eventData[AppForAppFlowStrings.cardType] = data?.cardType
            eventData[AppForAppFlowStrings.position] = position
            eventData[AppForAppFlowStrings.location] = RECO_SCREEN
            logWebEnageEvent(SPOTLIGHT_CLICK,eventData)
            handleHomeScreenActions(cardAction, { url: actionDeeplinkURL,isOpenWebView: isOpenWebView,webViewTitle: webViewTitle})
        }}>
            <View style={[styles.container, containerStyle, bgColor.length ? { backgroundColor: bgColor } : null]}>
                {isShowSkeleton
                    ? <SkeletonsRightImage />
                    : <View style={{ flex: 1 }}>
                        <View style={styles.textContainer}>
                            <TextView texts={headerTextObj} style={styles.headerTextStyle} />
                            <TextView texts={paragraphTextObj} style={styles.paragraphText} />
                        </View>
                    </View>
                }
                <Image />
                <Image source={{ uri: image }} style={styles.image} />
            </View>
        </TouchableWithoutFeedback>
    )
}

export default SpotLightComponentV2

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.green5DBD98,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderRadius:4,
        flex: 1
    },
    textContainer: {
        marginLeft: spacing.spacing16,
        marginVertical:spacing.spacing20
    },
    headerTextStyle: {
        ...TextStyle.text_16_normal,
        flex: 1,
        flexWrap: 'wrap',
    },
    image: {
        height: 104,
        width: 104,
        resizeMode: "contain",
        marginTop: spacing.spacing8,
        marginHorizontal: spacing.spacing8
    },
    paragraphText: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        marginTop: spacing.spacing8,
    }
})
