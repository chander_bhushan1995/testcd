import React, { useContext } from 'react'
import { StyleSheet, Text, View, Dimensions, FlatList, Image, TouchableOpacity, Alert } from 'react-native'
import { TextStyle } from '../../../Constants/CommonStyle'
import colors from '../../../Constants/colors'
import spacing from '../../../Constants/Spacing'
import { getComponent } from './ComponentsFactory'
import { handleHomeScreenActions } from '../Helpers/ButtonActions'
import { HomeScreenContext } from '../Screens/HomeScreen'
const TitleDescriptionListContainer = props => {

    const title = props.data?.title
    const description = props.data?.description
    const components = props.data?.components ?? []
    const bgColor = props.data?.bgColor
    const bgImage = props.data?.bgImage
    const button = props.data?.button
    const buttonText = button?.text
    const buttonAction = button?.action

    const { homeScreenState } = useContext(HomeScreenContext)

    const CONTENT_LEFT_MARGIN = components[0]?.type == "WhiteBackgroundImageComponent" ? (Dimensions.get('screen').width / 1.6) : spacing.spacing16

    const _renderItem = ({ item }) => {
        const Component = getComponent(item.type)
        return <Component data={item} />
    }
    let cardsData = homeScreenState.cardsResponse
    return (
        <View style={[styles.container, props.containerStyle, { backgroundColor: bgColor }]}>
            <Image source={bgImage} style={styles.backgroundImage} />
            <View style={styles.btnTextContainer}>
                <View style={{ flex: 1 }}>
                    <Text style={styles.title}>{title}</Text>
                    <Text style={styles.description}>{description}</Text>
                </View>
                <View>
                    {button
                        ? <TouchableOpacity style={styles.buttonContainer} onPress={() => { handleHomeScreenActions(buttonAction, cardsData) }}>
                            <Text style={styles.buttonText}>
                                {buttonText}
                            </Text>
                        </TouchableOpacity>
                        : null
                    }
                </View>
            </View>
            <FlatList
                showsHorizontalScrollIndicator={false}
                style={styles.listStyle}
                nestedScrollEnabled={true}
                ItemSeparatorComponent={() => { return <View style={{ width: 12, backgroundColor: 'transparent' }} /> }}
                contentContainerStyle={[styles.listContentContainerStyle, { paddingLeft: CONTENT_LEFT_MARGIN }]}
                horizontal={true}
                data={components}
                renderItem={_renderItem}
                keyExtractor={(item, index) => { return index.toString() }}
            />
        </View>
    )
}

const styles = StyleSheet.create({

    container: {
        paddingTop: spacing.spacing20,
        marginLeft: spacing.spacing16,
        marginTop: spacing.spacing16,
        borderTopLeftRadius: 8,
        borderBottomLeftRadius: 8
    },
    title: {
        ...TextStyle.text_16_bold,
        color: colors.white,
        marginRight: spacing.spacing20,
    },
    description: {
        ...TextStyle.text_14_normal,
        marginTop: spacing.spacing8,
        marginRight: spacing.spacing20,
        color: colors.white,
    },
    backgroundImage: {
        resizeMode: 'contain',
        position: 'absolute',
        bottom: spacing.spacing0,
        left: spacing.spacing12,
        opacity: 1,
        width: (Dimensions.get('screen').width / 2.8),
        height: (Dimensions.get('screen').width / 2.8)
    },
    btnTextContainer: {
        flexDirection: 'row',
        marginHorizontal: spacing.spacing12
    },
    buttonContainer: {
        borderRadius: 2,
        borderWidth: 1,
        borderColor: colors.white,
        paddingHorizontal: spacing.spacing16,
        paddingVertical: spacing.spacing8,
    },
    buttonText: {
        ...TextStyle.text_14_bold,
        color: colors.white
    },
    listStyle: {
        marginVertical: spacing.spacing16,
    },
    listContentContainerStyle: {
        paddingRight: spacing.spacing20
    }

})

export default TitleDescriptionListContainer
