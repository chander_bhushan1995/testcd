import {
    Image,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Dimensions,
    NativeModules,
    Platform, findNodeHandle,
} from 'react-native';
import React, {PureComponent} from 'react';
import {TextStyle} from '../../../Constants/CommonStyle';
import colors from '../../../Constants/colors';
import images from '../../../images/index.image';
import {PLATFORM_OS} from '../../../Constants/AppConstants';
import {AppForAppFlowStrings} from '../../Constant/AppForAllConstants';


export const ArrowDirection = {
    TOP: 'TOP',
    DOWN: 'DOWN',
};


const nativeBrigeRef = NativeModules.ChatBridge;
const TRIANGLE_WIDTH = 16;


export default class TooltipComponent extends PureComponent {
    constructor(props) {
        super(props);
        this.selectedDirection = props.arrowDirection ?? ArrowDirection.DOWN;
        this.triangleStyle = props.triangleStyle;
        this.style = this.props.style;
        this.containerStyle = props.containerStyle;
        this.parentRef = props.parentRef;
        this.childRef = props.childRef;
        this.triangleBackgroundColor = this.style?.backgroundColor;
        this.state = {
            height: 0,
            childLayout: null,
            containerLayoutStyle: null, // container style after measuring x,y of caller component
            triangleLayoutStyle: null, // triangle style after measuring x,y of caller component
            tooltipLayout: null,        // tooltip Layout
            opacity: 0,
        };
    }

    // componentDidMount() {
    //     this.handleParentChildLayout();
    // }

    onCancelClick = () => {
        this.props.onCloseTooptip ? this.props.onCloseTooptip() : null;
    };

    handleParentChildLayout = () => {
        if (!this.parentRef && !this.childRef) {
            this.setState({opacity: 1})
            return;
        }
        this.childRef.measureLayout(findNodeHandle(this.parentRef), (pageX, pageY, Width, Height) => {
            this.setState({childLayout: {x: pageX, y: pageY, width: Width, height: Height}});
            switch (this.selectedDirection) {
                case ArrowDirection.DOWN:
                    this.setState({
                        containerLayoutStyle: {
                            top: this.state.childLayout?.y - 2 - (this.state.tooltipLayout?.height ?? 0), // 2 is constant spacing b/w target view and tooltip
                            bottom: null,
                        },
                        triangleLayoutStyle: {
                            marginRight: null,
                            marginLeft: (this.state.childLayout?.x - (TRIANGLE_WIDTH + (TRIANGLE_WIDTH / 2))),
                            alignSelf: 'flex-start',
                        },
                    });
                    break;
                case ArrowDirection.TOP:
                    this.setState({
                        containerLayoutStyle: {
                            top: this.state.childLayout?.y + 2 + this.state.childLayout?.height,  // 2 is constant spacing b/w target view and tooltip
                            bottom: null,
                        },
                        triangleLayoutStyle: {
                            marginRight: null,
                            marginLeft: (this.state.childLayout?.x - (TRIANGLE_WIDTH + (TRIANGLE_WIDTH / 2))),
                            alignSelf: 'flex-start',
                        },
                    });
                    break;
                default:
                    break;
            }
            this.setState({opacity: 1})
        });
    };

    render() {
        if (!this.props.isVisible) {
            if (Platform.OS === PLATFORM_OS.android) {
                nativeBrigeRef.tooltipCancelled();
            }
            return null;
        }
        let {data} = this.props;

        const text = data.text ?? AppForAppFlowStrings.yourTextHere;
        const bottomHeight = Number(data?.bottomHeight ?? 0) ?? 0;
        const imagePath = data.image;

        return (
            <View style={[{
                bottom: bottomHeight,
                left: 0,
                right: 0,
                position: 'absolute',
                width: Dimensions.get('screen').width - 40,
                marginLeft: 20,
                opacity: this.state.opacity
            }, this.state.containerLayoutStyle, this.containerStyle]}
                  onLayout={(event) => {
                      if (!this.state.tooltipLayout) {
                          this.setState({tooltipLayout: event.nativeEvent.layout});
                          this.handleParentChildLayout();
                      }
                  }}
            >
                {this.selectedDirection === ArrowDirection.TOP ?
                    <TopTriangle
                        triangleStyle={[TooltipStyle.topTriangle, this.triangleBackgroundColor ? {borderBottomColor: this.triangleBackgroundColor} : null, this.state.triangleLayoutStyle, this.triangleStyle]}/> : null}
                <View style={[TooltipStyle.rootViewStyle, this.style]}
                      onLayout={(event) => {
                          this.setState({height: event.nativeEvent.layout.height});
                      }}>
                    <Text style={{...TextStyle.text_12_medium, color: colors.white, flex: 1}}>
                        {text}
                    </Text>
                    {imagePath
                        ? <TouchableOpacity style={TooltipStyle.imageContainerStyle} onPress={this.onCancelClick}>
                            <Image style={TooltipStyle.imageStyle} source={imagePath}/>
                        </TouchableOpacity>
                        : null}
                </View>
                {this.selectedDirection === ArrowDirection.DOWN ?
                    <DownTriangle
                        triangleStyle={[TooltipStyle.downTriangle, this.triangleBackgroundColor ? {borderTopColor: this.triangleBackgroundColor} : null, this.state.triangleLayoutStyle, this.triangleStyle]}/> : null}
            </View>
        );
    }
}

const DownTriangle = (prop) => {
    return (
        <View style={prop.triangleStyle}/>
    );
};

const TopTriangle = props => {
    return (
        <View style={props.triangleStyle}/>
    );
};

const TooltipStyle = StyleSheet.create({
    rootViewStyle: {
        flexDirection: 'row',
        backgroundColor: colors.charcoalGrey,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
        paddingLeft: 12,
        paddingRight: 12,
        paddingTop: 16,
        paddingBottom: 16,
        elevation: 5,
        flex: 1,
    },

    imageContainerStyle: {
        height: 24,
        width: 24,
        backgroundColor: colors.transparent,
        marginLeft: 16,
        alignItems: 'center',
        justifyContent: 'center',
    },

    imageStyle: {
        height: 24,
        width: 24,
        justifyContent: 'center',
    },

    downTriangle: {
        width: 0,
        height: 0,
        backgroundColor: colors.transparent,
        borderStyle: 'solid',
        borderLeftWidth: 12,
        borderRightWidth: 12,
        borderTopWidth: TRIANGLE_WIDTH,
        borderLeftColor: colors.transparent,
        borderRightColor: colors.transparent,
        borderTopColor: colors.charcoalGrey,
        alignSelf: 'flex-end',
        marginTop: -8,
        marginRight: 20,
    },
    topTriangle: {
        width: 0,
        height: 0,
        backgroundColor: colors.transparent,
        borderStyle: 'solid',
        borderLeftWidth: 12,
        borderRightWidth: 12,
        borderBottomWidth: TRIANGLE_WIDTH,
        borderLeftColor: colors.transparent,
        borderRightColor: colors.transparent,
        borderBottomColor: colors.charcoalGrey,
        alignSelf: 'flex-end',
        marginBottom: -8,
        marginRight: 20,
    },
});


