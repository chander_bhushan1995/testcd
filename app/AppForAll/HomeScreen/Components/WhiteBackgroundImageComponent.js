import {Image, StyleSheet, Text, View, TouchableOpacity, Platform} from 'react-native';
import React from "react";
import { TextStyle } from "../../../Constants/CommonStyle";
import colors from "../../../Constants/colors";
import dimens from '../../../Constants/Dimens';
import spacing from '../../../Constants/Spacing';
import { MHCTestCategories } from '../../Constant/ScreensStaticData/MHCData'
import { handleHomeScreenActions, ButtonsAction } from '../Helpers/ButtonActions'
import { MHC_QUICK_TEST } from "../../../Constants/WebengageEvents";
import { AppForAppFlowStrings } from '../../Constant/AppForAllConstants';
import {PLATFORM_OS} from '../../../Constants/AppConstants';
import {logWebEnageEvent} from '../../../commonUtil/WebengageTrackingUtils';

const WhiteBackgroundImageComponent = (props) => {
    const { data } = props;

    const handleOnPress = () => {
        let dataForAction = null
        if (data.buttonAction == ButtonsAction.START_SECTIONAL_MHC){
            const selectedCatObj = MHCTestCategories.filter((obj, index, array) => {
                return obj.name == data?.mhcCategory
            })[0]
            let eventData = new Object();
            eventData[AppForAppFlowStrings.location] = "Home"
            eventData[AppForAppFlowStrings.group] = selectedCatObj.name
            logWebEnageEvent(MHC_QUICK_TEST,eventData)
            dataForAction = selectedCatObj
        }else if (data.buttonAction == ButtonsAction.START_SOD_FLOW){
            dataForAction = data.data ?? ""
        }
        handleHomeScreenActions(data.buttonAction, dataForAction, {location: AppForAppFlowStrings.homeBigCard}, null)
    }

    return (
        <TouchableOpacity onPress={props.handleOnPress || handleOnPress}>
            <View style={{ flexDirection: "row" }}>
                <View style={ConnectivityComponentStyle.rootViewStyle}>
                    <View style={ConnectivityComponentStyle.imageContainerStyle}>
                        <Image source={data.image} style={ConnectivityComponentStyle.imageStyle}/>
                    </View>
                    <Text style={ConnectivityComponentStyle.title} numberOfLines={2} adjustsFontSizeToFit={true}>{data.title}</Text>
                </View>
            </View>
        </TouchableOpacity>
    );
};
const ConnectivityComponentStyle = StyleSheet.create({
    rootViewStyle: {
        width: dimens.dimen92,
        backgroundColor: colors.transparent,
        alignItems: 'center',
        justifyContent: "center"
    },

    imageContainerStyle: {
        height: dimens.dimen88,
        width: dimens.dimen88,
        backgroundColor: colors.white,
        borderRadius: 6,
        marginBottom: spacing.spacing12,
        justifyContent: "center",
        alignItems: 'center'
    },
    imageStyle: {
        height: Platform.OS == PLATFORM_OS.android ? dimens.dimen88 : dimens.dimen56,
        width: Platform.OS == PLATFORM_OS.android ? dimens.dimen88 : dimens.dimen56,
        resizeMode: 'center'
    },
    title: {
        ...TextStyle.text_14_normal,
        color: colors.white,
        textAlign: 'center'
    }
});

export default WhiteBackgroundImageComponent;


