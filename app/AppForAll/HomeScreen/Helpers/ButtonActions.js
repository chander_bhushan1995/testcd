import {NativeModules} from "react-native";
import {AppForAppFlowStrings} from '../../Constant/AppForAllConstants';
import {BuyBackStatus} from '../Reducers/HomeScreenReducer'
import {PENDINGACTION} from '../Screens/HomeScreen'
import {MHC_CHECK_NOW} from "../../../Constants/WebengageEvents";
import {MHC} from "../../../Constants/WebengageAttributes";
import {logAppsFlyerEvent, logWebEnageEvent} from '../../../commonUtil/WebengageTrackingUtils';
import {addEvent, Events} from '../../../CardManagement/Analytics';
import { APIData } from "../../../../index";

const nativeBridgeRef = NativeModules.ChatBridge;
export const ButtonsAction = {
    START_BUYBACK: "START_BUYBACK",
    START_COMPLETE_MHC: "START_COMPLETE_MHC",
    START_SECTIONAL_MHC: "START_SECTIONAL_MHC",
    PAY_BILL: "PAY_BILL",
    CARD_OFFERS: "CARD_OFFERS",
    SHOW_ATM: "SHOW_ATM",
    SHOW_BANK: "SHOW_BANK",
    SEE_ALL_OFFERS: "SEE_ALL_OFFERS",       //clicked see all button of offers container
    SEE_OFFER: "SEE_OFFER",                  //clicked on single offer card
    MAINTENANCE_PLANS: "MAINTENANCE_PLANS",
    EXTENDED_WARRANTY: "EXTENDED_WARRANTY",
    HOME_ASSIST: "HOME_ASSIST",
    SPOTLIGHT_ACTION: "SPOTLIGHT_ACTION",
    VIEW_OFFLINE_OFFERS: "VIEW_OFFLINE_OFFERS",  //for deeplink handling on homescreen.js pending actions method
    VIEW_ONLINE_OFFERS: "VIEW_ONLINE_OFFERS",   //for deeplink handling on homescreen.js pending actions method
    ACTIVATE_VOUCHER: "ACTIVATE_VOUCHER",

    START_SOD_FLOW: "START_SOD_FLOW",           // for sod
    VIEW_SERVICE_DETAIL: "VIEW_SERVICE_DETAIL", // view detail of a runing service
    RATE_COMPLETED_SERVICE: "RATE_COMPLETED_SERVICE", // request for rating
    RESCHEDULE_NOW: "RESCHEDULE_NOW",              // reschedule service visit after payment successfull
    ON_HOLD_RESCHEDULE: "ON_HOLD_RESCHEDULE",   // service was on hold now customer need to reschedule
    PAYMENT_INCOMPLETE_DETAIL: "PAYMENT_INCOMPLETE_DETAIL", // show to the incomplete detail of payment

}

export const handleHomeScreenActions = (buttonActions, data, eventParams=null, schedulePickupSheet) => {
    const isLoggedIn = Number(APIData?.customer_id) ? true : false
    PENDINGACTION.buttonAction = null
    let stringifiedData = null
    switch (buttonActions) {
        case ButtonsAction.ACTIVATE_VOUCHER:
            if (isLoggedIn) {
                nativeBridgeRef.goToActivateVoucher(AppForAppFlowStrings.home)
            } else {
                nativeBridgeRef.showPopupForVerifyNumber(AppForAppFlowStrings.activateVoucherLoginAlert.title, "", AppForAppFlowStrings.activateVoucher, status => {
                    if (status) {
                        nativeBridgeRef.goToActivateVoucher(AppForAppFlowStrings.home)
                    }
                })
            }
            break
        case ButtonsAction.SPOTLIGHT_ACTION:
            if (data?.isOpenWebView) {
                nativeBridgeRef.openWebView("", data?.webViewTitle, data?.url)
            } else {
                nativeBridgeRef.handleSpotLight(data?.url)
            }
            break
        case ButtonsAction.START_BUYBACK:
            if (isLoggedIn) {
                if (data.currentBuybackStatus == BuyBackStatus.MODEL_NOT_FOUND) {
                    nativeBridgeRef.shareLink("")
                } else {
                    if(data.buybackData?.buybackStatus?.length === 1 && data.buybackData?.buybackStatus[0]?.recentExchangeCancelled){
                        schedulePickupSheet.open();
                    }else{
                        nativeBridgeRef.startBuyBack(data.buybackData,null)
                    }
                }
            } else {
                PENDINGACTION.buttonAction = ButtonsAction.START_BUYBACK
                nativeBridgeRef.showPopupForVerifyNumber(AppForAppFlowStrings.buybackLoginAlert.title, "", AppForAppFlowStrings.buyback, status => {
                })
            }
            break;
        case ButtonsAction.START_COMPLETE_MHC:
            nativeBridgeRef.goToMHCMainNavigator(buttonActions, data);
            let eventData = new Object();
            eventData["Location"] = MHC;
            logWebEnageEvent(MHC_CHECK_NOW, eventData);
            break;
        case ButtonsAction.START_SECTIONAL_MHC:
            nativeBridgeRef.goToMHCMainNavigator(buttonActions, data)
            break;
        case ButtonsAction.PAY_BILL:
            if (isLoggedIn) {
                nativeBridgeRef.routeToCardManagement()
            } else {
                nativeBridgeRef.showPopupForVerifyNumber(AppForAppFlowStrings.CreditCardBillLoginAlert.title, "", AppForAppFlowStrings.payBill, status => {
                    if (status) {
                        setTimeout(() => {
                            nativeBridgeRef.routeToCardManagement()
                        }, 1000)
                    }
                })
            }
            break;
        case ButtonsAction.SHOW_ATM:
        case ButtonsAction.SHOW_BANK:
            if (isLoggedIn) {
                nativeBridgeRef.goToATMsBankScreen(buttonActions)
            } else {
                nativeBridgeRef.showPopupForVerifyNumber(AppForAppFlowStrings.newByBankATMAlert.title, "", buttonActions, status => {
                    if (status) {
                        setTimeout(() => {
                            nativeBridgeRef.goToATMsBankScreen(buttonActions)
                        }, 1000)
                    }
                })
            }
            break;
        case ButtonsAction.START_SOD_FLOW:
            nativeBridgeRef.handleSODFlow(data,eventParams)
            break;
        case ButtonsAction.CARD_OFFERS:
            nativeBridgeRef.viewAllOffers(JSON.stringify(data?.data ?? {}), "", AppForAppFlowStrings.explore, 0)
            break;
        case ButtonsAction.SEE_ALL_OFFERS:
            nativeBridgeRef.viewAllOffers(JSON.stringify(data?.data ?? {}), "", AppForAppFlowStrings.explore, 0)
            break;
        case ButtonsAction.SEE_OFFER:
            logWebEnageEvent(Events.offerDetails, {Location: 'Home Screen'});
            logAppsFlyerEvent(Events.offerDetails,{})
            nativeBridgeRef.viewOnlineOfferDetail(JSON.stringify(data))
            break;
        case ButtonsAction.MAINTENANCE_PLANS:
            break;
        case ButtonsAction.EXTENDED_WARRANTY:
            nativeBridgeRef.startEWFlow()
            break;
        case ButtonsAction.HOME_ASSIST:
            nativeBridgeRef.startHAFlow()
            break;
        case ButtonsAction.VIEW_SERVICE_DETAIL:
            stringifiedData = JSON.stringify(data)
            nativeBridgeRef.showSRDetails(stringifiedData)
            break;
        case ButtonsAction.RATE_COMPLETED_SERVICE:
            stringifiedData = JSON.stringify(data)
            nativeBridgeRef.rateCompletedService(stringifiedData)
            break;
        case ButtonsAction.RESCHEDULE_NOW:
            stringifiedData = JSON.stringify(data)
            nativeBridgeRef.rescheduleServiceRequest(stringifiedData)
            break;
        case ButtonsAction.ON_HOLD_RESCHEDULE:
            stringifiedData = JSON.stringify(data)
            nativeBridgeRef.rescheduleOnHoldRequest(stringifiedData)
            break;
        case ButtonsAction.PAYMENT_INCOMPLETE_DETAIL:
            stringifiedData = JSON.stringify(data)
            nativeBridgeRef.showPaymentPending(stringifiedData)
            break;
        default:
            break;
    }
}
