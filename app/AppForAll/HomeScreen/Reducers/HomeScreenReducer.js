import * as Actions from './ActionsTypes'
import HomeScreenData, {
    removeObjHomeScreenDataArray,
} from '../../Constant/ScreensStaticData/HomeScreenData';
import {formatAMPM, formatDateDD_MM_YYYY, formateDateWD_DDMMM, numberWithComma} from '../../../commonUtil/Formatter';
import colors from '../../../Constants/colors'
import {ButtonsAction} from '../Helpers/ButtonActions';
import {Platform} from 'react-native';
import {isBankCard, cardTypeId, networkId} from '../../../CardManagement/CardUtils'
import {updateObjects, Tags} from '../Components/ComponentsFactory'
import { sodAppliancesDataMapping } from '../Screens/HomeScreen';
import {APIData} from '../../../../index';
import {
    AppForAppFlowStrings,
    RaisedServiceRequestsStatus,
    SODServiceRequestTypes,
    PaymentStatus,
    PossibleOnHoldStatuses,
} from '../../Constant/AppForAllConstants';
import {arrayUnion} from '../../../commonUtil/CommonMethods.utility'
import {PLATFORM_OS} from "../../../Constants/AppConstants";
import {getRandomMHCCategory} from '../../Constant/ScreensStaticData/MHCData';
import images from '../../../images/index.image';
import {buybackFlowType} from '../../../Constants/BuybackConstants';
import {daysDifference, getCurretDateMilliseconds} from "../../../commonUtil/DateUtils";
import { parseJSON, deepCopy} from "../../../commonUtil/AppUtils";

//INITIAL STATE
export const initialState = {
    data: [],
    isLoadingHomeScreenData: true,
    pendingServicesCards: [], // payment is pending or slots are aquired by other then reschedule
    cardsResponse: null,
    buybackResponse: null,
    anyBuybackForSchedulePickup: null,
};

let anyBuybackForSchedulePickup = null

export const BuyBackStatus = {
    BUYBACK_NOT_STARTED: "BUYBACK_NOT_STARTED",
    SERVICEABILITY_CHECKED: "SERVICEABILITY_CHECKED",
    QUOTE_CREATED: "QUOTE_CREATED",
    MHC_COMPLETED: "MHC_COMPLETED",
    QUESTIONS_IN_PROGRESS: "QUESTIONS_IN_PROGRESS",
    QUOTE_GENERATED: "QUOTE_GENERATED",
    ORDER_PLACED: "ORDER_PLACED",
    VOUCHER_GENERATED: 'VOUCHER_GENERATED',
    MODEL_NOT_FOUND: "MODEL_NOT_FOUND",
}

const getURLParams = url => {
    let regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while (match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params
}

const prepareSpotLightData = (cardArray, spotLightData, totalVisibleCards) => {
    const finalCards = []
    let displayableCards = spotLightData.filter((card) => {
        return card.display
    })
    if (APIData?.isRenew ?? false) {
        let filteredData = displayableCards.filter((card) => {
            return card.cardType == AppForAppFlowStrings.renewal
        })
        if (filteredData?.length > 0)
            finalCards.push(filteredData[0]) //add renewal card if available
    }
    if (APIData?.isLead ?? false) {
        let filteredData = displayableCards.filter((card) => {
            return card.cardType == AppForAppFlowStrings.completePurchase
        })

        if (filteredData?.length > 0)
            finalCards.push(filteredData[0]) //add complete_purchase card if available
    }
    displayableCards = displayableCards.filter((card) => {
        return !([AppForAppFlowStrings.renewal, AppForAppFlowStrings.completePurchase].includes(card.cardType))
    }) //remove renewal and complete purchase data

    displayableCards.sort((currentCard, nextCard) => {    // sort on priority base
        return Number(currentCard.priority) - Number(nextCard.priority)
    })
    let index = finalCards.length - 1
    index = index >= 0 ? index : 0
    while ((finalCards.length < totalVisibleCards) && displayableCards[index]) {
        let actionURL = displayableCards[index].button.action
        if (actionURL) {
            let availableHAServices = APIData?.availableHAServices ?? []
            let availablePEProducts = APIData?.availablePEProducts ?? {}
            let availableFProducts = APIData?.availableFProducts ?? []
            let params = getURLParams(actionURL)
            if (params.category == "HA") {
                if (!((availableHAServices.length > 0) && params.service && availableHAServices.includes(params.service))) {
                    finalCards.push(displayableCards[index])
                }
            } else if (params.category == "PE") { // add PE related spotlight cards if there is no any membership for specified product in spotlight card
                if (availablePEProducts && params.product && availablePEProducts.hasOwnProperty(params.product)) {
                    if (!(availablePEProducts[params.product] ?? []).includes(params.service ?? "ADLD")) {
                        finalCards.push(displayableCards[index])
                    }
                } else {
                    finalCards.push(displayableCards[index])
                }
            } else if (params.category == "F") { // add F related spotlight cards if there is no any membership for specified product in spotlight card
                if (!((availableFProducts.length > 0) && params.product && availableFProducts.includes(params.product))) {
                    finalCards.push(displayableCards[index])
                }
            } else {
                finalCards.push(displayableCards[index])
            }
        } else {
            finalCards.push(displayableCards[index])
        }
        index++
    }
    displayableCards.sort((currentCard, nextCard) => { // sort on priority base with renewal and complete_purchase
        return Number(currentCard.priority) - Number(nextCard.priority)
    })

    return finalCards.length > 0 ? updateObjects(cardArray, "compTag", Tags.SpotLightCarouselV2, {"components": finalCards}) : removeObjHomeScreenDataArray(cardArray, 'compTag', Tags.SpotLightSectionalViewComponent)
}

const prepareOfferData = (cardArray, offerArray) => {
    const offerComponents = []
    //remove offer section if there are no offer
    cardArray.forEach((componentObject, index) => {
        if (offerArray.length <= 0 && componentObject.compTag == Tags.OffersTitleDescriptionListContainer) {
            cardArray.splice(index, 1)
            return
        }
    })


    offerArray.forEach(offer => {
        let supportedBanks = offer.cardDetailsList[0]?.tagging ?? null
        let moreText = ((offer.cardDetailsList?.length - 1) > 0) ? AppForAppFlowStrings.moreText((offer.cardDetailsList?.length ?? 0) - 1) : null
        offerComponents.push({
            "type": "OfferCardComponent",
            "title": offer?.merchantName,
            "description": offer?.offerTitle,
            "image": offer?.image,
            "buttonAction": ButtonsAction.SEE_OFFER,
            "offerData": offer,
            "tagging": supportedBanks,
            "moreText": moreText,
            "isShowSkeleton": false,
            "compTag": Tags.OffersOfferCardComponent
        })
    });
    return updateObjects(cardArray, "compTag", Tags.OffersTitleDescriptionListContainer, {"components": offerComponents})
}

const prepareBuyBackData = (buyBackData, deviceUUID) => {
    const buyBackStatusArray = buyBackData?.buybackStatus ?? []
    let activeSR = null

    let title = null
    let description = null
    let note = null
    let subDescription = null
    let image = null
    if (buyBackStatusArray.length == 1) {
        activeSR = buyBackStatusArray[0]
        image = activeSR.deviceInfo?.image
        let brandModel = Platform.OS === 'ios' ? activeSR.deviceInfo?.modelName : activeSR.deviceInfo?.deviceName
        anyBuybackForSchedulePickup = (activeSR?.recentExchangeCancelled ?? false) ? activeSR : null
        switch (activeSR.buyBackStatus) {
            case BuyBackStatus.BUYBACK_NOT_STARTED:
                title = AppForAppFlowStrings.getUpTo(numberWithComma(activeSR.price))
                description = AppForAppFlowStrings.bestsellingPriceFor(brandModel)
                break;
            case BuyBackStatus.SERVICEABILITY_CHECKED:
            case BuyBackStatus.QUOTE_CREATED:
                description = AppForAppFlowStrings.resaleOffer(brandModel)
                note = AppForAppFlowStrings.finishHealthTest(numberWithComma(activeSR.price))
                break;
            case BuyBackStatus.MHC_COMPLETED:
            case BuyBackStatus.QUESTIONS_IN_PROGRESS:
                description = AppForAppFlowStrings.lastStepPending(brandModel)
                note = AppForAppFlowStrings.fewQuestions(numberWithComma(activeSR.price))
                break;
            case BuyBackStatus.QUOTE_GENERATED:
                if (activeSR?.subType === buybackFlowType.BOTH) {
                    description = AppForAppFlowStrings.resaleOffer(brandModel);
                    subDescription = AppForAppFlowStrings.seeYourDevice();
                } else {
                    description = AppForAppFlowStrings.finalOffer(numberWithComma(activeSR.price));
                    note = AppForAppFlowStrings.schedulePickup(daysDifference(getCurretDateMilliseconds(), activeSR?.expireOn ?? getCurretDateMilliseconds()));
                }
                break;
            case BuyBackStatus.ORDER_PLACED:
            case BuyBackStatus.VOUCHER_GENERATED:
                if (activeSR?.subType === buybackFlowType.RETAILER_PICKUP) {
                    description = AppForAppFlowStrings.exchangeRequest(brandModel)
                    subDescription = AppForAppFlowStrings.finalExchangePrice(numberWithComma(activeSR.price), activeSR?.flow)
                }else{
                    description = AppForAppFlowStrings.resaleRequest(brandModel)
                    note = AppForAppFlowStrings.finalSellingPrice(numberWithComma(activeSR.price))
                }
                break;
            case BuyBackStatus.MODEL_NOT_FOUND:
                description = AppForAppFlowStrings.saleOldMobile
                subDescription = AppForAppFlowStrings.brandNotSupported
                break;
            default:
                break;
        }
    } else if (buyBackStatusArray.length > 1) {
        //filter all buybackStatus object where described below condition meets
        const activeSRs = buyBackStatusArray.filter((statusObject) => {
            return !([BuyBackStatus.BUYBACK_NOT_STARTED, BuyBackStatus.MODEL_NOT_FOUND].includes(statusObject.buybackStatus))
        })
        // if recently exchange is cancelled by retailer then to show bottom sheet on home [Buyback Phase 2]
        anyBuybackForSchedulePickup = activeSRs.filter((statusObject) => {
            return statusObject.recentExchangeCancelled
        })[0]
        //filter single buybackstatus object by matching current device UUID
        activeSR = activeSRs.filter((statusObject) => {
            return statusObject.deviceInfo?.deviceIdentifier == deviceUUID
        })[0]
        //check if there's no any buybackstatus for current device then show first buybackstatus
        activeSR = !activeSR ? activeSRs[0] : activeSR
        let brandModel = Platform.OS === 'ios' ? activeSR.deviceInfo?.modelName : activeSR.deviceInfo?.brand + " " + activeSR.deviceInfo?.modelName
        description = AppForAppFlowStrings.activeRequests(activeSRs.length)
        note = AppForAppFlowStrings.resalePriceIs(brandModel, numberWithComma(activeSR.price))
        image = activeSR.deviceInfo?.image
    }
    return {
        "type": "LeftAlignedComponent",
        "title": title,
        "description": description,
        "note": note,
        "subDescription": subDescription,
        "isBadgeEnabled": false,
        "isShowSkeleton": false,
        "imageBackground": colors.blue028,
        "image": {uri: image},
        "buttonAction": ButtonsAction.START_BUYBACK,
        "data": {buybackData: buyBackData, currentBuybackStatus: activeSR.buyBackStatus},
    }
}


const prepareStateForBuyBack = (cardArray, buyBackData, deviceUUID) => {
    const buybackDataObj = prepareBuyBackData(buyBackData, deviceUUID)
    return updateObjects(cardArray, "compTag", Tags.BuyBackLeftAlignedComponent, buybackDataObj)
}

export const prepareCardsData = (cardsResponse) => {

    let cardsData = []
    let answeredData = []
    let onBoardingQA = APIData?.onBoardingQA ?? []
    //retrieve all bank names selected in onBoardingQA for ASSETS_F
    if (onBoardingQA.length > 0) {
        let questionForFinance = onBoardingQA.filter((question) => question.questionCode == AppForAppFlowStrings.onBoardingQuestionsCode.financeAssets)[0]
        if (questionForFinance.answered) {
            let selectedOptions = questionForFinance.optionList?.filter((option) => {
                return option.selected
            })
            selectedOptions.forEach((option) => { // create debit, credit card objects for each selected bank name
                answeredData.push({
                    cardIssuerCode: option.optionCode,
                    cardTypeId: 1,
                    networkId: 0
                })
                answeredData.push({
                    cardIssuerCode: option.optionCode,
                    cardTypeId: 2,
                    networkId: 0
                })
            })
        }
    }

    //below code works for cards Response / those cards which were added by user as credit or debit card
    let filteredCards = (cardsResponse.data?.cards ?? []).filter(card => isBankCard(card.contentType.toUpperCase()))
    filteredCards.forEach((card) => {
        return cardsData.push({
            cardIssuerCode: card.issuerCode ?? "",
            cardTypeId: cardTypeId(card.contentType ?? ""),
            networkId: networkId(card.contentNo ?? "")
        })
    })
    // retrieving union of answered banks name prepared cards and added cards by user
    cardsData = arrayUnion(cardsData, answeredData, (obj1, obj2) => {
        return obj1.cardIssuerCode == obj2.cardIssuerCode && obj1.cardTypeId == obj2.cardTypeId;
    })
    return cardsData
}


const prepareWhiteBgImgCompData = (mhcCategoryData) => {
    let objectArr = []
    mhcCategoryData.viewModel.forEach(viewModel => {
        const compDataObject = {
            "type": "WhiteBackgroundImageComponent",
            "image": viewModel.icon,
            "title": viewModel.name,
            "mhcCategory": mhcCategoryData.name,
            "buttonAction": ButtonsAction.START_SECTIONAL_MHC
        }
        objectArr.push(compDataObject)
    })

    return objectArr
}

const prepareMHCData = (homeScreenData) => {
    let mhcCategoryData = getRandomMHCCategory()
    let testCards = prepareWhiteBgImgCompData(mhcCategoryData)
    let mhcData = {
        "title": mhcCategoryData.title,
        "description": mhcCategoryData.description,
        "bgColor": colors.blue028,
        "bgImage": images.ic_MHCLarge,
        "components": testCards
    }
    return updateObjects(homeScreenData, "compTag", Tags.MHCTitleDescriptionListContainer, mhcData)
}

const getSODProductPostName = (serviceRequestType) => {
    if (serviceRequestType == SODServiceRequestTypes.breakdown) {
        return " Repair"
    } else if (serviceRequestType == SODServiceRequestTypes.pms) {
        return " Service"
    } else {
        return ""
    }
}

const prepareStateForServiceRequests = (homeScreenData, serviceRequestsResponse, pendingServicesCards) => {
    let finalCardsData = (pendingServicesCards ?? []).length > 0 ? pendingServicesCards : []
    let sodAppliancesSubcategories = (sodAppliancesDataMapping?.subCategories ?? []).concat(sodAppliancesDataMapping?.otherSubCategories?.subCategories ?? [])

    //filtered all service which are pending or inprogress
    let pendingOrInprogressServices = serviceRequestsResponse?.data?.filter((service) => {
        return (service.status == RaisedServiceRequestsStatus.IP || service.status == RaisedServiceRequestsStatus.P)
    }) ?? []
    pendingOrInprogressServices.forEach((service, index) => {
        let assets = service?.assets ?? []
        let productInfo = sodAppliancesSubcategories.filter((subCategory) => {
            return assets.length > 0 ? subCategory.subCategoryCode == (service?.assets[0]?.productCode ?? "") : null
        })[0] ?? {}
        let productName = productInfo.subCategoryName
        let productImage = productInfo.subCatImageUrl ?? "https://abc.in/1.jpg" // dummy url added in case image not found
        productName += getSODProductPostName(service.serviceRequestType)
        let scheduledDate = formateDateWD_DDMMM(service.scheduleSlotStartDateTime)
        let scheduleStartTime = formatAMPM(service.scheduleSlotStartDateTime)
        let scheduleEndTime = formatAMPM(service.scheduleSlotEndDateTime)
        finalCardsData.push({
            "header": productName,
            "firstNote": AppForAppFlowStrings.scheduledFor,
            "secondNote": AppForAppFlowStrings.time,
            "firstSubNote": scheduledDate,
            "secondSubNote": scheduleStartTime + '-' + scheduleEndTime,
            "image": {"uri": productImage},
            "data": service,
            "button": {"text": AppForAppFlowStrings.viewDetails, 'action': ButtonsAction.VIEW_SERVICE_DETAIL}
        })
    })

    //filtered all service which are on hold
    let onHoldServices = serviceRequestsResponse?.data?.filter((service) => {
        return (service.status == RaisedServiceRequestsStatus.OH)
    }) ?? []
    onHoldServices.forEach((service, index) => {
        let assets = service?.assets ?? []
        let productInfo = sodAppliancesSubcategories.filter((subCategory) => {
            return assets.length > 0 ? subCategory.subCategoryCode == (service?.assets[0]?.productCode ?? "") : null
        })[0] ?? {}
        let productName = productInfo.subCategoryName
        let productImage = productInfo.subCatImageUrl ?? "https://abc.in/1.jpg" // dummy url added in case image not found
        productName += getSODProductPostName(service.serviceRequestType)
        let firstNote = service?.workflowData?.repairAssessment?.description ?? ""
        let tag = null
        let button = {"text": AppForAppFlowStrings.viewDetails, 'action': ButtonsAction.VIEW_SERVICE_DETAIL}
        if ([PossibleOnHoldStatuses.AA, PossibleOnHoldStatuses.SN, PossibleOnHoldStatuses.TCCA, PossibleOnHoldStatuses.TCO, PossibleOnHoldStatuses.TR].includes(service?.workflowStageStatus ?? "")) {
            tag = AppForAppFlowStrings.onHold
        }
        if ([PossibleOnHoldStatuses.SPA].includes(service?.workflowStageStatus ?? "")) {
            button = {"text": AppForAppFlowStrings.rescheduleNow, 'action': ButtonsAction.ON_HOLD_RESCHEDULE}
        }
        finalCardsData.push({
            "header": productName,
            "tag": tag,
            "firstNote": firstNote,
            "secondNote": null,
            "firstSubNote": null,
            "secondSubNote": null,
            "image": {"uri": productImage},
            "data": service,
            "button": button
        })
    })


    //filtered all service which are completed but feedback is pending
    let completedServices = serviceRequestsResponse?.data?.filter((service) => {
        return service.status == RaisedServiceRequestsStatus.CO && service.serviceRequestFeedback == null
    }) ?? []
    completedServices.forEach((service, index) => {
        let assets = service?.assets ?? []
        let productInfo = sodAppliancesSubcategories.filter((subCategory) => {
            return assets.length > 0 ? subCategory.subCategoryCode == (service?.assets[0]?.productCode ?? "") : null
        })[0] ?? {}
        let productName = productInfo.subCategoryName ?? ""
        // let productImage = productInfo.subCatImageUrl ?? ""
        productName += getSODProductPostName(service.serviceRequestType)
        let endDate = formateDateWD_DDMMM(service.actualEndDateTime)
        finalCardsData.push({
            "header": productName,
            "firstNote": AppForAppFlowStrings.completedOn,
            "secondNote": null,
            "firstSubNote": endDate,
            "secondSubNote": null,
            "image": images.rate_service,
            "data": service,
            "button": {"text": AppForAppFlowStrings.rateOurService, 'action': ButtonsAction.RATE_COMPLETED_SERVICE}
        })
    })
    return finalCardsData.length > 0 ? updateObjects(homeScreenData, "compTag", Tags.ScheduledUIComponentContainer, {"cardsData": finalCardsData}) : homeScreenData
}

const prepareStateForSODPendingServices = (homeScreenData, pendingServices) => {

    let ordersInfo = pendingServices?.data?.orderInfo ?? []
    let sodAppliancesSubcategories = (sodAppliancesDataMapping?.subCategories ?? []).concat(sodAppliancesDataMapping?.otherSubCategories?.subCategories ?? [])
    let finalCardsData = []
    if (ordersInfo.length <= 0)
        return {homeData: homeScreenData, pendingServicesCards: finalCardsData}

    ordersInfo.forEach((orderInfo, index) => {
        let assets = orderInfo?.thirdPartyParams?.assets ?? []
        let productInfo = sodAppliancesSubcategories.filter((subCategory) => {
            return assets.length > 0 ? subCategory.subCategoryCode == (assets[0].productCode ?? "") : null
        })[0] ?? {}
        let productName = productInfo.subCategoryName ?? ""
        productName += getSODProductPostName(orderInfo?.thirdPartyParams?.serviceRequestType ?? "")
        let heading = orderInfo.paymentStatus == PaymentStatus.success ? productName + AppForAppFlowStrings.requestPaymentDone : productName + AppForAppFlowStrings.requestPlaced
        let firstNote = orderInfo.paymentStatus == PaymentStatus.success ? AppForAppFlowStrings.slotsExhausted : AppForAppFlowStrings.paymentUnderProcess
        let button = orderInfo.paymentStatus == PaymentStatus.success ? {
            "text": AppForAppFlowStrings.rescheduleNow,
            'action': ButtonsAction.RESCHEDULE_NOW
        } : {"text": AppForAppFlowStrings.viewDetails, 'action': ButtonsAction.PAYMENT_INCOMPLETE_DETAIL}
        let productImage = productInfo.subCatImageUrl ?? ""
        finalCardsData.push({
            "header": heading,
            "firstNote": firstNote,
            "secondNote": null,
            "firstSubNote": null,
            "secondSubNote": null,
            "image": {"uri": productImage},
            "data": orderInfo,
            "button": button
        })
    })

    if (finalCardsData.length > 0) {
        return {
            homeData: updateObjects(homeScreenData, "compTag", Tags.ScheduledUIComponentContainer, {"cardsData": finalCardsData}),
            pendingServicesCards: finalCardsData
        }
    } else {
        return {homeData: homeScreenData, pendingServicesCards: finalCardsData}
    }
}

export const HomeScreenReducer = (state = initialState, action) => {
    let homeScreenData = deepCopy(state.data);
    switch (action.type) {
        case Actions.REQUEST_HOME_SCREEN_DATA:
            homeScreenData = deepCopy({"data": action.data}).data
            homeScreenData = prepareMHCData(homeScreenData)
            return {...initialState, data: homeScreenData, isLoadingHomeScreenData: false}

        case Actions.RESET_HOME_SCREEN_DATA:
            homeScreenData = deepCopy({"data": action.data}).data
            homeScreenData = prepareMHCData(homeScreenData)
            let brandModel = Platform.OS === PLATFORM_OS.android ? APIData?.brand : APIData?.modelName
            // if (!isLoggedIn) {
            //     homeScreenData = removeObjHomeScreenDataArray(homeScreenData, 'compTag', Tags.SpotLightSectionalViewComponent)
            // }
            if (!(APIData?.isNonSubscriber)) {
                homeScreenData = removeObjHomeScreenDataArray(homeScreenData, 'compTag', Tags.ActivateVoucherCard)
            }
            return {
                ...state,
                cardsResponse: null,
                buybackResponse: null,
                data: deepCopy({"data": updateObjects([...homeScreenData], "compTag", Tags.MobilePerformanceLeftAlignedComponent, {"description": "Check " + brandModel + " mobile health within minutes."})}).data
            }
        case Actions.REQUEST_SPOTLIGHT_DATA:
            homeScreenData = prepareSpotLightData(homeScreenData, action.spotLightData, action.totalVisibleCards)
            return {...state, data: homeScreenData};
        case Actions.REQUEST_OFFER_DATA:
            return {...state, data: prepareOfferData(homeScreenData, action.offerData.data ?? [])};
        case Actions.REQUEST_BUYBACK_DATA:
            let homeScreenPreparedCards = prepareStateForBuyBack(homeScreenData, action.buybackData, action.deviceUUID)
            let buybackPickupData = anyBuybackForSchedulePickup
            anyBuybackForSchedulePickup = null
            return { ...state, buybackResponse: action.buybackData,anyBuybackForSchedulePickup: buybackPickupData,data: homeScreenPreparedCards}
        case Actions.REQUEST_CARDS_DATA:
            return {...state, cardsResponse: action.cardsResponse, cardsData: action.cardsData}
        case Actions.REQUEST_SERVICE_REQUESTS_DATA:
            return {
                ...state,
                data: prepareStateForServiceRequests(homeScreenData, action.serviceRequests, state.pendingServicesCards)
            }
        case Actions.REQUEST_PENDING_SOD_SERVICES:
            let processedData = prepareStateForSODPendingServices(homeScreenData, action.pendingRequests)
            let homeData = processedData.homeData
            let pendingServicesCards = processedData.pendingServicesCards
            return {...state, pendingServicesCards: pendingServicesCards, data: homeData}
        case Actions.REQUEST_BUYBACK_DATA_ERROR:
            return {
                ...state,
                data: removeObjHomeScreenDataArray(homeScreenData, 'compTag', Tags.BuyBackLeftAlignedComponent)
            };
        case Actions.REQUEST_OFFER_DATA_ERROR:
            return {
                ...state,
                data: removeObjHomeScreenDataArray(homeScreenData, 'compTag', Tags.OffersTitleDescriptionListContainer)
            };
        default:
            return {...state};
    }

};
export default HomeScreenReducer;
