import React, {useReducer, useEffect, useState, useContext, useRef, useImperativeHandle} from 'react';
import {
    Platform,
    View,
    ScrollView,
    Text,
    NativeModules,
    TouchableOpacity,
    NativeEventEmitter,
    SafeAreaView,
    RefreshControl,
    StyleSheet,
    Image,
} from 'react-native';
import {PLATFORM_OS} from '../../../Constants/AppConstants';
import colors from '../../../Constants/colors';
import {HomeScreenReducer} from '../Reducers/HomeScreenReducer';
import {initialState} from '../Reducers/HomeScreenReducer';
import * as APIHelper from '../../../network/APIHelper';
import * as Actions from '../Reducers/ActionsTypes';
import {getComponent} from '../Components/ComponentsFactory';
import spacing from '../../../Constants/Spacing';
import TooltipComponent from '../Components/TooltipComponent';
import {prepareCardsData} from '../Reducers/HomeScreenReducer';
import images from '../../../images/index.image';
import {AppForAppFlowStrings, SelectedActionType} from '../../Constant/AppForAllConstants';
import {ButtonsAction} from '../Helpers/ButtonActions';
import BlockingLoader from '../../../CommonComponents/BlockingLoader';
import useLoader from '../../../CardManagement/CustomHooks/useLoader';
import {Deeplink} from '../../../Constants/Deeplinks';
import {
    parseJSON,
    getrewardOnStatus,
    getSODAppliancesData,
    parseQueryParamsFromUrl
} from '../../../commonUtil/AppUtils';
import Loader from '../../../Components/Loader';
import HomeScreenData from '../../Constant/ScreensStaticData/HomeScreenData';
import RBSheet from '../../../Components/RBSheet';
import SchedulePickupBottomSheet from '../../../Buyback/BuybackComponent/SchedulePickupBottomSheet';
import ScrollableTabView, {ScrollableTabBar} from '../../../scrollableTabView/index';
import {Tags} from '../Components/ComponentsFactory'
import RecommendationComponent from "../../../Catalyst/components/RecommendationComponent";
import TooltipComponentV2 from "../../../Catalyst/components/common/TooltipComponentV2";
import {STR_SEE_NEW_RECOMMENDATIONS} from "../../../Catalyst/data/CatalystConstants";
import {
    CATALYST_CARD_CLICK,
    CATALYST_CARD_VIEW,
    CATALYST_FAVOURITE,
    CATALYST_DISMISS,
    UPDATE_CATALYST_RECO,
    HOME_TAB,
    CATALYST_AUTO_REFRESH,
    NO_CATALYST_RECO,
    RECO_SCREEN,
    SPOTLIGHT_VIEW
} from '../../../Constants/WebengageEvents';
import {logWebEnageEvent, trackAppScreen} from '../../../commonUtil/WebengageTrackingUtils';
import {RECO_SCREENVIEW} from "../../../Constants/WebengageEvents";
import {
    STR_FAVOURITE_STRIP_TITLE,
    STR_RECOMMENDED_PICKS_FOR_TODAY,
    STR_FAVOURITES_AND_DISMISS_HINT
} from "../../../Catalyst/data/CatalystConstants";
import {OAImageSource} from "../../../Constants/OAImageSource";
import {TextStyle} from "../../../Constants/CommonStyle";
import SectionalViewComponentV2 from "../Components/SectionalViewComponentV2";
import InViewPort from '../../../commonUtil/InViewPort';
import {MenuProvider} from 'react-native-popup-menu';
import * as Store from '../../../commonUtil/Store'
import { RootContext, localEventEmitter } from "../../../TabBar/index.tabbar";
import {APIData, updateAPIData} from '../../../../index';
import { TooltipRating } from '../../../Catalyst/components/common/TooltipRating';
import { getServedByValue } from '../../../Buyback/BuyBackUtils/BuybackUtils';
import useForceUpdate from '../../../CardManagement/CustomHooks/useForceUpdate';

const nativeBridgeRef = NativeModules.ChatBridge;
const eventEmitter = new NativeEventEmitter(nativeBridgeRef);
var isRewardOn = false;
let tooltipText = '';
let buybackRetryCount = 0;
let isNewHomeScreen = false;

// Remote config properties.
let showTodayTab = false;
let recommendationForceUpdateInterval = 240; // time in minutes

export const HomeScreenContext = React.createContext();
export let PENDINGACTION = {}
export let resetStateOf = {}   // used to reset state of any component in case of reset home screen like pagination of spotlight
export let sodAppliancesDataMapping = {}
export let isShowFav = false;

const HomeScreen: () => React$Node = props => {

    const { navigation } = props;
    const {
        customer_id,
        apiHeader,
        user_Name,
        isNonSubscriber,
        isIDFenceInMem,
        isAnsweredAllQuestions,
        rewardScratchDate,
        deviceIdentifier,
        home_screen_sub_tab
    } = APIData ?? {}
    const { deeplinkPath, setDeeplink } = useContext(RootContext);
    const [forceUpdate] = useForceUpdate()
    //Local states
    const [state, dispatch] = useReducer(HomeScreenReducer, initialState);
    const [showHomeToolTip, setShowHomeToolTip] = useState(false);
    const [showRatingToolTip, setShowRatingToolTip] = useState(false);
    const [isShownShedulePickupBottomSheet, setShownShedulePickupBottomSheet] = useState(false)
    const [isBlockingLoader, blockingLoaderMessage, startBlockingLoader, stopBlockingLoader] = useLoader();

    const [ratingBuybackData, setRatingBuybackData] = useState({
        quoteId: null, servedBy: null, isAbb: false
    })

    const schedulePickupSheet = useRef(null);

    useEffect(() => {
        nativeBridgeRef.getRecommendationProps(data => {
            let recommendationProps = parseJSON(data);
            recommendationForceUpdateInterval = recommendationProps.recommendation_force_update_interval
            showTodayTab = recommendationProps.show_today_tab
            isNewHomeScreen = recommendationProps.show_catalyst_recommendations && recommendationProps.catalyst_user_group
        })

        getrewardOnStatus((status) => {
            // add login as root
            isRewardOn = status;
            paintHomeScreen()
        });

        eventEmitter.addListener('UpdateHomeScreen', params => {
            updateAPIData({
                apiHeader: {
                    ...apiHeader,
                    token: params.token
                },
                isRenew: params.isRenew,
                isLead: params.isLead,
                user_Name: params.user_Name,
                availableHAServices: params.availableHAServices,
                availablePEProducts: params.availablePEProducts,
                availableFProducts: params.availableFProducts,
                onBoardingQA: parseJSON(params.onBoardingQA ?? '{}').data ?? []
            })

            if ((params.customer_id != customer_id)
                || (params.isAnsweredAllQuestions != isAnsweredAllQuestions)
                || (params.isIDFenceInMem != isIDFenceInMem)
                || (params.isNonSubscriber != isNonSubscriber)) {
                setShowHomeToolTip(false);
                updateAPIData({
                    customer_id: params.customer_id,
                    isAnsweredAllQuestions: params.isAnsweredAllQuestions,
                    isIDFenceInMem: params.isIDFenceInMem,
                    rewardScratchDate: params.rewardScratchDate,
                    isNonSubscriber: params.isNonSubscriber,
                })
            }
            forceUpdate()
            nativeBridgeRef.getRecommendationProps(data => {
                let recommendationProps = parseJSON(data);
                recommendationForceUpdateInterval = recommendationProps.recommendation_force_update_interval
                showTodayTab = recommendationProps.show_today_tab
                isNewHomeScreen = recommendationProps.show_catalyst_recommendations && recommendationProps.catalyst_user_group
            })

            resetHomeScreen(); // it should call after setting of customerID State so that spotlight will be or not could be decided
            hitAllAPIs()
            if (PENDINGACTION.buttonAction) { //it should execute after resetting HomeScreen
                startBlockingLoader(AppForAppFlowStrings.pleaseWait);
            }
        });

        localEventEmitter.addListener("TabChanged", (param) => {
            if (param !== "Home") {
                setShowHomeToolTip(false);
            }
        });

        eventEmitter.addListener('refreshBuyBackStatus', params => {
            getBuyBackData();
        });

        eventEmitter.addListener('refreshCardAndOffers', params => {
            if (params.onBoardingQA) {
                updateAPIData({
                    onBoardingQA: parseJSON(params.onBoardingQA ?? '{}').data ?? []
                })
                forceUpdate()
            }
            getAllCards();
        });

        //if user selected an action out of this screen like chat from native navigation bar or switched tab bar
        eventEmitter.addListener('selectedActionType', params => {
            switch (params.type) {
                case SelectedActionType.CHAT:
                case SelectedActionType.TABSWITHCHED:
                    PENDINGACTION = {};
                    break;
                default:
                    break;
            }
        });

        eventEmitter.addListener('Logout', params => {
            clearAsyncStorageOnLogout()
        });

        return () => {
            localEventEmitter.removeListener("TabChanged", () => { });
            if (Platform.OS === 'ios') {
                eventEmitter.removeListener('UpdateHomeScreen', () => {
                });
                eventEmitter.removeListener('refreshBuyBackStatus', () => {
                });
                eventEmitter.removeListener('refreshCardAndOffers', () => {
                });
                eventEmitter.removeListener('updateInitialProps', () => {
                });
                eventEmitter.removeListener('selectedActionType', () => {
                });
                eventEmitter.removeListener('Logout', () => {
                });

                // eventEmitter.removeAllListeners() // TODO:- uncomment once confirmed that it works
            }
        };

    }, []);

    useEffect(() => { // use effect used to get callback when customer Id and rewardState is updated
        handleTooltip();
    }, [customer_id, isAnsweredAllQuestions, isIDFenceInMem, rewardScratchDate, isNonSubscriber]);

    useEffect(() => { // use effect used to get callback when customer Id and username is updated
        let homeTitle = 'Home'
        if (user_Name?.length) {
            homeTitle = 'Hi ' + user_Name;
            if (Platform.OS === PLATFORM_OS.android) {
                homeTitle = homeTitle + ', welcome back';
            }
        }
        navigation.setParams({
            text: homeTitle,
            showShadow: !isNewHomeScreen
        })
    }, [customer_id, user_Name, isNewHomeScreen]);

    useEffect(() => {
        if (deeplinkPath?.length) { // it calls with each render cycle
            handleDeeplink();
        }
    }, [deeplinkPath])

    useEffect(() => {
        Store.getShowSchedulePickupBottomSheet((error, value) => {
            if (value === null || value !== "true") {
                if ((state?.anyBuybackForSchedulePickup) && !(deeplinkPath?.length) && !PENDINGACTION.pendingDeeplink && !isShownShedulePickupBottomSheet) {
                    setShownShedulePickupBottomSheet(true)
                    schedulePickupSheet.current.open()
                    Store.setShowSchedulePickupBottomSheet("true");
                }
            }
        });
    }, [state?.anyBuybackForSchedulePickup])

    const clearAsyncStorageOnLogout = () => { // remove all catalyst recommendation related data
        Store.setRecommendationsData(null);
        Store.setLastRecommendationSaveTime(null);
        Store.setFavoriteHintCount(null);
        Store.setFavoriteRecommendationData(null);
    }

    const getHomeScreenCacheData = (callback) => { // get cached home screen data from async storage
        Store.getHomeScreenData((error, result) => {
            callback(parseJSON(result))
        })
    }

    const setHomeScreenCacheData = (homeScreenData) => { // set home screen data for async storage
        Store.setHomeScreenData(homeScreenData);
    };

    const resetHomeScreen = () => {
        resetStateOf.isResetSpotlightPagination = true
        getHomeScreenCacheData(homeScreenData => { // data found in home screen
            dispatch({type: Actions.RESET_HOME_SCREEN_DATA, data: homeScreenData ?? HomeScreenData});
            if (!homeScreenData) { // data found in home screen
                updateHomeScreenData(() => {})
            }
        })
    };

    const dispatchHomeData = (data) => {
        dispatch({type: Actions.REQUEST_HOME_SCREEN_DATA, data: data});
        resetHomeScreen();
        hitAllAPIs()
        handleTooltip();
    }

    const updateHomeScreenData = (callback) => {
        APIHelper.getHomeScreenData((response, error) => {
            let data = response?.data
            if (data !== null) {
                setHomeScreenCacheData(JSON.stringify(data)) // store response in async storage
            }
            callback(data)
        })
    }

    const paintHomeScreen = () => {
        getHomeScreenCacheData(homeScreenData => {
            let callback = (data) => dispatchHomeData(data ?? HomeScreenData)
            if (homeScreenData) {
                dispatchHomeData(homeScreenData)
                callback = () => {
                }
            }
            updateHomeScreenData(callback)
        })
    }

    const hitAllAPIs = () => {
        // Here API?.customer_id and APIData?.apiHeader?.token is used instead of customer_id and apiHeader?.token to fix a bug, dont remove.
        let customerId = Number(APIData?.customer_id) ?? 0
        if (customerId > 0 && APIData?.apiHeader?.token) {
            getAllCards();
            getSODAppliancesData((sodAppliancesMapping) => {
                sodAppliancesDataMapping = sodAppliancesMapping;
                getPendingSODServices();
            });
        } else {
            getOffers([]);
        }
        nativeBridgeRef.getRecommendationProps(data => {
            let recommendationProps = parseJSON(data);
            recommendationForceUpdateInterval = recommendationProps.recommendation_force_update_interval
            showTodayTab = recommendationProps.show_today_tab
            isNewHomeScreen = recommendationProps.show_catalyst_recommendations && recommendationProps.catalyst_user_group
            getSpotLightData();
        })
        getBuyBackData();
    };

    const getAllCards = () => {
        APIHelper.getAllCards((response, error) => {
            if (!error) {
                stopBlockingLoader();
                let cardsData = prepareCardsData(response);
                dispatch({type: Actions.REQUEST_CARDS_DATA, cardsResponse: response, cardsData: cardsData});
                getOffers(cardsData);
                if ([ButtonsAction.VIEW_OFFLINE_OFFERS, ButtonsAction.SEE_ALL_OFFERS, ButtonsAction.CARD_OFFERS, ButtonsAction.VIEW_ONLINE_OFFERS].includes(PENDINGACTION.buttonAction)) {
                    setTimeout(() => {
                        performPendingAction(response);
                    }, 100);
                }
            } else {
                getOffers([]); // if cards data api fails
            }
        });
    };

    const getOffers = (cardsData) => {
        APIHelper.getCardOffers(cardsData, (response, error) => {
            if (response !== null) {
                dispatch({type: Actions.REQUEST_OFFER_DATA, offerData: response});
            } else {
                dispatch({type: Actions.REQUEST_OFFER_DATA_ERROR});
            }
        });
    };

    const getPendingSODServices = () => {
        APIHelper.getPendingSODServices((response, error) => {
            if (response != null) {
                dispatch({type: Actions.REQUEST_PENDING_SOD_SERVICES, pendingRequests: response});
            }
            getServiceRequests();
        });
    };

    const getServiceRequests = () => {
        APIHelper.getServiceRequests((response, error) => {
            if (response !== null) {
                dispatch({type: Actions.REQUEST_SERVICE_REQUESTS_DATA, serviceRequests: response});
            } else {
                dispatch({type: Actions.REQUEST_SERVICE_REQUESTS_DATA_ERROR});
            }
        });
    };

    const getBuyBackData = () => {
        APIHelper.getBuyBackData((response, error) => {
            if (response !== null) {
                stopBlockingLoader();
                dispatch({
                    type: Actions.REQUEST_BUYBACK_DATA,
                    buybackData: response.data,
                    deviceUUID: deviceIdentifier,
                });
                let placedOrders = response?.data?.buybackStatus?.filter(item => item?.orderId?.length)
                let orderIds = placedOrders?.map((item) => item.orderId)
                if (orderIds.length) {
                    APIHelper.getBuybackRequestFeedbackStatus(orderIds.join(','), (response, error) => {
                        let ratingPendingOrderIds = response?.data?.filter(item => !item.serviceRequestFeedback)?.map((item) => item.refPrimaryTrackingNo)
                        if (ratingPendingOrderIds?.length) {
                            let ratingNeedForOrderId = ratingPendingOrderIds[0];
                            let ratingNeedForOrder = placedOrders?.filter(item => item?.orderId == ratingNeedForOrderId)[0]
                            setRatingBuybackData({ quoteId: ratingNeedForOrder?.quoteId, servedBy: getServedByValue(ratingNeedForOrder?.subType), isAbb: ratingNeedForOrder?.membershipId ? true : false })
                            setShowRatingToolTip(true)
                        } else {
                            setRatingBuybackData({ quoteId: null, servedBy: null, isAbb: false })
                            setShowRatingToolTip(false)
                        }
                    });
                } else {
                    setRatingBuybackData({ quoteId: null, servedBy: null, isAbb: false })
                    setShowRatingToolTip(false)
                }
                if (PENDINGACTION.buttonAction == ButtonsAction.START_BUYBACK) {
                    setTimeout(() => {
                        performPendingAction(response.data);
                    }, 100);
                }
            } else {
                if (buybackRetryCount < 3) {
                    buybackRetryCount++;
                    getBuyBackData();
                }
            }
        });
    };

    const getSpotLightData = () => {
        APIHelper.getSpotLightData(isNewHomeScreen, (response, error) => {
            if (response) {
                dispatch({
                    type: Actions.REQUEST_SPOTLIGHT_DATA,
                    spotLightData: response.data ?? [],
                    totalVisibleCards: response.visibleItems ?? 5,
                });
            } else {
                getSpotLightData();
            }
        });
    };

    const handleRefreshControl = () => { // handling pull to refresh for user
        PENDINGACTION = {}
        buybackRetryCount = 0;
        resetHomeScreen();
        if (customer_id) {
            nativeBridgeRef.refreshMemberships();
        } else {
            hitAllAPIs();
        }
    };

    const handleTooltip = () => {
        if (!showHomeToolTip) { // if tooltip already not visible
            let text = null;
            // TODO: KAG, check for andorid??
            console.log("TOOLTIP TEXT BEING SET HERE!!!\nParams : ", customer_id, isNonSubscriber, isRewardOn, isAnsweredAllQuestions, isIDFenceInMem);
            if (customer_id) { // if customer logged in
                if ((isNonSubscriber === undefined || isAnsweredAllQuestions === undefined || isIDFenceInMem === undefined)) {
                    // this is the case when relative properties are not yet fetched.
                    return
                }
                if (!((isNonSubscriber && isRewardOn) || isAnsweredAllQuestions)) { // if user is subscribe or reward is off and all questions not answered
                    text = AppForAppFlowStrings.toolTipAlert.personalizedExperience;
                } else if (isNonSubscriber && !isIDFenceInMem && isRewardOn) { // if user is non-subscriber and he/she not purchased idfence yet and reward on
                    text = isAnsweredAllQuestions ? AppForAppFlowStrings.toolTipAlert.pendingRewards : AppForAppFlowStrings.toolTipAlert.rewardAlert;
                }
            } else { // user is not logged in
                text = AppForAppFlowStrings.toolTipAlert.nonVerifiedAlert;
            }
            if (text) {
                tooltipText = text
                localEventEmitter.emit('RefreshAccountRedDot', true);
                setShowHomeToolTip(true);
            } else {
                localEventEmitter.emit('RefreshAccountRedDot', false);
                setShowHomeToolTip(false);
            }
        }
    };

    //handle deeplinks
    const handleDeeplink = () => {
        const deeplink = deeplinkPath
        switch (true) {
            case deeplink.includes(Deeplink.buyback):
                if (state.buybackResponse && customer_id) {
                    nativeBridgeRef.startBuyBack(state.buybackResponse, deeplink);
                } else {
                    !isBlockingLoader ? startBlockingLoader(AppForAppFlowStrings.pleaseWait) : null;
                    PENDINGACTION.buttonAction = ButtonsAction.START_BUYBACK;
                    PENDINGACTION.pendingDeeplink = deeplink
                }
                setDeeplink(null);
                break;
            case deeplink.includes(Deeplink.myOffers):
            case deeplink.includes(Deeplink.onlineOffer):
            case deeplink.includes(Deeplink.offlineOffer):
                let moveToIndex = deeplink.includes(Deeplink.offlineOffer) ? 1 : 0;
                let urlParams = parseQueryParamsFromUrl(deeplink);
                let category = urlParams?.category || ""
                if (!(Number(customer_id) ?? 0) || state.cardsResponse) {
                    nativeBridgeRef.viewAllOffers(JSON.stringify(state.cardsResponse), category, 'Deeplink', moveToIndex);
                } else {
                    !isBlockingLoader ? startBlockingLoader(AppForAppFlowStrings.pleaseWait) : null;
                    PENDINGACTION.buttonAction = deeplink.includes(Deeplink.offlineOffer) ? ButtonsAction.VIEW_OFFLINE_OFFERS : ButtonsAction.VIEW_ONLINE_OFFERS;
                    PENDINGACTION.fromScreen = 'Deeplink';
                    PENDINGACTION.pendingDeeplink = deeplink
                    PENDINGACTION.category = category
                }
                setDeeplink(null);
                break;
            default:
                break;
        }
    };

    return (
        <HomeScreenContext.Provider value={{
            homeScreenState: state,
            schedulePickupSheet: schedulePickupSheet.current,
        }}>
            <MenuProvider>
            <BlockingLoader visible={isBlockingLoader} loadingMessage={blockingLoaderMessage}/>
            {
                state.isLoadingHomeScreenData
                    ? <Loader isLoading={state.isLoadingHomeScreenData}/>
                    : <HomeScreenLayout showHomeToolTip={showHomeToolTip}
                        isNewHomeScreen={isNewHomeScreen}
                                        handleRefreshControl={handleRefreshControl} tooltipText={tooltipText}
                                        onCloseTooptip={() => setShowHomeToolTip(false)}
                                        showBuybackRating={showRatingToolTip} onCloseBuybackRating={()=> setShowRatingToolTip(false)} ratingBuybackData={ratingBuybackData}
                                        tabIndex={home_screen_sub_tab ?? 0}/>
            }
            <RBSheet
                ref={schedulePickupSheet}
                duration={10}
                height={330}
                closeOnSwipeDown={true}
                closeOnPressMask={true}
            >
                <View style={{flex: 1}}>
                    <SchedulePickupBottomSheet
                        isLoading={false}
                        buybackData={state?.anyBuybackForSchedulePickup ?? {}}
                        customerId={customer_id}
                        onSchedulePickup={() => {
                            schedulePickupSheet.current.close()
                            setTimeout(() => {
                                nativeBridgeRef.startBuyBack(state?.buybackResponse ?? {}, AppForAppFlowStrings.buybackDeeplinkUrl(state?.anyBuybackForSchedulePickup?.quoteId ?? 1))
                            }, 100)
                        }}
                        onCancelSchedulePickup={() => {
                            schedulePickupSheet.current.close();
                            getBuyBackData();
                        }}
                    />
                </View>
            </RBSheet>
            </MenuProvider>
        </HomeScreenContext.Provider>
    );
}

let eventSendForRIDs = [];
export let eventSendForPriority = [];
export let currentSpotliteScrollIndex = 0;
export const setCurrentSpotliteScrollIndex = (index) => {
    currentSpotliteScrollIndex = index
}
export const addEventPriority = (priority) => {
    eventSendForPriority = [...eventSendForPriority, priority];
}

const ExploreView = React.memo((props) => {
    const {data, handleRefreshControl = () => {}} = props
    const [scrollViewSize, setScrollViewSize] = useState({});

    useEffect(() => {
        eventEmitter.addListener('HomeViewAppeared', params => {
            eventSendForRIDs = [];
            eventSendForPriority = [];
        });

        return () => {
            eventEmitter.removeListener('HomeViewAppeared', () => {
            });
        }
    }, [])

    return (<ScrollView
        onLayout={({nativeEvent}) => setScrollViewSize({
            width: nativeEvent.layout.width,
            height: nativeEvent.layout.height
        })}
        contentContainerStyle={styles.scrollViewContainerStyle}
        horizontal={false}
        nestedScrollEnabled={true}
        bounces={Platform.OS === "ios"}
        refreshControl={<RefreshControl refreshing={false} onRefresh={handleRefreshControl}/>}
        style={{flex: 1, backgroundColor: colors.color_F3F2F7}}>
        {
            data.map((data, index, array) => {
                const Component = getComponent(data.type);
                return (data?.type === "SectionalViewComponent" && data?.components[0]?.type === "CarouselVariation2")
                    ? <InViewPort key={index.toString()}
                        parentViewSize={scrollViewSize}
                        onChange={(isVisible) => {
                            if (isVisible) {
                                let components = data?.components[0]?.components
                                let spotliteData = components?.[currentSpotliteScrollIndex]
                                if (!eventSendForPriority.includes(spotliteData?.priority)) {
                                    let eventData = new Object();
                                    eventData[AppForAppFlowStrings.cardTitle] = spotliteData?.header?.value
                                    eventData[AppForAppFlowStrings.cardType] = spotliteData?.cardType
                                    eventData[AppForAppFlowStrings.position] = currentSpotliteScrollIndex + 1
                                    eventData[AppForAppFlowStrings.location] = "Homepage"
                                    logWebEnageEvent(SPOTLIGHT_VIEW, eventData);
                                    eventSendForPriority = [...eventSendForPriority, spotliteData?.priority];
                                }
                            }
                        }}>
                        <Component data={data} key={index.toString()}/>
                    </InViewPort>
                    : <Component data={data} key={index.toString()}/>
            })
        }
    </ScrollView>)
})

const FavouriteStrip = React.memo(() => {
    return (<TouchableOpacity onPress={() => nativeBridgeRef.openFavoriteRecommendations()}>
        <View style={styles.favouriteStripStyle}>
            <Text style={styles.favouriteStripTextStyle}>{STR_FAVOURITE_STRIP_TITLE}</Text>
            <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                <Image source={OAImageSource.my_favorites_heart.source}
                       style={OAImageSource.my_favorites_heart.dimensions}
                />
                <Image source={OAImageSource.cta_arrow_blue.source}
                       style={[OAImageSource.cta_arrow_blue.dimensions, {paddingTop: spacing.spacing14}]}
                />
            </View>
        </View>
    </TouchableOpacity>);
})

const TodayView = React.memo(React.forwardRef((props, ref) => {

    const {data, goToExplore} = props
    const [showNewRecommendationCTA, setShowNewRecommendationCTA] = useState(false);
    const [recommendationData, setRecommendationData] = useState({});
    const [newRecommendationData, setNewRecommendationData] = useState({});
    const [favRecommendationData, setFavRecommendationData] = useState([]);
    const [favoriteHintCount, setFavoriteHintCount] = useState(0);
    const [scrollContentSize, setScrollContentSize] = useState({});
    const [scrollContentOffset, setScrollContentOffset] = useState({x: 0, y: 0});
    const [scrollViewSize, setScrollViewSize] = useState({});
    const scrollViewRef = useRef(null);
    const viewRef = useRef(null);
    const [recoContentOffet, setRecoContentOffet] = useState({x: 0, y: 0});
    const [viewOffet, setViewOffet] = useState({x: 0, y: 0});
    const [refreshReco, setRefreshReco] = useState(false);

    useEffect(() => {
        //trackAppScreen(RECO_SCREENVIEW);
        let eventData = new Object();
        logWebEnageEvent(RECO_SCREENVIEW, eventData);
        getCatalystRecommendations();
        getFavCatalystRecommendations();
        Store.getFavoriteHintCount((error, result) => {
            let favCount = Number(result)
            if (favCount) {
                setFavoriteHintCount(favCount);
            }
        })
        eventEmitter.addListener('FavoriteRecommendationUpdated', params => {
            updateRecommendationsDataForFavorite(params.uiComponentId, params.isFavorite)
        });
        eventEmitter.addListener('HomeViewAppeared', params => {
            setRefreshReco(true);
            eventSendForRIDs = [];
            eventSendForPriority = [];
        });

        return () => {
            eventEmitter.removeListener('FavoriteRecommendationUpdated', () => {
            });
            eventEmitter.removeListener('HomeViewAppeared', () => {
            });
        }
    }, [])

    useEffect(() => {
        if (refreshReco) {
            getCatalystRecommendations();
        }
    }, [refreshReco])

    useImperativeHandle(ref, () => ({
        viewAppear() {
            setRefreshReco(true);
        }
    }));

    const getCatalystRecommendations = () => {
        // if (!isBlockingLoader) {
        //     startBlockingLoader(AppForAppFlowStrings.pleaseWait);
        // }
        getRecommendationsDataFromCache((recommendationsData) => {
            getLastRecommendationSaveTime((forecUpdateDateTime) => {
                let timeDiff = 0;
                if (forecUpdateDateTime) {
                    timeDiff = Math.abs(new Date() - forecUpdateDateTime) / 6e4;
                } else {
                    setLastRecommendationSaveTime(new Date());
                }

                if (timeDiff < recommendationForceUpdateInterval && recommendationsData) {
                    setRecommendationData(recommendationsData);
                } else if (timeDiff >= recommendationForceUpdateInterval) {
                    try {
                        Store.setRecommendationsData(null);
                        Store.setLastRecommendationSaveTime(null);
                        Store.setFavoriteRecommendationData(null);
                    } catch (exception) {
                    }
                    logWebEnageEvent(CATALYST_AUTO_REFRESH, {}, true);
                    setShowNewRecommendationCTA(false);
                }

                APIHelper.getCatalystRecommendedUIComponents(false, (response, error) => {
                    // stopBlockingLoader();
                    if (response) {
                        if (timeDiff < recommendationForceUpdateInterval && recommendationsData) {
                            if (response?.data?.rcId !== recommendationsData?.rcId) {
                                setShowNewRecommendationCTA(true);
                                setNewRecommendationData(response?.data)
                            }
                        } else {
                            setShowNewRecommendationCTA(false);
                            setRecommendationData(response?.data);
                            setRecommendationsDataInCache(response?.data ?? {})
                            setLastRecommendationSaveTime(new Date());
                        }
                    }
                    setRefreshReco(false);
                })
            })
        })
    }

    const getFavCatalystRecommendations = () => {
        APIHelper.getCatalystRecommendedUIComponents(true, (response, error) => {
            if (response) {
                setFavRecommendationData(response?.data?.components ?? [])
                Store.setFavoriteRecommendationData(JSON.stringify(response?.data?.components ?? "[]"));
            }
        })
    }

    const deleteRecommendationCard = (data, menuData) => {
        if (menuData?.[0]?.action === 'DISMISS') {
            let jsonRecommendationsData = recommendationData
            let findIndex = jsonRecommendationsData?.components?.findIndex(item => item?.uiComponentId === data?.uiComponentId);
            if (findIndex != -1) {
                jsonRecommendationsData?.components?.splice(findIndex, 1);
                setRecommendationData(jsonRecommendationsData);
                setRecommendationsDataInCache(jsonRecommendationsData)
            }
        } else {
            //TODO need to add default action handling Imran
        }
    }

    const updateRecommendationsDataForFavorite = (uiComponentId, isFavorite) => {
        getRecommendationsDataFromCache((recommendationsData) => {
            Store.getFavoriteRecommendationData((error1, result1) => {
                if (result1) {
                    let catalystRecommendationsFavList = parseJSON(result1);
                    let findIndex = catalystRecommendationsFavList?.findIndex(item => item?.uiComponentId === uiComponentId);
                    let recommendationItem = recommendationsData?.components?.filter(data => data?.uiComponentId === uiComponentId)?.[0];
                    if (findIndex === -1 && isFavorite) {
                        recommendationItem.favorite.value = isFavorite;
                        catalystRecommendationsFavList.push(recommendationItem);
                    } else {
                        if (isFavorite) {
                            catalystRecommendationsFavList[findIndex] = recommendationItem;
                        } else {
                            catalystRecommendationsFavList.splice(findIndex, 1);
                        }
                    }

                    recommendationsData?.components?.map((data, index) => {
                        if (uiComponentId === data?.uiComponentId) {
                            data.favorite.value = isFavorite;
                        }
                    });
                    setFavRecommendationData(catalystRecommendationsFavList)
                    setRecommendationData(recommendationsData);
                    setRecommendationsDataInCache(recommendationsData)
                    Store.setFavoriteRecommendationData(JSON.stringify(catalystRecommendationsFavList));
                }
            });
        })
        return
    }

    const setLastRecommendationSaveTime = (date) => {
        Store.setLastRecommendationSaveTime(date?.toString());
    }

    const getLastRecommendationSaveTime = (callback) => {
        Store.getLastRecommendationSaveTime((error, result) => {
            if (result) {
                callback(new Date(result));
            } else {
                callback(null)
            }
        });
    }

    const getRecommendationsDataFromCache = (callback) => {
        Store.getRecommendationsData((error, result) => {
            if (result) {
                callback(parseJSON(result));
            } else {
                callback(null)
            }
        });
    }

    const setRecommendationsDataInCache = (recommendationsData) => {
        if (!(recommendationsData?.components?.length)) {
            let eventData = new Object();
            eventData[AppForAppFlowStrings.location] = RECO_SCREEN
            logWebEnageEvent(NO_CATALYST_RECO, eventData);
        }
        Store.setRecommendationsData(JSON.stringify(recommendationsData))
    }

    const increaseFavoriteHintCount = () => {
        Store.setFavoriteHintCount((favoriteHintCount + 1).toString());
        setFavoriteHintCount(favoriteHintCount + 1);
    }

    const updateFavoriteRecommendationStatus = (rcId, rId, uiComponentId, isFavorite) => {
        APIHelper.updateFavoriteRecommendation(rcId, [{
            rId: rId,
            uiComponentId: uiComponentId,
            isFavorite: isFavorite
        }], (response, error) => {
            if (response != null) {
                updateRecommendationsDataForFavorite(uiComponentId, isFavorite)
            }
        })
    }

    const onReloadRecommendationsClick = () => {
        let eventData = new Object();
        eventData[AppForAppFlowStrings.location] = RECO_SCREEN
        logWebEnageEvent(UPDATE_CATALYST_RECO, eventData);
        setShowNewRecommendationCTA(false);
        setRecommendationData(newRecommendationData);
        setRecommendationsDataInCache(newRecommendationData)
        setNewRecommendationData({});
        setLastRecommendationSaveTime(new Date());
        scrollToRecommendations()
    }

    const SpotLightView = data.map((data, index, array) => {
        data.title = "Based on your recent activity";
        return <InViewPort key={index.toString()}
            parentViewSize={scrollViewSize}
            onChange={(isVisible) => {
                if (isVisible) {
                    let components = data?.components[0]?.components
                    let spotliteData = components?.[currentSpotliteScrollIndex]
                    if (!eventSendForPriority.includes(spotliteData?.priority)) {
                        let eventData = new Object();
                        eventData[AppForAppFlowStrings.cardTitle] = spotliteData?.header?.value
                        eventData[AppForAppFlowStrings.cardType] = spotliteData?.cardType
                        eventData[AppForAppFlowStrings.position] = currentSpotliteScrollIndex + 1
                        eventData[AppForAppFlowStrings.location] = RECO_SCREEN
                        logWebEnageEvent(SPOTLIGHT_VIEW, eventData);
                        eventSendForPriority = [...eventSendForPriority, spotliteData?.priority];
                    }
                }
            }}>
            <SectionalViewComponentV2 data={data} key={index.toString()}/>
        </InViewPort>;
    });

    const scrollToRecommendations = () => {
        try {
            if (Platform.OS == PLATFORM_OS.ios) {
                setTimeout(() => {
                    try {
                        scrollViewRef?.current?.scrollTo({
                            x: recoContentOffet.x,
                            y: recoContentOffet.y - viewOffet.y,
                            animated: true
                        });
                    } catch (e) {
                    }
                }, 100);
            } else {
                setTimeout(() => {
                    try {
                        scrollViewRef?.current?.scrollTo({ x: 0, y: 0, animated: true });
                    } catch (e) { }
                }, 600);
            }
        } catch (e) {
        }
    }

    return (
        <View style={{flex: 1}} ref={ref}>
            <View style={{flex: 1}} ref={viewRef}
                  onLayout={({nativeEvent}) => viewRef.current.measureInWindow((x, y, width, height) => setViewOffet({
                      x: x,
                      y: y
                  }))}>
                <ScrollView
                    scrollToOverflowEnabled={Platform.OS == PLATFORM_OS.android}
                    ref={scrollViewRef}
                    scrollEventThrottle={16}
                    onScroll={({nativeEvent}) => {
                        const {x, y} = nativeEvent.contentOffset
                        setScrollContentOffset({x: x, y: y})
                    }}
                    onContentSizeChange={(width, height) => setScrollContentSize({width: width, height: height})}
                    onLayout={({nativeEvent}) => setScrollViewSize({
                        width: nativeEvent.layout.width,
                        height: nativeEvent.layout.height
                    })}
                    contentContainerStyle={styles.scrollViewContainerStyle}
                    horizontal={false}
                    nestedScrollEnabled={true}
                    bounces={Platform.OS === "ios"}
                    style={{flex: 1, backgroundColor: colors.color_F3F2F7}}>
                    {SpotLightView}
                    {favRecommendationData?.length > 0 ? <FavouriteStrip/> : null}
                    <RecommendationComponent data={recommendationData}
                                             scrollViewSize={scrollViewSize}
                                             scrollContentSize={scrollContentSize}
                                             updateRecoViewPosition={(x, y) => setRecoContentOffet({
                                                 x: x + scrollContentOffset.x,
                                                 y: y + scrollContentOffset.y
                                             })}
                                             headerTitle={STR_RECOMMENDED_PICKS_FOR_TODAY}
                                             favouritesHintData={favoriteHintCount < 3 ? {
                                                 title: STR_FAVOURITES_AND_DISMISS_HINT,
                                                 imageUrl: OAImageSource.tip_light_buib
                                             } : null}
                                             componentVisible={(index, data) => {
                                                 if (!eventSendForRIDs.includes(data?.rid)) {
                                                     let eventData = new Object();
                                                     eventData[AppForAppFlowStrings.componentId] = data?.uiComponentId
                                                     eventData[AppForAppFlowStrings.id] = data?.rid
                                                     eventData[AppForAppFlowStrings.commId] = recommendationData.rcId
                                                     eventData[AppForAppFlowStrings.product] = data?.product
                                                     eventData[AppForAppFlowStrings.service] = data?.service
                                                     eventData[AppForAppFlowStrings.commCat] = data?.commCategory
                                                     eventData[AppForAppFlowStrings.commSubcat] = data?.commSubcategory
                                                     eventData[AppForAppFlowStrings.position] = index
                                                     eventData[AppForAppFlowStrings.location] = RECO_SCREEN
                                                     logWebEnageEvent(CATALYST_CARD_VIEW, eventData);
                                                     eventSendForRIDs = [...eventSendForRIDs, data?.rid]
                                                 }
                                             }}
                                             deleteRecommendation={(index, data, menuData) => {
                                                 let eventData = new Object();
                                                 eventData[AppForAppFlowStrings.componentId] = data?.uiComponentId
                                                 eventData[AppForAppFlowStrings.id] = data?.rid
                                                 eventData[AppForAppFlowStrings.commId] = recommendationData.rcId
                                                 eventData[AppForAppFlowStrings.product] = data?.product
                                                 eventData[AppForAppFlowStrings.service] = data?.service
                                                 eventData[AppForAppFlowStrings.commCat] = data?.commCategory
                                                 eventData[AppForAppFlowStrings.commSubcat] = data?.commSubcategory
                                                 eventData[AppForAppFlowStrings.position] = index
                                                 logWebEnageEvent(CATALYST_DISMISS, eventData);
                                                 deleteRecommendationCard(data, menuData);
                                                 increaseFavoriteHintCount()
                                             }}
                                             markFavorite={(index, data) => {
                                                 let eventData = new Object();
                                                 eventData[AppForAppFlowStrings.componentId] = data?.uiComponentId
                                                 eventData[AppForAppFlowStrings.id] = data?.rid
                                                 eventData[AppForAppFlowStrings.commId] = recommendationData.rcId
                                                 eventData[AppForAppFlowStrings.product] = data?.product
                                                 eventData[AppForAppFlowStrings.service] = data?.service
                                                 eventData[AppForAppFlowStrings.commCat] = data?.commCategory
                                                 eventData[AppForAppFlowStrings.commSubcat] = data?.commSubcategory
                                                 eventData[AppForAppFlowStrings.position] = index
                                                 logWebEnageEvent(CATALYST_FAVOURITE, eventData);
                                                 updateFavoriteRecommendationStatus(recommendationData.rcId, data.rid, data.uiComponentId, !(data.favorite?.value ?? false))
                                                 increaseFavoriteHintCount()
                                             }}
                                             onClick={(index, data, actionUrl) => {
                                                 let eventData = new Object();
                                                 eventData[AppForAppFlowStrings.componentId] = data?.uiComponentId?.toString()
                                                 eventData[AppForAppFlowStrings.id] = data?.rid?.toString()
                                                 eventData[AppForAppFlowStrings.commId] = recommendationData.rcId?.toString()
                                                 eventData[AppForAppFlowStrings.product] = data?.product?.toString()
                                                 eventData[AppForAppFlowStrings.service] = data?.service?.toString()
                                                 eventData[AppForAppFlowStrings.commCat] = data?.commCategory?.toString()
                                                 eventData[AppForAppFlowStrings.commSubcat] = data?.commSubcategory?.toString()
                                                 eventData[AppForAppFlowStrings.position] = index
                                                 eventData[AppForAppFlowStrings.location] = RECO_SCREEN
                                                 logWebEnageEvent(CATALYST_CARD_CLICK, eventData);
                                                 nativeBridgeRef.processDeeplink(actionUrl, eventData);
                                             }}
                                             onExploreViewClick={() => {
                                                 let eventData = new Object();
                                                 eventData[AppForAppFlowStrings.location] = RECO_SCREEN
                                                 logWebEnageEvent(HOME_TAB, eventData);
                                                 goToExplore()
                                             }}/>
                </ScrollView>
                {(showNewRecommendationCTA) ? <TooltipComponentV2 data={{
                    "text": STR_SEE_NEW_RECOMMENDATIONS,
                    "bottomHeight": spacing.spacing5,
                    "image": images.ic_whiteClose
                }} onReloadRecommendationsClick={onReloadRecommendationsClick}/> : null}
            </View>
        </View>)
}))

const NewHomeScreen = React.memo((props) => {
    const { data, handleRefreshControl } = props
    const tabRef = useRef(null);
    const exploreScreenData = data.filter(component => component.compTag !== Tags.SpotLightSectionalViewComponent)
    const todayScreenData = data.filter(component => component.compTag === Tags.SpotLightSectionalViewComponent)
    const todayTabRef = useRef(null);

    useEffect(() => {
        tabRef?.current?.goToPage(props?.tabIndex ?? 0)
        eventEmitter.addListener('SwitchHomeScreenSubTab', params => {
            tabRef?.current?.goToPage(params?.tabIndex ?? 0)
        })
        return () => {
            eventEmitter.removeListener('SwitchHomeScreenSubTab', () => {
            });
        }
    }, [])

    return (<View style={styles.container}>
        <ScrollableTabView initialPage={showTodayTab ? 0 : 1}
                           prerenderingSiblingsNumber={1}
                           ref={tabRef}
                           locked={true}
                           onChangeTab={({i, ref}) => {
                               eventSendForRIDs = [];
                               eventSendForPriority = [];
                               if (ref.props.tabLabel == "Today") {
                                   todayTabRef?.current?.viewAppear();
                               }
                           }}
                           renderTabBar={() => <ScrollableTabBar tabsContainerStyle={styles.tabContainerView}
                                                                 hasShadow={true}
                                                                 underlineStyle={styles.tabBarUnderlineView}
                                                                 textStyle={styles.tabBarTextStyle}
                                                                 activeTextColor={colors.color_008DF6}
                                                                 inactiveTextColor={colors.color_757575}/>}
        >
            <TodayView ref={todayTabRef} tabLabel={"Today"} key={0} data={todayScreenData} goToExplore={() => tabRef?.current?.goToPage(1)} />
            <ExploreView tabLabel={"Explore"} key={1} data={exploreScreenData} handleRefreshControl={handleRefreshControl}/>
        </ScrollableTabView>
    </View>)
})

const HomeScreenLayout = props => {
    const { showHomeToolTip, tooltipText, isNewHomeScreen, showBuybackRating, onCloseBuybackRating = () => { }, ratingBuybackData, handleRefreshControl } = props
    const { homeScreenState } = useContext(HomeScreenContext)

    return (
        <SafeAreaView style={{flex: 1}}>
            {isNewHomeScreen ?
                <NewHomeScreen data={homeScreenState.data} handleRefreshControl={handleRefreshControl} tabIndex={props.tabIndex ?? 0}/> :
                <ExploreView data={homeScreenState.data} handleRefreshControl={handleRefreshControl} />}
            {(showHomeToolTip) ? <TooltipComponent
                isVisible={showHomeToolTip}
                data={{
                    "text": tooltipText,
                    "bottomHeight": spacing.spacing5,
                    "image": images.ic_whiteClose
                }} onCloseTooptip={props.onCloseTooptip} /> : null}
            {showBuybackRating ? <TooltipRating data={{
                text: "Assured BuyBack Completed",
                ratingText: "Rate your experience >",
                bottomHeight: spacing.spacing5,
                leftImage: images.final_price_success,
                crossImage: images.ic_whiteClose
            }}
                onClick={() => nativeBridgeRef.openRating(ratingBuybackData.quoteId, ratingBuybackData.servedBy, ratingBuybackData.isAbb)}
                onCrossClick={() => onCloseBuybackRating()} /> : null}
        </SafeAreaView>
    );
};

/*
* handling pending button/card actions which are not handle due to login required
 */
const performPendingAction = (responseData) => {
    switch (PENDINGACTION.buttonAction) {
        case ButtonsAction.START_BUYBACK:
            PENDINGACTION = {};
            nativeBridgeRef.startBuyBack(responseData, PENDINGACTION?.pendingDeeplink);
            break;
        case ButtonsAction.VIEW_OFFLINE_OFFERS:
        case ButtonsAction.SEE_ALL_OFFERS:
        case ButtonsAction.CARD_OFFERS:
        case ButtonsAction.VIEW_ONLINE_OFFERS:
            let selectedIndex = ButtonsAction.VIEW_OFFLINE_OFFERS == PENDINGACTION.buttonAction ? 1 : 0;
            nativeBridgeRef.viewAllOffers(JSON.stringify(responseData), PENDINGACTION?.category ?? "", PENDINGACTION.fromScreen ?? AppForAppFlowStrings.explore, Number(selectedIndex));
            PENDINGACTION = {};
            break;
        default:
            break;
    }
};

const styles = StyleSheet.create({
    scrollViewContainerStyle: {
        backgroundColor: colors.color_F3F2F7,
        paddingBottom: Platform.OS == PLATFORM_OS.ios ? spacing.spacing40 : 0
    },
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: colors.white
    },
    tabBarTextStyle: {
        fontSize: 14,
        fontFamily: "Lato-Regular",
        lineHeight: 20
    },
    tabBarUnderlineView: {
        backgroundColor: colors.blue028,
        height: spacing.spacing2,
        borderRadius: spacing.spacing2
    },
    tabContainerView: {
        borderBottomColor: colors.grey,
        justifyContent: Platform.OS === PLATFORM_OS.ios ? 'space-around' : 'flex-start'
    },
    favouriteStripStyle: {
        flexDirection: 'row',
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'space-between',
        flex: 1,
        paddingHorizontal: spacing.spacing12,
        height: spacing.spacing44,
        borderWidth: spacing.spacing1,
        borderColor: colors.borderOpacity_point_1,
        borderRadius: spacing.spacing4,
        backgroundColor: colors.color_E4F2FD,
        marginBottom: spacing.spacing5,
        marginHorizontal: spacing.spacing16,
        marginTop: spacing.spacing20,
    },
    favouriteStripTextStyle: {
        ...TextStyle.text_14_normal,
        color: colors.color_212121,
    }
})

export default HomeScreen;
