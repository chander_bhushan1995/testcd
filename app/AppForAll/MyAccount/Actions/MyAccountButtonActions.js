import { NativeModules, Platform, Alert } from 'react-native'
import { AppForAppFlowStrings } from '../../Constant/AppForAllConstants';

import {
    SHARE_APP,
    MY_REWARDS,
    LOGOUT,
    CHAT,
    CHAT_ON_HOME_TAPPED,
    RATE_APP,
    ACTIVATE_VOUCHER,
} from '../../../Constants/WebengageEvents';
import {logWebEnageEvent} from '../../../commonUtil/WebengageTrackingUtils';

const nativeBrigeRef = NativeModules.ChatBridge;

export const WebViewFor = {
    PRIVACYPOLICY: "PRIVACYPOLICY",
    RATEONAPPSTORE: "RATEONAPPSTORE",
    ACTIVATEVOUCHER: "ACTIVATEVOUCHER"
}

export const MyAccountBtnActions = {
    goToMyRewards: (isAnsweredAllQuestions,showScratchCard) => {
        let eventData = new Object();
        eventData[AppForAppFlowStrings.location] = AppForAppFlowStrings.accountTabScreen
        logWebEnageEvent(MY_REWARDS, eventData);
        nativeBrigeRef.startMyRewardScreen(isAnsweredAllQuestions,showScratchCard ?? false)
    },

    goToBookMarkOffers: (isLoggedIn) => { //navigate to bookmark offers screen
        if (isLoggedIn) {
            nativeBrigeRef.goToBookMarkOffers()
        } else {
            nativeBrigeRef.showPopupForVerifyNumber(AppForAppFlowStrings.bookmarkLoginAlert.title, "", AppForAppFlowStrings.bookMarkOffer, status => {
                if (status) {
                    nativeBrigeRef.goToBookMarkOffers()
                }
            })
        }
    },

    handleOnShare: () => { //calling shareLink method from bridge according to platform
        let eventData = new Object();
        eventData[AppForAppFlowStrings.location] = AppForAppFlowStrings.accountTabScreen
        logWebEnageEvent(SHARE_APP,eventData)
        nativeBrigeRef.shareLink("")
    },

    openWebView: (WebViewFor) => {
        let eventData = new Object();
        eventData[AppForAppFlowStrings.location] = AppForAppFlowStrings.accountTabScreen
        switch (WebViewFor) {
            case WebViewFor.RATEONAPPSTORE:
                logWebEnageEvent(RATE_APP, eventData);
                break
            case WebViewFor.ACTIVATEVOUCHER:
                logWebEnageEvent(ACTIVATE_VOUCHER, eventData);
                break
            default:
                break
        }
        nativeBrigeRef.openWebView(WebViewFor,"","")
    },

    goToSettings: ()=>{
        nativeBrigeRef.goToSettings()
    },

    goToActivateVoucher: (isLoggedIn) => { //navigate to bookmark offers screen
        if (isLoggedIn) {
            nativeBrigeRef.goToActivateVoucher(AppForAppFlowStrings.myAccountScreen)
        } else {
            nativeBrigeRef.showPopupForVerifyNumber(AppForAppFlowStrings.activateVoucherLoginAlert.title, "", AppForAppFlowStrings.activateVoucher, status => {
                if (status) {
                    nativeBrigeRef.goToActivateVoucher(AppForAppFlowStrings.myAccountScreen)
                }
            })
        }
    },

    openChat: (isLoggedIn) => { //navigate to bookmark offers screen
        let eventData = new Object();
        eventData[AppForAppFlowStrings.location] = AppForAppFlowStrings.accountTabScreen
        logWebEnageEvent(CHAT, eventData);
        logWebEnageEvent(CHAT_ON_HOME_TAPPED, new Object());
        if (isLoggedIn) {
            nativeBrigeRef.openChat()
        } else {
            nativeBrigeRef.showPopupForVerifyNumber(AppForAppFlowStrings.chatLoginAlert.title, "", AppForAppFlowStrings.chat, status => {
                nativeBrigeRef.openChat()
            })
        }
    },

    handleLogout: () => {
        showAlert(AppForAppFlowStrings.myAccount.logoutTitle, AppForAppFlowStrings.myAccount.logoutmessage, AppForAppFlowStrings.myAccount.logoutCTAText)
    },

    showAllServiceRequests: (isLoggedIn) => {
        if(isLoggedIn) {
            nativeBrigeRef.showMyServiceRequestsScreen()
        }else {
            nativeBrigeRef.showPopupForVerifyNumber(AppForAppFlowStrings.serviceRequestAlert.title, "", AppForAppFlowStrings.serviceRequests, status => {
                nativeBrigeRef.showMyServiceRequestsScreen()
            })
        }
    },

    showGadgetAppliancesAndBanks: (isLoggedIn) => {
        if(isLoggedIn) {
            nativeBrigeRef.goToMyProfile();
        } else {
            nativeBrigeRef.showPopupForVerifyNumber(AppForAppFlowStrings.gadgetAppliancesAndBanksAlert.title, "", AppForAppFlowStrings.gadgetAppliancesAndBanks, status => {
                nativeBrigeRef.goToMyProfile();
            })
        }
    }
}

function showAlert(title, alertMessage, buttonText) {
    if (this.DialogViewRef !== null && this.DialogViewRef !== undefined) {
        this.DialogViewRef.showDailog({
            title: title,
            message: alertMessage,
            primaryButton: {
                text: buttonText, onClick: () => {
                    logWebEnageEvent(LOGOUT, new Object());
                    nativeBrigeRef.handleLogout()
                },
            },
            cancelable: true,
            onClose: () => {
            },
        });
    }
}

