import QuestionsGenericScreen, {flowTypes} from '../Components/QuestionsGenericComponent';
import {connect} from 'react-redux';
import * as Actions from '../../../Constants/ActionTypes';
import {store} from '../../../Navigation/index.userregistration';
import {NativeModules} from 'react-native';
import { makePostRequest } from '../../../network/ApiManager';
import {SUBMIT_ANSWER_OF_QUESTIONS, SUBMIT_NAME_EMAIL} from '../../../Constants/ApiUrls';
import {QuestionsCategoryCode} from '../../../Constants/AppConstants';
import { parseJSON, deepCopy} from "../../../commonUtil/AppUtils";

const chatBridge = NativeModules.ChatBridge;

const mapStateToProps = state => ({
    // Store has access to all the Reducers
    name: state.appData.params.user_Name,
    email: state.appData.params.user_Email,

    custId: state.appData.params.customer_id,

    isPopulated: state.appData.isPopulated,

    flowType: state.appData.params.flowType,
    type: state.appData.type,

    index: state.appData.index,
    isLoading: state.appData.isLoading,

    totalScreens: state.appData.totalScreens,
    currentScreenNo: state.appData.currentScreenNo,

    currentBrand: state.appData.params.currentBrand,
    currentModel: state.appData.params.currentModel,

    currentOptionList: state.appData.currentOptionList,

    dataSourceQuestionsScreen: state.appData.dataSourceQuestionsScreen,

    questionAnswer: parseJSON(state.appData.questionAnswer).data,
});

const mapDispatchToProps = dispatch => ({
    prepareDataSourceFromResponse: (questionAnswerResponse, currentBrand, currentModel, flowType, name, email) => {
        store.dispatch({
            type: Actions.PARSE_QUESTION_ANSWER_RESPONSE,
            questionAnswerResponse: questionAnswerResponse,
            currentBrand: currentBrand,
            currentModel: currentModel,
            flowType: flowType,
            name: name,
            email: email,
        });
    },

    changeStateOfCheckbox: (section, row, dataSourceQuestionsScreen, index, currentOptionList) => {
        store.dispatch({
            type: Actions.CHANGE_STATE_OF_CHECKBOX,
            column: section,
            row: row,
            dataSourceQuestionsScreen: dataSourceQuestionsScreen,
            index: index,
            currentOptionList: currentOptionList,
        });
    },

    saveNameAndEmail: (name, email, custId) => {
        store.dispatch({type: Actions.LOADING_BUTTON, isLoading: true});

        if (name || email) {
            submitNameAndEmailData(name, email, custId);
        } else {
            store.dispatch({type: Actions.NAME_EMAIL_SUBMITTED, isLoading: false});
        }
    },

    saveAnsweredQuestionsData: (questionAnswer, index, currentScreenNo, navigation, custId, currentOptionList, quit, flowType) => {
        store.dispatch({type: Actions.LOADING_BUTTON, isLoading: true});
        submitAnsweredQuestionsData(questionAnswer, index, currentScreenNo, navigation, custId, currentOptionList, quit, flowType);
    },
});

function getSelectedOptionList(questionAnswer){
    return [].concat.apply([], questionAnswer.sectionData.filter(section => section.data.length).map(section => section.data.filter(option => option.isSelected && !option.isOther).map(option => {
        return option.optionId})))
}

function submitAnsweredQuestionsData(questionAnswer, index, currentScreenNo, navigation, custId, currentOptionList, quit, flowType) {
        let bodyData = {
            'questionId': questionAnswer.questionId,
            'selectedOptionList': getSelectedOptionList(questionAnswer),
            'otherAnswerList': questionAnswer.sectionData?.[0].data.filter(option => option.isOther).map(option => {
                let [brand, ...model] = option.optionText.split(" ");
                model = model.join(" ");
                return ({
                    'brand': brand ?? '',
                    'model': model ?? '',
                });

            }) ?? [],
            'categoryCode': QuestionsCategoryCode.onBoarding,
        };
    makePostRequest(SUBMIT_ANSWER_OF_QUESTIONS(custId), bodyData, (response, error) => {
            if (response) {
                store.dispatch({
                    type: Actions.SUBMIT_PRESSED,
                    index: index,
                    currentScreenNo: currentScreenNo,
                    isLoading: false,
                });

                if(flowType !== flowTypes.Edit) {
                    navigation.setParams({currentOptionList: currentOptionList, quit: quit});
                }
            } else {
                store.dispatch({type: Actions.API_FAILED, isLoading: false, error: error.error});
            }
        });
}

function submitNameAndEmailData(name, email, custId) {
        let bodyData = {
            'customers': [{
                'custId': custId,
            }],
        };

        if (name) {
            bodyData.customers[0].firstName = name;
        }

        if (email) {
            bodyData.customers[0].emailId = email;
        }
    makePostRequest(SUBMIT_NAME_EMAIL, bodyData, (response, error) => {
            if (response) {
                getCustomerDetail((status) => {
                    if (status) {
                        store.dispatch({type: Actions.NAME_EMAIL_SUBMITTED, isLoading: false});
                    }else {
                        store.dispatch({type: Actions.API_FAILED, isLoading: false});
                    }
                })

            } else {
                store.dispatch({type: Actions.API_FAILED, isLoading: false, error: error.error});
            }
    });
}

async function getCustomerDetail(onResponseCome) {
    try {
        var response = await chatBridge.callForGetCustumerResposne();
        onResponseCome(true);

    } catch (e) {
        onResponseCome(false);
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(QuestionsGenericScreen);
