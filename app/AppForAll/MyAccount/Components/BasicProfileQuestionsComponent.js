import React, {useEffect, useState} from 'react';
import {
    BackHandler,
    Image,
    NativeModules,
    Platform,
    SafeAreaView, ScrollView,
    SectionList,
    FlatList,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import spacing from '../../../Constants/Spacing';
import colors from '../../../Constants/colors';
import ButtonWithLoader from '../../../CommonComponents/ButtonWithLoader';
import InputBox from '../../../Components/InputBox';
import {TextStyle} from '../../../Constants/CommonStyle';
import {store} from '../../../Navigation/index.userregistration';
import { AppForAppFlowStrings } from '../../Constant/AppForAllConstants';

function testEmail(input) {
    if (input.length > 96) {
        return false;
    }
    const re = /.+@.+[.]([a-zA-Z]){2,3}/;
    return re.test(input);
}


const BasicDetailView = props => {
    // TODO: Add dropdown for android case in InputBox in case of email.
    return (
        <View style={props.style}>
            <Text style={[TextStyle.text_20_bold, {lineHeight: 28}]}>{props.item.question}</Text>
            <InputBox
                containerStyle={styles.textInputContainer}
                inputHeading={props.item.fieldHeader}
                placeholder={props.item.fieldPlaceholder}
                placeholderTextColor={colors.grey}
                isMultiLines={false}
                keyboardType={props.item.keyboardType}
                editable={true}
                errorMessage={props.item.errorMessage}
                value={props.item.fieldValue}
                onChangeText={(text) => {
                    props.item.onFocus();
                    if (props.item.isEmail) {
                        props.setEmail(text);
                        store.getState().appData.currentEmail = text
                    } else {
                        props.setName(text);
                        store.getState().appData.currentName = text
                    }
                    props.handleSubmitEnabled(store.getState().appData.currentName.length > 0 && store.getState().appData.currentEmail.length > 0)
                }}
            />
        </View>
    );
};

function isTextEmpty(text) {
  if(text === undefined || text === null || text.trim() === ''){
      return true;
  }else {
      return false;
  }
}

const BasicProfileQuestionsScreen = props => {
    const [name, setName] = useState(props.name);
    const [email, setEmail] = useState(props.email);
    const [isSubmitEnabled,setSubmitEnabled] = useState(!isTextEmpty(name) && !isTextEmpty(email))
    const [errorMessage, setErrorMessage] = useState('');

    store.getState().appData.currentName = !isTextEmpty(name) ? name : "";
    store.getState().appData.currentEmail = !isTextEmpty(email) ? email : "";

    const handleSubmitEnabled = (val) => {
        setSubmitEnabled(val)
    }



    return (
        <View style={[styles.MainContainer]}>
            <ScrollView style={{marginBottom: spacing.spacing16}}>
                <FlatList
                    scrollEnabled={false}
                    data={[
                        {
                            isEmail: false,
                            question: AppForAppFlowStrings.onboardingQuestions.nameQuestion,
                            fieldHeader: AppForAppFlowStrings.onboardingQuestions.fullNameOnID,
                            fieldPlaceholder: AppForAppFlowStrings.onboardingQuestions.fullName,
                            keyboardType: "default",
                            fieldValue: name,
                            onFocus: () => {}
                        },
                        {
                            isEmail: true,
                            question: AppForAppFlowStrings.onboardingQuestions.andEmail,
                            fieldHeader: AppForAppFlowStrings.onboardingQuestions.email,
                            fieldPlaceholder: AppForAppFlowStrings.onboardingQuestions.enterEmail,
                            keyboardType: AppForAppFlowStrings.onboardingQuestions.emailAddress,
                            fieldValue: email,
                            errorMessage: errorMessage,
                            onFocus: () => setErrorMessage('')
                    },
                    ]}
                    renderItem={({item}) =>
                        <BasicDetailView style={{marginTop: spacing.spacing48, marginHorizontal: spacing.spacing16}}
                                         item={item} setName={setName} setEmail={setEmail} handleSubmitEnabled={handleSubmitEnabled}/>
                    }
                />
                <View style={styles.buttonStyle}>
                    <ButtonWithLoader
                        isLoading={props.isLoading}
                        // enable={(!props.isLoading)}
                        enable = {isSubmitEnabled}
                        Button={{
                            text: props.buttonText,
                            onClick: () => {
                                if(email && !testEmail(email)) {
                                    setErrorMessage(AppForAppFlowStrings.onboardingQuestions.enterValidEmail);
                                } else {
                                    props.onPressSubmitDetails(name,email);
                                }
                            },
                        }}/>
                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    MainContainer: {
        backgroundColor: colors.white,
        flex: 1,
    },
    buttonStyle: {
        marginHorizontal: spacing.spacing16,
        marginTop: spacing.spacing56,
        height: 48,
    },
    textInputContainer: {
        marginTop: spacing.spacing16,
        width: '100%',
    }
});

export default BasicProfileQuestionsScreen;
