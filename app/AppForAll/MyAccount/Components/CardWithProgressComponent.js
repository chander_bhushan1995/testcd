import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native'
import { TextStyle, CommonStyle } from '../../../Constants/CommonStyle';
import RightArrowButton from '../../../CommonComponents/RightArrowButton'
import ProgressBarsRow from './ProgressBarsRowComponent'
import spacing from "../../../Constants/Spacing";
import colors from '../../../Constants/colors';
import Images from '../../../images/index.image';

const CardInfoHeader = props => {
    const { nameText, relationshipNoText, contactText, addressData, onEditClick } = props

    return (<View style={{ marginLeft: spacing.spacing16, flex: 1 }}>

        <View style={{ flexDirection: 'row' }}>
            <Text style={styles.header}>{nameText}</Text>
            <TouchableOpacity style={styles.rightImageTouchable} onPress={onEditClick}>
                <Image source={Images.ic_edit_transparent} style={styles.rightImage} />
            </TouchableOpacity>
        </View>
        <Text style={styles.info}>{relationshipNoText}</Text>
        <Text style={styles.info}>{contactText}</Text>
        {addressData?.trim().length ? <Text style={styles.info}>{addressData}</Text> : null}
    </View>)
}

const CardWithProgressComponent = props => {

    const headerText = props.headerText || null
    const headerTextStyle = props.headerTextStyle || styles.headerTextStyle
    const image = props.image || Images.incompleteProfilePlaceholder
    const footerText = props.footerText || ''
    const footerContainerStyle = props.footerContainerStyle || styles.footerContainerStyle
    const headerContainerStyle = props.headerContainerStyle || styles.imageTextContainer
    const footerTextStyle = props.footerTextStyle || styles.footerTextStyle
    const footerButtonText = props.footerButtonText
    const imageStyle = props.imageStyle || {}
    const totalBars = props.totalBars || 0
    const filledBars = props.filledBars || 0
    const cardContainerStyle = props.cardContaierStyle || styles.cardContainer
    const { showProfileInfo = false, nameText, relationshipNoText, contactText, addressData, onEditClick = () => {}} = props

    return (
        <View style={[CommonStyle.cardContainerWithShadow, cardContainerStyle]}>

            <View style={headerContainerStyle}>
                {image ? <Image source={image} style={[imageStyle]} /> : null}
                {showProfileInfo
                    ? <CardInfoHeader
                        nameText={nameText}
                        relationshipNoText={relationshipNoText}
                        contactText={contactText}
                        addressData={addressData}
                        onEditClick={onEditClick}
                    />
                    : <Text style={[headerTextStyle]} >{headerText}</Text>
                }
            </View>

            {parseInt(totalBars) > 0 ? <ProgressBarsRow totalBars={totalBars} filledBars={filledBars} containerStyle={[styles.progressContainer, showProfileInfo && {marginTop: spacing.spacing20}]} /> : null}

            <View style={footerContainerStyle}>
                <Text style={footerTextStyle}>{footerText}</Text>
                {footerButtonText
                    ? <RightArrowButton image={Images.whiteArrow} text={footerButtonText}
                        textStyle={styles.progressCardButtonText} style={{ marginBottom: 0 }} 
                        imageStyle={{ marginLeft: 0, height: 24, width: 14 }}
                        onPress = {props.onPress}
                    />
                    : null}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    cardContainer: {
        margin: 0,
        backgroundColor: colors.blue
    },
    footerContainerStyle: {
        flexDirection: 'row'
    },
    footerTextStyle: {
        ...TextStyle.text_12_normal,
        color: colors.white,
    },
    imageTextContainer: {
        flexDirection: 'row',
        marginHorizontal: spacing.spacing12,
        marginTop: spacing.spacing20,
    },
    headerTextStyle: {
        ...TextStyle.text_16_bold,
        flexWrap: 'wrap',
        flexShrink: 1,
        color: colors.white,
        marginLeft: spacing.spacing16
    },
    progressContainer: {
        flexDirection: "row",
        width: "100%",
        marginTop: spacing.spacing36,
        marginBottom: spacing.spacing8
    },
    progressCardButtonText: {
        ...TextStyle.text_12_bold,
        color: colors.white
    },
    header: {
        ...TextStyle.text_16_bold,
        color: colors.white,
        flex: 1,
    },
    info: {
        ...TextStyle.text_12_normal,
        marginTop: spacing.spacing4,
        color: colors.white,
        flexShrink: 1,
        flexWrap: 'wrap'
    },
    rightImageTouchable: {
        marginTop: spacing.spacing_neg_5,
        width: spacing.spacing32,
        height: spacing.spacing32,
        // alignSelf: "flex-end"
    },
    rightImage: {
        tintColor: colors.white,
        alignSelf: 'center',
        margin: spacing.spacing16,
        width: spacing.spacing16,
        height: spacing.spacing16,
    }
})

export default CardWithProgressComponent