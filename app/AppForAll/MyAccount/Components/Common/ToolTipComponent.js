import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, Animated, Image, Dimensions, TouchableOpacity } from 'react-native'
import images from '../../../../images/index.image'
import { TextStyle } from '../../../../Constants/CommonStyle';
import colors from '../../../../Constants/colors';


const ToolTipComponent = props => {

    const containerWidth = props.containerWidth || (Dimensions.get('screen').width - 40)
    const leftImage = props.leftImage || images.bankLogo
    const rightImage = props.rightImage || images.rightWhiteArrow
    const text = props.text || "YOUR TEXT COMES HERE"
    const containerStyle = props.container
    const imageLeftStyle = props.imageLeftStyle
    const textStyle = props.textStyle
    const imageRightStyle = props.imageRightStyle
    const animationDuration = props.animationDuration || 2000
    const isAnimated = props.isAnimated || true
    const handleOnPress = props.onPress || null
    const [animationValue] = useState(new Animated.Value(0))

    useEffect(() => {
        isAnimated ? startAnimation() : null
        return (() => {
            animationValue.stopAnimation()
        })
    }, [animationValue])

    const startAnimation = () => {
        Animated.timing(animationValue, {
            delay: 2000,
            toValue: 1,
            duration: animationDuration
        }).start()
    }

    const width = animationValue.interpolate({
        inputRange: [0, 1],
        outputRange: [72, containerWidth]
    })

    const opacity = animationValue.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 1]
    })

    return (
        <View>
            <TouchableOpacity onPress={handleOnPress}>
                <Animated.View style={[styles.containerStyle, { width: width }, containerStyle]}>
                    <Image source={leftImage} style={[styles.leftImage, imageLeftStyle]} />
                    <Animated.Text adjustsFontSizeToFit={true} style={[styles.textStyle, { opacity: opacity }, textStyle]} numberOfLines={2}>{text}</Animated.Text>
                    <Animated.Image source={rightImage} style={[styles.rightImage, { opacity: opacity }, imageRightStyle]} />
                </Animated.View>
            </TouchableOpacity>
        </View>
    )
}

export default ToolTipComponent

const styles = StyleSheet.create({
    containerStyle: {
        bottom: 20,
        marginHorizontal: 8,
        backgroundColor: colors.charcoalGrey,
        paddingHorizontal: 16,
        paddingVertical: 20,
        borderRadius: 36,
        flexDirection: "row",
        justifyContent: 'space-between',
        position: 'absolute',
        alignItems: 'center',
        overflow: 'hidden',
        alignSelf: 'center',
        // height: 72,
    },
    leftImage: {
        height: 40,
        width: 40,
        resizeMode: 'contain'
    },
    rightImage: {
        height: 24,
        width: 24,
        resizeMode: 'contain'
    },
    textStyle: {
        ...TextStyle.text_14_bold,
        color: colors.white,
        flex: 1,
        flexWrap: 'wrap',
        marginHorizontal: 16,
    }
})
