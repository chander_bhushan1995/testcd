import React, {useEffect, useState} from 'react';
import {
    BackHandler,
    Image,
    NativeModules,
    Platform,
    SafeAreaView,
    ScrollView,
    SectionList,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import spacing from '../../../Constants/Spacing';
import {ButtonStyle, TextStyle} from '../../../Constants/CommonStyle';
import {questionTypes} from './QuestionsGenericComponent';
import colors from '../../../Constants/colors';
import ButtonWithLoader from '../../../CommonComponents/ButtonWithLoader';
import CheckboxWithText from '../../../CommonComponents/CheckboxWithText';
import InputBox from '../../../Components/InputBox';
import SearchBar from "./SearchBar";
import {AppForAppFlowStrings} from "../../Constant/AppForAllConstants";
import images from '../../../images/index.image';

// Send title, subtitle and style in the props
const HeaderView = props => {
    let subtitleTextView = props.subtitle ? (
        <Text style={[TextStyle.text_14_normal, {lineHeight: 22, marginTop: spacing.spacing8}]}>
            {props.subtitle}
        </Text>
    ) : null;

    return (
        <View style={props.style}>
            <Text
                style={[
                    TextStyle.text_20_bold, {lineHeight: 28},
                ]}>
                {props.title}
            </Text>
            {subtitleTextView}
        </View>
    );
};

// No need to send anything inside props except style.
const BankSearchView = props => {
    const searchBoxProps = {
        hint: AppForAppFlowStrings.hint_search_box,
        isEditable: false,
        showLeftImage: true
    };

    const onSearchClick = () => {
        props.navigation.navigate('SearchBank', {props})
    };
    //TODO: props.sectionWiseData contains data for both the sections i.e TOP and BOTTOM(others).
    // In order to see it's structure, visit QuestionAnswerResponseParser.js. This section data has been prepared based on the API response which
    // Ankur Batham is providing me.
    return (
        <View style={props.style}>
            <View style={{
                flexDirection: 'row',
            }}>
                <Image
                    source={images.pciImage}
                    style={{
                        marginTop: spacing.spacing8,
                        marginLeft: spacing.spacing8,
                        height: 16,
                        width: 36,
                    }}
                />
                <Text style={[TextStyle.text_12_regular, {
                    marginHorizontal: spacing.spacing8,
                    marginTop: spacing.spacing8,
                }]}>100% Safe and Secure | PCI-DSS Compliant</Text>
            </View>

            <TouchableOpacity style={styles.searchBarContainer} onPress={onSearchClick}>
                <SearchBar data={searchBoxProps}/>
            </TouchableOpacity>
        </View>
    );
};

// Send title, subtitle, questionType, dataForSectionList, buttonText, isLoading
const PreBasicDetailsQuestionsScreen = props => {
    let title = props.title;
    if(props.questionType == questionTypes.AssetPE) {
        title = props.title.replace("{{model_name}}",`${props.currentBrand}`)
    }

    let headerView = <HeaderView
        style={{marginLeft: spacing.spacing16, marginTop: spacing.spacing32, marginRight: spacing.spacing16}}
        title={title} subtitle={props.subtitle}/>;

    let bankSearchView = props.questionType === questionTypes.AssetF ? (
        <BankSearchView style={{marginLeft: spacing.spacing16, marginTop: spacing.spacing16}}
                        sectionWiseData={props.dataForSectionList}
                        navigation={props.navigation}
                        onPressButton={props.onPressButton}

        />) : null;

    return (
        <View style={[styles.MainContainer]}>
            <ScrollView style={{marginBottom: spacing.spacing48}}>
                {headerView}
                {bankSearchView}
                <View style={styles.sectionListStyle}>
                    <SectionList
                        scrollEnabled={false}
                        SectionSeparatorComponent={() =>
                            <View style={styles.horizontalLineCompleteView}/>
                        }
                        ItemSeparatorComponent={() =>
                            <View style={styles.horizontalLineCompleteView}/>
                        }
                        sections={props.dataForSectionList}
                        renderItem={({item, index, section}) => <CheckboxWithText
                            onPressCheckbox={props.onPressCheckbox}
                            section={section}
                            row={index}
                            item={item}
                            currentBrand={props.currentBrand}
                            currentModel={props.currentModel}/>}
                        renderSectionHeader={({section}) => {
                            let headerStyle = (section.sectionTitle != null) ? styles.sectionHeader : null;
                            return <Text style={headerStyle}>{section.sectionTitle}</Text>;
                        }}
                        keyExtractor={(item, index) => index}
                    />
                </View>
            </ScrollView>
            <View style={{
                bottom: spacing.spacing0,
                position: 'absolute',
                width: '100%',
                height: 80,
                backgroundColor: colors.white,
            }}>
                <View style={styles.buttonStyle}>
                    <ButtonWithLoader
                        isLoading={props.isLoading}
                        enable={(!props.isLoading) && !props.isDisabled}
                        Button={{
                            text: props.buttonText,
                            onClick: () => {
                                props.onPressButton();
                            },
                        }}/>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    MainContainer: {
        backgroundColor: colors.white,
        flex: 1,
    },
    buttonStyle: {
        marginHorizontal: spacing.spacing16,
        marginTop: spacing.spacing16,
        height: 48,
    },
    sectionListStyle: {
        marginTop: spacing.spacing20,
        marginBottom: spacing.spacing48,
    },
    sectionHeader: {
        marginLeft: spacing.spacing18,
        marginTop: spacing.spacing32,
        marginBottom: spacing.spacing8,
        color: colors.grey,
        fontSize: 12,
        fontFamily: 'Lato-Bold',
        lineHeight: 18,
    },
    horizontalLineCompleteView: {
        flexDirection: 'row',
        borderBottomColor: colors.color_E0E0E0,
        borderBottomWidth: StyleSheet.hairlineWidth,
        alignSelf: 'stretch',
        height: spacing.spacing1,
    },
    searchBarContainer: {
        marginRight: spacing.spacing16,
        marginTop: spacing.spacing36,
    },
    cancelButtonContainer: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginLeft: spacing.spacing16,
    },
    cancelButton: {
        paddingTop: spacing.spacing16,
        paddingBottom: spacing.spacing16,
        paddingLeft: spacing.spacing12,
        paddingRight: spacing.spacing16,
    },
});

export default PreBasicDetailsQuestionsScreen;



