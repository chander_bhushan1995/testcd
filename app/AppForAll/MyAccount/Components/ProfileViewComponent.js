import React from 'react'
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import spacing from "../../../Constants/Spacing";
import colors from '../../../Constants/colors';
import {TextStyle} from '../../../Constants/CommonStyle';
import Images from '../../../images/index.image';
import ButtonWithLoader from '../../../CommonComponents/ButtonWithLoader';

const ProfileViewComponent = props => {
    const leftImage = props.leftImage || Images.profilePlaceholder
    const header = props.headerText || ''
    const description = props.descriptionText || ''
    const subDescription = props.subDescriptionText || ''
    const rightImage = props.rightImage || Images.ic_edit_transparent
    const onPress = props.onPress
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={onPress}>
                <View style={styles.profileDataContainer}>
                    <Image source={leftImage}/>
                    <View style={{marginLeft: spacing.spacing12, flex: 1}}>
                        <Image source={rightImage} style={styles.rightImage}/>
                        <Text style={styles.header}>{header}</Text>
                        <Text style={styles.description}>{description}</Text>
                        <Text style={styles.subDescription}>{subDescription}</Text>
                        {props.addressData?.trim().length === 0 ? null :
                            <Text style={styles.subDescription}>{props.addressData}</Text>}
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {},
    topSection: {
        ...TextStyle.text_12_bold,
        marginTop: spacing.spacing24,
        marginHorizontal: spacing.spacing16,
        marginBottom: spacing.spacing8,
    },
    profileDataContainer: {
        paddingTop: spacing.spacing34,
        paddingBottom: spacing.spacing34,
        paddingHorizontal: spacing.spacing16,
        flexDirection: 'row',
    },
    header: {
        marginTop: spacing.spacing_n_20,
        ...TextStyle.text_16_bold,
        flexShrink: 1,
        flexWrap: 'wrap',
    },
    description: {
        ...TextStyle.text_12_normal,
        marginTop: spacing.spacing4,
        color: colors.color_888F97,
        flexShrink: 1,
        flexWrap: 'wrap'
    },
    subDescription: {
        ...TextStyle.text_12_normal,
        color: colors.color_888F97,
        flexShrink: 1, flexWrap: 'wrap'
    },
    rightImage: {
        alignSelf: 'flex-end',
        width: spacing.spacing18,
        height: spacing.spacing18,
    }
})

export default ProfileViewComponent
