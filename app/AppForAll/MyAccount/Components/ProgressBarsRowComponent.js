import React,{useState} from 'react'
import { View, StyleSheet } from 'react-native'
import colors from '../../../Constants/colors'
import spacing from '../../../Constants/Spacing'


const ProgressBarsRowComponent = props => {

    const totalBars = parseInt(props.totalBars || 0)
    const filledBars = parseInt(props.filledBars || 0)
    const containerStyle = props.containerStyle || styles.containerStyle
    const filledBarStyle = props.filledBarStyle || styles.filledBarStyle
    const unFilledBarStyle = props.unFilledBarStyle || styles.unFilledBarStyle
    
    const SINGLE_BAR_MAX_WIDTH = 0
    const UNFILLED_BARS  = totalBars - filledBars
    const [barMAXWidth, setBarMAXWidth] = useState(0)

    return (
        <View style={containerStyle}>

            {Array.from(Array(Math.floor(filledBars > totalBars ? totalBars : filledBars)), (value, key) => {
                return <View style={[styles.singleBarStyle,unFilledBarStyle,{flex: 1 / totalBars}]} key={key}><View style={[styles.singleBarStyle,filledBarStyle,{flex:1,marginRight:0}]} /></View>
            })}
            {Array.from(Array(Math.floor(UNFILLED_BARS > totalBars ? totalBars : UNFILLED_BARS)), (value, key) => {
                return <View style={[styles.singleBarStyle,unFilledBarStyle,{flex: 1 / totalBars}]} key={key}></View>
            })}

        </View>
    )
}

const styles = StyleSheet.create({
    containerStyle: {
        flexDirection: "row"
    },
    singleBarStyle: {
        borderRadius: 2,
        overflow: "hidden",
        height: 4,
        marginRight: spacing.spacing10,
    },
    filledBarStyle: {
        backgroundColor: colors.white
    },
    unFilledBarStyle: {
        backgroundColor: colors.blue028
    }
})

export default ProgressBarsRowComponent