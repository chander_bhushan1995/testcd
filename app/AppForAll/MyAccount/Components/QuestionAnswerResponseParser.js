import {questionTypes} from './QuestionsGenericComponent';
import { AppForAppFlowStrings } from '../../Constant/AppForAllConstants';

export default class QuestionAnswerResponseParser {

    questionAnswerDataSourceFromResponse(questionAnswer, currentBrand, currentModel) {
        const parsedDataSourceFromResponse = [];

        for (let i = 0; i < questionAnswer.length; i++) {
            let questionAndOptions = questionAnswer[i];
            parsedDataSourceFromResponse.push({
                questionId: questionAndOptions.questionId,
                questionText: questionAndOptions.questionText,
                questionSubtext: questionAndOptions.subText,
                questionCode: questionAndOptions.questionCode,
                sectionData: this.prepareDataForSectionList(questionAndOptions.optionList.sort((obj1, obj2) => {
                    return (obj1.displayType > obj2.displayType) ? -1 : ((obj1.displayType < obj2.displayType) ? 1 :
                        (obj1.selected ? (obj2.selected ? (obj1.optionRank - obj2.optionRank) : -1) :
                            (obj2.selected ? 1 : (obj1.optionRank - obj2.optionRank))));
                }), questionAndOptions.otherAnswerList, questionAndOptions.questionCode, currentBrand, currentModel),
            });
        }

        return parsedDataSourceFromResponse;
    }

    prepareDataForSectionList(sortedOptionList, otherAnswerList, questionCode, currentBrand, currentModel) {
        // Make use of questionCode for sectionTitle
        let sectionTitle = questionCode === questionTypes.AssetHA ? AppForAppFlowStrings.onboardingQuestions.otherAppliance : AppForAppFlowStrings.onboardingQuestions.others;
        let dataForSectionList = [{sectionTitle: null, data: [], sectionIndex: 0}, {
            sectionTitle: sectionTitle,
            data: [],
            sectionIndex: 1,
        }];
        sortedOptionList.forEach((item) => {
            let insertData = item.displayType === 'TOP' ? dataForSectionList[0].data : dataForSectionList[1].data;
            insertData.push({
                optionId: item.optionId,
                optionText: item.optionText,
                isSelected: item.selected,
                isEditable: true,
                isOther: false,
            });
        });

        let insertData = dataForSectionList[0].data;
        otherAnswerList.forEach((item) => {
            if (!((currentBrand === item.brand) && (currentModel === item.model))) {
                insertData.unshift({
                    optionId: Math.round(Math.random() * 10000),
                    optionText: item.brand + ' ' + item.model,
                    isSelected: true,
                    isEditable: true,
                    isOther: true,
                });
            }
        });

        if (questionCode === questionTypes.AssetPE) {
            insertData.unshift({
                optionId: Math.floor(Math.random() * 10000),
                optionText: currentBrand + ' ' + currentModel,
                isSelected: true,
                isEditable: false,
                isOther: true,
            });
        }

        if (dataForSectionList[1].data.length == 0) {
            delete dataForSectionList[1];
        }

        return dataForSectionList;
    }
}
