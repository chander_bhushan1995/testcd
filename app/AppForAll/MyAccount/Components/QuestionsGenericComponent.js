import React, {useEffect} from 'react';
import {
    BackHandler,
    Image,
    NativeModules,
    Platform,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import {TextStyle} from '../../../Constants/CommonStyle';
import colors from '../../../Constants/colors';
import BasicProfileQuestionsScreen from './BasicProfileQuestionsComponent';
import PreBasicDetailsQuestionsScreen from './PreBasicDetailsQuestionsComponent';
import DialogView from '../../../Components/DialogView';
import {store} from '../../../Navigation/index.userregistration';
import * as Actions from '../../../Constants/ActionTypes';
import images from '../../../images/index.image';
import {AppForAppFlowStrings} from '../../Constant/AppForAllConstants';
import WebEngage from "react-native-webengage";
import {ONBOARDING_SKIP} from "../../../Constants/WebengageEvents";
import {logWebEnageEvent} from '../../../commonUtil/WebengageTrackingUtils';
import { parseJSON, deepCopy} from "../../../commonUtil/AppUtils";

const nativeBridgeRef = NativeModules.ChatBridge;
let DialogViewRef;

/*
CB:- these variable are used to mantain back button of navigation bar to show previous question
*/
let globalIndex = 0
let globalCurrentScreenNo = 0
let globalCurrentOptionsList = []
let showAlertForLogout = () => {
}

export const questionTypes = {
    AssetPE: 'ASSET_PE',
    AssetHA: 'ASSET_HA',
    AssetF: 'ASSET_F',
};

export const flowTypes = {
    CompleteProfile: 'CompleteProfile',
    UnlockReward: 'UnlockReward',
    Edit: 'Edit',
    GeneralCompleteProfile: 'GeneralCompleteProfile',
};

function getButtonText(props) {
    let buttonText = 'Next';
    if (props.flowType === flowTypes.Edit) {
        // It will always be the last screen
        buttonText = 'Save changes';
    } else if ((props.flowType === flowTypes.GeneralCompleteProfile) && (props.index === props.questionAnswer.length &&
        (props.name && props.email))) {
        // In case of CompleteProfile case when basic details are submitted
        buttonText = 'Save details';
    }
    return buttonText;
}

const getCurrentScreen = (props) => {

    globalIndex = props.index
    globalCurrentScreenNo = props.currentScreenNo
    globalCurrentOptionsList = props.currentOptionList

    if ((props.flowType === flowTypes.Edit && !props.questionAnswer.length) ||
        ((props.flowType === flowTypes.CompleteProfile || props.flowType === flowTypes.UnlockReward || props.flowType === flowTypes.GeneralCompleteProfile) && (props.index > props.questionAnswer.length) && (!props.name ||
            !props.email))) {

        let buttonText = (props.flowType === flowTypes.UnlockReward || props.flowType === flowTypes.CompleteProfile) ? 'Unlock rewards' : 'Save details';
        return <BasicProfileQuestionsScreen buttonText={buttonText} isLoading={props.isLoading} name={props.name}
                                            email={props.email} onPressSubmitDetails={(name, email) => {
            props.saveNameAndEmail(name, email, props.custId);
        }}/>;
    }

    if (props.index <= props.questionAnswer.length) {
        // Logic is written as per Punnet and Krishna discussion
        let buttonText = getButtonText(props);

        let questionAnswer = props.dataSourceQuestionsScreen[props.index - 1];

        let isDisabled = questionAnswer.sectionData.filter((section) =>
            section.data.filter((item) => item.isSelected).length > 0).length <= 0;

        return <PreBasicDetailsQuestionsScreen title={questionAnswer.questionText}
                                               subtitle={questionAnswer.questionSubtext}
                                               questionType={questionAnswer.questionCode}
                                               isDisabled={isDisabled}
                                               isLoading={props.isLoading}
                                               buttonText={buttonText}
                                               dataForSectionList={questionAnswer.sectionData}
                                               currentBrand={props.currentBrand}
                                               currentModel={props.currentModel}

                                               onPressCheckbox={(section, row) => {
                                                   props.changeStateOfCheckbox(section.sectionIndex, row, props.dataSourceQuestionsScreen, props.index, props.currentOptionList);
                                               }}
                                               onPressButton={() => {
                                                   // This is required for re-render of navigation bar.
                                                   let appData = store.getState().appData;
                                                   appData.currentOptionList = [];
                                                   props.saveAnsweredQuestionsData(questionAnswer, props.index, props.currentScreenNo, props.navigation, props.custId, props.currentOptionList, false, props.flowType);
                                               }}
                                               navigation={props.navigation}
        />;
    }

    return null;
};

// NOTE: You will call this only when something is pending from user's side.
const QuestionsGenericScreen = props => {
    showAlertForLogout = (title, alertMessage, btnText) => {
        DialogViewRef.showDailog({
            title: title,
            message: alertMessage,
            primaryButton: {
                text: btnText, onClick: () => {
                }
            },
            cancelable: true,
            onClose: () => {
            }
        });
    }

    const showAlertForSaveChanges = (title, alertMessage, text, isSkipped, isBackPressed) => {
        if (DialogViewRef !== null && DialogViewRef !== undefined) {
            DialogViewRef.showDailog({
                title: title,
                message: alertMessage,
                primaryButton: {
                    text: text, onClick: () => {
                        if (isSkipped) {
                            goBack(isSkipped, false)
                        } else {
                            let appData = store.getState().appData;

                            if (appData.index > appData.dataSourceQuestionsScreen.length) {
                                let email = appData.currentEmail ? appData.currentEmail : props.email;
                                let name = appData.currentName ? appData.currentName : props.name;

                                props.saveNameAndEmail(name, email, props.custId)
                            } else {
                                let questionAnswer = appData.dataSourceQuestionsScreen[appData.index - 1];
                                props.saveAnsweredQuestionsData(questionAnswer, appData.index, appData.currentScreenNo, props.navigation, props.custId, appData.currentOptionList, true, props.flowType);
                            }
                        }
                    }
                },
                cancelable: true,
                onClose: () => {
                }
            });
        }
    };

    const onHardwareBackPress = () => {
        let state = store.getState();
        onBackPress(state.appData.currentOptionList, false, true, showAlertForSaveChanges, props.navigation);
        return true;
    }

    useEffect(() => {
        props.prepareDataSourceFromResponse(props.questionAnswer, props.currentBrand, props.currentModel, props.flowType, props.name, props.email);
        props.navigation.setParams({showAlertRef: showAlertForSaveChanges});
        BackHandler.addEventListener("hardwareBackPress", onHardwareBackPress);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', onHardwareBackPress)
        }

    }, [props.prepareDataSourceFromResponse]);

    if (!props.isPopulated) {
        return (<View/>);
    } else {
        // props.navigation.setParams({showAlertRef: showAlertForSaveChanges});
    }

    if (props.type === Actions.API_FAILED) {
        let errorMessage = store.getState().appData.errorMessage;
        showAlertForAPI(
            'Alert', errorMessage,
        );
    }

    let currentScreen = null;
    if (props.type === Actions.NAME_EMAIL_SUBMITTED || ((currentScreen = getCurrentScreen(props)) == null)) {
        //TODO: Move to another screen. Here you navigate to the next screen.
        nativeBridgeRef.sendCustomerDetailsUpdatedEvent();
        goBack(false, false);
    }

    // const handleAndroidBackBtn = () => {
    //     onBackPress(state.appData.currentOptionList, false, true, showAlertForSaveChanges, props.navigation);
    // }

    let progressBarView = (props.flowType === flowTypes.CompleteProfile || props.flowType === flowTypes.GeneralCompleteProfile || props.flowType === flowTypes.UnlockReward) ?
        <View style={{backgroundColor: colors.color_E0E0E0, height: 4, width: '100%'}}><View
            style={{
                backgroundColor: colors.blue028,
                height: 4,
                width: `${props.currentScreenNo / (props.totalScreens) * 100}%`,
            }}/></View> : null;

    return (
        <SafeAreaView style={[styles.MainContainer]}>
            {progressBarView}
            {currentScreen}
            <DialogView ref={ref => (DialogViewRef = ref)}/>
        </SafeAreaView>
    );
};

function showAlertForAPI(title, alertMessage) {
    if (DialogViewRef !== null && DialogViewRef !== undefined) {
        DialogViewRef.showDailog({
            title: title,
            message: alertMessage,
            primaryButton: {
                text: 'Ok',
            },
            cancelable: true,
        });
    }
};

QuestionsGenericScreen.navigationOptions = navigationData => {

    if (navigationData.navigation.state.params.quit) {
        goBack(false, true)
        return;
    }

    let state = store.getState();
    let flowType = state.appData.flowType;

    let title = 'Complete profile';
    if (flowType === flowTypes.UnlockReward || flowType == flowTypes.CompleteProfile) {
        title = 'Unlock rewards';
    }

    let navigationView = null;
    let headerRight = null;
    if (flowType === flowTypes.Edit) {

        // NOTE: There will never be edit case of Basic Profile
        let questionAnswerParsed = parseJSON(state.appData.questionAnswer).data;
        let whoseEdit = questionAnswerParsed[0].questionCode;

        if (whoseEdit === questionTypes.AssetPE) {
            title = 'My Gadgets';
        } else if (whoseEdit === questionTypes.AssetHA) {
            title = 'My Appliances';
        } else if (whoseEdit === questionTypes.AssetF) {
            title = 'Banks I transact with';
        }

        navigationView = (<View
            style={
                Platform.OS === 'ios'
                    ? {alignItems: 'center'}
                    : {alignItems: 'flex-start'}
            }
        >
            <Text style={[TextStyle.text_16_bold, {lineHeight: 24}]}>{title}</Text>
        </View>);
    } else {
        if (flowType === flowTypes.UnlockReward || flowType === flowTypes.GeneralCompleteProfile) {
            headerRight = (<TouchableOpacity style={styles.NavigationBarRightButton}
                                             onPress={() => {
                                                 onBackPress(state.appData.currentOptionList, true, false, navigationData.navigation.getParam('showAlertRef'), navigationData.navigation);
                                             }}>
                <Text style={[TextStyle.text_12_medium, {color: colors.grey, lineHeight: 20}]}>SKIP</Text>
            </TouchableOpacity>);
        }

        navigationView = (
            <View>
                <View style={Platform.OS === 'ios' ? {alignItems: 'center'} : {alignItems: 'flex-start'}}>
                    <Text style={styles.headerTitle}>{title}</Text>
                    <Text
                        style={styles.headerSubtitle}>{`STEP ${state.appData.currentScreenNo} OF ${state.appData.totalScreens}`}</Text>
                </View>
            </View>
        );
    }

    return {
        headerTitle: navigationView,
        headerLeft: (
            <TouchableOpacity onPress={
                () => {
                    onBackPress(state.appData.currentOptionList, false, true, navigationData.navigation.getParam('showAlertRef'), navigationData.navigation);
                }
            }>
                <Image
                    source={images.backImage}
                    style={{
                        height: 30,
                        width: 30,
                        marginLeft: 9,
                        marginRight: 12,
                        marginVertical: 12,
                        resizeMode: 'contain',
                    }}
                />
            </TouchableOpacity>
        ),
        headerRight: headerRight,
        headerTitleStyle: {color: colors.white},
        headerTintColor: colors.white,
    };
};

function handleShowPrevQuesOnBack(flowType, navigation) { // handle back button of navigation bar to show previous question
    if (globalIndex > 1) {
        store.dispatch({
            type: Actions.SUBMIT_PRESSED,
            index: globalIndex - 2,
            currentScreenNo: globalCurrentScreenNo - 2,
            isLoading: false,
        });
        navigation.setParams({currentOptionList: globalCurrentOptionsList, quit: false});
    }
}

function onBackPress(currentOptionList, isSkipped, isBackPressed, showAlertRef, navigation) {
    let appData = store.getState().appData;
    let flowType = appData.flowType;

    let currentEmail = store.getState().appData.currentEmail;
    let currentName = store.getState().appData.currentName;
    let questionAnswer = {};

    if (flowType !== flowTypes.Edit && !isSkipped) { // handle back button of navigation bar to show previous question
        handleShowPrevQuesOnBack(flowType, navigation)
        if (globalIndex > 1)
            return
    }

    if (appData.index <= appData.dataSourceQuestionsScreen.length) {
        questionAnswer = appData.dataSourceQuestionsScreen[appData.index - 1];
    }

    if (((flowType === flowTypes.GeneralCompleteProfile || flowType === flowTypes.CompleteProfile || flowType === flowTypes.UnlockReward)
        && ((appData.index > appData.dataSourceQuestionsScreen.length) ? (currentEmail && currentName) : (questionAnswer.questionCode === questionTypes.AssetPE)))
        || currentOptionList.length) {
        if (!isSkipped) {
            if(!(flowType === flowTypes.Edit && questionAnswer.sectionData.filter((section) =>
                section.data.filter((item) => item.isSelected).length > 0).length <= 0)){
                showAlertRef(AppForAppFlowStrings.onboardingQuestions.wantSaveChanges, AppForAppFlowStrings.onboardingQuestions.pressSaveToUpdate, AppForAppFlowStrings.onboardingQuestions.saveChanges, isSkipped, isBackPressed);
                return
            }else if (flowType === flowTypes.Edit){
                goBack(isSkipped, isBackPressed);
                return
            }
            showAlertRef(AppForAppFlowStrings.onboardingQuestions.wantSaveChanges, AppForAppFlowStrings.onboardingQuestions.pressSaveToUpdate, AppForAppFlowStrings.onboardingQuestions.saveChanges, isSkipped, isBackPressed);
        } else {
            showAlertRef(AppForAppFlowStrings.onboardingQuestions.wantToSkip, AppForAppFlowStrings.onboardingQuestions.unlockRewardsLater,
                AppForAppFlowStrings.onboardingQuestions.skipToHome, isSkipped, isBackPressed);
        }
    } else {
        goBack(isSkipped, isBackPressed);
    }
};

function goBack(isSkipped, isBackPressed) {
    let flowType = store.getState().appData.flowType;
    if (flowType == flowTypes.UnlockReward) {
        nativeBridgeRef.gotoTabbar(false, false, true);
        if (isSkipped) {
            logWebEnageEvent(ONBOARDING_SKIP);
        }
    } else {
        if (store.getState().appData.isDataChanged) {
            nativeBridgeRef.updateProfile()
        }
        nativeBridgeRef.goBack();
    }
}


const styles = StyleSheet.create({
    MainContainer: {
        backgroundColor: colors.white,
        flex: 1,
    },
    headerTitle: {
        fontSize: 16,
        fontFamily: 'Lato',
        fontWeight: 'bold',
        color: colors.charcoalGrey,
    },
    headerSubtitle: {
        fontSize: 10,
        fontFamily: 'Lato',
        fontWeight: 'normal',
        color: colors.grey,
    },
    NavigationBarRightButton: {
        paddingRight: 10,
    },
});

export default QuestionsGenericScreen;
