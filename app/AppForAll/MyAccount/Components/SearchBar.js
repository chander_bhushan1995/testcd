import { StyleSheet, View, Text, Image, TextInput, TouchableOpacity } from 'react-native'
import React, { useState } from "react";
import colors from "../../../Constants/colors";
import { TextStyle } from "../../../Constants/CommonStyle"
import spacing from "../../../Constants/Spacing";
import images from '../../../images/index.image';

export default function SearchBar(props) {
    const { style, data } = props;
    const [text, setText] = useState('');

    const onCancelClick = () => {
        setText('');
        data.onTextUpdate("");
    };


    const onTextChange = (text) => {
        setText(text);
        data.onTextUpdate(text);
    };


    return (
        <View style={[styles.MainContainer, style]}>
            {
                data.isEditable
                    ? <TextInput style={styles.inputStyle}
                        placeholder={data.hint}
                        onChangeText={onTextChange}
                        value={text}
                        editable={data.isEditable} 
                        autoFocus={data.autoFocus}
                        />
                    : <View style={[styles.inputStyle,{justifyContent: 'center'}]}>
                        <Text style={styles.textStyle}>
                            {data.hint}
                        </Text>
                    </View>
            }

            {text ? <TouchableOpacity onPress={onCancelClick} style={styles.imageViewStyle}>
                <Image source={images.ic_clearSearch} style={styles.imageStyle} />
            </TouchableOpacity> : null}

        </View>
    );
}

const styles = StyleSheet.create({
    MainContainer: {
        flexDirection: 'row',
        borderRadius: 4,
        borderColor: colors.color_888F97,
        borderWidth: 1,
        width: "100%",
        paddingHorizontal: spacing.spacing12,
        alignItems: 'center',
    },
    textStyle: {
        ...TextStyle.text_14_bold,
        color: colors.grey
    },
    inputStyle: {
        ...TextStyle.text_14_bold,
        flex: 1,
        height: 48
    },

    imageStyle: {
        width: 24,
        height: 24,
        margin: spacing.spacing3,
    }
});
