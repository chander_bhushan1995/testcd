import {FlatList, Image, StyleSheet, TouchableOpacity, View, Alert, BackHandler, Platform} from 'react-native'
import CheckboxWithText from '../../../CommonComponents/CheckboxWithText'
import React, {useEffect, useState} from "react";
import colors from "../../../Constants/colors";
import spacing from "../../../Constants/Spacing";
import SearchBar from "./SearchBar";
import { AppForAppFlowStrings } from '../../Constant/AppForAllConstants'
import { store } from '../../../Navigation/index.userregistration'
import * as Actions from "../../../Constants/ActionTypes";
import { isValidString } from "../../../commonUtil/Validator";
import ButtonWithLoader from "../../../CommonComponents/ButtonWithLoader";
import { flowTypes } from "./QuestionsGenericComponent";
import images from '../../../images/index.image';

const SelectBankScreen = (props) => {
    const { navigation } = props;
    const { appData } = store.getState();
    const [state, setState] = useState([]);
    const previousProps = props.navigation.getParam("props")
    const { sectionWiseData } = previousProps;
    const data = prepareData(sectionWiseData);

    let isDisabled = sectionWiseData.filter((section) =>
            section.data.filter((item) => item.isSelected).length > 0).length <= 0;

    useEffect(() => { //load first time data
        BackHandler.addEventListener("hardwareBackPress", ()=>onBackPress(navigation));
        return () => {
                // BackHandler.removeEventListener(onBackPress)
        }
    }, [])

    const onTextUpdate = (text) => {
        let filteredData = [];
        if (isValidString(text)) {
            filteredData = data.filter((item) => {
                var regex = new RegExp('^'+text, "i");
                return regex.test(item.optionText)
            });
        }
        setState(filteredData);
    };

    const onItemPressed = (section, row) => {
        let itemInAction = state[row];
        let filteredData = state.map(value => {
            let item = { ...value };
            if (item.optionId === itemInAction.optionId) {
                item.isSelected = !item.isSelected;
            }
            return item;
        });

        let indexInSection = section.data.map(value => value.optionId).indexOf(itemInAction.optionId);
        setState(filteredData);
        store.dispatch({
            type: Actions.CHANGE_STATE_OF_CHECKBOX,
            column: section.sectionIndex,
            row: indexInSection,
            dataSourceQuestionsScreen: appData.dataSourceQuestionsScreen,
            index: appData.index,
            currentOptionList: appData.currentOptionList,
        });
    };

    const onBackPress = (navigation) => {
        navigation.pop();
        return true;
    };

    const onActionButtonClick = () => {
        previousProps.onPressButton();
        onBackPress(navigation);
    };

    const searchBoxProps = {
        onTextUpdate: onTextUpdate,
        hint: AppForAppFlowStrings.hint_search_box,
        isEditable: true,
        showLeftImage: false,
        autoFocus: true
    };

    return (
        <View style={styles.mainContainer}>
            <View style={{ padding: spacing.spacing16 }}>
                <SearchBar data={searchBoxProps} style={{ marginTop: spacing.spacing16 }} />
            </View>
            <FlatList style={{ marginTop: 40, flex: 1 }}
                data={state}
                renderItem={({ index, item }) => {
                    return <CheckboxWithText item={item}
                        section={sectionWiseData[item.sectionIndex]}
                        onPressCheckbox={onItemPressed}
                        row={index}
                        currentBrand={appData.currentBrand}
                        currentModel={appData.currentModel}
                    />
                }}
                ItemSeparatorComponent={() =>
                    <View style={styles.LineSeparatorStyle} />
                }
                keyExtractor={item => item.optionId}
            />
            <View style={styles.actionButtonStyle}>
                <ButtonWithLoader
                    isLoading={appData.isLoading}
                    enable={!isDisabled}
                    Button={{
                        text: getButtonText(),
                        onClick: onActionButtonClick,
                    }} />
            </View>

        </View>
    );
};

const BackButton = ({ onPress }) => (
    <TouchableOpacity onPress={
        () => {
            onPress()
        }
    }>
        <Image
            source={images.backImage}
            style={{
                height: 30,
                width: 30,
                marginLeft: 9,
                marginRight: 12,
                marginVertical: 12,
                resizeMode: 'contain',
            }}
        />
    </TouchableOpacity>
);


SelectBankScreen.navigationOptions = (navigationData) => {
    return {
        // mode: 'modal',
        headerStyle: {
            borderBottomWidth: 0,
            elevation: 0,
        },
        headerLeft: <BackButton onPress={() => {
            navigationData.navigation.pop()
        }} />
    };
};



function getButtonText() {
    if (store.getState().appData.flowType === flowTypes.GeneralCompleteProfile) {
        return AppForAppFlowStrings.textCompleteProfile;
    } else if (store.getState().appData.flowType === flowTypes.Edit) {
        return AppForAppFlowStrings.textEdit;
    } else {
        return AppForAppFlowStrings.textUnlockRewards;
    }
}

function prepareData(sectionData) {
    let data = [];
    for (let item of sectionData) {
        for (let subItem of item.data) {
            subItem.sectionIndex = item.sectionIndex;
            data.push(subItem);
        }
    }
    return data;
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colors.white,
    },
    backImageStyle: {
        width: 24,
        height: 24,
        resizeMode: 'center'
    },
    toolbarViewStyle: {
        height: 56,
        width: 56
    },
    LineSeparatorStyle: {
        flexDirection: 'row',
        borderBottomColor: colors.color_E0E0E0,
        borderBottomWidth: StyleSheet.hairlineWidth,
        alignSelf: 'stretch',
        height: spacing.spacing1,
    },
    actionButtonStyle: {
        margin: spacing.spacing16,
        height: 48,
    },
});
export default SelectBankScreen
