import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native';
import spacing from "../../../Constants/Spacing";
import colors from '../../../Constants/colors';
import { TextStyle } from '../../../Constants/CommonStyle';
import Images from '../../../images/index.image';
import ButtonWithLoader from '../../../CommonComponents/ButtonWithLoader';

const VerifyMobileComponent = props => {
    const headerText = props.headerText || ''
    const descriptionText = props.descriptionText || ''
    const buttonText = props.buttonText || ''
    const onClick = props.onClick || null
    return (
        <View style={styles.verifyMobileContainer}>
            <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1, justifyContent: 'flex-start' }}>
                    <Text style={[TextStyle.text_20_normal]}>{headerText}</Text>
                    <View style={{ height: 2, width: "5%", backgroundColor: colors.blue, marginVertical: 12 }} />
    <Text style={[TextStyle.text_16_normal, { color: colors.color_888F97 }]}>{descriptionText}</Text>
                </View>
                <Image style={{ justifyContent: 'flex-end',flexDirection: "row" }} source={Images.reward} />
            </View>
            <View style={{ marginTop: spacing.spacing36 }}><ButtonWithLoader Button={{ text: buttonText, onClick: onClick}} enable={true}/></View>
        </View>
    )
}

const styles = StyleSheet.create({

    verifyMobileContainer: {
        marginTop: spacing.spacing32,
        marginBottom: spacing.spacing36,
        marginHorizontal: spacing.spacing16
    },
})

export default VerifyMobileComponent
