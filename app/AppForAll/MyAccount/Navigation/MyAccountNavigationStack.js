import { createStackNavigator } from "react-navigation";
import MyAccountScreen from '../Actions/MyAccountActions';
import colors from "../../../Constants/colors";

const navigator = createStackNavigator({
    MyAccountScreen: {
        screen: MyAccountScreen,
        navigationOptions: {
            gesturesEnabled: false,
            title: "Account",
            headerTitleStyle: {
                color: colors.charcoalGrey,
            },
            header: null,
            headerTintColor: colors.blue,
        }
    },

}, {
});

export default navigator;
