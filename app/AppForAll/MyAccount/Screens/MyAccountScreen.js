import React, {useContext, useEffect, useState, useReducer} from 'react';
import {
    Dimensions,
    Image,
    NativeEventEmitter,
    NativeModules,
    Platform,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    Text,
    View,
} from 'react-native';
import ClickableCell from '../../../CustomComponent/customcells/clickableCell/ClickableCell';
import spacing from '../../../Constants/Spacing';
import {AppForAppFlowStrings} from '../../Constant/AppForAllConstants';
import Colors from '../../../Constants/colors';
import colors from '../../../Constants/colors';
import LinkButton from '../../../CustomComponent/LinkButton';
import {TextStyle} from '../../../Constants/CommonStyle';
import CardWithProgress from '../Components/CardWithProgressComponent';
import Images from '../../../images/index.image';
import VerifyMobileComponent from '../Components/VerifyMobileComponent';
import ProfileViewComponent from '../Components/ProfileViewComponent';
import {MyAccountBtnActions, WebViewFor} from '../Actions/MyAccountButtonActions';
import SkeletonContent from '../../../lib/src/index';
import DialogView from '../../../Components/DialogView';
import {Deeplink} from '../../../Constants/Deeplinks';
import BlockingLoader from '../../../CommonComponents/BlockingLoader';
import useLoader from '../../../CardManagement/CustomHooks/useLoader';
import {parseJSON, deepCopy, getrewardOnStatus} from '../../../commonUtil/AppUtils';

import {COMPLETE_PROFILE, PROFILE} from '../../../Constants/WebengageEvents';
import {OATextStyle} from '../../../Constants/commonstyles';
import {logWebEnageEvent} from '../../../commonUtil/WebengageTrackingUtils';
import {PLATFORM_OS} from '../../../Constants/AppConstants';
import SnackBarComponent from '../../MyProfile/Components/SnackBarComponent'
import {APIData, updateAPIData} from '../../../../index';
import { RootContext, localEventEmitter } from "../../../TabBar/index.tabbar";
import * as APIHelper from "../../../network/APIHelper";
import useForceUpdate from '../../../CardManagement/CustomHooks/useForceUpdate';

const nativeBridgeRef = NativeModules.ChatBridge;
const eventEmitter = new NativeEventEmitter(nativeBridgeRef);

const HeaderComponent = props => {
    const {user_Email, user_Name, customerId, mobile_number, customer_Address, questionArray, isLoggedIn, isIDFenceInMem, isNonSubscriber, isRewardOn, isAnsweredAllQuestions, setIsAnsweredAllQuestions} = props;
    const {addressLine1 = '', cityName = '', stateName = ''} = customer_Address ?? {};
    const address = addressLine1 + ' ' + cityName + ' ' + stateName;
    const isNameEmailAvailable = (user_Email?.trim()?.length && user_Name?.trim()?.length);
    const filledBars = questionArray.filter((question) => question?.answered).length;
    const totalBars = (isNameEmailAvailable) ? questionArray.length : (questionArray.length + 1);
    const unfilledBar = totalBars - filledBars;

    if (filledBars == totalBars && !isAnsweredAllQuestions) {
        setIsAnsweredAllQuestions(true);
    }

    const handleCompleteProfile = () => {
        nativeBridgeRef.startQuestionScreen(questionArray.filter((question) => !(question?.answered)), totalBars, filledBars + 1, (isNonSubscriber && isRewardOn && !isIDFenceInMem) ? AppForAppFlowStrings.completeProfile : AppForAppFlowStrings.generalCompleteProfile);
    };

    return (
        <View>
            {
                isLoggedIn
                    ?
                    filledBars == totalBars         // if all questions answered & user logged in
                        ? <ProfileViewComponent
                            topSectionText={AppForAppFlowStrings.myProfileText}
                            bottomSectionText={AppForAppFlowStrings.otherOptions}
                            headerText={user_Name}
                            descriptionText={AppForAppFlowStrings.relationshipNo + customerId}
                            subDescriptionText={user_Email + ' | ' + mobile_number}
                            addressData={address}
                            onPress={() => {
                                let eventData = new Object();
                                eventData[AppForAppFlowStrings.location] = AppForAppFlowStrings.accountTabScreen;
                                logWebEnageEvent(PROFILE, eventData);
                                nativeBridgeRef.startEditDetails()
                            }}
                        />
                        : <View style={styles.cardContainer}>
                            <CardWithProgress     // any one question is not answered & user logged in
                                cardContaierStyle={[styles.cardStyle, {marginHorizontal: 0}]}
                                headerContainerStyle={styles.cardHeaderContainer}
                                headerText={(isNonSubscriber && isRewardOn && !isIDFenceInMem) ? AppForAppFlowStrings.unlockReward : AppForAppFlowStrings.personalizedExperience}
                                showProfileInfo={isNameEmailAvailable}
                                nameText={user_Name}
                                relationshipNoText={AppForAppFlowStrings.relationshipNo + customerId}
                                contactText={user_Email + ' | ' + mobile_number}
                                addressData={address}
                                footerText={AppForAppFlowStrings.stepsLeft(unfilledBar)}
                                footerContainerStyle={styles.cardFooterContainer}
                                footerButtonText={AppForAppFlowStrings.completeProfileText}
                                totalBars={totalBars || 0}
                                filledBars={filledBars || 0}
                                onPress={() => {
                                    let eventData = new Object();
                                    eventData[AppForAppFlowStrings.location] = AppForAppFlowStrings.accountTabScreen;
                                    eventData["Rewards"] = isRewardOn ? "On" : "Off"
                                    logWebEnageEvent(COMPLETE_PROFILE, eventData);
                                    handleCompleteProfile();
                                }}
                                onEditClick={() => {
                                    let eventData = new Object();
                                    eventData[AppForAppFlowStrings.location] = AppForAppFlowStrings.accountTabScreen;
                                    logWebEnageEvent(PROFILE, eventData);
                                    nativeBridgeRef.startEditDetails()
                                }}
                            />
                        </View>

                    : <VerifyMobileComponent //if user is not logged in
                        headerText={AppForAppFlowStrings.rewardsWaitingForYou}
                        descriptionText={AppForAppFlowStrings.completeProfileAndVerifyMobile}
                        buttonText={AppForAppFlowStrings.verifyMobile}
                        onClick={() => nativeBridgeRef.openLoginWithCallback(status => {})}
                    />
            }
        </View>
    );
}

const MyAccountScreen = props => {
    const {
        customer_id,
        user_Email = "",
        user_Name = "",
        mobile_number = "",
        isIDFenceInMem,
        isNonSubscriber,
        customer_Address,
        onBoardingQA = [],
        verifyEmailDismissed = false
    } = APIData ?? {}
    const [forceUpdate] = useForceUpdate()
    const { deeplinkPath, setDeeplink } = useContext(RootContext)
    const [isBlockingLoader, blockingLoaderMessage, startBlockingLoader, stopBlockingLoader] = useLoader();
    const [draftEmailId, setDraftEmailId] = useState(null);
    const [isAnsweredAllQuestions, setIsAnsweredAllQuestions] = useState(false);
    const isNameEmailAvailable = (user_Email?.trim()?.length && user_Name?.trim()?.length);
    const [isLoading, setIsLoading] = useState(false);
    const customerId = Number(customer_id);
    const isLoggedIn = customerId ? true : false;
    const [isRewardOn, setIsRewardOn] = useState(false);
    const answeredQuestions = onBoardingQA?.filter((question) => question?.answered);
    const unansweredQuestions = onBoardingQA?.filter((question) => !(question?.answered));
    const isAllGenericQuestionsAnswered = onBoardingQA?.length == answeredQuestions?.length;
    const [isVerifyEmailDismissed, setIsVerifyEmailDismissed] = useState(verifyEmailDismissed);
    const [snackBarData, setSnackBarData] = useState({isShowSnackBar: false, snackBarMessage: null})
    const showRewards = () => (isLoggedIn && isAnsweredAllQuestions && isRewardOn) // rewards will be enable if reward is on and all questions are answered
    const showNewTagOnRewards = () => (isNonSubscriber && !isIDFenceInMem) // new badge will be visible to non subscriber customers only

    useEffect(() => {
        getrewardOnStatus((status) => {
            // add login as root
            setIsRewardOn(status);
            setupAccountscreen();
        });

        if (isLoggedIn) {
            getCustomerDetails();
        }

        eventEmitter.removeListener('CustomerDetailsUpdated', null);
        eventEmitter.addListener('CustomerDetailsUpdated', params => {
            getCustomerDetails();
            nativeBridgeRef.getApiProperty(apiProperty => {
                let apiProperties = parseJSON(apiProperty);
                setIsVerifyEmailDismissed(apiProperties?.verifyEmailDismissed ?? false);
            });
        });

        eventEmitter.addListener('ProfileUpdated', params => {
            if (Platform.OS === PLATFORM_OS.android) {
                params = {...params, customer_Address: parseJSON(params?.customer_Address ?? "{}") ?? {}}
            }
            updateAPIData({
                customer_id: params?.customer_id,
                user_Email: params?.user_Email ?? '',
                user_Name: params?.user_Name ?? '',
                mobile_number: params?.mobile_number ?? '',
                isNonSubscriber: params?.isNonSubscriber,
                isIDFenceInMem: params?.isIDFenceInMem,
                customer_Address: params?.customer_Address
            })
            forceUpdate()
            getQuestionsData();
        });
        return () => {
            eventEmitter.removeListener('ProfileUpdated', () => {});
            eventEmitter.removeListener('CustomerDetailsUpdated', () => {});
        };
    }, []);

    useEffect(() => {
        if (deeplinkPath?.length) {
            handleDeeplink()
        }
    }, [deeplinkPath, isBlockingLoader])

    const getQuestionsData = () => {
        setIsLoading(true);
        APIHelper.getQuestions((response, error) => {
            setIsLoading(false);
            if (response) {
                nativeBridgeRef.setOnBoardingData(JSON.stringify(response))
                updateAPIData({
                    onBoardingQA: response?.data
                })
                forceUpdate()
            }
        });
    }

    const getCustomerDetails = () => {
        setIsLoading(true);
        APIHelper.getCustomerDetails((response, error) => {
            setIsLoading(false);
            stopBlockingLoader();
            if (response) {
                nativeBridgeRef.updateCustomerDetails(JSON.stringify(response));
                const { emailId, firstName, mobileNumber, addresses, draftEmailId } = response?.data?.customers?.[0] ?? {};
                updateAPIData({
                    user_Email: emailId,
                    user_Name: firstName,
                    mobile_number: mobileNumber?.toString(),
                    customer_Address: addresses?.filter(address => address.addressType === "PER")?.[0] ?? addresses?.[0] ?? {}
                })
                forceUpdate()
                setDraftEmailId(draftEmailId)
            }
        });
    }

    const sendEmailVerification = (email) => {
        setIsLoading(true);
        APIHelper.sendVerificationEmail(email, (response, error) => {
            stopBlockingLoader();
            setIsLoading(false);
            if (response) {
                showSnackBar(`Verification link has been sent to ${draftEmailId}. Please click on link to verify your email.`, 4000)
            } else {
                let errorMessage = error?.message ?? error?.error?.[0]?.message ?? "Error sending verification email";
                showSnackBar(errorMessage ?? "Error sending verification email", 2000)
            }
        });
    }

    const showSnackBar = (message, timeout) => {
        setSnackBarData({isShowSnackBar: true, snackBarMessage: message})
        setTimeout(() => setSnackBarData({isShowSnackBar: false, snackBarMessage: null}), timeout)
    }

    const setupAccountscreen = () => {
        if (deeplinkPath?.length) {
            startBlockingLoader(AppForAppFlowStrings.pleaseWait);
        }
        if (!(onBoardingQA?.length) && isLoggedIn) { //question data is not in props hit the api
            getQuestionsData();
        }
    };
    //handle deeplinks
    const handleDeeplinkWithData = (data) => {
        const filledBars = answeredQuestions?.length;
        const totalBars = (isNameEmailAvailable) ? onBoardingQA?.length : (onBoardingQA?.length + 1);
        const isAnsweredAllQuestions = (totalBars == filledBars);
        switch (true) {
            case deeplinkPath.includes(Deeplink.myRewards):
                (data && isRewardOn) ? MyAccountBtnActions.goToMyRewards(isAnsweredAllQuestions, false) : null;
                break;
            case deeplinkPath.includes(Deeplink.rewardAppShare):
                (data && isRewardOn) ? MyAccountBtnActions.goToMyRewards(isAnsweredAllQuestions, true) : null;
                break;
            case deeplinkPath.includes(Deeplink.myProfile):
                nativeBridgeRef.startEditDetails();
                break;
            case deeplinkPath.includes(Deeplink.profileQuestion):
            case deeplinkPath.includes(Deeplink.onboardingQuestion):
                nativeBridgeRef.startQuestionScreen(unansweredQuestions, totalBars, filledBars + 1, (isNonSubscriber && isRewardOn && !isIDFenceInMem) ? AppForAppFlowStrings.completeProfile : AppForAppFlowStrings.generalCompleteProfile);
                break;
            default:
                break;
        }
        setDeeplink(null);
    };

    const handleDeeplink = () => {
        switch (true) {
            case deeplinkPath.includes(Deeplink.profileQuestion):
            case deeplinkPath.includes(Deeplink.onboardingQuestion):
            case deeplinkPath.includes(Deeplink.myProfile):
                if (!isLoading && onBoardingQA?.length) {
                    stopBlockingLoader();
                    setTimeout(() => {
                        handleDeeplinkWithData(onBoardingQA);
                    }, 200);
                } else if (isLoading && !isBlockingLoader) {
                    startBlockingLoader(AppForAppFlowStrings.pleaseWait);
                }
                break;
            case deeplinkPath.includes(Deeplink.myRewards):
            case deeplinkPath.includes(Deeplink.rewardAppShare):
                if (!isLoading && onBoardingQA?.length && isRewardOn) {
                    stopBlockingLoader();
                    setTimeout(() => {
                        handleDeeplinkWithData(onBoardingQA);
                    }, 200);
                } else if (!isRewardOn && !isLoading && onBoardingQA?.length) {
                    setDeeplink(null);
                    stopBlockingLoader();
                } else if (isLoading && !isBlockingLoader) {
                    startBlockingLoader(AppForAppFlowStrings.pleaseWait);
                }
                break;
            default:
                stopBlockingLoader();
                break;
        }
    }

    const SingleActionMessageComponent = props => {
        const {description, actionText, onAction, onCross} = props;
        return (
            <View style={styles.emailContainer}>
                <TouchableOpacity style={styles.emailActionView} onPress={onCross}>
                    <Image source={Images.ic_blackClose} style={styles.emailBoxCrossIcon}/>
                </TouchableOpacity>
                <Text style={styles.emailDescriptionTextStyle}>{description}</Text>
                <TouchableOpacity style={styles.emailActionView} onPress={onAction}>
                    <Text style={styles.emailBoxCTA}>{actionText}</Text>
                </TouchableOpacity>
            </View>
        );
    };

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: colors.color_F3F2F7
            }}>
            <ScrollView showsVerticalScrollIndicator={false}
                        automaticallyAdjustContentInsets={true}
                        style={{backgroundColor: colors.color_F3F2F7}}>
                {(draftEmailId && !isVerifyEmailDismissed) ? <SingleActionMessageComponent
                    description={AppForAppFlowStrings.verifyEmailText(draftEmailId)}
                    actionText={AppForAppFlowStrings.resendLink}
                    onAction={() => {
                        startBlockingLoader(AppForAppFlowStrings.pleaseWait);
                        sendEmailVerification(draftEmailId);
                    }}
                    onCross={() => {
                        setIsVerifyEmailDismissed(true);
                        nativeBridgeRef.setVerifyEmailDismissed(true);
                    }}
                /> : null}
                <SkeletonContent
                    containerStyle={{flex: 1}}
                    duration={1000}
                    isLoading={isLoading}>
                    {isLoading
                        ? <View style={{
                            height: 100,
                            marginVertical: spacing.spacing20,
                            marginLeft: 20,
                            width: Dimensions.get('screen').width - 40,
                        }}/>
                        : <HeaderComponent isLoggedIn={isLoggedIn} isRewardOn={isRewardOn}
                                           questionArray={onBoardingQA}
                                           user_Name={user_Name}
                                           user_Email={user_Email}
                                           mobile_number={mobile_number}
                                           customerId={customerId}
                                           customer_Address={customer_Address}
                                           setIsAnsweredAllQuestions={setIsAnsweredAllQuestions}
                                           isAnsweredAllQuestions={isAnsweredAllQuestions}
                                           isIDFenceInMem={isIDFenceInMem}
                                           isNonSubscriber={isNonSubscriber}
                        />
                    }
                </SkeletonContent>

                {/* all clickable cells for my account */}
                {isAllGenericQuestionsAnswered ? <ClickableCell
                    item={{
                        title: AppForAppFlowStrings.myGadgetsAppliancesAndBankCards,
                        imageSource: {source: Images.rightBlueArrow},
                    }}
                    containerStyle={styles.cell}
                    titleStyle={OATextStyle.TextFontSize_16_normal}
                    onPress={() => MyAccountBtnActions.showGadgetAppliancesAndBanks(isLoggedIn)}
                /> : null}

                {/*TODO: - Hides the reward section as because we have remove idfence trail plan as reward so if rewards need to show then uncomment below section*/}
                {showRewards() ? <ClickableCell
                    item={{
                        title: AppForAppFlowStrings.rewards,
                        imageSource: {source: Images.rightBlueArrow},
                    }}
                    containerStyle={styles.cell}
                    titleStyle={OATextStyle.TextFontSize_16_normal}
                    onPress={() => MyAccountBtnActions.goToMyRewards(isAnsweredAllQuestions)}
                    badgeText={showNewTagOnRewards() ? AppForAppFlowStrings.newBadge : null}
                    badgeStyle={showNewTagOnRewards() ? styles.badgeStyle : null}
                    badgeTextStyle={showNewTagOnRewards() ? styles.badgeTextStyle : null}
                /> : null}

                <ClickableCell
                    item={{title: AppForAppFlowStrings.myServiceRequest, imageSource: {source: Images.rightBlueArrow},}}
                    containerStyle={styles.cell}
                    titleStyle={OATextStyle.TextFontSize_16_normal}
                    onPress={() => {
                        MyAccountBtnActions.showAllServiceRequests(isLoggedIn);
                    }}
                />

                <ClickableCell
                    item={{title: AppForAppFlowStrings.bookmarkOffers, imageSource: {source: Images.rightBlueArrow},}}
                    containerStyle={styles.cell}
                    titleStyle={OATextStyle.TextFontSize_16_normal}
                    onPress={() => {
                        MyAccountBtnActions.goToBookMarkOffers(isLoggedIn);
                    }}
                />


                <ClickableCell item={{
                    title: Platform.OS === 'ios' ? AppForAppFlowStrings.rateOnAppStore : AppForAppFlowStrings.rateOnGoogle,
                    imageSource: {source: Images.rightBlueArrow},
                }}
                               containerStyle={styles.cell}
                               titleStyle={OATextStyle.TextFontSize_16_normal}
                               onPress={() => {
                                   MyAccountBtnActions.openWebView(WebViewFor.RATEONAPPSTORE, '', '');
                               }}
                />

                <ClickableCell
                    item={{title: AppForAppFlowStrings.chatWithUs, imageSource: {source: Images.rightBlueArrow}}}
                    containerStyle={styles.cell}
                    titleStyle={OATextStyle.TextFontSize_16_normal}
                    onPress={() => {
                        MyAccountBtnActions.openChat(isLoggedIn);
                    }}
                />

                {Platform.OS == PLATFORM_OS.android
                    ? <ClickableCell
                        item={{
                            title: AppForAppFlowStrings.privacyPolicy,
                            imageSource: {source: Images.rightBlueArrow},
                        }}
                        containerStyle={styles.cell}
                        titleStyle={OATextStyle.TextFontSize_16_normal}
                        onPress={() => {
                            MyAccountBtnActions.openWebView(WebViewFor.PRIVACYPOLICY, '', '');
                        }}
                    />
                    : <ClickableCell
                        item={{
                            title: AppForAppFlowStrings.settings,
                            imageSource: {source: Images.rightBlueArrow},
                        }}
                        containerStyle={styles.cell}
                        titleStyle={OATextStyle.TextFontSize_16_normal}
                        onPress={() => {
                            MyAccountBtnActions.goToSettings();
                        }}
                    />
                }

                {/* if use logged in then show logout menu */}
                {isLoggedIn
                    ? <ClickableCell item={{
                        title: AppForAppFlowStrings.logout,
                        imageSource: {source: Images.rightBlueArrow},
                    }}
                                     containerStyle={styles.cell}
                                     titleStyle={OATextStyle.TextFontSize_16_normal}
                                     onPress={() => {
                                         MyAccountBtnActions.handleLogout();
                                     }}/>
                    : null}

                <ClickableCell item={{
                    title: AppForAppFlowStrings.haveVoucher,
                    subTitle: AppForAppFlowStrings.activateMembership, imageSource: {source: Images.rightBlueArrow},
                }}
                               containerStyle={[styles.cell, styles.voucherCell]}
                               titleStyle={OATextStyle.TextFontSize_16_normal}
                               onPress={() => {
                                   MyAccountBtnActions.goToActivateVoucher(isLoggedIn);
                               }}
                />
                {/* footer of my account */}
                <View style={styles.footerContainer}>
                    <Image source={Images.oaLogo}/>
                    <View style={styles.shareContainer}>
                        <View style={styles.footerTextDescription}>
                            <Text>{AppForAppFlowStrings.shareText}</Text>
                        </View>
                        <LinkButton style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}}
                                    data={{
                                        action: AppForAppFlowStrings.shareApp,
                                    }}
                                    style={splashWhiteButton}
                                    onPress={MyAccountBtnActions.handleOnShare}
                        />
                    </View>
                </View>
                <BlockingLoader visible={isBlockingLoader} loadingMessage={blockingLoaderMessage}/>
            </ScrollView>
            {snackBarData.isShowSnackBar ?
                <SnackBarComponent snackBarStyle={{marginBottom: (Platform.OS === PLATFORM_OS.android ? 80 : 20)}}
                                   isAutoDismissable={false} text={snackBarData.snackBarMessage}/> : null}
            <DialogView ref={ref => (this.DialogViewRef = ref)}/>
            {/*below tootip code is useful when pending rewards will come from server*/}
            {/*<ToolTipComponent isAnimated={true} text={"You have a pending rewards card in your account. Tap here to see."} onPress={() => { }} />*/}
        </SafeAreaView>
    );
};


const splashWhiteButton = {
    buttonStyle: {},
    actionTextStyle: {
        ...TextStyle.text_16_bold,
        color: Colors.blue,
    },
};

const styles = StyleSheet.create({

    cardContainer: {
        marginHorizontal: spacing.spacing16,
        marginVertical: spacing.spacing24,
    },
    //progress card styles
    cardStyle: {
        paddingHorizontal: spacing.spacing12,
        paddingVertical: spacing.spacing20,
        backgroundColor: colors.color_1A73E9,
        borderRadius: 4,
        marginHorizontal: spacing.spacing20,
    },
    cardHeaderContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    cardFooterContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },

    cell: {
        backgroundColor: Colors.white,
    },
    voucherCell: {
        marginTop: spacing.spacing16,
    },
    footerContainer: {
        marginTop: spacing.spacing24,
        marginHorizontal: spacing.spacing16,
        marginBottom: spacing.spacing40,
    },
    footerTextDescription: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginRight: spacing.spacing16,
    },
    shareContainer: {
        flexDirection: 'row',
    },
    badgeStyle: {
        alignSelf: 'center',
        marginRight: spacing.spacing10,
        backgroundColor: colors.saffronYellow,
        justifyContent: 'flex-end',
        paddingHorizontal: spacing.spacing4,
        borderRadius: 2,
    },
    badgeTextStyle: {
        ...TextStyle.text_10_bold,
        color: colors.white,
        letterSpacing: 2,

    },
    emailContainer: {
        flexDirection: 'column',
        borderRadius: spacing.spacing4,
        borderWidth: spacing.spacing1,
        borderColor: colors.saffronYellow,
        margin: spacing.spacing8,
        backgroundColor: colors.color_card_background_light_yellow,
    },
    emailDescriptionTextStyle: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        marginHorizontal: spacing.spacing12,
    },
    emailActionView: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
    },
    emailBoxCrossIcon: {
        width: spacing.spacing16,
        height: spacing.spacing16,
        marginTop: spacing.spacing8,
        marginRight: spacing.spacing8,
    },
    emailBoxCTA: {
        ...TextStyle.text_14_bold,
        color: colors.blue028,
        lineHeight: 16,
        marginHorizontal: spacing.spacing16,
        marginVertical: spacing.spacing8
    }
});

export default MyAccountScreen;
