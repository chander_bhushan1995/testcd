import {createStackNavigator} from 'react-navigation';
import SettingsListingComponent from '../Screens/SettingsScreen'
import WebViewComponent from '../../../CardManagement/PayBill/WebViewComponent';

const navigator = createStackNavigator({
    SettingsListingComponent: {
        screen: SettingsListingComponent,
    },
    WebViewComponent: {
        screen: WebViewComponent,
    },
}, {
});

export default navigator

