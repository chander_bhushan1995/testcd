import {
    Image,
    NativeEventEmitter,
    NativeModules,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import React, {useContext, useEffect, useReducer, useRef, useState} from 'react';
import {AppForAppFlowStrings} from '../../Constant/AppForAllConstants';
import {TextStyle} from '../../../Constants/CommonStyle';
import ClickableCell from '../../../CustomComponent/customcells/clickableCell/ClickableCell';
import Images from '../../../images/index.image';
import {OATextStyle} from '../../../Constants/commonstyles';
import colors from '../../../Constants/colors';
import {MyAccountBtnActions, WebViewFor} from '../../MyAccount/Actions/MyAccountButtonActions';
import spacing from '../../../Constants/Spacing';
import Colors from '../../../Constants/colors';
import {SettingsContext} from './index.myaccountsettings';
import {PRIVACY_POLICY} from '../../../Constants/ApiUrls';
import {APIData} from '../../../../index';
import BackButton from '../../../CommonComponents/NavigationBackButton';

const nativeBridgeRef = NativeModules.ChatBridge;
const eventEmitter = new NativeEventEmitter(nativeBridgeRef);
const SettingsListingComponent = props => {

    const { isPushEnabled }  = useContext(SettingsContext)
    const [switchValue, setSwitchValue] = useState(isPushEnabled ?? false);

    useEffect(()=>{

        eventEmitter.addListener('pushStatusUpdated', params => {
                setSwitchValue(params.isPushEnabled ?? false)
        })

        return ()=>{
            eventEmitter.removeListener('pushStatusUpdated',() => {
            })

        }
    },[])


    return (
        <ScrollView showsVerticalScrollIndicator={false}
                    contentContainerStyle={{paddingBottom: spacing.spacing64}}
                    automaticallyAdjustContentInsets={true}
                    style={{backgroundColor: colors.color_F3F2F7, paddingTop: 20}}>
            <ClickableCell
                item={{
                    title: AppForAppFlowStrings.notifications,
                    imageSource: {source: Images.rightBlueArrow},
                }}
                containerStyle={styles.cell}
                titleStyle={OATextStyle.TextFontSize_16_normal}
                isWithSwitch={true}
                isSwitchEnabled = {switchValue}
                onSwitchValueChange={value => {
                    setSwitchValue(value);
                    nativeBridgeRef.switchValueChanged(value)
                }}
                onPress={() => {
                    setSwitchValue(!switchValue);
                    nativeBridgeRef.switchValueChanged(switchValue)
                }}
            />
            <ClickableCell
                item={{
                    title: AppForAppFlowStrings.privacyPolicy,
                    imageSource: {source: Images.rightBlueArrow},
                }}
                containerStyle={styles.cell}
                titleStyle={OATextStyle.TextFontSize_16_normal}
                onPress={() => {
                    props.navigation.navigate('WebViewComponent', {
                        url: APIData?.api_base_url + PRIVACY_POLICY,
                        headerTitle: AppForAppFlowStrings.privacyPolicy,
                    });
                }}
            />
        </ScrollView>
    );
};


const styles = StyleSheet.create({
    cell: {
        backgroundColor: Colors.white,
    },
});




SettingsListingComponent.navigationOptions = (navigationData) => {
    return {
        headerLeft: <BackButton onPress={()=>{nativeBridgeRef.goBack()}} />,
        headerTitle: (
            <View style={Platform.OS === 'ios' ? {alignItems: 'center'} : {alignItems: 'flex-start'}}>
                <Text style={TextStyle.text_16_bold}>{AppForAppFlowStrings.settingsTitle}</Text>
            </View>
        ),
    };
};

export default SettingsListingComponent;
