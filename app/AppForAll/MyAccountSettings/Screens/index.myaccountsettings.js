import React, { useEffect } from 'react';
import AppNavigator from '../Navigation/MyAccountSettingsNavigatonStack';
import {setAPIData, updateAPIData} from "../../../../index";

export const SettingsContext = React.createContext();

function Root(props) {
    setAPIData(props);

    useEffect(() => {
        updateAPIData(props)
    }, [props])

    return (
        <SettingsContext.Provider value={{
            isPushEnabled: props.isPushEnabled
        }}>
            <AppNavigator/>
        </SettingsContext.Provider>
    );
}
export default Root;
