import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TextStyle } from '../../../Constants/CommonStyle'
import colors from '../../../Constants/colors'
import { AppForAppFlowStrings } from '../../Constant/AppForAllConstants'


const SnackBarComponent = props => {
    const isAutoDismissable = props.isAutoDismissable ?? true
    const timeout = props.timeout ?? 2000
    const text = props.text ?? AppForAppFlowStrings.yourTextHere
    const snackBarStyle = props.snackBarStyle || {}
    const textStyle = props.textStyle || {}
    const [isVisible, setIsVisible] = useState(true)

    useEffect(() => {
        isAutoDismissable ? setTimeout(()=>{setIsVisible(false)}, timeout) : null
    }, [timeout, isVisible])

    return (
        <View>
            {isVisible
                ? <View style={[styles.snackBar, snackBarStyle]}>
                    <Text style={[styles.defaultTextStyle, textStyle]}>{text}</Text>
                </View>
                : null
            }
        </View>
    )
}

export default SnackBarComponent

const styles = StyleSheet.create({
    snackBar: {
        bottom: 0,
        backgroundColor: colors.charcoalGrey,
        elevation: 4,
        shadowColor: colors.black,
        shadowOffset: {
            width: 4,
            height: 4,
        },
        shadowOpacity: 0.12
    },
    defaultTextStyle: {
        ...TextStyle.text_14_normal,
        color: colors.white,
        margin: 16,
    }
})
