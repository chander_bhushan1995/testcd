import { createStackNavigator } from "react-navigation";
import MyProfileScreen from '../Screens/MyProfileScreen';
import colors from "../../../Constants/colors";

const navigator = createStackNavigator({
    MyProfileScreen: {
        screen: MyProfileScreen,
        navigationOptions: {
            gesturesEnabled: false,
            title: "My Profile",
            headerTitleStyle: {
                color: colors.charcoalGrey,
            },
            headerStyle: {
                backgroundColor: colors.white,
                elevation: 4,
                shadowOpacity: 0.1, 
                shadowOffset: {
                    width: 1,
                    height: 1,
                  },
                  shadowColor: '#000000',
            },
            headerTintColor: colors.blue,
        }
    },
    
}, {
});

export default navigator;
