import React, { useEffect, useState } from 'react'
import {
    Platform,
    StyleSheet,
    Text,
    View,
    NativeModules,
    SafeAreaView,
    NativeEventEmitter,
    BackHandler
} from 'react-native'
import { AppForAppFlowStrings } from '../../Constant/AppForAllConstants'
import spacing from '../../../Constants/Spacing'
import { TextStyle } from '../../../Constants/CommonStyle'
import colors from '../../../Constants/colors'
import CellWithLeftTitleAndRightImageV2 from '../../../CustomComponent/customcells/cellwithltitleandrimg/CellWithLeftTitleAndRightImage.componentV2'
import images from '../../../images/index.image'
import SnackBarComponent from '../Components/SnackBarComponent'
import Loader from '../../../Components/Loader'
import ApiErrorComponent from '../../../CommonComponents/ApiErrorComponent'
import { OATextStyle } from '../../../Constants/commonstyles'
import BackButton from '../../../CommonComponents/NavigationBackButton';
import * as APIHelper from "../../../network/APIHelper";
import { APIData, updateAPIData } from '../../../../index'
import useForceUpdate from '../../../CardManagement/CustomHooks/useForceUpdate'

const nativeBridgeRef = NativeModules.ChatBridge;
const eventEmitter = new NativeEventEmitter(nativeBridgeRef);

const MyProfileScreen = props => {
    const {
        onBoardingQA = []
    } = APIData ?? {}
    const [forceUpdate] = useForceUpdate()
    const [isLoading, setIsLoading] = useState(false)    
    const [isError, setIsError] = useState(false)    
    const [isShowSnackBar, setShowSnackBar] = useState(false)

    const handleCellPress = (questionObject) => {
        nativeBridgeRef.editQuestionsScreen([questionObject])
    }

    useEffect(() => { //load first time data
        BackHandler.addEventListener("hardwareBackPress", onBackPress);
        getQuestionsData()

        eventEmitter.addListener("ProfileUpdated", params => {
            getQuestionsData()
            setShowSnackBar(true)
        });
        return () => {
            BackHandler.removeEventListener(onBackPress)
            eventEmitter.removeListener("ProfileUpdated", () => {})
        }
    }, [])

    const getQuestionsData = () => {
        setIsLoading(true);
        APIHelper.getQuestions((response, error) => {
            setIsLoading(false);
            if (response) {
                nativeBridgeRef.setOnBoardingData(JSON.stringify(response))
                updateAPIData({ 
                    onBoardingQA: response?.data
                })
                setIsError(false)
                forceUpdate()
            } else {
                setIsError(true)
            }
        });
    }

    if (isError) {
        return <ApiErrorComponent isLoading={isLoading}
            onClose={() => nativeBridgeRef.goBack()}
            onTryAgainClick={getQuestionsData}
        />
    }

    if (isLoading) {
        return <Loader isLoading={isLoading} />
    }

    return (
        <SafeAreaView style={styles.screen}>
            {onBoardingQA?.map((questionObject, index) => {
                let selectedAnswers = (questionObject?.optionList?.map((option, index, array) => { return option.selected ? option.optionText : null }))
                let otherAnswers = (questionObject?.otherAnswerList ?? []).map((object) => { return (object?.brand + " " + object?.model) })
                selectedAnswers = (otherAnswers.concat(selectedAnswers)).filter(Boolean).join(", ");
                var image = null
                var title = ""
                switch (questionObject?.questionCode) {
                    case AppForAppFlowStrings.assetHAQC:
                        image = images.homeServ
                        title = AppForAppFlowStrings.myAppliances
                        break;
                    case AppForAppFlowStrings.assetPEQC:
                        image = images.ic_gadgetProfile
                        title = AppForAppFlowStrings.myGadgets
                        break;
                    case AppForAppFlowStrings.assetFQC:
                        image = images.ic_banksProfile
                        title = AppForAppFlowStrings.banksTransactWith
                        break;
                }
                return <CellWithLeftTitleAndRightImageV2
                    key={index.toString()}
                    item={{
                        title: title, subTitle: selectedAnswers,
                        rightImage: images.ic_edit, imageSource: image, leftImageStyle: styles.leftImageStyle,
                        rightImageStyle: styles.rightImageStyle
                    }}
                    titleStyle={{ ...OATextStyle.TextFontSize_14_normal, color: colors.charcoalGrey }}
                    isBottomSeperator={true}
                    bottomSeperatorStyle={{ marginVertical: 8 }}
                    onPress={() => { handleCellPress(questionObject) }}
                />
            })
            }
            <View style={{ flex: 1 }} />
            {isShowSnackBar ? <SnackBarComponent text={AppForAppFlowStrings.changeSaved} /> : null}
        </SafeAreaView>
    )
}

export default MyProfileScreen

MyProfileScreen.navigationOptions = (navigationData) => {
    return {
        headerLeft: <BackButton onPress={()=>{nativeBridgeRef.goBack()}}/>,
        headerTitle: (
            <View style={Platform.OS === 'ios' ? { alignItems: 'center' } : { alignItems: 'flex-start' }}>
                <Text style={TextStyle.text_16_bold}>{AppForAppFlowStrings.myProfileTitle}</Text>
            </View>
        ),
    };
};

function onBackPress() {
    nativeBridgeRef.goBack();
};

const styles = StyleSheet.create({
    screen: {
        backgroundColor: colors.white,
        flex: 1,
    },
    personalDetailsContainer: {
        marginTop: 32,
    },
    personalDetailsText: {
        ...TextStyle.text_16_normal,
        marginHorizontal: 12,
        color: colors.charcoalGrey
    },
    textFieldsContainer: {
        marginTop: spacing.spacing12,
    },
    textField: {
        marginTop: spacing.spacing12,
        marginHorizontal: spacing.spacing12,
    },
    textFieldHeaderText: {
        color: colors.grey
    },
    textHintContainer: {
        marginTop: spacing.spacing8,
        marginBottom: 10,
    },
    textView: {
        marginHorizontal: spacing.spacing12,
        marginBottom: spacing.spacing24,
        marginTop: spacing.spacing8
    },
    footerText: {
        ...TextStyle.text_12_normal,
        color: colors.greyWithOpacity,
    },
    clickableText: {
        ...TextStyle.text_12_normal,
        color: colors.blue,
    },
    leftImageStyle: {
        height: 48,
        width: 48,
        resizeMode: 'contain',
    },
    rightImageStyle: {
        height: 24,
        width: 24,
        resizeMode: 'cover',
    }
})
