import React from 'react'
import {
    StyleSheet, Text, View, ImageBackground, Dimensions, Image,
    TouchableOpacity, NativeModules
} from 'react-native'
import { CommonStyle, TextStyle } from '../../../Constants/CommonStyle'
import spacing from '../../../Constants/Spacing'
import colors from '../../../Constants/colors'
import TagComponent from '../../../Catalyst/components/common/TagsComponent'
import images from '../../../images/index.image'
import { handleRewardBtnActions, ButtonsAction } from '../Helpers/ButtonActions'
import { RewardStats } from '../../Constant/ScreensStaticData/IDFenceData'

const RewardCards = props => {

    return (
        <TouchableOpacity onPress={() => { handleRewardBtnActions(ButtonsAction.Open_Scratch_Card, { state: RewardStats.LOCKED }) }}>
            <ImageBackground source={images.ic_cardBg} style={[CommonStyle.cardContainerWithShadow, styles.rewardCard]}>
                {/* <View style={styles.tagContainer}>
                    <TagComponent tags={[props.tag]}
                        singleTagStyle={styles.singleTagStyle}
                        textStyle={{ ...TextStyle.text_10_bold, color: colors.white }}
                    />
                </View> */}
                {/* <ImageBackground > */}
                <ImageBackground source={images.ic_myRewardsBadge} style={styles.rewardsCenterView} />
                {props.text ?
                    <View style={{ backgroundColor: colors.white, marginTop: -5, padding: spacing.spacing8 }}>
                        <Text style={styles.rewardsText}>{props.text}</Text>
                    </View> : null}
                {/* </ImageBackground> */}
            </ImageBackground>
        </TouchableOpacity>
    )
}

export default RewardCards

const styles = StyleSheet.create({
    tagContainer: {
        position: 'absolute',
        top: spacing.spacing0,
        right: spacing.spacing8
    },
    singleTagStyle: {
        backgroundColor: colors.saffronYellow,
        borderWidth: 2,
        paddingHorizontal: spacing.spacing8,
        paddingVertical: spacing.spacing0,
        borderColor: colors.transparent,
        marginLeft: 0,
        borderRadius: 4,
    },
    rewardCard: {
        width: (Dimensions.get('screen').width - 40) / 2,
        height: (Dimensions.get('screen').width - 40) / 2,
        backgroundColor: colors.blue028,
        justifyContent: 'center',
        borderRadius: 4
    },
    rewardsCenterView: {
        height: ((Dimensions.get('screen').width - 40) / 2)/2,
        width: ((Dimensions.get('screen').width - 40) / 2)/2,
        alignSelf: 'center',
        justifyContent: 'center',
        borderRadius: 36
    },
    rewardBadge: {
        height: 48,
        width: 48,
        alignSelf: 'center'
    },
    rewardsText: {
        ...TextStyle.text_12_normal,
        color: colors.charcoalGrey,
        textAlign: 'center',
    }
})
