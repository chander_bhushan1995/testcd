import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import spacing from '../../../Constants/Spacing'
import colors from '../../../Constants/colors'
import { TextStyle, CommonStyle } from '../../../Constants/CommonStyle'
import * as Progress from 'react-native-progress';
import RightArrowButton from '../../../CommonComponents/RightArrowButton'

const ServiceDescriptionView = props => {
    const serviceDescription = props.serviceDescription
    const descriptionIcon = props.descriptionIcon
    const btnText = props.btnText
    return (
        <View>
            <View style={styles.serviceDescriptionView}>
                <Image style={styles.serviceDescriptionIcon} source={descriptionIcon} />
                <Text style={styles.serviceDescription}>{serviceDescription}</Text>
            </View>
            <TouchableOpacity style={styles.button}>
                <Text style={styles.buttonText}>{btnText}</Text>
            </TouchableOpacity>
        </View>
    )
}

const ActionView = props => {
    const actionTitle = props.actionTitle
    const actionDescription = props.actionDescription
    const actionBackgroundColor = props.actionBackgroundColor
    const btnText = props.btnText
    return (
        <View style={styles.actionView}>
            <Text style={styles.actionTitle}>{actionTitle}</Text>
            <Progress.Bar progress={0.3} width={null}
                color={colors.color_45B448} style={styles.progressBar}
                height={4} />
            <View style={styles.actionButtonContainer}>
                <Text style={styles.actionDescription}>{actionDescription}</Text>
                <RightArrowButton text={btnText} textStyle={styles.actionButtonText} style={{ justifyContent: 'flex-end', }}
                    imageStyle={{ marginRight: 0, height: 10 }} onClick={() => {
                    }
                    } />
            </View>
        </View>
    )
}


const UnlockedActionRewards = props => {

    const cardTitle = props.cardTitle
    const cardDescription = props.cardDescription
    const actionTitle = props.actionTitle
    const actionDescription = props.actionDescription
    const actionBackgroundColor = props.actionBackgroundColor
    const serviceDescription = props.serviceDescription
    const descriptionIcon = props.descriptionIcon
    const btnText = props.btnText
    return (
        <View style={[CommonStyle.cardContainerWithShadow, { borderRadius: 4 }]}>
            <Text style={styles.cardTitle}>{cardTitle}</Text>
            <Text style={styles.cardDescription}>{cardDescription}</Text>
            <ActionView actionTitle={actionTitle} actionDescription={actionDescription}
                btnText={btnText} />
            <ServiceDescriptionView serviceDescription={serviceDescription} descriptionIcon={descriptionIcon}
                btnText={btnText}
            />
        </View>
    )
}

export default UnlockedActionRewards

const styles = StyleSheet.create({
    actionView: {
        marginVertical: spacing.spacing16,
        marginHorizontal: spacing.spacing12,
        borderRadius: 8,
        backgroundColor: 'rgba(69,180,72,0.08)'
    },
    cardTitle: {
        ...TextStyle.text_16_bold,
        marginHorizontal: spacing.spacing12,
        marginTop: spacing.spacing16,
        color: colors.charcoalGrey,
    },
    cardDescription: {
        ...TextStyle.text_12_normal,
        marginHorizontal: spacing.spacing12,
        marginTop: spacing.spacing4,
    },
    actionTitle: {
        ...TextStyle.text_12_normal,
        color: colors.charcoalGrey,
        marginHorizontal: spacing.spacing12,
        marginTop: spacing.spacing12
    },
    actionButtonContainer: {
        flexDirection: 'row',
        marginHorizontal: spacing.spacing12,
        justifyContent: 'space-between'
    },
    actionDescription: {
        ...TextStyle.text_14_bold,
        color: colors.charcoalGrey,
    },
    actionButtonText: {
        ...TextStyle.text_12_bold,
        color: colors.blue028
    },
    serviceDescriptionView: {
        backgroundColor: 'rgba(207,2,27,0.1)',
        padding: spacing.spacing12,
        flexDirection: 'row',
        marginHorizontal: spacing.spacing12,
        borderRadius: 4
    },
    serviceDescriptionIcon: {
        height: 24,
        width: 24,
    },
    serviceDescription: {
        ...TextStyle.text_14_semibold,
        color: colors.color_212B36,
        marginLeft: spacing.spacing12
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 2,
        marginTop: spacing.spacing24,
        backgroundColor: colors.color_0282F0,
        marginHorizontal: spacing.spacing16,
        marginBottom: spacing.spacing16
    },
    buttonText: {
        ...TextStyle.text_16_bold,
        margin: 12,
        textAlign: 'center',
        color: colors.white
    },
    progressBar: {
        marginHorizontal: spacing.spacing12,
        marginVertical: spacing.spacing8,
        backgroundColor: colors.greye0,
        borderWidth: 0
    }

})
