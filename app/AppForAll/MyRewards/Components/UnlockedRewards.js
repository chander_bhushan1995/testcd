import React from 'react'
import { StyleSheet, Text, View, ImageBackground, Image, TouchableOpacity, TouchableWithoutFeedback } from 'react-native'
import { CommonStyle, TextStyle } from '../../../Constants/CommonStyle'
import images from '../../../images/index.image'
import spacing from '../../../Constants/Spacing'
import colors from '../../../Constants/colors'
import { handleRewardBtnActions } from '../Helpers/ButtonActions'

const UnlockedRewards = props => {
    const state = props.state
    const title = props.title
    const description = props.description
    const btnText = props.buttonText
    const btnAction = props.buttonAction
    const image = props.image
    const imageStyle = props.imageStyle
    const couponCode = props.couponCode
    const couponCopyIcon = props.couponCopyIcon
    const secondBtnText = props.secondBtnText
    const secondBtnAction = props.secondBtnAction
    const validTill = props.validTill
    return (
        <TouchableWithoutFeedback onPress={() => {}}>
            <View style={[CommonStyle.cardContainerWithShadow, { borderRadius: 4 }]}>
                <ImageBackground source={images.confettiImage} style={styles.bgImage}>
                    <Image source={image} style={imageStyle} />
                </ImageBackground>
                <Text style={styles.title}>{title}</Text>
                <Text style={styles.description}>{description}</Text>
                {/* {couponCode ? //if coupon code available
                <View style={styles.couponView}>
                    <View>
                        <Text
                            style={{
                                marginTop: spacing.spacing8,
                                marginHorizontal: spacing.spacing12, ...TextStyle.text_10_bold, color: colors.color_888F97
                            }}>{"COPY CODE"}</Text>
                        <Text style={{ marginHorizontal: spacing.spacing12, ...TextStyle.text_10_bold, color: colors.charcoalGrey }}>{couponCode}</Text>
                    </View>
                    <TouchableOpacity>
                        <Image source={couponCopyIcon}
                            style={{ height: 32, width: 32, marginVertical: spacing.spacing12, marginRight: spacing.spacing12 }}
                        />
                    </TouchableOpacity>
                </View>
                :   // if coupon code not available */}
                {validTill ? <Text style={[styles.bottomNoteText, { color: colors.grey }]}>{validTill}</Text> : null}
                <TouchableOpacity style={styles.button} onPress={() => { handleRewardBtnActions(btnAction, { state: state }) }}>
                    <Text style={styles.buttonText}>{btnText}</Text>
                </TouchableOpacity>
                {/* } */}
                {secondBtnText ?
                    <TouchableOpacity style={styles.secondaryButton} onPress={() => { handleRewardBtnActions(secondBtnAction, state) }}>
                        <Text style={[styles.buttonText, { color: colors.color_0282F0 }]}>{secondBtnText}</Text>
                    </TouchableOpacity> : null}
            </View >
        </TouchableWithoutFeedback>
    )
}

export default UnlockedRewards

const styles = StyleSheet.create({
    bgImage: {
        height: 88,
        width: 220,
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: spacing.spacing8,
    },
    title: {
        ...TextStyle.text_20_bold,
        marginTop: 10,
        alignSelf: 'center',
        color: colors.color_0282F0,
        marginHorizontal: spacing.spacing16,
        textAlign: 'center'
    },
    description: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        marginHorizontal: spacing.spacing16,
        alignSelf: 'center',
        textAlign: 'center'
    },
    secondaryButton: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 2,
        marginTop: spacing.spacing0,
        marginHorizontal: spacing.spacing16,
        marginBottom: spacing.spacing16
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 2,
        marginTop: spacing.spacing32,
        backgroundColor: colors.color_0282F0,
        marginHorizontal: spacing.spacing16,
        marginBottom: spacing.spacing20
    },
    buttonText: {
        ...TextStyle.text_16_bold,
        margin: 12,
        textAlign: 'center',
        color: colors.white
    },
    couponView: {
        borderStyle: 'dashed',
        borderWidth: 2,
        borderColor: colors.blue028,
        borderRadius: 4,
        flexDirection: 'row',
        marginHorizontal: spacing.spacing16,
        backgroundColor: 'rgba(2, 130, 240, 0.08)',
        marginTop: spacing.spacing24,
        marginBottom: spacing.spacing16
    },
    bottomNoteText: {
        marginTop: spacing.spacing24,
        marginHorizontal: spacing.spacing24,
        alignSelf: 'center'
    }
})
