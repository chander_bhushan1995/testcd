import React from 'react'
import { StyleSheet, Text, View, Dimensions, TouchableOpacity } from 'react-native'
import colors from '../../../Constants/colors'
import { TextStyle } from '../../../Constants/CommonStyle'
import spacing from '../../../Constants/Spacing'
import LinearGradient from 'react-native-linear-gradient'
import { AppForAppFlowStrings } from '../../Constant/AppForAllConstants'



const UsedExpiredRewards = props => {
    const title = props.title
    const description = props.description
    const isViewAllEnabled = props.isViewAllEnabled
    const isExpired = props.isExpired
    return (
        <View style={styles.container}>
            <View style={styles.seperatorLine} />
            <Text style={styles.title}>{title}</Text>
            <Text style={[styles.description,{color: isExpired ? colors.redBF0 : colors.grey,}]}>{description}</Text>
            {isViewAllEnabled
                ? <LinearGradient style={styles.buttonContainer} colors={['rgba(255,255,255,0)', 'rgba(255,255,255,0.9)']}>
                    <TouchableOpacity style={styles.button}>
                        <Text style={{ ...TextStyle.text_14_bold, color: colors.blue028 }}>{AppForAppFlowStrings.viewAll}</Text>
                    </TouchableOpacity>
                </LinearGradient>
                : null}
            <View style={styles.seperatorLine} />
        </View>
    )
}

export default UsedExpiredRewards

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white
    },
    seperatorLine: {
        backgroundColor: colors.color_D8D8D8,
        height: 1,
        width: Dimensions.get('screen').width
    },
    title: {
        ...TextStyle.text_16_normal,
        color: colors.charcoalGrey,
        marginHorizontal: spacing.spacing16,
        marginTop: spacing.spacing20
    },
    description: {
        ...TextStyle.text_12_normal,
        marginHorizontal: spacing.spacing16,
        marginBottom: spacing.spacing20
    },
    button: {
        paddingHorizontal: spacing.spacing22,
        paddingVertical: spacing.spacing10,
        backgroundColor: colors.white,
        borderRadius: 4,
        borderColor: colors.greye0,
        borderWidth: 1
    },
    buttonContainer: {
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center'
    }

})
