import { NativeModules, Alert } from "react-native";

const nativeBridgeRef = NativeModules.ChatBridge;

export const ButtonsAction = {
    Open_ID_Fence_Plan: "Open_ID_Fence_Plan",
    Claim_ID_Fence: "Claim_ID_Fence",
    Open_Scratch_Card: "Open_Scratch_Card",
    View_Dashboard: "View_Dashboard",
}

//all actions handle for rewards screen
export const handleRewardBtnActions = (buttonActions, data) => {
    switch (buttonActions) {
        case ButtonsAction.Open_Scratch_Card:
            nativeBridgeRef.openScratchCard(data.state)
            break
        default:
            break;
    }
}
