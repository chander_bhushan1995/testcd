import { HTTP_GET, executeApi } from "../../../network/ApiManager";
import {APIData} from '../../../../index';

import { CHECK_SUBSCRIPTION_STATUS } from "../../../Constants/ApiUrls";

const getDataFromApiGateway = (apiParams) => {
    let params = APIData;
    apiParams.apiProps = { baseUrl: params?.api_gateway_base_url, header: params?.apiHeader };
    return apiParams;
};

export const checkSubscriptionStatus = (memUUID, callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: CHECK_SUBSCRIPTION_STATUS(memUUID),
        callback: callback
    };
    executeApi(getDataFromApiGateway(apiParams));
};


