import React, {useEffect} from 'react'
import { createStackNavigator } from "react-navigation";
import MyRewardsScreen from '../Screens/MyRewardsScreen';
import colors from "../../../Constants/colors";
import { Alert } from "react-native";
import codePush from "react-native-code-push";
import {setAPIData, updateAPIData} from "../../../../index";

const Navigator = createStackNavigator({
    MyRewardsScreen: {
        screen: MyRewardsScreen,
        navigationOptions: {
            gesturesEnabled: false,
            headerTitleStyle: {
                color: colors.charcoalGrey,
            },
            headerTintColor: colors.blue,
        }
    },
}, {
});

const MyRewardsNavigationStack = props => {
    setAPIData(props);

    useEffect(() => {
        updateAPIData(props)
    }, [props])
    
    return <Navigator screenProps={props} />
}

export default codePush(MyRewardsNavigationStack);
