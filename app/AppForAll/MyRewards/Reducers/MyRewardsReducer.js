import * as Actions from './ActionsTypes'
import { parseJSON, deepCopy} from "../../../commonUtil/AppUtils";

//INITIAL STATE
export const initialState = {
    data: deepCopy({ "data": "HomeScreenData" }).data,
    cardsResponse: null,
    cardsData: null,
};


export const RewardState = {
    Locked: 'Locked',
    Unlocked: 'Unlocked',
    Expired: 'Expired'
}


export const MyRewardsReducer = (state = initialState, action) => {

    switch (action.type) {
        case Actions.REQUEST_REWARDS_DATA:
        break
        default:
            return { ...state };
    }

};
export default MyRewardsReducer;
