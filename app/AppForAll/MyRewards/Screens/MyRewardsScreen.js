import React, {useEffect, useRef} from 'react';
import {
    BackHandler,
    FlatList,
    Image,
    NativeEventEmitter,
    NativeModules,
    Platform,
    SafeAreaView,
    SectionList,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import colors from '../../../Constants/colors';
import BlockingLoader from '../../../CommonComponents/BlockingLoader';
import useLoader from '../../../CardManagement/CustomHooks/useLoader';
import spacing from '../../../Constants/Spacing';
import images from '../../../images/index.image';
import RewardCards from '../Components/RewardCards';
import UnlockedRewards from '../Components/UnlockedRewards';
import {TextStyle} from '../../../Constants/CommonStyle';
import UsedExpiredRewards from '../Components/UsedExpiredRewards';
import {
    RewardStats
} from '../../Constant/ScreensStaticData/IDFenceData';
import DialogView from '../../../Components/DialogView';
import {AppForAppFlowStrings} from '../../Constant/AppForAllConstants';
import {APIData, updateAPIData} from "../../../../index";
import useForceUpdate from '../../../CardManagement/CustomHooks/useForceUpdate';

const nativeBridgeRef = NativeModules.ChatBridge;
const eventEmitter = new NativeEventEmitter(nativeBridgeRef);

export let showAlert = null

export default function MyRewardsScreen(props) {
    const {
        isNonSubscriber,
        isAnsweredAllQuestions,
        rewardScratchDate,
        rewardState = RewardStats.LOCKED
    } = APIData ?? {} 

    const [forceUpdate] = useForceUpdate()
    
    console.ignoredYellowBox = ['Warning:'];
    console.disableYellowBox = true;

    const [isBlockingLoader, blockingLoaderMessage, startBlockingLoader, stopBlockingLoader] = useLoader();

    const alertRef = useRef(null);
    showAlert = (title, alertMessage, primaryButton = { text: 'Yes', onClick: () => { } }, secondaryButton, checkBox) => {
        alertRef.current.showDailog({
            title: title,
            message: alertMessage,
            primaryButton: primaryButton,
            secondaryButton: secondaryButton,
            checkBox: checkBox,
            cancelable: true,
            onClose: () => { },
        });
    };

    useEffect(() => {
        BackHandler.addEventListener("hardwareBackPress", onBackPress);
        eventEmitter.addListener("rewardStateChange", params => { //listner to update state when coupon scratched from native screen
            updateAPIData({
                rewardScratchDate: params?.rewardScratchDate,
                rewardState: params?.rewardState
            })
            forceUpdate()
        });
        return () => {
            BackHandler.removeEventListener(onBackPress)
            eventEmitter.removeListener("rewardStateChange", () => {
            })
        }
    }, [rewardState])

    return (
        <View style={{ flex: 1 }}>
            <BlockingLoader visible={isBlockingLoader} loadingMessage={blockingLoaderMessage} />
            {/* if user answered allquestions and there are no scratch date but id fence in membership then show no rewards else show reward card */}
            {(isAnsweredAllQuestions && rewardScratchDate && isNonSubscriber) ? <MyRewards /> : <NoRewards />}
            <DialogView ref={alertRef} />
        </View>
    );
}

const NoRewards = props => { // no reward view
    return (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
            <Image source={images.ic_giftEmpty} style={{ height: 64, width: 58, resizeMode: 'contain' }} />
            <Text style={styles.noRewardText}>
                {AppForAppFlowStrings.noRewardsToUnlock}
            </Text>
        </View>
    )
}


const MyRewards = props => { //rewards state view

    const lockedRewards = []    //an array that contains all rewards which are in locked state
    const unlockedRewards = [] // an array that contains all unlocked rewards which are in unlocked state
    const usedExpiredRewards = [] //an array that contains all rewards which are in used or expired state

    // switch (idFenceRewardState) { //calculate of prepare reward data based on some of the conditions and state
    //     case RewardStats.LOCKED:
    //     case RewardStats.EARNED:
    //         break;
    //     case RewardStats.CLAIMED:
    //         break;
    //     case RewardStats.UNLOCKED:
    //         break;
    //     case RewardStats.USED:
    //     case RewardStats.EXPIRED:
    //         break;
    // }

    const RenderRewardsItem = props => { // a grid list to show locked reward cards
        return <FlatList
            style={{ marginHorizontal: 16 }}
            keyExtractor={(item, index) => toString(index)}
            data={lockedRewards}
            renderItem={({ item }) => <RewardCards image={images.reward} tag={"NEW"} />}
            numColumns={2}
            ItemSeparatorComponent={props => <View style={{ width: 8, backgroundColor: 'transparent' }} />}
            bounces={false}
            columnWrapperStyle={{ justifyContent: "space-between" }}
            alwaysBounceVertical={false}
        />
    }

    const RenderUnlocked = (props) => { // a list to show unlocked rewards that may or may not claimed according to current scenario

        return <FlatList
            style={{ marginHorizontal: 16 }}
            keyExtractor={(item, index) => toString(index)}
            data={unlockedRewards}
            renderItem={({ item, index }) => {
                if (index == 0) {
                    return <UnlockedRewards title={item.title} image={images.reward}
                        state={idFenceRewardState}
                        description={item.description}
                        buttonText={item.buttonText} buttonAction={item.buttonAction}
                        secondBtnText={item.secondaryButtonText} secondBtnAction={item.secondaryButtonAction}
                        validTill={item.validTill}
                    />
                }
            }}
            horizontal={false}
            ItemSeparatorComponent={props => <View style={{ height: 16, backgroundColor: 'transparent' }} />}
            bounces={false}
        />
    }

    const RenderUsedExpired = props => { // list to show used or expired rewards
        return <FlatList
            keyExtractor={(item, index) => toString(index)}
            data={usedExpiredRewards}
            renderItem={({ item, index }) => <UsedExpiredRewards title={item.title}
                description={item.usedExpiredText} isViewAllEnabled={index == 2} isExpired={item.isExpired}
            />}
            horizontal={false}
            bounces={false}
        />
    }

    const _renderSectionHeader = ({ section }) => {
        if (section.data.length == 0)
            return null

        return (
            <View>
                <View style={{ height: 32 }} />
                {section.title.length > 0 ?
                    <View style={styles.sectionHeader}>
                        <Text style={[TextStyle.text_10_bold, { color: colors.grey }]}>{section.title}</Text>
                    </View>
                    : null}
            </View>
        )
    }

    return (
        <SafeAreaView>
            <SectionList                //section list to sections for each state's list of reward
                stickySectionHeadersEnabled={false}
                showsVerticalScrollIndicator={false}
                sections={[
                    { title: '', data: lockedRewards },
                    { title: AppForAppFlowStrings.unlockedRewards, data: unlockedRewards },
                    { title: AppForAppFlowStrings.usedExpiredRewards, data: usedExpiredRewards },
                ]}
                renderItem={({ item, index, section }) => {
                    if (section.title == AppForAppFlowStrings.unlockedRewards) {
                        return <RenderUnlocked data={item} />
                    }
                    else if (section.title == AppForAppFlowStrings.usedExpiredRewards) {
                        return <RenderUsedExpired />
                    } else {
                        return <RenderRewardsItem />
                    }
                }}
                renderSectionHeader={_renderSectionHeader}
                keyExtractor={(item, index) => toString(index)}
            />
        </SafeAreaView>
    )
}

const BackButton = ({ onPress }) => (   // back button of navigation bar
    <TouchableOpacity onPress={
        () => {
            nativeBridgeRef.goBack()
        }
    }>
        <Image
            source={images.backImage}
            style={{
                height: 30,
                width: 30,
                marginLeft: 9,
                marginRight: 12,
                marginVertical: 12,
                resizeMode: 'contain',
            }}
        />
    </TouchableOpacity>
);

MyRewardsScreen.navigationOptions = (navigationData) => {   // customization of navigation bar of the screen
    return {
        headerLeft: <BackButton />,
        headerTitle: (
            <View style={Platform.OS === 'ios' ? { alignItems: 'center' } : { alignItems: 'flex-start' }}>
                <Text style={TextStyle.text_16_bold}>{AppForAppFlowStrings.myRewardsTitle}</Text>
            </View>
        ),
    };
};

function onBackPress() {
    nativeBridgeRef.goBack();
};




const styles = StyleSheet.create({

    sectionHeader: {
        justifyContent: 'flex-end',
        marginBottom: spacing.spacing12,
        marginHorizontal: spacing.spacing16,
    },
    noRewardText: {
        ...TextStyle.text_18_normal,
        marginHorizontal: spacing.spacing32,
        marginTop: spacing.spacing32,
        color: colors.grey,
        textAlign: 'center'
    }
})

