import React, {useEffect, useState} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    SafeAreaView, NativeModules, FlatList, Image, TouchableOpacity,
} from 'react-native';
import colors from '../../../Constants/colors';
import {HeaderBackButton} from 'react-navigation';
import {CommonStyle, SafeAreaStyle, TextStyle} from '../../../Constants/CommonStyle';
import useBackHandler from '../../../CardManagement/CustomHooks/useBackHandler';
import useCommonComponent from '../CustomeHooks/useCommonComponent';
import MyServiceRequestsParser from './MyServiceRequestsParser';
import {getSODAppliancesData} from '../../../commonUtil/AppUtils';
import spacing from '../../../Constants/Spacing';
import ButtonWithLoader from '../../../CommonComponents/ButtonWithLoader';
import { TabIndex } from "../../../Constants/AppConstants";

const bridgeRef = NativeModules.ChatBridge;

export default function MyServiceRequestsComponent(props) {
    const {navigation} = props;
    const {state} = useCommonComponent();
    const {serviceRequestsList, showEmptyList} = state;
    const [servicesData, setServicesData] = useState([]);
    useEffect(() => {
        updateNavigationHeader();
    }, []);

    useEffect(() => {
        initData();
    }, [serviceRequestsList]);

    useBackHandler(() => bridgeRef.goBack());

    const initData = () => {
        getSODAppliancesData((sodAppliancesMapping) => {
            const myServiceRequestsParser = new MyServiceRequestsParser();
            setServicesData(myServiceRequestsParser.getMyServiceRequestsData(sodAppliancesMapping, serviceRequestsList).list);
        });
    };

    const updateNavigationHeader = () => {
        navigation.setParams({
            headerTitle: 'Quick Repair Bookings',
        });
    };

    const renderItem = ({item, index}) => {
        return <item.ComponantName data={item.componentData}/>;
    };

    const getEmptyRequestView = () => {
        return serviceRequestsList?.length === 0  && showEmptyList?
            <View style={{alignItems: 'center', flex:1}}>
                <Image source={require('../../../images/fogg_bg.webp')}
                       style={styles.emptyRequestBg}/>
                <Text style={[TextStyle.text_14_normal, {marginTop: spacing.spacing16, marginBottom: spacing.spacing24}]}>Time to make a start!</Text>
                <View style={{marginLeft: spacing.spacing16, marginRight: spacing.spacing16, width: '90%'}}>
                <ButtonWithLoader
                    isLoading={false}
                    enable={true}
                    Button={{
                        text: 'Let’s book a quick repair',
                        onClick: () => {
                            bridgeRef.openTabBarWithIndex(TabIndex.newHome)
                        },
                    }
                    }/>
                </View>
            </View> : null;
    };

    return (
        <SafeAreaView style={SafeAreaStyle.safeArea}>
            <View style={styles.container}>
                {getEmptyRequestView()}
                <FlatList
                    renderItem={renderItem}
                    data={servicesData}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={() => {
                        return <View style={CommonStyle.separator}/>;
                    }}
                />
            </View>
        </SafeAreaView>
    );
}

MyServiceRequestsComponent.navigationOptions = ({navigation}) => {
    return {
        headerTitle: (
            <View style={Platform.OS === 'ios' ? {alignItems: 'center'} : {alignItems: 'flex-start'}}>
                <Text style={TextStyle.text_18_bold}>Quick Repair Bookings</Text>
            </View>
        ),
        headerLeft: (
            <TouchableOpacity onPress={
                () => {
                    bridgeRef.goBack()
                }
            }>
                <Image
                    source={require('../../../images/ic_back.webp')}
                    style={{
                        height: 24,
                        width: 30,
                        marginLeft: 12,
                        marginRight: 12,
                        marginVertical: 12,
                        resizeMode: 'contain',
                    }}
                />
            </TouchableOpacity>
        ),
    };
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.color_F6F6F6,
    },
    emptyRequestBg: {
        width: '70%',
        height: 184,
        resizeMode: 'contain',
    },
});
