import React from 'react';
import {
    StyleSheet, Text, View,
} from 'react-native';
import spacing from '../../../Constants/Spacing';
import {TextStyle} from '../../../Constants/CommonStyle';

export default function MyServiceRequestsHeaderComponent(props) {

    return (
        <View>
            <Text style={[TextStyle.text_10_bold, styles.headerStyle]}>{props?.data?.text}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    headerStyle: {
        marginTop: spacing.spacing28,
        marginBottom: spacing.spacing12,
        marginLeft: spacing.spacing12,
    },
});
