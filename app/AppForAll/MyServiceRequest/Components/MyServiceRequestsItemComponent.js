import React from 'react';
import {
    Image, NativeModules,
    StyleSheet, Text, TouchableOpacity, View,
} from 'react-native';
import colors from '../../../Constants/colors';
import spacing from '../../../Constants/Spacing';
import {TextStyle} from '../../../Constants/CommonStyle';
import {ReportTypes, AppForAppFlowStrings} from '../../Constant/AppForAllConstants';

const nativeBrigeRef = NativeModules.ChatBridge;
import { parseJSON, deepCopy, getServiceName} from '../../../commonUtil/AppUtils';
import {APPLIANCE, SERVICE_TYPE, SR_NUMBER} from '../../../Constants/WebengageAttrKeys';
import * as WebengageKyes from "../../../Constants/WebengageAttrKeys";
import {logWebEnageEvent} from '../../../commonUtil/WebengageTrackingUtils';
import {downloadAlertFileUrl} from "../../../idfence/constants/Urls";
import {alertReportFileName, AlertsTabConstants} from "../../../idfence/constants/Constants";
import {downloadReport} from "../DataLayer/ServiceRequestsAPIUrls";
import {Constants} from "react-native-svg-charts/lib/module/util";

const nativeBridgeRef = NativeModules.ChatBridge;
export default function MyServiceRequestsItemComponent(props) {

    const title = props?.data?.productInfo?.subCategoryName + " " + getServiceName(props?.data?.data?.serviceRequestType);
    const rightArrow = () => {
        return props?.data?.isRightArrow || props?.data?.data?.serviceRequestFeedback ? (
            <View style={styles.arrowContainerStyle}>
                <Image style={styles.arrowStyle} source={require('../../../images/right_arrow.webp')}/>
            </View>) : null;
    };

    const rightButton = () => {
        return props?.data?.rightButton ? (
            <TouchableOpacity onPress={() => handleRightBtnClick(props?.data?.data)}>
                <Text style={[TextStyle.text_14_bold, styles.ratingTextStyle]}>{props?.data?.rightButton?.text}</Text>
            </TouchableOpacity>) : null;
    };

    function handleRightBtnClick(itemData) {
        nativeBridgeRef.getApiProperty(apiProperty => {
            apiProperty = parseJSON(apiProperty);
            let url = apiProperty.api_gateway_base_url +
                downloadReport(itemData?.serviceRequestId, ReportTypes.jobsheet);
            nativeBridgeRef.downloadDocument(url,
                "INVOICE_SR_" + itemData.serviceRequestId+".pdf",
                AppForAppFlowStrings.downloadInvoice);
        });
    }

    const ratingView = () => {
        return (props?.data?.isRatingEnabled && (props?.data?.data?.serviceRequestFeedback === null || props?.data?.data?.serviceRequestFeedback === undefined)) ?
            <TouchableOpacity onPress={() => nativeBrigeRef.rateCompletedService(JSON.stringify(props?.data?.data))}>
                <Text style={[TextStyle.text_14_bold, styles.ratingTextStyle]}>Rate our service ></Text>
            </TouchableOpacity> : null;
    };

    const getOnHoldBadge = () => {
        return props?.data?.data?.status === 'OH' ?
            <View style={styles.onHoldContainerStyle}>
                <Text style={[TextStyle.text_12_normal, styles.onHoldTextStyle]}>On-Hold</Text>
            </View> : null;
    };

    //send SOD Event
    const itemClick = () => {
        let eventAttr = new Object();
        eventAttr[APPLIANCE] = props?.data?.data?.assets[0]?.productCode;
        eventAttr[SERVICE_TYPE] = getServiceName(props?.data?.data?.serviceRequestType);
        eventAttr[SR_NUMBER] = props?.data?.data?.refPrimaryTrackingNo;
        logWebEnageEvent(WebengageKyes.SOD_SR_DETAIL, eventAttr);
        nativeBrigeRef.showSRDetails(JSON.stringify(props?.data?.data))
    }

    const getSRIdText = () => {
        return props?.data?.data?.refPrimaryTrackingNo ?
            <Text
                style={[TextStyle.text_10_bold]}>{'REQUEST ID ' + props?.data?.data?.refPrimaryTrackingNo}</Text> : null;
    };

    return (
        <View style={styles.container}>
            <TouchableOpacity style={{flexDirection: 'row', flex: 1}} onPress={() => itemClick()}>
                <View
                    style={styles.applianceIconContainerStyle}>
                    <Image source={{uri: props?.data?.productInfo?.subCatImageUrl}}
                           style={styles.applianceIconStyle}/>
                </View>
                <View style={{marginRight: spacing.spacing20, flex: 1}}>
                    {getSRIdText()}
                    <Text
                        style={[TextStyle.text_16_bold, styles.titleStyle]}>{title}</Text>
                    <Text style={[TextStyle.text_12_medium]}>{props?.data?.subTitle}</Text>
                    {ratingView()}
                    {getOnHoldBadge()}
                </View>
                {rightArrow()}
                {rightButton()}
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
        flexDirection: 'row',
        paddingTop: spacing.spacing20,
        paddingBottom: spacing.spacing20,
        paddingLeft: spacing.spacing16,
        paddingRight: spacing.spacing12,
    },
    applianceIconContainerStyle: {
        width: 60,
        height: 60,
        justifyContent: 'center',
        marginRight: spacing.spacing16,
    },
    applianceIconStyle: {
        width: 36,
        height: 36,
        resizeMode: 'contain',
    },
    titleStyle: {
        marginTop: spacing.spacing8,
    },
    ratingTextStyle: {
        marginTop: spacing.spacing12,
        color: colors.color_0282F0,
    },
    arrowContainerStyle: {
        width: 24,
        height: 24,
        justifyContent: 'flex-end',
    },
    arrowStyle: {
        width: 8,
        height: 14,
        resizeMode: 'contain',
    },
    onHoldContainerStyle: {
        borderWidth: 1,
        paddingTop: spacing.spacing4,
        paddingBottom: spacing.spacing4,
        borderColor: colors.color_FF9800,
        backgroundColor: colors.color_FFF2E1,
        marginTop: spacing.spacing8,
        borderRadius: spacing.spacing2,
        flexWrap: 'wrap',
        width: '25%',
    },
    onHoldTextStyle: {
        color: colors.color_FF9800,
        alignSelf: 'center',
    },
});
