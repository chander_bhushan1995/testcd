import MyServiceRequestsHeaderComponent from './MyServiceRequestsHeaderComponent';
import MyServiceRequestsItemComponent from './MyServiceRequestsItemComponent';
import {DOWNLOAD_INVOICE} from "../DataLayer/Actions";

let sodAppliancesDataMapping = {};

export default class MyServiceRequestsParser {

    getMyServiceRequestsData(sodAppliancesMapping, response) {
        let components = [];
        sodAppliancesDataMapping = sodAppliancesMapping;
        let sodAppliancesSubcategories = (sodAppliancesDataMapping?.subCategories ?? []).concat(sodAppliancesDataMapping?.otherSubCategories?.subCategories ?? [])
        const servicesPending = response.filter(data => ((data.status === 'P' || data.status === 'IP' || data.status === 'OH')));
        const servicesCompleted = response.filter(data => (data.status === 'CO'));
        if (servicesPending.length > 0) {
            components.push({
                ComponantName: MyServiceRequestsHeaderComponent,
                componentData: {text: servicesPending.length + ' ACTIVE REQUESTS'},
            });
            servicesPending.map(item => {
                this.extractComponent(item, components, sodAppliancesSubcategories);
            });
        }

        if (servicesCompleted.length > 0) {
            components.push({
                ComponantName: MyServiceRequestsHeaderComponent,
                componentData: {text: 'COMPLETED REQUESTS (' + servicesCompleted.length + ')'},
            });
            servicesCompleted.map(item => {
                this.extractComponent(item, components, sodAppliancesSubcategories);
            });
        }
        return {list: components};
    }

    extractComponent(item, components, sodAppliancesSubcategories) {
        if (item !== null && item !== undefined && item?.assets !== null && item?.assets !== undefined) {
            let componentData = {data: item};
            const product = sodAppliancesSubcategories?.filter(data => data.subCategoryCode === item?.assets[0]?.productCode);
            if (item.status === 'P' || item.status === 'IP' || item.status === 'OH') {
                const issueArray = item?.workflowData?.visit?.issueReportedByCustomer;
                let issueDescription = '';
                for (let i = 0; i < issueArray?.length; i++) {
                    issueDescription = issueDescription + issueArray[i]?.issueDescription + (i === (issueArray?.length - 1) ? '' : ', ');
                }
                componentData = {
                    ...componentData,
                    isRightArrow: true,
                    isRatingEnabled: false,
                    productInfo: product[0],
                    subTitle: issueDescription,
                };
            } else {
                const date = item?.actualEndDateTime?.split('-');
                const subTitle = (date !== undefined && date[0] !== undefined && date[1] !== undefined) ? 'Completed on ' + date[0] + ' ' + date[1] : '';
                let rightBtnData = null;
                if(item?.showDownloadJobsheet === true){
                    rightBtnData = {
                        text: "Download Invoice",
                        action: DOWNLOAD_INVOICE
                    }
                }
                componentData = {
                    ...componentData,
                    isRightArrow: false,
                    isRatingEnabled: true,
                    productInfo: product[0],
                    subTitle: subTitle,
                    rightButton: rightBtnData
                };
            }
            components.push({
                ComponantName: MyServiceRequestsItemComponent,
                componentData: componentData,
            });
        }
    }
}
