import { useContext } from "react";
import {ServiceRequestsContext} from '../Navigation/index.myservicerequests';


export default function useCommonComponent() {
   return useContext(ServiceRequestsContext)
}
