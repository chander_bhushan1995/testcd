import {serviceRequests} from './ServiceRequestsAPIUrls';
import {APIData} from '../../../../index';

import { HTTP_GET, executeApi } from '../../../network/ApiManager';

const getDataFromApiGateway = (apiParams) => {
    let params = APIData;
    apiParams.apiProps = {baseUrl: params?.api_gateway_base_url, header: params?.apiHeader};
    return apiParams;
};

const custId = () => {
    return APIData?.customer_id;
};

export const getServiceRequests = (callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: serviceRequests + custId() + '&options=serviceOrderDetails,showAssets',
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams));
};
