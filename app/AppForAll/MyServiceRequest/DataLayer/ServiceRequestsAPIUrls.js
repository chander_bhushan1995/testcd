export const serviceRequests = 'serviceplatform/api/servicerequests?customerId=';
export const downloadReport = (srID, reportType) => `/serviceplatform/api/servicerequests/${srID}/documents?reportType=${reportType}`;

