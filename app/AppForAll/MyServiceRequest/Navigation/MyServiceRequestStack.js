import {createStackNavigator} from 'react-navigation';
import MyServiceRequestsComponent from '../Components/MyServiceRequestsComponent';
import colors from '../../../Constants/colors';
import QuestionsGenericScreen from '../../MyAccount/Actions/QuestionsGenericAction';
import SelectBankScreen from '../../MyAccount/Components/SelectBankScreen';

const navigator = createStackNavigator({
    MyServiceRequestsComponent: {
        screen: MyServiceRequestsComponent,
        // navigationOptions: {
        //     gesturesEnabled: false,
        //     title: "Enter your details",
        //     headerTitleStyle: {
        //         color: colors.charcoalGrey,
        //     },
        //     headerStyle: {
        //         backgroundColor: colors.white
        //     },
        //     headerTintColor: colors.blue
        // }
    },
});

// const navigator1 = createStackNavigator({
//     Main: {
//         screen: navigator,
//     },
// }, {
//     mode: 'modal',
//     headerMode: 'none',
//     initialRouteName: 'Main',
// });

const MyServiceRequestsNavigator = createStackNavigator(
    {
        MyServiceRequestsComponent: {
            screen: MyServiceRequestsComponent,
        },
    },{
        transitionConfig: () => ({ screenInterpolator: () => null }),
    });

export default MyServiceRequestsNavigator;

// export default navigator1;
