import React, {useRef, useReducer, useEffect} from 'react';
import {GET_SERVICE_REQUESTS, ERROR} from '../DataLayer/Actions';
import useLoader from '../../../CardManagement/CustomHooks/useLoader';
import * as ServiceRequestsAPI from '../DataLayer/ServiceRequestsAPIHelper';
import BlockingLoader from '../../../CommonComponents/BlockingLoader';
import {MenuProvider} from 'react-native-popup-menu';
import AppNavigator from './MyServiceRequestStack';
import DialogView from '../../../Components/DialogView';
import codePush from "react-native-code-push";
import {setAPIData, updateAPIData} from "../../../../index";

let data = [{
    'status': 'P',
    'serviceRequestId': '120',
}, {
    'status': 'C',
    'serviceRequestId': '121',
}, {
    'status': 'P',
    'serviceRequestId': '122',
}, {
    'status': 'C',
    'serviceRequestId': '123',
}, {
    'status': 'P',
    'serviceRequestId': '124',
}];

export const ServiceRequestsContext = React.createContext();

const initialState = {
    serviceRequestsList: [],
    showEmptyList: false,
    error: null,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_SERVICE_REQUESTS:
            return {
                ...state,
                serviceRequestsList: action.data ?? [],
                showEmptyList: true
            };
        case ERROR:
            return {
                ...state,
                showEmptyList: true
            };
        default:
            return state;
    }
};

function Root(props) {
    setAPIData(props);
    console.ignoredYellowBox = ['Warning:'];
    console.disableYellowBox = true;

    const [isBlockingLoader, blockingLoaderMessage, startBlockingLoader, stopBlockingLoader] = useLoader();
    const alertRef = useRef(null);
    const showAlert = (title, alertMessage, primaryButton = {
        text: 'Yes', onClick: () => {
        }
    }, secondaryButton, checkBox) => {
        alertRef.current.showDailog({
            title: title,
            message: alertMessage,
            primaryButton: primaryButton,
            secondaryButton: secondaryButton,
            checkBox: checkBox,
            cancelable: true,
            onClose: () => {
            },
        });
    };

    const [state, serviceRequestsDispatch] = useReducer(reducer, initialState);

    useEffect(() => {
        updateAPIData(props)
    }, [props])

    useEffect(() => {
        startBlockingLoader('Fetching Requests');
        getServiceRequests();
    }, []);

    const getServiceRequests = () => {
        ServiceRequestsAPI.getServiceRequests((response, error) => {
            stopBlockingLoader();
            if (response?.data) {
                serviceRequestsDispatch({type: GET_SERVICE_REQUESTS, data: response.data});
            } else {
                showAlert("Something went wrong!", "There are no service requests", {text: "Ok"})
            }
        });
    }

    return (
        <ServiceRequestsContext.Provider value={{
            Alert: {showAlert: showAlert},
            BlockingLoader: {startLoader: startBlockingLoader, stopLoader: stopBlockingLoader},
            state: state,
        }}>
            <BlockingLoader visible={isBlockingLoader} loadingMessage={blockingLoaderMessage}/>
            <MenuProvider>
                <AppNavigator/>
            </MenuProvider>
            <DialogView ref={alertRef}/>
        </ServiceRequestsContext.Provider>
    );
}

export default codePush(Root);
