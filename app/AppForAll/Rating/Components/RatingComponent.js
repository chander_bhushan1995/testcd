import React, { useState, useEffect, useContext, useRef } from 'react';
import {
    Image, NativeModules, Platform, ScrollView, Text, View,
    SafeAreaView, StyleSheet, TouchableOpacity, Linking
} from 'react-native';
import BackButton from '../../../CommonComponents/NavigationBackButton';
import colors from '../../../Constants/colors';
import useBackHandler from '../../../CardManagement/CustomHooks/useBackHandler';
import { TextStyle } from "../../../Constants/CommonStyle";
import {RatingContext} from '../Navigation/index.rating';
import {APIData} from '../../../../index';
import spacing from "../../../Constants/Spacing";
import * as RatingAPI from '../DataLayer/RatingAPIHelper';
import Loader from '../../../Components/Loader';
import { logWebEnageEvent } from '../../../commonUtil/WebengageTrackingUtils';
import {
    RATING_HEADER, PLATFORM_OS, RATE_APPSTORE, RATE_NOW,
    BACK_TO_HOME, RATING_NOTE, RATING_NOTE_2, THANKS_FOR_FEEDBACK
} from '../../../Constants/AppConstants';
import AirbnbRating from '../../../CustomComponent/Rating/AirbnbRating';
import ButtonWithLoader from '../../../CommonComponents/ButtonWithLoader';
import images from '../../../images/index.image';
import { BuybackEventLocation } from "../../../Constants/BuybackConstants";
import InputBox from '../../../Components/InputBox'
import RBSheet from "../../../Components/RBSheet";
import { TECHNICAL_ERROR_MESSAGE } from '../../../Constants/ActionTypes';
import * as StoreReview from "react-native-store-review";
import * as EventAttrs from '../../../Constants/WebengageAttrKeys';
import * as EventNames from '../../../Constants/WebengageEvents';

const bridgeRef = NativeModules.ChatBridge;

const RatedView = (props) => {

    const { needAppStoreRating = false, rateOnAppStore = () => { }, close = () => { } } = props;
    let confettiImage = images.reward_confetti
    let confettiText = RATE_APPSTORE(Platform.OS == PLATFORM_OS.android ? 'play store' : 'app store')
    let confettiButton = { text: RATE_NOW, action: rateOnAppStore }

    if (!needAppStoreRating) {
        confettiImage = images.star_confetti_normal
        confettiText = THANKS_FOR_FEEDBACK
        confettiButton = { text: BACK_TO_HOME, action: close }
    }

    return (<View>
        <TouchableOpacity style={style.crossTouch} onPress={close}>
            <Image style={style.crossIcon} source={require("../../../images/icon_cross.webp")} />
        </TouchableOpacity>
        <Image style={style.confetti} source={confettiImage} />
        <Text style={[TextStyle.text_14_normal, {
            color: colors.charcoalGrey,
            marginHorizontal: spacing.spacing40,
            textAlign: 'center',
        }]}>
            {confettiText}
        </Text>
        <View style={[style.primaryButtonContainer, {
            marginBottom: spacing.spacing32,
            marginTop: spacing.spacing24
        }]}>
            <ButtonWithLoader
                isLoading={false} enable={true}
                Button={{ text: confettiButton.text, onClick: confettiButton.action }}
            />
        </View>
    </View>)
}


export default function RatingComponent() {

    const { loader, Alert } = useContext(RatingContext);
    const { isLoading, loaderMessage } = loader
    const [rating, setRating] = useState(5);
    const [tags, setTags] = useState([]);
    const [feedbackText, setFeedbackText] = useState(null)
    const [isButtonLoading, setIsButtonLoading] = useState(false);
    const feedbackBottomSheet = useRef(null);

    useBackHandler(() => bridgeRef.goBack());

    const updateTagData = (tagId) => {
        let data = [];
        tags.map((obj) => {
            if (obj?.tagId == tagId) {
                obj.isSelected = !(obj.isSelected);
            }
            data.push(obj)
        })
        setTags(data);
    };

    const TagView = () => {
        return tags ? (<View style={style.tagContainer}>
            {tags.map((tag, key) => {
                const tagText = tag.tag ?? '';
                return (
                    <TouchableOpacity key={key}
                        style={[style.tagStyle, { backgroundColor: tag.isSelected ? colors.blue028 : colors.white }]}
                        onPress={() => updateTagData(tag.tagId)}>
                        <Text style={[TextStyle.text_12_bold, { color: tag.isSelected ? colors.white : colors.blue028 }]}>{tagText}</Text>
                    </TouchableOpacity>
                );
            },
            )}
        </View>) : null;
    };

    const getSelectedTags = () => {
        return tags.filter((item) => { return item.isSelected }) ?? [];
    }

    useEffect(() => {
        getBuybackRatingTags()
        // trackAppScreen(CATALYST_FAVOURITE_SCREENVIEW);
    }, [rating])

    const getBuybackRatingTags = () => {
        // startBlockingLoader()
        RatingAPI.getBuybackFeedbackTags(rating, (response, error) => {
            // stopBlockingLoader()
            if (response) {
                setTags(prepareBuybackTags(response.data))
            } else {
                Alert.showAlert('Unable to proceed', error[0]?.errorMessage ?? TECHNICAL_ERROR_MESSAGE, { text: 'OK', onClick: () => { bridgeRef.goBack() } });
            }
        })
    }

    const prepareBuybackTags = (responseData = []) => {
        let tags = []
        responseData?.forEach((item, index) => {
            let tagObject = {}
            tagObject.tag = item?.value
            tagObject.tagId = item?.valueId ?? index
            tagObject.isSelected = false
            tags.push(tagObject)
        })
        return tags
    }

    const onRatingSubmit = () => {
        let quoteId = APIData?.quoteId
        let isAbb = APIData?.isAbb
        let servedBy = APIData?.servedBy
        let selectedTags = getSelectedTags()
        let selectedTagIDs = selectedTags.map((item) => { return item.tagId })
        setIsButtonLoading(true);
        RatingAPI.submitRating(quoteId, rating, selectedTagIDs, feedbackText, (response, error) => { // api call for submission of rating
            setIsButtonLoading(false);
            if (response) {
                let webEngageAttr = new Object()
                webEngageAttr[EventAttrs.LOCATION] = BuybackEventLocation.BUYBACK
                webEngageAttr[EventAttrs.ABB] = isAbb
                webEngageAttr[EventAttrs.SERVED_BY] = servedBy
                webEngageAttr[EventAttrs.STAR_RATING] = rating
                webEngageAttr[EventAttrs.TAGS] = selectedTags.map((item) => { return item.tag }).join(',')
                logWebEnageEvent(EventNames.RATING_SUBMITTED, webEngageAttr)
                feedbackBottomSheet.current.open();
            } else {
                Alert.showAlert('Unable to proceed', error[0]?.errorMessage ?? TECHNICAL_ERROR_MESSAGE, { text: 'OK' });
            }
        });
    };

    const openStoreReview = () => {
        if (Platform.OS === PLATFORM_OS.iOS) {
            if (StoreReview.isAvailable) {
                StoreReview.requestReview();
            } else {
                bridgeRef.openURLiOS("itms://itunes.apple.com/in/app/one-assist-for-iphone/id1074619069?mt=8");
            }
        } else {
            Linking.openURL("market://details?id=com.oneassist");
        }
    }

    return (
        <SafeAreaView style={style.container}>
            <Loader isLoading={isLoading} loadingMessage={loaderMessage} />
            <View style={style.headerView}>
                <BackButton imageStyle={style.backImageStyle}
                    onPress={() => {
                        bridgeRef.getUserInfo(() => {
                            bridgeRef.refreshBuybackStatus();
                            bridgeRef.updateProfile();
                            bridgeRef.goBack();
                        })
                    }} />
                <View style={style.titleHeader} >
                    <Image style={style.chatIconStyle} source={require('../../../images/green_right.webp')}></Image>
                    <Text style={[TextStyle.text_14_bold, style.chatTextStyle]}>Your request is completed</Text>
                </View>
            </View>
            <ScrollView
                contentContainerStyle={style.scrollViewContainerStyle}
                horizontal={false}
                nestedScrollEnabled={true}
                bounces={Platform.OS === "ios"}
                style={{ flex: 1, backgroundColor: colors.color_F3F2F7 }}>
                <Text style={style.ratingTitle}>{RATING_HEADER}</Text>
                <AirbnbRating
                    count={5}
                    defaultRating={rating}
                    size={32}
                    showRating={false}
                    showSmallRating={true}
                    onFinishRating={setRating}
                />
                <Text style={style.ratingDescription}>{rating > 3 ? RATING_NOTE : RATING_NOTE_2}</Text>
                <TagView />
                <InputBox
                    containerStyle={style.textInputContainer}
                    inputHeading={"Leave a comment (optional)"}
                    multiline={true}
                    onChangeText={setFeedbackText}
                    height={90}
                    placeholder={"Tell us your feedback"}
                    editable={true}
                />
            </ScrollView>
            <RBSheet
                ref={feedbackBottomSheet}
                duration={10}
                height={280}
                closeOnSwipeDown={false}
                closeOnPressMask={false}
            >
                <RatedView needAppStoreRating={rating > 3}
                    rateOnAppStore={() => {
                        feedbackBottomSheet?.current?.close();
                        openStoreReview()
                        bridgeRef.getUserInfo(() => {
                            bridgeRef.refreshBuybackStatus();
                            bridgeRef.updateProfile();
                            bridgeRef.goBack();
                        })
                    }}
                    close={() => {
                        feedbackBottomSheet.current.close();
                        bridgeRef.getUserInfo(() => {
                            bridgeRef.refreshBuybackStatus();
                            bridgeRef.updateProfile();
                            bridgeRef.goBack();
                        })
                    }} />
            </RBSheet>
            <View style={style.primaryButtonContainer}>
                <ButtonWithLoader
                    isLoading={isButtonLoading}
                    enable={getSelectedTags().length}
                    Button={{ text: 'Submit', onClick: onRatingSubmit }}
                />
            </View>
        </SafeAreaView>
    );
}

export const style = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: colors.white
    },
    headerView: {
        elevation: 4,
        backgroundColor: colors.color_F6F6F6,
        marginBottom: 4,
        shadowOpacity: 0.1,
        width: '100%',
        shadowOffset: {
            width: 1,
            height: 3,
        },
        shadowColor: '#000000',
    },
    scrollViewContainerStyle: {
        flex: 1,
        backgroundColor: colors.white,
        paddingBottom: Platform.OS == PLATFORM_OS.ios ? spacing.spacing40 : 0
    },
    headerTitle: {
        fontSize: 16,
        fontFamily: "Lato",
        color: colors.charcoalGrey
    },
    tabBarTextStyle: {
        fontSize: 13,
        fontFamily: "Lato-Semibold",
        lineHeight: 20
    },
    tabBarUnderlineView: {
        backgroundColor: colors.blue028,
        height: spacing.spacing4,
        borderRadius: spacing.spacing2
    },
    tabContainerView: {
        borderBottomColor: colors.grey,
    },
    chatIconStyle: {
        width: 16,
        height: 16,
        marginTop: 3,
        resizeMode: 'contain',
    },
    chatTextStyle: {
        marginLeft: spacing.spacing6,
        textAlign: 'center',
        color: colors.seaGreen,
    },
    primaryButton: {
        marginTop: spacing.spacing32,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
    },
    container1: {
        marginTop: spacing.spacing16,
    },
    ratingTitle: {
        ...TextStyle.text_16_bold,
        color: colors.charcoalGrey,
        marginTop: 24,
        marginBottom: 16,
        marginHorizontal: 16,
        textAlign: 'center',
    },
    ratingDescription: {
        ...TextStyle.text_16_bold,
        color: colors.charcoalGrey,
        marginTop: spacing.spacing20,
        marginHorizontal: 16,
        textAlign: 'center',
    },
    primaryButtonContainer: {
        marginTop: spacing.spacing12,
        marginHorizontal: spacing.spacing16,
        marginBottom: spacing.spacing18,
    },
    confetti: {
        alignSelf: 'center',
        height: 64,
        width: 150,
        resizeMode: 'contain',
        marginBottom: 10,
    },
    tagContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: spacing.spacing16,
        marginBottom: spacing.spacing4,
        marginHorizontal: spacing.spacing32,
        justifyContent: 'center',
    },
    tagStyle: {
        marginHorizontal: spacing.spacing6,
        marginBottom: spacing.spacing8,
        paddingHorizontal: spacing.spacing24,
        paddingVertical: spacing.spacing8,
        borderRadius: 2,
        borderColor: colors.blue028,
        borderWidth: 1,
    },
    textInputContainer: {
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        marginTop: spacing.spacing32,
        marginBottom: spacing.spacing16
    },
    crossTouch: {
        width: 48,
        height: 48,
        marginRight: spacing.spacing0,
        marginTop: spacing.spacing0,
        padding: spacing.spacing16,
        alignSelf: "flex-end"
    },
    crossIcon: {
        width: 16,
        height: 16,
        tintColor: colors.black,
        resizeMode: "contain"
    },
    titleHeader: { 
        marginLeft: spacing.spacing12, 
        marginBottom: spacing.spacing24, 
        flexDirection: 'row' 
    },
    backImageStyle: {
        marginLeft: spacing.spacing8,
        marginTop: spacing.spacing8,
        tintColor: colors.black,
    }
});