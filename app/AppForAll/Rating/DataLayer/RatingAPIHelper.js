import { HTTP_GET, HTTP_POST, executeApi } from "../../../network/ApiManager";

import {APIData} from '../../../../index';

import { NEGATIVE_QUESTIONNAIRE, RATING_SUBMIT } from '../../../Constants/ApiUrls';

const getDataFromApiGateway = (apiParams) => {
    let params = APIData;
    apiParams.apiProps = { baseUrl: params?.api_gateway_base_url, header: params?.apiHeader };
    return apiParams;
};

const custId = () => {
    return APIData?.customer_id;
};

export const submitRating = (quoteId, rating, tags, feedbackComment, callback) => {
    let tagsString = (tags ?? []).join(',');
    let body = { quoteId: quoteId, feedbackRating: rating, feedbackCode: tagsString, feedbackComments: feedbackComment };
    let apiParams = {
        httpType: HTTP_POST,
        url: RATING_SUBMIT,
        requestBody: body,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getBuybackFeedbackTags = (rating, callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: NEGATIVE_QUESTIONNAIRE + rating,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
}