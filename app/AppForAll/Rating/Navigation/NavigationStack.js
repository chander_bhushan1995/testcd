import {createStackNavigator} from 'react-navigation';

import RatingComponent from '../Components/RatingComponent';
import Dialog from '../../../Components/DialogView';

const navigator = createStackNavigator({
    RatingComponent: {
        screen: RatingComponent,
        navigationOptions: {
            gesturesEnabled: false,
            header: null
        },
    },
}, {});

const navigator1 = createStackNavigator({
    Main: {
        screen: navigator,
    },
    Dialog: {
        screen: Dialog,
    },
}, {
    mode: 'screen',
    headerMode: 'none',
    initialRouteName: 'Main',
});

export default navigator1;
