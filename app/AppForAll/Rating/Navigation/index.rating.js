import React, { useRef, useReducer, useEffect } from 'react';
import AppNavigator from './NavigationStack';
import useLoader from '../../../CardManagement/CustomHooks/useLoader';
import DialogView from '../../../Components/DialogView';
import codePush from "react-native-code-push";
import {setAPIData, updateAPIData} from "../../../../index";

export const RatingContext = React.createContext();

function Root(props) {
    setAPIData(props);

    console.ignoredYellowBox = ['Warning:'];
    console.disableYellowBox = true;

    const [isLoading, loaderMessage, startLoading, stopLoader] = useLoader();

    const alertRef = useRef(null);

    useEffect(() => {
        updateAPIData(props)
    }, [props])
    
    const showAlert = (title, alertMessage, primaryButton = { text: 'Yes', onClick: () => { } }, secondaryButton, checkBox) => {
        alertRef.current.showDailog({
            title: title,
            message: alertMessage,
            primaryButton: primaryButton,
            secondaryButton: secondaryButton,
            checkBox: checkBox,
            cancelable: true,
            onClose: () => { },
        });
    };


    return (
        <RatingContext.Provider value={{
            loader: { isLoading: isLoading, loaderMessage: loaderMessage },
            Alert: { showAlert: showAlert }
        }}>
            <AppNavigator />
            <DialogView ref={alertRef} />
        </RatingContext.Provider>
    );
}

export default codePush(Root);
