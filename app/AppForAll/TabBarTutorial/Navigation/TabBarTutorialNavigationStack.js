import { createStackNavigator } from "react-navigation";
import TabBarTutorialScreen from '../Screens/TabBarTutorialScreen';

const navigator = createStackNavigator({
    TabBarTutorialScreen: {
        screen: TabBarTutorialScreen,
        navigationOptions: {
            header: null,
            mode: "modal",
            transitionConfig: () => ({
                containerStyle: {
                    backgroundColor: 'transparent'
                }
            })
        },

    },

}, {
});

export default navigator;
