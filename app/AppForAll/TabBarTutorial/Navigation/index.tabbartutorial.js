import React from "react";
import {connect, Provider} from "react-redux";
import {createReactNavigationReduxMiddleware, reduxifyNavigator} from 'react-navigation-redux-helpers'
import AppNavigator from "./TabBarTutorialNavigationStack";
import {NavigationActions} from "react-navigation";
import {Platform, YellowBox} from 'react-native';
import {applyMiddleware, combineReducers, createStore} from 'redux';

const INITIAL_PROPS = 'initial_props';
const initialState = {
    ...AppNavigator.router.getStateForAction(
        AppNavigator.router.getActionForPathAndParams("TabBarTutorialScreen"))
};

YellowBox.ignoreWarnings([
    "Warning: isMounted(...) is deprecated",
    "Module RCTImageLoader"
]);

const navReducer = (state = initialState, action) => {

    if (action.type === NavigationActions.SET_PARAMS) {
        try {
            const lastRoute = state.routes[0].routes.find(route => route.routeName === action.params.routeName);
            action.key = lastRoute.key;
        } catch (e) {
        }
    }
    const nextState = AppNavigator.router.getStateForAction(action, state);
    // Simply return the original `state` if `nextState` is null or undefined.
    return nextState || state;
};

// This is a reducer
const appData = (state = initialState, action) => {
    switch (action.type) {
        case INITIAL_PROPS:
            return {...state, params: action.initialProps};
        default:
            return {...state};
    }
}

const appReducer = combineReducers({
    nav: navReducer,
    appData
});

// Note: createReactNavigationReduxMiddleware must be run before reduxifyNavigator
const middleware = createReactNavigationReduxMiddleware(
    "TabBarTutorialScreen",
    state => state.nav,
);

const App = reduxifyNavigator(AppNavigator, "TabBarTutorialScreen");
const mapStateToProps = (state) => ({
    state: state.nav
});

const AppWithNavigationState = connect(mapStateToProps)(App);

const store = createStore(
    appReducer,
    applyMiddleware(middleware),
);

export default class Root extends React.Component {
    constructor(props) {
        super(props);
        initialState.routes[0].routeName = "TabBarTutorialScreen"
    }

    render() {
        return (
            <Provider store={store}>
                <AppWithNavigationState/>
            </Provider>
        );
    }
}
