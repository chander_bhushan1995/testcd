
import React, { useState, useEffect } from 'react'
import {
    StyleSheet, Text, View, Image,
    SafeAreaView, Dimensions, Animated,
    NativeModules, TouchableOpacity, Platform
} from 'react-native';
import colors from '../../../Constants/colors';
import images from '../../../images/index.image';
import spacing from '../../../Constants/Spacing';
import { TextStyle } from '../../../Constants/CommonStyle';
import { HomeScreenTutorialData, HomeScreenTutorialDataNew } from '../../Constant/ScreensStaticData/HomeScreenTutorialData';
import Carousel, { Pagination } from 'react-native-snap-carousel'
import codePush from "react-native-code-push";
import { parseJSON } from "../../../commonUtil/AppUtils";
import {APIData, setAPIData, updateAPIData} from "../../../../index";

const nativeBrigeRef = NativeModules.ChatBridge;

const TabBarTutorialScreen = props => {
    setAPIData(props);
    let customer_id = APIData?.customer_id;

    const [catalystVariationGroup, setCatalystVariationGroup] = useState(false);
    const [showCatalystRecommendations, setShowCatalystRecommendations] = useState(false);
    
    const [heightAnimation] = useState(new Animated.Value(80))
    const [opacityAnimation] = useState(new Animated.Value(0))

    useEffect(() => {
        updateAPIData(props)
    }, [props])

    useEffect(() => {
        startAnimation()
    }, [heightAnimation, opacityAnimation, startAnimation])

    useEffect(() => {
        nativeBrigeRef.getRecommendationProps(data => {
            let recommendationProps = parseJSON(data);
            setShowCatalystRecommendations(recommendationProps.show_catalyst_recommendations)
            setCatalystVariationGroup(recommendationProps.catalyst_user_group);
        })

        if (!customer_id) {
            setCatalystVariationGroup(false);
            return
        }
    }, [])

    const startAnimation = () => {
        Animated.timing(heightAnimation, {
            toValue: 320,
            duration: 1000
        }).start(() => {
            Animated.timing(opacityAnimation, {
                toValue: 1,
                duration: 1000
            }).start()
        })
    }

    // return ( null)
    return (
        <SafeAreaView style={[styles.rootContainer]}>
            <Animated.View style={[styles.modalContainer, { height: heightAnimation }]}>
                <Animated.View style={{ opacity: opacityAnimation }}>
                    <TouchableOpacity style={styles.crossTouch} onPress={() => nativeBrigeRef.goBack()}>
                        <Image style={styles.crossIcon} source={images.ic_clearSearch} />
                    </TouchableOpacity>
                    <TutorialCarouselComponent data={(catalystVariationGroup && showCatalystRecommendations) ? HomeScreenTutorialDataNew : HomeScreenTutorialData} />
                </Animated.View>
            </Animated.View>
        </SafeAreaView>
    )
}

const TutorialCarouselComponent = props => {

    const components = props.data ?? [];
    const [activeDotIndex, setActiveIndex] = useState(0)

    return (
        <View style={{ marginHorizontal: spacing.spacing10 }}>
            <Carousel layout={'default'}
                inactiveSlideScale={0.95}
                containerCustomStyle={{marginVertical: - 15}}
                itemWidth={Dimensions.get('screen').width - 56}
                sliderWidth={Dimensions.get('screen').width - 36}
                data={components}
                onSnapToItem={(index) => setActiveIndex(index)}
                renderItem={({ item, index }) => {
                    return <TutorialComponent style={{ marginHorizontal: 0 }} data={item} index={index} />
                }}
            />
            {(components.length != 1)
                ? <Pagination
                    dotsLength={components.length}
                    containerStyle={styles.paginationContainerStyle}
                    dotStyle={styles.activeDotStyle}
                    dotContainerStyle={styles.dotContainerStyle}
                    inactiveDotStyle={styles.inactiveDotStyle}
                    activeDotIndex={activeDotIndex}
                    dotColor={colors.blue028}
                />
                : null}
        </View>
    )
}

const TutorialComponent = props => {

    const data = props.data || {};

    return (
        <View>
            <Image source={data.image} style={styles.imageStyle} />
            <View style={styles.headerText}>
                <Text style={TextStyle.text_20_bold}>{data.header}</Text>
                {data.tag ? <View style={styles.tagStyle}><Text style={styles.tagTextStyle}>{data.tag}</Text></View> : null}
            </View>
            <Text style={styles.descriptionText}>{data.description}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,
        opacity: 1,
        // backgroundColor: "transparent",
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        height: Dimensions.get("screen").height,
        width: Dimensions.get("screen").width,
    },
    modalContainer: {
        backgroundColor: colors.white,
        bottom: 10,
        alignSelf: "center",
        position: "absolute",
        borderRadius: 8,
        width: Dimensions.get("screen").width - 16
    },
    crossTouch: {
        width: 48,
        height: 48,
        marginRight: spacing.spacing4,
        marginTop: spacing.spacing4,
        marginBottom: spacing.spacing24,
        padding: spacing.spacing10,
        alignSelf: "flex-end"
    },
    crossIcon: {
        width: 28,
        height: 28,
        resizeMode: "contain"
    },
    headerText: { 
        marginTop: spacing.spacing12, 
        flexDirection: 'row' 
    },
    descriptionText: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        marginVertical: spacing.spacing12
    },
    imageStyle: {
        height: 32,
        width: 32,
    },
    carouselContainerStyle: {
        margin: spacing.spacing0,
    },
    activeDotStyle: {
        height: 6,
        width: 18,
        color: colors.blue028,
        borderRadius: 3
    },
    dotContainerStyle: {
        height: 0,
        padding: 0,
        margin: 0,
        marginHorizontal: 3
    },
    inactiveDotStyle: {
        height: 6,
        width: 6,
        borderRadius: 3
    },
    paginationContainerStyle: {// default styles for paginationContainer view
        height: 30,
        paddingVertical: 10,
        marginVertical: 32,
        marginHorizontal: 0,
        alignItems: "center",
    },
    tagStyle: {
        alignSelf: 'center',
        marginHorizontal: spacing.spacing10,
        backgroundColor: colors.saffronYellow,
        justifyContent: 'flex-end',
        paddingHorizontal: spacing.spacing4,
        borderRadius: 2,
    },
    tagTextStyle: {
        ...TextStyle.text_10_bold,
        color: colors.white,
        letterSpacing: 2,
    }
})

export default TabBarTutorialScreen;