import React, { useEffect, useRef } from "react";
import { View, StyleSheet, Image, TouchableOpacity, Text, SafeAreaView } from "react-native";

import PlanCard from "./PlanCard";
import AutoHeightRBSheet from "../../Components/AutoHeightRBSheet";
import images from "../../images/index.image";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import { TextStyle } from "../../Constants/CommonStyle";
import CoverAmountsBottomSheet from "./CoverAmountsBottomSheet";
import TextListBottomSheet from "./SODComponents/TextListBottomSheet";
import CheckboxListBottomSheet from "./SODComponents/CheckboxListBottomSheet";
import { ScreenDimension } from "../../Constants/AppConstants";

export const AutoHeightBottomSheetType = {
    PLAN_BENEFITS: "PLAN_BENEFITS",
    COVER_AMOUNTS: "COVER_AMOUNTS",
    LEFT_IMAGE_RIGHT_TEXT_LIST: "LEFT_IMAGE_RIGHT_TEXT_LIST",
    CHECK_LIST: "CHECK_LIST",
};


const AutoHeightBottomSheet = props => {

    const rbSheet = useRef(null);
    const bottomSheetType = props.bottomSheetType;
    const title = props.title;
    const description = props.description;
    const onDismiss = props.onDismiss ?? (() => {
    });

    useEffect(() => {
        rbSheet.current.open();
    },[]);

    return (
        <AutoHeightRBSheet
            ref={rbSheet}
            closeOnSwipeDown={false}
            customStyles={{ container: { borderTopLeftRadius: 8, borderTopRightRadius: 8 } }}
            onClose={() => {
                setTimeout(() => {
                    onDismiss();
                }, 1);
            }}
        >
            <TouchableOpacity activeOpacity={1} style={styles.closeButtonContainer} onPress={() => {
                onDismiss();
            }}>
                <Image source={images.closeSheet} style={styles.crossIcon} />
            </TouchableOpacity>
            {title ? <Text style={styles.bottomSheetTitle}>{title}</Text> : null}
            {description ? <Text style={styles.bottomSheetSubtitle}>{description}</Text> : null}
            <SafeAreaView>
                <View style={{marginBottom: spacing.spacing32, maxHeight: ScreenDimension.screenHeight * 0.70}}>
                {bottomSheetType == AutoHeightBottomSheetType.PLAN_BENEFITS
                    ? <PlanCard planCardViewModel={props.data?.planCardViewModel} isShowFullBenefits={true}
                                onBuyPlan={props.data?.onBuyPlan} />
                    : null}

                {bottomSheetType == AutoHeightBottomSheetType.COVER_AMOUNTS
                    ? <CoverAmountsBottomSheet coverAmountsViewModel={props?.data?.coverAmountsViewModel}
                                               onSelectedCoverAmount={props?.data?.onSelectCoverAmount} />
                    : null}

                {bottomSheetType == AutoHeightBottomSheetType.LEFT_IMAGE_RIGHT_TEXT_LIST
                    ? <TextListBottomSheet data={props?.data} />
                    : null}

                {bottomSheetType == AutoHeightBottomSheetType.CHECK_LIST
                    ? <CheckboxListBottomSheet data={props?.data} />
                    : null}
                </View>
            </SafeAreaView>
        </AutoHeightRBSheet>
    );
};

const styles = StyleSheet.create({
    crossIcon: {
        height: 48,
        width: 48,
        resizeMode: "contain",
    },
    bottomSheetTitle: {
        ...TextStyle.text_16_bold,
        color: colors.charcoalGrey,
        marginHorizontal: spacing.spacing20,
    },
    bottomSheetSubtitle: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        marginTop: spacing.spacing4,
        marginHorizontal: spacing.spacing20,
    },
    closeButtonContainer: {
        marginTop: spacing.spacing12,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "flex-end",
    },
});

export default AutoHeightBottomSheet;


