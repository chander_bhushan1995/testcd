import React from 'react';
import {StyleSheet, TouchableOpacity, ImageBackground} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../../Constants/colors';
import {handleBuyPlanBtnActions} from '../Helpers/BuyPlanButtonActions';
import spacing from '../../Constants/Spacing';
import {TextStyle} from '../../Constants/CommonStyle';
import TextView from '../../Catalyst/components/common/TextView';


const BlogCardComponent = props => {

    const headerText = props?.data?.header;
    const noteText = props?.data?.note;
    const image = props?.data?.image;
    const button = props?.data?.button;
    const extraProps = props.extraProps;
    return (
        <TouchableOpacity style={styles.container}
                          onPress={() => handleBuyPlanBtnActions(button?.action, {...extraProps,data: button.data ?? {}})}
                          activeOpacity={1}>
            <ImageBackground source={{uri: image}} style={styles.image}>
                <LinearGradient style={styles.gradientViewStyle} start={{x: 0, y: 0}}
                                end={{x: 0, y: 1}} colors={[colors.transparent, colors.black]}>
                    <TextView style={styles.titleStyle} texts={headerText}/>
                    <TextView style={styles.subTitleStyle} texts={noteText}/>
                </LinearGradient>
            </ImageBackground>
        </TouchableOpacity>
    );
};


const styles = StyleSheet.create({

    container: {
        borderRadius: spacing.spacing8,
        overflow: 'hidden',
        marginTop: spacing.spacing4,
        width: 280,
        height: 208,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing8,
    },
    image: {
        flex: 1,
        resizeMode: 'cover',
        justifyContent: 'center',
        paddingTop: spacing.spacing120,
        backgroundColor: colors.grey,
    },
    backgroundViewStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
    },

    gradientViewStyle: {
        flexDirection: 'row',
        height: 98,
        bottom: 0,
        paddingHorizontal: spacing.spacing12,
        paddingBottom: spacing.spacing12,
        alignItems: 'flex-end',
    },
    titleStyle: {
        ...TextStyle.text_14_bold,
        color: colors.white,
        marginRight: spacing.spacing50,
        flex: 1,
    },
    subTitleStyle: {
        ...TextStyle.text_10_regular,
        color: colors.white,
        alignSelf: 'flex-end',
    },

});

export default BlogCardComponent;
