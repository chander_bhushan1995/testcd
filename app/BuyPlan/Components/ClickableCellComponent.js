import React from 'react';
import {View, StyleSheet, TouchableOpacity, Text, Image} from 'react-native';
import {handleBuyPlanBtnActions} from '../Helpers/BuyPlanButtonActions';
import spacing from '../../Constants/Spacing';
import SeparatorView from '../../CustomComponent/separatorview';
import colors from '../../Constants/colors';
import {OATextStyle} from '../../Constants/commonstyles';
import TextView from '../../Catalyst/components/common/TextView';
import images from '../../images/index.image';


const ClickableCellComponent = props => {

    const button = props?.data?.button;
    const headerText = props?.data?.header;
    const extraProps = props.extraProps;
    const isBottomSeparator = props?.data?.isBottomSeparator ?? false
    return (
        <View style={styles.container}>
            <SeparatorView separatorStyle={styles.SeparatorViewStyle}/>
            <TouchableOpacity activeOpacity={1}
                              onPress={()=>{handleBuyPlanBtnActions(button?.action, {...extraProps,data: button.data ?? {}})}}>
                <View style={styles.RowContainer}>
                    <View style={styles.ColumnContainer}>
                        <TextView style={styles.titleStyle} texts={headerText}/>
                    </View>
                    <Image source={images.rightBlueArrow} style={[styles.ImageRight]}/>
                </View>
            </TouchableOpacity>
            {isBottomSeparator ? <SeparatorView separatorStyle={styles.SeparatorViewStyle}/> : null}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'flex-start',
    },
    RowContainer: {
        alignSelf: 'flex-start',
        flexDirection: 'row',
        padding: spacing.spacing16,
    },
    ColumnContainer: {
        flex: 1,
        alignSelf: 'center',
    },
    SeparatorViewStyle: {backgroundColor: colors.color_F6F6F6, marginTop: spacing.spacing0,height: 1},
    titleStyle: {
        ...OATextStyle.TextFontSize_16_normal,
    },
});

export default ClickableCellComponent;
