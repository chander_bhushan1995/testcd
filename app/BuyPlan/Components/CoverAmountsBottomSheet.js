import { ScrollView, Text, View, StyleSheet, Dimensions, TouchableOpacity } from "react-native";
import spacing from "../../Constants/Spacing";
import { TextStyle } from "../../Constants/CommonStyle";
import React from "react";
import colors from "../../Constants/colors";
import { ScreenDimension } from "../../Constants/AppConstants";

const RenderCell = props => {
    const viewModel = props.cell;
    return (
      <TouchableOpacity activeOpacity={1} onPress={()=>{props.onPress(viewModel?.coverAmount)}}
        style={[styles.coverAmountCellContainer, viewModel?.isSelected ? styles.selectedContainer : styles.deSelectedContainer]}>
        <Text
          style={[{ ...TextStyle.text_12_regular }, viewModel?.isSelected ? styles.selectedTextColor : styles.deSelectedTextColor]}
        >
          {viewModel?.title}
        </Text>
        <View style={{ flexDirection: "row" }}>
          <Text
            style={
              [styles.subtitle,
                viewModel?.isSelected ? styles.selectedTextColor : styles.deSelectedTextColor,
              ]}
          >
            {viewModel?.subTitle}
          </Text>
          <View style={{ flexDirection: "row" }}>
            <Text
              style={[styles.note, viewModel?.isSelected ? styles.selectedTextColor : styles.deSelectedTextColor]}>{viewModel?.note}</Text>
            <Text
              style={[styles.description, viewModel?.isSelected ? styles.selectedTextColor : styles.deSelectedTextColor]}>{viewModel?.description}</Text>
          </View>
        </View>
        {viewModel?.isPopular
          ? <View style={styles.tagContainer}>
            <Text style={styles.tagText}>POPULAR</Text>
          </View>
          : null}
      </TouchableOpacity>
    );
  }
;


const CoverAmountsBottomSheet = props => {
    const viewModel = props?.coverAmountsViewModel.bottomSheetVM;
    return (
      <View style={{ marginVertical: spacing.spacing20, maxHeight: ScreenDimension.screenHeight * 0.60 }}>
        <ScrollView bounces={false}>
          {viewModel?.coverAmountCellVM?.map((cell) => {
            return <RenderCell cell={cell} onPress={props?.onSelectedCoverAmount}/>;
          })}
        </ScrollView>
      </View>
    );
  }
;

const styles = StyleSheet.create({
  coverAmountCellContainer: {
    marginHorizontal: spacing.spacing20,
    marginTop: spacing.spacing16,
    borderColor: colors.greye0,
    borderRadius: 4,
    padding: spacing.spacing12,
    overflow: "hidden",
  },
  selectedContainer: {
    backgroundColor: colors.blue028,
    borderWidth: 0,
  },
  deSelectedContainer: {
    backgroundColor: colors.white,
    borderWidth: 1,
  },
  selectedTextColor: {
    color: colors.white,
  },
  deSelectedTextColor: {
    color: colors.charcoalGrey,
  },
  note: {
    ...TextStyle.text_16_bold,
  },
  description: {
    ...TextStyle.text_12_regular,
    alignSelf: "center",
    marginLeft: spacing.spacing4,
  },
  subtitle: {
    ...TextStyle.text_20_bold,
    flex: 1,
    marginTop:
    spacing.spacing4,
  },
  tagContainer: {
    position: "absolute",
    top: 0,
    right: 0,
    paddingHorizontal: 6,
    paddingVertical: 2,
    borderBottomLeftRadius: 2,
    backgroundColor: colors.saffronYellow,
  },
  tagText: {
    ...TextStyle.text_10_bold,
    color: colors.white,
  },
});


export default CoverAmountsBottomSheet;
