import React, {useEffect, useRef, useState} from "react";
import {NativeModules, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import ButtonWithLoader from "../../CommonComponents/ButtonWithLoader";
import TextView from "../../Catalyst/components/common/TextView";
import {TextStyle} from "../../Constants/CommonStyle";
import colors from "../../Constants/colors";
import {handleBuyPlanBtnActions} from "../Helpers/BuyPlanButtonActions";
import spacing from "../../Constants/Spacing";

const bridgeRef = NativeModules.ChatBridge;

const DualButtonComponent = props => {
  const primaryButton = props?.data?.primaryButton;
  const secondaryButton = props?.data?.secondaryButton;
  const note = props?.data?.note;
  const extraProps = props.extraProps ?? {};
  const primaryButtonRef = useRef(null);
  const [primaryButtonText, setPrimaryButtonText] = useState(props?.data?.isShowOfferText ? (extraProps?.reducerState?.offersData?.offerText ?? primaryButton.text) : primaryButton.text);
  const [noteText, setNoteText] = useState(note?.value);


  useEffect(() => {
    if (props.extraProps?.reducerState?.idFenceBuyPlanText) {
      setPrimaryButtonText(props.extraProps?.reducerState?.idFenceBuyPlanText);
    }else if(props?.data?.isShowOfferText) {
      setPrimaryButtonText(extraProps?.reducerState?.offersData?.offerText ?? primaryButton.text)
    }
    if (props.extraProps?.reducerState?.idFenceBuyPlanNoteText) {
      setNoteText(props.extraProps?.reducerState?.idFenceBuyPlanNoteText);
    }
  }, [props.extraProps?.reducerState]);

  return (
    <View style={styles.container}>
      {secondaryButton
        ? <TouchableOpacity style={styles.secondaryButton} onPress={() => {
            handleBuyPlanBtnActions(secondaryButton.action, { ...extraProps, data: secondaryButton?.data });
        }}>
          <Text style={styles.secondaryButtonText}>{secondaryButton?.text}</Text>
        </TouchableOpacity>
        : null}
      <View ref={primaryButtonRef} style={{ marginTop: spacing.spacing8}} onLayout={({ nativeEvent }) => {
        if (extraProps?.measureFloatActionTargetLayout && props?.data?.isShowFloatingActionButton) { // if current component is target for showing animated floating actions button
          extraProps?.measureFloatActionTargetLayout(primaryButtonRef.current, props?.data?.floatingActionButton);
        }
      }}>
        <ButtonWithLoader
          isLoading={false}
          enable={true}
          isHTMLText={true}
          Button={{
            text: primaryButtonText,
            onClick: () => {
              handleBuyPlanBtnActions(primaryButton.action, { ...extraProps, data: primaryButton?.data });
            },
          }} />
      </View>
      {noteText ? <TextView style={{ ...styles.note }} texts={{ ...note, value: noteText }} /> : null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: spacing.spacing24,
    paddingTop: spacing.spacing36,
  },
  secondaryButton: {
    height: 44,
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: spacing.spacing16,
  },
  note: {
    ...TextStyle.text_14_normal,
    color: colors.grey,
    textAlign: "center",
    marginTop: spacing.spacing12,
  },
  secondaryButtonText: {
    ...TextStyle.text_14_bold,
    color: colors.blue028,
  },
});

export default DualButtonComponent;
