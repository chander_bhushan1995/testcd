import {View} from "react-native";
import colors from "../../Constants/colors";
import React from "react";
import spacing from "../../Constants/Spacing";

const EmptyComponent = props => {
    let bgColor = props?.data?.bgColor ?? colors.transparent;
    let height = props?.data?.height ?? spacing.spacing32;
    return (
        <View style={{backgroundColor: bgColor, height: height}}></View>
    )
}

export default EmptyComponent;
