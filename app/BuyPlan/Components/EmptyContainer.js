import { View, StyleSheet, FlatList } from "react-native";
import colors from "../../Constants/colors";
import React from "react";
import spacing from "../../Constants/Spacing";
import { getComponent } from "../../Catalyst/components/ComponentsFactory";

const EmptyContainer = props => {
    const bgColor = props?.data?.bgColor ?? colors.color_FFF1D8;
    const borderColor = props?.data?.borderColor ?? colors.transparent;
    const borderWidth = props?.data?.borderWidth ?? 0;
    const borderRadius = props?.data?.borderRadius ?? 0;
    const marginHorizontal = props?.data?.marginHorizontal ?? 0;
    const showShadow = props?.data?.showShadow ?? false
    const shadowStyle = showShadow ? style.shadow : {};

    const _renderComponent = ({ item, index }) => {
        const Component = getComponent(item.type);
        return <Component data={item} extraProps={props.extraProps}/>;
    };

    return (
        <View style={[{
            ...style.container,
            backgroundColor: bgColor,
            borderColor: borderColor,
            borderWidth: borderWidth,
            borderRadius: borderRadius,
            marginHorizontal: marginHorizontal
        },shadowStyle]}>
            <FlatList
                data={props?.data?.components ?? []}
                renderItem={_renderComponent}
                showsVerticalScrollIndicator={false}
                bounces={false}
                keyExtractor={(item, index) => {
                    return index.toString();
                }}
                extraData={false}
            />
        </View>
    );
};


const style = StyleSheet.create({
    container: {
        marginBottom: spacing.spacing16,
        justifyContent: "center",
    },
    shadow: {
        shadowColor: colors.lightGrey,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowRadius: 4,
        shadowOpacity: 0.72,
        elevation: 2,
    },
});
export default EmptyContainer;
