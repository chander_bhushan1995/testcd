import React from "react";
import { View, StyleSheet, TouchableOpacity, FlatList, Text, Image, ImageBackground } from "react-native";
import { TextStyle } from "../../Constants/CommonStyle";
import TextView from "../../Catalyst/components/common/TextView";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import images from "../../images/index.image";
import { getComponent } from "../../Catalyst/components/ComponentsFactory";
import { handleBuyPlanBtnActions } from "../Helpers/BuyPlanButtonActions";

const FullWidthContainer = props => {
  const data = props.data ?? {};

  const headerText = data.header ?? {};
  const paragraphText = data.paragraph ?? null;
  const noteText = data.note ?? null;
  const button = data?.button ?? null;
  const isHorizontalContainer = data?.isHorizontalContainer ?? false;
  const isChild = data?.isChild ?? false
  const knowMoreButton = data?.knowMoreButton;
  const headerBGImage = data?.headerBGImage ?? "https://falseimageurl/";
  const headerTextStyle = {
    fontSize: headerText?.fontSize ?? styles.primaryText.fontSize,
    color: headerText?.fontColor ?? styles.primaryText.color,
  };
  const paragraphTextStyle = {
    fontSize: paragraphText?.fontSize ?? styles.secondaryText.fontSize,
    color: paragraphText?.fontColor ?? styles.secondaryText.color,
  };
  const containerBottom = button ? (spacing.spacing32 + styles.circularButton.height / 2) : (isChild ? 0 : spacing.spacing36);
  const noteTextBottom = button ? (styles.circularButton.height / 2) + 8 : spacing.spacing8;
  const containerPadding = isChild ? 0 : (!noteText && !button) ? spacing.spacing20 : spacing.spacing0;
  const containerStyle = {
    backgroundColor: data?.bgColor,
    marginBottom: containerBottom,
    paddingBottom: containerPadding,
  };
  const shadow = data?.showShadow ? styles.shadow : {};
  const buttonImage = button?.image ? { uri: button?.image } : images.chevronGif;

  const _renderComponent = ({ item, index }) => {
    const Component = getComponent(item.type);
    return <Component data={item} extraProps={props.extraProps} />;
  };

  return (
    <View style={[styles.container, shadow, containerStyle]}>
      <ImageBackground source={{ uri: headerBGImage }} style={{
        resizeMode: "contain",
        marginTop: spacing.spacing20,
        paddingTop: spacing.spacing20,
        marginHorizontal: spacing.spacing10,
      }}>
        <View style={{ flexDirection: "row", marginHorizontal: spacing.spacing10 }}>
          <TextView style={{ ...styles.primaryText, ...headerTextStyle, flex: 1 }} texts={headerText} />
          {knowMoreButton
            ? <TouchableOpacity style={{ marginLeft: spacing.spacing16, flexDirection: "row" }} onPress={() => {
              handleBuyPlanBtnActions(knowMoreButton?.action, { ...props.extraProps, data: knowMoreButton.data ?? {} });
            }}>
              <Text style={[styles.knowMoreButtonText]}>{knowMoreButton?.text}</Text>
                <Image source={images.rightBlueArrow}
                       style={{ height: 10, width: 5, marginTop: 6, marginLeft: spacing.spacing4 }} />
            </TouchableOpacity>
            : null}
        </View>
        {paragraphText
          ? <TextView style={{ ...styles.secondaryText, ...paragraphTextStyle }} texts={paragraphText} />
          : null}
      </ImageBackground>
      <View style={{ marginTop: spacing.spacing20 }}>
        <FlatList
          horizontal={isHorizontalContainer}
          showsHorizontalScrollIndicator={false}
          data={data?.components ?? []}
          renderItem={_renderComponent}
          showsVerticalScrollIndicator={false}
          bounces={false}
          keyExtractor={(item, index) => {
            return index.toString();
          }}
        />
      </View>

      {noteText || button
        ? <View style={styles.footerView}>
          {noteText ?
            <TextView style={[styles.noteText, { marginBottom: noteTextBottom }]} texts={noteText} /> : null}
          {button
            ? button.text
              ? <TouchableOpacity activeOpacity={1} style={styles.buttonWithText} onPress={() => {
                handleBuyPlanBtnActions(button?.action, { ...props.extraProps, data: button.data ?? {} });
              }}>
                <Text style={styles.buttonText}>{button.text}</Text>
                <Image source={buttonImage} style={styles.buttonImage} />
              </TouchableOpacity>
              : <TouchableOpacity activeOpacity={1} style={styles.circularButton} onPress={() => {
                handleBuyPlanBtnActions(button?.action, { ...props.extraProps, data: button.data ?? {} });
              }}>
                <Image source={buttonImage} style={styles.buttonImage} />
              </TouchableOpacity>
            : null}
        </View>
        : null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    flex: 1,
  },
  primaryText: {
    ...TextStyle.text_16_normal,
    lineHeight:spacing.spacing26
  },
  secondaryText: {
    ...TextStyle.text_14_normal,
    marginTop: spacing.spacing12,
    marginHorizontal: spacing.spacing10,
  },
  noteText: {
    ...TextStyle.text_10_normal,
    textAlign: "center",
  },
  shadow: {
    shadowColor: colors.lightGrey,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 4,
    shadowOpacity: 0.72,
    elevation: 2,
  },
  footerView: {
    marginTop: spacing.spacing44,
    alignItems: "center",
  },
  circularButton: {
    height: spacing.spacing36,
    width: spacing.spacing36,
    backgroundColor: colors.color_007DFF,
    borderRadius: (spacing.spacing36) / 2,
    bottom: -(spacing.spacing36) / 2,
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
  },
  buttonWithText: {
    height: spacing.spacing36,
    backgroundColor: colors.color_007DFF,
    borderRadius: (spacing.spacing36) / 2,
    bottom: -(spacing.spacing36) / 2,
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    flexDirection: "row",
    paddingHorizontal: spacing.spacing16,
  },
  buttonImage: {
    height: 24,
    width: 24,
  },
  buttonText: {
    ...TextStyle.text_14_bold,
    color: colors.white,
  },
  knowMoreButtonText: {
    ...TextStyle.text_12_bold,
    color: colors.blue028,
  },
});

export default FullWidthContainer;
