import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import colors from "../../Constants/colors";
import React from "react";
import { TextStyle } from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import Images from "../../images/index.image";
import TagComponent from "../../Catalyst/components/common/TagsComponent";

const GadgetItemComponent = props => {
    const tag = props?.data?.tag;
    let paragraphText = "";
    paragraphText = props?.data?.brands?.join(", ");
    if (props?.data?.note) {
        if (paragraphText) {
            paragraphText = paragraphText + " | ";
        }
        paragraphText = paragraphText + props?.data?.note;
    }
    const leftImage = props.data.image ? { uri: props.data?.image } : null;
    return (
        <TouchableOpacity onPress={() => props?.onPress(props.data)}>
            <View>
                <View style={style.container}>
                    {leftImage && <Image style={style.leftImage} source={leftImage} />}
                    <View style={{ flex: 3, justifyContent: "center" }}>
                        <View style={{ flexDirection: "row", alignItems: "center"}}>
                            <Text style={style.headerStyle}>{props?.data?.name}</Text>

                            {tag ? <TagComponent tags={[tag]}
                                                 style={{marginTop: 0}}
                                                 singleTagStyle={style.singleTagStyle}
                                                 textStyle={{
                                                     ...TextStyle.text_10_bold,
                                                     color: colors.white,
                                                     marginTop: 0,
                                                 }} />
                                : null
                            }

                        </View>
                        {paragraphText && <Text style={style.paragraph}>{paragraphText}</Text>}
                    </View>
                    <Image style={style.rightImage} source={Images.rightBlueArrow} />

                </View>
                <View style={style.divider} />

            </View>
        </TouchableOpacity>);
};


const style = StyleSheet.create({
    container: {
        flexDirection: "row",
        margin: spacing.spacing16,
    },
    leftImage: {
        width: 40,
        height: 40,
        resizeMode: "contain",
        marginRight: spacing.spacing16,
    },
    headerStyle: {
        ...TextStyle.text_16_bold,
        color: colors.black,
    },
    paragraph: {
        ...TextStyle.text_14_normal,
        marginTop: spacing.spacing4,
    },
    rightImage: {
        width: 12,
        height: 12,
        resizeMode: "contain",
        marginLeft: spacing.spacing4,
        marginTop: spacing.spacing2,
        alignSelf: "center",
    },
    divider: {
        height: 1,
        backgroundColor: colors.greye0,
    },
    singleTagStyle: {
        backgroundColor: colors.saffronYellow,
        borderWidth: 2,
        paddingHorizontal: spacing.spacing8,
        paddingVertical: spacing.spacing0,
        borderColor: colors.transparent,
        borderRadius: 2,
        marginTop: 0,
    },
});
export default GadgetItemComponent;
