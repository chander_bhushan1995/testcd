import {FlatList, StyleSheet, Text, View} from "react-native";
import colors from "../../Constants/colors";
import React from "react";
import {TextStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import TextView from "../../Catalyst/components/common/TextView";
import {GEDGET_SHEET_TITLE} from "../BuyPlanConstants";
import GadgetItemComponent from "./GadgetItemComponent";
import {handleBuyPlanBtnActions} from "../Helpers/BuyPlanButtonActions";
import {AppStringConstants} from "../../Constants/AppStringConstants";
import {Category} from "../../Constants/AppConstants";
import {getGadgetSheetTitle} from "../Helpers/BuyPlanUtils";
import {getComponent} from "../../Catalyst/components/ComponentsFactory";


const GadgetListComponent = props => {
    const extraProps = props?.extraProps ?? {}
    const title = getGadgetSheetTitle(props?.data?.data)
    let products = []
    if(props?.data?.data?.products){
        products = [...props?.data?.data?.products]
    }else if (props?.data?.data?.components){
        products = [...props?.data?.data?.components]
    }
    if (props?.data?.data?.otherProducts) {
        let category1 = {
            "name": AppStringConstants.MOST_BOUGHT,
            "isCategory": true
        }
        let category2 = {
            "name": AppStringConstants.OTHER_APPLIANCES,
            "isCategory": true
        }
        products.unshift(category1)
        products.push(category2)
        let others = props?.data?.data?.otherProducts
        products = products.concat(others)

    }


    const _renderComponent = ({item, index}) => {
        if(item.type){
            const Component = getComponent(item.type);
            return <Component data={item} />;
        }
        else if (item.isCategory) {
            return <View>
                <Text style={style.category}>{item.name}</Text>
                <View style={style.divider}/>
            </View>
        } else {
            return <GadgetItemComponent data={item} onPress={() => {
                if (props?.data?.onItemClick) {
                    props?.data?.onItemClick(item)
                } else if (item.button) {
                    handleBuyPlanBtnActions(item.button.action, {...extraProps, data: item.button.data})
                }
            }}/>;
        }
    };

    return (
        <View>
            <View>
                <TextView style={style.headerStyle} texts={{
                    value: title,
                    isHTML: true
                }}/>
                {!props?.data?.data?.otherProducts && <View style={style.divider}/>}
                <FlatList
                    data={products}
                    renderItem={_renderComponent}
                    showsVerticalScrollIndicator={false}
                    bounces={false}
                    keyExtractor={(item, index) => {
                        return index.toString();
                    }}
                    extraData={false}
                />
                {props?.data?.data?.description && <TextView style={style.paragraph} texts={{
                    value: props?.data?.data?.description,
                    isHTML: true
                }}/>}
            </View>
        </View>
    )
}


const style = StyleSheet.create({

    headerStyle: {
        ...TextStyle.text_16_regular,
        color: colors.black,
        marginHorizontal: spacing.spacing20,
    },
    paragraph: {
        ...TextStyle.text_14_normal,
        marginTop: spacing.spacing20,
        marginHorizontal: spacing.spacing20,
    },

    category: {
        ...TextStyle.text_10_regular,
        marginHorizontal: spacing.spacing16,
        marginTop: spacing.spacing32
    },

    divider: {
        height: 1,
        backgroundColor: colors.greye0,
        marginTop: spacing.spacing16
    }
});
export default GadgetListComponent;
