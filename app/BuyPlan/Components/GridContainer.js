import {FlatList, StyleSheet, View} from "react-native";
import colors from "../../Constants/colors";
import React from "react";
import {CommonStyle, TextStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import TextView from "../../Catalyst/components/common/TextView";
import {handleBuyPlanBtnActions} from "../Helpers/BuyPlanButtonActions";
import {getComponent} from "../../Catalyst/components/ComponentsFactory";


const GridContainer = props => {
    const dividerColor = props?.data?.dividerColor ?? colors.white
    const radius = props?.data?.radius ?? 0
    const columns = props?.data?.col
    const shadowStyle = props?.data?.showShadow ? CommonStyle.cardContainerWithShadow : {}
    const headerFont = props?.data?.header?.fontSize ?? spacing.spacing16
    const paragraphFont = props?.data?.paragraph?.fontSize ?? spacing.spacing14
    const extraProps = props.extraProps ?? {};


    const _renderComponent = ({item, index, separators}) => {
        const Component = getComponent(item.type);
        const colDividerStyle = ((index + 1) % columns) != 0 ? {
            borderRightWidth: 0.4,
            borderRightColor: dividerColor
        } : null
        return <Component onShowUnderlay={separators.highlight}
                          onHideUnderlay={separators.unhighlight}
                          data={item}
                          dividerStyle={colDividerStyle}
                          onItemClick={(button) => {
                              handleBuyPlanBtnActions(button?.action, {...extraProps, data: button?.data})
                          }}/>;
    };

    const headerComponent = () => {
        return <View style={{marginBottom: spacing.spacing4}}>
            {
                props.data?.header
                    ? <TextView style={{...style.primaryText, fontSize: headerFont}} texts={props?.data?.header}/>
                    : null
            }
            {
                props.data?.paragraph
                    ? <TextView style={{...style.secondaryText, fontSize: paragraphFont}} texts={props?.data?.paragraph}/>
                    : null
            }
        </View>

    }

    const footerComponent = () => {
        {
            return props?.data?.footer ?
                <View style={style.footer}>
                    <TextView style={[TextStyle.text_12_normal,{textAlign: 'center'}]}
                              texts={props?.data?.footer}/>
                </View> : null
        }
    }

    const itemSeparator = (highlighted) => {

        return <View
            style={[
                {...style.separator, backgroundColor: dividerColor},
                highlighted && {marginLeft: 0}
            ]}
        />
    }
    return (
        <View style={[shadowStyle,
            {...style.container, borderRadius: radius}]}>
            <FlatList style={{
                flex: 1,
            }}
                      data={props?.data?.components ?? []}
                      renderItem={_renderComponent}
                      showsVerticalScrollIndicator={false}
                      bounces={false}
                      numColumns={props?.data?.col}
                      ItemSeparatorComponent={itemSeparator}
                      ListHeaderComponent={headerComponent}
                      ListFooterComponent={footerComponent}
                      keyExtractor={(item, index) => {
                          return index.toString();
                      }}
                      extraData={false}
            />
        </View>
    )
}


const style = StyleSheet.create({

    container: {
        marginHorizontal: spacing.spacing16,
        flex: 1,
        padding: 2,
    },

    primaryText: {
        ...TextStyle.text_16_normal,
    },
    secondaryText: {
        ...TextStyle.text_14_normal,
        marginTop: spacing.spacing8,
        marginBottom: spacing.spacing20
    },
    footer: {
        marginTop: spacing.spacing12
    },
    separator: {
        width: '100%',
        height: 0.4,
    }
});
export default GridContainer;
