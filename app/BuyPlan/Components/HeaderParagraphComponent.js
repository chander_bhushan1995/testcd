import { View, StyleSheet } from "react-native";
import colors from "../../Constants/colors";
import React from "react";
import { TextStyle } from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import TextView from "../../Catalyst/components/common/TextView";


const HeaderParagraphComponent = props => {

  const headerText = props?.data.header ?? {};
  const paragraphText = props?.data.paragraph ?? null;

  const headerTextStyle = {
    fontSize: headerText?.fontSize ?? styles.primaryText.fontSize,
    color: headerText?.fontColor ?? styles.primaryText.color,
  };
  const paragraphTextStyle = {
    fontSize: paragraphText?.fontSize ?? styles.secondaryText.fontSize,
    color: paragraphText?.fontColor ?? styles.secondaryText.color,
  };

  return (
    <View style={styles.container}>
      <TextView style={{ ...styles.primaryText, ...headerTextStyle }} texts={props?.data?.header} />
      {props?.data?.paragraph ? <TextView style={{ ...styles.secondaryText, ...paragraphTextStyle }}
                                          texts={props?.data?.paragraph} /> : null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: spacing.spacing20,
    marginHorizontal: spacing.spacing20,
  },
  primaryText: {
    ...TextStyle.text_18_normal,
    color: colors.charcoalGrey,
  },
  secondaryText: {
    ...TextStyle.text_14_normal,
    color: colors.color_848F99,
    marginTop: spacing.spacing8,
  },
});
export default HeaderParagraphComponent;
