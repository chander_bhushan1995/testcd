import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import colors from "../../Constants/colors";
import React from "react";
import { TextStyle } from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import TextView from "../../Catalyst/components/common/TextView";
import Images from "../../images/index.image";
import { handleBuyPlanBtnActions } from "../Helpers/BuyPlanButtonActions";
import RatingBarTemplate from "../../Catalyst/components/common/RatingBarTemplate";

const HighlightedTestimonialComponent = props => {
    const button = props?.data?.button || {};
    const imageURL = props?.data?.image ? { uri: props?.data?.image } : Images.testimonialPlaceholder;
    return (
        <TouchableOpacity activeOpacity={1} onPress={() => {
            if (button?.action) {
                handleBuyPlanBtnActions(button?.action, { ...props?.extraProps, data: button.data ?? {} });
            }
        }}>
            <View style={style.container}>

                <View style={style.imageContainer}>
                    <Image style={style.leftImage} source={imageURL} />
                </View>

                <View style={{ flex: 3, marginHorizontal: spacing.spacing16 }}>
                    <RatingBarTemplate rating={props?.data?.rating} starImageStyle={{ height: 14, width: 14 }} />
                    <View style={{
                        marginTop: spacing.spacing4,
                    }}>
                        {props?.data?.header ?
                            <TextView style={style.headerStyle} texts={props?.data?.header} /> : null}
                        {props?.data?.paragraph ?
                            <TextView style={style.paragraph} texts={props?.data?.paragraph} /> : null}
                    </View>

                    {
                        props?.data?.button
                            ?
                            <TouchableOpacity
                                onPress={() => {
                                    handleBuyPlanBtnActions(button?.action, {
                                        ...props?.extraProps,
                                        data: button.data ?? {},
                                    });
                                }}
                                style={{ flexDirection: "row", marginTop: spacing.spacing10, alignItems: "center" }}>
                                <Image style={style.buttonImage} source={Images.playBlue} />
                                <Text style={style.secondaryButtonText}>{props.data?.button?.text}</Text>
                            </TouchableOpacity>
                            : null
                    }
                </View>
            </View>
        </TouchableOpacity>);
};

const style = StyleSheet.create({
    container: {
        flexDirection: "row",
        marginTop: spacing.spacing20,
        marginHorizontal: spacing.spacing20,
    },

    imageContainer: {
        justifyContent: "center",
        alignItems: "center",
        width: 96,
        height: 96,
    },

    leftImage: {
        resizeMode: "contain",
        width: 96,
        height: 96,
        margin: spacing.spacing10,
    },
    headerStyle: {
        ...TextStyle.text_16_normal,
        color: colors.charcoalGrey,
    },
    paragraph: {
        ...TextStyle.text_14_normal,
        marginTop: spacing.spacing12,
    },
    buttonImage: {
        width: 24,
        height: 24,
        resizeMode: "stretch",
    },
    secondaryButton: {
        height: 48,
        marginTop: spacing.spacing32,
        marginLeft: spacing.spacing4,
    },
    secondaryButtonText: {
        ...TextStyle.text_14_bold,
        color: colors.blue028,
    },
});
export default HighlightedTestimonialComponent;
