import {Image, StyleSheet, View} from "react-native";
import React from "react";
import {TextStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import TextView from "../../Catalyst/components/common/TextView";
import RatingBarTemplate from "../../Catalyst/components/common/RatingBarTemplate";

const HorizontalGridItemComponent = props => {
    let image = props?.data?.image ? {uri: props?.data?.image} : null
    return (
        <View style={style.container}>
            {image && <Image style={style.leftImage} source={image}></Image>}
            <View style={{marginHorizontal: spacing.spacing8, flex: 1}}>
                {props?.data?.rating ? <RatingBarTemplate starImageStyle={style.starImage}
                                                          rating={props?.data?.rating}/> : null}
                <TextView style={style.header}
                          texts={props?.data?.header}/>
            </View>

        </View>)
}


const style = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        marginVertical: spacing.spacing16
    },
    leftImage: {
        width: 52,
        height: 52,
        resizeMode: 'contain',
    },
    header: {
        ...TextStyle.text_14_normal_charcoalGrey,
        marginTop: spacing.spacing5,
    },
    starImage: {
        height: 10,
        width: 10,
        marginRight: spacing.spacing2,
        marginTop: spacing.spacing10,

    }
});
export default HorizontalGridItemComponent;
