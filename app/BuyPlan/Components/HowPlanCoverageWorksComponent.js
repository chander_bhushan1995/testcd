import React from "react";
import {Text, View, StyleSheet, Image} from "react-native";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import {TextStyle} from "../../Constants/CommonStyle";
import {OAImageSource} from "../../Constants/OAImageSource";
import TextView from "../../Catalyst/components/common/TextView";


const HowPlanCoverageWorksComponent = props => {

    const paragraphText = props?.data.paragraph ?? null;
    const progressViewFirst = props?.data.progressViewFirst ?? {};
    const progressViewSecond = props?.data.progressViewSecond ?? {};
    return (
        <View style={style.container}>
            { (paragraphText != null) ? <TextView style={style.txtTitleStyle} texts={paragraphText} /> : null }

            <View style={style.subContainer}>
                {progressView(spacing.spacing100, colors.color_888F97, progressViewFirst, colors.color_BDBDBD, 'LEFT')}
                <Image source={OAImageSource.green_right.source}
                       style={[OAImageSource.green_right.dimensions, {marginHorizontal: spacing.spacing5}]}/>
                {progressView(spacing.spacing200, colors.color_212121, progressViewSecond, colors.color_45B448, 'RIGHT')}
            </View>
        </View>
    )

    function progressView(width, textColor, progressViewTexts, progressColor, progressRadiusLocation) {
        const topTitle = progressViewTexts.topTitle ?? null;
        const bottomTitle = progressViewTexts.bottomTitle ?? null;

        return (<View style={{alignItems: 'center'}}>
            {(topTitle != null) ? <TextView style={[style.txtSubTitleStyle, {color: textColor}]} texts={topTitle} /> : null}
            <View style={{
                backgroundColor: progressColor, height: spacing.spacing4, width: width,
                borderBottomLeftRadius: (progressRadiusLocation === 'LEFT' ? spacing.spacing4 : 0),
                borderTopLeftRadius: (progressRadiusLocation === 'LEFT' ? spacing.spacing4 : 0),
                borderBottomRightRadius: (progressRadiusLocation === 'RIGHT' ? spacing.spacing4 : 0),
                borderTopRightRadius: (progressRadiusLocation === 'RIGHT' ? spacing.spacing4 : 0)
            }}/>
            {(bottomTitle != null) ? <TextView style={[style.txtSubTitleStyle, {color: textColor}]} texts={bottomTitle} /> : null}
        </View>);
    }
}

const style = StyleSheet.create({
    container: {
        alignItems: 'center',
        paddingVertical: spacing.spacing24,
        marginVertical: spacing.spacing40,
        paddingHorizontal: spacing.spacing16,
        alignContent: 'center',
        justifyContent: 'center',
    },
    subContainer: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: spacing.spacing18,
    },
    txtTitleStyle: {
        ...TextStyle.text_14_normal,
        color: colors.color_212121,
    },
    txtSubTitleStyle: {
        ...TextStyle.text_12_normal,
        color: colors.color_888F97,
        marginVertical: spacing.spacing6,
    },
});
export default HowPlanCoverageWorksComponent;
