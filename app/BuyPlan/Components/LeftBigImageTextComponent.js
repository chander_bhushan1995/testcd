import { StyleSheet, Image, Dimensions, TouchableOpacity } from "react-native";
import React from "react";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import TextView from "../../Catalyst/components/common/TextView";
import { TextStyle } from "../../Constants/CommonStyle";
import { handleBuyPlanBtnActions } from "../Helpers/BuyPlanButtonActions";

const LeftBigImageTextComponent = props => {
  const data = props.data ?? {};
  const button = data?.button
  const image = data.image ? { uri: data.image } : null;
  const paragraph = data?.paragraph ?? {};
  const bgColor = data?.bgColor ?? colors.transparent
  return (
    <TouchableOpacity activeOpacity={1} style={[styles.container,{backgroundColor: bgColor}]} onPress={()=>{
      handleBuyPlanBtnActions(button?.action, { ...props.extraProps, data: button.data ?? {} });
    }}>
      <Image style={styles.image} source={image} />
      <TextView texts={paragraph} style={styles.description} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: spacing.spacing16,
    borderColor: colors.color_blue028_opacity_2,
    borderRadius: 2,
    borderWidth: 1,
    padding: spacing.spacing12,
    flexDirection: "row",
    width: Dimensions.get('screen').width/1.4
  },
  image: {
    height: 56,
    width: 64,
    resizeMode: "contain",
    marginRight: spacing.spacing12,
  },
  description: {
    ...TextStyle.text_14_normal,
    color: 'rgba(33,33,33,0.75)',
    alignSelf: 'center',
    flex: 1
  },
});

export default LeftBigImageTextComponent;
