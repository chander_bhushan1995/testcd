import React from "react";
import { View, StyleSheet, Image, Text } from "react-native";
import spacing from "../../Constants/Spacing";
import TextView from "../../Catalyst/components/common/TextView";
import { TextStyle } from "../../Constants/CommonStyle";
import colors from "../../Constants/colors";

const CircleItem = props => {
    return (
        <View style={styles.circle}>
            <Text style={styles.circleText}>{props?.text ?? ""}</Text>
        </View>
    );
};

const LeftImageTextComponent = props => {
    const data = props.data ?? {};
    const paragraphText = data.paragraph || {};
    const localImage = data.isLocalImage ?? false;
    const image = data.image ? (localImage ? data.image : { uri: data.image}) : null;
    const paragraphFontSize = data?.paragraph?.fontSize ? data?.paragraph?.fontSize : styles.paragraph.fontSize;
    const paragraphFontColor = data?.paragraph?.fontColor ? data?.paragraph?.fontColor : styles.paragraph.color;
    const marginHorizontal = data.marginHorizontal ?? spacing.spacing20

    return (
        <View style={[styles.component, {marginHorizontal: marginHorizontal}]}>
            {image ? <Image source={image}  style={[styles.image,props?.data?.imageStyle]} />
                : <CircleItem text={data?.itemNumber ?? 0} />}
            <View style={{flexShrink:1, justifyContent: 'center' }}>
                <TextView style={{ ...styles.paragraph, fontSize: paragraphFontSize, color: paragraphFontColor }}
                          texts={paragraphText} />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    component: {
        marginTop: spacing.spacing12,
        flexDirection: "row",
    },
    paragraph: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        marginLeft: spacing.spacing8,
        marginRight: spacing.spacing5,
        flexShrink: 1
    },
    image: {
        width: 28,
        height: 28,
        resizeMode: "contain",
    },
    circle: {
        width: 28,
        height: 28,
        borderRadius: 14,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: colors.color_EDF5FE,
        borderWidth: 1,
        borderColor: colors.color_B4D9FB,
    },
    circleText: {
        ...TextStyle.text_14_bold,
        color: colors.charcoalGrey,
    },
});
export default LeftImageTextComponent;
