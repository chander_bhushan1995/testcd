import { Image, Text, TouchableOpacity, View, StyleSheet, ScrollView, Dimensions } from "react-native";
import spacing from "../../Constants/Spacing";
import images from "../../images/index.image";
import ButtonWithLoader from "../../CommonComponents/ButtonWithLoader";
import React, { useState } from "react";
import colors from "../../Constants/colors";
import { ButtonStyle, TextStyle } from "../../Constants/CommonStyle";
import Tooltip from "react-native-walkthrough-tooltip";
import { getProductWithServicesList } from "../Helpers/BuyPlanUtils";
import { logAppsFlyerEvent } from "../../commonUtil/WebengageTrackingUtils";
import * as EventKeys from "../../Constants/WebengageAttrKeys"
import { CURRENCY } from "../../Constants/AppConstants";
import { AF_CONTENT_VIEW } from "../../Constants/WebengageEvents";

const ButtonVariations = {
    ContinueWithPrice: "ContinueWithPrice",
    ContinueOnly: "ContinueOnly",
};

const PlanCard = props => {

    const planViewModel = props.planCardViewModel ?? {};
    const buttonTitle = props.buttonTitle ?? "Buy Now";
    const isShowFullBenefits = props?.isShowFullBenefits ?? false;
    const buyPlanJourney = props?.buyPlanJourney
    const onSeeBenefits = props?.onSeeBenefits ?? ((e) => {
    });
    const onBuyPlan = props?.onBuyPlan ?? ((e) => {
    });
    const [toolTipVisible, setToolTipVisible] = useState(false);

    const ButtonWithPrice = () => {
        return (
            <TouchableOpacity activeOpacity={.8}
                              onPress={() => {
                                  onBuyPlan(planViewModel);
                              }}
                              style={[ButtonStyle.BlueButton, styles.primaryButton]}>
                <View style={{ flex: 1 }}>
                    <Text style={[TextStyle.text_16_bold, { color: colors.white }]}>{planViewModel?.price}</Text>
                    <View style={{ flexDirection: "row" }}>
                        {(planViewModel?.anchorPrice != planViewModel?.price) ?
                            <Text style={styles.buttonAnchorText}>{`${planViewModel?.anchorPrice}`}</Text> : null}
                        {planViewModel?.saveAmount ?
                            <Text style={[styles.buttonAnchorText, { textDecorationLine: "none" }]}>
                                {` (you save ${planViewModel?.saveAmount})`}
                            </Text> : null}

                    </View>
                </View>
                <View style={{ flexDirection: "row", alignSelf: "center" }}>
                    <Text style={[TextStyle.text_16_bold, { color: colors.white }]}>Continue</Text>
                    <Image source={images.rightWhiteArrow}
                           style={{
                               height: 12,
                               width: 8,
                               marginTop: 2,
                               marginLeft: spacing.spacing4,
                               tintColor: colors.white,
                               alignSelf: "center",
                           }} />
                </View>
            </TouchableOpacity>
        );
    };

    const getButtonVariation = () => {
        let variation = planViewModel?.buttonVariation ?? ButtonVariations.ContinueOnly;
        switch (variation) {
            case ButtonVariations.ContinueWithPrice:
                return <ButtonWithPrice />;
                break;
            default:
                return <ButtonWithLoader
                    isLoading={false}
                    enable={true}
                    Button={{
                        text: buttonTitle,
                        onClick: () => {
                            onBuyPlan(planViewModel);
                        },
                    }
                    } />;
                break;
        }
    };

    const getSeeBenifitButtonVariation = () => {
       return (planViewModel?.planBenefits?.length > planViewModel?.MAX_BENEFITS_IN_PLAN_CARD && !isShowFullBenefits
            ? <TouchableOpacity activeOpacity={1} style={styles.secondaryButtonContainer} onPress={() => {
                onSeeBenefits(planViewModel);
                let eventDataAppsFlyer = {};
                eventDataAppsFlyer[EventKeys.AF_PRICE] = planViewModel?.plan?.price;
                eventDataAppsFlyer[EventKeys.AF_CONTENT_ID] = planViewModel?.plan?.planCode;
                eventDataAppsFlyer[EventKeys.AF_CONTENT_TYPE] = getProductWithServicesList(props?.destination, [planViewModel?.plan],[], buyPlanJourney?.product?.name);
                eventDataAppsFlyer[EventKeys.AF_CURRENCY] = CURRENCY;
                logAppsFlyerEvent(AF_CONTENT_VIEW,eventDataAppsFlyer)
            }}>
                <Text style={styles.secondaryButtonText}>See all benefits</Text>
                <View style={{ justifyContent: "flex-end" }}>
                    <Image source={images.arrow_down} style={styles.downArrow} />
                </View>
            </TouchableOpacity>
            : null
        )
    }

    return (
        <View style={[styles.planCardContainer, isShowFullBenefits ? {
            marginHorizontal: 0,
            marginTop: 0,
        } : styles.containerBorder]}>
            <View style={styles.planCardHeaderContainer}>

                <View style={{ flex: 1 }}>
                    <Text style={styles.planTitle}>{planViewModel?.title}</Text>
                    {planViewModel?.noOfClaimsView && !isShowFullBenefits
                        ? <Tooltip
                            animated={true}

                            contentStyle={{ backgroundColor: colors.charcoalGrey }}
                            arrowSize={{ width: 16, height: 8 }}
                            backgroundColor="rgba(0,0,0,0)"
                            isVisible={toolTipVisible}
                            content={<Text
                                style={styles.tooltipText}>{planViewModel?.noOfClaimsView?.tooltipText}</Text>}
                            placement="bottom"
                            showChildInTooltip={false}
                            onClose={() => setToolTipVisible(false)}
                        >
                            <TouchableOpacity activeOpacity={1} style={styles.noOfClaimsViewContainer} onPress={() => {
                                setToolTipVisible(true && planViewModel?.noOfClaimsView?.tooltipText);
                            }}>
                                <Text
                                    style={styles.planFeatureText}>{planViewModel?.noOfClaimsView?.claimsViewText}</Text>
                                {planViewModel?.noOfClaimsView?.tooltipText
                                    ? <Image source={images.blueInfo} style={styles.infoIcon} />
                                    : null}
                            </TouchableOpacity>
                        </Tooltip>
                        : null}
                </View>

                <View style={{ marginLeft: spacing.spacing20 }}>
                    <View style={{ flexDirection: "row" }}>
                        {(planViewModel?.anchorPrice != planViewModel?.price) ?
                            <Text style={styles.anchorPrice}>{planViewModel?.anchorPrice}</Text> : null}
                        <Text style={styles.priceText}>{planViewModel?.price}</Text>
                    </View>

                    {planViewModel?.offerText ? <Text style={styles.offerText}>{planViewModel?.offerText}</Text> : null}
                    <Text style={styles.planDurationText}>{planViewModel?.planDurationText}</Text>
                </View>
            </View>

            {!isShowFullBenefits && planViewModel?.planTagLine
                ? <View style={{ marginTop: spacing.spacing20 }}>
                    <Text style={styles.planDescription}>{planViewModel?.planTagLine}</Text>
                </View>
                : null}

            <ScrollView style={{ maxHeight: Dimensions.get("screen").height / 3 }} scrollEnabled={isShowFullBenefits}
                        showsVerticalScrollIndicator={false}>
                <View style={{ marginVertical: spacing.spacing20 }}>
                    {planViewModel?.planBenefits?.map((item, index) => {
                        return (index < planViewModel?.MAX_BENEFITS_IN_PLAN_CARD || isShowFullBenefits) ? (
                            <View key={index.toString()} style={styles.featureItemContainer}>
                                <Image source={images.compareTick} style={styles.imageStyle} />
                                <Text style={styles.featureText}>{item?.benefit}</Text>
                            </View>) : null;
                    })}
                </View>
            </ScrollView>
            {
                (planViewModel?.seeBenefitPosition == "top")  ? <View style={{marginBottom:10}}>{getSeeBenifitButtonVariation()}</View>  : null
            }
            <View style={{ marginHorizontal: spacing.spacing4, marginTop: isShowFullBenefits ? 20 : 0, marginBottom:10}}>
                {getButtonVariation()}
            </View>
            {
                (planViewModel?.seeBenefitPosition == "bottom")  ? getSeeBenifitButtonVariation(): null
            }

        </View>
    );
};

const styles = StyleSheet.create({
    planCardContainer: {
        backgroundColor: colors.white,
        marginTop: spacing.spacing24,
        marginHorizontal: spacing.spacing16,
        paddingTop: spacing.spacing28,
        paddingBottom: spacing.spacing10,
        paddingHorizontal: spacing.spacing12,
    },
    containerBorder: {
        borderRadius: 8,
        borderWidth: 1,
        borderColor: colors.color_97CFFF,
    },
    planCardHeaderContainer: {
        flexDirection: "row",
    },
    planTitle: {
        ...TextStyle.text_18_normal,
        color: colors.charcoalGrey,
    },
    planFeatureText: {
        ...TextStyle.text_14_bold,
        color: colors.charcoalGrey,
    },
    planPricesContainer: {
        flexDirection: "row",
    },
    anchorPrice: {
        ...TextStyle.text_12_regular,
        color: colors.grey,
        marginRight: spacing.spacing4,
        textDecorationLine: "line-through",
        alignSelf: "flex-end",
    },
    priceText: {
        ...TextStyle.text_20_bold,
        color: colors.charcoalGrey,
    },
    offerText: {
        ...TextStyle.text_12_regular,
        color: colors.color_45B448,
    },
    planDescription: {
        ...TextStyle.text_14_normal,
        color: colors.grey,
    },
    featureItemContainer: {
        flexDirection: "row",
        marginTop: spacing.spacing12,
    },
    imageStyle: {
        height: 12,
        width: 16,
        resizeMode: "contain",
        marginRight: spacing.spacing12,
        marginTop: spacing.spacing5,
    },
    featureText: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        flex: 1,
    },
    secondaryButtonContainer: {
        flexDirection: "row",
        height: 44,
        marginHorizontal: spacing.spacing4,
        justifyContent: "center",
        alignItems: "center",
    },
    secondaryButtonText: {
        ...TextStyle.text_14_bold,
        color: colors.color_0081F8,
    },
    downArrow: {
        height: 12,
        width: 12,
        tintColor: colors.color_0081F8,
        resizeMode: "contain",
        marginLeft: spacing.spacing4,
    },
    coverAmountContainer: {
        height: 40,
        paddingVertical: spacing.spacing8,
        paddingRight: spacing.spacing12,
        flexDirection: "row",
    },
    coverAmountText: {
        ...TextStyle.text_16_bold,
        color: colors.blue028,
    },
    editIcon: {
        height: 12,
        width: 12,
        resizeMode: "contain",
        tintColor: colors.blue028,
        marginLeft: spacing.spacing8,
        alignSelf: "center",
    },
    planDurationText: {
        ...TextStyle.text_12_regular,
        color: colors.grey,
        alignSelf: "flex-end",
    },
    noOfClaimsViewContainer: {
        flexDirection: "row",
        marginTop: spacing.spacing4,
    },
    infoIcon: {
        height: 18,
        width: 18,
        resizeMode: "cover",
        marginLeft: spacing.spacing4,
        alignSelf: "center",
    },
    tooltipText: {
        ...TextStyle.text_14_bold,
        color: colors.white,
    },
    primaryButton: {
        paddingHorizontal: spacing.spacing16,
        paddingVertical: spacing.spacing12,
        flexDirection: "row",
    },
    buttonAnchorText: {
        ...TextStyle.text_12_normal,
        color: colors.greye0,
        textDecorationLine: "line-through",
    },
});

export default PlanCard;
