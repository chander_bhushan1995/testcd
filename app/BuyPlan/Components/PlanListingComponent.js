import React from "react";
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import spacing from "../../Constants/Spacing";
import { coverAmountFormat } from "../../commonUtil/Formatter";
import images from "../../images/index.image";
import PlanCard from "./PlanCard";
import { TextStyle } from "../../Constants/CommonStyle";
import colors from "../../Constants/colors";

const PlanListingComponent = props => {

    const reducerState = props?.extraProps?.reducerState ?? {}
    const currentStep = props?.extraProps?.currentStep ?? {}
    const totalSteps = props?.extraProps?.totalSteps ?? {}
    const showCoverAmountsBottomSheet = props?.extraProps?.showCoverAmountsBottomSheet ?? (()=>{})
    const showPlanBenefitsBottomSheet = props?.extraProps?.showPlanBenefitsBottomSheet ?? (()=>{})
    const buyPlan = props?.extraProps?.buyPlan ?? (()=>{})
    const renewalData = props?.extraProps?.renewalData ?? null
    const showAllPlans = props?.extraProps?.showAllPlans ?? (()=>{})
    const destination = props?.extraProps?.destination
    const buyPlanJourney = props?.extraProps?.buyPlanJourney

    return (
        <>
            <View style={styles.screenStepperContainer}>
                <Text style={styles.stepsText}>STEP {currentStep} OF {totalSteps}</Text>
                <Text style={styles.choosePlanText}>{props.data?.headerText}</Text>
            </View>

            {reducerState?.coverAmountsViewModel
                ? <TouchableOpacity activeOpacity={1} style={styles.coverAmountDropdownContainer}
                                    onPress={showCoverAmountsBottomSheet}>
                    <Text style={styles.coverAmountHeading}>Cover amount</Text>
                    <View style={styles.dropdownContainer}>
                        <Text
                            style={styles.dropdownText}>{`Upto ₹ ${coverAmountFormat(reducerState?.coverAmountsViewModel?.selectedCoverAmount)} Cover Amount`}</Text>
                        <Image source={images.arrow_down} style={styles.downArrow} />
                    </View>
                </TouchableOpacity>
                : null}

            {reducerState?.planCardsToRender.map((planCardObj) => {
                let planCard = {...planCardObj}
                planCard.buttonVariation = props?.data?.planPrimaryButtonVariation
                planCard.seeBenefitPosition = props?.data?.seeBenefitPosition ?? "top"
                return <PlanCard planCardViewModel={planCard} onSeeBenefits={showPlanBenefitsBottomSheet}
                                 onBuyPlan={buyPlan} buttonTitle={renewalData ? "Renew Now" : "Buy Now"} destination={destination}
                                 buyPlanJourney={buyPlanJourney}
                />;
            })}

            {renewalData ? <TouchableOpacity onPress={showAllPlans}>
                <Text style={styles.chooseAnyOtherHeading}>or, choose any other plan</Text>
            </TouchableOpacity> : null}
        </>
    );
};

const styles = StyleSheet.create({
    screenStepperContainer: {
        marginTop: spacing.spacing16,
        marginHorizontal: spacing.spacing16,
    },
    stepsText: {
        ...TextStyle.text_12_bold,
        color: colors.color_848F99,
    },
    choosePlanText: {
        ...TextStyle.text_16_bold,
        color: colors.charcoalGrey,
    },
    coverAmountDropdownContainer: {
        marginTop: spacing.spacing16,
        marginHorizontal: spacing.spacing16,
    },
    coverAmountHeading: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
    },
    dropdownContainer: {
        marginTop: spacing.spacing8,
        borderRadius: 2,
        borderColor: colors.grey,
        borderWidth: 1,
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "space-between",
        padding: spacing.spacing12,
        backgroundColor: colors.white,
    },
    downArrow: {
        height: 14,
        width: 18,
        tintColor: colors.color_0081F8,
        resizeMode: "contain",
        marginLeft: spacing.spacing4,
    },
    dropdownText: {
        ...TextStyle.text_14_bold,
        color: colors.charcoalGrey,
    },
    chooseAnyOtherHeading: {
        ...TextStyle.text_16_bold,
        color: colors.color_0082F8,
        marginVertical: spacing.spacing24,
        paddingVertical: spacing.spacing4,
        textAlign: "center",
    },
});

export default PlanListingComponent;
