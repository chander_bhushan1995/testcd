import React, {useState} from 'react';
import {Text, TouchableOpacity, View, StyleSheet, Image} from 'react-native';
import {CommonStyle, TextStyle} from '../../Constants/CommonStyle';
import spacing from '../../Constants/Spacing';
import TextView from '../../Catalyst/components/common/TextView';
import colors from '../../Constants/colors';
import {ButtonsAction, handleBuyPlanBtnActions} from '../Helpers/BuyPlanButtonActions';

import Images from '../../images/index.image';


const QuestionCardComponent = props => {

    const headerText = props?.data?.header;
    const primaryAction = props?.data?.primaryButton;
    const secondaryAction = props?.data?.secondaryButton;
    const button = props?.data?.button;
    const image = props?.data?.image;
    const [isCorrectAnswer, setIsCorrectAnswer] = useState(false);
    const [isAnswered, setIsAnswered] = useState(false);
    const [answerObject, setAnswerObject] = useState(null);
    const handleButtonAction = (action, answer) => {
        if (action == ButtonsAction.CHECK_ANSWER) {
            setIsAnswered(true);
            if (props?.data?.correctAnswer?.toLowerCase() == answer?.toLowerCase()) {
                setIsCorrectAnswer(true);
                setAnswerObject(props?.data?.correctAnswerResponse);
            } else {
                setIsCorrectAnswer(false);
                setAnswerObject(props?.data?.incorrectAnswerResponse);
            }
        } else {
            handleBuyPlanBtnActions(button?.action, {...props.extraProps});
        }
    };

    return (
        <View style={styles.container}>

            {!isAnswered ? <>
                    <View style={{flexDirection: 'row'}}>
                        <View style={{flex: image ? 1 : 0}}><TextView style={styles.headerText} texts={headerText}/></View>
                        {image ? <Image style={styles.image} source={{uri: image}}/> : null}
                    </View>
                    <View style={styles.actionContainer}>
                        <View style={CommonStyle.separator}/>

                        {button
                            ? <TouchableOpacity style={{
                                alignItems: 'center',
                                width: '100%',
                                height: 48,
                                justifyContent: 'flex-end',
                                flexDirection: 'row',
                            }} onPress={() => {
                                handleBuyPlanBtnActions(button?.action, {...props.extraProps, data: button.data ?? {}});
                            }}>
                                <Text style={[styles.actionText, {marginRight: 4}]}>{button?.text}</Text>
                                <Image source={Images.rightBlueArrow} style={{marginRight: 16}}/>
                            </TouchableOpacity>

                            : <View style={{flexDirection: 'row'}}>
                                <TouchableOpacity style={styles.action} onPress={() => {
                                    handleButtonAction(secondaryAction.action, primaryAction.text);
                                }}>
                                    <Text style={styles.actionText}>{primaryAction?.text}</Text>
                                </TouchableOpacity>
                                <View style={{width: 1, backgroundColor: colors.lightGrey2}}/>
                                <TouchableOpacity style={styles.action} onPress={() => {
                                    handleButtonAction(secondaryAction.action, secondaryAction.text);
                                }}>
                                    <Text style={styles.actionText}>{secondaryAction?.text}</Text>
                                </TouchableOpacity>
                            </View>}
                    </View>
                </>

                : <View>
                    <TextView
                        style={[styles.answerHeader, {color: isCorrectAnswer ? colors.seaGreen : colors.badgeRed}]}
                        texts={answerObject?.header}
                    />
                    <TextView style={styles.answerDescription} texts={answerObject?.description}/>
                </View>
            }
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        ...CommonStyle.cardContainerWithShadow,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing8,
        overflow: 'hidden',
        borderRadius: 4,
        maxWidth: 270,
        height: 150,
        justifyContent: 'space-between',
    },
    headerText: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        marginTop: spacing.spacing16,
        marginHorizontal: spacing.spacing16,
    },
    actionContainer: {
        // marginTop: spacing.spacing40,
    },
    action: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 48,
    },
    actionText: {
        ...TextStyle.text_14_bold,
        color: colors.blue028,
    },
    answerHeader: {
        ...TextStyle.text_12_bold,
        marginHorizontal: spacing.spacing16,
        marginTop: spacing.spacing16,
    },
    answerDescription: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        marginTop: spacing.spacing4,
        marginHorizontal: spacing.spacing16,
    },
    image: {
        height: 46,
        width: 46,
        resizeMode: 'contain',
        marginRight: spacing.spacing8,
        marginLeft: spacing.spacing8,
        marginTop: spacing.spacing16,
    },
    buttonText: {
        ...TextStyle.text_16_bold,
        color: colors.blue028,
    },
});

export default QuestionCardComponent;
