import {FlatList, Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import React, {useEffect, useRef, useState} from "react";
import {TextStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import {AppStringConstants} from "../../Constants/AppStringConstants";
import SelectableOptionComponent from "../../Components/SelectableOptionComponent";
import colors from "../../Constants/colors";
import images from "../../images/index.image";
import {fetchPriceRanges} from "../../network/APIHelper";
import Loader from "../../Components/Loader";

const RangeSelectionComponent = (props) => {
    const [selectedPosition, setSelectedPosition] = useState(-1);
    const [showSubRange, setShowSubRange] = useState(false);
    const subRangeHorizontalListRef = useRef(null);
    const [isLoading, setIsLoading] = useState(true);
    const buyPlanJourney = props?.extraProps?.buyPlanJourney
    const [data, setData] = useState([])


    useEffect(() => {
        getPriceRanges()
    }, []);


    const getPriceRanges = () => {
        setIsLoading(true);
        fetchPriceRanges(buyPlanJourney.service, buyPlanJourney.product.code, buyPlanJourney.brandName, (response) => {
            setIsLoading(false);
            if (!response) {
                return;
            } else {
                setData(response?.data?.invoiceRange?.ranges ?? [])
            }
        });
    };

    const getSubRangeComponent = () => {
        const selectedItem = data[selectedPosition];
        return <View>
            <FlatList
                ref={subRangeHorizontalListRef}
                style={{marginLeft: spacing.spacing8, marginTop: spacing.spacing24}}
                data={data ?? []}
                initialScrollIndex={selectedPosition}
                onScrollToIndexFailed={info => {
                    const wait = new Promise(resolve => setTimeout(resolve, 100));
                    wait.then(() => {
                        subRangeHorizontalListRef.current?.scrollToIndex({index: selectedPosition, animated: true});
                    });
                }}
                renderItem={({item, index}) => {
                    return <SelectableOptionComponent itemData={item}
                                                      isSelected={index == selectedPosition}
                                                      onSelectionUpdate={(isSelected) => {
                                                          if (isSelected) {
                                                              setSelectedPosition(index);
                                                          }
                                                      }}/>;
                }}
                showsHorizontalScrollIndicator={false}
                bounces={false}
                horizontal={true}
                extraData={false}
                keyExtractor={(item, index) => {
                    return index.toString();
                }}
            />
            <Text style={{
                ...style.primaryText,
                marginTop: spacing.spacing32,
            }}>{AppStringConstants.PRICE_RANGE_TYPE_2_HEADER}</Text>
            <FlatList
                data={selectedItem.subRange ?? []}
                renderItem={({item, index, separators}) => (
                    <TouchableOpacity style={{
                        paddingHorizontal: spacing.spacing20,
                        paddingVertical: spacing.spacing18,
                    }} onPress={() => {
                        props.onSelectionChange(item);
                    }}>
                        <Text style={TextStyle.text_16_normal}>{item}</Text>

                    </TouchableOpacity>

                )}
                showsVerticalScrollIndicator={false}
                bounces={false}
                keyExtractor={(item, index) => {
                    return index.toString();
                }}
                ItemSeparatorComponent={(highlighted) => {
                    return <View
                        style={[style.divider,
                            highlighted && {marginLeft: 0},
                        ]}
                    />;
                }}

                extraData={false}
            />
        </View>;
    };


    if (isLoading) {
        return <Loader isLoading={isLoading} loaderStyle={{
            backgroundColor: "transparent",
            height: 200,
            width: "100%",
        }}/>
    }
    return (
        <View>
            <TouchableOpacity style={style.close} onPress={() => {
                props.onClose();
            }}>
                <Image source={images.closeSheet} style={style.closeIcon}/>
            </TouchableOpacity>
            {!showSubRange
                ? (<View style={style.container}>
                    <Text style={style.primaryText}>{AppStringConstants.PRICE_RANGE_TYPE_1_HEADER}</Text>
                    <View style={style.rangeContainer}>
                        {
                            data.map((item, index) => {

                                return <SelectableOptionComponent itemData={item}
                                                                  isSelected={index == selectedPosition}
                                                                  onSelectionUpdate={(isSelected) => {
                                                                      if (isSelected) {
                                                                          setSelectedPosition(index);
                                                                          if (item.subRange?.length > 0) {
                                                                              setShowSubRange(item);
                                                                          } else {
                                                                              props.onSelectionChange(item);
                                                                          }
                                                                      }
                                                                  }}/>;
                            })
                        }
                    </View>

                </View>)
                : getSubRangeComponent()
            }

        </View>
    );

};

const style = StyleSheet.create({
    container: {
        marginBottom: spacing.spacing32
    },
    primaryText: {
        ...TextStyle.text_16_bold,
        marginTop: spacing.spacing12,
        marginBottom: spacing.spacing12,
        paddingHorizontal: spacing.spacing20,

    },
    rangeContainer: {
        flexDirection: "row",
        flexWrap: "wrap",
        paddingVertical: spacing.spacing20,
        paddingHorizontal: spacing.spacing16

    },
    close: {
        height: 24,
        width: 24,
        borderRadius: 12,
        backgroundColor: colors.greyF1F2F3,
        alignItems: "center",
        justifyContent: "center",
        marginTop: spacing.spacing12,
        marginRight: spacing.spacing12,
        alignSelf: "flex-end",

    },
    closeIcon: {
        height: 48,
        width: 48,
        resizeMode: "contain",
    },
    divider: {
        height: 0.5,
        width: "100%",
        backgroundColor: colors.greye0,
    },
});

export default RangeSelectionComponent;
