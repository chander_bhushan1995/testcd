import { View, StyleSheet, Text, ScrollView, Image, TouchableOpacity } from "react-native";
import React, { useState } from "react";
import spacing from "../../../Constants/Spacing";
import { TextStyle } from "../../../Constants/CommonStyle";
import colors from "../../../Constants/colors";
import images from "../../../images/index.image";
import ButtonWithLoader from "../../../CommonComponents/ButtonWithLoader";
import CheckBox from "react-native-check-box";
import { ScreenDimension } from "../../../Constants/AppConstants";

const CheckboxListBottomSheet = props => {

    const checkboxListVM = props?.data?.checkboxListVM ?? [];
    const onSelectionComplete = props?.data?.onSelectionComplete ?? (() => {
    });

    const [selectedList, setSelectedList] = useState([]);

    const onSelectCheckBox = (isChecked, object) => {
        if (isChecked) {
            setSelectedList(selectedList.filter((obj) => {
                return obj.id !== object.id;
            }));
        } else {
            setSelectedList([...selectedList, object]);
        }
    };

    return (
        <View style={{ maxHeight: ScreenDimension.screenHeight * 0.60 }}>
            <ScrollView horizontal={false} showsVerticalScrollIndicator={false} bounces={false}>
                {checkboxListVM?.map((object,index) => {
                    let isChecked = selectedList.filter((obj) => {
                        return obj.id === object.id;
                    }).length > 0;
                    return <TouchableOpacity key={index.toString()} activeOpacity={1} style={styles.checkBoxContainer}
                                             onPress={() => {
                                                 onSelectCheckBox(isChecked, object);
                                             }}>
                        <CheckBox
                            style={styles.checkboxStyle}
                            checkedImage={<Image source={images.checkBoxChecked}
                                                 style={styles.checkboxStyle} />}
                            unCheckedImage={<Image source={images.checkBoxBlank}
                                                   style={styles.checkboxStyle} />}
                            isChecked={isChecked}
                            onClick={() => {
                                onSelectCheckBox(isChecked, object);
                            }
                            }
                        />
                        <Text style={styles.textStyle}>{object.text}</Text>
                    </TouchableOpacity>;
                })}
            </ScrollView>
            <View style={{ marginHorizontal: spacing.spacing16, marginVertical: spacing.spacing16 }}>
                <ButtonWithLoader
                    isLoading={false}
                    enable={selectedList?.length>0}
                    isHTMLText={true}
                    Button={{
                        text: "Done",
                        onClick: () => {
                            onSelectionComplete(selectedList);
                        },
                    }} />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    checkBoxContainer: {
        marginHorizontal: spacing.spacing16,
        marginTop: spacing.spacing16,
        flexDirection: "row",
    },
    checkboxStyle: {
        width: spacing.spacing20,
        height: spacing.spacing20,
        marginRight: spacing.spacing4,
    },
    textStyle: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        flex: 1,
    },
});

export default CheckboxListBottomSheet;
