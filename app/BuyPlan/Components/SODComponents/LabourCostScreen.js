import React from "react";
import { View, StyleSheet, Text, TouchableOpacity, ScrollView, Dimensions, FlatList, Modal } from "react-native";
import spacing from "../../../Constants/Spacing";
import { CommonStyle, TextStyle } from "../../../Constants/CommonStyle";
import colors from "../../../Constants/colors";
import ButtonWithLoader from "../../../CommonComponents/ButtonWithLoader";


const LabourCostScreen = props => {

  const onDismiss = props?.onDismiss ?? (() => {
  });

  const _renderItem = ({ item, index }) => {
    return (
      <View style={{ flexDirection: "row", borderBottomColor: colors.greye0, borderBottomWidth: 1 }}>
        <View style={[styles.cellPart, index == 0 ? { backgroundColor: colors.color_F6F6F6 } : null]}>
          <Text
            style={index == 0 ? styles.cellTextBold : styles.cellTextNormal}>{item.leftEndText}</Text>
        </View>
        <View style={{ height: "100%", width: 1, backgroundColor: colors.greye0 }} />
        <View style={[styles.cellPart, index == 0 ? { backgroundColor: colors.color_F6F6F6 } : null]}>
          <Text style={styles.cellTextBold}>{item.rightEndText}</Text>
        </View>
      </View>
    );
  };

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={props?.isShowLabourCostModal}
      onTouchCancel={true}
      onRequestClose={() => {
        onDismiss();
      }}
    >
      <TouchableOpacity activeOpacity={1} style={styles.container} onPress={() => {
        onDismiss();
      }}>

        <View onStartShouldSetResponder={(): boolean => true} style={styles.card}>
          <Text style={styles.heading}>{props?.data?.heading}</Text>
          <View style={{ maxHeight: Dimensions.get("screen").height / 3 }}>
            <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
              <View onStartShouldSetResponder={(): boolean => true}
                    style={styles.listContainer}>
                <FlatList data={props?.data?.cells}
                          renderItem={_renderItem} />
              </View>
            </ScrollView>
          </View>
          <Text style={styles.description}>{props?.data?.description}</Text>
          <View style={{
            marginHorizontal: spacing.spacing16,
            marginTop: spacing.spacing24,
            marginBottom: spacing.spacing16,
          }}>
            <ButtonWithLoader
              isLoading={false}
              enable={true}
              Button={{
                text: "Okay",
                onClick: () => {
                  onDismiss();
                },
              }
              } />
          </View>
        </View>
      </TouchableOpacity>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,0.7)",
    alignItems: "center",
    justifyContent: "center",
  },
  heading: {
    ...TextStyle.text_14_bold,
    color: colors.charcoalGrey,
    marginTop: spacing.spacing20,
    marginHorizontal: spacing.spacing16,
  },
  card: {
    ...CommonStyle.cardContainerWithShadow,
    width: Dimensions.get("screen").width - 16,
    borderRadius: 4,
  },
  cellPart: {
    flex: 1,
    padding: spacing.spacing16,
  },
  listContainer: {
    ...CommonStyle.cardContainerWithShadow,

    marginHorizontal: 16,
    marginTop: spacing.spacing16,
  },
  cellTextNormal: {
    ...TextStyle.text_12_normal,
    color: colors.charcoalGrey,
  },
  cellTextBold: {
    ...TextStyle.text_12_bold,
    color: colors.charcoalGrey,
  },
  description: {
    ...TextStyle.text_14_normal,
    color: colors.charcoalGrey,
    marginTop: spacing.spacing24,
    marginHorizontal: spacing.spacing16,
  },
});

export default LabourCostScreen;
