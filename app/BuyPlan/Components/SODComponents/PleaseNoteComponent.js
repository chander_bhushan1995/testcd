import React from 'react';
import {View, StyleSheet, FlatList, TouchableOpacity, Text} from 'react-native';
import spacing from '../../../Constants/Spacing';
import TextView from '../../../Catalyst/components/common/TextView';
import {TextStyle} from '../../../Constants/CommonStyle';
import colors from '../../../Constants/colors';
import {handleBuyPlanBtnActions} from '../../Helpers/BuyPlanButtonActions';


const PleaseNoteComponent = props => {
    const notes = props.data?.notes ?? [];
    const header = props.data?.header ?? {};

    const _renderComponent = ({item, index}) => {
        let note = item.description ?? {};
        let button = item.button ?? {};
        return (
            <View style={styles.noteContainer}>
                <View style={{flexDirection: 'row'}}>
                    <Text style={{marginRight: 4, ...TextStyle.text_14_bold}}>{'\u2022'}</Text>
                    <TextView texts={note} style={styles.noteText}/>
                </View>
                {button
                    ? <TouchableOpacity activeOpacity={1} onPress={() => {
                        handleBuyPlanBtnActions(button?.action, {...props.extraProps, data: button.data ?? {}});
                    }}>
                        <Text style={styles.buttonText}>{button?.text}</Text>
                    </TouchableOpacity>
                    : null}
            </View>
        );
    };


    return (
        <View style={styles.container}>
            <TextView texts={header} style={styles.header}/>
            <FlatList
                horizontal={false}
                showsHorizontalScrollIndicator={false}
                scrollEnabled={false}
                data={notes ?? []}
                renderItem={_renderComponent}
                showsVerticalScrollIndicator={false}
                bounces={false}
                keyExtractor={(item, index) => {
                    return index.toString();
                }}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginTop: spacing.spacing24,
        marginHorizontal: spacing.spacing16,
        backgroundColor: 'rgba(247,181,0,0.12)',
        borderColor: 'rgba(247,181,0,0.4)',
        borderWidth: 1,
        borderRadius: 4,
    },
    header: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        marginHorizontal: spacing.spacing16,
        marginTop: spacing.spacing16,
    },
    noteContainer: {
        marginHorizontal: spacing.spacing16,
        marginTop: spacing.spacing8,
    },
    noteText: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        flex: 1
    },
    buttonText: {
        ...TextStyle.text_14_bold,
        color: colors.blue028,
    },
});

export default PleaseNoteComponent;
