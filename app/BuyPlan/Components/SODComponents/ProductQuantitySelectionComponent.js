import React from "react";
import { View, StyleSheet, Text, TouchableOpacity, FlatList } from "react-native";
import spacing from "../../../Constants/Spacing";
import { TextStyle } from "../../../Constants/CommonStyle";
import colors from "../../../Constants/colors";

const ProductQuantitySelectionComponent = props => {

  const onchangeQuantity = props?.extraProps?.onchangeQuantity ?? (() => {
  });
  const data = props?.data;

  const _renderItem = ({ item, index }) => {
    const currentCount = item?.currentValue ?? 0;
    const maxValue = item?.maxValue ?? 0;
    const minValue = item?.minValue ?? 0;

    return (
      <View style={styles.container}>
        <View style={{ flex: 1 }}>
          <Text style={styles.title} numberOfLines={1}>{item?.title}</Text>
          <View style={{ flexDirection: "row" }}>
            {item?.subNote ? <Text style={styles.subNote}>{item?.subNote}</Text> : null}
            <Text style={styles.note}>{item?.note}</Text>
          </View>
          <Text style={styles.description}>{item?.description}</Text>
        </View>
        <View style={styles.buttonContainer}>
          {currentCount === minValue
            ? <TouchableOpacity activeOpacity={1} style={{
              backgroundColor: colors.blue028,
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
              width: 96,
            }} onPress={() => {
              onchangeQuantity(item?.planCode, item?.planName,1, maxValue, item?.price);
            }}>
              <Text style={styles.addButton}>{"ADD"}</Text>
            </TouchableOpacity>
            :
            <View style={{ flexDirection: "row", flex: 1, width: 96 }}>
              <TouchableOpacity activeOpacity={1} style={[styles.stepper]} onPress={() => {
                onchangeQuantity(item?.planCode, item?.planName,-1, maxValue, item?.price);
              }}><Text
                style={styles.stepperText}>-</Text></TouchableOpacity>
              <View style={[styles.stepper, { backgroundColor: colors.white }]}><Text
                style={styles.stepperText}>{currentCount}</Text></View>
              <TouchableOpacity activeOpacity={1} style={[styles.stepper]} onPress={() => {
                onchangeQuantity(item?.planCode, item?.planName,1, maxValue, item?.price);
              }}><Text
                style={styles.stepperText}>+</Text></TouchableOpacity>
            </View>
          }

        </View>
      </View>
    );
  };


  return (
    <FlatList
      horizontal={false}
      scrollEnabled={false}
      showsHorizontalScrollIndicator={false}
      data={data?.components ?? []}
      renderItem={_renderItem}
      showsVerticalScrollIndicator={false}
      bounces={false}
      keyExtractor={(item, index) => {
        return index.toString();
      }}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: spacing.spacing24,
    paddingRight: spacing.spacing16,
    paddingLeft: spacing.spacing32,
    flexDirection: "row",
    borderBottomWidth: 1,
    borderBottomColor: colors.lightGrey2,
  },
  title: {
    ...TextStyle.text_16_normal,
    color: colors.charcoalGrey,
  },
  subNote: {
    ...TextStyle.text_10_normal,
    color: colors.color_888F97,
    alignSelf: "center",
    marginRight: spacing.spacing4,
    textDecorationLine: "line-through",
    textDecorationStyle: "solid",
  },
  note: {
    ...TextStyle.text_14_bold,
    color: colors.charcoalGrey,
  },
  description: {
    ...TextStyle.text_12_normal,
    color: colors.color_888F97,
  },
  buttonContainer: {
    borderRadius: 2,
    overflow: "hidden",
    borderColor: colors.blue028,
    borderWidth: 1,
    alignItems: "center",
    justifyContent: "center",
    height: 32,
    marginLeft: spacing.spacing8,
  },
  addButton: {
    ...TextStyle.text_12_bold,
    color: colors.white,
  },
  stepper: {
    backgroundColor: "rgba(2, 130, 240, 0.2)",
    height: 32,
    width: 32,
    alignItems: "center",
    justifyContent: "center",
  },
  stepperText: {
    ...TextStyle.text_12_bold,
    color: colors.blue028,
  },
});

export default ProductQuantitySelectionComponent;
