import React, { useEffect, useState } from "react";
import { View, StyleSheet, Text, Modal, TouchableOpacity } from "react-native";
import spacing from "../../../Constants/Spacing";
import { TextStyle } from "../../../Constants/CommonStyle";
import colors from "../../../Constants/colors";
import InputBox from "../../../Components/InputBox";
import { EditProfileConstants, isEmailAddressValid, userNameRegex } from "../../../EditDetails/EditProfileUtils";
import ButtonWithLoader from "../../../CommonComponents/ButtonWithLoader";

const SODNotifyMeComponent = props => {

  const [email, setEmail] = useState({ email: "", error: null });
  const [customerName, setCustomerName] = useState({ name: "", error: null });
  const [isButtonEnabled, setButtonEnabled] = useState(false);

  const onSubmit = props?.onSubmit ?? (() => {
  });

  const onDismiss = props?.onDismiss ?? (() => {
  });

  useEffect(() => {
    checkButtonEnable();
  }, [email, customerName]);

  const checkButtonEnable = () => {
    if (userNameRegex.test(customerName.name.trim().toString()) && customerName.name.length > 0) {
      setButtonEnabled(true);
    } else {
      setButtonEnabled(false);
    }
  };

  const validateFields = () => {
    if (email.email.length == 0 || isEmailAddressValid(email.email)) {
      onSubmit(customerName.name, email.email);
    } else {
      setEmail(({ ...email, error: "Invalid EmailId" }));
    }
  };


  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={props?.isNotifyMe}
      onTouchCancel={false}
      onRequestClose={() => {
        onDismiss();
      }}
    >
      <View style={styles.mainContainer}>
        <View style={styles.container}>
          <Text style={styles.title}>{props?.title}</Text>
          <Text style={styles.description}>{props?.description}</Text>

          <InputBox
            maxLength={EditProfileConstants.INPUTBOX_MAX_MIN_LENGTH.MAXLENGTH_200}
            inputHeading={EditProfileConstants.INPUT_FIELDS_HEADING.ENTER_NAME}
            containerStyle={{ marginTop: spacing.spacing24 }}
            placeholder={EditProfileConstants.INPUT_FIELDS_HEADING.ENTER_NAME}
            isMultiLines={false}
            value={customerName?.name}
            errorMessage={customerName.error}
            editable={true}
            onChangeText={(text) => {
              text = text.replace(/\s{2,}/g, " ");
              setCustomerName({ name: text, error: "" });
            }}
          />

          <InputBox
            inputHeading={EditProfileConstants.INPUT_FIELDS_HEADING.ENTER_EMAIL}
            multiline={false}
            containerStyle={{ marginTop: spacing.spacing24 }}
            numberOfLines={1}
            maxLength={250}
            keyboardType={"email-address"}
            autoFocus={true}
            errorMessage={email.error}
            onChangeText={(text) => {
              setEmail(({ email: text, error: "" }));
            }
            }
            editable={true} />

          <View style={{ marginTop: spacing.spacing32 }}>
            <ButtonWithLoader
              isLoading={false}
              enable={isButtonEnabled}
              isHTMLText={false}
              Button={{
                text: "Submit",
                onClick: () => {
                  validateFields();
                },
              }} />
          </View>
          <TouchableOpacity style={styles.secondaryButton} onPress={() => {
            onDismiss();
          }}>
            <Text style={styles.secondaryButtonText}>{"Not Now"}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,0.7)",
    alignItems: "center",
    justifyContent: "center",
  },
  container: {
    borderRadius: 4,
    backgroundColor: colors.white,
    marginHorizontal: spacing.spacing16,
    paddingHorizontal: spacing.spacing16,
    paddingTop: spacing.spacing32,
  },
  title: {
    ...TextStyle.text_16_bold,
    color: colors.charcoalGrey,
  },
  description: {
    ...TextStyle.text_14_normal,
    color: colors.color_888F97,
    marginTop: spacing.spacing8,
  },
  secondaryButton: {
    height: 48,
    alignItems: "center",
    justifyContent: "center",
    marginTop: spacing.spacing12,
    marginBottom: spacing.spacing12,
  },
  secondaryButtonText: {
    ...TextStyle.text_14_bold,
    color: colors.blue028,
  },
});

export default SODNotifyMeComponent;
