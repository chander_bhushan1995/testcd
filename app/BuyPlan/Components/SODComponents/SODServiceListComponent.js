import {FlatList, Image, Text, TouchableOpacity, View, StyleSheet} from 'react-native';
import React from 'react';
import spacing from '../../../Constants/Spacing';
import {CommonStyle, TextStyle} from '../../../Constants/CommonStyle';
import colors from '../../../Constants/colors';
import images from '../../../images/index.image';

const ServiceCell = props => {
    let data = props.data
    let button = props.data?.button
    let position = props?.extraProps?.position ? props?.extraProps?.position : "Bottom"
    return (
        <TouchableOpacity activeOpacity={1} onPress={()=>{props?.extraProps?.checkAlreadyHaveMembership(button.data,position)}}>
            <View style={{flexDirection: 'row'}}>
                <Text style={styles.cellTitle} numberOfLines={1}>{data?.title ?? ''}</Text>
                <View style={styles.buttonContainer}>
                    <Text style={styles.buttonText}>{data?.button?.text}</Text>
                    <Image style={styles.buttonImage} source={images.rightBlueArrow}/>
                </View>
            </View>
            <Text style={[styles.cellDescription,{color: data?.descriptionColor}]}>{data?.description ?? ''}</Text>

            {props.isSeperatorVisible ? <View style={styles.cellSeperator}/> : null}
        </TouchableOpacity>
    );
};


const SODServiceListComponent = props => {
    let data = props.data ?? {}

    return (
        <View style={styles.serviceListContainer}>
            <Text style={styles.serviceListHeader}>{data?.title}</Text>
            <View style={styles.serviceListCard}>
                <FlatList data={data.components ?? []}
                          renderItem={({item, index, separators}) => <ServiceCell extraProps={props.extraProps} data={item} isSeperatorVisible={index < data?.components?.length-1}/>}
                          scrollEnabled={false}/>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({

    serviceListContainer: {
        backgroundColor: 'rgb(100, 117, 223)',
        paddingVertical: spacing.spacing12,
    },
    serviceListHeader: {
        ...TextStyle.text_18_bold,
        color: 'white',
        marginHorizontal: spacing.spacing24,
    },
    serviceListCard: {
        ...CommonStyle.cardContainerWithShadow,
        marginTop: spacing.spacing24,
        marginHorizontal: spacing.spacing12,
        borderRadius: 4,
        shadowColor: colors.lightGrey,
    },
    cellTitle: {
        ...TextStyle.text_16_bold,
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing16,
        flex: 1,
    },
    cellDescription: {
        ...TextStyle.text_12_normal,
        color: colors.seaGreen,
        marginTop: spacing.spacing4,
        marginLeft: spacing.spacing16,
        marginBottom: spacing.spacing16,
    },
    cellSeperator: {
        ...CommonStyle.separator,
        alignSelf: 'flex-end',
    },
    buttonContainer: {
        marginTop: spacing.spacing16,
        marginRight: spacing.spacing16,
        alignItems: 'center',
        flexDirection: 'row',
    },
    buttonText: {
        ...TextStyle.text_12_bold,
        color: colors.blue028,
    },
    buttonImage: {
        height: 10,
        width: 5,
        resizeMode: 'contain',
        marginLeft: 8,
    },

});

export default SODServiceListComponent;
