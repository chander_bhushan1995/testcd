import React, { useEffect } from "react";
import { View, StyleSheet, Text, ScrollView } from "react-native";
import spacing from "../../../Constants/Spacing";
import { TextStyle } from "../../../Constants/CommonStyle";
import colors from "../../../Constants/colors";
import { ScreenDimension } from "../../../Constants/AppConstants";
import LeftImageTextComponent from "../LeftImageTextComponent";
import images from "../../../images/index.image";


const TextListBottomSheet = props => {

    const listOfText = props?.data?.listOfText ?? [];
    const imageStyle = props?.data?.imageStyle
    const image = props?.data?.image ?? images.notCovered

    return (
        <View style={{ paddingBottom: spacing.spacing20, maxHeight: ScreenDimension.screenHeight * 0.60 }}>
            <ScrollView horizontal={false} showsVerticalScrollIndicator={false} bounces={false}>
                {listOfText?.map((text, index) => {
                    return <View style={styles.noteContainer} key={index.toString()}>
                        <LeftImageTextComponent data={{
                            image: image,
                            isLocalImage: true,
                            imageStyle: imageStyle,
                            paragraph: {
                                "fontSize": 16,
                                "value": text,
                                "isHTML": false
                            } }} />
                    </View>;
                })}
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    noteContainer: {
        marginTop: spacing.spacing12,
    },
    noteText: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        flex: 1,
    },
});

export default TextListBottomSheet;
