import React from "react";
import { View, StyleSheet, Image, Text } from "react-native";
import spacing from "../../../Constants/Spacing";
import images from "../../../images/index.image";
import { TextStyle } from "../../../Constants/CommonStyle";
import colors from "../../../Constants/colors";


const TimeSlotsComponent = props => {
    const image = props?.data?.image ?? images.pinCodeRepairs;
    return (
        <View style={styles.container}>
            <Image style={styles.image} source={image} />
            <Text style={styles.description} numberOfLines={1}>{props?.data?.description}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginTop: spacing.spacing24,
        marginHorizontal: spacing.spacing16,
        flexDirection: "row",
    },
    description: {
        ...TextStyle.text_14_normal,
        color: colors.grey,
        flex: 1,
        alignSelf: "center",
    },
    image: {
        height: 20,
        width: 20,
        marginRight: spacing.spacing8,
    },
});

export default TimeSlotsComponent;
