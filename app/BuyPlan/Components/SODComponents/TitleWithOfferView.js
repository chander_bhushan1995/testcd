import React from 'react';
import {View, StyleSheet, Text, Image, TouchableOpacity} from 'react-native';
import {CommonStyle, TextStyle} from '../../../Constants/CommonStyle';
import colors from '../../../Constants/colors';
import spacing from '../../../Constants/Spacing';
import LinearGradient from 'react-native-linear-gradient';
import images from '../../../images/index.image';


const TitleWithOfferView = props => {
    const gradientColors = props?.data?.gradientColors ?? [colors.color_17D18D, colors.color_1DC5C7];
    const image = props?.data?.image ?? images.offersIcon;
    return (
        <View style={styles.container}>
            <Text numberOfLines={1} style={styles.title}>{props.data?.title}</Text>

            {props?.data?.description
                ? <TouchableOpacity activeOpacity={1} style={{flex: 1}} onPress={() => {
                    props?.extraProps?.onTappedOfferView(props?.data?.couponCode);
                }}>
                    <LinearGradient style={styles.gradientViewStyle} start={{x: 0, y: 0}}
                                    end={{x: 1, y: 0}} colors={gradientColors}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Image source={image} style={styles.image}/>
                            <Text style={styles.description}>{props?.data?.description}</Text>
                        </View>
                    </LinearGradient>
                </TouchableOpacity>
                : null}
            <View style={CommonStyle.separator}/>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {},
    title: {
        ...TextStyle.text_16_bold,
        color: colors.charcoalGrey,
        marginHorizontal: spacing.spacing16,
        marginTop: spacing.spacing32,
        marginBottom: spacing.spacing24,
    },
    image: {
        height: 18,
        width: 18,
        marginLeft: spacing.spacing16,
        marginVertical: spacing.spacing8,
        marginRight: spacing.spacing4,
        resizeMode: 'contain',
    },
    description: {
        ...TextStyle.text_12_bold,
        color: colors.white,
        alignSelf: 'center',
    },
});

export default TitleWithOfferView;
