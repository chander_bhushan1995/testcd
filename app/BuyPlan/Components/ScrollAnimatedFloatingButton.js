import React, {useEffect, useRef} from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import spacing from '../../Constants/Spacing';
import colors from '../../Constants/colors';
import TextView from '../../Catalyst/components/common/TextView';
import {TextStyle} from '../../Constants/CommonStyle';
import RightArrowButton from '../../CommonComponents/RightArrowButton';
import * as Animatable from 'react-native-animatable';
import {handleBuyPlanBtnActions} from '../Helpers/BuyPlanButtonActions';


const ScrollAnimatedFloatingButton = props => {

    const AnimatableView = useRef(null);
    const description = props?.extraProps?.reducerState?.floatingActionData?.description ?? props?.data?.description ?? {
        value: '',
        isHTML: false,
    };
    const primaryButton = props?.extraProps?.reducerState?.floatingActionData?.primaryButton ?? props?.data?.primaryButton;
    const extraProps = props?.extraProps ?? {};
    const offset = props.bottomOffset ?? 0

    useEffect(() => {
        if (props.isVisible) {
            AnimatableView.current.fadeInUp(500).then(endState => console.log(endState.finished ? 'fade finished' : 'fade cancelled'));
        } else {
            AnimatableView.current.fadeOutDown(500).then(endState => console.log(endState.finished ? 'fade finished' : 'fade cancelled'));
        }
    }, [props.isVisible]);

    const performButtonAction = () => {
        handleBuyPlanBtnActions(props?.data?.primaryButton?.action, {
            ...extraProps,
            data: props?.data?.primaryButton?.data,
        });
    };

    return (
        <Animatable.View ref={AnimatableView} style={{backgroundColor: 'transparent'}}>
            <TouchableOpacity activeOpacity={1} style={[styles.container,{marginBottom: offset}]} onPress={performButtonAction}>
                <View style={{flex: 1}}><TextView style={styles.paragraph}
                                                  texts={description}/></View>
                <RightArrowButton text={primaryButton?.text} style={styles.rightArrowContainer}
                                  textStyle={styles.buttonText}
                                  imageStyle={styles.rightArrowStyle}
                                  onPress={performButtonAction}/>
            </TouchableOpacity>
        </Animatable.View>
    );
};

const styles = StyleSheet.create({
    container: {
        borderRadius: 6,
        marginHorizontal: spacing.spacing20,
        paddingVertical: spacing.spacing16,
        paddingHorizontal: spacing.spacing12,
        backgroundColor: colors.blue028,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 20,
        elevation: 4,
        shadowOpacity: 0.3,
        shadowOffset: {
            width: 4,
            height: 4,
        },
        shadowColor: '#000000',
    },
    paragraph: {
        ...TextStyle.text_14_normal,
        color: colors.white,
        marginRight: spacing.spacing16,
    },
    rightArrowContainer: {
        marginBottom: null,
        alignSelf: 'center',
    },
    rightArrowStyle: {
        width: 8,
        height: 12,
        marginLeft: spacing.spacing4,
        resizeMode: 'stretch',
        tintColor: 'white',
    },
    buttonText: {
        ...TextStyle.text_16_bold,
        color: colors.white,
    },
});

export default ScrollAnimatedFloatingButton;
