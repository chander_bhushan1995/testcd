import React, {useEffect, useRef, useState} from "react";
import {Dimensions, FlatList, StyleSheet, View} from "react-native";
import ScrollableTabView, {ScrollableTabBar} from "../../scrollableTabView";
import colors from "../../Constants/colors";
import spacing from "../../Constants/Spacing";
import {getComponent} from "../../Catalyst/components/ComponentsFactory";
import {logWebEnageEvent} from "../../commonUtil/WebengageTrackingUtils";
import * as WebEngageKeys from "../../Constants/WebengageAttrKeys";

const screenHeight = Dimensions.get("screen").height
const maxSheetHeight = screenHeight - 90
const SectionView = props => {
    const data = props?.data ?? {};

    const _renderComponent = ({item}) => {
        const Component = getComponent(item.type);
        return <Component data={item} extraProps={props.extraProps}/>;
    };

    return (
        <View style={{flex: 1}}
        >
            <FlatList
                onLayout={(event) => {
                    props.updateHeight(event.nativeEvent.layout.height)
                }}
                contentContainerStyle={{flex: 1}}
                data={data?.components ?? []}
                renderItem={_renderComponent}
                showsVerticalScrollIndicator={false}
                bounces={false}
                keyExtractor={(item, index) => {
                    return index.toString();
                }}
                listKey={(item, index) => {
                    return index;
                }}
                scrollEnabled={false}

            />
        </View>
    );
};

let tabHeights = []
const SectionTabContainer = props => {
    const data = props?.data ?? {};
    const selectedIndex = props?.extraProps?.initialSelectedIndex ?? 0;
    const tabs = data?.tabs ?? [];
    const tabBarRef = useRef(null);
    const extraProps = props.extraProps ?? {};
    const [height, updateHeight] = useState(maxSheetHeight)
    const currentSelectedIndex = useRef(selectedIndex)

    useEffect(() => {
        return () => {
            tabHeights = []
        }
    }, props?.data)

    if (tabs.length <= 0) {
        return null;
    }

    const addTabHeight = (tabHeight) => {
        tabHeights.push(tabHeight + 50)
        let maxHeight = Math.max(...tabHeights)
        updateHeight(maxHeight)
    }

    const getGenericEventData = () => {
        let eventData = {}
        eventData[WebEngageKeys.LOCATION] = extraProps.location;
        eventData[WebEngageKeys.CATEGORY] = extraProps?.category;
        eventData[WebEngageKeys.PRODUCT] = extraProps?.product;
        eventData[WebEngageKeys.SERVICE] = extraProps?.service;
        return eventData
    }

    return (
        <View style={styles.container}>
            <ScrollableTabView style={{flex: 1, height: height}}
                               initialPage={selectedIndex}
                               prerenderingSiblingsNumber={5}
                               onChangeTab={({i, ref}) => {
                                   if (currentSelectedIndex.current != i){
                                       let selectedTabData = tabs[i];
                                       if (selectedTabData?.data?.eventName) {
                                           let eventData = {...getGenericEventData(),...extraProps?.eventsData, ...(selectedTabData?.data?.eventData ?? {})}
                                           logWebEnageEvent(selectedTabData?.data?.eventName, eventData)
                                       }
                                       currentSelectedIndex.current = i
                                   }
                               }}
                               ref={tabBarRef}
                               renderTabBar={() => <ScrollableTabBar tabsContainerStyle={styles.tabContainerView}
                                                                     underlineStyle={styles.tabBarUnderlineView}
                                                                     textStyle={styles.tabBarTextStyle}
                                                                     activeTextColor={colors.blue028}
                                                                     inactiveTextColor={colors.grey}/>}
            >


                {tabs.map((tabsData, index) => {
                    return <SectionView tabLabel={tabsData?.title ?? ""} key={index} data={tabsData}
                                        extraProps={extraProps}
                                        updateHeight={addTabHeight}/>;
                })}
            </ScrollableTabView>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginTop: spacing.spacing16,
        flex: 1,
    },
    tabBarTextStyle: {
        fontSize: 13,
        fontFamily: "Lato-Semibold",
        lineHeight: 20,
    },
    tabBarUnderlineView: {
        backgroundColor: colors.blue028,
        height: spacing.spacing4,
        borderRadius: spacing.spacing2,
    },
    tabContainerView: {
        borderBottomColor: colors.blue028,
    },
});

export default SectionTabContainer;
