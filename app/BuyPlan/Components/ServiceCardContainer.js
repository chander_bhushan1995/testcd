import spacing from "../../Constants/Spacing";
import { TextStyle, CommonStyle } from "../../Constants/CommonStyle";
import { Text, View, StyleSheet, TouchableOpacity, FlatList } from "react-native";
import React from "react";
import TagComponent from "../../Catalyst/components/common/TagsComponent";
import colors from "../../Constants/colors";
import TextView from "../../Catalyst/components/common/TextView";
import { getComponent } from "../../Catalyst/components/ComponentsFactory";
import { handleBuyPlanBtnActions } from "../Helpers/BuyPlanButtonActions";


const ServiceCardContainer = props => {

    const tag = props.data?.tag ?? null;


    const _renderComponent = ({ item, index }) => {
        const Component = getComponent(item.type);
        return <Component data={item} extraProps={props.extraProps} />;
    };

    return (
        <View style={styles.component}>
            {tag
                ? <TagComponent tags={[tag]}
                                style={styles.tagContainerStyle}
                                singleTagStyle={styles.singleTagStyle}
                                textStyle={{ ...TextStyle.text_10_bold, color: colors.white }} />
                : null
            }
            <TextView style={styles.headerStyle} texts={props?.data?.header} />
            {props?.data?.paragraph ? <TextView style={styles.paragraph} texts={props?.data?.paragraph} /> : null}
            <View style={{ marginTop: spacing.spacing12 }}>
                <FlatList
                    data={props?.data?.components ?? []}
                    renderItem={_renderComponent}
                    showsVerticalScrollIndicator={false}
                    bounces={false}
                    keyExtractor={(item, index) => {
                        return index.toString();
                    }}
                />
            </View>
            <TouchableOpacity style={styles.button}
                              onPress={() => handleBuyPlanBtnActions(props?.data?.button?.action, {
                                  ...props.extraProps,
                                  data: props?.data?.button?.data,
                              })}>
                <Text style={styles.buttonText}>{props.data?.button?.text}</Text>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    component: {
        ...CommonStyle.cardContainerWithShadow,
        marginBottom: spacing.spacing20,
        marginHorizontal: spacing.spacing16,
        borderRadius: 6,
    },
    singleTagStyle: {
        backgroundColor: colors.saffronYellow,
        borderWidth: 2,
        paddingHorizontal: spacing.spacing8,
        paddingVertical: spacing.spacing0,
        borderColor: colors.transparent,
        borderRadius: 2,
    },
    tagContainerStyle: {
        position: "absolute",
        marginTop: 0,
        top: 0,
        right: 0,
    },
    headerStyle: {
        ...TextStyle.text_16_normal,
        color: colors.charcoalGrey,
        marginHorizontal: spacing.spacing16,
        marginTop: spacing.spacing36,
    },
    paragraph: {
        ...TextStyle.text_14_normal,
        marginTop: spacing.spacing12,
        marginHorizontal: spacing.spacing20,
    },
    button: {
        backgroundColor: colors.blue028,
        alignSelf: "flex-end",
        alignItems: "center",
        paddingVertical: spacing.spacing12,
        paddingHorizontal: spacing.spacing20,
        margin: spacing.spacing16,
        borderRadius: 4,
        minWidth: 96 + (2 * spacing.spacing20), // text size + horizontal spacing
    },
    buttonText: {
        ...TextStyle.text_16_bold,
        color: colors.white,
    },
});

export default ServiceCardContainer;
