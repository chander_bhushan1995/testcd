import React from "react";
import { View, StyleSheet, Image } from "react-native";
import spacing from "../../Constants/Spacing";
import TextView from "../../Catalyst/components/common/TextView";
import { TextStyle } from "../../Constants/CommonStyle";
import colors from "../../Constants/colors";

const SimpleImageTextComponent = props => {
    const data = props.data ?? {};
    const paragraphText = data.paragraph || {};
    const image = data.image ? { uri: data.image } : null;
    const paragraphFontSize = data?.paragraph?.fontSize ? data?.paragraph?.fontSize : styles.paragraph.fontSize;
    const paragraphFontColor = data?.paragraph?.fontColor ? data?.paragraph?.fontColor : styles.paragraph.color;
    return (
        <View style={styles.component}>
            {image ? <Image source={image} style={styles.image} /> : null}
            <View>
                <TextView style={{ ...styles.paragraph, fontSize: paragraphFontSize, color: paragraphFontColor }}
                          texts={paragraphText} />

            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    component: {
        flex: 1,
        flexDirection: "row",
        marginVertical: spacing.spacing12,
        marginHorizontal: spacing.spacing12,
    },
    paragraph: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        flex: 1,
        flexWrap: "wrap",
        marginRight: spacing.spacing20,
    },
    image: {
        width: 32,
        height: 32,
        marginRight: spacing.spacing8,
    },
    circle: {
        width: 28,
        height: 28,
        borderRadius: 14,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: colors.color_EDF5FE,
        borderWidth: 1,
        borderColor: colors.color_B4D9FB,
    },
    circleText: {
        ...TextStyle.text_14_bold,
        color: colors.charcoalGrey,
    },
});
export default SimpleImageTextComponent;
