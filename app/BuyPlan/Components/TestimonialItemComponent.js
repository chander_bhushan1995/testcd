import {Text, View, StyleSheet, Image, TouchableOpacity} from "react-native";
import colors from "../../Constants/colors";
import React from "react";
import {TextStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import TextView from "../../Catalyst/components/common/TextView";
import images from "../../images/index.image";

const TestimonialItemComponent = props => {
    let initialsText = ""
    let splitWords = props?.data?.splitName?.split(" ")

    for (let word of splitWords) {

        initialsText = initialsText + word.charAt(0)
    }

    return (
            <View style={style.container}>
                <View style={{...style.initialsViewStyle, backgroundColor: props?.data?.color}}>

                    <Text style={style.initialsText}>{initialsText?.toUpperCase()}</Text>
                </View>
                <View style={{flex: 3}}>
                    <TextView style={style.headerStyle} texts={props?.data?.header} showReadMore={props.showReadMore ?? false} numberOfLines={ props.showReadMore ? 2 : 0} onClickReadMore={props?.onClickReadMore}/>
                    {props?.data?.paragraph ? <TextView style={{...style.paragraph, marginTop: spacing.spacing16}}
                                                        texts={props?.data?.paragraph}/> : null}
                </View>

                {props?.data?.rating ? <View style={{flexDirection: 'row'}}>
                    <Text
                        style={{...style.rating, fontFamily: 'Lato-Bold'}}>{props?.data?.rating}</Text>
                    <Image style={style.starImage} source={images.star}/>
                </View> : null
                }

            </View>
    )
}


const style = StyleSheet.create({
    container: {
        flexDirection: 'row',
        margin: spacing.spacing16
    },
    initialsViewStyle: {
        borderRadius: 50,
        width: 36,
        height: 36,
        justifyContent: 'center',
    },
    initialsText: {
        ...TextStyle.text_16_normal,
        color: colors.white,
        alignSelf: 'center',
    },
    headerStyle: {
        ...TextStyle.text_14_normal,
        color: colors.black,
        marginHorizontal: spacing.spacing16,
    },
    rating: {
        ...TextStyle.text_16_normal,
        color: colors.black,
    },
    paragraph: {
        ...TextStyle.text_12_normal,
        marginHorizontal: spacing.spacing16,
    },
    starImage: {
        width: 18,
        height: 18,
        resizeMode: 'contain',
        marginLeft: spacing.spacing4,
        marginTop: spacing.spacing2
    },
    // divider: {
    //     height: 1,
    //     backgroundColor: colors.lightGrey
    // }
});
export default TestimonialItemComponent;
