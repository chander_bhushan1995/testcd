import {FlatList, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import colors from "../../Constants/colors";
import React, {useEffect, useState} from "react";
import {TextStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import TextView from "../../Catalyst/components/common/TextView";
import TestimonialItemComponent from "./TestimonialItemComponent";
import {ButtonsAction, handleBuyPlanBtnActions} from "../Helpers/BuyPlanButtonActions";
import {fetchTestimonialData} from "../../network/APIHelper";
import Loader from "../../Components/Loader";
import * as WebEngageKeys from "../../Constants/WebengageAttrKeys";
import * as WebEngageEventsName from "../../Constants/WebengageEvents";
import { logWebEnageEvent } from "../../commonUtil/WebengageTrackingUtils";

let page
let reviewData = {}
const TestimonialListContainer = props => {
    const { data, extraProps } = props
    const [button, setButton] = useState(data?.button)

    const [listData, setListData] = useState(data?.singleItemData ?? data?.data ?? [])
    const [isLoading, setIsLoading] = useState(false);
    const category = extraProps?.category
    const service = extraProps?.service ?? ""
    const product = extraProps?.product ?? ""
    const pageSize = button?.data?.pageSize ?? (data?.pageSize ?? 2)
    const [showSingleReview, setShowSingleReview] = useState(data?.showSingleReview ?? false)

    useEffect(() => {
        if (showSingleReview) { return }
        page = data?.nextPage ?? 1
        getTestimonialData(page)
    }, [showSingleReview])

    const getTestimonialData = (page) => {
        setIsLoading(true);
        fetchTestimonialData(category, service, product, page, pageSize, (response, error) => {
            if (error || !response) {
                setIsLoading(false);
                return
            }
            const updatedData = [...listData];
            reviewData = response.data
            updatedData.push(...response.data?.Reviews ?? [])

            setListData(updatedData)
            setIsLoading(false);
        });
    };

    const loadNextPage = () => {
        if (page < reviewData.totalPages) {
            page = page + 1;
            getTestimonialData(page)
        }
    }

    const expandSingleReviewToList = () => {
        let eventData = {};
        eventData[WebEngageKeys.LOCATION] = extraProps?.location;
        eventData[WebEngageKeys.CATEGORY] = category;
        eventData[WebEngageKeys.VARIATIONID] = extraProps?.eventPageVariationId;
        eventData[WebEngageKeys.PRODUCT] = product;
        eventData[WebEngageKeys.SERVICE] = service;
        logWebEnageEvent(WebEngageEventsName.VIEW_MORE_REVIEWS_EVENT, eventData);
        setListData(data?.data ?? []);
        setButton(null)
        setShowSingleReview(false)
    }
    
    const showReviewsSheet = (singleItemData) => {
        let eventData = {};
        eventData.location = extraProps?.location;
        eventData.category = category;
        eventData.service = service;
        eventData.product = product;
        const actionData = {
            ...button?.data,
            loadedData: listData,
            singleItemData: singleItemData,
            extraProps: extraProps,
            nextPage: 2,
            pageSize: pageSize,
            showSingleReview: singleItemData ? true : false,
            showReadMore: false,
            button: singleItemData ? button : null,
        };
        handleBuyPlanBtnActions(singleItemData ? ButtonsAction.SHOW_TESTIMONIAL_READ_MORE_BOTTOM_SHEET : ButtonsAction.SHOW_TESTIMONIAL_BOTTOM_SHEET, {
            ...eventData,
            ...extraProps,
            data: actionData
        });
    }

    const showReview = (data) => {
        showReviewsSheet(data)
    }

    const showReviewList = () => {
        showReviewsSheet(null)
    }
    
    const _renderComponent = ({item, index, separators}) => {
        return <TestimonialItemComponent onShowUnderlay={separators.highlight}
                                         onHideUnderlay={separators.unhighlight}
                                         onClickReadMore={() => showReview([item])} 
                                         showReadMore={data.showReadMore ?? true}
                                         data={item}/>;
    }

    const footerComponent = () => {
        {
            return (((listData && listData.length && page < reviewData.totalPages) || showSingleReview) && button)
                ? <TouchableOpacity style={style.secondaryButton}
                                    onPress={() => {
                                        if (showSingleReview) {
                                            expandSingleReviewToList()
                                        } else {
                                            showReviewList()
                                        }
                                    }}>
                    <Text style={style.secondaryButtonText}>{button?.text}</Text>
                </TouchableOpacity>
                : null;
        }
    };

    const itemSeparator = (highlighted) => {
        return <View
            style={style.divider}
        />;
    };

    const getContentView = () => {
        return <View style={style.container}>
            {
                data?.header && !showSingleReview
                    ? <TextView style={style.headerStyle} texts={data?.header}/>
                    : null
            }

            <FlatList
                data={listData ?? []}
                renderItem={_renderComponent}
                showsVerticalScrollIndicator={false}
                ItemSeparatorComponent={itemSeparator}
                ListFooterComponent={footerComponent}
                onEndReached={() => {
                    if (!button) {
                        loadNextPage()
                    }
                }}
                onEndReachedThreshold={0.1}
                bounces={false}
                keyExtractor={(item, index) => {
                    return index.toString();
                }}
                extraData={false}
            />
            <View style={style.divider}/>

        </View>
    }

    if (!listData || (listData.length === 0)) {
        return null
    } else {
        return (
            <View>
                {(listData) && getContentView()}
                {(isLoading) && <View style={{height: spacing.spacing50, marginTop: spacing.spacing16}}>
                    <Loader isLoading={isLoading}/>
                </View>}

            </View>
        );
    }
};

const style = StyleSheet.create({

    container: {
        backgroundColor: colors.white,
    },

    headerStyle: {
        ...TextStyle.text_16_bold,
        color: colors.black,
        marginHorizontal: spacing.spacing16,
        marginBottom: spacing.spacing16
    },
    paragraph: {
        ...TextStyle.text_14_normal,
        marginHorizontal: spacing.spacing20,
    },

    divider: {
        height: 1,
        width: '100%',
        backgroundColor: colors.color_E0E0E0
    },
    secondaryButton: {
        height: 48,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: spacing.spacing4,
        borderTopWidth: 1,
        borderBottomWidth: 0.5,
        borderColor: colors.color_E0E0E0
    },
    secondaryButtonText: {
        ...TextStyle.text_14_bold,
        color: colors.blue028,
    },
});
export default TestimonialListContainer;
