import {Image, StyleSheet, TouchableOpacity, View} from "react-native";
import React from "react";
import {TextStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import TextView from "../../Catalyst/components/common/TextView";
import Images from "../../images/index.image";
import RatingBarTemplate from "../../Catalyst/components/common/RatingBarTemplate";
import colors from "../../Constants/colors";

const VerticalGridItemComponent = props => {
    const paragraphColor = props?.data?.paragraph?.color ?? colors.grey
    const dividerStyle = props?.dividerStyle ?? {}
    const headerFont = props?.data?.header?.fontSize ?? style.header.fontSize
    const paragraphFont = props?.data?.paragraph?.fontSize ?? style.paragraph.fontSize

    return (
        <TouchableOpacity activeOpacity={1} style={[style.container, dividerStyle]}
        onPress={()=>{
            props.onItemClick(props.data?.button)
        }}>

            {props?.data?.image ? <Image style={style.leftImage} source={{uri: props?.data?.image}}></Image> : null }

            <View>
                <TextView style={{...style.header, fontSize: headerFont}} texts={props?.data?.header}/>
                {props?.data?.paragraph ? <TextView style={{...style.paragraph, fontSize: paragraphFont, color: paragraphColor}}
                                                    texts={props?.data?.paragraph}/> : null}
            </View>

        </TouchableOpacity>
    )
}


const style = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingHorizontal: spacing.spacing8,
        paddingVertical: spacing.spacing20
    },
    leftImage: {
        width: 40,
        height: 40,
        resizeMode:'contain'
    },
    header: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        marginTop: spacing.spacing8,
        alignSelf:'center',
        textAlign: 'center'
    },
    paragraph: {
        ...TextStyle.text_14_normal,
        marginHorizontal: spacing.spacing8,
        marginTop: spacing.spacing4,
        textAlign: 'center'
    },
    starImage: {
        height: 10,
        width: 10,
        marginRight: spacing.spacing2,
    }
});
export default VerticalGridItemComponent;
