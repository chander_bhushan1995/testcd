import {Text, View, StyleSheet, Image, TouchableOpacity, ImageBackground} from "react-native";
import colors from "../../Constants/colors";
import React from "react";
import {TextStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import TextView from "../../Catalyst/components/common/TextView";
import Images from "../../images/index.image";
import {handleBuyPlanBtnActions} from "../Helpers/BuyPlanButtonActions";

const VideoThumbnailComponent = props => {
    const image = props?.data?.image ? {uri: props?.data?.image} : null;
    const button=props?.data?.button || {};
    return (
        <TouchableOpacity activeOpacity={1} onPress={() => {
            handleBuyPlanBtnActions(button?.action, {...props?.extraProps,data: button.data ?? {}})
        }}>
            <View style={style.container}>
                <ImageBackground source={image} style={style.backgroundImage}>
                    <Image style={style.image} source={Images.play}/>
                    {props?.data?.paragraph &&
                    <TextView style={style.paragraph} texts={props?.data?.paragraph}></TextView>}
                </ImageBackground>

            </View>
        </TouchableOpacity>
    )
}

const style = StyleSheet.create({

    container: {
        height: 140,
        marginHorizontal: spacing.spacing16,
        marginTop: spacing.spacing24,
        borderRadius: 10,
        borderWidth: 1,
        overflow: 'hidden'

    },

    image: {
        width: 40,
        height: 40,
        marginTop: 40,
    },

    backgroundImage: {
        flex: 1,
        resizeMode: "cover",
        alignItems: 'center',

    },

    paragraph: {
        ...TextStyle.text_14_normal,
        color: colors.white,
        marginTop: spacing.spacing24,
        marginBottom: spacing.spacing20,
        marginHorizontal: spacing.spacing20,
    },

    divider: {
        height: 1,
        backgroundColor: colors.lightGrey
    }
});
export default VideoThumbnailComponent;

