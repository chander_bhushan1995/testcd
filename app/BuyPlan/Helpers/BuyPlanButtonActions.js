import { NativeModules } from "react-native";
import buyPlanJourney from "./BuyPlanJourneySingleton";
import WebViewComponent from "../../CardManagement/PayBill/WebViewComponent";
import {
    getParamsForPaymentFlow, onBackPress,
} from "./BuyPlanUtils";
import { store } from "../../CommonComponents/Index/index.common";
import {APIData} from '../../../index';
import {
    Category,
    Destination,
    FIREBASE_KEYS,
    FlowSequence,
    LoginForScreen,
    NumberVerifyAlerts, PlanServices, Services,
} from "../../Constants/AppConstants";
import { getFirebaseData } from "../../commonUtil/AppUtils";
import { logWebEnageEvent } from "../../commonUtil/WebengageTrackingUtils";
import * as WebEngageEventsName from "../../Constants/WebengageEvents";
import * as WebEngageKeys from "../../Constants/WebengageAttrKeys";

import {
    PAGE_PLAN_Code,
    PAGE_PLAN_NAME,
    PLAN_CODE,
    PLAN_NAME,
    PREMIUM_AMOUNT,
} from "../../Constants/WebengageAttrKeys";
import { data } from "react-native-chart-kit/data";
import {DISCOUNT} from "../../Constants/WebengageAttrKeys";
import {SOD_FLOW} from "../BuyPlanConstants";


const nativeBridge = NativeModules.ChatBridge;

export const ButtonsAction = {
    SCROLL_TO_INDEX: "SCROLL_TO_INDEX",
    OPEN_SERVICE_DETAIL: "OPEN_SERVICE_DETAIL",
    SHOW_SECTION_BOTTOM_SHEET: "SHOW_SECTION_BOTTOM_SHEET",
    FIND_A_PLAN_SCREEN: "FIND_A_PLAN_SCREEN",
    OPEN_PINCODE_SCREEN: "OPEN_PINCODE_SCREEN",
    SHOW_TESTIMONIAL_BOTTOM_SHEET: "SHOW_TESTIMONIAL_BOTTOM_SHEET",
    SHOW_TESTIMONIAL_READ_MORE_BOTTOM_SHEET: "SHOW_TESTIMONIAL_READ_MORE_BOTTOM_SHEET",
    SHOW_GEDGETS_BOTTOM_SHEET: "SHOW_GEDGETS_BOTTOM_SHEET",
    SHOW_SERVICE_BOTTOM_SHEET: "SHOW_SERVICE_BOTTOM_SHEET",
    CHECK_ANSWER: "CHECK_ANSWER",
    OPEN_RISK_CALCULATER: "OPEN_RISK_CALCULATER",
    OPEN_WEBVIEW: "OPEN_WEBVIEW",
    SHARE_APP: "SHARE_APP",
    OPEN_SOD_PRODUCT_SERVICE_SCREEN: "OPEN_SOD_PRODUCT_SERVICE_SCREEN",
    SEE_LABOUR_COST: "SEE_LABOUR_COST",
    SHOW_SOD_PLAN_EXCLUDED_BENEFITS: "SHOW_SOD_PLAN_EXCLUDED_BENEFITS",
    KNOW_MORE: "KNOW_MORE",
    SHOW_MORE_SOD_PRODUCTS: "SHOW_MORE_SOD_PRODUCTS",
    START_SOD: "START_SOD",
    WATCH_VIDEO: "WATCH_VIDEO",
};


const startSODFlow = (params) => {
    let routeName = "PinCodeScreen";
    getFirebaseData(FIREBASE_KEYS.MODULE_FLOW_SEQUENCING, (data) => {
        if (data && data?.sodFlowSequencing.screen_1 === FlowSequence.SODFlow.PRODUCT_SCREEN) {
            routeName = "SODPlanDetailScreen";
        } else {
            routeName = "PinCodeScreen";
        }
        let navParamsToPass = {
            services: params?.data?.services ?? [],
            productCode: params?.data?.productCode,
            destination: params?.data?.destination ?? Destination.sod,
            productName: params?.data?.productName,
            category: params?.data?.category,
            serviceRequestType: params?.data?.serviceRequestType,
            eventLocation: params?.data?.eventLocation,
        };

        if (params?.navigation) {
            params?.navigation?.navigate(routeName, navParamsToPass);
        } else {
            nativeBridge.openBuyPlanJourney({ routeName: routeName, "data": navParamsToPass });
        }
    });
};

const buyIDFencePlan = (params) => {
    let plan = params?.reducerState?.idFenceTrailPlan;
    let premiumPlan = params?.reducerState?.idFencePremiumPlan;
    if (params?.reducerState?.isIDFenceForRenewal) {
        params?.renewIDFenceMethod();
    } else {
        nativeBridge.showPopupForVerifyNumber(NumberVerifyAlerts.buyPlan.message, "", LoginForScreen.SALES, status => {
            if (params?.reducerState?.idFenceTrailPlan && params?.reducerState?.idFencePremiumPlan) { // trail purchase
                nativeBridge.openPaymentModule(getParamsForPaymentFlow(Destination.idfance, plan, APIData, {
                    siPlanCode: premiumPlan.planCode,
                    paymentOptions: plan?.paymentOps,
                    productName:buyPlanJourney?.product?.name,
                    productCode: "WP01",
                }));
            } else { // premium purchase
                nativeBridge.openPaymentModule(getParamsForPaymentFlow(Destination.idfance, premiumPlan, APIData, { productCode: "WP01",
                    productName:buyPlanJourney?.product?.name,}));
            }
        });
    }
};

const findAPlanJourney = (params) => {
    let destination = Destination.getDestination(params?.buyPlanJourney.category, params?.buyPlanJourney.service);
    let route = Destination.getRoutesForDestination(destination).first();

    if (params?.buyPlanJourney.category === Category.finance && params?.buyPlanJourney.service === PlanServices.idFence) {
        buyIDFencePlan(params);
        return;
    }

    if (params?.reducerState?.offersData) {
        params.buyPlanJourney.offer = params?.reducerState?.offersData;
    }
    if (params?.navigation) {
        params?.navigation?.navigate(route, { buyPlanJourney: params.buyPlanJourney, destination: destination });
    } else {
        nativeBridge.openBuyPlanJourney({
            "routeName": route,
            "data": { buyPlanJourney: params.buyPlanJourney, destination: destination },
        });
    }
};


/*
* below method handles all button or card actions of components
* Parameter - buttonAction - consists unique action type
* Parameter - params - consists data to complete and action most of the time it contains data, extraProps or eventsData keys
* here extra props key represents extra params passed through out the main screen to component and then component to this file.
* */

let eventData = null;
let eventName = null;

export const handleBuyPlanBtnActions = async (buttonAction, params) => {

    eventData = { ...(params?.data?.eventData ?? {}) };
    eventData[WebEngageKeys.LOCATION] = params.location;
    eventData[WebEngageKeys.CATEGORY] = params?.category;
    eventData[WebEngageKeys.VARIATIONID] = params?.eventPageVariationId;
    eventData[WebEngageKeys.PRODUCT] = params?.product;
    eventData[WebEngageKeys.SERVICE] = params?.service;
    eventName = params?.data?.eventName ?? null;

    if (store.getState().nav.routes.last()?.routeName == "BottomSheetScreen") {
        onBackPress(params.navigation);
    }

    switch (buttonAction) {

        case ButtonsAction.WATCH_VIDEO:
            if (!eventName) {
                eventName = WebEngageEventsName.WATCH_VIDEO;
            }

            if (params?.navigation) {
                params?.navigation?.navigate("VideoViewComponent",
                    {
                        videoURL: params?.data?.url,
                        headerTitle: params?.data?.headerTitle,
                    });
            } else {
                nativeBridge.openBuyPlanJourney({
                    "routeName": "VideoViewComponent",
                    "isPresent": true,
                    "data": {
                        videoURL: params?.data?.url,
                        headerTitle: params?.data?.headerTitle,
                    },
                });
            }
            break;
        case ButtonsAction.SCROLL_TO_INDEX:
            if (params?.scrollToIndex) {
                params?.scrollToIndex(params?.data?.index);
            }
            eventData[WebEngageKeys.CATEGORY] = params.category;
            eventName = WebEngageEventsName.AUTO_SCROLL_SERVICES;
            break;
        case ButtonsAction.OPEN_SERVICE_DETAIL:
            if (!params?.data?.buyPlanJourney) {
                let object = { ...buyPlanJourney };
                object.category = params?.data?.category;
                object.service = params?.data?.service;
                object.serviceName = params?.data?.serviceName;
                object.screenTitle = params?.data?.screenTitle;
                params.data.buyPlanJourney = object;
            }
            eventData[WebEngageKeys.CATEGORY] = params.data?.buyPlanJourney?.category;
            eventData[WebEngageKeys.SERVICE] = params.data?.buyPlanJourney?.service;
            if ([PlanServices.idFence, PlanServices.wallet].includes(params?.data?.buyPlanJourney?.service) || params?.data?.buyPlanJourney?.category == Category.personalElectronics) { // when user selected product with service from tabpage
                if (!params?.data?.buyPlanJourney?.product?.name) {
                    let productService = "wallet";
                    params.data.buyPlanJourney = { ...params.data.buyPlanJourney, product: { name: productService } };
                }
                eventData[WebEngageKeys.PRODUCT] = params?.data?.buyPlanJourney?.product?.name;
                eventName = WebEngageEventsName.SELECT_PRODUCT_SERVICE;
            } else {
                eventName = WebEngageEventsName.SELECT_SERVICE;
            }

            if (params?.navigation) {
                params?.navigation?.navigate("ServiceDetailScreen", { buyPlanJourney: params.data.buyPlanJourney });
            } else {
                nativeBridge.openBuyPlanJourney({
                    "routeName": "ServiceDetailScreen",
                    "data": { buyPlanJourney: params.data.buyPlanJourney },
                });
            }

            break;

        case ButtonsAction.SHOW_MORE_SOD_PRODUCTS:
        case ButtonsAction.SHOW_SECTION_BOTTOM_SHEET:
        case ButtonsAction.SHOW_TESTIMONIAL_BOTTOM_SHEET:
        case ButtonsAction.SHOW_TESTIMONIAL_READ_MORE_BOTTOM_SHEET:
        case ButtonsAction.SHOW_GEDGETS_BOTTOM_SHEET:
        case ButtonsAction.SHOW_SERVICE_BOTTOM_SHEET:

            if (buttonAction === ButtonsAction.SHOW_TESTIMONIAL_BOTTOM_SHEET) {
                eventName = WebEngageEventsName.VIEW_MORE_REVIEWS_EVENT;
            } else if (buttonAction === ButtonsAction.SHOW_TESTIMONIAL_READ_MORE_BOTTOM_SHEET) {
                eventName = null;
            } else if (buttonAction === ButtonsAction.SHOW_SECTION_BOTTOM_SHEET) {
                eventName = WebEngageEventsName.HOW_CLAIM_WORKS;
            } else if (buttonAction === ButtonsAction.SHOW_GEDGETS_BOTTOM_SHEET) {
                eventData[WebEngageKeys.SERVICE] = params?.data?.service;
                eventName = WebEngageEventsName.SELECT_SERVICE;
            } else if (buttonAction === ButtonsAction.SHOW_SERVICE_BOTTOM_SHEET) {
                eventData[WebEngageKeys.PRODUCT] = params?.data?.name;
                eventName = WebEngageEventsName.SELECT_PRODUCT;
            }
            params.data = { ...params.data, eventsData: eventData,navigation: params.navigation };

            if (params?.navigation) {
                params?.navigation?.navigate("BottomSheetScreen", {
                    bottomSheetData: params?.data,
                    bottomSheetType: buttonAction,
                    servicePageReducer: params?.reducerState,
                    buyPlanJourney: params?.buyPlanJourney
                });
            } else {
                nativeBridge.openBuyPlanJourney({
                    "routeName": "BottomSheetScreen",
                    "isPresent": true,
                    "data": {
                        bottomSheetData: params?.data,
                        bottomSheetType: buttonAction,
                        servicePageReducer: params?.reducerState,
                        buyPlanJourney: params?.buyPlanJourney
                    },
                });
            }

            break;

        case ButtonsAction.FIND_A_PLAN_SCREEN:
            //events are specific to idfence just because to give support of existing events for idfence

            if (params?.buyPlanJourney?.category === Category.finance && params?.buyPlanJourney?.service === PlanServices.idFence) {
                let idFenceEventData = {};
                let plan = params?.reducerState?.idFenceTrailPlan ?? params?.reducerState?.idFencePremiumPlan;
                idFenceEventData[PAGE_PLAN_Code] = plan?.planCode;
                idFenceEventData[PAGE_PLAN_NAME] = plan?.planName;
                idFenceEventData[WebEngageKeys.ALREADY_HAS_PLAN] = params?.reducerState?.alreadyHasPlan;
                logWebEnageEvent(WebEngageEventsName.PLAN_DETAIL_PRIMARY_CTA, idFenceEventData);
                logWebEnageEvent(WebEngageEventsName.FIND_A_PLAN, eventData);
                eventData[PLAN_CODE] = plan?.planCode;
                eventData[PLAN_NAME] = plan?.planName;
                eventData[PREMIUM_AMOUNT] = plan?.price;
                logWebEnageEvent(WebEngageEventsName.SELECT_PLAN, eventData);
            } else {
                eventData[DISCOUNT] = params?.reducerState?.offersData?.isShowAlert ? params?.reducerState?.offersData?.couponCode : "No";
                eventName = WebEngageEventsName.FIND_A_PLAN;
            }
            findAPlanJourney(params);
            break;
        case ButtonsAction.OPEN_PINCODE_SCREEN:
            if (params?.navigation) {
                params?.navigation?.navigate("PinCodeScreen", params?.data);
            } else {
                nativeBridge.openBuyPlanJourney({ "routeName": "PinCodeScreen", "data": params?.data });
            }
            break;
        case ButtonsAction.OPEN_RISK_CALCULATER:
            if (params?.routeToRiskCalculator) {
                params?.routeToRiskCalculator();
            }
            break;
        case ButtonsAction.OPEN_WEBVIEW:
            if (eventName && eventName === WebEngageKeys.WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_FENCE_BLOG_CARD_CLICK) {
                eventData[WebEngageKeys.WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.BP] = parseInt(APIData.partnerCode);
                eventData[WebEngageKeys.WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.BU] = parseInt(APIData.partnerBUCode);
                eventData[WebEngageKeys.BLOG_TITLE] = params?.data?.title;
                eventData[WebEngageKeys.LOCATION] = (params?.reducerState?.isIDFenceTrialAvailable || params?.reducerState?.isIDFencePremiumAvailable) ? "IDFence_Premium_Screen" : "IDFence_Trial_screen";
                eventData[WebEngageKeys.PLAN_CODE] = params?.reducerState?.idFenceTrailPlan?.planCode ?? params?.reducerState?.idFencePremiumPlan?.planCode;
                eventData[WebEngageKeys.PLAN_NAME] = params?.reducerState?.idFenceTrailPlan?.planName ?? params?.reducerState?.idFencePremiumPlan?.planName;
                eventData[WebEngageKeys.ALREADY_HAS_PLAN] = params?.reducerState?.alreadyHasPlan;
            }
            if (params?.data?.url && params.navigation) {
                params?.navigation?.navigate("WebViewComponent", {
                    url: params?.data?.url,
                    headerTitle: params?.data?.title,
                });
            }
            break;
        case ButtonsAction.SHARE_APP:
            nativeBridge.shareLink(params?.textToShare);
            break;
        case ButtonsAction.OPEN_SOD_PRODUCT_SERVICE_SCREEN:
            if (params?.data?.navigation) {
                params?.data?.navigation?.navigate("SODProductServiceScreen", ...params);
            } else {
                nativeBridge.openBuyPlanJourney({
                    "routeName": "SODProductServiceScreen", data: { ...params },
                });
            }
            break;
        case ButtonsAction.SEE_LABOUR_COST:
            if (params?.onSeeLabourCost) {
                params?.onSeeLabourCost();
            }
            break;
        case ButtonsAction.SHOW_SOD_PLAN_EXCLUDED_BENEFITS:
            if (params?.showNotCoveredBenefits) {
                params?.showNotCoveredBenefits();
            }
            break;
        case ButtonsAction.START_SOD:
            startSODFlow(params);
            break;
        case ButtonsAction.KNOW_MORE:
            let object = { ...buyPlanJourney };
            object.category = params?.data?.category;
            object.service = params?.data?.service;
            object.serviceName = params?.data?.serviceName;
            object.screenTitle = params?.data?.screenTitle;
            params.data.buyPlanJourney = object;
            if (params?.data?.navigation) {
                params?.data?.navigation?.navigate("KnowMoreScreen", { buyPlanJourney: params.data.buyPlanJourney });
            } else {
                nativeBridge.openBuyPlanJourney({
                    "routeName": "KnowMoreScreen",
                    "data": { buyPlanJourney: params.data.buyPlanJourney },
                });
            }
            break;
        default:
            break;
    }

    if (eventName) {
        logWebEnageEvent(eventName, eventData);
        eventData = null;
        eventName = null;
    }
};

