import * as ActionTypes from "../../Constants/ActionTypes";
import {getIDFenceMemberships, MembershipStatus} from "../../commonUtil/MembershipsUtilMethods";
import {
    AVAILABLE_SLOTS,
    Category,
    LABOUR_COST_DESCRIPTION,
    LABOUR_COST_TITLE,
    NEED_SERVICE_EXPERT,
    PAY_AFTER_SERVICE,
    PlanServices,
    PLATFORM_OS,
    PROCEED_TO_BOOK,
    Services,
} from "../../Constants/AppConstants";
import {ButtonsAction} from "./BuyPlanButtonActions";
import colors from "../../Constants/colors";
import {getTodayDate} from "../../commonUtil/DateUtils";
import {updateObjects} from "../../AppForAll/HomeScreen/Components/ComponentsFactory";
import {isFormComplete, setFieldError} from "../../commonUtil/Validator";
import {Platform} from "react-native";
import {coverAmountFormat, formatDateDD_MMM_YYYY, numberWithComma} from "../../commonUtil/Formatter";
import {FormField} from "../../Catalyst/components/ComponentsFactory";
import {checkAlreadyHasPlanInMembership, getServiceNameForClaims} from "./BuyPlanUtils";
import {AppStringConstants} from "../../Constants/AppStringConstants";
import { parseJSON } from "../../commonUtil/AppUtils";


export const initialState = {
    screenComponents: [], // components array would be stored here
    bottomSheetData: null,
    offersData: null,
    testimonialData: [],

    activeMemberships: null,
    pendingMemberships: null,

    isInitiallyMemSet: false,
    isIDFenceTrialAvailable: false,
    isIDFencePremiumAvailable: false,
    isIDFenceForRenewal: false,
    idFenceTrialMembership: null,
    idFencePremiumPlan: null,
    idFenceTrailPlan: null,
    idFenceBuyPlanText: null,
    idFenceBuyPlanNoteText: null,
    floatingActionData: null,

    plansCardViewModel: [], // it will store all plans card view models
    planCardsToRender: [], // it will contain only those plan cards view which needed to be render base on cover amount or other filter
    coverAmountsViewModel: null,//{selectedCoverAmount: 0,bottomSheetVM: {heading: "",description: "",coverAmountCellVM: [{ title: "", subTitle: "", note: "", description: "", isPopular: false }]}},

    isLoadingData: false,
    SODServicesList: [],
    isSODPagePrepared: false,
    screenHeader: null,
    sodAppliancesOffers: [],
    sodPlansData: [],
    sodAvailableTimeSlots: null,
    sodProductIssues: [],
    labourCostResponse: [],
    productSpecsResponse: [],
    labourCostVM: null,
    planExcludedBenefitsVM: {heading: null, listOfBenefits: []},
    sodIssuesVM: null,
    footerVM: null,
    sodSelectedPlansAndQuantity: null, // it will help to create final payment request //{assetTechnology,selectedProductCode,overAllQuantity,overallTotalPrice,[{planCode,totalQuantity,totalPrice,singleQuantityPrice,[issues], maxEligibalQuantity}]}
    isFormComplete: false,
    eventPageVariationId: null
};


const getFilteredOfferData = (data) => {
    let offerData = data?.offerResponse;
    let offerObj = null;
    let filteredData = offerData?.coupons?.filter((couponObj) => {
        if (couponObj.category == data?.buyPlanJourney?.category && (couponObj?.services ?? []).includes(data?.buyPlanJourney?.service)) {
            if (data?.buyPlanJourney?.category == Category.personalElectronics) {
                return (couponObj?.products ?? []).includes(data?.buyPlanJourney?.product?.code);
            }
            return true;
        }
        return false;
    }) ?? [];
    if (filteredData.length > 0) {
        offerObj = filteredData[0];
    }
    return offerObj;
};

const addTestimonialData = (screenComponentData, testimonialData) => {
    const updatedComponents = addDataToComponent(screenComponentData,
        "TestimonialListContainer",
        testimonialData);
    return updatedComponents;
};

function addDataToComponent(screenComponents, valueToFind, data) {
    Object.keys(screenComponents).forEach(key => {
        const keyValue = screenComponents[key];
        if (typeof keyValue === "object") {
            if (keyValue.type === valueToFind) {
                if (data) {
                    keyValue.data = data;
                } else {
                    let searchedIndex = screenComponents.indexOf(keyValue);
                    screenComponents.splice(searchedIndex, 1);
                }
                return screenComponents;
            } else if (keyValue.components) {
                addDataToComponent(keyValue.components, valueToFind, data);
            }
        }
    });
    return screenComponents;
}

const getIDFenceTrailMembership = (memberships) => {
    let obj = {
        isIDFenceTrial: false,
        isIDFencePremium: false,
        isIDFenceForRenewal: false,
        idFenceTrialMembership: null,
    };
    let idFenceMems = getIDFenceMemberships(memberships?.activeMemberships) ?? [];
    let idFencePendingMems = getIDFenceMemberships(memberships?.pendingMemberships) ?? [];
    if (idFenceMems.length <= 0) {
        idFencePendingMems.every((membership) => {
            if (membership?.trial?.toUpperCase() === "Y" || membership?.trial?.toUpperCase() === "N") {
                if (membership?.trial?.toUpperCase() === "N") {
                    obj.isIDFencePremium = true;
                } else {
                    obj.isIDFenceTrial = true;
                    obj.idFenceTrialMembership = membership;
                }
                return false;
            }
            return true;
        });
    } else {
        idFenceMems.every((membership) => {
            if (membership?.trial?.toUpperCase() === "Y" || membership?.trial?.toUpperCase() === "N") {
                if (membership?.trial?.toUpperCase() === "N") {
                    obj.isIDFencePremium = true;
                } else {
                    obj.isIDFenceTrial = true;
                    obj.idFenceTrialMembership = membership;
                }
                return false;
            }
            return true;
        });
    }

    obj.isIDFenceForRenewal = obj.idFenceTrialMembership && !(obj.idFenceTrialMembership?.isSIEnabled ?? false) && (obj.idFenceTrialMembership?.membershipStatus != MembershipStatus.cancelled);
    return obj;
};

const prepareBtnAndNoteForIDFence = (state, trialPlan, premiumPlan) => {
    let obj = {btnText: null, noteText: null};
    let planCode = state?.idFenceTrailPlan ? state?.idFenceTrailPlan?.planCode : state?.idFencePremiumPlan?.planCode;
    obj.alreadyHasPlan = checkAlreadyHasPlanInMembership(state?.pendingMemberships, state?.activeMemberships, planCode);
    let floatActionCTAText = "Start Free";
    let floatingActionText = "<b>Safeguard your online presence with ID Fence</b>"
    if (!state?.isIDFenceTrialAvailable && !state?.isIDFencePremiumAvailable && trialPlan) {
        obj.btnText = `Get free for ${trialPlan?.planDuration}`;
        obj.noteText = `(₹${premiumPlan?.price}/month after ${trialPlan?.planDuration} days, with select cards)`;
    } else {
        if (state.isIDFenceForRenewal) {
            obj.btnText = `Upgrade to Premium at ₹${premiumPlan?.price}/month `;
            let amountSaved = premiumPlan?.anchorPrice - premiumPlan?.price;
            if (amountSaved > 0) {
                obj.noteText = `Save ₹${amountSaved} per month`;
                obj.btnText += ` <strike>₹${premiumPlan?.anchorPrice}</strike>`;
            }
            floatActionCTAText = "Upgrade";
            floatingActionText = `<b>Upgrade to ID Fence at ₹${premiumPlan?.price}/month</b>`
        } else {
            obj.btnText = `Buy at ₹${premiumPlan?.price}/month `;
            floatActionCTAText = "Buy Now";
            floatingActionText = "<b>Safeguard your online presence with ID Fence</b>"
        }
    }
    obj.idFenceFloatActionData = {
        "description": {
            "value": floatingActionText,
            "isHTML": true,
        },
        "primaryButton": {
            "text": floatActionCTAText,
            "action": "FIND_A_PLAN_SCREEN",
        },
    };
    return obj;
};


const prepareSODProductComponents = (state, cdnComponents = []) => {
    let finalComponents = [];
    let header = {};
    cdnComponents.forEach((component) => {
        if (component?.type === "SODServiceListComponent") {
            component = {
                "type": component.type,
                "title": NEED_SERVICE_EXPERT,
                "components": state.SODServicesList.map((service) => {
                    header.title = service?.productCode + " Service & Repair";
                    let description = service?.description;
                    let descriptionColor = colors.color_888F97;
                    let productOffer = state.sodAppliancesOffers.filter((offer) => {
                        return offer?.productCode === service?.productCode;
                    })[0];
                    if (productOffer) {
                        let serviceOffer = productOffer?.offers?.filter((serviceOffer) => {
                            return serviceOffer?.service === service?.serviceCode;
                        })[0];
                        if (serviceOffer && serviceOffer?.detailedOfferText) {
                            description = serviceOffer?.detailedOfferText;
                            descriptionColor = colors.seaGreen;
                        }
                    }

                    return {
                        "title": service?.name,
                        "description": description,
                        "descriptionColor": descriptionColor,
                        "button": {
                            "text": component?.serviceSelectionCTAText ?? "Book Now",
                            "action": ButtonsAction.OPEN_SOD_PRODUCT_SERVICE_SCREEN,
                            "data": service,
                        },
                    };
                }),
            };
            header.data = component;
        }
        finalComponents.push(component);
    });
    return {headerComponent: header, finalComponents: finalComponents};
};


const prepareSODProductServiceScreen = (state, selectedServiceRequestType, productCode, productName) => {
    let components = state?.screenComponents ?? [];
    let title = "";
    let description = "";
    let image = "";
    let gradientColors = [];
    let header = {};
    let labourCostVM = null;
    let issuesListVM = null;
    let planIncludedBenefits = new Set();
    let planNotCoveredBenefits = new Set();
    let sodSelectedPlansAndQuantity = {
        selectedProductCode: productCode,
        overAllQuantity: 0,
        overAllTotalPrice: 0,
        plans: [],
    };

    let footerVM = {
        primaryButton: {isEnabled: false, text: PROCEED_TO_BOOK, action: ButtonsAction.BOOK_SOD},
        description: PAY_AFTER_SERVICE,
    };
    header.title = productName + (selectedServiceRequestType === Services.breakdown ? " Repair" : " Service");

    issuesListVM = state?.sodProductIssues?.map((issue) => {
        return {id: issue.serviceTaskId, text: issue.taskName};
    });

    // prepare vm for labour cost
    if (state.productSpecsResponse && Array.isArray(state.productSpecsResponse) && Array.isArray(state.labourCostResponse)) {
        let cells = [];
        state.labourCostResponse.forEach((labourCost, index) => {
            if (labourCost?.productVariantId === state.productSpecsResponse[index].id && labourCost?.cost > 0) {
                let specsData = state.productSpecsResponse[index];
                cells.push({
                    leftEndText: (specsData?.productSize ?? "") + (specsData?.productUnit ?? ""),
                    rightEndText: "₹ " + labourCost?.cost,
                });
            }
        });
        if (cells.length > 0) {
            cells.splice(0, 0, {leftEndText: productName, rightEndText: "Labour Cost"});
        }
        labourCostVM = {heading: LABOUR_COST_TITLE, description: LABOUR_COST_DESCRIPTION, cells: cells};
    }

    // product quantity selection cells vm
    let productdQuantityCells = state.sodPlansData.map((plan) => {
        planIncludedBenefits = new Set(plan?.benefits?.map((benefitObj) => {
            return benefitObj?.benefit;
        }) ?? []);
        planNotCoveredBenefits = new Set(plan?.excludedBenefits?.map((benefitObj) => {
            return benefitObj?.benefit;
        }) ?? []);
        return {
            planCode: plan?.planCode,
            planName: plan?.planName,
            title: plan?.planName,
            price: plan?.price,
            description: "",
            currentValue: 0,
            maxValue: plan?.allowedUnits,
            subNote: (plan?.anchorPrice && plan?.anchorPrice != plan?.price) ? "₹" + plan?.anchorPrice : null,
            note: "₹" + plan?.price,
        };
    }) ?? [];

    // modify cdn components as per requirement
    components = components.map((component) => {
        if (component.type === "TitleWithOfferView") {
            let couponCode = null;
            let productOffer = state.sodAppliancesOffers.filter((offer) => {
                return offer?.productCode === productCode;
            })[0];

            if (productOffer) {
                let serviceOffer = productOffer?.offers?.filter((serviceOffer) => {
                    return serviceOffer?.service === selectedServiceRequestType;
                })[0];
                title = (selectedServiceRequestType === Services.breakdown ? productOffer?.habdHeaderText : productOffer?.pmsHeaderText).replace(/%@/g, productName);
                if (serviceOffer && serviceOffer?.offerText) {
                    description = serviceOffer?.offerText;
                    image = serviceOffer?.imageURL;
                    gradientColors = serviceOffer?.backgroundColors;
                    couponCode = serviceOffer?.couponCode;
                    // descriptionColor = colors.seaGreen;
                }
            }

            component = {
                type: component.type,
                title: title,
                description: description,
                image: {uri: image},
                gradientColors: gradientColors,
                couponCode: couponCode,
            };
        } else if (component.type === "ProductQuantitySelectionComponent") {
            if (Array.isArray(state.sodPlansData)) {
                component.components = productdQuantityCells;
            }
        } else if (component.type === "TimeSlotsComponent") {
            if (state?.sodAvailableTimeSlots?.serviceRequestDate === getTodayDate()) {
                component.description = AVAILABLE_SLOTS((state?.sodAvailableTimeSlots?.serviceSlots ?? []).length);
            } else {
                component.type = "View";
            }
        } else if (component.type === "PleaseNoteComponent") {
            if ((labourCostVM?.cells?.length ?? 0) <= 0) {
                component.notes = component?.notes?.map((note) => {
                    note.button = null;
                    return note;
                });
            }
        } else if (component.type === "SODPlanBenefits" && planIncludedBenefits.size > 0) {
            let planBenefitsComponents = [];
            planIncludedBenefits?.forEach((value) => {
                planBenefitsComponents.push({
                    "type": "LeftImageTextComponent",
                    "image": "https://ws.oneassist.in/static/oaapp/buytab/card_benefits_icon.png",
                    "paragraph": {
                        "value": value,
                        "isHTML": false,
                    },
                });
            });
            component = {
                "type": "FullWidthContainer",
                "bgColor": "#F1F2F3",
                "showShadow": false,
                "header": {
                    "fontSize": 16,
                    "value": selectedServiceRequestType === Services.breakdown ? "<b>Repair includes</b>" : "<b>Servicing includes</b>",
                    "isHTML": true,
                },
                "components": planBenefitsComponents,
            };
        }
        return component;
    });
    return {
        components: components,
        labourCostVM: labourCostVM,
        screenFooterVM: {},
        headerComponent: header,
        notCoveredBenefitsVM: {heading: "What is not covered", listOfBenefits: Array.from(planNotCoveredBenefits)},
        footerVM: footerVM,
        sodSelectedPlansAndQuantity: sodSelectedPlansAndQuantity,
        issuesListVM: issuesListVM,
    };
};

const updateSODPlanQuantityPrice = (state, data) => {
    let selectedPlansQuantities = {...state.sodSelectedPlansAndQuantity};
    let filteredData = selectedPlansQuantities?.plans?.filter((plan) => {
        return plan?.planCode === data?.planCode;
    }).first();
    let filteredDataIndex = selectedPlansQuantities?.plans?.indexOf(plan => plan.planCode === data?.planCode);
    if (data?.changeBy > 0) { // add  plan quantity
        selectedPlansQuantities.overAllTotalPrice += data?.singleUnitPrice;
        selectedPlansQuantities.overAllQuantity += 1;
        if (filteredData) {
            filteredData.totalQuantity += 1;
            filteredData.totalPrice += data?.singleUnitPrice;
            filteredData.issues?.push(data?.issues);
            filteredData.maxEligibalQuantity = data?.maxEligibility;
            selectedPlansQuantities.plans[filteredDataIndex] = filteredData;
            filteredData.singleQuantityPrice = data?.singleUnitPrice;
        } else {
            filteredData = {
                planCode: data?.planCode,
                planName: data?.planName,
                totalQuantity: 1,
                totalPrice: data?.singleUnitPrice,
                issues: data?.issues ? [data?.issues] : null,
                maxEligibalQuantity: data?.maxEligibility,
                singleQuantityPrice: data?.singleUnitPrice,
            };
            selectedPlansQuantities?.plans?.push(filteredData);
        }
    } else { // remove  plan quantity
        filteredData.totalQuantity -= 1;
        filteredData.totalPrice -= data?.singleUnitPrice;
        Array.isArray(filteredData?.issues) ? filteredData.issues?.pop() : null;
        selectedPlansQuantities.overAllTotalPrice -= data?.singleUnitPrice;
        selectedPlansQuantities.overAllQuantity -= 1;
        selectedPlansQuantities.plans[filteredDataIndex] = filteredData;
    }

    // update selected plan cell ui component
    let updatedProductQuantityComponent = state?.screenComponents?.filter(component => component.type === "ProductQuantitySelectionComponent").first().components.filter(cell => cell.planCode === data?.planCode).first();
    updatedProductQuantityComponent.currentValue = filteredData.totalQuantity;
    updatedProductQuantityComponent.description = filteredData.issues ? filteredData.issues.map((issueArray) => {
        return "1 Repair - " + issueArray.map(issueObj => issueObj.text).join(", ");
    }).join("\n") : null;

    // uppate footer button text
    let footerButtonText = filteredData.totalQuantity > 0 ? PROCEED_TO_BOOK + ` (₹${filteredData.totalPrice})` : PROCEED_TO_BOOK;
    let updatedFooterVM = {
        primaryButton: {
            isEnabled: filteredData.totalQuantity > 0,
            text: footerButtonText,
            action: ButtonsAction.BOOK_SOD,
        },
        description: PAY_AFTER_SERVICE,
    };

    let finalComponents = updateObjects(state?.screenComponents, "planCode", data?.planCode, updatedProductQuantityComponent);
    return {updatePlanQuantities: selectedPlansQuantities, components: finalComponents, footerVM: updatedFooterVM};
};

const prepareBrandsData = (data, productCode) => {
    let productBrands = data?.assetMakeDtl[productCode];
    let brandsUiData = {};
    let allBrandsData = [];
    let filteredData = [];
    let popularBrands = [];
    let additionalAttr = [];

    if (productBrands) {
        for (let brandItem of productBrands) {
            allBrandsData.push(brandItem);
            if (brandItem.additionalAttributes) {
                let additionalAttribute = {...brandItem.additionalAttributes};
                additionalAttribute.brandName = brandItem.paramValue;
                additionalAttribute.brandData = brandItem;
                additionalAttr.push(additionalAttribute);
            }
        }

        if (allBrandsData && allBrandsData.length > 0) {
            allBrandsData.sort((ob1, ob2) => {
                if (ob1.paramValue < ob2.paramValue) {
                    return -1;
                }
                if (ob1.paramValue < ob2.paramValue) {
                    return 1;
                }
                return 0;
            });
            filteredData = [...allBrandsData];
            if (additionalAttr.length > 3) {
                additionalAttr.sort((ob1, ob2) => {
                    if (ob1.priority < ob2.priority) {
                        return -1;
                    }
                    if (ob1.priority < ob2.priority) {
                        return 1;
                    }
                    return 0;
                });
            }
            popularBrands = additionalAttr.slice(0, 4);
            for (let brand of popularBrands) {
                filteredData = filteredData.filter(item => {
                    return item.paramValue !== brand.brandName;
                });
            }
        }

    }
    brandsUiData.allData = allBrandsData;
    brandsUiData.filteredBrands = filteredData;
    brandsUiData.popularBrands = popularBrands;
    brandsUiData.searchedKey = "";
    return brandsUiData;
};

const filterBrands = (key, brandsUiData) => {
    if (brandsUiData && brandsUiData.allData) {
        if (key) {
            let filteredData = brandsUiData.allData.filter((item) => {
                return item.paramValue.toLowerCase().includes(key.toLowerCase());
            });

            filteredData.sort((ob1, ob2) => {
                if (ob1.paramValue < ob2.paramValue) {
                    return -1;
                }
                if (ob1.paramValue < ob2.paramValue) {
                    return 1;
                }
                return 0;
            });
            brandsUiData.filteredBrands = filteredData;
            brandsUiData.searchedKey = key;
        } else {
            let filteredData = [...brandsUiData.allData];
            if (brandsUiData.popularBrands) {
                for (let brand of brandsUiData.popularBrands) {
                    filteredData = filteredData.filter(item => {
                        return item.paramValue !== brand.brandName;
                    });
                }
            }
            brandsUiData.filteredBrands = filteredData;
            brandsUiData.searchedKey = "";
        }
    }
    return brandsUiData;
};

const getNoOfClaimsOrAppliancesVM = (category, service, plan) => {
    let noOfClaimsView = {};
    if (category == Category.personalElectronics || (category == Category.homeAppliances && [PlanServices.extendedWarranty, PlanServices.whcInspection].includes(service))) {
        let services = plan?.services ?? [];
        let claimsViewText = null;
        let tooltipText = null;
        let claimsPerServiceTexts = [];
        let overAllClaims = 0;

        services.map((service) => {
            let policy = (service?.policy ?? []).first();
            if (policy) {
                if (policy?.noOfClaims > 0) {
                    if (overAllClaims != -1) {
                        overAllClaims += policy?.noOfClaims;
                    }
                    claimsPerServiceTexts.push({
                        count: policy?.noOfClaims,
                        text: ` ${policy?.noOfClaims} Claim${policy?.noOfClaims > 1 ? "s" : ""} for ${getServiceNameForClaims(service?.serviceName)}`,
                    });
                } else if (policy?.noOfClaims == -1) {
                    overAllClaims = -1;
                    claimsPerServiceTexts.push({
                        count: policy?.noOfClaims,
                        text: ` Unlimited Claims for ${getServiceNameForClaims(service?.serviceName)}`,
                    });
                }
            }
        });
        if (overAllClaims != 0) {
            claimsViewText = overAllClaims == -1 ? "(Unlimited Claims)" : (overAllClaims == 1 ? "(1 Claim)" : `(Get Upto ${overAllClaims} claims)`);
            if (claimsPerServiceTexts.length > 0) {
                let firstCountText = claimsPerServiceTexts.first();
                tooltipText = firstCountText.count > 1 ? "Get Upto " : "";
                if (claimsPerServiceTexts.length === 1) {
                    tooltipText += firstCountText?.text;
                } else {
                    let lastServiceText = claimsPerServiceTexts.pop();
                    tooltipText += claimsPerServiceTexts.map(obj => obj.text).join(",") + ` and ` + lastServiceText.text;
                }
            }
            noOfClaimsView.claimsViewText = claimsViewText;
            noOfClaimsView.tooltipText = tooltipText;
        } else {
            noOfClaimsView = null;
        }
    } else if (category == Category.homeAppliances && service == Services.sop) {
        if (plan?.allowedMaxQuantity > 0) {
            noOfClaimsView.claimsViewText = plan?.allowedMaxQuantity === 1 ? `For 1 Appliance` : `For ${plan?.allowedMaxQuantity >= 500 ? "Unlimited" : plan?.allowedMaxQuantity} Appliances`;
        } else {
            noOfClaimsView = null;
        }
    }

    return noOfClaimsView;
};

const preparePlansListingUI = (actionData) => {
    let plansArray = actionData?.plans ?? [];
    let taglinesData = actionData?.plansTaglineResponse ?? {};
    let category = actionData?.category;
    let service = actionData?.service;
    let invoiceRange = actionData?.invoiceRange;
    let invoiceValue = actionData?.invoiceValue;
    let coverAmountsViewModel = null;
    let savedAmt = null;
    let plansViewModel = plansArray.map((plan) => {
        let priceText = plan?.price ?? 0;
        let offerText = null;
        let taglineText = null;
        let productsSet = new Set();
        let servicesSet = new Set();
        plan?.productServices?.map((productService) => {
            productsSet.add(productService?.productCode);
            productService?.serviceList?.map((service) => {
                servicesSet.add(service);
            });
        });
        let eligibleTaglinesArray = (taglinesData?.taglines ?? []).filter((taglineObj) => { // check if mandatory list is subset of services
            let set = new Set(taglineObj.mandatory_servicelist ?? []);
            return set.subSet(servicesSet);
        }) ?? [];
        if (eligibleTaglinesArray?.first()?.tagline && taglinesData?.taglines?.length == 1) {
            taglineText = eligibleTaglinesArray?.first()?.tagline;
        } else {
            eligibleTaglinesArray = eligibleTaglinesArray.filter((taglineObj) => {
                let set = new Set(taglineObj.productCodes ?? []);
                return set.subSet(productsSet);
            }) ?? [];
            taglineText = eligibleTaglinesArray?.first()?.tagline;
        }
        if (actionData?.offerData) {
            let savedAmount = Math.ceil((plan?.price / 100) * actionData?.offerData?.percentDiscount);
            priceText = Math.ceil(plan?.price - savedAmount);
            offerText = `(${actionData?.offerData?.percentDiscount}% OFF - Save ₹ ${savedAmount})`;
            savedAmt = savedAmount;
        }
        let planBenefits = plan?.benefits?.sort((benefit, benefit1) => {
            return benefit.rank - benefit1.rank;
        });

        if (invoiceValue || invoiceRange) {
            if (invoiceValue) {
                planBenefits.unshift({benefit: `Cover amount of upto ₹ ${invoiceValue}`});
            } else if (plan?.coverAmount) {
                planBenefits.unshift({benefit: `Cover amount of upto ₹ ${plan?.coverAmount}`});
            } else if (plan?.maxInsuranceValue) {
                planBenefits.unshift({benefit: `Cover amount of upto ₹ ${plan?.maxInsuranceValue}`});
            }
        }

        const benifitCount = ((actionData?.components) ?? []).filter(component=> component.type == "PlanListingComponent").first()?.benifitCount ?? 2

        return {
            title: plan?.planName,
            noOfClaimsView: getNoOfClaimsOrAppliancesVM(category, service, plan),
            price: `₹ ${numberWithComma(priceText)}`,
            saveAmount: savedAmt,
            payableAmount: priceText,
            anchorPrice: `₹ ${numberWithComma(plan?.anchorPrice ?? 0)}`,
            planDurationText: `For ${plan?.planDuration}`,
            offerText: offerText,//"(20% OFF - Save ₹ 760)",
            planTagLine: taglineText,
            planBenefits: planBenefits,
            MAX_BENEFITS_IN_PLAN_CARD: benifitCount,
            plan: plan,
        };
    });
    let plansCardNeedToRender = plansViewModel;
    let planCardsViewModel = plansViewModel;
    plansCardNeedToRender = plansViewModel;
    planCardsViewModel = plansViewModel;
    if ((category === Category.homeAppliances && [PlanServices.whcInspection].includes(service)) || category === Category.finance) {
        let uniqueCoverAmounts = new Set(plansArray.map((plan) => {
            return plan?.coverAmount;
        }).sort());
        let popularCoverAmount = plansArray.sort((prevPlan, nextPlan) => {
            return prevPlan.planStatistics?.rank < nextPlan.planStatistics?.rank;
        })?.first()?.coverAmount;
        let coverAmtInfo = (actionData?.coverAmountInfo ?? []).find(item =>{
             return (item?.category === actionData?.category && item?.service === actionData?.service)
        }) ?? {}
        coverAmountsViewModel = {
            uniqueCoverAmounts: uniqueCoverAmounts,
            selectedCoverAmount: [...uniqueCoverAmounts].first(),
            bottomSheetVM: {
                heading: "Change cover amount",
                description: coverAmtInfo?.description,
                coverAmountCellVM: [...uniqueCoverAmounts].map((coverAmount) => {
                    let planVM = planCardsViewModel.filter(planVM => planVM?.plan?.coverAmount === coverAmount).sort((planVM, planVM1) => {
                        return planVM.plan.price < planVM1.plan.price;
                    }).first();
                    return {
                        coverAmount: coverAmount,
                        title: "Cover amount",
                        subTitle: `₹ ${coverAmountFormat(coverAmount)}`,
                        note: planVM.price,
                        description: planVM.planDurationText,
                        isPopular: coverAmount === popularCoverAmount,
                        isSelected: [...uniqueCoverAmounts].first() == planVM.plan.coverAmount,
                    };
                }),
            },
        };
    }

    if (coverAmountsViewModel) {
        plansCardNeedToRender = planCardsViewModel?.filter(planVM => planVM?.plan?.coverAmount === coverAmountsViewModel?.selectedCoverAmount).sort((planVM, planVM1) => {
            return planVM.plan.price < planVM1.plan.price;
        });
    } else {
        plansCardNeedToRender = planCardsViewModel;
    }

    return {
        allPlansVM: planCardsViewModel,
        plansNeedToShowVM: plansCardNeedToRender,
        coverAmountsViewModel: coverAmountsViewModel,
    };
};


const changeCoverAmount = (state, coverAmountSelected) => {
    let allPlanCardsVM = state.plansCardViewModel;
    let planCardsToRender = state.planCardsToRender;
    let coverAmountsViewModel = state.coverAmountsViewModel;
    planCardsToRender = allPlanCardsVM.filter(planVM => planVM?.plan?.coverAmount === coverAmountSelected).sort((planVM, planVM1) => {
        return planVM.plan.price < planVM1.plan.price;
    });
    coverAmountsViewModel.selectedCoverAmount = coverAmountSelected;
    coverAmountsViewModel.bottomSheetVM.coverAmountCellVM = coverAmountsViewModel.bottomSheetVM.coverAmountCellVM.map((cell) => {
        cell.isSelected = cell.coverAmount === coverAmountSelected;
        return cell;
    });
    return {plansNeedToShowVM: planCardsToRender, coverAmountsViewModel: coverAmountsViewModel};
};
const getWHCApplianceData = (planData) => {
    let planQtyMap = null;
    let sortedKeys = null;
    let besPlanKey;
    if (planData && planData.length > 0) {
        planQtyMap = {};
        sortedKeys = [];
        let planRank = 0;
        for (let plan of planData) {
            if (plan.allowedMaxQuantity) {
                let planDataForQty = planQtyMap[parseInt(plan.allowedMaxQuantity)] ?? [];
                planDataForQty.push(plan);
                planQtyMap[parseInt(plan.allowedMaxQuantity)] = planDataForQty;

                if (plan?.planStatistics?.rank) {
                    if (planRank === 0 || plan?.planStatistics?.rank < planRank) {
                        planRank = plan?.planStatistics?.rank;
                        besPlanKey = parseInt(plan.allowedMaxQuantity)
                    }
                }
            }
        }
    }
    if (planQtyMap) {

        sortedKeys = Object.keys(planQtyMap);
        sortedKeys.sort((a, b) => {
            return parseInt(a) - parseInt(b);
        });
    }

    return {
        planQtyMap: planQtyMap,
        sortedKeys: sortedKeys,
        besPlanKey: besPlanKey
    };
};

const prepareProductInfoComponents = (action) => {
    let screenData = action.data.find(item => {
        return (item.category === action.buyPlanJourney.category && item.service === action.buyPlanJourney.service
            && ((action.buyPlanJourney.product && item.product) ? item.product === action.buyPlanJourney.product.code : true));
    });

    if (!screenData) {
        return;
    }
    let paragraphText = "Step " + action.pageInfo.currentStep + " of " + action.pageInfo.totalSteps;
    screenData.paragraph.value = paragraphText;

    const invoiceDaysLimit = action.buyPlanJourney?.product?.invoiceDaysLimit;
    let invoiceMinDate = new Date();
    invoiceMinDate.setDate(invoiceMinDate.getDate() - invoiceDaysLimit);

    let dateText = "";
    if (invoiceDaysLimit % 360 == 0) {
        dateText = invoiceDaysLimit / 360 + " " + "Year";
    } else if (invoiceDaysLimit % 30 == 0) {
        let month = invoiceDaysLimit / 30;
        dateText = month + +" " + (month == 1 ? " Month" : " Months");
    } else {
        dateText = invoiceDaysLimit + " Days";
    }

    if (screenData.highlightedNote && screenData.highlightedNote.components) {
        for (let item of screenData.highlightedNote.components) {
            if (item.showInvoiceDaysLimit) {
                let text = item.paragraph.value.replace("%1s", dateText);
                text = text.replace("%2s", formatDateDD_MMM_YYYY(invoiceMinDate.getTime()));
                item.paragraph.value = text;
            }
        }
    } else if (screenData.note && screenData.note.showInvoiceDaysLimit) {
        screenData.note.paragraph.value = screenData.note.paragraph.value.replace("%s", dateText);
    }

    for (let item of screenData.components) {
        switch (item.type) {
            case FormField.BRAND_SELECTION_INPUT:
                item.inputValue = action.buyPlanJourney?.brandName;
                break;
            case FormField.PURCHASE_AMOUNT_RANGE_INPUT:
                item.inputValue = action.buyPlanJourney?.invoiceRange;
                break;
            case FormField.PURCHASE_AMOUNT_INPUT:
                item.inputValue = action.buyPlanJourney?.invoiceValue;
                break;
            case FormField.GADGET_SELECTION_INPUT:
                item.inputValue = action.buyPlanJourney?.product?.name;
                break;
            case FormField.DATE_SELECTION_INPUT:
                if (action.buyPlanJourney?.invoiceDate) {
                    item.inputValue = action.buyPlanJourney?.invoiceDate;
                } else {
                    item.inputValue = new Date();
                }

                break;
        }
    }
    return screenData;
};


export const BuyPlanReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.REQUEST_SCREEN_COMPONENTS:
            return {
                ...state,
                screenComponents: (action.data?.components ?? []),
                offersData: getFilteredOfferData(action.data?.offerData),
                eventPageVariationId: action.pageVariationId
            };
        case ActionTypes.REQUEST_TESTIMONIAL_DATA:
            if (action.data?.reviewData) {
                let updatedScreenComponents = addTestimonialData(action.data?.peScreenData, action.data?.reviewData);
                return {
                    ...state, screenComponents: updatedScreenComponents,
                };
            } else {
                return {...state, screenComponents: action.data};
            }
        case ActionTypes.REQUEST_BottomSheet_SCREEN_DATA:
            return {...state, bottomSheetData: action.data, eventPageVariationId: action.pageVariationId};
        case ActionTypes.SET_MEMBERSHIPS:
            let actionData = action.data;
            if (Platform.OS === PLATFORM_OS.android) {
                actionData = {
                    ...actionData, activeMemberships: parseJSON(actionData.activeMemberships),
                    pendingMemberships: parseJSON(actionData.pendingMemberships),
                };
            }
            let returnedObject = getIDFenceTrailMembership(actionData);

            return {
                ...state,
                activeMemberships: actionData.activeMemberships,
                pendingMemberships: actionData.pendingMemberships,
                isIDFenceTrialAvailable: returnedObject.isIDFenceTrial,
                isIDFencePremiumAvailable: returnedObject.isIDFencePremium,
                isIDFenceForRenewal: returnedObject.isIDFenceForRenewal,
                idFenceTrialMembership: returnedObject.idFenceTrialMembership,
                isInitiallyMemSet: action.isInitiallyMemSet,
            };
        case ActionTypes.REQUEST_IDFENCE_PLANS_DATA:
            let trialPlan = (action.data.idFenceTrialPlansResponse?.data ?? []).length > 0 ? action.data.idFenceTrialPlansResponse?.data?.filter((plan) => {
                return plan?.trial;
            }).first() : null;
            let premiumPlan = (action.data.idFencePremiumPlansResponse?.data ?? []).filter((plan) => {
                return !(plan?.trial);
            }).first();
            let btnAndNoteTextForIDFence = prepareBtnAndNoteForIDFence(state, trialPlan, premiumPlan);
            return {
                ...state,
                isLoadingData: false,
                idFenceTrailPlan: trialPlan,
                idFencePremiumPlan: premiumPlan,
                idFenceBuyPlanText: btnAndNoteTextForIDFence.btnText,
                idFenceBuyPlanNoteText: btnAndNoteTextForIDFence.noteText,
                floatingActionData: btnAndNoteTextForIDFence.idFenceFloatActionData,
                alreadyHasPlan: btnAndNoteTextForIDFence.alreadyHasPlan,
            };
        case ActionTypes.REQUEST_SOD_SERVICE:
            return {...state, SODServicesList: action.data};
        case ActionTypes.REQUEST_SOD_PRODUCT_PAGE:
            let data = prepareSODProductComponents(state, action.data);
            return {...state, screenHeader: data.headerComponent, screenComponents: data.finalComponents};
        case ActionTypes.REQUEST_SOD_APPLIANCES_OFFERS:
            return {...state, sodAppliancesOffers: action.data};
        case ActionTypes.REQUEST_SOD_PLANS_DATA:
            return {...state, sodPlansData: action.data};
        case ActionTypes.REQUEST_SOD_TODAY_TIME_SLOTS:
            return {...state, sodAvailableTimeSlots: action.data};
        case ActionTypes.REQUEST_SOD_PRODUCT_ISSUES:
            return {...state, sodProductIssues: action.data};
        case ActionTypes.REQUEST_SOD_LABOUR_COST:
            return {
                ...state,
                labourCostResponse: action.data.labourCostResponse,
                productSpecsResponse: action.data.productSpecsResponse,
            };
        case ActionTypes.START_LOADING_DATA:
            return {...state, isLoadingData: true};
        case ActionTypes.PAINT_SOD_PRODUCT_SERVICE_SCREEN:
            let bakedData = prepareSODProductServiceScreen(state, action?.data?.selectedServiceRequestType, action?.data?.productCode, action?.data?.productName);
            return {
                ...state,
                isLoadingData: false,
                screenComponents: bakedData.components,
                labourCostVM: bakedData.labourCostVM,
                screenHeader: bakedData.headerComponent,
                planExcludedBenefitsVM: bakedData.notCoveredBenefitsVM,
                footerVM: bakedData.footerVM,
                sodIssuesVM: bakedData.issuesListVM,
                sodSelectedPlansAndQuantity: bakedData.sodSelectedPlansAndQuantity,
            };
        case ActionTypes.REQUEST_SOD_PRODUCT_SERVICE_PAGE:
            return {...state, screenComponents: action.data};
        case ActionTypes.UPDATE_SOD_PLAN_QUANTITY_PRICE:
            let {updatePlanQuantities, components, footerVM} = updateSODPlanQuantityPrice(state, action.data);
            return {
                ...state,
                screenComponents: components,
                footerVM: footerVM,
                sodSelectedPlansAndQuantity: updatePlanQuantities,
            };
            break;

        case ActionTypes.REQUEST_PRODUCT_INFO_COMPONENTS:
            let finalData = prepareProductInfoComponents(action);
            cardData = null
            if(action.applianceData){
                let gadgets = action.applianceData.find(item => {
                    return (item.category === action?.buyPlanJourney?.category && item.serviceType === action?.buyPlanJourney?.service);
                });
                cardData = {
                    type: "GadgetListComponent",
                    data: gadgets,
                };
            }

            let isCompleted = isFormComplete(finalData);
            return {
                ...state,
                productInfoData: finalData,
                isFormComplete: isCompleted,
                applianceData: cardData,
            };

        case ActionTypes.GET_PRICE_RANGE:
            return {...state, priceRangeData: action.data, action: ""};

        case ActionTypes.SET_FIELD_ERROR:
            screenData = state.productInfoData;
            setFieldError(screenData.components, action.index);
            return {...state, productInfoData: screenData};

        case ActionTypes.VALIDATE_FORM_FIELD:
            let screenData = state.productInfoData;
            let actionType = ActionTypes.VALIDATE_FORM_FIELD;
            let componentToValidate = screenData.components[action.index];

            componentToValidate.inputValue = action.text;
            screenData.components[action.index] = componentToValidate;
            if (componentToValidate.type === FormField.GADGET_SELECTION_INPUT) {
                let otherFields = screenData.components.filter(item => {
                    return (item.type === FormField.PURCHASE_AMOUNT_RANGE_INPUT
                        || item.type === FormField.DATE_SELECTION_INPUT
                        || item.type === FormField.BRAND_SELECTION_INPUT);
                });
                if (otherFields) {
                    for (let item of otherFields) {
                        if (item.type === FormField.DATE_SELECTION_INPUT) {
                            item.inputValue = new Date();
                        } else {
                            item.errorMessage = "";
                        }
                    }
                }
            } else if (componentToValidate.type === FormField.PURCHASE_AMOUNT_INPUT) {
                componentToValidate.errorMessage = "";
            } else if (componentToValidate.type === FormField.BRAND_SELECTION_INPUT) {
                let otherField = screenData.components.find(item => {
                    return item.type === FormField.PURCHASE_AMOUNT_RANGE_INPUT
                });
                if (otherField) {
                    otherField.errorMessage = "";
                }
            }
            let isValid = isFormComplete(screenData);

            return {
                ...state,
                action: actionType,
                isFormComplete: isValid,
                productInfoData: screenData,
            };

        case ActionTypes.VALIDATE_FORM:
            screenData = state.productInfoData;
            let isFormValid = true;
            for (let item of screenData?.components) {
                if (item.type === FormField.PURCHASE_AMOUNT_INPUT) {
                    if (item.inputValue == 0) {
                        isFormValid = false;
                        setFieldError(screenData.components,
                            screenData.components?.indexOf(item), AppStringConstants.ERROR_ENTER_INVOICE_AMOUNT);
                    } else if (item.inputValue < 5000) {
                        isFormValid = false;
                        setFieldError(screenData.components,
                            screenData.components?.indexOf(item),
                            AppStringConstants.ERROR_MINIMUM_AMOUNT_SHOULD_BE.replace("%s", 5000));
                    } else if (item.inputValue > 300000) {
                        isFormValid = false;
                        setFieldError(screenData.components,
                            screenData.components?.indexOf(item), AppStringConstants.ERROR_MAXIMUM_AMOUNT_EXCEEDED);
                    }
                }
            }
            actionType = null;
            if (isFormValid) {
                actionType = ActionTypes.FORM_VALIDATED;
            }
            return {
                ...state,
                action: actionType,
                productInfoData: screenData,
            };
            break;

        case ActionTypes.TOGGLE_CHECKBOX:
            screenData = state.productInfoData;
            let checkboxComponent = screenData?.note;
            if (checkboxComponent) {
                checkboxComponent.isChecked = !checkboxComponent.isChecked;
            }
            screenData.note = checkboxComponent;
            isCompleted = isFormComplete(screenData);
            return {
                ...state,
                type: ActionTypes.TOGGLE_CHECKBOX,
                isFormComplete: isCompleted,
                productInfoData: screenData,
            };

        case ActionTypes.REQUEST_BRANDS:
            let brandUiData = prepareBrandsData(action.data, action.productCode);
            return {
                ...state,
                brandUiData: brandUiData,
            };

        case ActionTypes.FILTER_BRANDS:
            let filteredBrandData = filterBrands(action.key, {...state.brandUiData});
            return {
                ...state,
                brandUiData: filteredBrandData,
            };

        case ActionTypes.REQUEST_MORE_SOD_PRODUCTS:
            let cardData = [{
                type: "GadgetListComponent",
                data: action.data,
                onItemClick: action.onItemClick
            }];
            return {...state, bottomSheetData: cardData};

        case ActionTypes.REQUEST_WHC_APPLIANCES_COUNTS:
            let applianceData = getWHCApplianceData(action.data);
            let maxAllowedQtyLimit = action.appliances?.maxAllowedQtyLimit?? 5
            let visibleItemCount = applianceData?.sortedKeys?.length;
            if (visibleItemCount > maxAllowedQtyLimit) {
                visibleItemCount = maxAllowedQtyLimit;
            }
            let stepInfo = `STEP ${action?.pageInfo?.currentStep} OF ${action?.pageInfo?.totalSteps}`;
            let paragraph = "";
            let totalProducts = 0;
            let collapsedProductText = "";
            let allProductText = "";
            let collapsedProductCount = 0;
            let productsToShow = [];
            let collapsedProductLimit = action.appliances?.collapsedProductLimit ?? 5
            if (action.appliances?.products) {
                totalProducts = action.appliances?.products?.length;
                for (let item of action.appliances?.products) {
                    if (productsToShow.length < collapsedProductLimit) {
                        collapsedProductText = collapsedProductText + item.name + ", ";
                        productsToShow.push(item);
                    }
                    allProductText = allProductText + item.name + ", ";

                }
            }

            if (action.appliances?.otherProducts) {
                totalProducts = totalProducts + action.appliances?.otherProducts?.length;
                for (let item of action.appliances?.otherProducts) {
                    if (productsToShow.length < collapsedProductLimit) {
                        collapsedProductText = collapsedProductText + item.name + ", ";
                        productsToShow.push(item);
                    }
                    allProductText = allProductText + item.name + ", ";
                }
            }
            collapsedProductText = collapsedProductText?.trim()?.substring(0, collapsedProductText.length - 1);
            allProductText = allProductText?.trim()?.substring(0, allProductText.length - 1);

            collapsedProductCount = totalProducts - productsToShow.length;
            if (collapsedProductCount > 0) {
                collapsedProductText = collapsedProductText + "...";
            }
            if (totalProducts > 0) {
                paragraph = AppStringConstants.TYPE_OF_APPLIANCES.replace("%1s", totalProducts);
                paragraph = paragraph.replace("%2s", collapsedProductText);
            }

            return {
                ...state,
                whcAppliancesData: applianceData,
                visibleItems: applianceData?.sortedKeys?.slice(0, visibleItemCount),
                header: {
                    value: AppStringConstants.WHC_APPLIANCE_COUNT_SELECTION_HEADER,
                    isHTML: true,
                },
                // paragraph: {
                //     value: paragraph,
                //     isHTML: true,
                // },
                paragraph: paragraph,
                pageInfo: {
                    value: stepInfo,
                    isHTML: false,
                },
                collapsedProductCount: collapsedProductCount,
                allProductText: allProductText,
            };

        case ActionTypes.SHOW_PLAN_LISTING:
            let plansListingPage = preparePlansListingUI(action.data);
            return {
                ...state,
                plansCardViewModel: plansListingPage.allPlansVM,
                planCardsToRender: plansListingPage.plansNeedToShowVM,
                coverAmountsViewModel: plansListingPage.coverAmountsViewModel,
                screenComponents: action.data?.components,
            };

        case ActionTypes.CHANGE_COVER_AMOUNT:
            let updatedState = changeCoverAmount(state, action.data);
            return {
                ...state,
                planCardsToRender: updatedState.plansNeedToShowVM,
                coverAmountsViewModel: updatedState.coverAmountsViewModel,
            };

        default:
            return state;
    }
};

export default BuyPlanReducer;
