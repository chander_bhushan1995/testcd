import {Category, Destination, FlowSequence, PlanServices, PLATFORM_OS, Services} from "../../Constants/AppConstants";
import {NativeModules, Platform} from "react-native";
import {setUnion} from "../../commonUtil/CommonMethods.utility";
import {store} from "../../CommonComponents/Index/index.common";
import {
    formatDateIn_DD_MMM_yyy,
    formatDateIn_DDMMMyyyy,
    formatDateIn_DDMMyyy,
    formatDateIn_MMMyyyy
} from "../../commonUtil/Formatter";
import {GEDGET_SHEET_TITLE, RANDOM_TESTEMONIALS_COLOR} from "../BuyPlanConstants";
import {AppStringConstants} from "../../Constants/AppStringConstants";
import {Deeplink} from "../../Constants/Deeplinks";
import {ButtonsAction} from "./BuyPlanButtonActions";
import {getDateFromString} from "../../commonUtil/DateUtils";


const commonReqObj = {
    "activity": "S",
    "businessUnitType": ["App"],
    "disjunctive": true,
    "isSubscription": true,
    "noOfCustomer": 1,
    "numberOfPlan": 10,
    "planCode": "",
    "priceType": "S",
    "recommendationParam": [],
    "trialPlanRequired": "N",
    // "isBackedByInsurance": null
};

const nativeBridgeRef = NativeModules.ChatBridge;
export const onBackPress = (navigation) => {
    if (store.getState().nav.routes.length > 1) { // dismiss modal presentation
        navigation.goBack();
    } else if (store.getState().nav.routes[0].routes.length > 1) { // dismiss screen presentation
        navigation.pop();
    } else {
        nativeBridgeRef.goBack();
    }
    return true
};

export const getBuySubtabIndex = (forCategory) => {
    switch (forCategory) {
        case Category.quickRapairs:
            return 0;
            break;
        case Category.personalElectronics:
            return 1;
            break;
        case Category.homeAppliances:
            return 2;
            break;
        case Category.finance:
            return 3;
            break;
        default:
            return 0;
            break;
    }
};

export const getRouteAndDataForDeeplink = (deeplinkURL, state, callback) => {
    let routeName = null;
    let data = null;

    let gadgetsData = state?.gadgetsData ?? [];
    let moduleFlowSeqData = state?.moduleFlowSequencing ?? {};

    let parse = require("url-parse");
    let url = parse(deeplinkURL, true);
    let category = url?.query?.category ?? "";
    let contentType;

    switch (true) {
        case deeplinkURL.includes(Deeplink.covidKnowMore):
            routeName = "KnowMoreScreen";
            category = url?.query?.category ?? Category.quickRapairs
            data = {
                "buyPlanJourney": {
                    "category": category,
                    "service": "know more",
                    "screenTitle": "Quick Repairs",
                    "serviceName": "Quick Repairs",
                },
            };
            break;
        case deeplinkURL.includes(Deeplink.riskCalculator):
            routeName = "ServiceDetailScreen";
            data = {
                "buyPlanJourney": {
                    "category": Category.finance,
                    "service": PlanServices.idFence,
                    "screenTitle": "ID Fence",
                    "serviceName": "ID Fence",
                    "silentRouteToRC": true,
                },
            };
            break;
        case deeplinkURL.includes(Deeplink.homeSuggestAPlan):
            routeName = "ServiceDetailScreen";
            data = {
                "buyPlanJourney": {
                    "category": Category.homeAppliances,
                    "service": PlanServices.whcInspection,
                    "screenTitle": "HomeAssist",
                    "serviceName": "HomeAssist",
                },
            };
            break;
        case deeplinkURL.includes(Deeplink.suggestAplanHome):
            routeName = "PinCodeScreen";
            service = url?.query?.service ?? PlanServices.whcInspection;
            serviceName = url?.query?.serviceName ?? "HomeAssist";
            serviceName = serviceName?.replace(/\_/g, " ");
            data = {
                "buyPlanJourney": {
                    "category": Category.homeAppliances,
                    "service": service,
                    "serviceName": serviceName,
                },
            };
            break;
        case deeplinkURL.includes(Deeplink.walletSuggestAPlan):
            routeName = "ServiceDetailScreen";
            data = {
                "buyPlanJourney": {
                    "category": Category.finance,
                    "service": PlanServices.wallet,
                    "screenTitle": "Wallet Protection",
                    "serviceName": "Wallet Protection",
                },
            };
            break
        case deeplinkURL.includes(Deeplink.suggestAplanWallet):
            routeName = "PlanListingScreen";
            data = {
                "buyPlanJourney": {
                    "category": Category.finance,
                    "service": PlanServices.wallet,
                    "serviceName": "Wallet Protection",
                },
            };
            break;
        case deeplinkURL.includes(Deeplink.mobileSuggestAPlan):
            routeName = "ServiceDetailScreen";
            let productCode = url?.query?.product ?? "MP01";
            let service = url?.query?.service ?? PlanServices.adld;
            service = (service === "EW") ? PlanServices.extendedWarranty : service;
            let serviceName = url?.query?.serviceName ?? "";
            let formattedName = serviceName?.replace(/\_/g, " ");
            let productObj = gadgetsData.filter((categoryObj) => {
                return (categoryObj?.category == Category.personalElectronics && categoryObj?.serviceType == service);
            }).first()?.products?.filter(product => product.code == productCode).first();
            data = {
                "buyPlanJourney": {
                    "category": category,
                    "service": service,
                    "screenTitle": formattedName,
                    "serviceName": formattedName,
                    "product": productObj
                },
            };
            break;
        case deeplinkURL.includes(Deeplink.mobileUpgrade):
        case deeplinkURL.includes(Deeplink.suggestAplanMobile):
        case deeplinkURL.includes(Deeplink.submitDetails):
            productCode = url?.query?.product ?? "MP01";
            productObj = gadgetsData.filter((categoryObj) => {
                return (categoryObj?.category == Category.personalElectronics && categoryObj?.serviceType == PlanServices.adld);
            }).first()?.products?.filter(product => product.code == productCode).first();
            if (!productObj) {
                break;
            }
            routeName = "ProductInfoInputScreen";
            data = {
                "buyPlanJourney": {
                    "category": Category.personalElectronics,
                    "service": PlanServices.adld,
                    "serviceName": "Accidental Damage",
                    "product": productObj,
                },
            };
            break;
        case deeplinkURL.includes(Deeplink.homeAssistSeggestPlan): //HA_EW
            routeName = "ServiceDetailScreen";
            data = {
                "buyPlanJourney": {
                    "category": Category.homeAppliances,
                    "service": PlanServices.extendedWarranty,
                    "serviceName": "Extended Warranty",
                    "screenTitle": "Extended Warranty",
                },
            };
            break;
        case deeplinkURL.includes(Deeplink.sopSuggestPlan): //SOP
            routeName = "ServiceDetailScreen";
            data = {
                "buyPlanJourney": {
                    "category": Category.homeAppliances,
                    "service": Services.sop,
                    "serviceName": "Unlimited Services",
                    "screenTitle": "Unlimited Services",
                },
            };
            break;
        case deeplinkURL.includes(Deeplink.salesRecommendationIDFence):
            routeName = "ServiceDetailScreen";
            data = {
                "buyPlanJourney": {
                    "category": Category.finance,
                    "service": PlanServices.idFence,
                    "screenTitle": "IDFence",
                    "serviceName": "ID Fence",
                },
            };
            break;
        case deeplinkURL.includes(Deeplink.salesRecommendation):
            service = url?.query?.service == "EW" ? PlanServices.extendedWarranty : (url?.query?.service ?? PlanServices.adld);
            serviceName = url?.query?.serviceName;
            if (!serviceName) {
                serviceName = service;
            }
            serviceName = serviceName?.replace(/\_/g, " ");

            switch (category) {
                case Category.personalElectronics:
                    if (url?.query?.product) {
                        let productObj = gadgetsData.filter((categoryObj) => {
                            return (categoryObj?.category === Category.personalElectronics && categoryObj?.serviceType === service);
                        }).first()?.products?.filter(product => product.code === url?.query?.product).first();
                        routeName = (url?.query?.brand && url?.query?.invoiceDate && url?.query?.invoiceValue) ? "PlanListingScreen" : "ProductInfoInputScreen";
                        data = {
                            "buyPlanJourney": {
                                "category": Category.personalElectronics,
                                "service": service,
                                "serviceName": serviceName,
                                "product": productObj,
                                "brandName": url?.query?.brand,
                                "invoiceValue": url?.query?.invoiceValue,
                                "invoiceDate": url?.query?.invoiceDate,
                            },
                        };
                    }
                    break;
                case Category.homeAppliances:
                    if (service == PlanServices.extendedWarranty) { // HA EW
                        routeName = "ProductInfoInputScreen";
                        let productObj = gadgetsData.filter((categoryObj) => {
                            return (categoryObj?.category === Category.homeAppliances && categoryObj?.serviceType === PlanServices.extendedWarranty);
                        }).first()?.products?.filter(product => product.code === url?.query?.product).first();
                        data = {
                            "buyPlanJourney": {
                                "category": Category.homeAppliances,
                                "service": PlanServices.extendedWarranty,
                                "serviceName": "Extended Warranty",
                                "product": productObj,
                                "brandName": url?.query?.brand,
                                "invoiceValue": url?.query?.amount,
                                "invoiceDate": url?.query?.date,
                            },
                        };
                    } else {
                        routeName = "PinCodeScreen";
                        data = {
                            "buyPlanJourney": {
                                "category": Category.homeAppliances,
                                "service": service == "WHC" ? PlanServices.whcInspection : Services.sop,
                                "serviceName": service == "WHC" ? "HomeAssist" : "Unlimited Services",
                            },
                        };
                    }
                    break;
                case Category.finance:
                    routeName = (url?.query?.product == Destination.wallet) ? "PlanListingScreen" : "ServiceDetailScreen";
                    data = {
                        "buyPlanJourney": {
                            "category": Category.finance,
                            "service": (url?.query?.product == Destination.wallet) ? PlanServices.wallet : PlanServices.idFence,
                            "serviceName": (url?.query?.product == Destination.wallet) ? "Wallet" : "ID Fence",
                            "screenTitle": "IDFence",
                        },
                    };
                    break;
                default:
                    break;
            }
            break;
        case deeplinkURL.includes(Deeplink.buy):
            if (deeplinkURL.includes(Deeplink.buyPlanListing)) {
                routeName = "PlanListingScreen";
                data = {};
                let service = url?.query?.service;
                if (service === 'EW') {
                    service = PlanServices.extendedWarranty;
                }
                let serviceName = url?.query?.serviceName;
                serviceName = serviceName?.replace(/\_/g, " ");
                if ((category).toLowerCase() === Category.personalElectronics?.toLowerCase() && url?.query?.product) {
                    let productObj = gadgetsData.filter((categoryObj) => {
                        return (categoryObj?.category === Category.personalElectronics && categoryObj?.serviceType === PlanServices.adld);
                    }).first()?.products?.filter(product => product.code === url?.query?.product).first();
                    let invoiceDate = url?.query?.date;
                    if (invoiceDate) {
                        invoiceDate = getDateFromString(invoiceDate, "/")
                    }

                    data = {
                        "buyPlanJourney": {
                            "category": category,
                            "service": service,
                            "serviceName": serviceName,
                            "product": productObj,
                            "brandName": url?.query?.brand,
                            "invoiceValue": url?.query?.amount,
                            "invoiceDate": invoiceDate,
                        },
                    };
                } else if ((category).toLowerCase() === Category.finance?.toLowerCase()) {
                    data = {
                        "buyPlanJourney": {
                            "category": Category.finance,
                            "service": PlanServices.wallet,
                            "serviceName": serviceName ?? "Wallet Protection",
                        },
                    };
                } else if ((category).toLowerCase() === Category.homeAppliances?.toLowerCase()) {
                    let service = url?.query?.service;
                    if (service === 'EW') {
                        service = PlanServices.extendedWarranty;
                    } else if (service === 'WHC') {
                        service = PlanServices.whcInspection;
                    }
                    let serviceName = url?.query?.serviceName;
                    serviceName = serviceName?.replace(/\_/g, " ");
                    if (service === PlanServices.extendedWarranty && url?.query?.product) {
                        let productObj = gadgetsData.filter((categoryObj) => {
                            return (categoryObj?.category === Category.personalElectronics && categoryObj?.serviceType === PlanServices.adld);
                        }).first()?.products?.filter(product => product.code === url?.query?.product).first();
                        let invoiceDate = url?.query?.date;
                        if (invoiceDate) {
                            invoiceDate = getDateFromString(invoiceDate, "/");
                        }

                        data = {
                            "buyPlanJourney": {
                                "category": category,
                                "service": service,
                                "serviceName": serviceName,
                                "product": productObj,
                                "brandName": url?.query?.brand,
                                "invoiceValue": url?.query?.amount,
                                "invoiceDate": invoiceDate,
                            },
                        };
                    } else if (service === PlanServices.whcInspection) {
                        routeName = "PinCodeScreen";
                        data = {
                            "buyPlanJourney": {
                                "category": Category.homeAppliances,
                                "service": PlanServices.whcInspection,
                                "serviceName": serviceName,
                                "silentRouteTo": {
                                    "action": "PLAN_LISTING",
                                    "allowedMaxQty": url?.query?.allowedMaxQty,
                                },
                            },
                        };
                    } else if (service === Services.sop) {
                        routeName = "PinCodeScreen";
                        data = {
                            "buyPlanJourney": {
                                "category": Category.homeAppliances,
                                "service": Services.sop,
                                "serviceName": serviceName,
                            },
                        };
                    }

                }
            } else if (deeplinkURL.includes(Deeplink.buyRecommendation)) {
                routeName = "ProductInfoInputScreen";
                let product = url?.query?.product;
                let service = url?.query?.service;
                let serviceName = url?.query?.serviceName;
                serviceName = serviceName?.replace(/\_/g, " ");

                if (category.toLowerCase() === Category.personalElectronics?.toLowerCase() ||
                    category.toLowerCase() === Category.homeAppliances?.toLowerCase()) {
                    if (!service) {
                        if (category.toLowerCase() === Category.personalElectronics?.toLowerCase()) {
                            service = PlanServices.adld
                            serviceName = "Accidental Damage";
                        } else if (category.toLowerCase() === Category.homeAppliances?.toLowerCase()) {
                            service = PlanServices.extendedWarranty
                            serviceName = "Extended Warranty"
                        }
                    }
                    if (service === "EW") {
                        service = PlanServices.extendedWarranty
                        serviceName = "Extended Warranty";
                    }
                    if (product && service) {
                        productObj = gadgetsData.filter((categoryObj) => {
                            return (categoryObj?.category === category && categoryObj?.serviceType === service);
                        }).first()?.products?.filter(productItem => productItem.code === product).first();
                    }
                    if (!productObj) {
                        if (category.toLowerCase() === Category.homeAppliances?.toLowerCase() &&
                            service && service === PlanServices.extendedWarranty) {
                        } else {
                            routeName = "";
                            break
                        }
                    }
                    let invoiceDate = url?.query?.date;
                    if (invoiceDate) {
                        invoiceDate = getDateFromString(invoiceDate, "/");
                    }
                    data = {
                        "buyPlanJourney": {
                            "category": category,
                            "service": service,
                            "serviceName": serviceName,
                            "product": productObj,
                            "brandName": url?.query?.brand,
                            "invoiceValue": url?.query?.amount,
                            "invoiceDate": invoiceDate,
                        },
                    };
                } else if (category.toLowerCase() === Category.quickRapairs?.toLowerCase()) {
                    routeName = "SODPlanDetailScreen";
                    let serviceArray = url?.query?.service?.split(",").map((serviceString) => {
                        let service = serviceString.replace(/Service/ig, "PMS");
                        return service.replace(/Repair/ig, "HA_BD");
                    });
                    data = {
                        "category": category,
                        "services": serviceArray,
                        "productCode": url?.query?.product,
                        "destination": Destination.sod,
                    };
                    if (moduleFlowSeqData?.sodFlowSequencing?.screen_1 === FlowSequence.SODFlow.PRODUCT_SCREEN) {
                        routeName = "SODPlanDetailScreen";
                    } else {
                        routeName = "PinCodeScreen";
                    }
                }
            } else if (category.toLowerCase() === Category.quickRapairs?.toLowerCase()) {
                routeName = "SODPlanDetailScreen";
                let serviceArray = url?.query?.service?.split(",").map((serviceString) => {
                    let service = serviceString.replace(/Service/ig, "PMS");
                    return service.replace(/Repair/ig, "HA_BD");
                });
                data = {
                    "category": category,
                    "services": serviceArray,
                    "productCode": url?.query?.product,
                    "destination": Destination.sod,
                };
                if (moduleFlowSeqData?.sodFlowSequencing?.screen_1 === FlowSequence.SODFlow.PRODUCT_SCREEN) {
                    routeName = "SODPlanDetailScreen";
                } else {
                    routeName = "PinCodeScreen";
                }
            } else {
                let product = url?.query?.product;
                let service = url?.query.service;
                let serviceName = (url?.query?.serviceName ?? "");
                serviceName = serviceName?.replace(/\_/g, " ");

                if (category.toLowerCase() === Category.personalElectronics?.toLowerCase()) {
                    if (!service) {
                        service = PlanServices.adld;
                    }
                    service = (service === "EW") ? PlanServices.extendedWarranty : service;
                    let productObj = gadgetsData.filter((categoryObj) => {
                        return (categoryObj?.category === Category.personalElectronics && categoryObj?.serviceType === service);
                    }).first()?.products?.filter(product => product.code === url?.query?.product).first();
                    routeName = "ServiceDetailScreen";
                    data = {
                        "buyPlanJourney": {
                            "category": category,
                            "service": service,
                            "serviceName": serviceName,
                            "product": productObj,
                            "brandName": url?.query?.brand,
                            "invoiceValue": url?.query?.amount,
                            "invoiceDate": url?.query?.date,
                        },
                    };
                } else if (category.toLowerCase() === Category.homeAppliances?.toLowerCase()) {
                    if (service) {
                        service = (service === "EW") ? PlanServices.extendedWarranty : service;
                        routeName = "ServiceDetailScreen";
                        data = {
                            "buyPlanJourney": {
                                "category": category,
                                "service": service,
                                "serviceName": serviceName
                            },
                        };
                    }
                }
            }

            break;
        case deeplinkURL.includes(Deeplink.howClaimWorks):
            contentType = url?.query?.type ?? "claim";
            service = url?.query?.service;
            serviceName = url?.query?.serviceName?.replace(/\_/g, " ") ?? "";

            if (service == "EW") {
                service = PlanServices.extendedWarranty;
            }
            productObj = gadgetsData.filter((categoryObj) => {
                return (categoryObj?.category === category
                    && (service ? categoryObj?.serviceType === service : true));
            }).first()?.products?.filter(product => product.code === url?.query?.product).first();
            routeName = "ServiceDetailScreen";
            let selectedIndex = 0;
            if (contentType.toLowerCase().includes("notcovered")) {
                selectedIndex = 2;
            } else if (contentType.toLowerCase().includes("covered")) {
                selectedIndex = 1;
            }
            data = {
                "buyPlanJourney": {
                    "category": category,
                    "service": service,
                    "serviceName": serviceName,
                    "product": productObj,
                    "screenTitle": serviceName,
                    "silentRouteTo": {
                        "action": ButtonsAction.SHOW_SECTION_BOTTOM_SHEET,
                        "type": contentType,
                        "initialSelectedIndex": selectedIndex
                    },
                },
            };
            break;
        default:
            routeName = "BuyTab";
            data = {
                "category": category
            }
            break;
    }

    if (data) {
        data["isCameFromDeeplink"] = url?.query?.isCameFromDeeplink ?? false;
        data["eventLocation"] = url?.query?.eventLocation ?? false;
        console.log(`Deeplink route ${routeName} data : \n ${JSON.stringify(data)}`);
    }

    callback(routeName, data);
};


export const getSuggestPlanRequest = (category, service, isTrialRequired, extraObj, trialPlanCode) => {
    let recommendationParam = [];
    let recommendationParamObj = {};
    if (category == Category.finance) { // FINANCE
        commonReqObj.disjunctive = "true";
        recommendationParamObj.productCode = "WP01";
        recommendationParamObj.categoryCode = category;
        if (service === PlanServices.idFence) {
            commonReqObj.trialPlanRequired = isTrialRequired ? "Y" : "N";
            recommendationParamObj.serviceList = [{name: PlanServices.idFence, value: ""}];
            if (!isTrialRequired) {
                recommendationParamObj.serviceList = [{
                    name: PlanServices.idFence,
                    value: "",
                }, {name: PlanServices.creditScoreMonitoring, value: ""}];
            }
        } else {
            recommendationParamObj.serviceList = [{name: PlanServices.wallet, value: ""}];
        }
    } else if (category === Category.homeAppliances) {  // HA
        commonReqObj.disjunctive = false;
        commonReqObj.numberOfPlan = 200;
        recommendationParamObj.categoryCode = Category.homeAppliances;

        if (service === PlanServices.extendedWarranty) {
            recommendationParamObj.invoiceValue = extraObj?.invoiceValue;
            if (extraObj?.invoiceDate) {
                recommendationParamObj.invoiceDate = formatDateIn_DDMMyyy(extraObj?.invoiceDate);
            }
            if (extraObj?.productCode) {
                recommendationParamObj.productCode = extraObj?.productCode;
            }
            recommendationParamObj.serviceList = [{name: PlanServices.extendedWarranty, value: ""}];
            recommendationParamObj.productSpecifications = [{
                name: "brand",
                value: extraObj?.brandCode ?? extraObj?.brandName
            }];
        } else if (service === "SOP") {
            commonReqObj.isBackedByInsurance = false;
            recommendationParamObj.serviceList = [{name: Services.breakdown, value: ""}];
        } else if (service === PlanServices.whcInspection) {
            recommendationParamObj.serviceList = [{name: PlanServices.whcInspection, value: ""}];
        }
    } else if (category === Category.personalElectronics) { // PE
        commonReqObj.disjunctive = true;
        commonReqObj.numberOfPlan = 500;
        if (extraObj?.invoiceRange) {
            let range = extraObj?.invoiceRange.split("-");
            recommendationParamObj.fromInvoiceValue = range[0];
            recommendationParamObj.toInvoiceValue = range[1];
        } else {
            recommendationParamObj.invoiceValue = extraObj?.invoiceValue;
        }
        recommendationParamObj.productCode = extraObj?.productCode;
        recommendationParamObj.productSpecifications = [{
            name: "brand",
            value: extraObj?.brandCode ?? extraObj?.brandName
        }];
        if (extraObj?.invoiceDate) {
            recommendationParamObj.invoiceDate = formatDateIn_DDMMyyy(extraObj?.invoiceDate);
        }
        if (service === PlanServices.extendedWarranty) {
            recommendationParamObj.serviceList = [{name: PlanServices.extendedWarranty, value: ""}];
        } else {
            recommendationParamObj.serviceList = [{name: PlanServices.adld, value: ""}];
        }

    } else if (category === Category.quickRapairs) {  // SOD
        recommendationParamObj.productCode = extraObj?.productCode;
        recommendationParamObj.serviceList = [{name: service, value: ""}];
        commonReqObj.isSubscription = false;
    } else if (category == Category.homeAppliances) {
        commonReqObj.disjunctive = "false";
        commonReqObj.numberOfPlan = 200;
        let recommendationParamObj = {};
        recommendationParamObj.categoryCode = category;
        recommendationParamObj.serviceList = [{name: service, value: ""}];
        recommendationParam.push(recommendationParamObj);
    }
    recommendationParam.push(recommendationParamObj);
    commonReqObj.recommendationParam = recommendationParam;

    if (trialPlanCode) {
        commonReqObj.planCode = trialPlanCode;
        commonReqObj.activity = "U";
    }
    return commonReqObj;
};

export const getRenewalRequest = (planCode, membership, apiData) => {
    let obj = {};

    let customerInfo = {};
    customerInfo.emailId = membership?.customers[0]?.emailId;
    customerInfo.firstName = membership?.customers[0]?.firstName;
    customerInfo.mobileNumber = membership?.customers[0]?.mobileNumber;
    customerInfo.previousCustId = membership?.customers[0]?.custId;
    customerInfo.relationship = membership?.customers[0]?.relationship;

    obj.customerInfo = [customerInfo];

    obj.initiatingSystem = Platform.OS == PLATFORM_OS.ios ? "21" : "20";
    obj.membershipId = (membership?.memId ?? "").toString();

    let orderInfo = {};
    orderInfo.partnerBUCode = apiData?.partnerBUCode;
    orderInfo.partnerCode = apiData?.partnerCode;
    orderInfo.planCode = (planCode ?? "").toString();
    obj.orderInfo = orderInfo;

    return obj;
};

export const getRenewalRequestForHA = (planCode, buyPlanJourney) => {
    let renewalApiRequest = {};

    const renewalData = buyPlanJourney?.renewalData;
    let customer = renewalData?.customerInfo ?? {};
    let renewalCustomerInfo = customer;

    let renewalAddressInfo = {};
    renewalAddressInfo.addrType = "INSPECTION";
    renewalAddressInfo.cityCode = buyPlanJourney?.cityCode;
    renewalAddressInfo.stateCode = buyPlanJourney?.stateCode;
    renewalAddressInfo.pinCode = buyPlanJourney?.pinCode;

    let renewalPaymentInfo = {};
    renewalPaymentInfo.paymentMode = null;

    let renewalOrderInfo = {};
    renewalOrderInfo.planCode = planCode?.toString();
    renewalOrderInfo.partnerBUCode = "2184";
    renewalOrderInfo.partnerCode = "139";

    renewalApiRequest.customerInfo = [renewalCustomerInfo];
    renewalApiRequest.addressInfo = [renewalAddressInfo];
    renewalApiRequest.paymentInfo = renewalPaymentInfo;
    renewalApiRequest.orderInfo = renewalOrderInfo;

    renewalApiRequest.initiatingSystem = Platform.OS == PLATFORM_OS.ios ? "21" : "20";
    renewalApiRequest.membershipId = renewalData?.memID?.toString();
    return renewalApiRequest;
};


// prepare product info request object with product issues
const getSODProductInfoWithIssues = (extraData) => {
    let productInfoWithIssuesArray = [];

    extraData?.plans?.map((plan) => {

        if (extraData?.selectedServiceRequestType === Services.breakdown) {
            plan?.issues?.map((issueArray) => {
                let planInfoObj = {};
                planInfoObj.planPrice = plan?.singleQuantityPrice;
                planInfoObj.serviceType = extraData?.selectedServiceRequestType;
                planInfoObj.planCode = plan?.planCode;
                planInfoObj.productCode = extraData?.selectedProductCode;
                planInfoObj.issueReportedByCustomer = issueArray.map((issueObj) => {
                    return {issueId: issueObj?.id, issueDescription: issueObj?.text};
                });
                productInfoWithIssuesArray.push(planInfoObj);
            });
        } else {
            for (let index = 0; index < plan?.totalQuantity; index++) {
                let planInfoObj = {};
                planInfoObj.planPrice = plan?.singleQuantityPrice;
                planInfoObj.serviceType = extraData?.selectedServiceRequestType;
                planInfoObj.planCode = plan?.planCode;
                planInfoObj.productCode = extraData?.selectedProductCode;
                productInfoWithIssuesArray.push(planInfoObj);
            }
        }
    });

    return productInfoWithIssuesArray;
};


export const createCustomerInfo = (destination, plan, apiData, extraData) => {

    let customerInfo = {};
    customerInfo.custId = apiData?.customer_id;
    customerInfo.firstName = apiData.firstName;
    customerInfo.lastName = apiData.lastName;
    customerInfo.emailId = apiData.user_Email;
    customerInfo.mobileNumber = apiData.mobile_number;
    customerInfo.mobileOs = Platform.OS == PLATFORM_OS.ios ? "IOS" : "AND";
    customerInfo.relationship = "self";
    let assetInfo = {};
    if (destination === Destination.sodQTY) {
        assetInfo.productCode = extraData?.selectedProductCode;
        assetInfo.invoiceValue = extraData?.overAllTotalPrice;
        assetInfo.quantity = extraData?.overAllQuantity;
    } else {
        if (destination !== Destination.whc) {
            assetInfo.productCode = extraData?.productCode ?? extraData?.buyPlanJourney?.product?.code;
            assetInfo.assetMake = extraData?.buyPlanJourney?.brandCode ?? extraData?.buyPlanJourney?.brandName;
            if (extraData?.buyPlanJourney?.invoiceDate) {
                assetInfo.invoiceDate = formatDateIn_DD_MMM_yyy(extraData?.buyPlanJourney?.invoiceDate);
            }
            assetInfo.invoiceValue = extraData?.buyPlanJourney?.invoiceValue;
            if (destination === Destination.peEW) {
                assetInfo.assetModel = "ABCDE";
                assetInfo.serialNo1 = "12345678";
            }
        }
    }

    if (![Destination.haSOP, Destination.whc].includes(destination)) {
        customerInfo.assetInfo = [assetInfo];
    }

    let addressInfo = {};
    addressInfo.addrType = destination === Destination.sodQTY ? "SOD" : "INSPECTION";
    addressInfo.pinCode = extraData?.pinCode ?? "";

    let orderInfo = {};
    orderInfo.partnerBUCode = apiData?.partnerBUCode;
    orderInfo.partnerCode = apiData?.partnerCode;
    orderInfo.planCode = destination === Destination.sodQTY ? -1 : plan?.planCode;
    orderInfo.activity = destination === Destination.sodQTY ? "SO" : null;
    orderInfo.paymentMode = "ONLINE";
    if (extraData?.buyPlanJourney?.offer) {
        orderInfo.promoName = extraData?.buyPlanJourney?.offer?.couponCode || null;
    }
    orderInfo.applicationDate = formatDateIn_DDMMMyyyy(new Date());

    if (destination === Destination.sodQTY) {
        orderInfo.planInfo = extraData?.plans?.map((plan) => {
            return {planCode: plan.planCode, quantity: plan.totalQuantity};
        });
    }

    let customerPaymentInfo = {};
    customerPaymentInfo.paymentMode = "ONLINE";
    customerPaymentInfo.viaLink = true;
    if (extraData?.siPlanCode) {
        orderInfo.siPlanCode = extraData?.siPlanCode;
    }
    customerPaymentInfo.paymentOptions = extraData?.paymentOptions;

    let thirdPartyParams = null;
    if (destination === Destination.sodQTY) {
        thirdPartyParams = {};
        thirdPartyParams.insuranceBacked = "N";
        thirdPartyParams.serviceRequestSourceType = "SOD";
        thirdPartyParams.serviceRequestType = extraData?.selectedServiceRequestType;
        thirdPartyParams.initiatingSystem = 21;
        thirdPartyParams.assets = getSODProductInfoWithIssues(extraData);
    }

    let newCustomer = {};
    newCustomer.customerInfo = [customerInfo];
    newCustomer.orderInfo = orderInfo;
    newCustomer.addressInfo = [addressInfo];
    newCustomer.initiatingSystem = Platform.OS == PLATFORM_OS.ios ? 21 : 20;
    newCustomer.action = "save";
    if (destination === Destination.peEW) {
        newCustomer.action = "submit";
    }
    if (destination === Destination.ew) {
        newCustomer.customerOrderInfo = orderInfo;
    }
    newCustomer.paymentInfo = customerPaymentInfo;
    newCustomer.thirdPartyParams = thirdPartyParams;

    if (destination === Destination.sodQTY || destination === Destination.ew || destination === Destination.whc) {
        newCustomer.customerOrderInfo = orderInfo;
    }

    return newCustomer;
};

export const getServicesString = (plan) => {
    let set = getServicesListSet(plan);
    return Array.from(set).join(",");
};

export const getServicesListSet = (plan) => {
    let servicesArray = new Set();
    if (plan?.productServices?.length > 0) {
        plan?.productServices?.map((productService) => {
            if (productService.serviceList?.length > 0) {
                productService.serviceList?.map((service) => {
                    servicesArray.add(service);
                });
            }
        });
    }
    return Array.from(servicesArray);
};

export const getProductWithServicesList = (destination, plans = [], servicesList = [], product) => { // right now handled for idfence only
    let serviceSet = new Set();

    if (plans.length > 0) {
        plans.map((plan) => {
            serviceSet = setUnion(serviceSet, getServicesListSet(plan));
        });
    }

    if (servicesList.length > 0) {
        serviceSet = setUnion(serviceSet, new Set(servicesList));
    }
    let newSet = new Set();

    for (let service of serviceSet) {
        newSet.add(service.replace(/ /g, "_"));
    }

    if (product) {
        return [product, Array.from(newSet).join(",")].join("|")
    }

    switch (destination) {
        case Destination.ew:
            return ["Home EW", Array.from(newSet).join(",")].join("|");
            break;
        case Destination.idfance:
            return [Destination.wallet, Array.from(newSet).join(",")].join("|");
            break;
        case Destination.sod, Destination.sodQTY:
            return [Destination.sod, Array.from(newSet).join(",")].join("|");
            break;
        default:
            return [destination, Array.from(newSet).join(",")].join("|");
            break;
    }
};

export const hasCreditScore = (plan) => {
    let index = 0;
    let innerIndex = 0;
    if (plan?.productServices?.length > 0) {
        for (index = 0; index < plan?.productServices?.length; index++) {
            if (plan?.productServices[index].serviceList?.length > 0) {
                for (innerIndex = 0; innerIndex < plan?.productServices[index].serviceList?.length; innerIndex++) {
                    if (plan?.productServices[index].serviceList[innerIndex] === PlanServices.creditScoreMonitoring) {
                        return true;
                    }
                }
            }
        }
    }
    return false;
};

export const getDestinationTitle = (destination, productName) => {
    switch (destination) {
        case Destination.whc:
            return "HomeAssist";
            break;
        case Destination.ew:
            return "Home EW";
            break;
        case Destination.pe:
        case Destination.peEW:
            return productName;
            break;
        case Destination.gadgetserve:
            return "GadgetServ";
            break;
        case Destination.wallet:
            return "Wallet";
            break;
        case Destination.idfance:
            return "ID Fence";
            break;
        case Destination.sod:
            return "SOD";
            break;
        default:
            return "";
    }

};

export const getParamsForPaymentFlow = (destination, plan, APIData, extraProps) => {

    let serviceType = "IDFence";
    let customerInfo = createCustomerInfo(destination, plan, APIData, extraProps);


    let servicesSet = getServicesListSet(plan);

    switch (destination) {
        case Destination.wallet:
            serviceType = "wallet";
            break;
        case Destination.idfance:
            serviceType = "IDFence";
            break;
        case Destination.pe:
        case Destination.peEW:
            serviceType = "PE";
            break;
        case Destination.whc:
        case Destination.ew :
        case Destination.haSOP:
            serviceType = "HS";
            break;
        case Destination.sod:
        case Destination.sodQTY:
            serviceType = "SOD";
            break;
        default:
            break;
    }

    let eventDict = {};
    if (destination == Destination.sod || destination == Destination.sodQTY) {
        eventDict = {
            ServiceType: customerInfo.thirdPartyParams?.serviceRequestType,
            Appliance: customerInfo.customerInfo[0].assetInfo[0].productCode,
            Count: (customerInfo.customerInfo[0].assetInfo[0].quantity ?? "").toString(),
            Cost: (customerInfo.customerInfo[0].assetInfo[0].invoiceValue ?? "").toString(),
            ProductService: getProductWithServicesList(destination, [], [customerInfo.thirdPartyParams?.serviceRequestType], extraProps?.productName),
        };
    } else {
        eventDict = {
            TYPE: getDestinationTitle(destination, extraProps?.productName),
            SERVICE: getServicesString(plan),
            COVER_AMOUNT: plan?.coverAmount ?? "",
            PREMIUM_AMOUNT: plan?.price ?? "",
            FINAL_PREMIUM_AMOUNT: extraProps?.finalPremiumAmount,
            ADDON_SERVICE: "",
            PRODUCT: extraProps?.productName,
            PLAN: plan?.planName ?? "",
            PLAN_CODE: plan?.planCode,
            DISCOUNT: extraProps?.discount,
            NO_OF_APPLIANCES: extraProps?.numberOfAppliances,
            ProductService: getProductWithServicesList(destination, [plan], [], extraProps?.productName),
        };
    }

    let paramObj = {
        data: JSON.stringify(customerInfo),
        service_Type: serviceType,
        hasEW: servicesSet.includes(PlanServices.extendedWarranty),
        isHA_SOP: (Destination.haSOP === destination) ? true : false,
        hasCreditScore: hasCreditScore(plan),
        eventDict: JSON.stringify(eventDict),
        isTrialPlan: plan?.trial,
        isFreeReward: false,
    };
    if (destination == Destination.ew) {
        paramObj.custId = APIData?.customer_id;
    }
    return paramObj;
};


export const checkAlreadyHasPlanInMembership = (pendingMemberships = [], activeMemberships = [], planCode) => {
    if (activeMemberships?.filter(mem => mem?.plan?.planCode === planCode).length > 0 || pendingMemberships?.filter(mem => mem?.plan?.planCode === planCode).length > 0) {
        return true;
    }
    return false;
};

export const checkSODProductServiceInMembership = (activeMembershipsParam = [], serviceRequestType, selectedProduct) => {
    let isAvailable = false;
    let membershipIteration = 0;
    let serviceIteration = 0;
    // filter ha active memberships
    let activeMemberships = activeMembershipsParam.filter((membership) => {
        return membership.membershipStatus === "A" && membership.categories.includes(Category.homeAppliances);
    }) ?? [];

    if (Array.isArray(activeMemberships)) {
        membershipIterator:  while (membershipIteration < activeMemberships.length) {
            let pmsBreakdownServices = activeMemberships[membershipIteration].plan?.services?.filter((serviceObj) => {
                if (Array.isArray(serviceObj?.description)) {
                    return ["HA_BD", "PMS"].includes(serviceObj?.description[0]?.claimType);
                }
                return false;
            });

            serviceIterator: while (serviceIteration < pmsBreakdownServices.length) {
                let setOfProducts = new Set();
                let claimType = pmsBreakdownServices[serviceIteration].description[0].claimType;

                // check for normal ha_bd plan
                if (claimType === Services.breakdown && pmsBreakdownServices[serviceIteration].insuranceBacked && claimType === serviceRequestType) {
                    setOfProducts = new Set((activeMemberships[membershipIteration].assets ?? []).map((asset) => {
                        return asset.prodCode;
                    }));
                }

                // check for normal sop plan
                else if (claimType === Services.breakdown && !pmsBreakdownServices[serviceIteration].insuranceBacked && claimType === serviceRequestType) {
                    setOfProducts = new Set(activeMemberships[membershipIteration].plan?.products ?? []);
                }

                // check for normal pms plan & noOfVisitsLeft is greater than zero
                else if (claimType === Services.pms && pmsBreakdownServices[serviceIteration].noOfVisitsLeft > 0 && claimType === serviceRequestType) {
                    setOfProducts = new Set(activeMemberships[membershipIteration].plan?.products ?? []);
                    // check for restricted PMS
                    let attributes = (pmsBreakdownServices[serviceIteration].attributes ?? []).filter((attribute) => {
                        return attribute?.attributeName === "RESTRICT_ASSETS" && attribute?.attributeValue == "Y";
                    });

                    if (attributes.length > 0) {

                        let assets = new Set((activeMemberships[membershipIteration].assets ?? []).map((asset) => {
                            return asset.prodCode;
                        }));
                        setOfProducts = new Set([...setOfProducts].filter((productCode) => {
                            return assets.has(productCode);
                        }) ?? []);
                    }
                }

                if (setOfProducts.size > 0 && setOfProducts.has(selectedProduct)) {
                    isAvailable = true;
                    break serviceIterator;
                    break membershipIterator;
                }

                ++serviceIteration;
            }

            ++membershipIteration;
            serviceIteration = 0;
        }
    }
    return isAvailable;
};

export const getRatingReviewRequest = (page, pageSize) => {
    let requestObject = {
        reportName: "RATING_FEEDBACK",
        columnFilters: [],
        columnSorts: [],
        pageNumber: page,
        pageSize: pageSize ?? 5,
    };
    return requestObject;
};

export const parseTestimonialResponse = (response) => {
    let parsedRes = {};
    let data = {};
    let reviews = [];
    if (response?.data?.kpiDataSet && response?.data?.kpiDataSet[0]?.kpis && response?.data?.kpiDataSet[0]?.kpis[0]?.measures) {
        for (let item in response?.data?.kpiDataSet[0]?.kpis[0]?.measures) {
            if (item.label === "Average Rating" && item.dataSet) {
                data.averageRating = item.dataSet[0];

            }
        }
    }
    if (response?.data?.tableDataSet) {
        let tableDataSet = response?.data?.tableDataSet[0];
        data.totalReviews = tableDataSet?.page?.totalRecords;
        data.totalPages = tableDataSet?.page?.totalPages;
        data.page = tableDataSet?.page?.number;
        data.pageSize = tableDataSet?.page?.limit;

        if (tableDataSet?.rows) {
            for (let rowItem of tableDataSet?.rows) {
                let reviewObject = {};
                if (rowItem[4]) {
                    reviewObject.header = {
                        value: rowItem[4],
                        isHTML: false,
                    };
                    reviewObject.paragraph = {
                        value: rowItem[0] + "<br>" + (rowItem[1] ? rowItem[1] + ", " : "") + formatDateIn_MMMyyyy(rowItem[2]),
                        isHTML: true,
                    };
                } else {
                    reviewObject.header = {
                        value: rowItem[0],
                        isHTML: false,
                    };
                    reviewObject.paragraph = {
                        value: rowItem[1] + ", " + rowItem[2],
                        isHTML: false,
                    };
                }

                reviewObject.rating = rowItem[3];
                reviewObject.splitName = rowItem[0];
                reviewObject.color = RANDOM_TESTEMONIALS_COLOR();
                reviews.push(reviewObject);
            }
            data.Reviews = reviews;
        }
    }
    parsedRes.data = data;
    return parsedRes;
};

export const getGadgetSheetTitle = (data) => {
    let title = "";
    if (data?.title) {
        title = data.title;
    } else {
        if (data?.category === Category.homeAppliances) {
            title = AppStringConstants.SELECT_APPLIANCE;
        } else {
            title = GEDGET_SHEET_TITLE(data?.serviceTypeName);
        }
    }
    return title;
};

export const getServiceNameForClaims = (serviceName) => {
    switch (serviceName) {
        case Services.liquidDamage:
            return "accidental damages";
            break;
        case Services.pms:
            return "PMS";
            break;
        case PlanServices.extendedWarranty:
            return "Extended Warranty Services";
            break;
        default:
            return serviceName;
            break;
    }
};

export const getObjectByLabel = (sourceObj, sourcekey, label) => {
    let searchedObj = null;
    for (let key of Object.keys(sourceObj)) {
        const keyValue = sourceObj[key];
        if (typeof keyValue === "object") {
            searchedObj = getObjectByLabel(keyValue, sourcekey, label);
            if (searchedObj) {
                break;
            }
        } else if (key === sourcekey && keyValue === label) {
            return sourceObj;
        }
    }
    return searchedObj;
};

export const getPlanListScreenTitle = (buyPlanJourney) => {
    if (!buyPlanJourney || !buyPlanJourney?.category) {
        return "Pick a plan";
    }
    if (buyPlanJourney?.category == Category.personalElectronics) {
        let title = (buyPlanJourney?.product?.name + " " + buyPlanJourney?.serviceName)?.toLowerCase();
        let firstLetter = title.slice(0, 1);
        title = firstLetter.toUpperCase() + title.substring(1);
        return title;
    } else if (buyPlanJourney?.category == Category.homeAppliances) {
        if (buyPlanJourney.service == Services.sop) {
            return "Appliance Service plan";
        } else if (buyPlanJourney.service == PlanServices.extendedWarranty) {
            return "Extended warranty plans";
        } else if (buyPlanJourney.service == PlanServices.whcInspection) {
            return "HomeAssist Plans";
        }
    } else if (buyPlanJourney?.category == Category.finance) {
        if (buyPlanJourney.service == PlanServices.wallet) {
            return "Wallet protection plan"
        }

    }
    return "Pick a plan";
}
