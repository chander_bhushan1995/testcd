import { useEffect, useRef } from "react";
import { BackHandler } from "react-native";

export const useConstructor = (callBack = () => {
}) => {
    const hasBeenCalled = useRef(false);
    if (hasBeenCalled.current) {
        return;
    }
    callBack();
    hasBeenCalled.current = true;
};

export function useBackButton(handler) {
    // Frustration isolated! Yay! 🎉
    useEffect(() => {
        BackHandler.addEventListener("hardwareBackPress", handler);

        return () => {
            BackHandler.removeEventListener(
                "hardwareBackPress",
                handler,
            );
        };
    }, [handler]);
}
