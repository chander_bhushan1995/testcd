import React, {useEffect, useReducer, useRef, useState} from "react";
import BuyPlanReducer, {initialState} from "../Helpers/BuyPlanReducer";
import {Destination, FIREBASE_KEYS} from "../../Constants/AppConstants";
import * as ActionTypes from "../../Constants/ActionTypes";
import {getPlansForWHC} from "../../network/APIHelper";
import {Dimensions, FlatList, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import TextView from "../../Catalyst/components/common/TextView";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import {TextStyle} from "../../Constants/CommonStyle";
import Loader from "../../Components/Loader";
import GadgetItemComponent from "../Components/GadgetItemComponent";
import {_APPLIANCES, _TYPE_OF_APPLIANCES_ARE_COVERED} from "../../Constants/AppStringConstants";
import BackButton from "../../CommonComponents/NavigationBackButton";
import {onBackPress} from "../Helpers/BuyPlanUtils";
import {getFirebaseData} from "../../commonUtil/AppUtils";
import AutoHeightRBSheet from "../../Components/AutoHeightRBSheet";
import images from "../../images/index.image";
import {AGE, CATEGORY, INVOICE_DATE, LOCATION, PRODUCT, SERVICE} from "../../Constants/WebengageAttrKeys";
import {NO_PLANS_FOUND_EVENT, SELECT_NO_OF_APPLIANCE} from "../../Constants/WebengageEvents";
import {logWebEnageEvent} from "../../commonUtil/WebengageTrackingUtils";
import {useBackButton} from "../Helpers/CustomHooks";

let currentStep = 0;
let totalSteps = 0;
let commonEventData = {};
const ApplianceCountSelectionScreen = (props) => {
    const [reducerState, dispatch] = useReducer(BuyPlanReducer, initialState);
    const [isLoading, setIsLoading] = useState(false);
    const allApplianceBottomSheet = useRef(null);
    const allAppliances = "No data available";
    const buyPlanJourney = props?.navigation?.getParam("buyPlanJourney") ?? {};
    const destination = props?.navigation?.getParam("destination") ?? Destination.whc;
    const routes = Destination.getRoutesForDestination(destination) ?? Destination.getDestination(buyPlanJourney.category, buyPlanJourney.service);

    useBackButton(() => onBackPress(props.navigation));


    useEffect(() => {
        currentStep = routes.indexOf("ApplianceCountSelectionScreen") + 1;
        totalSteps = routes.length;
        commonEventData[LOCATION] = "Select Appliance Count";
        commonEventData[CATEGORY] = buyPlanJourney?.category;
        commonEventData[SERVICE] = buyPlanJourney?.service;
        commonEventData[PRODUCT] = buyPlanJourney?.product?.name;
        fetchPlans();
    }, []);

    const fetchPlans = () => {
        setIsLoading(true);
        getPlansForWHC((response, error) => {
            if (error) {
                return;
            }
            if (!response.data || (response.data ?? []).length == 0) {
                let eventData = {...commonEventData};
                eventData[INVOICE_DATE] = "";
                eventData[AGE] = "";

                logWebEnageEvent(NO_PLANS_FOUND_EVENT, eventData);
            }
            getAllAppliances(response);
            setIsLoading(false);
        });
    };

    const getAllAppliances = (planRes) => {
        getFirebaseData(FIREBASE_KEYS.SERVICE_GADGETS, (data) => {
            let gadgets = data.find(item => {
                return (item.category === buyPlanJourney?.category && item.serviceType === buyPlanJourney?.service);
            });
            dispatch({
                type: ActionTypes.REQUEST_WHC_APPLIANCES_COUNTS,
                pageInfo: {currentStep: currentStep, totalSteps: totalSteps},
                data: planRes.data ?? [],
                appliances: gadgets,
            });

        });
    };

    const itemSeparator = (highlighted) => {
        return <View
            style={[styles.divider,
                highlighted && {marginLeft: 0},
            ]}
        />;
    };

    const _renderComponent = ({item, index, separators}) => {
        let key = item;
        let textToShow = "";
        if (key == 1) {
            textToShow = key + " Appliance";
        } else {
            textToShow = _APPLIANCES(key);
        }
        let componentItem = {
            name: textToShow,
            data: item,
            tag: (key == reducerState?.whcAppliancesData?.besPlanKey) ? "BEST SELLING" : ""
        };
        if(buyPlanJourney?.silentRouteTo?.action === "PLAN_LISTING" && buyPlanJourney?.silentRouteTo?.allowedMaxQty == key){
            moveToPlanListing(componentItem)
        }
        return <GadgetItemComponent onShowUnderlay={separators.highlight}
                                    onHideUnderlay={separators.unhighlight}
                                    data={componentItem}
                                    onPress={(item) => {
                                        moveToPlanListing(item);
                                    }}
        />;
    };

    const moveToPlanListing = (qtyItem) =>{
        let eventData = {...commonEventData};
        eventData["Value"] = qtyItem.data
        buyPlanJourney.numberOfAppliances = qtyItem.data
        logWebEnageEvent(SELECT_NO_OF_APPLIANCE, eventData);

        let routes = Destination.getRoutesForDestination(destination);
        if (routes.includes(props.navigation.state.routeName)) {
            let index = routes.indexOf(props.navigation.state.routeName);
            props.navigation.navigate(routes[index + 1], {
                buyPlanJourney: buyPlanJourney,
                destination: destination,
                suggestedPlans: reducerState.whcAppliancesData?.planQtyMap[qtyItem.data],
            });
        }
    }

    const headerDividerComponent = () => {
        return <View
            style={styles.divider}
        />;
    };

    const getContentView = () => {
        if (!reducerState.whcAppliancesData) return null;
        let moreText = "";
        if (reducerState.collapsedProductCount) {
            moreText = moreText + "+" + reducerState.collapsedProductCount + " more";
        }
        return <View style={{flex: 1, paddingBottom: spacing.spacing32}}>
            <TextView style={styles.stepInfoText}
                      texts={reducerState?.pageInfo}/>
            <View style={{marginHorizontal: spacing.spacing16}}>
                {reducerState.header ? <TextView style={styles.primaryText}
                                                 texts={reducerState?.header}/> : null}
                {reducerState.paragraph ?
                    <Text style={{...styles.secondaryText}}>
                        {reducerState?.paragraph}
                        {moreText && <Text style={styles.innerText} onPress={() => {
                            allApplianceBottomSheet.current.open();
                        }}>{moreText}</Text>}


                    </Text> : null}

            </View>
            <FlatList style={{ flex: 1, marginTop: spacing.spacing32}}
                      data={reducerState.visibleItems ?? []}
                      renderItem={_renderComponent}
                      showsVerticalScrollIndicator={true}
                      ItemSeparatorComponent={itemSeparator}
                      ListHeaderComponent={headerDividerComponent}
                      bounces={false}
                      keyExtractor={(item, index) => {
                          return index.toString();
                      }}
                      extraData={false}
            />
        </View>;
    };

    return (<SafeAreaView style={{flex: 1, backgroundColor: colors.white}}>
        {
            isLoading
                ? <Loader isLoading={isLoading}/>
                : getContentView()
        }
        <AutoHeightRBSheet
            ref={allApplianceBottomSheet}
            duration={10}
            customStyles={{
                container: {
                    paddingVertical: 0,
                    borderTopLeftRadius: 8,
                    borderTopRightRadius: 8,
                    maxHeight: Dimensions.get("screen").height / 1.2,
                },
            }}
            closeOnSwipeDown={false}>
            <View style={{
                borderRadius: spacing.spacing16,
                marginHorizontal: spacing.spacing16,
                paddingTop: spacing.spacing20,
                paddingBottom: spacing.spacing80
            }}>
                <TouchableOpacity style={styles.close} onPress={() => {
                    allApplianceBottomSheet.current.close();
                }}>
                    <Image source={images.closeSheet} style={styles.crossIcon}/>
                </TouchableOpacity>
                <TextView style={{...styles.primaryText, paddingHorizontal: spacing.spacing16}}
                          texts={{
                              value: _TYPE_OF_APPLIANCES_ARE_COVERED(15),
                              isHTML: true,
                          }}/>
                <Text style={{
                    ...styles.bottomSheetParagraph,
                    paddingHorizontal: spacing.spacing16,
                    marginBottom: spacing.spacing32,
                }}>{reducerState.allProductText}
                </Text>

            </View>
        </AutoHeightRBSheet>

    </SafeAreaView>);
};

ApplianceCountSelectionScreen.navigationOptions = (navigationData) => {
    return {
        title: "Find a Plan",
        headerLeft: <BackButton onPress={() => {
            onBackPress(navigationData.navigation);
        }}/>,
    };
};


const styles = StyleSheet.create({
    primaryText: {
        ...TextStyle.text_16_bold,
        marginTop: spacing.spacing4,
        marginBottom: spacing.spacing14,
    },
    bottomSheetParagraph: {
        ...TextStyle.text_16_regular,
        marginTop: spacing.spacing4,
    },
    secondaryText: {
        ...TextStyle.text_14_normal,
        marginBottom: spacing.spacing14,

    },
    stepInfoText: {
        ...TextStyle.text_12_bold,
        marginTop: spacing.spacing32,
        marginHorizontal: spacing.spacing16,
    },
    divider: {
        height: 0.1,
        width: "100%",
        backgroundColor: colors.greye0,
    },
    innerText: {
        color: colors.color_1468e2,
    },
    close: {
        height: 48,
        width: 48,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "flex-end",

    },
    crossIcon: {
        height: 48,
        width: 48,
        resizeMode: "contain",
    },

});


export default ApplianceCountSelectionScreen;
