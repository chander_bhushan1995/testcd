import React, {useEffect, useReducer, useRef, useState} from "react";
import {
    Dimensions,
    FlatList,
    Image, NativeModules, Platform,
    SafeAreaView,
    StyleSheet, Text,
    TouchableOpacity,
    View,
} from "react-native";
import codePush from "react-native-code-push";
import {ButtonsAction, handleBuyPlanBtnActions} from "../Helpers/BuyPlanButtonActions";
import {getComponent} from "../../Catalyst/components/ComponentsFactory";
import Loader from "../../Components/Loader";
import BuyPlanReducer, {initialState} from "../Helpers/BuyPlanReducer";
import {getDataFromCDNWithURL} from "../../network/APIHelper";
import * as ActionTypes from "../../Constants/ActionTypes";
import images from "../../images/index.image";
import spacing from "../../Constants/Spacing";
import {getFirebaseData} from "../../commonUtil/AppUtils";
import {PLATFORM_OS, TESTIMONIAL_LIST_TITLE} from "../../Constants/AppConstants";
import buyPlanJourney from "../Helpers/BuyPlanJourneySingleton";

import {logWebEnageEvent} from "../../commonUtil/WebengageTrackingUtils";
import * as WebEngageEventsName from "../../Constants/WebengageEvents";
import {useBackButton} from "../Helpers/CustomHooks";
import {onBackPress} from "../Helpers/BuyPlanUtils";
import {store} from "../../CommonComponents/Index/index.common";
import AutoHeightRBSheetV2 from "../../Components/AutoHeightRBSheetV2.tsx";
import { MembershipButtonActions } from "../../MembershipTab/Helpers/MembershipButtonActions";
import TextListBottomSheet from "../Components/SODComponents/TextListBottomSheet";
import { TextStyle } from "../../Constants/CommonStyle";
import colors from "../../Constants/colors";

let MAX_API_COUNT = 1;
const chatBrigeRef = NativeModules.ChatBridge;
const RenderDynamicComponents = props => {

    const getfooterComponent = () => {
         const footerData = (props.data ?? []).filter(component=>component.position == "footer")?.first()
        if(footerData){
            const Component = getComponent(footerData?.type);
            let extraProps = {
                ...props.extraProps,
                ...footerData?.extraProps,
            };
            return <Component data={footerData} extraProps={extraProps}/>
        }
    }

    const _renderComponent = ({item, index}) => {
        const Component = getComponent(item.type);
        let extraProps = {
            ...props.extraProps,
            ...item.extraProps,
        };
        return <Component data={item} extraProps={extraProps}/>;
    };

    return (
        <View style={{marginBottom:60,justifyContent:"flex-end"}}>
            <FlatList
                data={(props.data ?? []).filter(component=>component.position != "footer") ?? []}
                renderItem={_renderComponent}
                showsVerticalScrollIndicator={false}
                bounces={false}
                keyExtractor={(item, index) => {
                    return index.toString();
                }}
                extraData={false}
            />
            {
                getfooterComponent()
            }
        </View>
    );
};

const getBottomSheetView = (bottomSheetType, data, extraProps) => {
    switch (bottomSheetType) {
        case ButtonsAction.SHOW_SECTION_BOTTOM_SHEET:
        case ButtonsAction.SHOW_TESTIMONIAL_BOTTOM_SHEET:
        case ButtonsAction.SHOW_TESTIMONIAL_READ_MORE_BOTTOM_SHEET:
        case ButtonsAction.SHOW_GEDGETS_BOTTOM_SHEET:
        case ButtonsAction.SHOW_MORE_SOD_PRODUCTS:
        case ButtonsAction.SHOW_SERVICE_BOTTOM_SHEET:
            return <RenderDynamicComponents data={data} extraProps={extraProps}/>;
            break;
        case MembershipButtonActions.SHOW_ASSETS_BOTTOMSHEET:
            return <TextListBottomSheet data={extraProps.bottomSheetData}/>
            break;
        default:
            return <View/>;
            break;
    }
};


const BottomSheetScreen = props => {
    const rbSheet = useRef(null);

    const bottomSheetType = props.navigation.getParam("bottomSheetType") ?? "";
    let bottomSheetData = props.navigation.getParam("bottomSheetData") ?? {};
    const servicePageReducer = props.navigation.getParam("servicePageReducer") ?? {}
    const buyPlanJourney = props.navigation.getParam("buyPlanJourney") ?? {}

    const eventsData = bottomSheetData?.eventsData ?? {};
    const contentType = bottomSheetData?.type; // Only used for SectionTabContainer

    const [isLoading, setIsLoading] = useState(false);
    const [reducerState, dispatch] = useReducer(BuyPlanReducer, initialState);
    useBackButton(() => {
        onBackPress(props.navigation);
    });

    useEffect(() => {
        rbSheet.current.open();
        getBottomSheetData();
        triggerScreenViewEvent();
        if (Platform.OS === PLATFORM_OS.android) {
            chatBrigeRef.hideLoader();
        }
    }, []);

    const triggerScreenViewEvent = () => {
        switch (bottomSheetType) {
            case ButtonsAction.SHOW_SECTION_BOTTOM_SHEET:
                let eventData = {...eventsData};
                logWebEnageEvent(WebEngageEventsName.CLAIM_PROCESS_SCREENVIEW, eventData);
                break;
            default:
                break;
        }
    };

    const getBottomSheetData = () => {
        if (!bottomSheetType) {
            return null;
        }
        switch (bottomSheetType) {
            case ButtonsAction.SHOW_SECTION_BOTTOM_SHEET:
                eventsData.Location =  WebEngageEventsName.HOW_CLAIM_WORKS
                cdnDataWithURL();
                break;
            case ButtonsAction.SHOW_TESTIMONIAL_BOTTOM_SHEET:
            case ButtonsAction.SHOW_TESTIMONIAL_READ_MORE_BOTTOM_SHEET:
                fetchTestimonialData();
                break;
            case ButtonsAction.SHOW_GEDGETS_BOTTOM_SHEET:
                fetchGadgetData(bottomSheetData?.key);
                break;
            case ButtonsAction.SHOW_SERVICE_BOTTOM_SHEET:
                fetchProductServiceData(bottomSheetData?.key);
                break;

            case ButtonsAction.SHOW_MORE_SOD_PRODUCTS:
                dispatch({ type: ActionTypes.REQUEST_MORE_SOD_PRODUCTS, data: bottomSheetData ?? {} });
                break;
            default:
                break;
        }
    };

    const cdnDataWithURL = () => {
        setIsLoading(true);
        getDataFromCDNWithURL(bottomSheetData?.url, (response, error) => {
            if (error) {
                if (MAX_API_COUNT <= 3) {
                    ++MAX_API_COUNT;
                    cdnDataWithURL();
                }
                return;
            }
            dispatch({type: ActionTypes.REQUEST_BottomSheet_SCREEN_DATA, data: response.data ?? [], pageVariationId: response?.pageVariationId ?? ""});
            eventsData.VariationId = response?.pageVariationId ?? ""
            setIsLoading(false);
        });
    };

    const fetchTestimonialData = () => {
        let cardData = [{
            type: "TestimonialListContainer",
            data: bottomSheetData?.loadedData,
            singleItemData: bottomSheetData?.singleItemData,
            extraProps: bottomSheetData?.extraProps,
            nextPage: bottomSheetData?.nextPage,
            pageSize: bottomSheetData?.pageSize,
            showSingleReview: bottomSheetData?.showSingleReview,
            showReadMore: bottomSheetData?.showReadMore,
            button: bottomSheetData?.button,

            header: {
                value: TESTIMONIAL_LIST_TITLE,
                isHTML: true,
            },
        }];

        dispatch({type: ActionTypes.REQUEST_BottomSheet_SCREEN_DATA, data: cardData ?? []});
    };


    const fetchGadgetData = (key) => {
        getFirebaseData(key, data => {
            let gadgets = data.find(item => {
                return (item.category === bottomSheetData.category && item.serviceType === bottomSheetData.service);
            });
            let cardData = [{
                type: "GadgetListComponent",
                data: gadgets,
                onItemClick: (dataItem) => {
                    if (bottomSheetData?.category && bottomSheetData?.service && bottomSheetData?.serviceName) {
                        buyPlanJourney.category = bottomSheetData?.category;
                        buyPlanJourney.service = bottomSheetData?.service;
                        buyPlanJourney.serviceName = bottomSheetData?.serviceName;
                    }
                    if (bottomSheetData?.screenTitle) {
                        buyPlanJourney.screenTitle = bottomSheetData?.screenTitle;
                    }
                    buyPlanJourney.product = dataItem;
                    let params = {
                        data: {
                            buyPlanJourney: buyPlanJourney,
                        },
                    };
                    handleBuyPlanBtnActions(ButtonsAction.OPEN_SERVICE_DETAIL, params);
                },
            }];
            dispatch({type: ActionTypes.REQUEST_BottomSheet_SCREEN_DATA, data: cardData ?? []});
        });
    };

    const fetchProductServiceData = (key) => {
        getFirebaseData(key, data => {
            let services = data.find(item => {
                return (item.code === bottomSheetData.code);
            });
            let cardData = [{
                type: "GadgetListComponent",
                data: services,
                onItemClick: (dataItem) => {
                    if (bottomSheetData?.category && dataItem?.name && dataItem?.serviceType) {
                        buyPlanJourney.category = bottomSheetData?.category;
                        buyPlanJourney.service = dataItem?.serviceType;
                        buyPlanJourney.serviceName = dataItem?.name;
                    }
                    if (dataItem?.screenTitle) {
                        buyPlanJourney.screenTitle = dataItem?.screenTitle;
                    }
                    let productData = {
                        code: null,
                        brands: null,
                        invoiceDaysLimit: 0,
                        name: null,
                    }
                    productData.code = bottomSheetData?.code
                    productData.brands = dataItem.brands
                    productData.invoiceDaysLimit = dataItem?.invoiceDaysLimit ?? 0
                    productData.name = bottomSheetData?.name

                    buyPlanJourney.product = productData;
                    let params = {
                        data: {
                            buyPlanJourney: buyPlanJourney,
                        },
                    };
                    handleBuyPlanBtnActions(ButtonsAction.OPEN_SERVICE_DETAIL, params);
                },
            }];
            dispatch({type: ActionTypes.REQUEST_BottomSheet_SCREEN_DATA, data: cardData ?? []});
        });
    }

    return (
        <View style={{flex: 1}}>
            <AutoHeightRBSheetV2
                ref={rbSheet}
                closeOnSwipeDown={false}
                duration={250}
                customStyles={{
                    container: {
                        borderTopLeftRadius: 8,
                        borderTopRightRadius: 8,
                        maxHeight: Dimensions.get("screen").height / 1.2,
                    },
                }}
                onClose={() => {
                    if (store.getState().nav.routes.last()?.routeName == "BottomSheetScreen") {
                        onBackPress(props.navigation);
                    }
                }}
            >
                <TouchableOpacity style={styles.close} onPress={() => {
                    rbSheet.current.close();
                }}>
                    <Image source={images.closeSheet} style={styles.closeIcon}/>
                </TouchableOpacity>
                {bottomSheetData?.bottomSheetTitle ? <Text style={styles.bottomSheetTitle}>{bottomSheetData?.bottomSheetTitle}</Text> : null}
                {isLoading
                    ? <View style={{maxHeight: 100, marginVertical: spacing.spacing20}}><Loader
                        isLoading={isLoading}/></View>
                    : getBottomSheetView(bottomSheetType, reducerState.bottomSheetData, {
                        location: eventsData?.Location,
                        category: eventsData?.Category,
                        service: eventsData?.Service,
                        product: eventsData?.Product,
                        eventsData: eventsData,
                        type: contentType,
                        navigation: bottomSheetData?.navigation,
                        rbSheetRef: rbSheet,
                        initialSelectedIndex: bottomSheetData.initialSelectedIndex ?? 0,
                        bottomSheetData: bottomSheetData,
                        reducerState: servicePageReducer,
                        buyPlanJourney: buyPlanJourney,
                        eventPageVariationId: reducerState.eventPageVariationId
                    })
                }
            </AutoHeightRBSheetV2>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    close: {
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "flex-end",
        marginBottom: spacing.spacing8,
    },
    closeIcon: {
        height: 48,
        width: 48,
        resizeMode: "contain",
    },
    bottomSheetTitle: {
        ...TextStyle.text_16_bold,
        color: colors.charcoalGrey,
        marginHorizontal: spacing.spacing20,
    },
});

export default BottomSheetScreen;
