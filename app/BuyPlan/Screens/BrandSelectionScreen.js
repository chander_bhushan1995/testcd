import React, {useEffect, useReducer, useState} from "react";
import BuyPlanReducer, {initialState} from "../Helpers/BuyPlanReducer";
import {fetchBrands} from "../../network/APIHelper";
import * as ActionTypes from "../../Constants/ActionTypes";
import {REQUEST_BRANDS} from "../../Constants/ActionTypes";
import {FlatList, Image, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import colors from "../../Constants/colors";
import spacing from "../../Constants/Spacing";
import images from "../../images/index.image";
import InputBox from "../../Components/InputBox";
import {AppStringConstants} from "../../Constants/AppStringConstants";
import {TextStyle} from "../../Constants/CommonStyle";
import GadgetItemComponent from "../Components/GadgetItemComponent";
import Loader from "../../Components/Loader";
import {onBackPress} from "../Helpers/BuyPlanUtils";
import {useBackButton} from "../Helpers/CustomHooks";


const BrandSelectionScreen = (props) => {
    const [reducerState, dispatch] = useReducer(BuyPlanReducer, initialState);
    const buyPlanJourney = props?.navigation?.state?.params?.buyPlanJourney
    const [isLoading, setIsLoading] = useState(true);

    useBackButton(() => {
        if (props?.navigation) {
            props.navigation.pop()
        }
        return true;
    })

    useEffect(() => {
        fetchProductBrands()
    }, [])

    const fetchProductBrands = () => {
        setIsLoading(true)
        fetchBrands(buyPlanJourney.category,buyPlanJourney.product?.code, buyPlanJourney.service, (data, error) => {
            if (error || !data) {
                alert(error)
                return;
            }
            dispatch({type: REQUEST_BRANDS, data: data, productCode: buyPlanJourney?.product?.code})
            setIsLoading(false)
        })
    }

    const getPopularBrandComponent = () => {
        return (reducerState.brandUiData?.popularBrands && reducerState.brandUiData.popularBrands.length > 0)
            ? <View style={styles.popularBrandContainer}>
                <Text style={styles.popularBrandsTitle}>{AppStringConstants.POPULAR_BRANDS}</Text>
                <View style={{flexDirection: 'row', marginTop: spacing.spacing16, marginLeft: spacing.spacing16}}>
                    {
                        reducerState.brandUiData.popularBrands.map(item => {
                            let componentItem = {
                                name: item.brandName,
                                data: item.brandData
                            }
                            return <TouchableOpacity style={{alignItems: 'center', marginRight: spacing.spacing12,}}
                                                     onPress={() => {
                                                         if (props?.navigation?.state?.params?.onBrandSelection) {
                                                             props.navigation.state.params.onBrandSelection(componentItem)
                                                         }
                                                         props.navigation?.pop()
                                                     }}>
                                <View View style={styles.popularBrandBox}>
                                    <Image source={{uri: item.image}} style={styles.popularBrandImage}/>
                                </View>
                                <Text style={styles.popularBrandText}>{componentItem.name}</Text>

                            </TouchableOpacity>
                        })
                    }
                </View>
            </View>
            : null
    }

    const getOtherBrandComponents = () => {
        return <View style={{marginTop: spacing.spacing20}}>
            {!reducerState.brandUiData?.searchedKey
                ? <Text style={styles.popularBrandsTitle}>{AppStringConstants.OTHER_BRANDS}</Text>
                : null
            }
            {reducerState.brandUiData?.filteredBrands ?
                <FlatList style={{marginTop: spacing.spacing8}}
                          data={reducerState.brandUiData?.filteredBrands}
                          renderItem={({item}) => {
                              let componentItem = {
                                  name: item.paramValue,
                                  data: item
                              }
                              return <GadgetItemComponent data={componentItem}
                                                          onPress={(item) => {
                                                              if (props?.navigation?.state?.params?.onBrandSelection) {
                                                                  props.navigation.state.params.onBrandSelection(item)
                                                              }
                                                              props.navigation?.pop()
                                                          }}/>
                          }}/> : null
            }

        </View>
    }

    if(isLoading){
        return <Loader isLoading={isLoading} loaderStyle={{
            backgroundColor: "transparent",
            position: "absolute",
            top: 0,
            width: "100%",
        }}/>
    }

    return (<SafeAreaView style={styles.screen}>
        <TouchableOpacity style={styles.close} onPress={() => {
            if (props?.navigation) {
                props.navigation.pop()
            }
        }}>
            <Image source={images.closeSheet} style={styles.closeIcon}/>
        </TouchableOpacity>
        <InputBox
            containerStyle={{margin: spacing.spacing16}}
            placeholder={AppStringConstants.SEARCH}
            multiline={false}
            numberOfLines={1}
            autoFocus={false}
            errorMessage=""
            onChangeText={inputMessage => {
                dispatch({type: ActionTypes.FILTER_BRANDS, key: inputMessage});
            }}
            editable={true}>
        </InputBox>
        <ScrollView style={styles.scrollViewStyle}>
            {!reducerState.brandUiData?.searchedKey ? getPopularBrandComponent() : null}
            {getOtherBrandComponents()}
        </ScrollView>



    </SafeAreaView>)
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: colors.white,
        paddingTop: spacing.spacing16
    },
    close: {
        height: 48,
        width: 48,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'flex-end',
        marginTop: spacing.spacing12
    },
    closeIcon: {
        height: 48,
        width: 48,
        resizeMode: 'contain',
    },
    scrollViewStyle: {
        flex: 1,
        paddingBottom: spacing.spacing32,
        backgroundColor: colors.white,
    },
    popularBrandContainer: {
        marginVertical: spacing.spacing20,
    },
    popularBrandsTitle: {
        ...TextStyle.text_10_bold,
        marginHorizontal: spacing.spacing16
    },
    popularBrandBox: {
        borderWidth: spacing.spacing1,
        borderColor: colors.grey,
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
        width: 73,
        height: 73
    },
    popularBrandImage: {
        width: 50,
        height: 50,
        resizeMode: 'contain'
    },
    popularBrandText: {
        ...TextStyle.text_14_normal_charcoalGrey,
        marginTop: spacing.spacing8,

    }
});
export default BrandSelectionScreen
