import React, {useEffect, useRef, useState} from "react";
import {NativeEventEmitter, NativeModules, Platform, StyleSheet, View} from "react-native";
import colors from "../../Constants/colors";
import {style} from "../../CardManagement/CardListing/CardListingStyles";
import ScrollableTabView, {ScrollableTabBar} from "../../scrollableTabView";
import PersonalElectronicsTab from "./PersonalElectronicsTab";
import {getCDNDataVersioning, getFirebaseData} from "../../commonUtil/AppUtils";
import {Category, FINANCE, FIREBASE_KEYS, PLATFORM_OS, QUICK_REPAIR} from "../../Constants/AppConstants";
import {CATEGORY, VARIATIONID} from "../../Constants/WebengageAttrKeys";
import {logWebEnageEvent} from "../../commonUtil/WebengageTrackingUtils";
import {BUY_SUBTAB} from "../../Constants/WebengageEvents";
import {getBuySubtabIndex} from "../Helpers/BuyPlanUtils";

const nativeBridgeRef = NativeModules.ChatBridge;
const eventEmitter = new NativeEventEmitter(nativeBridgeRef);

const BuyPlanComponent = props => {

    const tabRef = useRef(null);
    const [tabBarData, setTabBarData] = useState(null);
    const [initialPageIndex, setInitialPageIndex] = useState(0);
    const pageVariationMapping = {}

    useEffect(() => {

        getFirebaseData(FIREBASE_KEYS.BUY_TAB_JSON, (data) => {
            setTabBarData(data);
        });

        eventEmitter.addListener("ChangeBuyPlanCategory", params => { // for first time when app already initialized and deeplink opened
            if (tabRef?.current) {
                tabRef?.current?.goToPage(getBuySubtabIndex(params?.category));
            }
        });

        nativeBridgeRef.getBuySubtabSelectedIndex((params) => { // for first time when app intializes from deeplink
            setInitialPageIndex(getBuySubtabIndex(params?.selectedCategory));
            tabRef?.current?.goToPage(getBuySubtabIndex(params?.selectedCategory));

        });

        return () => {
            eventEmitter.removeListener("ChangeBuyPlanCategory", () => {
            });
        };
    }, []);

    const selectIndexAction = (index) => {
        let tabItem = tabBarData[index];
        let eventData = {};
        eventData[CATEGORY] = tabItem.category;
        if (tabItem.category === Category.finance) {
            eventData[CATEGORY] = FINANCE;
        } else if (tabItem.category === Category.quickRapairs) {
            eventData[CATEGORY] = QUICK_REPAIR;
        }
        eventData[VARIATIONID] = pageVariationMapping[tabItem.category] ?? ""
        logWebEnageEvent(BUY_SUBTAB, eventData);
    };

    return (
        <View style={styles.container}>
            {tabBarData
                ? <ScrollableTabView
                    initialPage={initialPageIndex}
                    prerenderingSiblingsNumber={4}
                    locked={true}
                    onChangeTab={({i, from}) => {
                        if (i != from) {
                            selectIndexAction(i)
                        }
                    }}
                    ref={tabRef}
                    renderTabBar={() => <ScrollableTabBar tabsContainerStyle={style.tabContainerView}
                                                          underlineStyle={style.tabBarUnderlineView}
                                                          textStyle={styles.tabBarTextStyle}
                                                          activeTextColor={colors.blue028}
                                                          hasShadow={true}
                                                          inactiveTextColor={colors.grey}/>}
                >
                    {
                        tabBarData.map((tabItem, index) => {
                            return (<PersonalElectronicsTab key={index.toString()}
                                                            tabLabel={tabItem.name}
                                                            category={tabItem.category}
                                                            pageVariationMapping={pageVariationMapping}
                                                            initialPageIndex={initialPageIndex}
                                                            selectIndexAction={selectIndexAction}
                                                            currentIndex={index}
                            />);
                        })
                    }

                </ScrollableTabView>
                : null
            }
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: colors.white,
    },
    tabBarTextStyle: {
        fontSize: 14,
        fontFamily: "Lato-Regular",
        lineHeight: 20,
    },
});


// expected output: Array [3, 42, "foo"]


export default BuyPlanComponent;
