import colors from '../../Constants/colors';
import React, {useContext, useEffect, useState} from 'react';
import {
    BackHandler,
    Dimensions,
    Image,
    NativeModules,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import spacing from '../../Constants/Spacing';
import images from '../../images/index.image';
import InputBox from '../../Components/InputBox';
import {EditProfileConstants, userNameRegex} from '../../EditDetails/EditProfileUtils';
import {
    COMING_SOON,
    ENTER_MOBILE, EXPLORE_OTHER_SERVICES,
    FULL_NAME,
    NOTIFY_ME,
    NOTIFY_SUCCESS, SOMETHING_WRONG,
} from '../../Constants/AppConstants';
import ButtonWithLoader from '../../CommonComponents/ButtonWithLoader';
import {TextStyle} from '../../Constants/CommonStyle';
import {notifyMe} from '../../network/APIHelper';
import {stringConstants} from '../../CardManagement/DataLayer/StringConstants';
import {CommonContext} from '../../CommonComponents/Index/index.common';

const nativeBridgeRef = NativeModules.ChatBridge;

const NotifyMeFormScreen = props => {

    const [mobileNumber, setMobileNumber] = useState({mobile: '', error: null});
    const [customerName, setCustomerName] = useState({name: '', error: null});
    const [isLoading, setIsLoading] = useState(false);
    const [isButtonEnabled, setButtonEnabled] = useState(false);
    const [isNotifiedSuccess, setIsNotifiedSuccess] = useState(false);
    const campaignId = props.navigation.getParam('campaignId');
    const pinCode = props.navigation.getParam('pinCode');

    const {Alert} = useContext(CommonContext);

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', onBackPress);

        return () => {
            BackHandler.removeEventListener('hardwareBackPress', onBackPress);
        };
    }, []);

    useEffect(() => {
        validFields();
    }, [customerName, mobileNumber]);

    const onBackPress = () => {
        isNotifiedSuccess ? nativeBridgeRef.goBack() : props.navigation.pop();
    };

    const validFields = () => {
        if (userNameRegex.test(customerName.name.trim().toString())) {
            if (mobileNumber.mobile.length === 10 && customerName.name.length > 0) {
                setButtonEnabled(true);
            } else {
                setButtonEnabled(false);
            }
        } else {
            setButtonEnabled(false);
        }
    };

    const onNotifyMe = () => {
        setIsLoading(true);
        notifyMe(customerName.name, mobileNumber.mobile, pinCode, null,campaignId, (response, error) => {
            setIsLoading(false);
            if (error) {
                Alert.showAlert('Alert', SOMETHING_WRONG, {text: stringConstants.ok});
            } else {
                if (response.MESSAGE.error) {
                    Alert.showAlert('Alert', response.MESSAGE.error[0]?.message ?? SOMETHING_WRONG, {text: stringConstants.ok});
                } else {
                    setIsNotifiedSuccess(true);
                }
            }
        });
    };

    const onExploreOtherServices = () => {
        nativeBridgeRef.goBack();
    };

    return (
        <View style={styles.screen}>
            <TouchableOpacity activeOpacity={1} onPress={onBackPress}>
                <Image style={styles.crossIcon} source={images.ic_blackClose}/>
            </TouchableOpacity>
            <ScrollView bounces={false}>
                {isNotifiedSuccess
                    ? <View>
                        <Image source={images.excellentSuccess} style={styles.notifySuccessImage}/>
                        <Text style={styles.descriptionSuccess}>{NOTIFY_SUCCESS}</Text>
                    </View>
                    : <>
                        <Image source={images.pinCodeNotify} style={styles.notifyImage}/>
                        <Text style={styles.description}>{COMING_SOON}</Text>
                        <InputBox
                            inputHeading={EditProfileConstants.INPUT_FIELDS_HEADING.MOBILE_NUMBER}
                            multiline={false}
                            containerStyle={{marginTop: spacing.spacing20}}
                            numberOfLines={1}
                            maxLength={10}
                            keyboardType={'numeric'}
                            autoFocus={true}
                            placeholder={ENTER_MOBILE}
                            errorMessage={mobileNumber.error}
                            onChangeText={(text) => {
                                setMobileNumber(({mobile: text, error: ''}));
                            }
                            }
                            editable={true}/>

                        <InputBox
                            maxLength={EditProfileConstants.INPUTBOX_MAX_MIN_LENGTH.MAXLENGTH_200}
                            inputHeading={FULL_NAME}
                            containerStyle={{marginTop: spacing.spacing20}}
                            placeholder={''}
                            isMultiLines={false}
                            value={customerName?.name}
                            errorMessage={customerName.error}
                            editable={true}
                            onChangeText={(text) => {
                                text = text.replace(/\s{2,}/g, ' ');
                                setCustomerName({name: text, error: ''});
                            }}
                        />
                    </>}
            </ScrollView>
            <View style={{paddingVertical: 20}}>
                <ButtonWithLoader
                    isLoading={isLoading}
                    enable={isNotifiedSuccess ? true : isButtonEnabled}
                    Button={{
                        text: isNotifiedSuccess ? EXPLORE_OTHER_SERVICES : NOTIFY_ME,
                        onClick: () => {
                            isNotifiedSuccess ? onExploreOtherServices() : onNotifyMe();
                        },
                    }
                    }/>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: colors.white,
        paddingHorizontal: spacing.spacing24,
    },
    crossIcon: {
        marginTop: spacing.spacing40,
        height: 30,
        width: 30,
        resizeMode: 'contain',
        alignSelf: 'flex-end',
    },
    description: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        textAlign: 'center',
    },
    descriptionSuccess: {
        ...TextStyle.text_16_normal,
        color: colors.charcoalGrey,
        textAlign: 'center',
    },
    notifyImage: {
        width: Dimensions.get('screen').width * 0.60,
        resizeMode: 'contain',
        marginTop: spacing.spacing16,
        alignSelf: 'center',
    },
    notifySuccessImage: {
        width: Dimensions.get('screen').width * 0.13,
        resizeMode: 'contain',
        marginTop: Dimensions.get('screen').width * 0.13,
        alignSelf: 'center',
    },
});

export default NotifyMeFormScreen;
