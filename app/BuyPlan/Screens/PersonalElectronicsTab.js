import React, {useEffect, useReducer, useRef, useState} from "react";
import {FlatList, NativeEventEmitter, NativeModules, Platform, SafeAreaView, StyleSheet} from "react-native";
import colors from "../../Constants/colors";
import Loader from "../../Components/Loader";
import {getTabsData} from "../../network/APIHelper";
import BuyPlanReducer, {initialState} from "../Helpers/BuyPlanReducer";
import * as ActionTypes from "../../Constants/ActionTypes";
import { getComponent } from "../../Catalyst/components/ComponentsFactory";
import { CATEGORY_SCREEN } from "../../Constants/EventLocation";
import {PLATFORM_OS} from "../../Constants/AppConstants";
const nativeBridgeRef = NativeModules.ChatBridge;
const eventEmitter = new NativeEventEmitter(nativeBridgeRef);

const PersonalElectronicsTab = props => {

    const category = props.category;
    const [isLoading, setIsLoading] = useState(true);
    const [reducerState, dispatch] = useReducer(BuyPlanReducer, initialState);
    const flatListRef = useRef(null);
    const  pageVariationMapping = props.pageVariationMapping
    const initialPageIndex = props.initialPageIndex
    const currentIndex = props.currentIndex
    const selectIndexAction = props.selectIndexAction ?? null

    useEffect(() => {
        if (props.category) {
            getTabData();
        } else {
            setIsLoading(false);
        }
    }, []);

    useEffect(() => {
        if(reducerState?.eventPageVariationId) {
            pageVariationMapping[category] = reducerState?.eventPageVariationId
            if(currentIndex == initialPageIndex){
                if(selectIndexAction) {
                    selectIndexAction(initialPageIndex)
                }
            }
        }
    }, [reducerState?.eventPageVariationId]);

    const getTabData = () => {
        setIsLoading(true);
        getTabsData(category, (response, error) => {
            setIsLoading(false);
            if (error) {
                return;
            } else {
                dispatch({
                    type: ActionTypes.REQUEST_SCREEN_COMPONENTS,
                    data: { components: response.data ?? [] },
                    pageVariationId: response.pageVariationId,
                });
            }
        });
    };


    const scrollToIndex = (index) => {
        flatListRef.current.scrollToIndex({ animated: true, index: index });
    };

    const _renderComponent = ({ item, index }) => {

        const Component = getComponent(item.type);
        return <Component data={item}
                          extraProps={{
                              scrollToIndex: scrollToIndex,
                              location: CATEGORY_SCREEN,
                              category: category,
                              eventPageVariationId: reducerState.eventPageVariationId
                          }} />;
    };

    return (
        <SafeAreaView style={styles.screen}>
            {isLoading
                ? <Loader isLoading={isLoading} />
                : <FlatList
                    ref={flatListRef}
                    data={reducerState.screenComponents}
                    renderItem={_renderComponent}
                    showsVerticalScrollIndicator={false}
                    bounces={true}
                    keyExtractor={(item, index) => {
                        return index.toString();
                    }}
                    extraData={reducerState}
                />
            }
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: colors.color_F3F2F7,
    },
});

export default PersonalElectronicsTab;
