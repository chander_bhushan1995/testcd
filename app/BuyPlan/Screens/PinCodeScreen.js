import React, { useContext, useEffect, useRef, useState } from "react";
import {
    BackHandler,
    Dimensions,
    Image,
    NativeEventEmitter,
    NativeModules,
    Platform,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    View,
} from "react-native";
import spacing from "../../Constants/Spacing";
import images from "../../images/index.image";
import { TextStyle } from "../../Constants/CommonStyle";
import colors from "../../Constants/colors";
import {
    CAN_NOTIFY,
    CHECK_SERVICEABILITY,
    Destination,
    FIREBASE_KEYS,
    FlowSequence,
    NOT_SERVICING,
    NOTIFY_ME,
    PINCODE_DESCRIPTION,
    PINCODE_TITLE,
    SERVED,
    SOMETHING_WRONG,
} from "../../Constants/AppConstants";
import InputBox from "../../Components/InputBox";
import ButtonWithLoader from "../../CommonComponents/ButtonWithLoader";

import { getSRsReportForPinCode, notifyMe, verifyForPin } from "../../network/APIHelper";
import { stringConstants } from "../../CardManagement/DataLayer/StringConstants";
import { CommonContext } from "../../CommonComponents/Index/index.common";
import {APIData} from '../../../index';
import BackButton from "../../CommonComponents/NavigationBackButton";
import { getFirebaseData } from "../../commonUtil/AppUtils";
import { checkLocationPermission } from "../../commonUtil/LocationPermissionUtils";
import { onBackPress } from "../Helpers/BuyPlanUtils";
import { useBackButton } from "../Helpers/CustomHooks";
import { logWebEnageEvent } from "../../commonUtil/WebengageTrackingUtils";
import { PINCODE_SERVICEABILITY } from "../../Constants/WebengageEvents";
import { APPLIANCE, PINCODE, SERVICEABLE, TYPE } from "../../Constants/WebengageAttrKeys";
import DismissKeyboardView from "../../Components/DismissKeyboardView";

const nativeBridgeRef = NativeModules.ChatBridge;

let serviceRequestSourceType = null;
let firebaseKey = FIREBASE_KEYS.HA_CampaignId;
let campaignId = "";

const chatBridgeRef = NativeModules.ChatBridge;
const eventEmitter = new NativeEventEmitter(chatBridgeRef);

const PinCodeScreen = props => {

    const buyPlanJourney = props.navigation.getParam("buyPlanJourney");
    const [enteredPinCode, setEnteredPinCode] = useState(buyPlanJourney?.renewalData?.pinCode ?? "");
    const [isPrimaryBtnEnable, setPrimaryBtnEnabled] = useState(buyPlanJourney?.renewalData?.pinCode ? true : false);
    const [isLoading, setIsLoading] = useState(false);
    const [isShowNotifyMe, setShowNotifyMe] = useState(false);
    const [errorMessage, setErrorMessage] = useState(null);
    const [userCurrentLocation, setCurrentLocation] = useState(null);
    const [totalServicedAppl, setTotalServicedAppl] = useState(null);
    const textInputReference = useRef(null);
    const productCode = props.navigation.getParam("productCode");
    const services = props.navigation.getParam("services");
    const category = props.navigation.getParam("category");
    const isCameFromDeeplink = props.navigation.getParam("isCameFromDeeplink");
    const eventLocation = props.navigation.getParam("eventLocation");
    const position = props.navigation.getParam("position");
    const totalSpentTimeOnScreen = props.navigation.getParam("totalSpentTimeOnScreen");
    const selectedServiceRequestType = props.navigation.getParam("serviceRequestType");
    const productName = props.navigation.getParam("productName");
    const isLoggedIn = APIData.customer_id > 0 ? true : false;
    const destination = props.navigation.getParam("destination") ?? Destination.getDestination(buyPlanJourney?.category, buyPlanJourney?.service);

    useBackButton(() => onBackPress(props.navigation));
    const { Alert } = useContext(CommonContext);

    useEffect(() => {
        switch (destination) {
            case Destination.sod:
            case Destination.sodQTY:
                serviceRequestSourceType = "SOD";
                firebaseKey = FIREBASE_KEYS.QR_CampaignId;
                GetDeviceLocation(() => {
                });
                break;
            default:
                if (!enteredPinCode) {
                    textInputReference.current.focus();
                }
                break;
        }

        //below listener is specific to iOS to get lat long
        eventEmitter.addListener("willReceiveLocation", (params) => {
            let location = { lat: params.latitude, lng: params.longitude, pincode: params.pincode };
            setCurrentLocation(location);
        });

        getFirebaseData(firebaseKey, (data) => {
            campaignId = data;
        });

        return () => { // clean up
            eventEmitter.removeListener("willReceiveLocation", () => {
            });
        };


    }, []);

    useEffect(() => {
        if (userCurrentLocation && userCurrentLocation.pincode) {
            if (!textInputReference.current.isFocused() && enteredPinCode.length <= 0) {
                setEnteredPinCode(userCurrentLocation.pincode);
                getReportForPincode(userCurrentLocation.pincode);
                setPrimaryBtnEnabled(true);
            }
        }
    }, [userCurrentLocation]);

    /**
     * method to get location (lat,lng values) in callback
     * @param callback
     */
    const GetDeviceLocation = (callback) => {
        if (Platform.OS === "android") {
            checkLocationPermission(Alert.ref, onPermissionGrant = () => {
                chatBridgeRef.getLocation((location) => {
                    callback(location);
                });
            });
        } else { // here we are passing null in callback location will be received within event listener ( willReceiveLocation )
            chatBridgeRef.getUserLocation((params) => {
            });
        }
    };

    const routeToSOD = () => {
        let params = {
            "serviceRequestType": selectedServiceRequestType,
            "services": services,
            "productCode": productCode,
            "productName": productName,
            "pinCode": enteredPinCode,
            "totalSpentTimeOnScreen": totalSpentTimeOnScreen,
            "position": position,
            "isCameFromDeeplink": isCameFromDeeplink,
            "eventLocation": eventLocation,
            "category": category,
        };
        getFirebaseData(FIREBASE_KEYS.MODULE_FLOW_SEQUENCING, (data) => {
            if (data && data?.sodFlowSequencing.screen_1 === FlowSequence.SODFlow.PINCODE_SCREEN) {
                params.destination = Destination.sod,
                    props?.navigation?.navigate("SODPlanDetailScreen", params);
            } else {
                params.destination = Destination.sodQTY,
                    props?.navigation?.navigate("SODProductServiceScreen", params);
            }
        });
    };

    const verifyPinCode = () => {
        let eventData = {};
        setIsLoading(true);
        verifyForPin(enteredPinCode, serviceRequestSourceType, (response, error) => {
            setIsLoading(false);
            eventData[SERVICEABLE] = response?.data?.isPincodeServicable === "Y" ? "YES" : "NO";
            eventData[PINCODE] = enteredPinCode;
            eventData[TYPE] = (destination === Destination.sod || destination === Destination.sodQTY) ? "Service On Demand" : "HomeAssist";
            eventData[APPLIANCE] = productCode;
            logWebEnageEvent(PINCODE_SERVICEABILITY, eventData);
            if (error) {
                if (error?.message) {
                    setErrorMessage(error?.message);
                } else {
                    Alert.showAlert("Alert", error?.error[0]?.message ?? SOMETHING_WRONG, { text: stringConstants.ok });
                }
            } else {
                if (response?.data?.isPincodeServicable === "Y") {
                    setErrorMessage(null);
                    if (destination === Destination.sod) { // handle for sod
                        routeToSOD();
                    } else { // handle for non sod // TODO: - HANDLE IF PINCODE SERVICEABLE
                        buyPlanJourney.pinCode = enteredPinCode;
                        buyPlanJourney.cityCode = response?.data?.cityCode ?? "";
                        buyPlanJourney.stateCode = response?.data?.stateCode ?? "";
                        if (destination === Destination.haSOP || destination === Destination.whc) {
                            let routes = Destination.getRoutesForDestination(destination);
                            if (routes.includes(props.navigation.state.routeName)) {
                                let index = routes.indexOf(props.navigation.state.routeName);
                                let routeName = routes[index + 1];
                                if (buyPlanJourney.renewalData) {
                                    routeName = routes[index + 2];
                                }
                                props.navigation.navigate(routeName, {
                                    buyPlanJourney: buyPlanJourney,
                                    destination: destination,
                                });
                            }
                        }
                    }
                } else {
                    if (!isLoggedIn) {
                        props.navigation.navigate("NotifyMeFormScreen", {
                            pinCode: enteredPinCode,
                            campaignId: campaignId,
                        });
                        return;
                    }
                    setShowNotifyMe(true);
                }
            }
        });
    };

    const createLead = () => {
        setIsLoading(true);
        notifyMe(APIData.user_Name, APIData.mobile_number, enteredPinCode, APIData.user_Email, campaignId, (response, error) => {
            setIsLoading(false);
            if (error) {
                Alert.showAlert("Alert", error?.error[0]?.message ?? SOMETHING_WRONG, { text: stringConstants.ok });
            } else {
                if (response.MESSAGE.error) {
                    Alert.showAlert("Alert", response.MESSAGE.error[0]?.message ?? SOMETHING_WRONG, { text: stringConstants.ok });
                } else {
                    nativeBridgeRef.goBack();
                }
            }
        });
    };

    const getReportForPincode = (pinCode) => {
        getSRsReportForPinCode(pinCode, serviceRequestSourceType, "HA", (response, error) => {
            if (response) {
                setTotalServicedAppl(response?.data);
            }
        });
    };

    return (
        <ScrollView style={styles.screen} bounces={false} keyboardShouldPersistTaps='never'>
            <Image source={images.pinCode} style={styles.homePinImage} />
            <Text style={styles.heading}>{PINCODE_TITLE}</Text>
            <Text style={styles.description}>{PINCODE_DESCRIPTION}</Text>

            <InputBox
                textInputRef={(ref) => {
                    textInputReference.current = ref;
                }}
                blurOnSubmit={false}
                containerStyle={styles.textInputContainer}
                inputHeading={""}
                multiline={false}
                placeholder={"Enter 6 digit pincode"}
                numberOfLines={1}
                maxLength={6}
                autofocus={false}
                keyboardType={"numeric"}
                value={enteredPinCode}
                rightImage={isShowNotifyMe ? images.ic_edit_transparent : null}
                onChangeText={inputMessage => {
                    setEnteredPinCode(inputMessage);
                    setPrimaryBtnEnabled(inputMessage.length >= 6);
                }}
                editable={!isShowNotifyMe}
                errorMessage={errorMessage}
                onRightImageTapped={() => {
                    setShowNotifyMe(false);
                }}
                onFocus={() => {
                    setErrorMessage(null);
                }}
            >

            </InputBox>

            {isShowNotifyMe
                ? <View>
                    <Text style={styles.labelError}>{NOT_SERVICING}</Text>
                    <Text style={styles.notifyTextLabel}>{CAN_NOTIFY}</Text>
                </View>
                : null}

            <View style={styles.primaryButton}>
                <DismissKeyboardView>
                    <ButtonWithLoader
                        isLoading={isLoading}
                        enable={isShowNotifyMe ? true : isPrimaryBtnEnable}
                        Button={{
                            text: isShowNotifyMe ? NOTIFY_ME : CHECK_SERVICEABILITY,
                            onClick: () => {
                                isShowNotifyMe ? createLead() : verifyPinCode();
                                logWebEnageEvent(isShowNotifyMe ? "Notify Me" : "Verify pincode tapped", {});
                            },
                        }
                        } />
                </DismissKeyboardView>
            </View>

            {totalServicedAppl && ((totalServicedAppl?.HA?.completed ?? 0) > 0)
                ? <View style={styles.applianceServedContainer}>
                    <Image source={images.pinCodeRepairs} style={styles.applianceServedImage} />
                    <Text style={styles.applianceServedText}>{SERVED(totalServicedAppl?.HA?.completed)}</Text>
                </View>
                : null}
        </ScrollView>
    );
};

PinCodeScreen.navigationOptions = (navigationData) => {

    let marginTop = 0;

    let buyPlanJourney = navigationData?.navigation?.state?.params?.buyPlanJourney;
    let destination;
    if (buyPlanJourney) {
        destination = Destination.getDestination(buyPlanJourney?.category, buyPlanJourney?.service);
    } else if (navigationData?.navigation?.state?.params?.destination) {
        destination = navigationData?.navigation?.state?.params?.destination;
    }
    if (destination && destination === Destination.sod) {
        marginTop = StatusBar.currentHeight;
    }

    return {
        // header: null,
        // headerMode: 'none',
        // mode: 'modal',
        title: "Pincode",
        headerLeft: <BackButton onPress={() => {
            onBackPress(navigationData.navigation);
        }} />,
        headerStyle: {
            marginTop: marginTop,
        },
    };
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        paddingHorizontal: spacing.spacing16,
        backgroundColor: colors.white,
    },
    crossIcon: {
        marginTop: spacing.spacing40,
        height: 30,
        width: 30,
        resizeMode: "contain",
        alignSelf: "flex-end",
    },
    homePinImage: {
        width: Dimensions.get("screen").width * 0.43,
        marginTop: spacing.spacing8,
        alignSelf: "center",
        resizeMode: "contain",
    },
    heading: {
        ...TextStyle.text_18_bold,
        color: colors.charcoalGrey,
        textAlign: "center",
    },
    description: {
        ...TextStyle.text_14_normal,
        marginTop: spacing.spacing4,
        color: colors.color_979797,
        textAlign: "center",
    },
    primaryButton: {
        marginTop: spacing.spacing32,
    },
    labelError: {
        ...TextStyle.text_14_normal,
        color: colors.inoutErrorMessage,
        textAlign: "center",
    },
    notifyTextLabel: {
        ...TextStyle.text_14_normal,
        marginTop: spacing.spacing4,
        color: colors.color_888F97,
        textAlign: "center",
    },
    applianceServedContainer: {
        paddingHorizontal: spacing.spacing14,
        paddingVertical: spacing.spacing16,
        flexDirection: "row",
        backgroundColor: colors.color_F2F9FE,
        marginTop: spacing.spacing24,
    },
    applianceServedImage: {
        height: 32,
        width: 32,
        resizeMode: "contain",
        marginRight: spacing.spacing8,
    },
    applianceServedText: {
        ...TextStyle.text_12_normal,
        color: colors.charcoalGrey,
        flex: 1,
    },
});

export default PinCodeScreen;
