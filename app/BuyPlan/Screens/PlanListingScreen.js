import React, {useContext, useEffect, useReducer, useState} from "react";
import {
    NativeModules,
    SafeAreaView,
    FlatList, BackHandler,
} from "react-native";
import Loader from "../../Components/Loader";
import BuyPlanReducer, {initialState} from "../Helpers/BuyPlanReducer";
import spacing from "../../Constants/Spacing";

import BackButton from "../../CommonComponents/NavigationBackButton";
import {
    getParamsForPaymentFlow, getPlanListScreenTitle,
    getProductWithServicesList,
    onBackPress,
} from "../Helpers/BuyPlanUtils";

import {
    Destination,
    FIREBASE_KEYS, LoginForScreen, NO_PLANS_FOUND, NumberVerifyAlerts,
    PlanServices, SELECT_ISSUE, SOMETHING_WRONG,
} from "../../Constants/AppConstants";
import {
    fetchSuggestedPlans, getPlanListingFromCDN,
    renewHAMembership,
} from "../../network/APIHelper";
import {getFirebaseData} from "../../commonUtil/AppUtils";
import {stringConstants} from "../../CardManagement/DataLayer/StringConstants";
import {CommonContext} from "../../CommonComponents/Index/index.common";
import {APIData} from '../../../index';
import * as ActionTypes from "../../Constants/ActionTypes";
import AutoHeightBottomSheet, {AutoHeightBottomSheetType} from "../Components/AutoHeightBottomSheet";
import {
    AF_CONTENT_LIST, AF_CONTENT_TYPE,
    AGE,
    BRAND,
    CATEGORY, COVER_AMOUNT, DISCOUNT, FINAL_PREMIUM_AMOUNT, INVOICE_AMOUNT, INVOICE_DATE,
    LOCATION, NO_OF_APPLIANCE,
    PLAN_CODE,
    PLAN_NAME,
    PREMIUM_AMOUNT,
    PRODUCT,
    SERVICE,
} from "../../Constants/WebengageAttrKeys";
import {logAppsFlyerEvent, logWebEnageEvent} from "../../commonUtil/WebengageTrackingUtils";
import {
    AF_LIST_VIEW,
    CONTENT_LIST,
    CONTENT_TYPE,
    NO_PLANS_FOUND_EVENT,
    SEE_ALL_BENEFITS,
    SELECT_PLAN,
} from "../../Constants/WebengageEvents";
import {getComponent} from "../../Catalyst/components/ComponentsFactory";
import {useBackButton} from "../Helpers/CustomHooks";
import {daysDifference, getCurretDateMilliseconds} from "../../commonUtil/DateUtils";
import {formatDateTime} from "../../commonUtil/Formatter";

let apiArray = [];
const nativeBridge = NativeModules.ChatBridge;
let currentStep = 0;
let totalSteps = 0;
let commonEventData = {};

const PlanListingScreen = props => {
    const {Alert, BlockingLoader} = useContext(CommonContext);

    const [isLoading, setIsLoading] = useState(true);
    const [reducerState, dispatch] = useReducer(BuyPlanReducer, initialState);
    const [autoHeightBottomSheet, setAutoHeightBottomSheet] = useState({isVisible: false, data: null, type: null});

    const buyPlanJourney = props.navigation.getParam("buyPlanJourney");
    const destination = props.navigation.getParam("destination") ?? Destination.getDestination(buyPlanJourney?.category, buyPlanJourney?.service);
    const routes = Destination.getRoutesForDestination(destination);
    const category = buyPlanJourney.category;
    const service = buyPlanJourney.service;
    const offer = buyPlanJourney.offer;
    const product = buyPlanJourney.product;
    const renewalData = buyPlanJourney?.renewalData;

    const suggestedPlans = props?.navigation?.getParam("suggestedPlans");

    useBackButton(() => onBackPress(props.navigation));

    useEffect(() => {
        callAllAPI();
    }, [suggestedPlans]);

    useEffect(() => {
        commonEventData[LOCATION] = "Plan Listing Screen";
        commonEventData[CATEGORY] = buyPlanJourney?.category;
        commonEventData[SERVICE] = buyPlanJourney?.service;
        commonEventData[PRODUCT] = buyPlanJourney?.product?.name;
        let currentRouteIndex = routes.indexOf("PlanListingScreen");
        if (destination === Destination.haSOP) {
            currentStep = currentRouteIndex;
            totalSteps = routes.length - 1;
        } else {
            currentStep = currentRouteIndex + 1;
            totalSteps = routes.length;
        }


    }, []);


    const callAllAPI = () => {
        apiArray.push(getPlansTagLine(), callGetPlans(), getDataFromCDN(), getCoverAmountInfo());
        Promise.all(apiArray).then((values) => {
            apiArray = [];
            setIsLoading(false);
            dispatch({
                type: ActionTypes.SHOW_PLAN_LISTING,
                data: {
                    plansTaglineResponse: values[0],
                    plans: values[1],
                    components: values[2],
                    coverAmountInfo: values[3],
                    offerData: offer,
                    category: category,
                    service: service,
                    invoiceValue: buyPlanJourney?.invoiceValue,
                    invoiceRange: buyPlanJourney?.invoiceRange,
                },
            });
            setDefaultRenewalPlan(values[1]);
            let eventDataAppsFlyer = {};
            eventDataAppsFlyer[AF_CONTENT_TYPE] = getProductWithServicesList(destination, Array.isArray(values[1]) ? values[1] : [],[], buyPlanJourney?.product?.name);
            eventDataAppsFlyer[AF_CONTENT_LIST] = (Array.isArray(values[1]) ? values[1] : []).map(plan => plan?.planCode).join(",");
            logAppsFlyerEvent(AF_LIST_VIEW, eventDataAppsFlyer)
        });
    };

    function setDefaultRenewalPlan(planData) {
        if (renewalData) {
            let prevPlan = planData?.filter(data => data?.planCode === renewalData?.prevPlanCode);
            if (prevPlan?.[0]) {
                dispatch({type: ActionTypes.CHANGE_COVER_AMOUNT, data: prevPlan?.[0]?.coverAmount});
            }
        }
    }

    const showAPIError = (message) => {
        Alert.showAlert("Alert", message ?? SOMETHING_WRONG, {
            text: stringConstants.ok,
            onClick: () => {
                onBackPress(props.navigation);
            },
        });
    };

    const getPlansTagLine = async () => {
        return await new Promise((resolve, reject) => {
            getFirebaseData(FIREBASE_KEYS.PLAN_TAGLINE, (data) => {
                if (data) {
                    resolve(data);
                } else {
                    resolve(null);
                }
            });
        });
    };

    const getCoverAmountInfo = async () => {
        return await new Promise((resolve, reject) => {
            getFirebaseData(FIREBASE_KEYS.COVER_AMOUNT_INFO, (data) => {
                if (data) {
                    resolve(data);
                } else {
                    resolve(null);
                }
            });
        });
    };

    const callGetPlans = async () => {
        return await new Promise((resolve, reject) => {
            if (suggestedPlans) {
                resolve(suggestedPlans);
            } else {
                fetchSuggestedPlans(buyPlanJourney, (response, error) => {
                    if (error) {
                        showAPIError(error?.message ?? SOMETHING_WRONG);
                        resolve([]);
                    } else {
                        if (response.data && (response.data ?? []).length > 0) {
                            resolve(response.data);
                        } else {
                            let eventData = {...commonEventData};
                            eventData[INVOICE_AMOUNT] = buyPlanJourney?.invoiceValue;
                            eventData[INVOICE_DATE] = formatDateTime(buyPlanJourney?.invoiceDate);
                            eventData[BRAND] = buyPlanJourney?.brandName;
                            if(buyPlanJourney?.invoiceDate){
                                let age = daysDifference(buyPlanJourney?.invoiceDate?.getTime(), getCurretDateMilliseconds())
                                eventData[AGE] = age;
                            }
                            logWebEnageEvent(NO_PLANS_FOUND_EVENT, eventData);
                            showAPIError(NO_PLANS_FOUND);
                            resolve([]);
                        }
                    }
                });
            }
        });
    };


    const renewHAEWMembership = (plan) => {
        BlockingLoader.startLoader("Please wait...");
        renewHAMembership(plan?.plan?.planCode, buyPlanJourney, (response, error) => {
            BlockingLoader.stopLoader();
            if (error) {
                Alert.showAlert("Alert", error?.message, {
                    text: stringConstants.ok,
                    onClick: () => {
                    },
                });
            } else {
                nativeBridge.openHARenewalFlow(response?.data?.payNowLink);
            }
        });
    };

    const buyPlan = (plan) => {
        if (renewalData) {
            renewHAEWMembership(plan);
            return;
        }
        let eventData = {...commonEventData};
        eventData[PREMIUM_AMOUNT] = plan?.plan?.price;
        eventData[FINAL_PREMIUM_AMOUNT] = plan?.payableAmount;
        eventData[PLAN_CODE] = plan?.plan?.planCode;
        eventData[PLAN_NAME] = plan?.plan?.planName;
        eventData[COVER_AMOUNT] = plan?.plan?.coverAmount;
        eventData[DISCOUNT] = buyPlanJourney?.offerData?.couponCode ?? (buyPlanJourney?.offer?.couponCode ?? "No");
        eventData[NO_OF_APPLIANCE] = buyPlanJourney?.numberOfAppliances;

        logWebEnageEvent(SELECT_PLAN, eventData);
        nativeBridge.showPopupForVerifyNumber(NumberVerifyAlerts.buyPlan.message, "", LoginForScreen.SALES, status => {
            let destination = Destination.getDestination(category, service, product?.productCode);
            let productCode = destination === Destination.wallet ? Destination.wallet : product?.productCode;
            let paymentFlowInitProps = getParamsForPaymentFlow(destination, plan?.plan, APIData, {
                productCode: productCode,
                pinCode: buyPlanJourney?.pinCode,
                productName: buyPlanJourney?.product?.name,
                discount: buyPlanJourney?.offerData?.couponCode ?? (buyPlanJourney?.offer?.couponCode ?? "No"),
                numberOfAppliances: buyPlanJourney?.numberOfAppliances,
                finalPremiumAmount: plan?.payableAmount,
                buyPlanJourney: buyPlanJourney,
            });
            paymentFlowInitProps.currentStep = currentStep + 1;
            paymentFlowInitProps.totalSteps = totalSteps;
            nativeBridge.openPaymentModule(paymentFlowInitProps);
        });
    };

    const showCoverAmountsBottomSheet = () => {
        setAutoHeightBottomSheet({
            isVisible: true, data: {
                coverAmountsViewModel: reducerState?.coverAmountsViewModel, onSelectCoverAmount: (coverAmount) => {
                    dismissBottomSheet();
                    dispatch({type: ActionTypes.CHANGE_COVER_AMOUNT, data: coverAmount});
                },
            },
            type: AutoHeightBottomSheetType.COVER_AMOUNTS,
            title: reducerState?.coverAmountsViewModel?.bottomSheetVM?.heading,
            description: reducerState?.coverAmountsViewModel?.bottomSheetVM?.description,
        });
    };
    const showAllPlans = () => {
        props?.navigation?.navigate("ApplianceCountSelectionScreen",
            {
                buyPlanJourney: buyPlanJourney,
                destination: destination,
            });
    };
    const showPlanBenefitsBottomSheet = (planViewModel) => {
        logWebEnageEvent(SEE_ALL_BENEFITS, {...commonEventData});
        setAutoHeightBottomSheet({
            isVisible: true, data: {
                planCardViewModel: planViewModel, onBuyPlan: (plan) => {
                    dismissBottomSheet();
                    setTimeout(() => {
                        buyPlan(plan);
                    }, 100);
                },
            },
            type: AutoHeightBottomSheetType.PLAN_BENEFITS,
        });
    };

    const getDataFromCDN = async () => { // get screen components list
        return await new Promise((resolve, reject) => {
            getPlanListingFromCDN(buyPlanJourney?.category, buyPlanJourney?.service, buyPlanJourney?.product?.name, (response, error) => {
                if (error) {
                    Alert.showAlert("Alert", error?.message, {
                        text: stringConstants.ok,
                        onClick: () => {
                        },
                    });
                    resolve([]);
                }
                dispatch({type: ActionTypes.REQUEST_SCREEN_COMPONENTS, data: response?.data ?? [], pageVariationId: response?.pageVariationId});
                commonEventData.VariationId = response?.pageVariationId ?? ""
                resolve(response?.data ?? []);
            });
        });
    };

    const dismissBottomSheet = () => {
        setAutoHeightBottomSheet({isVisible: false, data: null, type: null, title: null, description: null});
    };

    const _renderComponent = ({item, index}) => {
        const Component = getComponent(item.type);
        return <Component key={index.toString()} data={item}
                          extraProps={{
                              reducerState: reducerState,
                              totalSteps: totalSteps,
                              currentStep: currentStep,
                              destination: destination,
                              buyPlan: buyPlan,
                              showAllPlans: showAllPlans,
                              showPlanBenefitsBottomSheet: showPlanBenefitsBottomSheet,
                              showCoverAmountsBottomSheet: showCoverAmountsBottomSheet,
                              renewalData: renewalData,
                              navigation: props?.navigation,
                              buyPlanJourney: buyPlanJourney,
                              location: "Plan Listing Screen",
                              category: buyPlanJourney?.category,
                              service: buyPlanJourney?.service,
                              product: buyPlanJourney?.product?.name ?? buyPlanJourney?.productService,
                              eventPageVariationId: reducerState.eventPageVariationId
                          }}/>;
    };

    if (isLoading) {
        return <Loader isLoading={isLoading}/>;
    }

    return (
        <SafeAreaView style={{flex: 1, paddingTop: spacing.spacing16}}>
            <FlatList
                contentContainerStyle={{paddingBottom: 0}}
                data={reducerState.screenComponents}
                renderItem={_renderComponent}
                showsVerticalScrollIndicator={false}
                bounces={false}
                keyExtractor={(item, index) => {
                    return index.toString();
                }}
                extraData={reducerState}
            />

            {autoHeightBottomSheet.isVisible
                ? <AutoHeightBottomSheet
                    data={autoHeightBottomSheet.data}
                    isShowFullBenefits={true}
                    bottomSheetType={autoHeightBottomSheet.type}
                    title={autoHeightBottomSheet.title}
                    description={autoHeightBottomSheet.description}
                    onDismiss={() => {
                        dismissBottomSheet();
                    }}
                />
                : null}

        </SafeAreaView>
    );
};

PlanListingScreen.navigationOptions = (navigationData) => {
    return {
        title: getPlanListScreenTitle(navigationData?.navigation?.state?.params?.buyPlanJourney),
        headerLeft: <BackButton onPress={() => {
            onBackPress(navigationData.navigation);
        }}/>,
    };
};

export default PlanListingScreen;
