import React, {useContext, useEffect, useReducer, useRef, useState} from "react";
import {getFirebaseData} from "../../commonUtil/AppUtils";
import {Category, Destination, FIREBASE_KEYS, PlanServices} from "../../Constants/AppConstants";
import * as ActionTypes from "../../Constants/ActionTypes";
import {
    BackHandler,
    Dimensions,
    Image,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    View
} from "react-native";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import {FormField} from "../../Catalyst/components/ComponentsFactory";
import InputBox from "../../Components/InputBox";
import images from "../../images/index.image";
import EmptyContainer from "../Components/EmptyContainer";
import {TextStyle} from "../../Constants/CommonStyle";
import TextView from "../../Catalyst/components/common/TextView";
import InputBoxDropDown from "../../Components/InputBoxDropDown";
import ButtonWithLoader from "../../CommonComponents/ButtonWithLoader";
import DateInputBox from "../../Components/DateInputBox";
import CheckBox from 'react-native-check-box';
import RangeSelectionComponent from "../Components/RangeSelectionComponent";
import BuyPlanReducer, {initialState} from "../Helpers/BuyPlanReducer";
import BackButton from "../../CommonComponents/NavigationBackButton";
import {onBackPress} from "../Helpers/BuyPlanUtils";
import {
    BRAND,
    CATEGORY,
    DISCOUNT,
    INVOICE_AMOUNT,
    INVOICE_DATE,
    LOCATION,
    PRODUCT,
    SERVICE,
} from "../../Constants/WebengageAttrKeys";

import {CommonContext} from "../../CommonComponents/Index/index.common";
import {APIData} from '../../../index';
import Loader from "../../Components/Loader";
import {logWebEnageEvent} from "../../commonUtil/WebengageTrackingUtils";

import {
    INVOICE_AMOUNT_SELECTED,
    PLAN_RECOMMENDATION_SCREEN,
    SEE_BEST_PLANS,
    SELECT_APPLIANCE,
    SELECT_DEVICE_BRAND,
    SELECT_INVOICE_RANGE,
} from "../../Constants/WebengageEvents";
import buyPlanJourney from "../Helpers/BuyPlanJourneySingleton";
import GadgetListComponent from "../Components/GadgetListComponent";
import {AppStringConstants} from "../../Constants/AppStringConstants";
import {useBackButton, useConstructor} from "../Helpers/CustomHooks";
import AutoHeightRBSheet from "../../Components/AutoHeightRBSheet";

let currentStep = 0;
let totalSteps = 0;
let commonEventData = {};
let dateEventAttributeValue = "Informatory";
const ProductInfoInputScreen = (props) => {
        const [reducerState, dispatch] = useReducer(BuyPlanReducer, initialState);
        const [isLoading, setIsLoading] = useState(false);
        const priceRangeSheetRef = useRef(null);
        const applianceSheetSheetRef = useRef(null);
        const buyPlanJourney = props.navigation.getParam("buyPlanJourney") ?? {};
        const destination = props.navigation.getParam("destination") ?? Destination.getDestination(buyPlanJourney.category, buyPlanJourney.service);
        const routes = Destination.getRoutesForDestination(destination);

        const {Alert} = useContext(CommonContext);

        useConstructor(() => { // before performing anything
            if (buyPlanJourney?.product?.code === "MP01") {
                buyPlanJourney.brandName = APIData?.brandName;
                buyPlanJourney.brandCode = undefined
            }
        });

        useBackButton(() => onBackPress(props.navigation));

        useEffect(() => {
            currentStep = routes.indexOf("ProductInfoInputScreen") + 1;
            totalSteps = routes.length;
            loadScreenComponents();
            showOfferAlert();
            commonEventData = {};
            commonEventData[LOCATION] = PLAN_RECOMMENDATION_SCREEN;
            commonEventData[CATEGORY] = buyPlanJourney?.category;
            commonEventData[SERVICE] = buyPlanJourney?.service;
            commonEventData[PRODUCT] = buyPlanJourney?.product?.name;
        }, []);

        useEffect(() => {
          // below event will get fired on first time render if brand selection dropdown visible for mobile with selected current device brand
          let brandSelectionComp = (reducerState.productInfoData?.components ?? []).filter(obj => obj.type == FormField.BRAND_SELECTION_INPUT).first()
          if (brandSelectionComp && buyPlanJourney?.product?.code === "MP01") {
            logWebEnageEvent(SELECT_DEVICE_BRAND, {
              ...commonEventData,
              Brand: APIData?.brandName,
            });
          }
        },[reducerState.productInfoData?.components])

        const showOfferAlert = () => {
            if (buyPlanJourney.offer) {
                Alert.showAlertWithImage(images.offer,
                    buyPlanJourney.offer.alertTitle,
                    buyPlanJourney.offer.alertDescription,
                    {
                        text: buyPlanJourney.offer.alertActionText, onClick: () => {
                        },
                    });
            }
        };

        const fetchGadgetData = (screenData) => {
            getFirebaseData(FIREBASE_KEYS.SERVICE_GADGETS, data => {
                dispatch({
                    type: ActionTypes.REQUEST_PRODUCT_INFO_COMPONENTS,
                    data: screenData,
                    pageInfo: {currentStep: currentStep, totalSteps: totalSteps},
                    invoiceDaysLimit: buyPlanJourney?.product?.invoiceDaysLimit,
                    buyPlanJourney: buyPlanJourney,
                    applianceData: data,
                });
            });
        };

        const loadScreenComponents = () => {
            setIsLoading(true);
            getFirebaseData(FIREBASE_KEYS.FIND_PLAN_VARIATIONS, data => {
                if (buyPlanJourney?.category === Category.homeAppliances
                    && buyPlanJourney?.service === PlanServices.extendedWarranty) {
                    fetchGadgetData(data ?? []);
                } else {
                    dispatch({
                        type: ActionTypes.REQUEST_PRODUCT_INFO_COMPONENTS,
                        data: data,
                        pageInfo: {currentStep: currentStep, totalSteps: totalSteps},
                        invoiceDaysLimit: buyPlanJourney?.product?.invoiceDaysLimit,
                        buyPlanJourney: buyPlanJourney,
                    });
                }
                setIsLoading(false);
            });
        };

        const moveToPlanList = () => {
            let eventData = {...commonEventData};
            let couponCode = buyPlanJourney?.offerData?.couponCode ?? (buyPlanJourney?.offer?.couponCode ?? "No");
            eventData[DISCOUNT] = couponCode;
            eventData[INVOICE_DATE] = dateEventAttributeValue;
            eventData[BRAND] = buyPlanJourney?.brandName;
            eventData[INVOICE_AMOUNT] = buyPlanJourney?.invoiceValue ?? buyPlanJourney?.invoiceRange;
            logWebEnageEvent(SEE_BEST_PLANS, eventData);


            let nextRoute = routes[currentStep];
            if (props?.navigation) {
                props?.navigation?.navigate(nextRoute, {
                    buyPlanJourney: buyPlanJourney,
                    destination: destination,
                });
            }
        };

        const getFormFieldTemplate = () => {
            if (!reducerState.productInfoData?.components) return null;
            return (
                <View>
                    {reducerState.productInfoData?.paragraph
                        ? <TextView style={styles.secondaryText}
                                    texts={reducerState.productInfoData?.paragraph}/>
                        : null}
                    {reducerState.productInfoData?.header
                        ? <TextView style={styles.primaryText}
                                    texts={reducerState.productInfoData?.header}/>
                        : null}

                    <View style={{paddingHorizontal: spacing.spacing16}}>
                        {
                            reducerState.productInfoData?.components.map((item, index) => {
                                switch (item.type) {
                                    case FormField.BRAND_SELECTION_INPUT:
                                        return getDropDownField(item, index, images.ic_edit);
                                    case FormField.PURCHASE_AMOUNT_RANGE_INPUT:
                                        return getDropDownField(item, index);
                                    case FormField.PURCHASE_AMOUNT_INPUT:
                                        return getInputField(item, index);
                                    case FormField.GADGET_SELECTION_INPUT:
                                        return getDropDownField(item, index);
                                    case FormField.DATE_SELECTION_INPUT:
                                        return getDateComponent(item, index);
                                        break;
                                }
                            })
                        }
                    </View>
                    {getNoteComponent()}
                    {
                        reducerState.productInfoData?.highlightedNote
                        && <View style={{marginTop: spacing.spacing32, marginHorizontal: spacing.spacing16}}>
                            <EmptyContainer data={{
                                bgColor: reducerState.productInfoData?.highlightedNote?.bgColor,
                                borderColor: reducerState.productInfoData?.highlightedNote?.borderColor,
                                borderWidth: 1,
                                borderRadius: 2,
                                components: reducerState.productInfoData?.highlightedNote?.components,
                            }}
                                            bgColor={reducerState.productInfoData?.color}/>
                        </View>
                    }
                </View>);
        };

        const getDateComponent = (fieldProps, index) => {
            let minDate = null;
            if (buyPlanJourney?.product) {
                minDate = new Date();
                minDate.setDate(minDate.getDate() - buyPlanJourney?.product?.invoiceDaysLimit);
            }

            const selectedDate = fieldProps.inputValue ?? new Date();
            buyPlanJourney.invoiceDate = selectedDate;
            return <DateInputBox
                containerStyle={styles.dateInputContainer}
                inputHeading={fieldProps.title}
                minimumDate={minDate}
                date={selectedDate}
                maximumDate={new Date()}
                format={"dd/mm/yyyy"}
                onDateChange={(date) => {
                    dateEventAttributeValue = date;
                    buyPlanJourney.invoiceDate = date;
                    dispatch({
                        type: ActionTypes.VALIDATE_FORM_FIELD,
                        text: date,
                        index: index,
                    });
                }}
            />;
        };

        const getNoteComponent = () => {
            if (reducerState.productInfoData?.note) {
                switch (reducerState.productInfoData?.note?.type) {
                    case FormField.LEFT_IMAGE_TEXT_COMPONENT:
                        let image = {uri: reducerState.productInfoData?.note.image};
                        const paragraphText = reducerState.productInfoData?.note.paragraph || {};
                        return (<View style={{
                            flexDirection: 'row', marginHorizontal: spacing.spacing16, marginTop: spacing.spacing12
                        }}>
                            <Image source={image} style={styles.noteImage}/>
                            <TextView style={{...styles.noteTextStyle, color: colors.charcoalGrey}}
                                      texts={paragraphText}/>
                        </View>)

                    case FormField.CHECK_BOX_INPUT:
                        dateEventAttributeValue = "Declaration";
                        return getCheckboxComponent(reducerState.productInfoData?.note);
                }
            }
            return null;
        };

        const getInputField = (fieldProps, index, rightImage) => {
            return <InputBox
                containerStyle={styles.fieldContainer}
                inputHeading={fieldProps.title}
                placeholder={fieldProps.hint}
                multiline={false}
                numberOfLines={1}
                autoFocus={false}
                errorMessage={fieldProps?.errorMessage}
                keyboardType={fieldProps.type === FormField.PURCHASE_AMOUNT_INPUT ? "numeric" : "default"}
                onBlur={() => {
                    if (fieldProps.type === FormField.PURCHASE_AMOUNT_INPUT) {
                        let eventData = {...commonEventData};
                        eventData[INVOICE_AMOUNT] = buyPlanJourney.invoiceValue;
                        logWebEnageEvent(INVOICE_AMOUNT_SELECTED, eventData);
                    }
                }}
                onChangeText={inputMessage => {
                    if (fieldProps.type === FormField.PURCHASE_AMOUNT_INPUT) {
                        buyPlanJourney.invoiceValue = inputMessage;
                    }
                    dispatch({type: ActionTypes.VALIDATE_FORM_FIELD, text: inputMessage, index: index});
                }}
                value={fieldProps.inputValue}
                editable={true}
                rightImage={rightImage}>
            </InputBox>;
        };

        const openPriceRangeSheet = (fieldProps, index) => {
            if (!buyPlanJourney.product || !buyPlanJourney.brandName) {
                dispatch({
                    type: ActionTypes.SET_FIELD_ERROR,
                    text: "",
                    index: index,
                });
            } else {
                priceRangeSheetRef.current.openWithIndex(index);
            }
        };
        const openBrandScreen = (fieldProps, index) => {
            if (!buyPlanJourney.product) {
                // alert(AppStringConstants.ERROR_MSG_SELECT_APPLIANCE)
                dispatch({
                    type: ActionTypes.SET_FIELD_ERROR,
                    text: "",
                    index: index,
                });
            } else {
                props.navigation?.navigate("BrandSelectionScreen", {
                    onBrandSelection: (item) => {
                        logWebEnageEvent(SELECT_DEVICE_BRAND, {
                            ...commonEventData,
                            Brand: item.name,
                        });
                        buyPlanJourney.brandName = item.name;
                        buyPlanJourney.brandCode = item.data?.oaSystemConfigPK?.paramName
                        dispatch({
                            type: ActionTypes.VALIDATE_FORM_FIELD,
                            text: item.name,
                            index: index,
                        });
                    },
                    buyPlanJourney: buyPlanJourney,
                });
            }
        };

        const getDropDownField = (fieldProps, index, rightImage) => {
            return <InputBoxDropDown containerStyle={styles.fieldContainer}
                                     key={index}
                                     isMultiLines="false"
                                     height={28}
                                     keyboardType={fieldProps.keyboardType}
                                     inputHeading={fieldProps.title}
                                     placeholder={fieldProps.hint}
                                     placeholderTextColor={colors.grey}
                                     maxLength={2}
                                     value={fieldProps.inputValue}
                                     errorMessage={fieldProps.errorMessage}
                                     rightImage={rightImage}
                                     onPress={() => {
                                         if (fieldProps.type == FormField.PURCHASE_AMOUNT_RANGE_INPUT) {
                                             logWebEnageEvent(SELECT_INVOICE_RANGE, commonEventData);
                                             openPriceRangeSheet(fieldProps, index);
                                         } else if (fieldProps.type == FormField.GADGET_SELECTION_INPUT) {
                                             applianceSheetSheetRef.current.openWithIndex(index);
                                         } else if (fieldProps.type == FormField.BRAND_SELECTION_INPUT) {
                                             openBrandScreen(fieldProps, index);
                                         }
                                     }}/>;
        };

        const getPrimaryButtonComponent = () => {
            return (
                <View style={styles.primaryButtonContainer}>
                    <ButtonWithLoader
                        isLoading={isLoading}
                        enable={reducerState.isFormComplete}
                        Button={{
                            text: "See Plans",
                            onClick: () => {
                                dispatch({
                                    type: ActionTypes.VALIDATE_FORM,
                                });
                            },
                        }}
                    />
                </View>
            );
        };


        const getCheckboxComponent = (fieldProps) => {
            return (
                <View>
                    <TouchableOpacity activeOpacity={1}
                                      style={styles.checkBoxContainer}
                                      onPress={() => {
                                          dispatch({type: ActionTypes.TOGGLE_CHECKBOX});
                                      }}>
                        <CheckBox
                            style={styles.checkboxStyle}
                            checkedImage={<Image source={require('../../images/ic_check_box.webp')}
                                                 style={styles.checkbox}/>}
                            unCheckedImage={<Image source={require('../../images/ic_check_box_outline_blank.webp')}
                                                   style={styles.checkbox}/>}
                            isChecked={fieldProps.isChecked}
                            onClick={() => {
                                dispatch({type: ActionTypes.TOGGLE_CHECKBOX});
                            }}
                        />
                        <TextView style={{...TextStyle.text_14_bold}}
                                  texts={fieldProps.paragraph}
                                  onPress={() => {
                                      dispatch({type: ActionTypes.TOGGLE_CHECKBOX});
                                  }}/>

                    </TouchableOpacity>

                </View>
            );
        };

        const getPriceRangeBottomSheet = () => {
            return (
                <AutoHeightRBSheet
                    ref={priceRangeSheetRef}
                    duration={200}
                    closeOnSwipeDown={false}>
                    <View style={{
                        paddingBottom: spacing.spacing20, maxHeight: Dimensions.get("screen").height / 1.2,
                        borderRadius: spacing.spacing16
                    }}>
                        <RangeSelectionComponent data={reducerState.priceRangeData}
                                                 extraProps={{
                                                     buyPlanJourney: buyPlanJourney
                                                 }}
                                                 onClose={() => {
                                                     priceRangeSheetRef.current.close();
                                                 }}
                                                 onSelectionChange={(selectedItem) => {
                                                     priceRangeSheetRef.current.close();
                                                     buyPlanJourney.invoiceRange = selectedItem;
                                                     let eventData = {...commonEventData};
                                                     eventData[INVOICE_AMOUNT] = selectedItem;
                                                     logWebEnageEvent(INVOICE_AMOUNT_SELECTED, eventData);
                                                     dispatch({
                                                         type: ActionTypes.VALIDATE_FORM_FIELD,
                                                         text: selectedItem,
                                                         index: priceRangeSheetRef.current.index(),
                                                     });
                                                 }}/>
                    </View>
                </AutoHeightRBSheet>
            );
        };

        const getApplianceBottomSheet = () => {
            let totalItems = 0;

            if (reducerState?.applianceData && reducerState?.applianceData?.data) {
                totalItems += reducerState?.applianceData?.data?.products?.length;
                if (reducerState?.applianceData?.data?.otherProducts) {
                    totalItems += reducerState?.applianceData?.data?.otherProducts?.length;
                }
            }

            let data = reducerState.applianceData ?? {};
            return (
                <AutoHeightRBSheet
                    ref={applianceSheetSheetRef}
                    duration={10}
                    closeOnSwipeDown={false}>
                    <View style={{paddingBottom: spacing.spacing20, maxHeight: Dimensions.get("screen").height / 1.2}}>
                        <TouchableOpacity style={styles.close} onPress={() => {
                            applianceSheetSheetRef.current.close();
                        }}>
                            <Image source={images.ic_blackClose} style={styles.closeIcon}/>
                        </TouchableOpacity>
                        <ScrollView showsVerticalScrollIndicator={false}
                                    style={{marginBottom: spacing.spacing80}} bounces={false}>
                            <GadgetListComponent data={{
                                ...data, onItemClick: (dataItem) => {
                                    let eventData = {...commonEventData}

                                    eventData["Name"] = dataItem?.name
                                    logWebEnageEvent(SELECT_APPLIANCE, eventData);

                                    applianceSheetSheetRef.current.close();
                                    buyPlanJourney.product = dataItem;
                                    dispatch({
                                        type: ActionTypes.VALIDATE_FORM_FIELD,
                                        text: dataItem?.name,
                                        index: applianceSheetSheetRef.current.index(),
                                    });
                                },
                            }}
                                                 onClose={() => {
                                                     applianceSheetSheetRef.current.close();
                                                 }}/>
                        </ScrollView>
                    </View>
                </AutoHeightRBSheet>
            );
        };

        if (reducerState.action === ActionTypes.FORM_VALIDATED) {
            moveToPlanList();
        }

        return (
            <SafeAreaView style={styles.SafeArea}>
                <View style={{flex: 1}}>
                    <ScrollView style={styles.scrollViewStyle}>
                        {reducerState.productInfoData?.components && getFormFieldTemplate()}
                    </ScrollView>
                    {getPrimaryButtonComponent()}

                    {getPriceRangeBottomSheet()}
                    {getApplianceBottomSheet()}
                </View>
                {isLoading
                    ? <Loader isLoading={isLoading} loaderStyle={{
                        backgroundColor: "transparent",
                        position: "absolute",
                        top: 0,
                        width: "100%",
                    }}/>
                    : null}


            </SafeAreaView>
        );
    }
;
ProductInfoInputScreen.navigationOptions = (navigationData) => {
    let screenTitle = "";
    if (buyPlanJourney?.category === Category.personalElectronics) {
        screenTitle = AppStringConstants.FIND_PLAN + ": " + buyPlanJourney?.product?.name;
    } else {
        screenTitle = AppStringConstants.FIND_A_PLAN;
    }
    return {
        title: screenTitle,
        headerLeft: <BackButton onPress={() => {
            onBackPress(navigationData.navigation);
        }}/>,
    };
};


const styles = StyleSheet.create({
    SafeArea: {
        flex: spacing.spacing1,
        backgroundColor: colors.white,
    },
    scrollViewStyle: {
        flex: 1,
        paddingBottom: spacing.spacing32,
        backgroundColor: colors.white,
    },

    fieldStyle: {
        marginVertical: spacing.spacing8,
    },
    textInputContainer: {
        justifyContent: "space-between",
        marginTop: spacing.spacing24,
    },
    primaryText: {
        ...TextStyle.text_16_bold,
        marginTop: spacing.spacing4,
        paddingHorizontal: spacing.spacing16,
    },
    secondaryText: {
        ...TextStyle.text_12_bold,
        marginTop: spacing.spacing32,
        paddingHorizontal: spacing.spacing16,
    },
    primaryButtonContainer: {
        justifyContent: "flex-end",
        alignSelf: "stretch",
        marginTop: spacing.spacing48,
        marginBottom: spacing.spacing20,
        paddingHorizontal: spacing.spacing16,
    },
    checkBoxContainer: {
        alignItems: "center",
        flexDirection: "row",
        marginTop: spacing.spacing16,
        marginBottom: spacing.spacing100,
        paddingVertical: spacing.spacing10
    },

    checkboxStyle: {
        width: 24,
        height: 24,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing8,
        marginTop: spacing.spacing8


    },
    checkbox: {
        height: 18,
        width: 18,
        opacity: 1,
    },
    fieldContainer: {
        marginTop: spacing.spacing32,
    },
    dateInputContainer: {
        marginTop: spacing.spacing20,
    },
    close: {
        height: 24,
        width: 24,
        borderRadius: 12,
        backgroundColor: colors.greyF1F2F3,
        alignItems: "center",
        justifyContent: "center",
        marginTop: spacing.spacing12,
        marginRight: spacing.spacing12,
        alignSelf: "flex-end",

    },
    closeIcon: {
        height: 10,
        width: 10,
        resizeMode: "contain",
    },

    noteTextStyle: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        flex: 1,
        marginLeft: spacing.spacing8,
        marginHorizontal: spacing.spacing20,
        alignSelf: 'center'
    },
    noteImage: {
        width: 32,
        height: 32,
        resizeMode: "contain",
    },
});


export default ProductInfoInputScreen;
