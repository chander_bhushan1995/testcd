import React, { useContext, useEffect, useReducer, useState } from "react";
import {
    Animated,
    FlatList,
    NativeModules,
    Platform,
    SafeAreaView,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from "react-native";
import images from "../../images/index.image";
import { TextStyle } from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import {
    ALREADY_COVERED,
    BOOK_FREE,
    CONTINUE_BOOKING,
    Destination,
    FIREBASE_KEYS,
    FlowSequence,
    LoginForScreen,
    MAX_QUANTITY_SELECTED,
    NEED_SPECIAL_ATTENTION,
    NumberVerifyAlerts,
    PLATFORM_OS,
    RAISE_SERVICE_REQ,
    SELECT_ISSUE,
    Services,
    SOMETHING_WRONG,
    SPECIAL_ATTENTION,
    TaskTypes,
    UP_TO_APPLIANCE,
} from "../../Constants/AppConstants";
import colors from "../../Constants/colors";
import SODServiceListComponent from "../Components/SODComponents/SODServiceListComponent";
import {
    checkSODProductServiceInMembership,
    getParamsForPaymentFlow,
    getProductWithServicesList,
    onBackPress,
} from "../Helpers/BuyPlanUtils";
import BuyPlanReducer, { initialState } from "../Helpers/BuyPlanReducer";
import Loader from "../../Components/Loader";
import {
    getPlansForSOD,
    getProductsSpecs,
    getServiceTask,
    getSODProductPage,
    getSODProductServicePage,
    getSODProductsList,
    getSODProductsOffers,
    getTimeSlotsForToday,
    notifyMe,
} from "../../network/APIHelper";

import {stringConstants} from "../../CardManagement/DataLayer/StringConstants";

import {CommonContext} from "../../CommonComponents/Index/index.common";
import {useBackButton} from "../Helpers/CustomHooks";
import * as ActionTypes from "../../Constants/ActionTypes";
import { getComponent } from "../../Catalyst/components/ComponentsFactory";
import Clipboard from "@react-native-community/clipboard";
import LabourCostScreen from "../Components/SODComponents/LabourCostScreen";
import ButtonWithLoader from "../../CommonComponents/ButtonWithLoader";
import {APIData} from '../../../index';
import { getFirebaseData, parseJSON } from "../../commonUtil/AppUtils";
import SODNotifyMeComponent from "../Components/SODComponents/SODNotifyMeComponent";
import {
    AF_CONTENT_ID,
    AF_CONTENT_LIST,
    AF_CONTENT_TYPE, AF_CURRENCY, AF_PRICE,
    APPLIANCE,
    COST,
    COUNT,
    LOCATION,
    NO_OF_APPLIANCE,
    PLAN_CODE,
    PLAN_NAME,
    POSITION,
    PREMIUM_AMOUNT,
    PRODUCT,
    SCREEN_DURATION,
    SERVICE,
    SERVICE_TYPE,
} from "../../Constants/WebengageAttrKeys";
import { logAppsFlyerEvent, logWebEnageEvent } from "../../commonUtil/WebengageTrackingUtils";
import {
    AF_CONTENT_VIEW, AF_LIST_VIEW,
    SELECT_PLAN, SOD_PROCEED_TO_BOOK,
    SOD_PRODUCT_SCREEN,
    SOD_PRODUCT_SERVICE_SCREEN,
    WHATS_NOTE_COVERED,
} from "../../Constants/WebengageEvents";
import { getCurretDateMilliseconds, minutesSecondDifference } from "../../commonUtil/DateUtils";
import AutoHeightBottomSheet, { AutoHeightBottomSheetType } from "../Components/AutoHeightBottomSheet";
import { getStatusBarHeight } from "react-native-iphone-x-helper";

let HEADER_EXPANDED_HEIGHT = 46;
let apiArray = [];
const HEADER_COLLAPSED_HEIGHT = 46;
const nativeBridgeRef = NativeModules.ChatBridge;
let currentTimestamp = null;
const SODPlanDetailScreen = props => {

    const [scrollY, setScrollY] = useState(new Animated.Value(0));
    const [reducerState, dispatch] = useReducer(BuyPlanReducer, initialState);
    const [isLoading, setIsLoading] = useState(true);

    const STATUS_BAR_HEIGHT = Platform.OS === "ios" ? getStatusBarHeight(true) : StatusBar.currentHeight;
    const [headerMaxHeight, setHeaderMaxHeight] = useState(0);
    const [isShowLabourCostModal, setShowLabourCostModal] = useState(false);
    const [autoHeightBottomSheet, setAutoHeightBottomSheet] = useState({isVisible: false, data: null, type: null});
    const [isShowNotifyMe, setShowNotifyMe] = useState({visible: false, message: ""});
    const {Alert, BlockingLoader} = useContext(CommonContext);

    const destination = props.navigation.getParam("destination") ?? Destination.sod;
    const productCode = props.navigation.getParam("productCode");
    const category = props.navigation.getParam("category");
    const services = props.navigation.getParam("services");
    const selectedServiceRequestType = props.navigation.getParam("serviceRequestType");
    const productName = props.navigation.getParam("productName");
    const pinCode = props.navigation.getParam("pinCode");
    const isCameFromDeeplink = props.navigation.getParam("isCameFromDeeplink");
    const eventLocation = props.navigation.getParam("eventLocation");
    const sodServiceSelectionPosition = props.navigation.getParam("position");
    const totalSpentTimeOnScreen = props.navigation.getParam("totalSpentTimeOnScreen");
    const isShowCollapsibleHeader = destination === Destination.sod;
    const headerBackgroundColor = isShowCollapsibleHeader ? colors.darkblue64 : colors.white;

    useBackButton(() => onBackPress(props.navigation));

    useEffect(() => {
        let eventData = {};
        currentTimestamp = getCurretDateMilliseconds();
        eventData[LOCATION] = eventLocation ?? "Product";
        eventData[APPLIANCE] = productCode;
        if (destination === Destination.sod && !isCameFromDeeplink) {
            logWebEnageEvent(SOD_PRODUCT_SCREEN, eventData);
        } else if (destination === Destination.sodQTY) {
            eventData[SERVICE_TYPE] = selectedServiceRequestType;
            eventData[POSITION] = sodServiceSelectionPosition;
            eventData[SCREEN_DURATION] = totalSpentTimeOnScreen;
            logWebEnageEvent(SOD_PRODUCT_SERVICE_SCREEN, eventData);
        }

        getSODProductsOffers((data) => {
            dispatch({ type: ActionTypes.REQUEST_SOD_APPLIANCES_OFFERS, data: data?.data });
            callAllAPI();
        });
        return ()=>{
            scrollY.removeAllListeners()
            scrollY.stopAnimation()
        }
    }, []);

    const headerHeight = scrollY.interpolate({
        inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
        outputRange: [HEADER_EXPANDED_HEIGHT + 2, HEADER_COLLAPSED_HEIGHT + 2],
        extrapolate: "clamp",
    });

    const headerTitleOpacity = scrollY.interpolate({
        inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
        outputRange: [0, 1],
        extrapolate: "clamp",
    });
    const headerChildOpacity = scrollY.interpolate({
        inputRange: [0, (HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT) / 2, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
        outputRange: [1, 0.8, 0],
        extrapolate: "clamp",
    });

    const headerBGColor = scrollY.interpolate({
        inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
        outputRange: [headerBackgroundColor, "rgb(255, 255, 255)"],
        extrapolate: "clamp",
    });

    const backIconTintColor = scrollY.interpolate({
        inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
        outputRange: ["rgb(255, 255, 255)", "rgb(0, 0, 0)"],
        extrapolate: "clamp",
    });


    const callAllAPI = () => {
        if (destination === Destination.sod) {
            apiArray.push(getSODServicesList());
        } else if (destination === Destination.sodQTY) {
            dispatch({ type: ActionTypes.START_LOADING_DATA });
            apiArray.push(callGetSODPlans(), callTodayTimeSlots(), callSODProductServicePageLayout());
            if (selectedServiceRequestType === Services.breakdown) {
                apiArray.push(getProductIssuesList(), callGetProductSpecs());
            }
        }
        Promise.all(apiArray).then((values) => {
            apiArray = [];
            setIsLoading(false);
            if (destination === Destination.sodQTY) {
                dispatch({
                    type: ActionTypes.PAINT_SOD_PRODUCT_SERVICE_SCREEN, data: {
                        selectedServiceRequestType: selectedServiceRequestType, productCode: productCode,
                        productName: productName,
                    },
                });
            } else if (destination === Destination.sod) {
                if (isCameFromDeeplink && Array.isArray(services) && services.first()) {
                    const [servicesList, pageLayout] = values;
                    let serviceObj = servicesList?.filter(service => service.serviceCode == services.first()).first();
                    if (serviceObj) {
                        checkAlreadyHaveMembership(serviceObj, "Top");
                    }
                }
            }
        });
    };

    const showAPIError = (message) => {
        Alert.showAlert("Alert", message ?? SOMETHING_WRONG, {
            text: stringConstants.ok,
            onClick: () => {
                onBackPress(props.navigation);
            },
        });
    };

    const getSODServicesList = async () => {
        return await new Promise((resolve, reject) => {
            getSODProductsList([productCode], services, (response, error) => {
                if (error) {
                    Alert.showAlert("Alert", SOMETHING_WRONG, {
                        text: stringConstants.ok,
                        onClick: () => {
                            onBackPress(props.navigation);
                        },
                    });
                    reject(error);
                } else {
                    dispatch({
                        type: ActionTypes.REQUEST_SOD_SERVICE,
                        data: response.data,
                    });
                    apiArray.push(callSODProductPageLayout(() => {
                        resolve(response.data);
                    }));
                }
            });
        });
    };

    const callSODProductPageLayout = async (servicesListResolver) => {
        return await new Promise((resolve, reject) => {
            getSODProductPage(category, (response, error) => {
                if (error) {
                    Alert.showAlert("Alert", SOMETHING_WRONG, {
                        text: stringConstants.ok,
                        onClick: () => {
                            onBackPress(props.navigation);
                        },
                    });
                    resolve(error);
                } else {
                    dispatch({
                        type: ActionTypes.REQUEST_SOD_PRODUCT_PAGE,
                        data: response.data,
                        pinCode: pinCode,
                    });
                    servicesListResolver();
                    resolve();
                }
            });
        });
    };

    const callSODProductServicePageLayout = async () => {
        await new Promise((resolve, reject) => {
            getSODProductServicePage(category, selectedServiceRequestType, (response, error) => {
                if (error) {
                    showAPIError(error?.message);
                    reject(error);
                } else {
                    dispatch({ type: ActionTypes.REQUEST_SOD_PRODUCT_SERVICE_PAGE, data: response.data });
                    resolve(response);
                }
            });
        });
    };

    const callTodayTimeSlots = async () => {
        await new Promise((resolve, reject) => {
            getTimeSlotsForToday(selectedServiceRequestType, Destination.sod, pinCode, APIData.partnerCode, APIData.partnerBUCode, productCode, (response, error) => {
                if (error) {
                    resolve(error);
                } else {
                    dispatch({ type: ActionTypes.REQUEST_SOD_TODAY_TIME_SLOTS, data: response.data ?? {} });
                    resolve(response);
                }
            });
        });
    };

    const getProductIssuesList = async () => {
        await new Promise((resolve, reject) => {
            getServiceTask(productCode, TaskTypes.ISSUE, [], (response, error) => {
                if (error) {
                    showAPIError(error?.message);
                    reject(error);
                } else {
                    dispatch({ type: ActionTypes.REQUEST_SOD_PRODUCT_ISSUES, data: response.data });
                    resolve(response);
                }
            });
        });
    };

    const callGetProductSpecs = async () => {
        await new Promise((resolve, reject) => {
            getProductsSpecs([productCode], (response, error) => {
                if (error) {
                    showAPIError(error?.message);
                    reject(error);
                } else {
                    apiArray.push(callGetLabourCost(response, resolve));
                    // resolve();
                }
            });
        });
    };

    const callGetLabourCost = async (productSpecsResponse, productSpecsResolver) => {
        let productVariantIDs = productSpecsResponse?.data?.map((productSpec) => {
            return productSpec?.id;
        }) ?? [];
        await new Promise((resolve, reject) => {
            getServiceTask(productCode, TaskTypes.LABOUR_COST, productVariantIDs, (response, error) => {
                if (error) {
                    showAPIError(error?.message);
                    reject(error);
                } else {
                    dispatch({
                        type: ActionTypes.REQUEST_SOD_LABOUR_COST,
                        data: { labourCostResponse: response.data, productSpecsResponse: productSpecsResponse?.data },
                    });
                    resolve();
                    productSpecsResolver();
                }
            });
        });
    };

    const callGetSODPlans = async () => {
        await new Promise((resolve, reject) => {
            getPlansForSOD(productCode, selectedServiceRequestType, (response, error) => {
                if (error) {
                    showAPIError(error?.message);
                    reject(error);
                } else {
                    dispatch({ type: ActionTypes.REQUEST_SOD_PLANS_DATA, data: response.data ?? [] });
                    let eventDataAppsFlyer = {};
                    eventDataAppsFlyer[AF_CONTENT_TYPE] = getProductWithServicesList(Destination.sodQTY, Array.isArray(response.data) ? response.data : [], [], productName);
                    eventDataAppsFlyer[AF_CONTENT_LIST] = (Array.isArray(response.data) ? response.data : []).map(plan => plan?.planCode).join(",");
                    logAppsFlyerEvent(AF_LIST_VIEW, eventDataAppsFlyer);
                    resolve(response);
                }
            });
        });
    };

    const createLeadForSOD = (name, email, count, isShowSuccessAlert) => {
        BlockingLoader.startLoader("Please wait...");
        getFirebaseData(FIREBASE_KEYS.QR_CampaignId, (campaignId) => {
            notifyMe(name, APIData.mobile_number, pinCode, email, campaignId, (response, error) => {
                BlockingLoader.stopLoader();
                if (error) {
                    setTimeout(() => {
                        showAPIError(error?.message), 100;
                    });
                } else {
                    if (isShowSuccessAlert) {
                        Alert.showAlertWithImage(null, SPECIAL_ATTENTION, UP_TO_APPLIANCE(count), {
                            text: stringConstants.ok,
                            onClick: () => {
                            },
                        });
                    }
                }
            });
        });
    };

    const checkAlreadyHaveMembership = (serviceObj, position) => {
        nativeBridgeRef.getAllMemberships((params) => {
            if (Platform.OS === PLATFORM_OS.android) {
                params = {
                    ...params,
                    activeMemberships: parseJSON(params.activeMemberships),
                    pendingMemberships: parseJSON(params.pendingMemberships),
                };
            }
            let redirectionData = {
                "destination": Destination.sodQTY,
                "category": category,
                "serviceRequestType": serviceObj?.serviceCode,
                "productCode": serviceObj?.productCode,
                "productName": serviceObj?.productName,
                "pinCode": pinCode,
                "totalSpentTimeOnScreen": minutesSecondDifference(currentTimestamp, getCurretDateMilliseconds()),
                "position": position,
                "isCameFromDeeplink": isCameFromDeeplink,
                "eventLocation": eventLocation,
            };
            if (APIData.customer_id && checkSODProductServiceInMembership(params?.activeMemberships, serviceObj?.serviceCode, productCode)) {
                Alert.showAlertWithImage(images.yellowInfo, ALREADY_COVERED, BOOK_FREE, {
                    text: RAISE_SERVICE_REQ,
                    onClick: () => { // redirect to membership
                        nativeBridgeRef.openMemTab();
                    },
                }, {
                    text: CONTINUE_BOOKING,
                    onClick: () => {
                        routeToPincodeOrSODPlanSelection(redirectionData);
                    },
                });
            } else {
                routeToPincodeOrSODPlanSelection(redirectionData);
            }
        });
    };

    const routeToPincodeOrSODPlanSelection = (params) => {
        getFirebaseData(FIREBASE_KEYS.MODULE_FLOW_SEQUENCING, (data) => {
            if (data && data?.sodFlowSequencing.screen_1 === FlowSequence.SODFlow.PRODUCT_SCREEN) {
                props?.navigation?.navigate("PinCodeScreen", { ...params, destination: Destination.sod });
            } else {
                props?.navigation?.navigate("SODProductServiceScreen", params);
            }
        });
    };


    const onchangeQuantity = (changedPlanCode, changedPlanName, changeBy, maxEligibility, singleUnitPrice) => {
        let data = {
            planCode: changedPlanCode,
            changeBy: changeBy,
            singleUnitPrice: singleUnitPrice,
            maxEligibility: maxEligibility,
            planName: changedPlanName,
        };
        if (changeBy > 0) {
            if (selectedServiceRequestType === Services.breakdown) {
                setAutoHeightBottomSheet({
                    isVisible: true, data: {
                        checkboxListVM: reducerState?.sodIssuesVM,
                        onSelectionComplete: (issuesObjArray) => {
                            dismissBottomSheet();
                            data.issues = issuesObjArray;
                            dispatch({ type: ActionTypes.UPDATE_SOD_PLAN_QUANTITY_PRICE, data: data });
                        },
                    },
                    type: AutoHeightBottomSheetType.CHECK_LIST,
                    title: SELECT_ISSUE,
                });
            } else { // handle for pms
                dispatch({ type: ActionTypes.UPDATE_SOD_PLAN_QUANTITY_PRICE, data: data });
            }
        } else {
            dispatch({ type: ActionTypes.UPDATE_SOD_PLAN_QUANTITY_PRICE, data: data });
        }
    };

    const dismissBottomSheet = () => {
        setAutoHeightBottomSheet({ isVisible: false, data: null, type: null, title: null, description: null });
    };

    const showNotCoveredBenefits = () => {
        // EventTracking.shared.eventTracking(name: .planWhatsNotCovered, [.location: self.productCode ?? "",.service: self.selectedServiceRequestType ?? ""])
        let eventData = {};
        eventData[LOCATION] = productCode;
        eventData[SERVICE] = selectedServiceRequestType;
        logWebEnageEvent(WHATS_NOTE_COVERED, eventData);
        setAutoHeightBottomSheet({
            isVisible: true, data: {
                listOfText: reducerState.planExcludedBenefitsVM?.listOfBenefits ?? [],
            },
            type: AutoHeightBottomSheetType.LEFT_IMAGE_RIGHT_TEXT_LIST,
            title: reducerState.planExcludedBenefitsVM?.heading,
        });
    };

    const onSeeLabourCost = () => {
        setShowLabourCostModal(true);
    };

    const onTappedOfferView = (couponCode) => {
        Clipboard.setString(couponCode);
        // alert(COPIED);
    };

    const logEventsForProceedToBook = () => {
        let eventData = {};
        let planCodes = reducerState?.sodSelectedPlansAndQuantity?.plans.map(plan => plan?.planCode).join(",");
        let planNames = reducerState?.sodSelectedPlansAndQuantity?.plans.map(plan => plan?.planName).join(",");
        eventData[AF_PRICE] = reducerState?.sodSelectedPlansAndQuantity?.overAllTotalPrice
        eventData[AF_CONTENT_ID] = planCodes
        eventData[AF_CURRENCY] = "INR"
        eventData[AF_CONTENT_TYPE] = getProductWithServicesList(destination, [], [selectedServiceRequestType], productName)
        logAppsFlyerEvent(AF_CONTENT_VIEW, eventData);

        eventData = {}; // reset value
        eventData[LOCATION] = productCode;
        eventData[SERVICE] = selectedServiceRequestType;
        eventData[PREMIUM_AMOUNT] = reducerState?.sodSelectedPlansAndQuantity?.overAllTotalPrice;
        eventData[NO_OF_APPLIANCE] = reducerState?.sodSelectedPlansAndQuantity?.overAllQuantity;
        eventData[PRODUCT] = productName;
        eventData[PLAN_NAME] = planNames;
        eventData[PLAN_CODE] = planCodes;
        logWebEnageEvent(SELECT_PLAN, eventData);

        eventData = {}; // reset value
        eventData[APPLIANCE] = productCode;
        eventData[COUNT] = reducerState?.sodSelectedPlansAndQuantity?.overAllQuantity.toString();
        eventData[SERVICE_TYPE] = selectedServiceRequestType;
        eventData[COST] = reducerState?.sodSelectedPlansAndQuantity?.overAllTotalPrice;
        logWebEnageEvent(SOD_PROCEED_TO_BOOK, eventData);
    };

    const onProceedToBook = () => {
        nativeBridgeRef.showPopupForVerifyNumber(NumberVerifyAlerts.buySODPlan.message, "", LoginForScreen.SOD_PLAN_SALES, status => {
            if (status) {
                if (reducerState.sodSelectedPlansAndQuantity) {
                    // filter plans where selected quantity is greater than max eligible quantity
                    let filteredPlan = reducerState.sodSelectedPlansAndQuantity?.plans.filter((plan) => {
                        return plan.maxEligibalQuantity < plan.totalQuantity;
                    }).first();
                    if (filteredPlan) {
                        if (!APIData?.user_Name) {
                            setShowNotifyMe({
                                visible: true,
                                message: MAX_QUANTITY_SELECTED(filteredPlan.maxEligibalQuantity),
                            });
                        } else {
                            createLeadForSOD(APIData.user_Name, APIData.user_Email, filteredPlan.maxEligibalQuantity, true);
                        }
                    } else {
                        logEventsForProceedToBook();
                        nativeBridgeRef.openPaymentModule(getParamsForPaymentFlow(Destination.sodQTY, null, APIData, {
                            selectedServiceRequestType: selectedServiceRequestType,
                            pinCode: pinCode,
                            selectedProductCode: productCode,
                            productName: productName,
                            ...reducerState.sodSelectedPlansAndQuantity,
                        }));
                    }
                }
            }
        });
    };

    const _renderComponent = ({ item, index }) => {
        const Component = getComponent(item.type);
        return <Component data={item}
                          extraProps={{
                              reducerState: reducerState,
                              navigation: props?.navigation,
                              onchangeQuantity: onchangeQuantity,
                              onTappedOfferView: onTappedOfferView,
                              onSeeLabourCost: onSeeLabourCost,
                              showNotCoveredBenefits: showNotCoveredBenefits,
                              checkAlreadyHaveMembership: checkAlreadyHaveMembership,
                              location: "Quick Repair",
                              service: selectedServiceRequestType,
                              category: "QR",
                              product: productName,
                          }} />;
    };

    return (
        <View style={styles.container}>
            <View style={{
                width: "100%",
                height: STATUS_BAR_HEIGHT,
                backgroundColor: headerBackgroundColor,
            }}>
                <StatusBar
                    barStyle={isShowCollapsibleHeader ? "light-content" : "dark-content"} translucent={true}
                    backgroundColor={headerBackgroundColor} animated={true}
                />
            </View>

            {(isLoading || reducerState.isLoadingData) ? <Loader isLoading={isLoading} /> :
                <FlatList
                    contentContainerStyle={{ paddingTop: isShowCollapsibleHeader ? headerMaxHeight : HEADER_COLLAPSED_HEIGHT }}
                    data={reducerState.screenComponents}
                    renderItem={_renderComponent}
                    showsVerticalScrollIndicator={false}
                    bounces={false}
                    keyExtractor={(item, index) => {
                        return index.toString();
                    }}
                    scrollEventThrottle={16}
                    onScroll={Animated.event(
                        [{
                            nativeEvent: {
                                contentOffset: {
                                    y: scrollY,
                                },
                            },
                        }], {
                            listener: event => {

                            },
                        })}
                />
            }

            {/*Collapsible header*/}
            <Animated.View style={{
                height: headerMaxHeight ? headerHeight : null,
                position: "absolute",
                top: STATUS_BAR_HEIGHT,
                width: "100%",
                overflow: "hidden",
            }} onLayout={(event) => {
                if (!headerMaxHeight && !isLoading) {
                    HEADER_EXPANDED_HEIGHT = event.nativeEvent.layout.height + 2;
                    setHeaderMaxHeight(event.nativeEvent.layout.height);
                }
            }}>
                <Animated.View style={{ paddingBottom: 2, backgroundColor: headerBGColor }}>
                    <Animated.View style={[styles.header, {
                        backgroundColor: headerBGColor,
                        shadowOpacity: isShowCollapsibleHeader ? headerTitleOpacity : 0.7,
                    }]}>
                        <TouchableOpacity onPress={() => {
                            onBackPress(props.navigation);
                        }}>
                            <Animated.Image source={images.backImage}
                                            style={[styles.backButton, { tintColor: isShowCollapsibleHeader ? backIconTintColor : colors.charcoalGrey }]} />
                        </TouchableOpacity>
                        <Animated.Text
                            style={[styles.headerTitle, { opacity: isShowCollapsibleHeader ? headerTitleOpacity : 1 }]}
                            numberOfLines={1}>{reducerState?.screenHeader?.title ?? ""}</Animated.Text>
                    </Animated.View>
                </Animated.View>
                <Animated.View style={{ opacity: headerChildOpacity }}>
                    {reducerState?.screenHeader?.data
                        ? <SODServiceListComponent data={reducerState?.screenHeader?.data} extraProps={{
                            reducerState: reducerState,
                            navigation: props?.navigation,
                            position: "Top",
                            checkAlreadyHaveMembership: checkAlreadyHaveMembership,
                        }} />
                        : null
                    }
                </Animated.View>
            </Animated.View>


            <SafeAreaView>
                {isShowLabourCostModal
                    ? <LabourCostScreen data={reducerState?.labourCostVM} isShowLabourCostModal={isShowLabourCostModal}
                                        onDismiss={() => {
                                            setShowLabourCostModal(false);
                                        }}
                    />
                    : null}

                {autoHeightBottomSheet.isVisible
                    ? <AutoHeightBottomSheet
                        data={autoHeightBottomSheet.data}
                        isShowFullBenefits={true}
                        bottomSheetType={autoHeightBottomSheet.type}
                        title={autoHeightBottomSheet.title}
                        description={autoHeightBottomSheet.description}
                        onDismiss={() => {
                            dismissBottomSheet();
                        }}
                    />
                    : null}

                {isShowNotifyMe.visible
                    ? <SODNotifyMeComponent
                        isNotifyMe={isShowNotifyMe.visible}
                        title={NEED_SPECIAL_ATTENTION}
                        description={isShowNotifyMe.message}
                        onSubmit={(name, email) => {
                            setShowNotifyMe({ visible: false, message: "" });
                            createLeadForSOD(name, email, 0, false);
                        }}
                        onDismiss={() => {
                            setShowNotifyMe({ visible: false, message: "" });
                        }}
                    />
                    : null}

                {reducerState?.footerVM
                    ? <View style={styles.screenFooter}>
                        <ButtonWithLoader
                            isLoading={false}
                            enable={reducerState?.footerVM?.primaryButton?.isEnabled}
                            Button={{
                                text: reducerState?.footerVM?.primaryButton?.text,
                                onClick: () => {
                                    onProceedToBook();
                                },
                            }} />
                        <Text style={styles.screenFooterDescription}>{reducerState?.footerVM?.description}</Text>
                    </View>
                    : null
                }
            </SafeAreaView>
        </View>
    );
};

SODPlanDetailScreen.navigationOptions = (navigationData) => {
    return {
        header: null,
    };
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    scrollContainer: {
        padding: 16,
    },
    title: {
        fontSize: 24,
        marginVertical: 0,
    },
    header: {
        height: 44,
        flexDirection: "row",
        alignItems: "center",
        shadowColor: colors.lightGrey,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowRadius: 4,
        shadowOpacity: 0.72,
        elevation: 2,
    },
    backButton: {
        height: 24,
        width: 24,
        tintColor: "white",
        marginHorizontal: 12,
        resizeMode: "contain",
    },
    headerTitle: {
        ...TextStyle.text_16_bold,
        marginRight: spacing.spacing12,
        color: colors.charcoalGrey,
        flex: 1,
        textAlign: Platform.OS === PLATFORM_OS.android ? "left" : "center",
    },
    screenFooter: {
        bottom: 0,
        width: "100%",
        paddingTop: spacing.spacing16,
        paddingHorizontal: spacing.spacing16,
        borderTopWidth: 1,
        borderTopColor: colors.shadow,
    },
    screenFooterDescription: {
        ...TextStyle.text_12_normal,
        color: colors.charcoalGrey,
        marginVertical: spacing.spacing4,
        textAlign: "center",
    },
});

export default SODPlanDetailScreen;
