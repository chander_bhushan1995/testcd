import React, {useContext, useEffect, useReducer, useRef, useState} from "react";
import BuyPlanReducer, {initialState} from "../Helpers/BuyPlanReducer";
import {getComponent} from "../../Catalyst/components/ComponentsFactory";
import {
    findNodeHandle,
    FlatList,
    NativeEventEmitter,
    NativeModules,
    SafeAreaView,
    StyleSheet,
    View,
} from "react-native";
import Loader from "../../Components/Loader";
import colors from "../../Constants/colors";
import BackButton from "../../CommonComponents/NavigationBackButton";
import {
    getOffersData,
    getServicePageFromCDN,
    loadIDFencePremiumPlans,
    loadIDFenceTrialPlans,
    renewMembership,
} from "../../network/APIHelper";
import * as ActionTypes from "../../Constants/ActionTypes";
import ScrollAnimatedFloatingButton from "../Components/ScrollAnimatedFloatingButton";
import {
    Category,
    Destination,
    LoginForScreen,
    NumberVerifyAlerts,
    PlanServices,
    Services,
    SOMETHING_WRONG,
} from "../../Constants/AppConstants";
import { stringConstants } from "../../CardManagement/DataLayer/StringConstants";
import { CommonContext } from "../../CommonComponents/Index/index.common";
import {APIData} from '../../../index';
import { getObjectByLabel, getParamsForPaymentFlow, onBackPress } from "../Helpers/BuyPlanUtils";
import { useBackButton } from "../Helpers/CustomHooks";
import { logWebEnageEvent } from "../../commonUtil/WebengageTrackingUtils";

import * as WebEngageEventsName from "../../Constants/WebengageEvents";
import {CALCULATE_RISK, PRODUCT_SERVICE_SCREENVIEW} from "../../Constants/WebengageEvents";
import * as WebEngageKeys from "../../Constants/WebengageAttrKeys";
import {CATEGORY, LOCATION, PAGE_PLAN_Code, PAGE_PLAN_NAME, PRODUCT, SERVICE} from "../../Constants/WebengageAttrKeys";
import {ButtonsAction, handleBuyPlanBtnActions} from "../Helpers/BuyPlanButtonActions";
import * as EventLocation from "../../Constants/EventLocation";
import {VARIATIONID} from "../../Constants/WebengageAttrKeys";


const nativeBridgeRef = NativeModules.ChatBridge;
let apiArray = [];
let BOTTOM_OFFSET = 0;

const eventEmitter = new NativeEventEmitter(nativeBridgeRef);
let deepLinkRouted = true;
let commonEventData = {};


const ServiceDetailScreen = props => {
    const [isLoading, setIsLoading] = useState(true);
    const [reducerState, dispatch] = useReducer(BuyPlanReducer, initialState);
    const [isShowBottomTooltip, setShowBottomTooltip] = useState(null);
    const [scrollableTooltipTargetLayout, setTooltipTargetLayout] = useState(null);
    const [floatingActionData, setFloatingActionData] = useState(null);
    const [isBuyIDFenceEventReceived, setBuyIDFenceEventReceived] = useState(false);
    const { Alert, BlockingLoader } = useContext(CommonContext);
    const buyPlanJourney = props.navigation.getParam("buyPlanJourney") ?? {};
    const flatListRef = useRef(null);
    const FLOATING_ACTION_OFFSET = 20;
    BOTTOM_OFFSET = FLOATING_ACTION_OFFSET + (scrollableTooltipTargetLayout?.height ?? 0);

    useBackButton(() => {
        onBackPress(props.navigation);
    });

    useEffect(() => {
        nativeBridgeRef.getAllMemberships((params) => {
            dispatch({type: ActionTypes.SET_MEMBERSHIPS, data: params, isInitiallyMemSet: true});
        });

        eventEmitter.addListener("BuyIDFenceFromEventRiskCalculator", params => {
            setBuyIDFenceEventReceived(true);
        });

        return () => {
            eventEmitter.removeListener("BuyIDFenceFromEventRiskCalculator", () => {
            });
        };
    }, []);

    useEffect( () => {
        if (reducerState.isInitiallyMemSet) {
            callAllAPI();
        }
    }, [reducerState.isInitiallyMemSet]);

    useEffect(() => {
        if (isBuyIDFenceEventReceived) {
            buyIDFencePlan();
            setBuyIDFenceEventReceived(false);
        }
    }, [isBuyIDFenceEventReceived]);

    useEffect(() => {
        if (!reducerState?.isLoadingData) {
            if ((reducerState?.idFenceTrailPlan || reducerState?.idFencePremiumPlan)) {
                if (buyPlanJourney.category === Category.finance && buyPlanJourney.service === PlanServices.idFence) {
                    let idFenceEventData = {};
                    let plan = reducerState?.idFenceTrailPlan ?? reducerState?.idFencePremiumPlan;
                    idFenceEventData[PAGE_PLAN_Code] = plan?.planCode;
                    idFenceEventData[PAGE_PLAN_NAME] = plan?.planName;
                    idFenceEventData[WebEngageKeys.ALREADY_HAS_PLAN] = reducerState?.alreadyHasPlan;
                    idFenceEventData[WebEngageKeys.VARIATIONID] = reducerState?.eventPageVariationId ?? "";
                    logWebEnageEvent(WebEngageEventsName.PLAN_DETAIL_PAGE_LOAD, idFenceEventData);

                    if (buyPlanJourney.silentRouteToRC) {
                        routeToRiskCalculator();
                    }
                }
            } else if (buyPlanJourney.silentRouteTo) {
                deepLinkRouted = false;
            }
        }
    }, [reducerState.isLoadingData]);

    useEffect(() => {
        if (!isLoading && !reducerState.isLoadingData) {
            if (reducerState.screenComponents && reducerState.screenComponents.length > 0) {
                routeTo(buyPlanJourney.silentRouteTo)
            }
        }
    })

    const scrollToIndex = (index) => {
        flatListRef.current.scrollToIndex({animated: true, index: index});
    };

    const _renderComponent = ({item, index}) => {
        const Component = getComponent(item.type);
        return <Component data={item}
                          extraProps={{
                              scrollToIndex: scrollToIndex,
                              measureFloatActionTargetLayout: measureFloatActionTargetLayout,
                              renewIDFenceMethod: renewIDFenceMembership,
                              routeToRiskCalculator: routeToRiskCalculator,
                              reducerState: reducerState,
                              navigation: props?.navigation,
                              buyPlanJourney: buyPlanJourney,
                              location: "Product Service Screen",
                              category: buyPlanJourney?.category,
                              service: buyPlanJourney?.service,
                              product: buyPlanJourney?.product?.name ?? buyPlanJourney?.productService,
                              eventPageVariationId: reducerState.eventPageVariationId
                          }}/>;
    };

    const measureFloatActionTargetLayout = (targetRef, floatingActionData) => { // set layout of target view according to which floating action button will be visible
        if (!scrollableTooltipTargetLayout) {
            targetRef.measureLayout(findNodeHandle(flatListRef.current), (x, y, width, height) => {
                setTooltipTargetLayout({x: x, y: y, width: width, height: height});
                setFloatingActionData(floatingActionData);
            });
        }
    };

    const handleScroll = (event) => { // handle scroll of flat list
        if (scrollableTooltipTargetLayout) {
            if (event.nativeEvent.contentOffset.y > (scrollableTooltipTargetLayout.y + scrollableTooltipTargetLayout.height)) {
                if (!isShowBottomTooltip) {
                    setShowBottomTooltip(true);
                }
            } else {
                if (isShowBottomTooltip) {
                    setShowBottomTooltip(false);
                }
            }
        }
    };

    const callAllAPI = async () => {
        let isTrailPlanRequired = false
        if (buyPlanJourney.category == Category.finance && buyPlanJourney.service == Services.idFence) {
            let trailPlans = null
            let premiumPlans = null
            if (!reducerState.isIDFenceTrialAvailable && !reducerState.isIDFencePremiumAvailable) { // load trail plans with mapped plans for upgrade
                trailPlans = await getIDFenceTrailPlans()
                isTrailPlanRequired = (trailPlans?.data ?? []).length > 0 // if trail plans are coming from backend
            }
            premiumPlans = await getIDFencePremiumPlans((trailPlans?.data ?? []).first()?.planCode)
            dispatch({
                type: ActionTypes.REQUEST_IDFENCE_PLANS_DATA,
                data: {
                    idFencePremiumPlansResponse: premiumPlans,
                    idFenceTrialPlansResponse: trailPlans,
                },
            });
        }

        apiArray = [loadOffersData(), loadServicePageFromCDN(isTrailPlanRequired)];
        const [offerResponse, servicePageResponse] = await Promise.all(apiArray)
        dispatch({type: ActionTypes.REQUEST_SCREEN_COMPONENTS,
            data: { components: servicePageResponse.data,
                offerData:  {offerResponse: offerResponse, buyPlanJourney: buyPlanJourney},
            },
            pageVariationId: servicePageResponse.pageVariationId
        });
        apiArray = [];
        setIsLoading(false);
    };

    const getIDFenceTrailPlans = async () => {
       return await new Promise((resolve, reject) => {
            loadIDFenceTrialPlans(buyPlanJourney?.category, PlanServices.idFence, (response, error) => {
                if (error) {
                    Alert.showAlert("Alert", error?.error[0]?.message ?? SOMETHING_WRONG, {
                        text: stringConstants.ok,
                        onClick: () => {
                            onBackPress(props.navigation);
                        },
                    });
                    reject()
                } else {
                    resolve(response)
                }
            });
        });
    };

    const getIDFencePremiumPlans = async (trialPlanCode) => {
        return await new Promise(((resolve, reject) => {
            loadIDFencePremiumPlans(buyPlanJourney?.category, PlanServices.idFence, trialPlanCode, (response, error) => {
                if (error) {
                    Alert.showAlert("Alert", error?.error[0]?.message ?? SOMETHING_WRONG, {
                        text: stringConstants.ok,
                        onClick: () => {
                            onBackPress(props.navigation);
                        },
                    });
                    reject()
                } else {
                    resolve(response);
                }
            });
        }));
    };

    const loadOffersData = async () => { // get offers data from API/CDN
      return await new Promise((resolve, reject) => {
            getOffersData((response, error) => {
                if (error) {
                    resolve(null);
                    return;
                }
                resolve(response?.data);
            });
        });
    };

    const loadServicePageFromCDN = async (isTrailPlanRequired) => { // get screen components list
        return await new Promise((resolve, reject) => {
            getServicePageFromCDN(isTrailPlanRequired,buyPlanJourney?.category, buyPlanJourney?.service, buyPlanJourney?.product?.name, (response, error) => {
                if (error) {
                    Alert.showAlert("Alert", error?.message, {
                        text: stringConstants.ok,
                        onClick: () => {
                        },
                    });
                }
                commonEventData = {};
                commonEventData[CATEGORY] = buyPlanJourney?.category;
                commonEventData[PRODUCT] = buyPlanJourney?.product?.name;
                commonEventData[SERVICE] = buyPlanJourney?.service;
                commonEventData[VARIATIONID] = response?.pageVariationId ?? "";
                logWebEnageEvent(PRODUCT_SERVICE_SCREENVIEW, commonEventData);
                resolve(response ?? []);
            });
        });
    };

    const renewIDFenceMembership = () => {
        BlockingLoader.startLoader("Please wait...");
        renewMembership(reducerState?.idFencePremiumPlan?.planCode, reducerState?.idFenceTrialMembership, (response, error) => {
            BlockingLoader.stopLoader();
            if (error) {
                Alert.showAlert("Alert", error?.message, {
                    text: stringConstants.ok,
                    onClick: () => {
                    },
                });
            } else {
                setTimeout(() => {
                    nativeBridgeRef.openIDFencePlanDetails(true, response?.data?.payNowLink);
                }, 200);
            }
        });
    };

    const buyIDFencePlan = () => {
        let plan = reducerState?.idFenceTrailPlan;
        let premiumPlan = reducerState?.idFencePremiumPlan;
        if (reducerState?.isIDFenceForRenewal) {
            renewIDFenceMembership();
        } else {
            nativeBridgeRef.showPopupForVerifyNumber(NumberVerifyAlerts.buyPlan.message, "", LoginForScreen.SALES, status => {
                if (status) {
                    if (reducerState?.idFenceTrailPlan && reducerState?.idFencePremiumPlan) { // trail purchase
                        nativeBridgeRef.openPaymentModule(getParamsForPaymentFlow(Destination.idfance, plan, APIData, {
                            siPlanCode: premiumPlan.planCode,
                            paymentOptions: plan?.paymentOps,
                            productName: buyPlanJourney?.product?.name,
                            productCode: "WP01",
                        }));
                    } else { // premium purchase
                        nativeBridgeRef.openPaymentModule(getParamsForPaymentFlow(Destination.idfance, premiumPlan, APIData, {
                            productCode: "WP01",
                            productName: buyPlanJourney?.product?.name
                        }));
                    }
                }
            });
        }
    };

    const routeToRiskCalculator = () => {
        let isSIEnabled = reducerState?.idFenceTrialMembership?.isSIEnabled;
        let hasTrial = reducerState?.isIDFenceTrialAvailable
        let hasPremium = reducerState?.isIDFencePremiumAvailable;
        let eventData = {};
        eventData[WebEngageKeys.LOCATION] = hasTrial ? "ID Fence Trial" : "ID Fence Premium";
        eventData[WebEngageKeys.VARIATIONID] = reducerState?.eventPageVariationId ?? "";
        logWebEnageEvent(CALCULATE_RISK, eventData);

        nativeBridgeRef.openRiskCalculator({
            custId: APIData.customer_id,
            hasATrial: hasTrial,
            hasAPremium: hasPremium,
            forPlan: reducerState?.idFenceTrailPlan ?? reducerState?.idFencePremiumPlan,
            isProd: APIData.isProd,
            isIDFenceForRenewal: reducerState?.isIDFenceForRenewal ?? false,
            isSIEnabled: isSIEnabled,
        });
    };

    const routeTo = (routeData) => {
        if (!deepLinkRouted) {
            if (reducerState.screenComponents) {
                if (routeData.action === ButtonsAction.SHOW_SECTION_BOTTOM_SHEET) {
                    let actionObj = getObjectByLabel(reducerState.screenComponents, "action", ButtonsAction.SHOW_SECTION_BOTTOM_SHEET);
                    if (actionObj) {
                        let eventData = {...commonEventData};
                        eventData[LOCATION] = EventLocation.DEEPLINK;

                        let data = {...actionObj.data, initialSelectedIndex: routeData.initialSelectedIndex};
                        handleBuyPlanBtnActions(actionObj.action, {
                            ...eventData,
                            navigation: props?.navigation,
                            data: data
                        });
                    }
                }
            }
            deepLinkRouted = true;
        }
    };
    return (
        <SafeAreaView style={styles.screen}>

            <FlatList
                contentContainerStyle={{ paddingBottom: isShowBottomTooltip ? 0 : 0 }}
                ref={flatListRef}
                data={reducerState.screenComponents}
                renderItem={_renderComponent}
                showsVerticalScrollIndicator={false}
                bounces={true}
                keyExtractor={(item, index) => {
                    return index.toString();
                }}
                scrollEventThrottle={16}
                onScroll={handleScroll}
                extraData={reducerState}
            />

            <View style={{position: "absolute", bottom: 32, width: "100%"}}>
                {isShowBottomTooltip == null || isShowBottomTooltip == undefined
                    ? null
                    : <ScrollAnimatedFloatingButton isVisible={isShowBottomTooltip} data={floatingActionData}
                                                    bottomOffset={FLOATING_ACTION_OFFSET}
                                                    extraProps={{
                                                        scrollToIndex: scrollToIndex,
                                                        renewIDFenceMethod: renewIDFenceMembership,
                                                        measureFloatActionTargetLayout: measureFloatActionTargetLayout,
                                                        reducerState: reducerState,
                                                        navigation: props?.navigation,
                                                        buyPlanJourney: buyPlanJourney,
                                                        location: "Product Service Screen",
                                                        category: buyPlanJourney?.category,
                                                        service: buyPlanJourney?.service,
                                                        product: buyPlanJourney?.product?.name,
                                                        eventPageVariationId: reducerState.eventPageVariationId,
                                                        routeToRiskCalculator: routeToRiskCalculator,
                                                    }}/>
                }
            </View>

            {(isLoading || reducerState.isLoadingData)
                ? <Loader isLoading={(isLoading || reducerState.isLoadingData)} loaderStyle={{
                    backgroundColor: "transparent",
                    position: "absolute",
                    top: 0,
                    width: "100%",
                }}/>
                : null}


        </SafeAreaView>
    );
};


ServiceDetailScreen.navigationOptions = (navigationData) => {
    return {
        title: navigationData.navigation.state?.params?.buyPlanJourney?.screenTitle,
        headerLeft: <BackButton onPress={() => {
            onBackPress(navigationData.navigation);
        }}/>,
    };
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: colors.color_F3F2F7,
    },
});

export default ServiceDetailScreen;
