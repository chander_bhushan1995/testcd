import React from 'react';
import {
    View,
    StyleSheet,
    Text,
    BackHandler,
    Image,
    Dimensions,
    TouchableOpacity, SafeAreaView,
} from "react-native";
import WebView from 'react-native-webview';
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import {TextStyle} from "../../Constants/CommonStyle";
import {OAImageSource} from "../../Constants/OAImageSource";
import Loader from "../../Components/Loader";
import { onBackPress } from "../Helpers/BuyPlanUtils";

let videoURLParams = "?autoplay=1&mute=1&showinfo=0&controls=1&fullscreen=0";
let videoURLWithParams = "";
let navigator;
let headerTitle = "Video";

export default class VideoViewComponent extends React.Component {

    static navigationOptions = ({navigation}) => {
        navigator = navigation;
    };

    constructor(p) {
        super(p);
        const {navigation} = this.props;
        let videoUrl = navigation.getParam('videoURL', null)
        headerTitle = navigation.getParam('headerTitle', "Video")
        if (videoUrl !== null && videoUrl !== undefined) {
            videoURLWithParams = videoUrl + videoURLParams;
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }

    getActivityIndicatorLoadingView() {
        return (
            <Loader isLoading={true} loadingMessage={""} loaderStyle={{backgroundColor: colors.charcoalGrey}}/>
        );
    }

    render = () => {
        return (
            <SafeAreaView style={style.container}>
                <View style={style.headerContainer}>
                    <Text style={style.textHeaderStyle}>{headerTitle}</Text>
                    <TouchableOpacity onPress={() => {
                        this.onBackPress();
                    }}>
                        <Image
                            source={OAImageSource.arrow_down}/>
                    </TouchableOpacity>
                </View>
                <WebView
                    style={style.webViewStyle}
                    startInLoadingState={false}
                    allowFileAccess={false}
                    javaScriptEnabled={true}
                    scrollEnabled={false}
                    allowsInlineMediaPlayback={true}
                    useWebKit={true}
                    allowsFullscreenVideo={false}
                    domStorageEnabled={true}
                    originWhitelist={["*"]}
                    source={{
                        html: `<html>
                          <body style="background-color: black;">
                            <video
                            autoplay="autoplay"
                            loop="loop"
                            playsinline
                            oncontextmenu="return false;"
                            preload="auto"
                            preload="yes"
                            loop
                            style="margin-top:${Dimensions.get('screen').height/2+ 80};justify-content: center;" 
                            width="100%" 
                            height="auto" 
                            controls>
                              <source src=${videoURLWithParams} type="video/mp4">
                              Your browser does not support the video
                            </video>

                          </body>
                        </html>`,
                    }}
                />
            </SafeAreaView>
        );
    }
    onShouldStartLoadWithRequest = (navigator) => {
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    onBackPress = () => {
        return  onBackPress(this.props.navigation);
    };
}
const style = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: colors.black,
    },
    headerContainer: {
        marginTop: spacing.spacing40,
        marginBottom: spacing.spacing20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: spacing.spacing16,
    },
    textHeaderStyle: {
        ...TextStyle.text_16_bold,
        color: colors.white,
    },
    webViewStyle: {
        backgroundColor: colors.charcoalGrey,
        justifyContent: 'center',
        alignItems: 'center',
        flex: spacing.spacing1
    },
});
