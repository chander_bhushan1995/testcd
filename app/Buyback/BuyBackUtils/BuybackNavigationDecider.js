import {buybackManagerObj} from '../../Navigation/index.buyback';
import {APIData} from '../../../index';
import NumberUtils from '../../commonUtil/NumberUtils';
import {NativeModules, Platform} from 'react-native';
import {checkIMEIPermission} from '../../commonUtil/PermissionUtils';
import {
    ABB_DIALOG_DUE_TO_DEVICE_AGE, ABB_DIALOG_TEXT, buybackStatusConstants, DL_BUYBACK_POST_PICKUP,
    DL_BUYBACK_SCHEDULE_PICKUP, DL_BUYBACK_START_MHC,
    DL_BUYBACK_TIMELINE,
} from '../../Constants/BuybackConstants';
import * as Store from '../../commonUtil/Store'
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

const POSSIBLE_BUYBACK_ROUTES = {   //possible initial flows
    BUYBACK_COMPONENT: 'BuybackComponent',
    BUYBACK_TIMELINE: 'BuybackRequestSuccessTimeline',
    BUYBACK_MULTIPLE_REQUESTS: 'BuybackMultipleServiceRequest',
};

// default route name and params
let routeAndParams = {routeName: POSSIBLE_BUYBACK_ROUTES.BUYBACK_COMPONENT, params: {}};
let dialogViewRef = null;
let decideCompletionHandler = null;
let abbNotEligibleMsg = ABB_DIALOG_TEXT;

const nativeBridgeRef = NativeModules.ChatBridge;

export function decideFlow(dialogeRef, callback) {
    dialogViewRef = dialogeRef;
    decideCompletionHandler = callback;
    nativeBridgeRef.getUserInfo(userInfo => {
        buybackManagerObj.setUserInfo(userInfo);
    });
    Store.getDeeplinkUrl((error, value) => {
        if (value !== null) {
            Store.setDeeplinkUrl('')
            let parse = require('url-parse');
            let url = parse(value, true);
            if (value.includes(DL_BUYBACK_TIMELINE)) {
                openBuybackComponentFromDeeplink(DL_BUYBACK_TIMELINE, url.query.quoteId);
            } else if (value.includes(DL_BUYBACK_SCHEDULE_PICKUP)) {
                openBuybackComponentFromDeeplink(DL_BUYBACK_SCHEDULE_PICKUP, url.query.quoteId);
            } else if (value.includes(DL_BUYBACK_POST_PICKUP)) {
                openBuybackComponentFromDeeplink(DL_BUYBACK_POST_PICKUP, url.query.quoteId);
            } else if (value.includes(DL_BUYBACK_START_MHC)) {
                openBuybackComponentFromDeeplink(DL_BUYBACK_START_MHC, url.query.quoteId);
            } else {
                openBuybackFlowWithDefaultData();
            }
        } else {
            openBuybackFlowWithDefaultData();
        }
    });
}

function openBuybackComponentFromDeeplink(deeplink, quoteId) {
    let buybackdata = buybackManagerObj.getBuybackStatusData();
    let filteredData = buybackdata.filter((value) => {
        return value.quoteId === quoteId;
    });
    if (filteredData.length === 0) {
        decideComponent(buybackdata);
    } else {
        let isDeeplink = false;
        if (filteredData[0].buyBackStatus === buybackStatusConstants.ORDER_PLACED || filteredData[0].buyBackStatus === buybackStatusConstants.VOUCHER_GENERATED) {
            routeAndParams.routeName = POSSIBLE_BUYBACK_ROUTES.BUYBACK_TIMELINE;
            routeAndParams.params = {quoteId: filteredData[0].quoteId};
            decideCompletionHandler(routeAndParams);
        } else {
            let buybackStatus = filteredData[0].buyBackStatus;
            if (buybackStatus === buybackStatusConstants.QUOTE_GENERATED && deeplink === DL_BUYBACK_SCHEDULE_PICKUP) {
                isDeeplink = true;
            } else if ((buybackStatus === buybackStatusConstants.SERVICEABILITY_CHECKED || buybackStatus === buybackStatusConstants.QUOTE_CREATED) && deeplink === DL_BUYBACK_START_MHC) {
                isDeeplink = true;
            }
            buybackManagerObj.setBuybackStatusData(filteredData);
            routeAndParams.routeName = POSSIBLE_BUYBACK_ROUTES.BUYBACK_COMPONENT;
            routeAndParams.params = {isDeeplink: isDeeplink, deeplink: deeplink};
            decideCompletionHandler(routeAndParams);
        }
    }
}

function decideComponent(buybackdata) {
    let props = APIData;
    if (props.openSuccessTimeline) {
        let filteredData = buybackdata.filter((value) => {
            return value.buyBackStatus === buybackStatusConstants.ORDER_PLACED && value.membershipId == props.membership_id;
        });
        if (filteredData?.length > 0) {
            routeAndParams.routeName = POSSIBLE_BUYBACK_ROUTES.BUYBACK_TIMELINE;
            routeAndParams.params = {quoteId: filteredData[0].quoteId};
            decideCompletionHandler(routeAndParams);
        }
        buybackManagerObj.setIsABBFlow(true);
    } else {
        checkABBFlowConditions(buybackdata);
    }
}

function checkABBFlowConditions(buybackdata) {
    getImei((imei) => {
        if (!imei) {
            nativeBridgeRef.goBack();
            return;
        }
        buybackManagerObj.setImei(imei);
        let data = checkIMEIInMembershipResponse(imei);
        let membershipId = data?.membershipId;
        if (membershipId && membershipId.length > 0 && data?.enableFileClaim) {
            if (buybackManagerObj.isABBFlowFailOnce()) {
                buybackManagerObj.setIsABBFlow(false);
            } else {
                buybackManagerObj.setIsABBFlow(true);
            }
            buybackManagerObj.setMembershipId(membershipId);
            let filteredData = buybackdata.filter((value) => {
                return value.membershipId === membershipId;
            });
            if (filteredData?.length > 0) {
                buybackManagerObj.setBuybackStatusData(filteredData);
                processBuybackFlow(filteredData);
            } else {
                let filteredDataNotStarted = buybackdata.filter((value) => {
                    return value.buyBackStatus === buybackStatusConstants.BUYBACK_NOT_STARTED;
                });
                if (filteredDataNotStarted?.length > 0) {
                    buybackManagerObj.setBuybackStatusData(filteredDataNotStarted);
                    processBuybackFlow(filteredDataNotStarted);
                } else {
                    processBuybackFlow(buybackdata);
                }
            }
        } else if (membershipId && membershipId.length > 0 && !data?.enableFileClaim) {
            buybackManagerObj.setMembershipId(membershipId);
            let filteredData = buybackdata.filter((value) => {
                return value.membershipId === membershipId;
            });
            if (filteredData?.length > 0 && (filteredData[0]?.buyBackStatus === buybackStatusConstants.ORDER_PLACED || filteredData[0]?.buyBackStatus === buybackStatusConstants.VOUCHER_GENERATED)) {
                buybackManagerObj.setIsABBFlow(true);
                buybackManagerObj.setBuybackStatusData(filteredData);
                processBuybackFlow(filteredData);
            } else {
                showNormalBuybackDialog(buybackdata);
            }
        } else {
            showNormalBuybackDialog(buybackdata);
        }
    });
}


function showNormalBuybackDialog(value) {
    let props = APIData;
    let filteredBBData = value.filter((value) => {
        return value?.deviceInfo?.modelName === props?.modelName;
    });
    let buybackdataValue;
    if (filteredBBData?.length > 0) {
        buybackdataValue = filteredBBData[0];
    }
    buybackManagerObj.setIsABBFlow(false);
    buybackManagerObj.setMembershipId('');
    if (!haveAnyABBMembership()) {
        processBuybackFlow(value);
        return;
    }
    dialogViewRef.showDailog({
        title: 'Looks like your ' + buybackdataValue?.deviceInfo?.deviceName + abbNotEligibleMsg,
        message: 'You can still get upto ₹ ' + NumberUtils.getFormattedPrice(buybackdataValue?.price) + ' for your device',
        imageUrl: require('../../images/icon_warning.webp'),
        primaryButton: {
            text: 'Continue to sell', onClick: () => {
                processBuybackFlow(value);
            },
        },
        secondaryButton: {
            text: 'Cancel Request', onClick: () => {
                nativeBridgeRef.goBack();
            },
        },
        cancelable: false,
        onClose: () => {
            nativeBridgeRef.goBack();
        },
    });
}

function processBuybackFlow(buybackdata) {
    if (buybackdata?.length > 1) {
        let filteredData = buybackdata.filter((value) => {
            return value.buyBackStatus !== buybackStatusConstants.MODEL_NOT_FOUND && value.buyBackStatus !== buybackStatusConstants.BUYBACK_NOT_STARTED;
        });
        let filteredBuybackNotStartedData = buybackdata.filter((value) => {
            return value.buyBackStatus === buybackStatusConstants.BUYBACK_NOT_STARTED;
        });
        routeAndParams.routeName = POSSIBLE_BUYBACK_ROUTES.BUYBACK_MULTIPLE_REQUESTS;
        routeAndParams.params = {buybackdata: filteredData, buybackNotStartedData: filteredBuybackNotStartedData};
        decideCompletionHandler(routeAndParams);
    } else if (buybackdata && (buybackdata[0]?.buyBackStatus === buybackStatusConstants.ORDER_PLACED || buybackdata[0]?.buyBackStatus === buybackStatusConstants.VOUCHER_GENERATED)) {
        routeAndParams.routeName = POSSIBLE_BUYBACK_ROUTES.BUYBACK_TIMELINE;
        routeAndParams.params = {quoteId: buybackdata[0].quoteId};
        decideCompletionHandler(routeAndParams);
    } else {
        routeAndParams.routeName = POSSIBLE_BUYBACK_ROUTES.BUYBACK_COMPONENT;
        decideCompletionHandler(routeAndParams);
    }
}

function getImei(callback) {
    Store.getIMEI((error, value) => {
        if (value) {
            return callback(value);
        } else {
            dialogViewRef.showDailog({
                title: 'To continue, please verify your mobile IMEI',
                imageUrl: require('../../images/verify_imei_icon.webp'),
                primaryButton: {
                    text: 'Verify IMEI', onClick: () => {
                        if (Platform.OS === 'android' && Platform.Version < 29) {
                            checkIMEIPermission(dialogViewRef, onPermissionGrant = () => {
                                nativeBridgeRef.getApiProperty(apiProperty => {
                                    let apiProperties = parseJSON(apiProperty);
                                    let imei = apiProperties.IMEI;
                                    Store.setIMEI(imei);
                                    return callback(imei);
                                });
                            });
                        } else {
                            nativeBridgeRef.openOCRflow((imei) => {
                                Store.setIMEI(imei);
                                return callback(imei);
                            });
                        }
                    },
                },
                cancelable: false,
                onClose: () => {
                    nativeBridgeRef.goBack();
                },
            });
        }
    });
}

function checkIMEIInMembershipResponse(imei) {
    let props = APIData;
    let membershipData = parseJSON(props.membershipList);
    if (membershipData?.length > 0) {
        for (let i = 0; i < membershipData.length; i++) {
            let membership = membershipData[i];
            let assets = membership?.assets;
            if (assets && assets?.length > 0) {
                for (let j = 0; j < assets.length; j++) {
                    let asset = assets[j];
                    if (asset?.serialNo === imei || asset?.serialNo2 === imei) {
                        let servicesArray = asset?.services;
                        for (let k = 0; k < servicesArray.length; k++) {
                            let service = servicesArray[k];
                            if (service.serviceName === 'Buyback') {
                                if(props.membership_id?.length > 0){
                                    //From mem tab
                                    if (service.isServiceTenureActive &&  props.membership_id == membership?.memId) {
                                        return {
                                            membershipId: '' + membership?.memId,
                                            enableFileClaim: membership?.enableFileClaim,
                                        };
                                    } else {
                                        abbNotEligibleMsg = ABB_DIALOG_DUE_TO_DEVICE_AGE;
                                    }
                                }else{
                                    //From home tab
                                    if (service.isServiceTenureActive) {
                                        return {
                                            membershipId: '' + membership?.memId,
                                            enableFileClaim: membership?.enableFileClaim,
                                        };
                                    } else {
                                        abbNotEligibleMsg = ABB_DIALOG_DUE_TO_DEVICE_AGE;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return {membershipId: '', enableFileClaim: false};
}

function openBuybackFlowWithDefaultData() {
    let value = buybackManagerObj.getBuybackStatusData();
    if (value !== null) {
        decideComponent(value);
    }
}

function haveAnyABBMembership() {
    let props = APIData;
    let membershipData = parseJSON(props.membershipList);
    if (membershipData?.length > 0) {
        for (let i = 0; i < membershipData?.length; i++) {
            let membership = membershipData[i];
            let assets = membership?.assets;
            if (assets && assets?.length > 0) {
                for (let j = 0; j < assets?.length; j++) {
                    let asset = assets[j];
                    let servicesArray = asset?.services;
                    for (let k = 0; k < servicesArray?.length; k++) {
                        let service = servicesArray[k];
                        if (service?.serviceName === 'Buyback') {
                            return true;
                        }
                    }
                }
            }
        }
    }
    return false;
}
