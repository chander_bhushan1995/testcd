import {buybackManagerObj} from '../../Navigation/index.buyback';

export function parseBuybackQuestion(data) {
    const QuestionList=buybackManagerObj.getQuestionsList();
    let questionStepsRequired = data.questionStepsRequired;
    if (questionStepsRequired !== null) {
        let quoteQuestionList = [];
        let mhcQuestionList = [];
        let orderQuestionList = [];

        for (let i = 0; i < questionStepsRequired.length; i++) {
            let item = questionStepsRequired[i];
            //TODO After discuss with backend we have to check a diff question type for device age Jira - AD-10693
            if(item.questionType === 'DEVICE_AGE' && item?.isCompleted === false){
                let questionItem = {
                    ...QuestionList[item.questionId],
                    isCompleted: item.isCompleted,
                    isSingleChoice: item.questionFormat === "SS",
                    questionType: 'QUOTE_QUESTION'
                };
                quoteQuestionList.push(questionItem);
            }
            else if (item.questionType === 'QUOTE_QUESTION') {
                let questionItem = {
                    ...QuestionList[item.questionId],
                    isCompleted: item.isCompleted,
                    isSingleChoice: item.questionFormat === "SS"
                };
                quoteQuestionList.push(questionItem);
            }
            else if (item.questionType === 'MHC') {
                let questionItem = {
                    questionId: item.questionId,
                    answer: "",
                };
                mhcQuestionList.push(questionItem);
            }

            else if (item.questionType === 'ORDER_QUESTION') {
                if (item.questionId === "Q-ADDR" || item.questionId === 'Q-PM') {
                    let questionItem = {
                        questionId: item.questionId,
                        isCompleted: item.isCompleted,
                    };
                    orderQuestionList.push(questionItem);
                }
            }
        }
        return {
            quoteQuestionList: quoteQuestionList,
            mhcQuestionList: mhcQuestionList,
            orderQuestionList: orderQuestionList
        }
    }
    return null;
}
