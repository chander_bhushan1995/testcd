import {
    BuybackEventLocation,
    buybackFlowType,
    buybackOrderStatus,
    buybackOrderStatusDescription,
    buybackOrderStatusMessages, buybackPartnerType, buybackVoucherStatus,
} from '../../Constants/BuybackConstants';
import images from '../../images/index.image';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import NumberUtils from '../../commonUtil/NumberUtils';
import {getPreciseDistance} from "geolib";
import {ABB, EXCHANGE_PRICE, SERVED_BY, BRAND, RETAILER_COUNT} from '../../Constants/WebengageAttrKeys';
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';
import {VIEW_NEARBY_STORES} from '../../Constants/WebengageEvents';

export const getStatusIcon = (orderStatus) => {
    if (orderStatus == null || orderStatus.orderStatus == null) {
        return images.awaiting_timeline_icon;
    }
    let status = orderStatus?.orderStatus;
    if (status === buybackOrderStatus.ORDER_STATUS_OG) {
        return images.awaiting_timeline_icon;
    } else if (status === buybackOrderStatus.ORDER_STATUS_OC || status === buybackOrderStatus.ORDER_STATUS_FPU || status === buybackOrderStatus.ORDER_STATUS_OEX) {
        return images.rejected_timeline_icon;
    } else if (status === buybackOrderStatus.ORDER_STATUS_PUS || status === buybackOrderStatus.ORDER_STATUS_PUR || status === buybackOrderStatus.ORDER_STATUS_SDP || status === buybackOrderStatus.ORDER_STATUS_SPU) {
        return images.ongoing_timeline_icon;
    }
    return images.awaiting_timeline_icon;
};

export const getStatusMessage = (orderStatus) => {
    if (orderStatus == null || orderStatus.stage == null) {
        return '';
    }
    let status = orderStatus?.stage;
    if (status === buybackOrderStatus.ORDER_STATUS_OG) {
        return buybackOrderStatusMessages.AWAITING_CONFIRMATION;
    } else if (status === buybackOrderStatus.ORDER_STATUS_PUS) {
        return buybackOrderStatusMessages.PICKUP_SCHEDULED;
    } else if (status === buybackOrderStatus.ORDER_STATUS_PUR) {
        return buybackOrderStatusMessages.PICKUP_RESCHEDULED;
        ;
    } else if (status === buybackOrderStatus.ORDER_STATUS_OEX) {
        return buybackOrderStatusMessages.ORDER_EXPIRED;
    } else if (status === buybackOrderStatus.ORDER_STATUS_SDP) {
        return buybackOrderStatusMessages.PAYMENT_SUCCESSFUL;
    } else if (status === buybackOrderStatus.ORDER_STATUS_SPU) {
        return buybackOrderStatusMessages.PAYMENT_SUCCESSFUL;
    } else if (status === buybackOrderStatus.ORDER_STATUS_FPU) {
        return buybackOrderStatusMessages.DEVICE_NOT_PICKED;
    } else if (status === buybackOrderStatus.ORDER_STATUS_OC) {
        return buybackOrderStatusMessages.ORDER_CANCELLED;
    }
    return buybackOrderStatusMessages.AWAITING_CONFIRMATION;
};

export const getStatusDescription = (orderStatus) => {
    if (orderStatus == null || orderStatus.stage == null) {
        return '';
    }
    let status = orderStatus?.stage;
    let deviceName = orderStatus?.deviceName;
    let price = orderStatus ? '\u20B9 ' + NumberUtils.getFormattedPrice(orderStatus?.fulfillmentPrice) : '';
    if (status === buybackOrderStatus.ORDER_STATUS_OG) {
        if (!isThirdPartyFlow()) {
            return buybackOrderStatusDescription.DESCRIPTION_OG_ABB;
        }
        return buybackOrderStatusDescription.DESCRIPTION_OG_THIRD_PARTY;
    } else if (status === buybackOrderStatus.ORDER_STATUS_PUS || status === buybackOrderStatus.ORDER_STATUS_PUR) {
        if (!isThirdPartyFlow()) {
            return buybackOrderStatusDescription.DESCRIPTION_PUS_ABB;
        }
        return buybackOrderStatusDescription.DESCRIPTION_PUS_NON_ABB;
    } else if (status === buybackOrderStatus.ORDER_STATUS_SDP || status === buybackOrderStatus.ORDER_STATUS_SPU) {
        return buybackOrderStatusDescription.DESCRIPTION_SDP(deviceName, price);
    }
    return buybackOrderStatusDescription.DESCRIPTION_OG_ABB;
};

export const getVoucherTitle = (orderStatus) => {
    if (orderStatus == null || orderStatus.stage == null) {
        return '';
    }
    if (orderStatus?.voucherStatus === buybackVoucherStatus.REDEEMED) {
        return buybackOrderStatusMessages.VOUCHER_REDEEMED;
    }
    return buybackOrderStatusMessages.VOUCHER_GENERATED;
};

export const getVoucherDescription = (orderStatus) => {
    if (orderStatus == null || orderStatus.stage == null) {
        return '';
    }
    if (orderStatus?.voucherStatus === buybackVoucherStatus.REDEEMED) {
        return buybackOrderStatusDescription.DESCRIPTION_VOUCHER_REDEEMED(orderStatus?.voucherRedeemDate);
    }
    return '';
};

export const isExchangeFlow = () => {
    let data = buybackManagerObj.getBuybackStatusDataFirstItem();
    return data?.subType === buybackFlowType.RETAILER_PICKUP;
};

export const getResalePriceText = () => {
    let data = buybackManagerObj.getBuybackStatusDataFirstItem();
    if (data?.flow === buybackPartnerType.PARTNER_XIAOMI && !buybackManagerObj.getABBNotEligible()) {
        return 'Assured Buyback Value';
    }
    return 'Exchange Value';
};

export const isThirdPartyFlow = () => {
    let data = buybackManagerObj.getBuybackStatusDataFirstItem();
    return data?.subType === buybackFlowType.THIRD_PARTY;
};

export const getServedByValue = (subType) => {
    if (subType === buybackFlowType.THIRD_PARTY) {
        return 'Instacash';
    }
    return 'OA';
};

export const getServedByValueFromFirstItem = () => {
    let buybackData = buybackManagerObj.getBuybackStatusDataFirstItem();
    if (buybackData?.subType === buybackFlowType.THIRD_PARTY) {
        return 'Instacash';
    }
    return 'OA';
};

export const getDistance = (userCoordinate, retailersCoordinate) => {
    if (!userCoordinate || !retailersCoordinate) {
        return 0;
    }
    let distance = getPreciseDistance(userCoordinate, retailersCoordinate)
    return Number((distance / 1000).toFixed(1))
}
export const getSchedulePickupButtonText = () => {
    let buybackData = buybackManagerObj.getBuybackStatusDataFirstItem();
    if (buybackData?.subType === buybackFlowType.THIRD_PARTY) {
        return 'Schedule pickup';
    } else if (buybackData?.subType === buybackFlowType.RETAILER_PICKUP) {
        return 'View Nearby Stores';
    } else if (buybackData?.subType === buybackFlowType.BOTH) {
        return 'Continue';
    }
    return 'Schedule pickup';
};
export const sendNearbyStoresWebengageEvent = (retailerCount) => {
    let data = buybackManagerObj.getBuybackStatusDataFirstItem();
    let eventData = new Object();
    eventData['Location'] = BuybackEventLocation.BUYBACK;
    eventData[ABB] = buybackManagerObj?.isABBFlow();
    eventData[SERVED_BY] = getServedByValueFromFirstItem();
    eventData[EXCHANGE_PRICE] = data?.price;
    eventData[BRAND] = data?.deviceInfo?.brand;
    eventData[RETAILER_COUNT] = retailerCount;
    logWebEnageEvent(VIEW_NEARBY_STORES, eventData);
};

