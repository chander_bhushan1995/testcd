import { getRetailersList, getStoresList } from '../buybackActions/BuybackApiHelper';
import { buybackManagerObj } from '../../Navigation/index.buyback';
import {buybackPartnerType} from '../../Constants/BuybackConstants';

export function decideRetailersFlow(callback) {
    const userLocation = buybackManagerObj.getLocation() ?? {}
    callGetRetailerAPI(userLocation?.lat ?? 0, userLocation?.lng ?? 0, callback)
}

function callGetRetailerAPI(lat, long, callback) {
    let data = buybackManagerObj.getBuybackStatusDataFirstItem();
    let quoteId = data?.quoteId;
    const hasOrderId = data?.orderId?.length ? true : false
    const isViewOnly = data?.flow === buybackPartnerType.PARTNER_XIAOMI && hasOrderId
    let finalCallback = (response, error) => {
        if (response !== null) {
            let retailers = response?.data ?? []
            if (response?.metaData) {
                retailers = []
                response?.data.forEach((item, index) => {
                    if (item?.servicePartnerBuCode == response?.metaData?.customerPhonePurchaseData?.partnerBuCode) { // if user purchased item from same retailer
                        item.purchasedFromSame = true
                    }
                    if (response?.metaData?.preferredBusinessUnitCode == item?.servicePartnerBuCode) { // last selected retailer
                        item.isLastSelected = true
                    }
                    retailers.push(item)
                })
            }
            callback(retailers, null)
        } else {
            callback(null, error)
        }
    }
    if (isViewOnly) {
        getStoresList(lat, long, (response, error) => {
            let newResponse = response
            newResponse.data = response?.data?.map((item, index) => {
                let newItem = item
                newItem.servicePartnerCode = item?.partnerCode
                newItem.latitude = Number(item?.latitude)
                newItem.longitude = Number(item?.longitude)
                newItem.mobile = Number(item?.mobile)
                newItem.servicePartnerBuCode = item?.partnerBusinessUnitCode
                newItem.name = item?.businessUnitName
                let address = ""
                if (item?.line1?.length) {
                    address = item?.line1
                }
                if (item?.line2?.length) {
                    address = (address?.length) ? (address + " " + item?.line2) : item?.line2
                }
                newItem.address = address
                return newItem
            }) ?? []
            finalCallback(newResponse, error)
        })
    } else {
        getRetailersList(quoteId, lat, long, finalCallback)
    }
}
