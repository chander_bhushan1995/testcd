import React, {Component} from "react";
import {BackHandler, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {ButtonStyle, TextStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import ButtonWithLoader from "../../CommonComponents/ButtonWithLoader";
import PropTypes from "prop-types";

export default class ABBNotEligible extends Component {
    static propTypes = {
        heading1: PropTypes.string,
        heading2: PropTypes.string,
        buttons: PropTypes.object,
        isLoading: PropTypes.bool
    };

    static defaultProps = {
        image: require("../../images/icon_warning.webp"),
        heading1: 'Sell your phone anyway',
        heading2: 'Your phone is not eligible for assured value due to its condition. You can still sell it at a very good price.',
        buttons: undefined,
        isLoading: false,
    };

    constructor(props) {
        super(props);
    }

    onBackPress = () => {
        this.props.onClose();
        return true;
    };

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }


    render() {
        return (
            <SafeAreaView style={styles.SafeArea}>
                <View style={styles.container}>
                    <TouchableOpacity onPress={() => {
                        this.props.onClose();
                    }}>
                        <Image source={require("../../images/icon_cross.webp")} style={styles.crossIconStyle}/>
                    </TouchableOpacity>
                    <View style={styles.descContainer}>
                        <Image source={this.props.image} style={styles.errorIconStyle}/>
                        <Text style={[TextStyle.text_20_bold, styles.headingStyle]}>{this.props.heading1}</Text>
                        <Text style={[TextStyle.text_14_normal, styles.subHeadingStyle]}>{this.props.heading2}</Text>
                        <View style={[styles.nextButtonStyle]}>
                            <ButtonWithLoader
                                isLoading={this.props.isLoading}
                                enable={!this.props.isLoading}
                                Button={{
                                    text: this.props?.buttons?.primaryButton?.text,
                                    onClick: () => {
                                        this.props?.buttons?.primaryButton?.onClick()
                                    }
                                }
                                }/>
                        </View>
                        <TouchableOpacity
                            style={[styles.secondaryButton]}
                            onPress={() => {
                                this.props?.buttons?.secondaryButton?.onClick()
                            }}>
                            <Text style={ButtonStyle.TextOnlyButton}>{this.props?.buttons?.secondaryButton?.text}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const
    styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: colors.white,
            padding: spacing.spacing16,
        },
        crossIconStyle: {
            height: 16,
            width: 16,
            resizeMode: "contain",
            alignSelf: 'flex-end',
            justifyContent: "flex-end"
        },
        errorIconStyle: {
            height: spacing.spacing48,
            width: spacing.spacing48,
            resizeMode: "contain",
            alignSelf: 'center',
            justifyContent: "center"
        },
        headingStyle: {
            marginTop: spacing.spacing36,
            textAlign: 'center'
        },
        subHeadingStyle: {
            marginTop: spacing.spacing12,
            textAlign: 'center'
        },
        submitButtonStyle: {
            padding: spacing.spacing12,
            borderRadius: 2,
            height: 48,
            alignItems: 'center',
        },
        descContainer: {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginLeft: spacing.spacing16,
            marginRight: spacing.spacing16,
        },
        nextButtonStyle: {
            width: "100%",
            borderRadius: 2,
            marginTop: spacing.spacing56,
        },
        SafeArea: {
            flex: 1,
            backgroundColor: colors.white
        },
        secondaryButton: {
            marginTop: spacing.spacing24,
        },

    });
