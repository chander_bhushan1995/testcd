import React, {Component} from 'react';
import {
    FlatList,
    Platform,
    StyleSheet,
    Text,
    View,
    BackHandler,
    ScrollView,
} from 'react-native';
import {TextStyle} from '../../Constants/CommonStyle';
import spacing from '../../Constants/Spacing';
import colors from '../../Constants/colors';
import {HeaderBackButton} from 'react-navigation';
import DialogView from '../../Components/DialogView';
import {MOBILE_BUYBACK} from '../../Constants/WebengageAttributes';
import {ADDITIONAL_DETAILS_SUBMIT, BUYBACK_QUOTE_GEN} from '../../Constants/WebengageEvents';
import {logAppsFlyerEvent, logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import {ABB, SERVED_BY} from '../../Constants/WebengageAttrKeys';
import {getServedByValueFromFirstItem} from '../BuyBackUtils/BuybackUtils';
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";
import ImageCollectionAlert from './ImageCollectionAlert';

let _this;
let currentIndex = 0;
let totalQuestions = 0;
let questionsType;
let heading = 'Tell us a little more about your mobile.';
let isHeadingChanged = false;
let answerData = new Object();
let answerDataForWebengage = new Object();
let isAddInvoiceQuestion = false;
export default class BuyBackQuestionList extends Component {

    static navigationOptions = ({navigation}) => {
        let title;
        title = typeof (navigation.state.params) === 'undefined' || typeof (navigation.state.params.title) === 'undefined' ? 'Mobile Condition' : navigation.state.params.title;
        return {
            headerTitle: <View style={Platform.OS === 'ios' ? {alignItems: 'center'} : {alignItems: 'flex-start'}}>
                <Text style={TextStyle.text_16_normal}>{title}</Text>
            </View>,
            headerLeft: <HeaderBackButton tintColor={colors.black}
                                          onPress={() => {
                                              navigation.pop();
                                          }}/>,
            headerTitleStyle: {color: colors.white},
            headerTintColor: colors.white,
        };
    };

    constructor(props) {
        super(props);
        this.state = {autoScroll: true};
        _this = this;
        this.props.resetData();
        questionsType = '';
    }

    onBackPress = () => {
        this.props.navigation.pop();
        return true;
    };

    componentDidMount() {
       this.props.navigation.addListener(
            'didFocus', payload => {
                let currentQuestionType = this.props.navigation?.getParam('params')?.currentQuestionType;
               if(!questionsType || questionsType !== currentQuestionType){
                   questionsType = currentQuestionType;
                   if (currentQuestionType === 'QUOTE_QUESTION') {
                       this.props.navigation.setParams({title: 'Mobile condition'});
                       heading = 'Tell us a little more about your mobile.';
                   } else {
                       this.props.navigation.setParams({title: 'Schedule pickup'});
                       heading = '';
                   }
                   if (currentQuestionType === 'ORDER_QUESTION') {
                       this.props.getManupulatedData1(this.props.navigation.getParam('params').questionData);
                       heading = '';
                   } else {
                       totalQuestions = this.props.navigation.getParam('params').questionData.length;
                       this.props.getManupulatedData(this.props.navigation.getParam('params').questionData);
                       heading = 'Tell us a little more about your mobile.';
                   }
               }
            }
        )
        isAddInvoiceQuestion = false;
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        currentIndex = 0;
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    getNextQuestionView() {
        if (this.props.nextQuestionInfo.shouldShow) {
            return <View style={styles.nextQuestion}>
                <View style={{flexDirection: 'row', justifyContent: 'flex-start', marginBottom: 50, marginRight: 20}}>
                    <Text
                        style={[TextStyle.text_14_bold, styles.questionNumberStyle]}>{this.props.nextQuestionInfo.questionNumber}</Text>
                    <Text
                        style={[TextStyle.text_16_bold, styles.questionTextStyle]}>{this.props.nextQuestionInfo.title}</Text>
                </View>
            </View>;
        } else {
            return null;
        }
    }

    onSaveQuestions(questionAndAnswers, shouldMoveAnotherScreen) {
        try {
            answerDataForWebengage['Location'] = MOBILE_BUYBACK;
            answerDataForWebengage[ABB] = buybackManagerObj?.isABBFlow();
            answerDataForWebengage[SERVED_BY] = getServedByValueFromFirstItem();
            logWebEnageEvent(ADDITIONAL_DETAILS_SUBMIT, deepCopy(answerDataForWebengage));
            logAppsFlyerEvent(BUYBACK_QUOTE_GEN, {})
        } catch (e) {
        }
        let data = buybackManagerObj.getBuybackStatusDataFirstItem();
        currentIndex = 0;
        _this.props.saveQuestions(questionAndAnswers, data?.quoteId, shouldMoveAnotherScreen);
    }

    shouldComponentUpdate(nextProps) {
        if (nextProps.shouldMoveAnotherScreen) {
            this.props.navigation.navigate('SuccessScreen');
            this.props.setLoadingDefault();
            // this.props.resetData();
            return false;
        } else if (this.props.isError) {
            this.DialogViewRef.showDailog({
                message: this.props.errorMessage,
                primaryButton: {
                    text: this.props.errorMessageButtonTitle, onClick: () => this.props.onCloseErrorDailog(),
                },
                cancelable: true,
                onClose: () => this.props.onCloseErrorDailog(),
            });
            return false;
        } else if (nextProps.questionType === 'ORDER_QUESTION' && !isHeadingChanged) {
            isHeadingChanged = true;
            this.props.navigation.setParams({title: 'Schedule pickup'});
            heading = '';
            return false;
        } else {
            return true;
        }
    }

    render() {
        let headingView = heading !== '' ?
            <Text style={[TextStyle.text_14_normal, styles.headingStyle]}>{heading}</Text> : null;
        return (

            <View style={styles.container}>
                <DialogView ref={ref => (this.DialogViewRef = ref)}
                            showDailog={this.props.isError}
                />
                <ImageCollectionAlert
                    ref = {ref => (this.imageAlertViewRef = ref)}
                    clickedItemAtIndex={(data, index) => this.showHintScreen(data, index)}
                />
                {headingView}
                <ScrollView ref={ref => this.scrollView = ref}
                            onContentSizeChange={(contentWidth, contentHeight) => {
                                this.scrollView.scrollToEnd({animated: true});
                            }}>
                    <View style={[{flexDirection: 'row'}, styles.container]}>
                        <FlatList
                            contentContainerStyle={{
                                flexGrow: 1,
                                marginTop: 12,
                                marginRight: Platform.OS === 'ios' ? 20 : 0,
                            }}
                            extraData={this.props.data}
                            style={styles.list}
                            showsVerticalScrollIndicator={false}
                            ref={(ref) => {
                                this.flatListRef = ref;
                            }}
                            onContentSizeChange={() => {
                                if (this.state.autoScroll) {
                                    try {
                                        if (Platform.OS === 'ios') {
                                            try {
                                                setTimeout(() => {
                                                    if (this != null && this.flatListRef != null) {
                                                        this.flatListRef.scrollToEnd();
                                                    }
                                                }, 100);
                                            } catch (e) {
                                            }
                                        } else {
                                            this.flatListRef.scrollToEnd({animated: true}, 100);
                                        }
                                    } catch (e) {
                                    }
                                }
                            }}
                            // Auto scroll to botom while added the view in list
                            onLayout={() => {
                                // need to validate for 200.
                                if (this.state.autoScroll) {
                                    try {
                                        if (Platform.OS === 'ios') {
                                            try {
                                                setTimeout(() => {
                                                    if (this != null && this.flatListRef != null) {
                                                        this.flatListRef.scrollToEnd();
                                                    }
                                                }, 100);
                                            } catch (e) {
                                            }
                                        } else {
                                            this.flatListRef.scrollToEnd({animated: true}, 100);
                                        }
                                    } catch (e) {
                                    }
                                }
                            }}
                            data={this.props.data}
                            renderItem={({item, index}) => this.renderItem(item, index)}
                            keyExtractor={(item, index) => index.toString()}

                        />

                    </View>
                </ScrollView>
                {this.getNextQuestionView()}

            </View>

        );
    }

    showImageCollection(data, index) {
        _this.imageAlertViewRef?.showDialog(data);
    }

    scrollToIndex = (index) => {

    };
    renderItem = (item, index) => {
        return <item.ComponantName key={index} data={item.componentData} index={index} onClick={this.onClick}
                                   onClickMultiChoiceItem={this.onClickMultiChoiceItem}
                                   isFinalQuestion={totalQuestions - 1 === index}
                                   navigation={this.props.navigation}
                                   isLoading={this.props.isLoading}
                                   submitEnabledData={totalQuestions - 1 === index}
                                   showImageCollection={this.showImageCollection}
                                   submitEnabled={(data) => {
                                       return this.shouldSubmitQuestionEnabled();
                                   }}
                                   updateScrolling={(enabled) => {
                                       this.setState({autoScroll: enabled});
                                   }}
                                   onSubmitQuestions={this.onSubmitQuestions}
        />;
    };

    shouldSubmitQuestionEnabled() {
        let data = answerData['Q4'];
        if (data !== undefined && data.answer === 'OPT-10PLUS') {
            return true;
        } else if (answerData['INVOICE_UPLOAD'] !== undefined && answerData['INVOICE_UPLOAD'] === true) {
            return true;
        } else {
            return false;
        }
    }

    onClick(data, childIndex, index, questionActionType) {
        if (questionActionType === 'MULTIPLE_CHOICE') {
            _this.props.updateMultipleQuestion(data, childIndex, index);
        } else if (questionActionType === 'ENTER_PINCODE') {
            totalQuestions = totalQuestions + 1;
            _this.props.updateQuestion(data, childIndex, index);
        } else if(questionActionType === 'Q-CN' || questionActionType === 'Q-ADDR'){
            // totalQuestions = totalQuestions + 1;
            _this.props.updateQuestion(data, childIndex, index);
        } else if (questionActionType === 'INVOICE_UPLOAD') {
            answerData['INVOICE_UPLOAD'] = true;
            _this.props.updateQuestion(data, childIndex, index);
        } else {
            currentIndex = index;
            if (_this.props.data[index].componentData.questionId === 'Q4' && (data[childIndex].answerID === 'OPT-03M' || data[childIndex].answerID === 'OPT-310M')) {
                if (!isAddInvoiceQuestion) {
                    isAddInvoiceQuestion = true;
                    totalQuestions = totalQuestions + 1;
                }
                _this.props.updateQuestion(data, childIndex, index, true);
            } else if (_this.props.data[index].componentData.questionId === 'Q_DEVICE_INVOICE' && (data[childIndex].answerID === 'OPT-YES' && buybackManagerObj.isABBFlow())) {
                if (!isAddInvoiceQuestion) {
                    isAddInvoiceQuestion = true;
                    totalQuestions = totalQuestions + 1;
                }
                _this.props.updateQuestion(data, childIndex, index, true);
            } else if (_this.props.data[index].componentData.questionId === 'Q4' && data[childIndex].answerID === 'OPT-10PLUS') {
                if (isAddInvoiceQuestion) {
                    isAddInvoiceQuestion = false;
                    totalQuestions = totalQuestions - 1;
                }
                _this.props.updateQuestion(data, childIndex, index, false);
            } else if (_this.props.data[index].componentData.questionId === 'Q_DEVICE_INVOICE' && data[childIndex].answerID === 'OPT-NO' && buybackManagerObj.isABBFlow()) {
                if (isAddInvoiceQuestion) {
                    isAddInvoiceQuestion = false;
                    totalQuestions = totalQuestions - 1;
                }
                _this.props.updateQuestion(data, childIndex, index, false);
            } else {
                _this.props.updateQuestion(data, childIndex, index);
            }

            if (questionsType === 'QUOTE_QUESTION') {
                if (_this.props.data[index].componentData.questionId !== 'Q-IMEI' && _this.props.data[index].componentData.questionId !== 'Q_INVOICE') {
                    answerData[_this.props.data[index].componentData.questionId] = {
                        questionId: _this.props.data[index].componentData.questionId,
                        answer: data[childIndex].answerID,
                    };
                    try {
                        let questionWebengage = _this.props.data[index].componentData.title.replace('/', '');
                        answerDataForWebengage[questionWebengage] = deepCopy(data[childIndex].answer);
                    } catch (e) {
                    }
                }
            }
        }
    }

    onSubmitQuestions() {
        // _this.props.completeQuoteQuestionsData(_this.props.data);
        let answersData = {questionType: 'QUOTE_QUESTION', data: []};
        for (let key in answerData) {
            if (key !== 'INVOICE_UPLOAD') {
                answersData.data.push(answerData[key]);
            }
        }

        _this.onSaveQuestions(answersData, true);
    }

    onClickMultiChoiceItem(data, childIndex, index) {
        currentIndex = index;
        let answers = [];
        let answersForWebEnagage = [];
        for (let i = 0; i < childIndex.length; i++) {
            if (questionsType === 'QUOTE_QUESTION') {
                answers.push(data[childIndex[i]].answerID);
            }
            answersForWebEnagage.push(data[childIndex[i]].answer);

        }
        answerData[_this.props.data[index].componentData.questionId] = {
            questionId: _this.props.data[index].componentData.questionId,
            answer: answers.toString(),
        };
        try {
            let questionWebengage = _this.props.data[index].componentData.title.replace('/', '');
            answerDataForWebengage[questionWebengage] = answersForWebEnagage.toString();
        } catch (e) {
        }
        _this.props.updateQuestion(data, childIndex, index);
    }

    componentDidUpdate(prevProps, prevState) {
        if (currentIndex > 0) {
            this.scrollToIndex(currentIndex - 1);
        }
    }

    // TODO: Manoj use this method,
    showHintScreen(hintData, selectedIndex) {
        _this.props.navigation.navigate('HintScreen', { data: hintData, selectedIndex: selectedIndex});
    }
}

const
    styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: colors.white,
        },
        headingStyle: {
            marginTop: spacing.spacing24,
            marginLeft: spacing.spacing16,
            color: colors.charcoalGrey,
        },
        list: {
            marginRight: spacing.spacing16,
            marginTop: spacing.spacing36,
            marginLeft: spacing.spacing16,
        },
        answerItemStyle: {
            padding: spacing.spacing16,
            borderRadius: spacing.spacing4,
            borderWidth: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginLeft: spacing.spacing16,
            marginBottom: spacing.spacing20,
            width: spacing.spacing150,
        },
        answerItemImageStyle: {
            width: spacing.spacing20,
            height: spacing.spacing32,
            marginBottom: spacing.spacing12,
            resizeMode: 'contain',
        },
        headerSubtitle: {
            fontSize: spacing.spacing12,
            fontFamily: 'Lato',
        },
        nextQuestion: {
            alignItems: 'flex-start',
            // height: spacing.spacing92,
            marginLeft: spacing.spacing16,
            marginRight: spacing.spacing20,
            justifyContent: 'flex-end',
        },
        questionNumberStyle: {
            width: spacing.spacing24,
            height: spacing.spacing24,
            borderRadius: 24 / 2,
            paddingTop: 3,
            backgroundColor: colors.lightGrey,
            color: colors.white,
            textAlign: 'center',
            overflow: "hidden"
        },
        questionTextStyle: {
            marginLeft: spacing.spacing16,
            color: colors.lightGrey,
        },


    });
