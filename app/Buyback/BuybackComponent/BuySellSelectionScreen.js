import React, {Component} from 'react';
import {Platform, Text, View, StyleSheet, TouchableOpacity, SafeAreaView, BackHandler} from 'react-native';
import {CommonStyle, TextStyle} from '../../Constants/CommonStyle';
import colors from '../../Constants/colors';
import spacing from '../../Constants/Spacing';
import RightArrowButton from '../../CommonComponents/RightArrowButton';
import Images from '../../images/index.image';
import {HeaderBackButton} from 'react-navigation';
import {buybackFlowStrings, buybackPartnerType} from '../../Constants/BuybackConstants';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import NumberUtils from '../../commonUtil/NumberUtils';
import {decideRetailersFlow} from '../BuyBackUtils/RetailersNavigationDecider';
import DialogView from '../../Components/DialogView';
import BlockingLoader from '../../CommonComponents/BlockingLoader';
import {MOBILE_BUYBACK} from '../../Constants/WebengageAttributes';
import {ABB, SERVED_BY} from '../../Constants/WebengageAttrKeys';
import {getServedByValue, sendNearbyStoresWebengageEvent} from '../BuyBackUtils/BuybackUtils';
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';
import {SCHEDULE_PICKUP} from '../../Constants/WebengageEvents';

export default class BuySellSelectionScreen extends Component {

    static navigationOptions = ({navigation}) => {

        return {
            headerTitle: <View style={Platform.OS === 'ios' ? {alignItems: 'center'} : {alignItems: 'flex-start'}}>
                <Text style={TextStyle.text_16_normal}></Text>
            </View>,
            headerLeft: <HeaderBackButton tintColor={colors.black}
                                          onPress={() => navigation.pop()}/>,
            headerTitleStyle: {color: colors.white},
            headerTintColor: colors.white,
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
        };
        this.openNearbyStoresScreen = this.openNearbyStoresScreen.bind(this);
        this.openSchedulePickupFlow = this.openSchedulePickupFlow.bind(this);
        this.navigateToRetailersFlow = this.navigateToRetailersFlow.bind(this);
    }

    openNearbyStoresScreen() {
        this.setState({isLoading: true});
        decideRetailersFlow(this.navigateToRetailersFlow);
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }

    componentWillMount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    onBackPress = () => {
        this.props.navigation.pop();
        return true;
    };

    /**
     * here we get response from retailers api
     * @param arrayOfRetailers
     */
    navigateToRetailersFlow(arrayOfRetailers, error) {
        this.setState({isLoading: false});
        if (error) {
            this.DialogViewRef.showDailog({
                title: 'ALERT',
                message: error,
                primaryButton: {
                    text: 'Retry', onClick: () => {
                        this.setState({isLoading: true});
                        decideRetailersFlow(this.navigateToRetailersFlow);
                    },
                },
                secondaryButton: {
                    text: 'Cancel', onClick: () => {
                    },
                },
                cancelable: false,
                onClose: () => {
                },
            });
        } else {
            sendNearbyStoresWebengageEvent(arrayOfRetailers?.length);
            if (arrayOfRetailers?.length == 0) {
                let questionsData = this.props.navigation.getParam('questionData', null);
                this.props.navigation.navigate('RetailersEmptyScreen', {questionsData: questionsData});
            } else if (arrayOfRetailers?.length > 1) {
                this.props.navigation.navigate('NearByRetailers', {params: {data: arrayOfRetailers}});
            } else {
                this.props.navigation.navigate('RetailerDetailScreen', {detailData: arrayOfRetailers[0] ?? {}});
            }
        }
    }

    openSchedulePickupFlow() {
        let buybackData = buybackManagerObj.getBuybackStatusDataFirstItem();
        let eventDataSchedulePickup = new Object();
        eventDataSchedulePickup['Location'] = MOBILE_BUYBACK;
        eventDataSchedulePickup['QuoteID'] = buybackData?.quoteId;
        eventDataSchedulePickup['Stage'] = buybackData?.buyBackStatus;
        eventDataSchedulePickup[ABB] = buybackManagerObj?.isABBFlow();
        eventDataSchedulePickup[SERVED_BY] = getServedByValue(buybackData?.subType);
        logWebEnageEvent(SCHEDULE_PICKUP, eventDataSchedulePickup);
        let data = this.props.navigation.getParam('questionData', null);
        this.props.resetQuestionsData();
        this.props.navigation.navigate('BuybackQuestions', {
            params: {
                questionData: data,
                currentQuestionType: 'ORDER_QUESTION',
            },
        });
    }

    getPrice() {
        let buybackData = buybackManagerObj.getBuybackStatusDataFirstItem();
        return '₹ ' + NumberUtils.getFormattedPrice(buybackData?.price);
    }

    getSellTitle() {
        return this.isABBAndXiaomiFlow()? buybackFlowStrings.sellButtonTitle : buybackFlowStrings.sell;
    }

    getSellDescription() {
        return this.isABBAndXiaomiFlow() ? buybackFlowStrings.xiaomiDescription : buybackFlowStrings.sellDescription;
    }

    getExchangeTitle() {
        return this.isABBAndXiaomiFlow() ? buybackFlowStrings.xiaomiExchangeTitle : buybackFlowStrings.exchange;
    }

    getExchangeDescription() {
        return this.isABBAndXiaomiFlow() ? buybackFlowStrings.xiaomiDescription : buybackFlowStrings.exchangeDescription;
    }

    getExchangeButtonTitle() {
        return this.isABBAndXiaomiFlow() ? buybackFlowStrings.xiaomiExchangeButtonTitle : buybackFlowStrings.exchangeButtonTitle;
    }

    isABBAndXiaomiFlow(){
        let buybackData = buybackManagerObj.getBuybackStatusDataFirstItem();
        return buybackData?.flow === buybackPartnerType.PARTNER_XIAOMI;
    }

    render() {
        return (
            <View style={styles.SafeArea}>
                <DialogView ref={ref => (this.DialogViewRef = ref)}/>
                <BlockingLoader
                    visible={this.state.isLoading}
                    loadingMessage={'Loading...'}
                />
                <Text
                    style={[TextStyle.text_16_bold, styles.headingStyle]}>{buybackFlowStrings.buySellSelectionTitle}</Text>
                <View style={[styles.container, {marginTop: spacing.spacing16}]}>
                    <TouchableOpacity style={{paddingTop: spacing.spacing20}} activeOpacity={1}
                                      onPress={this.openSchedulePickupFlow}>
                        <Text style={[TextStyle.text_20_bold, {marginLeft: spacing.spacing16}]}>{this.getPrice()}</Text>
                        <Text style={[TextStyle.text_16_bold, {
                            marginTop: spacing.spacing8,
                            marginLeft: spacing.spacing16,
                        }]}>{this.getSellTitle()}</Text>
                        <Text style={[TextStyle.text_14_normal, {
                            marginTop: spacing.spacing8,
                            marginLeft: spacing.spacing16,
                            marginRight: spacing.spacing12,
                        }]}>{this.getSellDescription()}</Text>
                        <View style={[CommonStyle.separator, {marginTop: spacing.spacing16}]}/>
                        <View style={styles.buttonContainer}>
                            <RightArrowButton image={Images.rightBlueArrow} text={buybackFlowStrings.sellButtonTitle}
                                              imageStyle={styles.rightArrowStyle}
                                              textStyle={styles.buttonText}
                                              style={{marginBottom: 0, alignSelf: 'flex-end'}}
                                              onPress={this.openSchedulePickupFlow}
                            />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={[styles.container, {marginTop: spacing.spacing24}]}>
                    <TouchableOpacity style={{paddingTop: spacing.spacing20}} activeOpacity={1}
                                      onPress={this.openNearbyStoresScreen}>
                        <Text style={[TextStyle.text_20_bold, {
                            marginLeft: spacing.spacing12,
                        }]}>{this.getPrice()}</Text>
                        <Text style={[TextStyle.text_16_bold, {
                            marginTop: spacing.spacing8,
                            marginLeft: spacing.spacing12,
                        }]}>{this.getExchangeTitle()}</Text>
                        <Text style={[TextStyle.text_14_normal, {
                            marginTop: spacing.spacing8,
                            marginLeft: spacing.spacing12,
                            marginRight: spacing.spacing12,
                        }]}>{this.getExchangeDescription()}</Text>
                        <View style={[CommonStyle.separator, {marginTop: spacing.spacing16}]}/>
                        <View style={styles.buttonContainer}>
                            <RightArrowButton image={Images.rightBlueArrow}
                                              text={this.getExchangeButtonTitle()}
                                              imageStyle={styles.rightArrowStyle}
                                              textStyle={styles.buttonText}
                                              style={{marginBottom: 0, alignSelf: 'flex-end'}}
                                              onPress={this.openNearbyStoresScreen}
                            />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    SafeArea: {
        flex: 1,
        backgroundColor: colors.color_F9F9F9,
        paddingTop: spacing.spacing24,
        paddingBottom: spacing.spacing12,
        paddingLeft: spacing.spacing12,
        paddingRight: spacing.spacing12,
    },
    headingStyle: {
        color: colors.black,
        marginLeft: spacing.spacing4,
        marginRight: spacing.spacing8,
    },
    container: {
        ...CommonStyle.cardContainerWithShadow,
        borderRadius: 4,
        paddingBottom: spacing.spacing12,
    },
    buttonContainer: {
        paddingHorizontal: spacing.spacing16,
        paddingTop: spacing.spacing12,
    },
    rightArrowStyle: {
        marginRight: 0,
        marginLeft: 8,
        width: 8,
        height: 12,
        resizeMode: 'stretch',
    },
    buttonText: {
        ...TextStyle.text_14_bold,
        color: colors.blue028,
    },
});
