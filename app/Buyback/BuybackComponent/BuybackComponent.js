import React, {Component} from 'react';
import {
    FlatList,
    Image,
    NativeModules,
    StyleSheet,
    Text,
    TouchableOpacity,
    SafeAreaView,
    View,
    BackHandler,
    AppState,
    Linking,
    PermissionsAndroid, NativeEventEmitter, Platform,
} from 'react-native';
import colors from '../../Constants/colors';
import {ButtonStyle, TextStyle} from '../../Constants/CommonStyle';
import spacing from '../../Constants/Spacing';
import * as StepConstants from '../../Constants/BuybackConstants';
import DialogView from '../../Components/DialogView';
import {
    BUY_BACK_STATUS_DATA,
    MHC_QUESTIONS_SAVE,
    APPLY_COUPON_DATA,
} from '../buybackActions/Constants';
import BlockingLoader from '../../CommonComponents/BlockingLoader';
import {HeaderBackButton} from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import {
    IMEI_PERMISSION,
    IMEI_PERMISSION_RATIONAL,
    LOCATION_OFF,
    NOT_SERVING, ON_DEVICE_LOCATION,
    TURN_ON_LOCATION,
} from '../../Constants/AppConstants';
import {IMPS, MOBILE_BUYBACK} from '../../Constants/WebengageAttributes';
import {
    ADDITIONAL_DETAILS,
    MHC_CHECK_NOW,
    SCHEDULE_PICKUP,
    PINCODE,
    APPLY_PROMO,
    BUYBACK_TIMELINE_SCREEN, BUYBACK_PAYMENT_DETAILS, VIEW_NEARBY_STORES,
} from '../../Constants/WebengageEvents';
import RBSheet from '../../Components/RBSheet';
import EnterPincodeLayout from './EnterPincodeLayout';
import {buybackStatusConstants, DL_BUYBACK_SCHEDULE_PICKUP} from '../../Constants/BuybackConstants';
import {DL_BUYBACK_START_MHC} from '../../Constants/BuybackConstants';
import NumberUtils from '../../commonUtil/NumberUtils';
import CouponCodeComponent from './CouponCodeComponent';
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import images from '../../images/index.image';
import TooltipComponent, {ArrowDirection} from '../../AppForAll/HomeScreen/Components/TooltipComponent';
import {checkLocationPermission} from '../../commonUtil/LocationPermissionUtils';
import {
    getSchedulePickupButtonText,
    getServedByValueFromFirstItem,
    isThirdPartyFlow, sendNearbyStoresWebengageEvent,
} from '../BuyBackUtils/BuybackUtils';
import {ABB, SERVED_BY} from '../../Constants/WebengageAttrKeys';
import {buybackFlowType} from '../../Constants/BuybackConstants';
import {decideRetailersFlow} from '../BuyBackUtils/RetailersNavigationDecider';
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

let currentQuestionType = '';
let _quoteId = '';
let buybackStatus = '';
let pincodeValue = '';
let price = '';
let cityName = '';
let mhcScore = '';
let couponCodeInfo = {};
let _this;
let infoIcon = null;
const chatBridgeRef = NativeModules.ChatBridge;
const eventEmitter = new NativeEventEmitter(chatBridgeRef);

export default class BuybackComponent extends Component {

    constructor(props) {
        super(props);
        _this = this;
        this.props.getDeviceInfo();
        this.updateTimeline();
        this.state = {
            isShowTooltip: false,
            appState: AppState.currentState,
        };
        _this = this;
    }

    extractDataBasedOnStatus(status, quoteId, deviceBrand, couponCodeInfo) {
        let currentItemIndex = 0;
        if (status === buybackStatusConstants.BUYBACK_NOT_STARTED) {
            this.props.getStatusDefaultData();
        } else if (status === buybackStatusConstants.SERVICEABILITY_CHECKED || status === buybackStatusConstants.QUOTE_CREATED) {
            currentItemIndex = 1;
            this.props.getBuyStatusData({
                quoteId: quoteId, currentItemIndex: currentItemIndex, pincode: pincodeValue, price: price,
                cityName: cityName, deviceBrand: deviceBrand, mhcScore: mhcScore,
            });
        } else if (status === buybackStatusConstants.MHC_COMPLETED) {
            currentItemIndex = 2;
            this.props.getBuyStatusData({
                quoteId: quoteId, currentItemIndex: currentItemIndex, pincode: pincodeValue, price: price,
                cityName: cityName, deviceBrand: deviceBrand, mhcScore: mhcScore,
            });
        } else if (status === buybackStatusConstants.QUESTIONS_IN_PROGRESS) {
            currentItemIndex = 2;
            this.props.getBuyStatusData({
                quoteId: quoteId, currentItemIndex: currentItemIndex, pincode: pincodeValue, price: price,
                cityName: cityName, deviceBrand: deviceBrand, mhcScore: mhcScore,
            });
        } else if (status === buybackStatusConstants.QUOTE_GENERATED) {
            currentItemIndex = 3;
            this.props.getBuyStatusData({
                quoteId: quoteId, currentItemIndex: currentItemIndex, pincode: pincodeValue, price: price,
                cityName: cityName, deviceBrand: deviceBrand, mhcScore: mhcScore,
            });
            if (!buybackManagerObj.isABBFlow() && (couponCodeInfo === null || couponCodeInfo === undefined || couponCodeInfo.isApplied == false)) {
                this.props.getCouponCode(quoteId);
            }
        }
    }

    componentDidUpdate() {
        if (this.props.type === BUY_BACK_STATUS_DATA && this.props.navigation.getParam('params') && this.props.navigation.getParam('params').isDeeplink) {
            let deeplink = this.props.navigation.getParam('params').deeplink;
            if (deeplink === DL_BUYBACK_SCHEDULE_PICKUP) {
                this.onClick(3);
            } else if (deeplink === DL_BUYBACK_START_MHC) {
                this.onClick(1);
            }
            let params = this.props.navigation.getParam('params');
            params.isDeeplink = false;
            params.deeplink = '';
            this.props.navigation.setParams(params);
            this.props.resetType();
        }
    }

    _handleAppStateChange(nextAppState) {
        _this.setState({ appState: nextAppState });
        if (nextAppState === 'active' && !buybackManagerObj.getLocation() && Platform.OS === 'ios') {
            //For location permission to get lat and lng value
            _this.getDeviceLocation((location) => { });
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);

        this.willFocus = this.props.navigation.addListener('willFocus', () => {
            this.updateTimeline();
        });
        AppState.addEventListener('change', this._handleAppStateChange);

        //below listener is specific to iOS to get lat long
        eventEmitter.addListener('willReceiveLocation', (params) => {
            this.setState({isLoading: false})
            let location = {lat: params.latitude, lng: params.longitude, pincode: params.pincode};
            buybackManagerObj.setLocation(location);
            let data = buybackManagerObj.getBuybackStatusDataFirstItem()
            if (buybackManagerObj.isABBFlow() && data?.buyBackStatus === buybackStatusConstants.BUYBACK_NOT_STARTED) {
                this.checkPincodeServiceablity(buybackManagerObj.getLocation()?.pincode);
            }
        });

        // request user location when user lands on screen
        if (!buybackManagerObj.getLocation()) {
            //For location permission to get lat and lng value
            this.getDeviceLocation((location) => {
                this.setState({isLoading: false})
                buybackManagerObj.setLocation(location);
                //call pincode api in case of ABB
                // request user location when user lands on screen
                let data = buybackManagerObj.getBuybackStatusDataFirstItem()
                if (buybackManagerObj.isABBFlow() && data?.buyBackStatus === buybackStatusConstants.BUYBACK_NOT_STARTED) {
                    this.checkPincodeServiceablity(buybackManagerObj.getLocation()?.pincode);
                }
            });
        }else{ // if location already available
            let data = buybackManagerObj.getBuybackStatusDataFirstItem()
            if (buybackManagerObj.isABBFlow() && data?.buyBackStatus === buybackStatusConstants.BUYBACK_NOT_STARTED) {
                this.checkPincodeServiceablity(buybackManagerObj.getLocation()?.pincode);
            }
        }
    }

    updateTimeline() {
        let data = buybackManagerObj.getBuybackStatusDataFirstItem();
        let brand = '';
        if (data) {
            brand = data?.deviceInfo?.brand;
            _quoteId = data?.quoteId;
            buybackStatus = data?.buyBackStatus;
            pincodeValue = parseInt(data?.pincode);
            cityName = data?.cityName;
            price = data?.price;
            mhcScore = data?.mhcScore;
            couponCodeInfo = data?.couponCodeInfo;
            this.extractDataBasedOnStatus(data?.buyBackStatus, data?.quoteId, brand, couponCodeInfo);
        }
    }

    /**
     * method to get location (lat,lng values) in callback
     * @param callback
     */
    getDeviceLocation(callback) {
        this.setState({isLoading: true})
        if (Platform.OS === 'android') {
            checkLocationPermission(this.DialogViewRef, onPermissionGrant = () => {
                chatBridgeRef.getLocation((location) => {
                    callback(location);
                });
            });
        } else { // here we are passing null in callback location will be received within event listener ( willReceiveLocation )
            chatBridgeRef.getUserLocation((params) => {
                this.setState({isLoading: false})
                this.showGoToSettingsDialoge();
            });
        }
    }

    onBackPress = () => {
        if (this.props.routeList !== undefined && this.props.routeList.length > 2) {
            this.props.navigation.pop();
        } else {
            chatBridgeRef.getUserInfo(userInfo => {
                chatBridgeRef.refreshBuybackStatus();
                chatBridgeRef.updateProfile();
                chatBridgeRef.goBack();
            });
        }
    };

    showGoToSettingsDialoge = () => {
        this.DialogViewRef.showDailog({
            title: LOCATION_OFF,
            message: ON_DEVICE_LOCATION,
            primaryButton: {
                text: TURN_ON_LOCATION, onClick: () => {
                    Linking.canOpenURL('app-settings:').then(supported => {
                        if (supported) {
                            return Linking.openURL('app-settings:');
                        }
                    }).catch(err => console.error('An error occurred', err));
                },
            },
            secondaryButton: {
                text: 'Later', onClick: () => {
                    chatBridgeRef.goBack();
                },
            },
            checkBox: null,
            cancelable: false,
            imageUrl: images.area_not_servicable,
            isCrossEnabled: false,
            onClose: () => {
            },
        });
    };

    showErrorDialog = (message, buttonTitle) => {
        if (this.DialogViewRef !== undefined) {
            this.DialogViewRef.showDailog({
                message: message,
                primaryButton: {
                    text: buttonTitle, onClick: () => this.props.onCloseErrorDailog(),
                },
                cancelable: true,
                onClose: () => this.props.onCloseErrorDailog(),
            });
        }
    }
    
    showPinCodeErrorDialoge = () => {
        this.DialogViewRef.showDailog({
            title: NOT_SERVING,
            message: null,
            primaryButton: {
                text: 'Continue', onClick: () => {
                    _this.pincodeViewRef.open();
                },
            },
            secondaryButton: {
                text: 'Cancel', onClick: () => {
                },
            },
            checkBox: null,
            cancelable: false,
            imageUrl: images.area_not_servicable,
            isCrossEnabled: false,
            onClose: () => {
            },
        });
    };

    checkPincodeServiceablity(pincode) {
        this.props.checkPincodeServiceablity(pincode, price);
    }

    FlatListItemSeparator = () => {
        return (
            <Image style={styles.stepsItemSeparator} source={require('../../images/steps_separator.webp')}/>
        );
    };

    getPoweredByView = () => {
        return isThirdPartyFlow() ?
            <View style={{flexDirection: 'row'}}>
                <Text style={[TextStyle.text_8_normal]}>Powered by</Text>
                <Image style={styles.poweredbyImageStyle} source={require('../../images/instacashlogo.webp')}/>
            </View> : null;
    };

    renderItem = (data, index) => {
        let view;
        //TODO this is to be done with proper generic handling
        if (buybackManagerObj.isABBFlow() && index === 0) {
            return null;
        }
        if (data.state === StepConstants.STEP_IN_PROGRESS) {
            if (index === 3) {
                let couponCode;
                if (this.props.couponCodeInfo !== null && this.props.couponCodeInfo !== undefined) {
                    couponCode = this.props.couponCodeInfo.couponCode;
                }

                view = <View style={{flex: 1}}>
                    <View style={styles.list}>
                        <Image style={styles.stepsImage} source={data.stepState}/>
                        <View style={styles.itemTextWrapper}>
                            <Text style={TextStyle.text_20_bold}>{data.heading1}</Text>
                            <CouponCodeComponent
                                style={styles.couponCodeComponent}
                                couponCode={couponCode}
                                apply={() => {
                                    let buybackData = buybackManagerObj.getBuybackStatusDataFirstItem();
                                    let eventData = new Object();
                                    eventData['Location'] = BUYBACK_TIMELINE_SCREEN;
                                    eventData['Coupon Code'] = couponCode;
                                    logWebEnageEvent(APPLY_PROMO, eventData);
                                    this.props.applyCoupon(buybackData?.quoteId, couponCode);
                                }
                                }
                                isApplying={this.props.isCouponApplying}
                                isApplied={this.props.isCouponApplied}
                                isError={this.props.isCouponError}
                                errorMessage={this.props.errorMessage}
                            />
                            <Text style={[TextStyle.text_12_normal]}>{data.heading2}</Text>
                        </View>
                    </View>
                    <TouchableOpacity style={[styles.primaryButton, ButtonStyle.BlueButton]} onPress={() => {
                        this.onClick(index);
                    }}>
                        <Text style={[ButtonStyle.primaryButtonText]}>{getSchedulePickupButtonText()}</Text>
                    </TouchableOpacity>
                </View>;
            } else {
                view = <TouchableOpacity style={styles.list} onPress={() => {
                    this.onClick(index);
                }}>
                    <Image style={styles.stepsImage} source={data.stepState}/>
                    <View style={styles.itemTextWrapper}>
                        <Text style={TextStyle.text_14_bold}>{data.heading1}</Text>
                        <Text style={[TextStyle.text_12_normal]}>{data.heading2}</Text>
                    </View>
                    <Text style={[TextStyle.text_14_bold, styles.stepsItemButtonStyle]}>{data.button.text}</Text>
                    <Image style={styles.rightArrowStyle} source={require('../../images/right_arrow.webp')}/>
                </TouchableOpacity>;
            }
        } else if (data.state === StepConstants.STEP_NOT_STARTED) {
            view = <TouchableOpacity style={styles.list} disabled={true} onPress={() => {
                this.onClick(index);
            }}>
                <Image style={styles.stepsImage} source={data.stepState}/>
                <View style={styles.itemTextWrapper}>
                    <Text style={[TextStyle.text_14_bold, {color: colors.greye0}]}>{data.heading1}</Text>
                </View>
            </TouchableOpacity>;
        } else {
            view = <TouchableOpacity style={styles.list} disabled={true} onPress={() => {
                this.onClick(index);
            }}>
                <Image style={styles.stepsImage} source={data.stepState}/>
                <View style={styles.itemTextWrapper}>
                    <Text style={TextStyle.text_12_normal}>{data.heading1}</Text>
                    <Text style={TextStyle.text_14_bold}>{data.heading2}</Text>
                </View>
            </TouchableOpacity>;
        }
        return view;
    };

    render() {
        let deviceName = '';
        let imageName = '';
        if (this.props !== null && this.props !== undefined && this.props.deviceInfo !== null && this.props.deviceInfo !== undefined && this.props.deviceInfo.deviceInfo !== null && this.props.deviceInfo.deviceInfo !== undefined) {
            let deviceInfo = this.props.deviceInfo.deviceInfo;
            if (deviceInfo.deviceName !== null && deviceInfo.deviceName !== undefined) {
                deviceName = deviceInfo.deviceName;
            }
            if (deviceInfo.image !== null && deviceInfo.image !== undefined) {
                imageName = deviceInfo.image;
            }
        }
        if (this.props.isError) {
            if (this.pincodeViewRef !== undefined) {
                this.pincodeViewRef.close();
            }
            let alertMessage = this.props.errorMessage
            let buttonTitle = this.props.errorMessageButtonTitle
            setTimeout(() => this.showErrorDialog(alertMessage, buttonTitle), 100);
        } else if (this.props.pincodeServiceablityData !== null && this.props.pincodeServiceablityData.pincodeAvailable) {
            if (this.pincodeViewRef !== undefined) {
                this.pincodeViewRef.close();
            }
        }
        if (this.props.type === APPLY_COUPON_DATA) {
            price = this.props.price;
        }
        return (
            <SafeAreaView style={styles.SafeArea}>
                <View style={{flex: 1, backgroundColor: colors.white}} ref={ref => (this.parentRef = ref)}>
                    <BlockingLoader
                        visible={(this.props.isLoading || this.state.isLoading)}
                        loadingMessage={'Loading...'}
                    />
                    <DialogView ref={ref => (this.DialogViewRef = ref)}/>
                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#004890', '#0282F0']}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <HeaderBackButton tintColor={colors.white}
                                              onPress={() => {
                                                  this.props.reset();
                                                  this.onBackPress();
                                              }}/>
                            <TouchableOpacity style={{marginTop: 16, flexDirection: 'row', marginRight: 16}}
                                              onPress={() => {
                                                  chatBridgeRef.openChat();
                                              }}>
                                <Image style={styles.chatIconStyle}
                                       source={require('../../images/icon_chat.webp')}></Image>
                                <Text style={[TextStyle.text_14_normal, styles.chatTextStyle]}>Chat</Text>
                            </TouchableOpacity>

                        </View>
                        <View style={styles.deviceInfoContainer}>
                            <View>
                                <Text
                                    style={[TextStyle.text_12_normal, styles.phoneTitleStyle]}>
                                    {deviceName !== '' ? 'For your ' + deviceName + ' , get upto' : null}
                                </Text>
                                <View style={{
                                    flexDirection: 'row',
                                    marginTop: spacing.spacing12,
                                    marginBottom: spacing.spacing24,
                                    alignItems: 'center',
                                }}>
                                    <Text
                                        style={[TextStyle.text_16_bold, styles.priceTitleStyle]}>
                                        {this.props.deviceInfo !== null ? '\u20B9 ' + NumberUtils.getFormattedPrice(price) + '/-' : null}
                                    </Text>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({isShowTooltip: true})
                                        }}
                                        ref={(ref) => {
                                            infoIcon = ref;
                                        }}
                                        style={styles.infoIconContainer}>
                                        <Text style={[TextStyle.text_10_bold, {color: colors.white}]}>i</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <Image style={styles.deviceIconStyle}
                                   source={{uri: imageName}}></Image>
                        </View>
                    </LinearGradient>
                    <Text style={[TextStyle.text_14_normal, styles.stepsTitleStyle]}>Get resale price in 4 simple
                        steps!</Text>
                    <FlatList
                        style={{marginTop: 20}}
                        data={this.props.stepsList ? this.props.stepsList : []}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({item, index}) => this.renderItem(item, index)}
                        keyExtractor={(item, index) => index.toString()}
                    />

                    <RBSheet
                        ref={ref => {
                            this.pincodeViewRef = ref;
                        }}
                        duration={10}
                        height={220}
                        closeOnSwipeDown={false}
                        onClose={() => {
                            // this.props.addAndRemoveCloseChatView(false)
                            this.props.resetPincodePopup();
                        }}>
                        <EnterPincodeLayout
                            pincodeServiceablityData={this.props.pincodeServiceablityData}
                            propsData={{
                                message: 'Pincode',
                                primaryButton: {
                                    text: 'Check serviceability', onClick: () => {
                                    },
                                },
                                checkPincode: (pinCode) => {
                                    pincodeValue = pinCode;
                                    this.checkPincodeServiceablity(pinCode);
                                },
                                cancelable: false,
                                onClose: () => {
                                    if (this.pincodeViewRef !== undefined) {
                                        this.pincodeViewRef.close();
                                    }
                                },
                                showPincodeErrorDialoge: () => {
                                    setTimeout(this.showPinCodeErrorDialoge, 100);
                                },
                            }}
                            error={this.props.isError}
                            errorMessage={this.props.errorMessage}
                            checkIMEIPermission={this.checkIMEIPermission}
                            // closePopup={(isClosePopup) => this.closePopup(isClosePopup)}
                        />
                    </RBSheet>

                    <DialogView ref={ref => (this.DialogViewRef = ref)}/>

                    <View style={styles.poweredbyStyle}>
                        {this.getPoweredByView()}
                    </View>

                    {this.state.isShowTooltip
                        ? <TooltipComponent
                            parentRef={this.parentRef}
                            childRef={infoIcon}
                            data={{
                                'text': 'The final price quote will vary depending on your device condition. The device condition will be decided at the end of diagnosis. ',
                                'image': images.ic_whiteClose,
                            }}
                            onCloseTooptip={() => {
                                this.setState({isShowTooltip: false});
                            }}
                            arrowDirection={ArrowDirection.TOP}
                            style={{backgroundColor: '#393939'}}
                            isVisible={this.state.isShowTooltip}
                        />
                        : null}
                </View>
            </SafeAreaView>
        );
    }

    /**
     * here we get response from retailers api
     * @param arrayOfRetailers
     */
    navigateToRetailersFlow(arrayOfRetailers, error) {
        if (error) {
            _this.DialogViewRef.showDailog({
                title: 'ALERT',
                message: error,
                primaryButton: {
                    text: 'Retry', onClick: () => {
                        decideRetailersFlow(this.navigateToRetailersFlow);
                    },
                },
                secondaryButton: {
                    text: 'Cancel', onClick: () => {
                    },
                },
                cancelable: false,
                onClose: () => {
                },
            });
        } else {
            sendNearbyStoresWebengageEvent(arrayOfRetailers?.length);
            if (arrayOfRetailers?.length > 1) {
                _this.props.navigation.navigate('NearByRetailers', {params: {data: arrayOfRetailers}});
            } else if(arrayOfRetailers?.length === 1){
                _this.props.navigation.navigate('RetailerDetailScreen', {detailData: arrayOfRetailers[0] ?? {}})
            } else{
                _this.props.navigation.navigate('RetailersEmptyScreen', {questionsData: _this.props.stepsList[3].questionData});
            }
        }
    }

    onClick(index) {
        switch (index) {
            case 0:
                currentQuestionType = 'pincode';
                let eventDataPincode = new Object();
                eventDataPincode['Location'] = MOBILE_BUYBACK;
                eventDataPincode[ABB] = buybackManagerObj?.isABBFlow();
                eventDataPincode[SERVED_BY] = getServedByValueFromFirstItem();
                logWebEnageEvent(PINCODE, eventDataPincode);
                this.pincodeViewRef.open();
                break;
            case 1:
                currentQuestionType = 'MHC';
                //send webengage event
                let data = buybackManagerObj.getBuybackStatusDataFirstItem();
                _quoteId = data?.quoteId;
                buybackStatus = data?.buyBackStatus;
                let eventData = new Object();
                eventData['Location'] = MOBILE_BUYBACK;
                eventData['QuoteID'] = _quoteId;
                eventData['Stage'] = buybackStatus;
                eventData[ABB] = buybackManagerObj?.isABBFlow();
                eventData[SERVED_BY] = getServedByValueFromFirstItem();
                logWebEnageEvent(MHC_CHECK_NOW, eventData);
                this.openMhc(currentQuestionType, this.props.stepsList[index].questionData);
                break;
            case 2:
                let eventDataQuestion = new Object();
                eventDataQuestion['Location'] = MOBILE_BUYBACK;
                eventDataQuestion[ABB] = buybackManagerObj?.isABBFlow();
                eventDataQuestion[SERVED_BY] = getServedByValueFromFirstItem();
                logWebEnageEvent(ADDITIONAL_DETAILS, eventDataQuestion);
                currentQuestionType = 'QUOTE_QUESTION';
                this.props.navigation.navigate('BuybackQuestions', {
                    params: {
                        questionData: this.props.stepsList[index].questionData,
                        currentQuestionType: currentQuestionType,
                    },
                });
                break;
            case 3:
                currentQuestionType = 'ORDER_QUESTION';
                let buybackData = buybackManagerObj.getBuybackStatusDataFirstItem();
                let subType = buybackData.subType;
                if (subType === buybackFlowType.RETAILER_PICKUP) {
                    decideRetailersFlow(this.navigateToRetailersFlow);
                    return;
                } else if (subType === buybackFlowType.BOTH) {
                    this.props.navigation.navigate('BuySellSelection', {
                        questionData: this.props.stepsList[index].questionData,
                        currentQuestionType: currentQuestionType,
                    });
                    return;
                }
                let eventDataSchedulePickup = new Object();
                eventDataSchedulePickup['Location'] = MOBILE_BUYBACK;
                eventDataSchedulePickup['QuoteID'] = _quoteId;
                eventDataSchedulePickup['Stage'] = buybackStatus;
                eventDataSchedulePickup[ABB] = buybackManagerObj?.isABBFlow();
                eventDataSchedulePickup[SERVED_BY] = getServedByValueFromFirstItem();
                logWebEnageEvent(SCHEDULE_PICKUP, eventDataSchedulePickup);
                this.props.navigation.navigate('BuybackQuestions', {
                    params: {
                        questionData: this.props.stepsList[index].questionData,
                        currentQuestionType: currentQuestionType,
                    },
                });
                break;
        }
    }

    getStepsImage = (type, state) => {
        if (state === StepConstants.STEP_COMPLETED) {
            return require('../../images/green_right.webp');
        }
        switch (type) {
            case StepConstants.STEP_LOCATION:
                return require('../../images/step_location_enable.webp');
            case StepConstants.STEP_MHC:
                if (state === StepConstants.STEP_NOT_STARTED) {
                    return require('../../images/step_setting_disable.webp');
                }
                return require('../../images/step_setting_enable.webp');
            case StepConstants.STEP_QUESTIONS:
                if (state === StepConstants.STEP_NOT_STARTED) {
                    return require('../../images/step_question_disable.webp');
                }
                return require('../../images/step_question_enable.webp');
            case StepConstants.STEP_PICKUP:
                if (state === StepConstants.STEP_NOT_STARTED) {
                    return require('../../images/step_ruppee_disable.webp');
                }
                return require('../../images/step_ruppee_enable.webp');
            default:
                return require('../../images/step_location_enable.webp');
        }
    };

    async checkIMEIPermission(onPermissionGrant) {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                onPermissionGrant();

            } else if (granted == PermissionsAndroid.RESULTS.DENIED) {
                _this.PermisionError(() => {
                    _this.checkIMEIPermission(onPermissionGrant);
                });
            } else {
                _this.showrationalPermissionDailog(IMEI_PERMISSION_RATIONAL);
            }
        } catch (err) {
            console.warn(err);
        }
    }

    showrationalPermissionDailog(alertMessage) {
        _this.DialogViewRef.showDailog({
            title: 'We Request Again',
            message: alertMessage,
            primaryButton: {
                text: 'Go to settings', onClick: () => {
                    chatBridgeRef.openAndroidAppSettings();
                },
            },
            secondaryButton: {
                text: 'Later', onClick: () => {
                },
            },
            cancelable: false,
            onClose: () => {
            },
        });
    }

    PermisionError(checkPermissionAgain) {
        _this.DialogViewRef.showDailog({
            message: IMEI_PERMISSION,
            primaryButton: {
                text: 'Allow', onClick: () => {
                    checkPermissionAgain();
                },
            },
            secondaryButton: {
                text: 'Later', onClick: () => {
                },
            },
            cancelable: false,
            onClose: () => {
            },
        });
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        this.willFocus.remove();
        AppState.removeEventListener('change', this._handleAppStateChange);
        eventEmitter.removeListener('willReceiveLocation', () => {
        });
    }

    async openMhc(currentQuestionType, mhcList) {
        try {
            let mhcResult = await chatBridgeRef.openMHC(JSON.stringify({
                questionType: currentQuestionType,
                data: mhcList,
            }));
            let mhcAnswersData = parseJSON(mhcResult);
            let mhcAnswers = {
                questionType: mhcAnswersData.questionType,
                data: mhcAnswersData.data,
                mhcScore: mhcAnswersData.mhcScore,
            };
            let data = buybackManagerObj.getBuybackStatusDataFirstItem();
            this.props.saveQuestions(mhcAnswers, data?.quoteId, MHC_QUESTIONS_SAVE, pincodeValue, price, data?.cityName, mhcAnswersData.mhcScore);
            buybackManagerObj.setBuybackStatusDataFirstItem(data);
        } catch (e) {
            // if reject the
        }
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    list: {
        paddingVertical: spacing.spacing4,
        margin: spacing.spacing4,
        backgroundColor: '#fff',
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    deviceInfoContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: spacing.spacing20,
        marginLeft: spacing.spacing16,
    },
    phoneTitleStyle: {
        color: colors.white,
    },
    priceTitleStyle: {
        color: colors.white,
    },
    stepsTitleStyle: {
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing16,
        color: colors.charcoalGrey,
    },
    stepsImage: {
        width: spacing.spacing32,
        height: spacing.spacing32,
        alignItems: 'center',
        alignSelf: 'flex-start',
        marginLeft: spacing.spacing16,
    },
    stepsItemButtonStyle: {
        alignItems: 'center',
        color: colors.blue,
    },
    rightArrowStyle: {
        width: spacing.spacing8,
        height: spacing.spacing12,
        marginLeft: spacing.spacing4,
        marginRight: spacing.spacing16,
    },
    itemTextWrapper: {
        flex: 1,
        marginLeft: spacing.spacing16,
    },
    stepsItemSeparator: {
        width: 4,
        height: 16,
        marginLeft: spacing.spacing34,
    },
    spinnerTextStyle: {
        color: '#FFF',
    },
    chatIconStyle: {
        width: 16,
        height: 16,
        marginTop: 4,
        resizeMode: 'contain',
    },
    deviceIconStyle: {
        width: spacing.spacing80,
        height: spacing.spacing80,
        marginRight: spacing.spacing16,
        resizeMode: 'contain',
    },
    chatTextStyle: {
        marginLeft: spacing.spacing6,
        textAlign: 'center',
        color: colors.white,
    },
    primaryButton: {
        marginTop: spacing.spacing32,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
    },
    SafeArea: {
        flex: 1,
        backgroundColor: colors.white,
    },
    poweredbyStyle: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
        marginTop: spacing.spacing8,
        marginBottom: spacing.spacing8,
        marginRight: spacing.spacing20,
    },
    poweredbyImageStyle: {
        width: spacing.spacing56,
        height: spacing.spacing14,
        marginLeft: spacing.spacing4,
        resizeMode: 'contain',
    },
    couponCodeComponent: {
        marginBottom: spacing.spacing8,
        marginRight: spacing.spacing12,
        marginTop: spacing.spacing12,
    },
    infoIconContainer: {
        width: 18,
        height: 18,
        borderRadius: 9,
        borderWidth: 2,
        borderColor: colors.white,
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 4,
        paddingLeft: 1,
        marginLeft: spacing.spacing12,
    },
});
