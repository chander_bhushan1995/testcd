import React, {Component} from 'react';
import {
    Text,
    View,
    StyleSheet,
    Platform,
    NativeModules, SafeAreaView, ScrollView, FlatList,
} from 'react-native';
import {CommonStyle, TextStyle} from '../../Constants/CommonStyle';
import spacing from '../../Constants/Spacing';
import colors from '../../Constants/colors';

import {HeaderBackButton} from 'react-navigation';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import {APIData} from '../../../index';
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

const nativeBridgeRef = NativeModules.ChatBridge;
export default class BuybackDeclarationComponent extends Component {

    static navigationOptions = ({navigation}) => {

        return {
            headerTitle: <View style={Platform.OS === 'ios' ? {alignItems: 'center'} : {alignItems: 'flex-start'}}>
                <Text style={TextStyle.text_16_normal}>Declaration</Text>
            </View>,
            headerLeft: <HeaderBackButton tintColor={colors.black}
                                          onPress={() => navigation.pop()}/>,
            headerTitleStyle: {color: colors.white},
            headerTintColor: colors.white,
        };
    };

    constructor(props) {
        super(props);
        this.state = {topText: '', bottomText: []};
    }

    onBackPress = () => {
        this.props.navigation.pop();
    };

    componentWillUnmount() {
    }

    componentDidMount() {
        nativeBridgeRef.getBuybackDeclarationData(data => {
            let declarationDataJson = parseJSON(data);
            let declarationData = declarationDataJson.declaration_data;
            this.setState({
                topText: declarationData?.top_des,
                bottomText: declarationData?.bottom_des,
            });
        });
    }

    renderItem = (data, index) => {
        return (
            <Text
                style={[TextStyle.text_12_normal, {
                    marginTop: spacing.spacing8,
                    color: colors.color_212121,
                    marginBottom: spacing.spacing8,
                }]}>{data?.text}</Text>
        );

    };

    render() {
        let props = APIData;
        let userName = props.user_Name
        if (!props.user_Name){
            userName = buybackManagerObj.getUserName()
        }
        return (
            <SafeAreaView style={styles.SafeArea}>
                <ScrollView style={styles.scrollViewStyle}>
                    <View style={{paddingBottom: spacing.spacing32}}>
                        <Text
                            style={[TextStyle.text_12_normal]}>{this.state.topText}</Text>
                        <Text
                            style={[TextStyle.text_10_regular, {marginTop: spacing.spacing16}]}>NAME</Text>
                        <Text
                            style={TextStyle.text_12_medium}>{userName}</Text>
                        <Text
                            style={[TextStyle.text_10_regular, {marginTop: spacing.spacing16}]}>ADDRESS</Text>
                        <Text
                            style={TextStyle.text_12_medium}>{buybackManagerObj.getAddress()}</Text>
                        <Text
                            style={[TextStyle.text_12_regular, {
                                marginTop: spacing.spacing16,
                                color: colors.color_888F97,
                            }]}>I
                            am selling an old product
                            with the below details</Text>
                        <View style={{flexDirection: 'row', marginTop: spacing.spacing16}}>
                            <Text
                                style={[TextStyle.text_12_bold, {width: '63%'}]}>IMEI</Text>
                            <Text
                                style={[TextStyle.text_12_bold, {color: colors.color_212121}]}>{buybackManagerObj.getImei()}</Text>
                        </View>
                        <View style={{flexDirection: 'row', marginTop: spacing.spacing4}}>
                            <Text
                                style={[TextStyle.text_12_bold, {
                                    width: '63%',
                                    color: colors.color_888F97,
                                }]}>Brand</Text>
                            <Text
                                style={[TextStyle.text_12_bold, {color: colors.color_212121}]}>{props?.brandName}</Text>
                        </View>
                        <View style={{flexDirection: 'row', marginTop: spacing.spacing4}}>
                            <Text
                                style={[TextStyle.text_12_bold, {
                                    width: '63%',
                                    color: colors.color_888F97,
                                }]}>Model</Text>
                            <Text
                                style={[TextStyle.text_12_bold, {color: colors.color_212121}]}>{props?.modelName}</Text>
                        </View>
                        <View style={{
                            flexDirection: 'row',
                            marginTop: spacing.spacing4,
                            marginBottom: spacing.spacing24,
                        }}>
                            <Text
                                style={[TextStyle.text_12_bold, {width: '63%', color: colors.color_888F97}]}>RAM/Internal
                                Storage</Text>
                            <Text
                                style={[TextStyle.text_12_bold, {color: colors.color_212121}]}>{props?.RAM + '/' + props?.ROM}</Text>
                        </View>
                        <View style={CommonStyle.separator}/>
                        <Text
                            style={[TextStyle.text_12_bold, {
                                color: colors.color_212121,
                                marginTop: spacing.spacing16,
                            }]}>I
                            confirm to all the below conditions</Text>
                        <FlatList
                            extraData={this.state.bottomText}
                            style={styles.listStyle}
                            data={this.state.bottomText}
                            renderItem={({item, index}) => this.renderItem(item, index)}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    SafeArea: {
        flex: 1,
        backgroundColor: colors.white,
    },
    scrollViewStyle: {
        paddingLeft: spacing.spacing16,
        paddingRight: spacing.spacing24,
        paddingTop: spacing.spacing24,
        paddingBottom: spacing.spacing48,
    },
    listStyle: {
        marginTop: spacing.spacing12,
        marginBottom: spacing.spacing16,
    },
});
