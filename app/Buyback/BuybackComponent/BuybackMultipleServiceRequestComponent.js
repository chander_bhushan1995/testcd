import React, {Component} from 'react';
import {
    FlatList,
    Image,
    NativeModules,
    Platform,
    StyleSheet,
    Text,
    SafeAreaView,
    BackHandler,
    TouchableOpacity,
    View,
} from 'react-native';
import {CommonStyle, TextStyle} from '../../Constants/CommonStyle';
import spacing from '../../Constants/Spacing';
import colors from '../../Constants/colors';

import {HeaderBackButton} from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import NumberUtils from '../../commonUtil/NumberUtils';
import {BUYBACK_REQUEST_LISTING} from '../../Constants/WebengageAttributes';
import {MOBILE_BUYBACK_INTENT} from '../../Constants/WebengageEvents';
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import {APIData} from '../../../index';
import {buybackStatusConstants, DL_BUYBACK_SCHEDULE_PICKUP} from '../../Constants/BuybackConstants';
import {ABB, SERVED_BY} from '../../Constants/WebengageAttrKeys';
import {getServedByValue} from '../BuyBackUtils/BuybackUtils';
import SchedulePickupBottomSheet from './SchedulePickupBottomSheet';
import RBSheet from '../../Components/RBSheet';

const chatBridgeRef = NativeModules.ChatBridge;
let BuybackNotStartedClickedIndex = -1;
let clickedIndex;
let noOfRequests;
export default class BuybackMultipleServiceRequestComponent extends Component {

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: <View style={Platform.OS === 'ios' ? {alignItems: 'center'} : {alignItems: 'flex-start'}}>
                <Text style={TextStyle.text_16_normal}>View status</Text>
            </View>,
            headerLeft: <HeaderBackButton tintColor={colors.black}
                                          onPress={() =>
                                              chatBridgeRef.getUserInfo(userInfo => {
                                                  chatBridgeRef.refreshBuybackStatus();
                                                  chatBridgeRef.updateProfile();
                                                  chatBridgeRef.goBack();
                                              })
                                          }/>,
            headerTitleStyle: {color: colors.white},
            headerTintColor: colors.white,
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            BuybackNotStartedDataList: [],
            BuybackDataList: [],
            buybackSchedulePickupData: {},
        };
    }

    componentDidMount() {
        this.willFocus = this.props.navigation.addListener(
            'willFocus',
            () => {
                if (clickedIndex === undefined) {
                    let BuybackNotStartedDataList = this.props.navigation.getParam('params').buybackNotStartedData;
                    let BuybackDataList = this.props.navigation.getParam('params').buybackdata;
                    noOfRequests = BuybackDataList.length;
                    this.setState({
                        BuybackNotStartedDataList: BuybackNotStartedDataList,
                        BuybackDataList: BuybackDataList,
                    });
                } else {
                    let buyBackDatum = buybackManagerObj.getBuybackStatusData();
                    if (clickedIndex === BuybackNotStartedClickedIndex) {
                        if (buyBackDatum[0]?.buyBackStatus !== buybackStatusConstants.BUYBACK_NOT_STARTED) {
                            this.state.BuybackDataList.push(buyBackDatum[0]);
                            this.setState({BuybackDataList: this.state.BuybackDataList, BuybackNotStartedDataList: []});
                        }
                    } else if (clickedIndex >= 0) {
                        let data = this.state.BuybackDataList[clickedIndex] = buyBackDatum[0];
                        this.setState({BuybackDataList: this.state.BuybackDataList});
                    }
                }
            });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            chatBridgeRef.getUserInfo(userInfo => {
                chatBridgeRef.refreshBuybackStatus();
                chatBridgeRef.updateProfile();
                chatBridgeRef.goBack();
            });
        });
    }

    componentWillUnmount() {
        this.state = {
            BuybackNotStartedDataList: [],
            BuybackDataList: [],
        };
        BackHandler.removeEventListener('hardwareBackPress', this.backHandler);
    }

    onBackPress = () => {
        this.props.navigation.pop();
    };

    render() {
        let view = null;

        if (this.state.BuybackNotStartedDataList.length > 0) {
            let BuybackNotStartedData = this.state.BuybackNotStartedDataList[0];

            view = <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#004890', '#0282F0']}
                                   style={styles.linearGradient}>
                <TouchableOpacity onPress={() => {
                    clickedIndex = BuybackNotStartedClickedIndex;
                    let data1 = [];
                    data1.push(BuybackNotStartedData);

                    let eventData = new Object();
                    eventData['Location'] = BUYBACK_REQUEST_LISTING;
                    eventData['Supported'] = 'No';
                    eventData['Brand and Model'] = BuybackNotStartedData.deviceInfo.deviceName;
                    eventData['Stage'] = BuybackNotStartedData.buyBackStatus;
                    eventData[ABB] = buybackManagerObj?.isABBFlow();
                    eventData[SERVED_BY] = getServedByValue(BuybackNotStartedData?.subType);
                    logWebEnageEvent(MOBILE_BUYBACK_INTENT, eventData);
                    buybackManagerObj.setBuybackStatusData(data1);
                    this.props.navigation.navigate('BuybackComponent');
                }
                }>
                    <View style={styles.deviceInfoContainer}>
                        <View>
                            <Text
                                style={[TextStyle.text_16_bold, styles.phoneTitleStyle]}>
                                {BuybackNotStartedData.deviceInfo !== undefined ? 'Sell your ' + BuybackNotStartedData?.deviceInfo?.brand + ' ' + BuybackNotStartedData?.deviceInfo?.modelName + ', for upto ' + '\u20B9 ' + NumberUtils.getFormattedPrice(BuybackNotStartedData?.price) + ' and get instant cash.' : null}
                            </Text>
                            <View style={{flexDirection: 'row'}}>
                                <View style={{width: '75%', flexDirection: 'row', marginTop: 12}}>
                                    <Text
                                        style={[TextStyle.text_16_bold, styles.priceTitleStyle]}>
                                        Let’s begin
                                    </Text>
                                    <Image style={[styles.arrowStyle, {marginTop: 22, marginLeft: 4}]}
                                           source={require("../../images/white_arrow.webp")}/>
                                </View>
                                <Image style={styles.deviceIconStyle}
                                       source={{uri: BuybackNotStartedData.deviceInfo ? BuybackNotStartedData?.deviceInfo?.image : ''}}></Image>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </LinearGradient>;
        }
        return (
            <SafeAreaView style={styles.SafeArea}>
                <View style={styles.container}>
                    {view}
                    <Text
                        style={[TextStyle.text_10_bold, styles.requestTextStyle]}>
                        {this.state.BuybackDataList.length} ACTIVE REQUESTS
                    </Text>
                    <FlatList
                        extraData={this.state.BuybackDataList}
                        style={styles.listStyle}
                        data={this.state.BuybackDataList}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({item, index}) => this.renderItem(item, index)}
                        keyExtractor={(item, index) => index.toString()}
                    />
                    <RBSheet
                        ref={ref => (this.dialogViewRef = ref)}
                        duration={10}
                        height={330}
                        closeOnSwipeDown={true}
                        closeOnPressMask={true}
                    >
                        <View style={{flex: 1}}>
                            <SchedulePickupBottomSheet
                                isLoading={false}
                                buybackData={this.state?.buybackSchedulePickupData ?? {}}
                                customerId={APIData.customer_id}
                                onSchedulePickup={() => {
                                    this.dialogViewRef.close()
                                    let data1 = [];
                                    data1.push(this.state?.buybackSchedulePickupData);
                                    buybackManagerObj.setBuybackStatusData(data1);
                                    this.props.navigation.navigate('BuybackComponent', {params: {isDeeplink: true, deeplink: DL_BUYBACK_SCHEDULE_PICKUP}});
                                }}
                                onCancelSchedulePickup={() => {
                                    this.dialogViewRef.close()
                                }}
                            />
                        </View>
                    </RBSheet>
                </View>
            </SafeAreaView>
        );
    }

    FlatListItemSeparator = () => {
        return (
            <View style={CommonStyle.separator}/>
        );
    };

    renderItem = (data, index) => {
        return (
            <TouchableOpacity style={styles.itemStyle} onPress={() => {
                let eventData = new Object();
                eventData['Location'] = BUYBACK_REQUEST_LISTING;
                eventData['Supported'] = 'Yes';
                eventData['Brand and Model'] = data?.deviceInfo?.deviceName;
                eventData['Stage'] = data?.buyBackStatus;
                eventData[ABB] = buybackManagerObj?.isABBFlow();
                eventData[SERVED_BY] = getServedByValue(data?.subType);
                logWebEnageEvent(MOBILE_BUYBACK_INTENT, eventData);

                clickedIndex = index;
                if (data.buyBackStatus === buybackStatusConstants.ORDER_PLACED || data.buyBackStatus === buybackStatusConstants.VOUCHER_GENERATED) {
                    let data1 = [];
                    data1.push(data);
                    buybackManagerObj.setBuybackStatusData(data1);
                    this.props.navigation.navigate('BuybackRequestSuccessTimeline', {params: {quoteId: data.quoteId}});
                } else if(data.buyBackStatus == buybackStatusConstants.QUOTE_GENERATED && data?.recentExchangeCancelled){
                    this.setState({
                        buybackSchedulePickupData: data
                    });
                    this.dialogViewRef.open();
                }else {
                    let data1 = [];
                    data1.push(data);
                    buybackManagerObj.setBuybackStatusData(data1);
                    this.props.navigation.navigate('BuybackComponent');
                }
            }
            }>
                <Image style={styles.deviceImageStyle}
                       source={{uri: data.deviceInfo.image ? data.deviceInfo.image : ''}}/>
                <View style={styles.textWrapperStyle}>
                    <Text
                        style={[TextStyle.text_16_bold]}>
                        {data.deviceInfo.deviceName}
                    </Text>
                    <Text
                        style={[TextStyle.text_12_normal, styles.deviceSpecsStyle]}>
                        {'(' + data.deviceInfo.primaryMemory + 'GB RAM, ' + data.deviceInfo.secondaryMemory + 'GB Storage)'}
                    </Text>
                </View>
                <Image style={styles.arrowStyle} source={require("../../images/right_arrow.webp")}/>
            </TouchableOpacity>
        );

    };
}

const
    styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: colors.greyf2,
        },
        requestTextStyle: {
            marginTop: spacing.spacing28,
            marginLeft: spacing.spacing12,
        },
        listStyle: {
            marginTop: spacing.spacing12,
        },
        itemStyle: {
            flex: 1,
            flexDirection: 'row',
            padding: spacing.spacing20,
            backgroundColor: colors.white,
        },
        itemStyle1: {
            flex: 1,
            flexDirection: 'row',
            padding: spacing.spacing20,
        },
        deviceImageStyle: {
            width: spacing.spacing56,
            height: spacing.spacing56,
            resizeMode: 'contain',
        },
        textWrapperStyle: {
            width: '75%',
            marginLeft: spacing.spacing16,
        },
        deviceSpecsStyle: {
            color: colors.charcoalGrey,
        },
        arrowStyle: {
            width: spacing.spacing14,
            height: spacing.spacing14,
            resizeMode: 'contain',
        },
        SafeArea: {
            flex: 1,
            backgroundColor: colors.white,
        },
        deviceInfoContainer: {
            marginTop: spacing.spacing16,
            marginLeft: spacing.spacing12,
        },
        phoneTitleStyle: {
            color: colors.white,
            marginRight: spacing.spacing12,
        },
        priceTitleStyle: {
            marginTop: spacing.spacing16,
            marginBottom: spacing.spacing24,
            color: colors.white,
        },
        deviceIconStyle: {
            width: spacing.spacing80,
            height: spacing.spacing80,
            marginRight: spacing.spacing16,
            resizeMode: 'contain',
        },
        linearGradient:
            {
                marginTop: spacing.spacing20,
                marginRight: spacing.spacing12,
                marginLeft: spacing.spacing12,
                marginBottom: spacing.spacing28,
                borderRadius: spacing.spacing4,
                overflow: 'hidden',
            },
    });
