import React, {Component} from "react";
import {
    Text,
    View,
    TouchableOpacity,
    ActivityIndicator,
    FlatList,
    StyleSheet,
    Image,
    Platform,
    NativeModules
} from "react-native";
import {TextStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";

import {HeaderBackButton, NavigationActions} from "react-navigation";

const chatBrigeRef = NativeModules.ChatBridge;
var _this;
export default class BuybackQuestionsComponent extends Component {

    constructor(props) {
        super(props);
        _this=this;
    }

    componentDidMount() {

    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    contentContainerStyle={{ flexGrow: 1}}
                    extraData={this.props}
                    style={styles.list}
                    ref={(ref) => { this.flatListRef = ref; }}
                    getItemLayout={this.getItemLayout}
                    data={this.props.data}
                    renderItem={({item, index}) => this.renderItem(item, index)}
                    keyExtractor={(item, index) => item.title}
                />
            </View>
        )
    }
    getItemLayout = (data, index) => (
        { length: 200, offset: 200 * index, index }
    )
    getAnswerItem = (data, index, item, itemIndex) => {
       return <TouchableOpacity style={this.getAnswerBackgroundStyle(data, item)} onPress={() => {
            let dataSource = this.props.data;
            if(data.isSingleChoice){
                for(let indexItem = 0; indexItem < dataSource[index].data.length; indexItem++){
                    dataSource[index].data[indexItem].isSelected = false;
                }
            }
            dataSource[index].data[itemIndex].isSelected = true;
            this.props.onClick(dataSource[index],index, this.props.index);
           this.scrollToIndex();
            // this.setState({dataSource: dataSource});
            if (index == (dataSource.length-1)) this.props.navigation.navigate("SchedulePickup", {});
        }}>
            <Image style={styles.answerItemImageStyle} source={item.image}/>
            <Text
                style={[TextStyle.text_14_normal, this.getAnswerTitleStyle(data, item)]}
                numberOfLines={1}>{item.answer}</Text>
            <Text style={[TextStyle.text_12_normal, this.getAnswerTitleStyle(data, item)]}
                  numberOfLines={1}>{item.description}</Text>
        </TouchableOpacity>;
    }

    getAnswerBackgroundStyle = (data, item) => {
        return [styles.answerItemStyle, item.isSelected ? {borderColor: colors.blue}:{borderColor: colors.greye0},
            item.isSelected && data.isAnswerTypeText ? {backgroundColor: colors.blue}:{backgroundColor: colors.white}]
    }

    getAnswerTitleStyle = (data, item) => {
        if(!item.isSelected){
            return {color: colors.charcoalGrey};
        }else{
            if(data.isAnswerTypeText){
                return {color: colors.white};
            }
            return {color: colors.blue};
        }
    }
    scrollToIndex = () => {
        this.flatListRef.scrollToIndex({animated: true, index: 4});
    }
    renderItem = (data, index) => {
        let answerView = [];
        let totalItems = data.data.length;
        for (let sectionIndex = 0; sectionIndex < totalItems / 2; sectionIndex++) {
            let firstItemIndex = 2 * sectionIndex;
            let item = data.data[firstItemIndex];
            let view1 = this.getAnswerItem(data, index, item, firstItemIndex);

            let view2 = null;
            if ((2 * sectionIndex + 1) < totalItems) {
                let secondItemIndex = 2 * sectionIndex + 1;
                let anotherItem = data.data[secondItemIndex];
                view2 = this.getAnswerItem(data, index, anotherItem, secondItemIndex);
            }
            answerView.push(
                <View style={{flexDirection: 'row'}}>
                    {view1}
                    {view2}
                </View>)
        }
        return <View>
            <View style={{flexDirection: 'row'}}>
                <Text style={[TextStyle.text_14_bold, styles.questionNumberStyle]}>{index + 1}</Text>
                <Text style={[TextStyle.text_16_bold, styles.questionTextStyle]}>{data.title}</Text>
            </View>
            <View style={{marginLeft: spacing.spacing24}}>
            {answerView}
            </View>
        </View>
    }
}

const
    styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: "#fff"
        },
        headingStyle: {
            marginTop: spacing.spacing24,
            marginLeft: spacing.spacing16,
            color: colors.charcoalGrey,
        },
        list: {
            marginLeft: spacing.spacing16,
            marginRight: spacing.spacing20,
            marginTop: spacing.spacing48,
        },
        questionNumberStyle: {
            width: spacing.spacing24,
            height: spacing.spacing24,
            borderRadius: 24 / 2,
            backgroundColor: colors.blue,
            color: colors.white,
            paddingTop:2,
            textAlign: 'center',
        },
        questionTextStyle: {
            flex:1,
            marginLeft: spacing.spacing16,
            marginBottom: spacing.spacing24,
        },
        answerItemStyle: {
            padding: spacing.spacing16,
            borderRadius: spacing.spacing4,
            borderWidth: 1,
            alignItems: "center",
            justifyContent: 'center',
            marginLeft: spacing.spacing16,
            marginBottom: spacing.spacing20,
            width: spacing.spacing150,
        },
        answerItemImageStyle: {
            width: spacing.spacing20,
            height: spacing.spacing32,
            marginBottom: spacing.spacing12,
            resizeMode: "contain"
        },
        headerSubtitle: {
            fontSize: spacing.spacing12,
            fontFamily: "Lato",
        }

    });
