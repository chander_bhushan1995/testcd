import React, {Component} from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
    Image,
    ScrollView,
    BackHandler,
    NativeModules,
    SafeAreaView, Modal, Platform, Linking, Clipboard, NativeEventEmitter, AppState
} from 'react-native';
import {CommonStyle, TextStyle} from '../../Constants/CommonStyle';
import spacing from '../../Constants/Spacing';
import colors from '../../Constants/colors';
import {HeaderBackButton} from 'react-navigation';
import BlockingLoader from '../../CommonComponents/BlockingLoader';
import DialogView from '../../Components/DialogView';
import LinearGradient from 'react-native-linear-gradient';
import NumberUtils from '../../commonUtil/NumberUtils';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import {APIData} from '../../../index';
import images from '../../images/index.image';
import {
    BuybackEventLocation,
    buybackFlowStrings,
    buybackOrderStatusDescription,
    buybackOrderStatusMessages,
    buybackStatusConstants,
    buybackVoucherStatus,
} from '../../Constants/BuybackConstants';
import {
    BUYBACK_CANCEL_CONFIRM,
    CANCEL_REQUEST, LOCATION_OFF, ON_DEVICE_LOCATION,
    PLATFORM_OS,
    SCHEDULE_PICKUP, TURN_ON_LOCATION,
} from '../../Constants/AppConstants';
import CommonBuybackAlertUI from './CommonBuybackAlertUI';
import {cancelRequest, getBuybackStatusData} from '../buybackActions/BuybackApiHelper';
import {REASON_CODE_CWOS, WorkFlowStage} from '../buybackActions/Constants';
import {checkLocationPermission} from '../../commonUtil/LocationPermissionUtils';
import {
    getResalePriceText,
    getServedByValueFromFirstItem,
    getStatusDescription,
    getStatusIcon,
    getStatusMessage, getVoucherDescription, getVoucherTitle,
    isExchangeFlow,
    isThirdPartyFlow, sendNearbyStoresWebengageEvent,
} from '../BuyBackUtils/BuybackUtils';
import {daysDifference, getCurretDateMilliseconds} from '../../commonUtil/DateUtils';
import * as EventAttrs from '../../Constants/WebengageAttrKeys';
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';
import * as EventNames from '../../Constants/WebengageEvents';
import TooltipComponent, {ArrowDirection} from '../../AppForAll/HomeScreen/Components/TooltipComponent';
import RightArrowButton from '../../CommonComponents/RightArrowButton';
import {decideRetailersFlow} from '../BuyBackUtils/RetailersNavigationDecider';
import Toast from "react-native-easy-toast";
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

let couponCodeFromFirebase;
const nativeBridgeRef = NativeModules.ChatBridge;
const eventEmitter = new NativeEventEmitter(nativeBridgeRef);
let _this;
let infoIcon = null;

export default class BuybackRequestSuccessTimelineComponent extends Component {

    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            isModalVisible: false,
            isLoading: false,
            isShowTooltip: false,
            appState: AppState.currentState,
        };
        this.gpsAction = this.gpsAction.bind(this);
        this.callAction = this.callAction.bind(this);
        this.openNearbyStoresScreen = this.openNearbyStoresScreen.bind(this);
        this.navigateToRetailersFlow = this.navigateToRetailersFlow.bind(this);
    }

    onBackPress = () => {
        if (this.props.routeList !== undefined && this.props.routeList.length === 3 && this.props.routeList[1].routeName === 'BuybackMultipleServiceRequest') {
            this.props.navigation.pop();
            return true;
        } else {
            nativeBridgeRef.goBack();
        }
    };

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        AppState.removeEventListener('change', this._handleAppStateChange);
        eventEmitter.removeListener('willReceiveLocation', () => {
        });
    }

    getValidString(stringValue) {
        if (stringValue && stringValue?.length > 0) {
            return stringValue + ', ';
        }
        return '';
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        AppState.addEventListener('change', this._handleAppStateChange);
        //below listener is specific to iOS to get lat long
        eventEmitter.addListener('willReceiveLocation', (params) => {
            this.setState({ isLoading: false })
            let location = { lat: params.latitude, lng: params.longitude, pincode: params.pincode };
            buybackManagerObj.setLocation(location);
        });

        // request user location when user lands on screen
        if (!buybackManagerObj.getLocation()) {
            //For location permission to get lat and lng value
            this.getDeviceLocation((location) => {
                this.setState({isLoading: false})
                buybackManagerObj.setLocation(location);
            });
        }

        if (this.props.navigation.getParam('params') === null || this.props.navigation.getParam('params') === undefined) {
            let data = buybackManagerObj.getBuybackStatusDataFirstItem();
            this.props.getOrderStatus(data?.quoteId);
        } else {
            this.props.getOrderStatus(this.props.navigation.getParam('params').quoteId);
        }
        nativeBridgeRef.getBuybackCouponCode(apiProperty => {
            let apiPropert = parseJSON(apiProperty);
            couponCodeFromFirebase = apiPropert.buyback_coupon_code;
        });
    }

    _handleAppStateChange(nextAppState) {
        _this.setState({appState: nextAppState});

        if (nextAppState === 'active' && !buybackManagerObj.getLocation() && Platform.OS === 'ios') {
            //For location permission to get lat and lng value
            _this.getDeviceLocation((location) => { });
        }
    }

    getPoweredByView = () => {
        return isThirdPartyFlow() ?
            <View style={{flexDirection: 'row'}}>
                <Text style={[TextStyle.text_8_normal]}>Powered by</Text>
                <Image style={styles.poweredbyImageStyle} source={require('../../images/instacashlogo.webp')}/>
            </View> : null;
    };

    getCouponCodeView() {
        if (couponCodeFromFirebase !== undefined && couponCodeFromFirebase.length > 0 && !buybackManagerObj.isABBFlow() && !isExchangeFlow()) {
            return <View style={styles.rewardsViewWrapperStyle}>
                <Image source={images.rewards_icon} style={styles.rewardsIconStyle}/>
                <Text style={[TextStyle.text_14_bold, styles.couponTextStyle]}>Exclusive discount on protection plans
                    for your new mobile!</Text>
                <View style={styles.couponCodeWrapperStyle}>
                    <Text style={[TextStyle.text_16_bold, styles.couponCodeStyle]}>Your
                        code: {couponCodeFromFirebase}</Text>
                    <Text style={[TextStyle.text_12_normal, styles.saveUptoTextStyle]}>Use this code to save money on
                        protection plans.</Text>
                </View>
            </View>;
        }
        return null;
    }

    cancelBuybackRequest() {
        this.setState({isModalVisible: true});
        let webEngageAttr = new Object();
        webEngageAttr[EventAttrs.LOCATION] = BuybackEventLocation.BUYBACK;
        webEngageAttr[EventAttrs.ABB] = buybackManagerObj?.isABBFlow();
        webEngageAttr[EventAttrs.SERVED_BY] = getServedByValueFromFirstItem();
        webEngageAttr[EventAttrs.TAG] = BuybackEventLocation.EXCHANGE_REQUEST;
        logWebEnageEvent(EventNames.CANCEL_REQUEST, webEngageAttr);
    }

    getHeaderContent() {
        let orderStatus = this.props.orderStatus;
        if (!isExchangeFlow()) {
            return <View style={styles.topBlueViewStyle}><View style={{flex: 0.44}}>
                <Text style={[TextStyle.text_12_normal, styles.phoneTitleStyle]}>Mobile</Text>
                <Text style={[TextStyle.text_16_bold, styles.phoneValueStyle]}>{orderStatus?.deviceName}
                </Text>
            </View>
                <View style={{marginLeft: spacing.spacing16, flex: 0.34}}>
                    <Text style={[TextStyle.text_12_normal, styles.phoneTitleStyle]}>Resale Price</Text>
                    <Text
                        style={[TextStyle.text_16_bold, styles.phoneValueStyle]}> {'\u20B9 ' + NumberUtils.getFormattedPrice(orderStatus?.fulfillmentPrice)}</Text>
                </View>
                <View style={styles.deviceIconWrapperStyle}>
                    <Image style={[styles.deviceIconStyle]} source={{uri: orderStatus?.image}}/>
                </View></View>;
        } else {
            return <View style={styles.topBlueViewStyle}><View style={{flex: 0.68}}>
                <Text
                    style={[TextStyle.text_10_bold, styles.phoneTitleStyle]}>{'REQUEST ID ' + orderStatus?.partnerOrderId ?? ""}</Text>
                <Text
                    style={[TextStyle.text_16_bold, styles.phoneValueStyle]}>{orderStatus?.deviceName}</Text>
            </View>
                <View style={{marginLeft: spacing.spacing16, flex: 0.1}}/>
                <View style={styles.deviceIconWrapperStyle}>
                    <Image style={[styles.deviceIconStyle]}
                           source={{uri: orderStatus?.image}}/>
                </View></View>;
        }
    }

    getDetailContent() {
        let voucherCode = this.props.orderStatus?.voucherCode;
        if(!this.props.orderStatus){
            return null;
        }
        if (voucherCode !== null && voucherCode !== undefined && voucherCode?.length > 0) {
            return this.getVoucherDetailContent();
        } else if (!isExchangeFlow()) {
            return this.getSaleDetailContent();
        } else {
            return this.getExchangeDetailContent();
        }
    }

    openNearbyStoresScreen() {
        _this.setState({isLoading: true});
        decideRetailersFlow(this.navigateToRetailersFlow);
    }

    /**
     * method to get location (lat,lng values) in callback
     * @param callback
     */
    getDeviceLocation(callback) {
        _this.setState({isLoading: true})
        if (Platform.OS === 'android') {
            checkLocationPermission(this.DialogViewRef, onPermissionGrant = () => {
                nativeBridgeRef.getLocation((location) => {
                    callback(location);
                });
            });
        } else { // here we are passing null in callback location will be received within event listener ( willReceiveLocation )
            nativeBridgeRef.getUserLocation((params) => {
                _this.setState({isLoading: false})
                this.showGoToSettingsDialoge();
            });
        }
    }

    showGoToSettingsDialoge = () => {
        this.DialogViewRef.showDailog({
            title: LOCATION_OFF,
            message: ON_DEVICE_LOCATION,
            primaryButton: {
                text: TURN_ON_LOCATION, onClick: () => {
                    Linking.canOpenURL('app-settings:').then(supported => {
                        if (supported) {
                            return Linking.openURL('app-settings:');
                        }
                    }).catch(err => console.error('An error occurred', err));
                },
            },
            secondaryButton: {
                text: 'Later', onClick: () => {
                    nativeBridgeRef.goBack();
                },
            },
            checkBox: null,
            cancelable: false,
            imageUrl: images.area_not_servicable,
            isCrossEnabled: false,
            onClose: () => {
            },
        });
    };

    /**
     * here we get response from retailers api
     * @param arrayOfRetailers
     */
    navigateToRetailersFlow(arrayOfRetailers, error) {
        _this.setState({isLoading: false});
        if (error) {
            this.DialogViewRef.showDailog({
                title: 'ALERT',
                message: error,
                primaryButton: {
                    text: 'Retry', onClick: () => {
                        _this.setState({isLoading: true});
                        decideRetailersFlow(this.navigateToRetailersFlow);
                    },
                },
                secondaryButton: {
                    text: 'Cancel', onClick: () => {
                    },
                },
                cancelable: false,
                onClose: () => {
                },
            });
        } else {
            sendNearbyStoresWebengageEvent(arrayOfRetailers?.length);
            if (arrayOfRetailers?.length == 0) {
                this.DialogViewRef.showDailog({
                    title: 'Oops no nearby store found.',
                    primaryButton: {
                        text: 'Close', onClick: () => {},
                    },
                    cancelable: false,
                    onClose: () => { },
                });
            } else if (arrayOfRetailers?.length > 1) {
                this.props.navigation.navigate('NearByRetailers', {params: {data: arrayOfRetailers}});
            } else {
                this.props.navigation.navigate('RetailerDetailScreen', {detailData: arrayOfRetailers[0] ?? {}});
            }
        }
    }

    gpsAction() {
        let webEngageAttr = new Object();
        webEngageAttr[EventAttrs.LOCATION] = BuybackEventLocation.TIMELINE_CHECK;
        logWebEnageEvent(EventNames.GET_DIRECTION, webEngageAttr);

        let location = buybackManagerObj.getLocation();
        nativeBridgeRef?.openOSMap({
            from: {latitude: location?.lat ?? 0, longitude: location?.lng ?? 0},
            to: {
                latitude: this.props.orderStatus?.businessUnit?.latitude ?? 0,
                longitude: this.props.orderStatus?.businessUnit?.longitude ?? 0,
            },
        });
    };

    callAction() {
        let phoneNumber = '';

        let webEngageAttr = new Object();
        webEngageAttr[EventAttrs.LOCATION] = BuybackEventLocation.TIMELINE_CHECK;
        logWebEnageEvent(EventNames.CALL_RETAILER, webEngageAttr);

        if (Platform.OS === PLATFORM_OS.android) {
            phoneNumber = `tel:${this.props.orderStatus?.businessUnit?.mobile}`;
        } else {
            phoneNumber = `telprompt:${this.props.orderStatus?.businessUnit?.mobile}`;
        }
        Linking.canOpenURL(phoneNumber)
            .then((supported) => {
                if (supported) {
                    return Linking.openURL(phoneNumber);
                }
            })
            .catch((err) => console.error('An error occurred', err));
    };

    getAddress(addressObj) {
        return this.getValidString(addressObj?.address) + this.getValidString(addressObj?.city) + this.getValidString(addressObj?.state) + '(' + addressObj?.pincode + ')';
    }

    getSaleDetailContent() {
        let orderStatus = this.props.orderStatus;
        return <ScrollView style={{flex: 1, backgroundColor: colors.white}}>
            <Text
                style={[TextStyle.text_10_bold, styles.requestIdStyle]}>
                {'ORDER ID ' + orderStatus?.partnerOrderId}
            </Text>
            <View style={styles.timelineStatusWrapperStyle}>
                <Image
                    source={getStatusIcon(orderStatus)}
                    style={{width: 20, height: 20}}/>
                <View style={{marginLeft: spacing.spacing20, marginRight: spacing.spacing24}}>
                    <Text
                        style={[TextStyle.text_20_bold]}>
                        {getStatusMessage(orderStatus)}
                    </Text>
                    <Text
                        style={[TextStyle.text_12_normal, {marginTop: spacing.spacing8}]}>
                        {getStatusDescription(orderStatus)}
                    </Text>
                </View>
            </View>
            <View style={[CommonStyle.separator, {marginTop: spacing.spacing12}]}/>
            <View style={{marginLeft: spacing.spacing16}}>
                <Text style={[TextStyle.text_10_bold, {marginTop: spacing.spacing28}]}>ADDRESS</Text>
                <Text style={[TextStyle.text_12_normal, {marginTop: spacing.spacing4, color: colors.charcoalGrey}]}>
                    {orderStatus ? this.getAddress(this.props.orderStatus?.address) : ''}
                </Text>
                <Text style={[TextStyle.text_10_bold, {marginTop: spacing.spacing20}]}>PAYMENT MODE</Text>
                <Text
                    style={[TextStyle.text_12_normal, {
                        marginTop: spacing.spacing4,
                        color: colors.charcoalGrey,
                    }]}>
                    {orderStatus?.paymentMode?.mode}
                </Text>
            </View>
            {this.getCouponCodeView()}
        </ScrollView>;
    }

    getVoucherGeneratedView() {
        let orderStatus = this.props.orderStatus;
        if (orderStatus?.voucherStatus === buybackVoucherStatus.UNUSED) {
            return <View style={{marginRight: spacing.spacing12}}>
                <View style={{flexDirection: 'row'}}>
                    <Text style={TextStyle.text_14_semibold}>{orderStatus?.voucherCode}</Text>
                    <TouchableOpacity onPress={() => {
                        Clipboard.setString(orderStatus?.voucherCode);
                        this.refs.toast.show('Voucher code copied to clipboard');
                    }}>
                        <Image source={images.copy_clipboard}
                               style={{
                                   width: spacing.spacing22,
                                   height: spacing.spacing22,
                                   marginLeft: spacing.spacing8,
                               }}/>
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'row', marginTop: spacing.spacing8}}>
                    <Text style={TextStyle.text_12_normal}>You can redeem your voucher on nearby MI store</Text>
                </View>
                <View style={{marginTop: spacing.spacing12}}>
                    <RightArrowButton image={images.rightBlueArrow}
                                      text={'Check nearby stores'}
                                      imageStyle={styles.rightArrowStyle}
                                      textStyle={styles.buttonText}
                                      style={{marginBottom: 0, alignSelf: 'flex-start'}}
                                      onPress={this.openNearbyStoresScreen}
                    />
                </View>
            </View>;
        }
        return null;
    }

    getVoucherDetailContent() {
        let orderStatus = this.props.orderStatus;
        let deviceName = orderStatus?.deviceName;
        let price = orderStatus ? '\u20B9 ' + NumberUtils.getFormattedPrice(orderStatus?.fulfillmentPrice) : '';
        return <ScrollView style={{flex: 1, backgroundColor: colors.white}}>
            <Text
                style={[TextStyle.text_10_bold, styles.requestIdStyle]}>
                {'ORDER ID ' + orderStatus?.partnerOrderId}
            </Text>
            <View style={styles.timelineStatusWrapperStyle}>
                <Image
                    source={require('../../images/green_right.webp')}
                    style={{width: 24, height: 24}}/>
                <View style={{marginLeft: spacing.spacing20, marginRight: spacing.spacing24}}>
                    <Text
                        style={[TextStyle.text_20_bold]}>
                        Phone Sold
                    </Text>
                    <Text
                        style={[TextStyle.text_12_normal, {marginTop: spacing.spacing8}]}>
                        {buybackOrderStatusDescription.DESCRIPTION_SDP(deviceName, price)}
                    </Text>
                </View>
            </View>
            <Image style={styles.stepsItemSeparator} source={require('../../images/steps_separator.webp')}/>
            <Image style={styles.stepsItemSeparator} source={require('../../images/steps_separator.webp')}/>
            <Image style={styles.stepsItemSeparator} source={require('../../images/steps_separator.webp')}/>
            <Image style={styles.stepsItemSeparator} source={require('../../images/steps_separator.webp')}/>
            <View style={styles.timelineStatusWrapperStyle}>
                <Image
                    source={require('../../images/green_right.webp')}
                    style={{width: 24, height: 24}}/>
                <View style={{marginLeft: spacing.spacing20, marginRight: spacing.spacing24}}>
                    <Text
                        style={[TextStyle.text_20_bold]}>
                        {getVoucherTitle(orderStatus)}
                    </Text>
                    <Text
                        style={[TextStyle.text_12_normal, {marginTop: spacing.spacing8}]}>
                        {getVoucherDescription(orderStatus)}
                    </Text>
                    {this.getVoucherGeneratedView()}
                </View>
            </View>
        </ScrollView>;
    }

    getExchangeDetailContent() {
        let orderStatus = this.props.orderStatus;
        return <ScrollView style={{flex: 1, backgroundColor: colors.white, marginHorizontal: spacing.spacing16}}>
            <View style={{flexDirection: 'row'}}>
                <Image style={styles.greenTickStyle} source={images.green_right}/>
                <Text style={[TextStyle.text_12_bold, styles.exchangeMessageStyle]}>Your visit is confirmed. Submit your
                    phone at the below service centre</Text>
            </View>
            <Text style={[TextStyle.text_10_bold, {color: colors.color_888F97, marginTop: spacing.spacing20}]}>SERVICE
                CENTRE ADDRESS</Text>
            <Text
                style={[TextStyle.text_14_bold, {marginTop: spacing.spacing12}]}>{this.props.orderStatus?.businessUnit?.name}</Text>
            <Text style={[TextStyle.text_12_normal, {marginTop: spacing.spacing4, color: colors.color_888F97}]}>
                {orderStatus ? this.getAddress(this.props.orderStatus?.businessUnit) : ''}
            </Text>
            <View style={{marginTop: spacing.spacing16, flexDirection: 'row'}}>
                <TouchableOpacity onPress={this.callAction} style={{flexDirection: 'row'}}>
                    <Image style={styles.callButtonStyle} source={images.call}/>
                    <Text style={[TextStyle.text_12_bold, {
                        color: colors.color_008DF6,
                        marginLeft: spacing.spacing8,
                        marginTop: spacing.spacing2,
                    }]}>Call Service Center</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.gpsAction} style={{flexDirection: 'row', marginLeft: spacing.spacing8}}>
                    <Image style={styles.callButtonStyle} source={images.ic_gps}/>
                    <Text style={[TextStyle.text_12_bold, {
                        color: colors.color_008DF6,
                        marginLeft: spacing.spacing8,
                        marginTop: spacing.spacing2,
                    }]}>Get Directions</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.specialNoteContainer}>
                <Image style={styles.warningImageStyle} source={images.icon_warning}/>
                <Text style={styles.specialNoteText}>{buybackFlowStrings.specialNoteMessage}</Text>
            </View>
            <View style={CommonStyle.separator}/>
            <Text style={[TextStyle.text_10_bold, {marginTop: spacing.spacing20}]}>{getResalePriceText()}</Text>
            <View style={{flexDirection: 'row', marginTop: spacing.spacing8}}>
                <Text style={{
                    ...TextStyle.text_24_semibold,
                    paddingTop: spacing.spacing2,
                }}>{'\u20B9' + NumberUtils.getFormattedPrice(orderStatus?.fulfillmentPrice)}</Text>
                <TouchableOpacity
                    onPress={() => this.setState({isShowTooltip: true})}
                    ref={(ref) => {
                        infoIcon = ref;
                    }}
                    style={styles.infoIconContainer}>
                    <Text style={[TextStyle.text_10_bold, {color: colors.charcoalGrey}]}>i</Text>
                </TouchableOpacity>
            </View>
            <Text style={[TextStyle.text_14_semibold, {marginTop: spacing.spacing4}]}>Price expires
                in {daysDifference(getCurretDateMilliseconds(), orderStatus?.expireOn ?? getCurretDateMilliseconds())} days</Text>
            <View style={[CommonStyle.separator, {marginTop: spacing.spacing16}]}/>
            <TouchableOpacity onPress={() => this.cancelBuybackRequest()} style={{marginTop: spacing.spacing16}}>
                <Text style={[TextStyle.text_14_bold, {color: colors.color_008DF6}]}>{CANCEL_REQUEST}</Text>
            </TouchableOpacity>
        </ScrollView>;
    }

    cancelOrder() {
        let orderStatus = _this.props.orderStatus;
        _this.setState({isModalVisible: false, isLoading: true});
        let data = buybackManagerObj.getBuybackStatusDataFirstItem();
        let webEngageAttr = new Object();
        webEngageAttr[EventAttrs.LOCATION] = BuybackEventLocation.BUYBACK;
        webEngageAttr[EventAttrs.EXCHANGE_PRICE] = data?.price;
        webEngageAttr[EventAttrs.TAG] = BuybackEventLocation.EXCHANGE_REQUEST;
        logWebEnageEvent(EventNames.CANCEL_REQUEST_CONFIRMATION, webEngageAttr);
        cancelRequest(orderStatus?.orderId, WorkFlowStage.OC, WorkFlowStage.CC, '', APIData.customer_id, '', (response, error) => {
            _this.setState({isLoading: false});
            if (response) {
                nativeBridgeRef.refreshBuybackStatus();
                nativeBridgeRef.goBack();
            } else {
            }
        });
    }

    removeCancelRequestDialog() {
        _this.setState({isModalVisible: false});
    }

    convertRequestInResale() {
        let orderStatus = _this.props.orderStatus;
        _this.setState({isModalVisible: false, isLoading: true});
        cancelRequest(orderStatus?.orderId, WorkFlowStage.OC, WorkFlowStage.CC, REASON_CODE_CWOS, APIData.customer_id, 'CUSTOMER', (response, error) => {
            _this.setState({isLoading: false});
            if (response) {
                nativeBridgeRef.refreshBuybackStatus();
                getBuybackStatusData((response, error) => {
                    if (response) {
                        let buybackData = response?.data?.buybackStatus;
                        let filteredData = buybackData.filter((value) => {
                            return (value.buyBackStatus === buybackStatusConstants.QUOTE_GENERATED && value?.deviceInfo?.deviceIdentifier === APIData?.deviceIdentifier);
                        });
                        if (filteredData?.length >= 1) {
                            buybackManagerObj.setBuybackStatusData(buybackData);
                            _this.props.navigation.navigate('BuybackComponent');
                            _this.props.navigation.pop();
                        } else {
                            nativeBridgeRef.goBack();
                        }
                    } else {
                        nativeBridgeRef.goBack();
                    }
                });
            } else {
                nativeBridgeRef.goBack();
            }
        });
    }

    render() {
        if (this.DialogViewRef && this.props.isError) {
            this.DialogViewRef.showDailog({
                message: this.props.errorMessage,
                primaryButton: {
                    text: this.props.errorMessageButtonTitle, onClick: () => {
                    },
                },
                cancelable: true,
                onClose: () => {
                },
            });
        }
        return (
            <SafeAreaView style={styles.SafeArea}>
                <View style={{flex: 1, backgroundColor: colors.white}} ref={ref => (this.parentRef = ref)}>
                    <Toast ref="toast" position="bottom" />
                    <DialogView ref={ref => (this.DialogViewRef = ref)} />
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={this.state.isModalVisible}
                        onRequestClose={_this.removeCancelRequestDialog}
                    >
                        <TouchableOpacity style={styles.modalItemContainer}
                                          onPress={() => _this.removeCancelRequestDialog()}>
                            <View style={{
                                backgroundColor: colors.white,
                                borderRadius: 4,
                                marginHorizontal: spacing.spacing16,
                            }}>
                                <CommonBuybackAlertUI
                                    data={buybackManagerObj.getBuybackStatusDataFirstItem()}
                                    title={{
                                        text: BUYBACK_CANCEL_CONFIRM,
                                        style: {...TextStyle.text_16_semibold, color: colors.charcoalGrey},
                                    }}
                                    primaryButton={{text: SCHEDULE_PICKUP, action: this.convertRequestInResale}}
                                    secondaryButton={{text: CANCEL_REQUEST, action: this.cancelOrder}}
                                />
                            </View>
                        </TouchableOpacity>
                    </Modal>
                    <BlockingLoader
                        visible={this.props.isLoading || this.state.isLoading}
                        loadingMessage={'Loading...'}
                    />
                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#004890', '#0282F0']}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <HeaderBackButton tintColor={colors.white}
                                              onPress={() => {
                                                  this.onBackPress();
                                              }}/>
                            <TouchableOpacity style={{marginTop: 16, flexDirection: 'row', marginRight: 16}}
                                              onPress={() => {
                                                  nativeBridgeRef.openChat();
                                              }}>
                                <Image style={styles.chatIconStyle} source={images.icon_chat}></Image>
                                <Text style={[TextStyle.text_14_normal, styles.chatTextStyle]}>Chat</Text>
                            </TouchableOpacity>

                        </View>
                        {this.getHeaderContent()}
                    </LinearGradient>
                    {this.getDetailContent()}
                    <View style={styles.poweredbyStyle}>
                        {this.getPoweredByView()}
                    </View>
                    {this.state.isShowTooltip
                        ? <TooltipComponent
                            parentRef={this.parentRef}
                            childRef={infoIcon}
                            data={{
                                'text': 'The final price quote will vary depending on your device condition. The device condition will be inspected at the service centre ',
                                'image': images.ic_whiteClose,
                            }}
                            onCloseTooptip={() => this.setState({isShowTooltip: false})}
                            arrowDirection={ArrowDirection.TOP}
                            style={{backgroundColor: '#393939'}}
                            isVisible={this.state.isShowTooltip}
                        />
                        : null}
                </View>
            </SafeAreaView>

        );
    }
}

const
    styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: colors.white,
            padding: spacing.spacing16,
        },
        topBlueViewStyle: {
            flexDirection: 'row',
            paddingRight: spacing.spacing32,
        },
        phoneTitleStyle: {
            marginTop: spacing.spacing16,
            marginLeft: spacing.spacing16,
            color: colors.white,
        },
        phoneValueStyle: {
            marginTop: spacing.spacing12,
            marginLeft: spacing.spacing16,
            color: colors.white,
        },
        requestIdStyle: {
            marginTop: spacing.spacing24,
            marginLeft: spacing.spacing16,
        },
        timelineStatusWrapperStyle: {
            flexDirection: 'row',
            marginTop: spacing.spacing20,
            marginLeft: spacing.spacing20,
        },
        rewardsViewWrapperStyle: {
            backgroundColor: colors.blue,
            marginTop: spacing.spacing28,
            marginBottom: spacing.spacing24,
            paddingBottom: spacing.spacing16,
        },
        rewardsIconStyle: {
            marginTop: spacing.spacing8,
            width: spacing.spacing24,
            height: spacing.spacing28,
            alignSelf: 'center',
        },
        couponTextStyle: {
            marginTop: spacing.spacing8,
            marginLeft: spacing.spacing16,
            marginRight: spacing.spacing16,
            alignSelf: 'center',
            textAlign: 'center',
            color: colors.white,
        },
        couponCodeWrapperStyle: {
            width: '90%',
            height: spacing.couponWrapperHeight,
            marginTop: spacing.spacing24,
            alignSelf: 'center',
            justifyContent: 'center',
            borderRadius: 2,
            backgroundColor: colors.white,
            padding: spacing.spacing20,
        },
        couponCodeStyle: {
            alignSelf: 'center',
            color: colors.seaGreen,
        },
        saveUptoTextStyle: {
            alignSelf: 'center',
            color: colors.charcoalGrey,
            marginTop: spacing.spacing8,
            textAlign: 'center',
        },
        explorePlansTextStyle: {
            alignSelf: 'center',
            color: colors.blue028,
            marginTop: spacing.spacing12,
        },
        deviceIconWrapperStyle: {
            marginLeft: spacing.spacing16,
            flex: 0.22,
            justifyContent: 'flex-end',
        },
        deviceIconStyle: {
            width: spacing.spacing80,
            height: spacing.spacing80,
            marginRight: spacing.spacing16,
            marginLeft: spacing.spacing16,
            resizeMode: 'contain',
        },
        chatIconStyle: {
            width: spacing.spacing16,
            height: spacing.spacing16,
            marginTop: spacing.spacing4,
            resizeMode: 'contain',
        },
        chatTextStyle: {
            marginLeft: spacing.spacing6,
            textAlign: 'center',
            color: colors.white,
        },
        SafeArea: {
            flex: 1,
            backgroundColor: colors.white,
        },
        poweredbyStyle: {
            flexDirection: 'row',
            alignSelf: 'flex-end',
            marginTop: spacing.spacing8,
            marginBottom: spacing.spacing16,
            marginRight: spacing.spacing20,
        },
        poweredbyImageStyle: {
            width: spacing.spacing56,
            height: spacing.spacing14,
            marginLeft: spacing.spacing4,
            resizeMode: 'contain',
        },
        greenTickStyle: {
            width: spacing.spacing16,
            height: spacing.spacing16,
            marginTop: spacing.spacing16,
            resizeMode: 'contain',
        },
        exchangeMessageStyle: {
            marginLeft: spacing.spacing4,
            color: colors.color_212121,
            marginTop: spacing.spacing14,
            marginRight: spacing.spacing12,
        },
        specialNoteContainer: {
            flexDirection: 'row',
            marginTop: spacing.spacing20,
            marginBottom: spacing.spacing20,
            padding: spacing.spacing8,
            borderRadius: 4,
            backgroundColor: colors.color_FFEECE,
        },
        warningImageStyle: {
            width: 24,
            height: 24,
            marginLeft: 8,
            marginRight: 12,
            alignItems: 'center',
            resizeMode: 'contain',
        },
        specialNoteText: {
            ...TextStyle.text_12_normal,
            color: colors.color_212B36,
            marginRight: spacing.spacing40,
        },
        callButtonStyle: {
            width: 20,
            height: 20,
            alignItems: 'center',
            resizeMode: 'contain',
        },
        modalItemContainer: {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: colors.color_8A000000,
            overflow: 'hidden',
        },
        infoIconContainer: {
            marginTop: spacing.spacing1,
            width: 18,
            height: 18,
            borderRadius: 9,
            borderWidth: 2,
            borderColor: colors.charcoalGrey,
            alignItems: 'center',
            justifyContent: 'center',
            paddingBottom: 4,
            paddingLeft: 1,
            marginLeft: spacing.spacing12,
        },
        stepsItemSeparator: {
            width: 4,
            height: 16,
            marginLeft: spacing.spacing30,
        },
        rightArrowStyle: {
            marginRight: 0,
            marginLeft: 8,
            width: 8,
            height: 12,
            resizeMode: 'stretch',
        },
        buttonText: {
            ...TextStyle.text_14_bold,
            color: colors.blue028,
        },
    });
