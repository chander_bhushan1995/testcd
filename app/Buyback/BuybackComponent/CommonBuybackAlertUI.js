import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {CANCEL_REQUEST, NO_RETAILERS, PRICE_EXP_IN, SCHEDULE_PICKUP} from '../../Constants/AppConstants';
import {daysDifference, getCurretDateMilliseconds} from '../../commonUtil/DateUtils';
import ButtonWithLoader from '../../CommonComponents/ButtonWithLoader';
import spacing from '../../Constants/Spacing';
import {TextStyle} from '../../Constants/CommonStyle';
import colors from '../../Constants/colors';
import {getResalePriceText} from '../BuyBackUtils/BuybackUtils';

const CommonBuybackAlertUI = props => {

    const data = props.data ?? {};
    const title = props.title ?? {};
    const primaryButton = props.primaryButton ?? {};
    const secondaryButton = props.secondaryButton ?? {};

    return (
        <View>
            <Text
                style={[styles.texts, styles.heading, title.style]}>{title.text ?? NO_RETAILERS}</Text>
            <Text style={[styles.texts, styles.resalePrice]}>{getResalePriceText() + " is"}</Text>
            <Text style={[styles.texts, styles.price]}>{`₹ ${data?.price}`}</Text>
            <Text
                style={[styles.texts, styles.priceExpires]}>{PRICE_EXP_IN(daysDifference(getCurretDateMilliseconds(), data?.expireOn ?? getCurretDateMilliseconds()))}</Text>
            <View style={styles.primaryBtnContainer}>
                <ButtonWithLoader
                    isLoading={false}
                    enable={true}
                    Button={{
                        text: primaryButton?.text ?? SCHEDULE_PICKUP,
                        onClick: () => {
                            primaryButton?.action ? primaryButton?.action() : null;
                        },
                    }}
                />
                <View style={styles.cancelBtn}>
                    <TouchableOpacity onPress={() => {
                        secondaryButton?.action();
                    }}>
                        <Text style={styles.cancelBtnText}>{secondaryButton?.text ?? CANCEL_REQUEST}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    texts: {
        marginHorizontal: spacing.spacing24,
        textAlign: 'center',
        marginTop: spacing.spacing8,
    },
    heading: {
        ...TextStyle.text_14_normal,
        marginTop: spacing.spacing32,
        color: colors.charcoalGrey,
    },
    resalePrice: {
        ...TextStyle.text_14_bold,
        color: colors.seaGreen,
    },
    price: {
        ...TextStyle.text_24_bold,
        color: colors.seaGreen,
        paddingTop: spacing.spacing2,
    },
    priceExpires: {
        ...TextStyle.text_14_normal,
        color: colors.grey,
    },
    primaryBtnContainer: {
        marginTop: spacing.spacing24,
        marginHorizontal: spacing.spacing16,
    },
    cancelBtn: {
        marginTop: spacing.spacing16,
        height: 48,
        justifyContent: 'center',
        marginBottom: spacing.spacing16,
    },
    cancelBtnText: {
        textAlign: 'center',
        color: colors.blue028,
    },
});

export default CommonBuybackAlertUI;
