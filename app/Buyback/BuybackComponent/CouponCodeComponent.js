import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {TextStyle} from '../../Constants/CommonStyle';
import spacing from '../../Constants/Spacing';
import colors from '../../Constants/colors';
import Loader from '../../Components/Loader';
import {isThirdPartyFlow} from '../BuyBackUtils/BuybackUtils';

export default class CouponCodeComponent extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let couponApplyView;
        let couponErrorView;

        if (this.props.isApplying) {
            couponApplyView = (
                <View style={{marginTop: 20, marginLeft: 16}}>
                    <Loader isLoading={this.props.isApplying} size={20} color={colors.blue028}/>
                </View>
            );
        } else if (this.props.isApplied) {
            couponApplyView = (
                <Text style={[TextStyle.text_14_bold, {color: colors.seaGreen}]}>Applied</Text>
            );
        } else {
            couponApplyView = (
                <Text style={[TextStyle.text_14_bold, {color: colors.blue028}]}>Apply</Text>
            );
        }
        if (this.props.isError) {
            couponErrorView = (
                <Text style={[TextStyle.text_14_normal, styles.couponErrorTextStyle]}>{this.props.errorMessage}</Text>);
        }
        if (!isThirdPartyFlow()) {
            return null;
        } else {
            return (
                <View style={this.props.style}>
                    <View style={styles.couponWrapperStyle}>
                        <Text style={[TextStyle.text_14_bold, {width: '82%'}]}>{this.props.couponCode}</Text>
                        <TouchableOpacity style={{alignItems: 'flex-end'}}
                                          disabled={this.props.isApplied || this.props.isApplying}
                                          onPress={() => this.props.apply()}>
                            {couponApplyView}
                        </TouchableOpacity>
                    </View>
                    {couponErrorView}
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    couponWrapperStyle: {
        height: 48,
        padding: spacing.spacing12,
        borderRadius: 4,
        borderWidth: 1,
        flexDirection: 'row',
        borderColor: colors.greye0,
    },
    couponErrorTextStyle: {
        marginTop: spacing.spacing4,
        color: colors.salmonRed,
    },
});
