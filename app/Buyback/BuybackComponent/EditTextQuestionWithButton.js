import React, {Component} from "react";
import {BackHandler, NativeModules, StyleSheet, Text, View} from 'react-native';
import {TextStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import InputBox from "../../Components/InputBox";
import {SUBMIT_NAME_EMAIL} from "../../Constants/ApiUrls";
import ButtonWithLoader from "../../CommonComponents/ButtonWithLoader";
import * as BuybackApiHelper from '../buybackActions/BuybackApiHelper';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import {APIData} from '../../../index';
import {makePostRequest} from "../../network/ApiManager";
import {updateUserDetail} from '../buybackActions/BuybackApiHelper';
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

const chatBrigeRef = NativeModules.ChatBridge;
let answersData = {questionType: "ORDER_QUESTION", data: []};
let buyBackStatusDatum;
export default class EditTextQuestionWithButton extends Component {

    constructor(props) {
        super(props);
        this.state = {
            answerValue: "",
            submitEnabled: false,
            isLoading: false,
            pincode: "",
            errorMessage: null,
        }
        let data = buybackManagerObj.getBuybackStatusDataFirstItem();
        this.setState({pincode:parseInt(data?.pincode)});
    }


    render() {
        return (
            <View style={styles.container}>
                {this.renderItem(this.props)}
            </View>
        )
    }

    setButtonState() {
        this.setState({submitEnabled: this.state.answerValue !== ""})
    }

    setLoadingState(isLoading) {
        this.setState({isLoading: isLoading, errorMessage: null})
    }

    executeSaveQuestionsApi(data, quoteId, questionId) {
        BuybackApiHelper.saveAnswersForQuestions(quoteId, data, (response, error) => {
            if (response) {
                if(questionId === 'Q-CN' || questionId === 'Q-ADDR'){
                    this.props.onClick(buyBackStatusDatum, this.props.index, this.props.index, questionId);
                }else{
                    this.props.onClick(buyBackStatusDatum, this.props.index, this.props.index);
                }
                this.setLoadingState(false);
            } else {
                    this.setLoadingState(false);
            }
        });
    }

    componentDidMount() {
        let questionId = this.props?.data?.questionId;
        if(questionId === 'Q-ADDR'){
            this.setLoadingState(true);
            BuybackApiHelper.getAddressDetailsApi(APIData.customer_id, (response, error) => {
                if (response) {
                    this.setLoadingState(false);
                    let addressVal = "";
                    if(response?.data?.customers[0] !== null && response?.data?.customers[0] !== undefined &&
                        response?.data?.customers[0]?.addresses[0] !== null && response?.data?.customers[0]?.addresses[0] !== undefined){
                        addressVal = response?.data?.customers[0]?.addresses[0]?.addressLine1;
                    }
                    this.setState({answerValue: addressVal}, () => {
                        this.setButtonState();
                    });
                } else {
                    this.setLoadingState(false);
                }
            });
        }
    }

    submitNameAndEmailData(queId, answer, custId) {
            let bodyData = {
                'customers': [{
                    'custId': custId,
                }],
            };
            if (queId === 'Q-CN' && answer) {
                this.makeUserInfoObj(answer, "")
                bodyData.customers[0].firstName = answer;
            }

            if (queId === 'Q-EMAIL' && answer) {
                this.makeUserInfoObj("", answer)
                bodyData.customers[0].emailId = answer;
            }

        makePostRequest(SUBMIT_NAME_EMAIL, bodyData, (response, error) => {
                if (response) {
                    this.setLoadingState(false);
                    let key = queId === 'Q-CN' ? 'userName' : 'userEmail';
                    let userInfo = {
                        [key]: answer
                    }
                    chatBrigeRef.updateUserInfo(userInfo);
                } else {
                    this.setLoadingState(false);
                }
        });
    }

    //TODO
    makeUserInfoObj(userName, userEmail){
        let userInfo = parseJSON(buybackManagerObj.getUserInfo());
        if(userName && userName?.length > 0){
            userInfo = {...userInfo, userName: userName};
        }else if(userEmail && userEmail?.length > 0){
            userInfo = {...userInfo, userEmail: userEmail};
        }
        buybackManagerObj.setUserInfo(JSON.stringify(userInfo));
    }

    executeButtonClick = (props) => {
        buyBackStatusDatum = buybackManagerObj.getBuybackStatusData();
        answersData.data = [];
        let questionId = props?.data?.questionId;
        answersData.data.push({questionId: props?.data?.questionId, answer: this.state.answerValue});
        if(questionId === 'Q-CN'){
            buybackManagerObj.setUserName(this.state.answerValue)
        }
        if(questionId === 'Q-ADDR'){
            buybackManagerObj.setAddress(this.state.answerValue);
            answersData.data.push({questionId: 'Q-CMOBL', answer: props?.data?.userInfo?.userMobile});
        }
        if (this.props?.data?.userInfo?.userName !== undefined || this.props?.data?.userInfo?.userEmail !== undefined) {
            answersData.data.push({questionId: 'Q-CN', answer: props?.data?.userInfo?.userName});
            answersData.data.push({questionId: 'Q-CMOBL', answer: props?.data?.userInfo?.userMobile});
            answersData.data.push({questionId: 'Q-EMAIL', answer: props?.data?.userInfo?.userEmail});
        }
        this.executeSaveQuestionsApi(answersData, buyBackStatusDatum[0]?.quoteId, questionId);
        if ((questionId === 'Q-CN' || questionId === 'Q-EMAIL') && props?.data?.customerId) {
            this.submitNameAndEmailData(questionId, this.state.answerValue, APIData.customer_id)
        }
        this.setLoadingState(true);
    }

    renderItem = (props) => {
        let subHeadingView = props?.data?.questionId === 'Q-ADDR' ? <Text
            style={[TextStyle.text_12_normal, styles.questionDescriptionTextStyle]}>{"Enter address for pincode " +
        this.state.pincode + "."}</Text> : null
        return <View>
            <View style={{flexDirection: 'row'}}>
                <Text style={[TextStyle.text_14_bold, styles.questionNumberStyle]}>{props?.index + 1}</Text>
                <Text style={[TextStyle.text_16_bold, styles.questionTextStyle]}>{props?.data?.title}</Text>
            </View>
            {subHeadingView}
            <InputBox
                ref={input => {
                    this.textInput = input;
                }}
                height={props?.data?.questionId === 'Q-ADDR' ? 98 : 40}
                containerStyle={{justifyContent: "space-between", marginLeft: spacing.spacing40}}
                inputHeading={props?.data?.inputHeading}
                placeholder={props?.data?.placeholder}
                keyboardType={props?.data?.questionId === 'Q-EMAIL' ? 'email-address' : 'default'}
                multiline={props?.data?.questionId === 'Q-ADDR' ? true : false}
                autoFocus={true}
                textAlignVertical={props?.data?.questionId === 'Q-ADDR' ? "top" : 'center'}
                errorMessage={this.state.errorMessage}
                onChangeText={inputMessage => {
                    this.setState({answerValue: inputMessage}, () => {
                        this.setButtonState();
                    });
                }
                }
                editable={true}
            value={this.state.answerValue}>
            </InputBox>
            <View style={[styles.nextButtonStyle]}>
                <ButtonWithLoader
                    isLoading={this.state.isLoading}
                    enable={this.state.submitEnabled}
                    Button={{
                        text: "Next",
                        onClick: () => {
                            if (this.props?.data?.inputValidate !== undefined) {
                                if (this.props?.data?.inputValidate(this.state.answerValue)) {
                                    this.executeButtonClick(props);
                                } else {
                                    this.setState({errorMessage: this.props?.data?.errorMessage})
                                }
                            } else {
                                this.executeButtonClick(props);
                            }


                        }
                    }
                    }/>
            </View>
        </View>
    }
}

const
    styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: colors.white,
            marginBottom: spacing.spacing24,
        },
        questionNumberStyle: {
            width: spacing.spacing24,
            height: spacing.spacing24,
            borderRadius: 24 / 2,
            backgroundColor: colors.blue,
            color: colors.white,
            textAlign: 'center',
            overflow: "hidden"
        },
        questionTextStyle: {
            marginLeft: spacing.spacing16,
            marginBottom: spacing.spacing8,
        },
        questionDescriptionTextStyle: {
            marginLeft: spacing.spacing40,
            marginBottom: spacing.spacing36,
            color: colors.grey,
        },
        nextButtonStyle: {
            borderRadius: 2,
            marginTop: spacing.spacing24,
            marginLeft: spacing.spacing40,
            marginBottom: spacing.spacing16,
        },

    });
