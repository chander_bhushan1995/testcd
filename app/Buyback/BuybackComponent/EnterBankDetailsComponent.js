import React, {Component} from 'react';
import {
    BackHandler,
    Image,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import {TextStyle} from '../../Constants/CommonStyle';
import spacing from '../../Constants/Spacing';
import colors from '../../Constants/colors';

import InputBox from '../../Components/InputBox';
import ButtonWithLoader from '../../CommonComponents/ButtonWithLoader';
import {IMPS, MOBILE_BUYBACK} from '../../Constants/WebengageAttributes';
import {BUYBACK_PAYMENT_DETAILS} from '../../Constants/WebengageEvents';
import CheckBox from 'react-native-check-box';
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';
import {contactCreationApi, getAccountStatusApi, savePayment} from '../buybackActions/BuybackApiHelper';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import images from '../../images/index.image';
import DialogView from '../../Components/DialogView';
import {buybackFlowStrings, enterBankDetailsScreenState} from '../../Constants/BuybackConstants';
import {SUBMIT_PICKUP_REQUEST} from '../../Constants/AppConstants';
import {IDFenceErrorApiErrorMsg} from '../../Constants/ApiError.message';
import {ABB, SERVED_BY} from '../../Constants/WebengageAttrKeys';
import {getServedByValueFromFirstItem} from '../BuyBackUtils/BuybackUtils';

let timeoutValue = 100;
let timeoutValueInterval = 3000;

export default class EnterBankDetailsComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            accountNumberValue: '',
            confirmAccountNumberValue: '',
            IFSCCodeValue: '',
            accountNumberValueErrorMessage: '',
            confirmAccountNumberValueErrorMessage: '',
            IFSCCodeValueErrorMessage: '',
            submitEnabled: false,
            isLoading: false,
            checked: false,
            rightImage: '',
            screenState: enterBankDetailsScreenState.STATE_INITIAL,
        };
    }

    onBackPress = () => {
        this.props.navigation.pop();
        return true;
    };

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }

    enableButton() {
        if (this.state.screenState === enterBankDetailsScreenState.STATE_FINAL_SUBMIT) {
            return this.state.checked;
        }
        let accountNumberLength = this.state.accountNumberValue.length;
        let confirmAccountNumberLength = this.state.confirmAccountNumberValue.length;
        let regex = /^[a-zA-Z]*$/;
        let ifscValidation = this.state.IFSCCodeValue.length >= 4 && regex.test(this.state.IFSCCodeValue.substring(0, 4));
        return accountNumberLength >= 9 && accountNumberLength <= 18 && confirmAccountNumberLength >= 9 && confirmAccountNumberLength <= 18 && this.state.accountNumberValue === this.state.confirmAccountNumberValue && ifscValidation;
    }

    setButtonState() {
        let enable = this.enableButton();
        this.setState({submitEnabled: enable, rightImage: enable ? images.green_right : ''});
    }

    getInitialStateUi() {
        return (this.state.screenState === enterBankDetailsScreenState.STATE_INITIAL) ?
            <ScrollView>
                <Text style={[TextStyle.text_20_bold, styles.headingStyle]}>{this.props.data.title}</Text>
                <InputBox
                    ref={input => {
                        this.textInput = input;
                    }}
                    containerStyle={styles.textInputContainer}
                    inputHeading={this.props.data.accountNumberTitle}
                    errorMessage={this.state.accountNumberValueErrorMessage}
                    multiline={false}
                    numberOfLines={1}
                    autoFocus={true}
                    keyboardType={'numeric'}
                    onChangeText={inputMessage => {
                        this.setState({accountNumberValue: inputMessage}, () => {
                            this.setButtonState();
                        });
                    }
                    }
                    editable={true}
                    rightImage={this.state.rightImage}>
                </InputBox>
                <InputBox
                    ref={input => {
                        this.textInput = input;
                    }}
                    containerStyle={styles.textInputContainer}
                    inputHeading={this.props.data.confirmAccountNumberTitle}
                    errorMessage={this.state.confirmAccountNumberValueErrorMessage}
                    multiline={false}
                    numberOfLines={1}
                    autoFocus={true}
                    secureTextEntry={true}
                    keyboardType={'numeric'}
                    onChangeText={inputMessage => {
                        let accNumberErrorMessage;
                        if (this.state.accountNumberValue.length < 9 || this.state.accountNumberValue.length > 18) {
                            accNumberErrorMessage = 'Enter a 9-18 digit account number.';
                        }
                        this.setState({
                            confirmAccountNumberValue: inputMessage,
                            accountNumberValueErrorMessage: accNumberErrorMessage,
                        }, () => {
                            this.setButtonState();
                        });
                    }
                    }
                    editable={true}
                    rightImage={this.state.rightImage}>
                </InputBox>
                <InputBox
                    ref={input => {
                        this.textInput = input;
                    }}
                    containerStyle={styles.textInputContainer}
                    inputHeading={this.props.data.IFSCCodeTitle}
                    errorMessage={this.state.IFSCCodeValueErrorMessage}
                    autoCapitalize={'characters'}
                    multiline={false}
                    numberOfLines={1}
                    autoFocus={true}
                    onChangeText={inputMessage => {
                        let confirmAccNumberErrorMessage;
                        let IFSCErrorMessage;
                        if (this.state.confirmAccountNumberValue.length === 0) {
                            confirmAccNumberErrorMessage = 'Enter your account number for confirmation.';
                        } else if (this.state.accountNumberValue !== this.state.confirmAccountNumberValue) {
                            confirmAccNumberErrorMessage = 'Account number and Confirm account number does not match.';
                        }
                        let regex = /^[a-zA-Z]*$/;
                        if (this.state.IFSCCodeValue.length >= 4 && !regex.test(this.state.IFSCCodeValue.substring(0, 4))) {
                            IFSCErrorMessage = 'Enter a valid IFSC Code';
                        }
                        this.setState({
                            IFSCCodeValue: inputMessage,
                            confirmAccountNumberValueErrorMessage: confirmAccNumberErrorMessage,
                            IFSCCodeValueErrorMessage: IFSCErrorMessage,
                        }, () => {
                            this.setButtonState();
                        });
                    }
                    }
                    editable={true}>
                </InputBox>
            </ScrollView> : null;
    }

    getDeclarationSection() {

    }

    getSubText() {
        return (this.state.screenState === enterBankDetailsScreenState.STATE_INITIAL) ?
            <Text style={[TextStyle.text_12_normal]}>{buybackFlowStrings.enter_bank_btn_subtext}</Text> : null;
    }

    getBankLogo() {
        return (this.state.screenState === enterBankDetailsScreenState.STATE_FINAL_SUBMIT) ?
            <View style={{
                justifyContent: 'center',
                alignItems: 'center',
                flex: 1,
                marginTop: spacing.spacing40,
            }}>
                <Image source={images.bank_logo_blue} style={styles.bankLogoStyle}/>
                <Text
                    style={[TextStyle.text_20_bold, {
                        marginTop: spacing.spacing28,
                        textAlign: 'center',
                    }]}>{buybackFlowStrings.verify_account_success}</Text></View> : null;
    }

    submitClick() {
        if (this.state.screenState === enterBankDetailsScreenState.STATE_INITIAL) {
            this.DialogViewRef.showDailog({
                imageUrl: images.lock,
                title: buybackFlowStrings.verify_bank_account_title,
                message: buybackFlowStrings.verify_bank_account_description,
                primaryButton: {
                    text: buybackFlowStrings.confirm_account, onClick: () => {
                        this.callContactCreationApi();
                        this.setState({
                            screenState: enterBankDetailsScreenState.STATE_ACCOUNT_STATUS_PROCESSING,
                            isLoading: true,
                        });
                    },
                },
                cancelable: true,
                onClose: () => {
                },
            });
            return;
        }
        this.saveAccountDetailsApi();
    }

    callContactCreationApi() {
        contactCreationApi({
            ifscCode: this.state.IFSCCodeValue,
            accountNo: this.state.accountNumberValue,
            accountType: 'BANK_ACCOUNT',
        }, (response, error) => {
            if (response) {
                this.callGetAccountStatusApi(response?.data?.fundId);
            } else {
                this.showApiErrorDialog();
            }
        });
    }

    callGetAccountStatusApi(fundId) {
        setTimeout(() => {
            try {
                getAccountStatusApi(fundId, (response, error) => {
                    if (response) {
                        if (response?.data?.status === 'active') {
                            this.setState({
                                screenState: enterBankDetailsScreenState.STATE_FINAL_SUBMIT,
                                isLoading: false,
                            });
                        } else if (timeoutValue <= 30000) {
                            timeoutValue = timeoutValue + timeoutValueInterval;
                            this.callGetAccountStatusApi(fundId);
                        } else {
                            this.setState({isLoading: false});
                        }
                    } else {
                        this.showApiErrorDialog();
                    }
                });
            } catch (e) {
            }
        }, timeoutValueInterval);
    }

    showApiErrorDialog() {
        this.DialogViewRef?.showDailog({
            title: 'Technical Error',
            message: IDFenceErrorApiErrorMsg.DEFAULT,
            primaryButton: {
                text: 'Okay, got it', onClick: () => {
                    this.setState({isLoading: false});
                },
            },
            cancelable: false,
            onClose: () => {
                this.setState({isLoading: false});
            },
        });
    }

    saveAccountDetailsApi() {
        let data = buybackManagerObj.getBuybackStatusDataFirstItem();
        let eventData = new Object();
        eventData['Location'] = MOBILE_BUYBACK;
        eventData['Type'] = IMPS;
        eventData['QuoteID'] = data?.quoteId;
        eventData[ABB] = buybackManagerObj?.isABBFlow();
        eventData[SERVED_BY] = getServedByValueFromFirstItem();
        logWebEnageEvent(BUYBACK_PAYMENT_DETAILS, eventData);
        let saveAccountDetails = {
            'quoteId': data?.quoteId,
            'paymentMode': IMPS,
            'accountNumber': this.state.accountNumberValue,
            'bankIFSCCode': this.state.IFSCCodeValue,
        };
        this.executeSaveQuestionsApi(saveAccountDetails);
        this.setState({isLoading: true});
    }

    getSubmitButton() {
        let checkboxView = null;
        if (this.state.screenState === enterBankDetailsScreenState.STATE_FINAL_SUBMIT) {
            checkboxView = <View style={styles.tncViewStyle}>
                <CheckBox
                    style={styles.checkboxStyle}
                    checkedImage={<Image source={require('../../images/ic_check_box.webp')}
                                         style={styles.checkbox}/>}
                    unCheckedImage={<Image source={require('../../images/ic_check_box_outline_blank.webp')}
                                           style={styles.checkbox}/>}
                    isChecked={this.state.checked}
                    onClick={() => {
                        this.setState(
                            {checked: !this.state.checked}, () => {
                                this.setButtonState();
                            });
                    }
                    }
                />
                <Text style={[TextStyle.text_12_normal]}>I agree to all </Text>
                <TouchableOpacity onPress={() => {
                    this.props.navigation.navigate('TermsAndConditions', {});
                }}
                >
                    <Text style={[TextStyle.text_12_normal, styles.tncTextStyle]}>Terms & Conditions</Text>
                </TouchableOpacity>
                <Text style={[TextStyle.text_12_normal]}> and </Text>
                <TouchableOpacity onPress={() => {
                    this.props.navigation.navigate('BuybackDeclaration', {});
                }}
                >
                    <Text style={[TextStyle.text_12_normal, styles.tncTextStyle]}>Declaration</Text>
                </TouchableOpacity>
            </View>;
        }
        let screenState = this.state.screenState;
        let buttonText = screenState === enterBankDetailsScreenState.STATE_FINAL_SUBMIT ? SUBMIT_PICKUP_REQUEST : 'Submit';
        return (screenState === enterBankDetailsScreenState.STATE_ACCOUNT_STATUS_PROCESSING) ? null :
            <View style={styles.nextButtonStyle}>
                {checkboxView}
                <ButtonWithLoader
                    isLoading={this.state.isLoading}
                    enable={this.state.submitEnabled}
                    Button={{
                        text: buttonText,
                        onClick: () => this.submitClick(),
                    }
                    }/>
            </View>;
    }

    getLoadingView() {
        return (this.state.screenState === enterBankDetailsScreenState.STATE_ACCOUNT_STATUS_PROCESSING) ?
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <Text style={[TextStyle.text_14_bold]}>Please wait</Text>
                <Text style={[TextStyle.text_12_normal]}>Securely redirecting to bank page</Text>
            </View> : null;
    }

    executeSaveQuestionsApi(saveAccountDetails, dispatch) {
        //it is a hack find root cause and remove this time i found same instance of api manager
        savePayment(saveAccountDetails, (response, error) => {
            this.setState({isLoading: false});
            if (response) {
                this.props.navigation.navigate('MobilePickupScheduledSuccess', {});
            } else {
                this.setState({IFSCCodeValueErrorMessage: error[0].errorMessage});
            }
        });
    }

    render() {
        return (
            <SafeAreaView style={styles.SafeArea}>
                <View style={styles.container}>
                    <DialogView ref={ref => (this.DialogViewRef = ref)}/>
                    {this.getInitialStateUi()}
                    {this.getLoadingView()}
                    {this.getBankLogo()}
                    {this.getDeclarationSection()}
                    {this.getSubText()}
                    {this.getSubmitButton()}
                </View>
            </SafeAreaView>
        );
    }
}

const
    styles = StyleSheet.create({
        container: {
            justifyContent: 'center',
            flex: 1,
            backgroundColor: colors.white,
            padding: spacing.spacing16,
        },
        crossIconStyle: {
            height: 16,
            width: 16,
            resizeMode: 'contain',
            alignSelf: 'flex-end',
            justifyContent: 'flex-end',
        },
        headingStyle: {
            marginTop: spacing.spacing40,
        },
        submitButtonStyle: {
            padding: spacing.spacing12,
            borderRadius: 2,
            height: 48,
            alignItems: 'center',
        },
        textInputContainer: {
            justifyContent: 'space-between',
            marginTop: spacing.spacing24,
        },
        nextButtonStyle: {
            borderRadius: 2,
            marginTop: spacing.spacing16,
            justifyContent: 'flex-end',
        },
        SafeArea: {
            flex: 1,
            backgroundColor: colors.white,
        },
        tncViewStyle: {
            flexDirection: 'row',
            marginTop: spacing.spacing8,
            marginBottom: spacing.spacing16,
        },
        checkboxStyle: {
            width: 18,
            height: 18,
            marginRight: spacing.spacing4,
        },
        tncTextStyle: {
            marginLeft: spacing.spacing4,
            textDecorationLine: 'underline',
            color: colors.blue028,
        },
        checkbox: {
            height: 16,
            width: 16,
        },
        bankLogoStyle: {
            height: 72,
            width: 72,
        },
    });
