import React, {Component} from "react";
import {
    BackHandler,
    Image,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import {TextStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import InputBox from "../../Components/InputBox";
import {API_ERROR} from "../buybackActions/Constants";
import ButtonWithLoader from "../../CommonComponents/ButtonWithLoader";
import {MOBILE_BUYBACK, PAYTM} from "../../Constants/WebengageAttributes";
import {BUYBACK_PAYMENT_DETAILS} from "../../Constants/WebengageEvents";
import CheckBox from "react-native-check-box";
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';
import {savePayment} from "../buybackActions/BuybackApiHelper";
import {buybackManagerObj} from '../../Navigation/index.buyback';
import {ABB, SERVED_BY} from '../../Constants/WebengageAttrKeys';
import {getServedByValueFromFirstItem} from '../BuyBackUtils/BuybackUtils';

export default class EnterPaytmMobileNumberComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            mobileNumberValue: "",
            submitEnabled: false,
            isLoading: false,
            checked: false,
        }
    }

    setLoadingState(isLoading) {
        this.setState({isLoading: isLoading})
    }

    onBackPress = () => {
        this.props. navigation.pop();
        return true;
    };

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }

    executeSaveQuestionsApi(saveUpiDetails) {
        savePayment(saveUpiDetails,(response, error) => {
            if(response)
            {
                    this.props.navigation.navigate("MobilePickupScheduledSuccess", {});
                    this.setLoadingState(false);
            }
            else
            {
                this.setLoadingState(false);
            }
        });
    }

    enableButton() {
        let mobileNoLength = this.state.mobileNumberValue.length;
        return mobileNoLength >= 10 && this.state.checked;
    }

    setButtonState() {
        this.setState({submitEnabled: this.enableButton()})
    }

    render() {
        return (
            <SafeAreaView style={styles.SafeArea}>
                <View style={styles.container}>
                    <TouchableOpacity onPress={() => {
                        this.props.navigation.pop();
                    }}
                    >
                        <Image source={require("../../images/icon_cross.webp")} style={styles.crossIconStyle}/>
                    </TouchableOpacity>
                    <Text style={[TextStyle.text_20_bold, styles.headingStyle]}>Enter paytm mobile number</Text>
                    <Text style={[TextStyle.text_12_normal, styles.descriptionStyle]}>In order to receive the full
                        amount,
                        make sure the mobile number is KYC verified by Paytm.</Text>
                    <InputBox
                        ref={input => {
                            this.textInput = input;
                        }}
                        containerStyle={styles.textInputContainer}
                        inputHeading='Mobile number'
                        multiline={false}
                        numberOfLines={1}
                        maxLength={10}
                        keyboardType={"numeric"}
                        autoFocus={true}
                        onChangeText={inputMessage => {
                            this.setState({
                                mobileNumberValue: inputMessage},() => {
                                this.setButtonState();
                            });
                        }
                        }
                        editable={true}>
                    </InputBox>
                    <View style={styles.tncViewStyle}>
                        <CheckBox
                            style={styles.checkboxStyle}
                            checkedImage={<Image source={require("../../images/ic_check_box.webp")} style={styles.checkbox}/>}
                            unCheckedImage={<Image source={require("../../images/ic_check_box_outline_blank.webp")} style={styles.checkbox}/>}
                            isChecked={this.state.checked}
                            onClick={() => {
                                this.setState(
                                {checked: !this.state.checked}, () => {
                                        this.setButtonState();
                                    })}
                            }
                        />
                        <Text style={[TextStyle.text_12_normal]}>I agree to all </Text>
                        <TouchableOpacity onPress={() => {
                            this.props.navigation.navigate("TermsAndConditions", {});
                        }}
                        >
                            <Text style={[TextStyle.text_12_normal, styles.tncTextStyle]}>Terms & Conditions</Text>
                        </TouchableOpacity>
                        <Text style={[TextStyle.text_12_normal]}> and </Text>
                        <TouchableOpacity onPress={() => {
                            this.props.navigation.navigate("BuybackDeclaration", {});
                        }}
                        >
                            <Text style={[TextStyle.text_12_normal, styles.tncTextStyle]}>Declaration</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.nextButtonStyle]}>
                        <ButtonWithLoader
                            isLoading={this.state.isLoading}
                            enable={this.state.submitEnabled}
                            Button={{
                                text: "Submit",
                                onClick: () => {
                                    let data = buybackManagerObj.getBuybackStatusDataFirstItem();
                                    let eventData = new Object();
                                    eventData["Location"] = MOBILE_BUYBACK;
                                    eventData["Type"] = PAYTM;
                                    eventData["QuoteID"] = data?.quoteId;
                                    eventData[ABB] = buybackManagerObj?.isABBFlow();
                                    eventData[SERVED_BY] = getServedByValueFromFirstItem();
                                    logWebEnageEvent(BUYBACK_PAYMENT_DETAILS, eventData);
                                    let saveUpiDetails = {
                                        "quoteId": data?.quoteId,
                                        "paymentMode": "PAYTM",
                                        "accountNumber": this.state.mobileNumberValue,
                                        "bankIFSCCode": "",
                                    };
                                    this.executeSaveQuestionsApi(saveUpiDetails);
                                    this.setLoadingState(true);
                                }
                            }
                            }/>
                    </View>
                    </View>
            </SafeAreaView>
        )
    }
}

const
    styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: colors.white,
            padding: spacing.spacing16,
        },
        crossIconStyle: {
            height: 16,
            width: 16,
            resizeMode: "contain",
            alignSelf: 'flex-end',
            justifyContent: "flex-end"
        },
        headingStyle: {
            marginTop: spacing.spacing40,
        },
        descriptionStyle: {
            marginTop: spacing.spacing8,
        },
        submitButtonStyle: {
            padding: spacing.spacing12,
            borderRadius: 2,
            height: 48,
            alignItems: 'center',
        },
        textInputContainer: {
            justifyContent: "space-between",
            marginTop: spacing.spacing24,
        },
        nextButtonStyle: {
            borderRadius: 2,
            marginTop: spacing.spacing16,
        },
        SafeArea: {
            flex: 1,
            backgroundColor: colors.white
        },
        tncViewStyle: {
            flexDirection: 'row',
            marginTop: spacing.spacing8,
        },
        checkboxStyle: {
            width: 18,
            height: 18,
            marginRight: spacing.spacing4,
        },
        tncTextStyle: {
            marginLeft: spacing.spacing4,
            textDecorationLine: 'underline',
            color: colors.blue028,
        },
        checkbox: {
            height: 16,
            width: 16
        }
    });
