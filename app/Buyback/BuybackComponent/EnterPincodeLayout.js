import React, {Component} from "react";
import {Image, StyleSheet, Text, TouchableOpacity, View, Platform} from "react-native";
import {ButtonStyle, CommonStyle} from "../../Constants/CommonStyle"
import colors from "../../Constants/colors"
import spacing from "../../Constants/Spacing";
import InputBox from "../../Components/InputBox";
import ButtonWithLoader from "../../CommonComponents/ButtonWithLoader";

export default class EnterPincodeLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputMessage: "",
            submitEnabled: false,
            isLoading: false
        };
    }

    setLoadingState(isLoading) {
        this.setState({isLoading: isLoading})
    }

    static getDerivedStateFromProps(props, state) {
        const newState = {};
        if (props.isError) {
            newState.isLoading = false
            //    show error here as well
        }
        else if (state.isLoading === true && props.pincodeServiceablityData !== null) newState.isLoading = false;
        return newState;
    }

    render() {
        let pincodeNotServiceableMessage = null;
        if (this.props.pincodeServiceablityData !== null) {
            pincodeNotServiceableMessage = this.props?.pincodeServiceablityData?.pincodeAvailable ? null : "Oops! OneAssist is currently not servicing in this pincode.";
            if (pincodeNotServiceableMessage){
                this.props.propsData.onClose();
                this.props.propsData.showPincodeErrorDialoge();
            }
        }
        let primaryButton = this.props.propsData.primaryButton ? <TouchableOpacity
            style={[styles.primaryButton, ButtonStyle.GreyButton, this.state.submitEnabled && ButtonStyle.BlueButton]}
            activeOpacity={.8}
            onPress={() => {
                this.props.propsData.checkPincode(this.state.inputMessage);
                if (this.props.propsData.primaryButton.onClick != null)
                    this.props.propsData.primaryButton.onClick();
                if (this.props.closePopup != null)
                    this.props.closePopup(true);
            }}>
            <Text style={ButtonStyle.primaryButtonText}>{this.props.propsData.primaryButton.text}</Text>
        </TouchableOpacity> : null;
        return (
            <View style={styles.container}>
                {/*<TouchableOpacity onPress={() => {*/}
                {/*    if (this.props.propsData.onClose != null)*/}
                {/*        this.props.propsData.onClose();*/}
                {/*    if (this.props.closePopup != null)*/}
                {/*        this.props.closePopup(true);*/}
                {/*}}*/}
                {/*>*/}
                {/*    <Image source={require("../images/icon_cross.webp")} style={CommonStyle.crossIconContainer}/>*/}
                {/*</TouchableOpacity>*/}
                <InputBox
                    ref={input => {
                        this.textInput = input;
                    }}
                    blurOnSubmit={false}
                    containerStyle={styles.textInputContainer}
                    inputHeading={"Pincode"}
                    multiline={false}
                    placeholder={"Enter 6 digit pincode"}
                    numberOfLines={1}
                    maxLength={6}
                    autofocus={true}
                    keyboardType={"numeric"}
                    onChangeText={inputMessage => {
                        this.setState({
                            inputMessage: inputMessage,
                            submitEnabled: inputMessage.length >= 6
                        })
                    }}
                    editable={true}
                    errorMessage={pincodeNotServiceableMessage}
                    onTouchStart={() => this.setState({selectedIndex: index})}>
                </InputBox>
                {/*{primaryButton}*/}
                <View style={styles.primaryButton}>
                    <ButtonWithLoader
                        isLoading={this.state.isLoading}
                        enable={this.state.submitEnabled}
                        Button={{
                            text: this.props.propsData.primaryButton.text,
                            onClick: () => {
                                if (Platform.OS === "ios") {
                                    this.props.propsData.checkPincode(this.state.inputMessage);
                                    this.setLoadingState(true);
                                }
                                else {
                                    if (Platform.Version >= 22 && Platform.Version<29) {
                                        this.props.checkIMEIPermission(onPermissionGrant = () => {
                                            this.props.propsData.checkPincode(this.state.inputMessage);
                                            this.setLoadingState(true);
                                        })
                                    }else{
                                        this.props.propsData.checkPincode(this.state.inputMessage);
                                        this.setLoadingState(true);
                                    }

                                }


                            }
                        }
                        }/>
                </View>

            </View>);
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        width: '100%',
        paddingBottom: 20
    },
    titleLayout: {
        textAlign: "center",
        marginTop: spacing.spacing8,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
    },
    descLayout: {
        textAlign: "center",
        marginTop: spacing.spacing12,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
    },
    primaryButton: {
        margin: spacing.spacing16,
    },
    primaryButtonText: {
        color: colors.white,
        textAlign: "center",
    },
    secondaryButton: {
        marginTop: spacing.spacing16,
    },
    textInputContainer: {
        justifyContent: "space-between",
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        marginTop: spacing.spacing32,
        marginBottom: spacing.spacing16
    },
});
