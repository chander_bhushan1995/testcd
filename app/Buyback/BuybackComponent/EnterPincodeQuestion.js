import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {TextStyle} from '../../Constants/CommonStyle';
import spacing from '../../Constants/Spacing';
import colors from '../../Constants/colors';
import ButtonWithLoader from '../../CommonComponents/ButtonWithLoader';
import DialogView from '../../Components/DialogView';
import InputBox from '../../Components/InputBox';
import {buybackFlowStrings} from '../../Constants/BuybackConstants';
import {checkPincodeServiceability, updatePincodeApi} from '../buybackActions/BuybackApiHelper';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import {APIData} from '../../../index';
import images from '../../images/index.image';
import {TECHNICAL_ERROR_MESSAGE} from '../../Constants/ActionTypes';

let _this = null
export default class EnterPincodeQuestion extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            submitEnabled: false,
            answerValue: '',
            errorMessage: '',
            buttonText: buybackFlowStrings.check_service_availability,
            rightImage: '',
        };
        _this = this
    }

    setButtonState() {
        this.setState({submitEnabled: this.state.answerValue !== '' && this.state.answerValue.length === 6});
    }

    callUpdatePincodeApi() {
        let data = buybackManagerObj.getBuybackStatusDataFirstItem();
        updatePincodeApi(data?.quoteId, APIData.customer_id, _this.state.answerValue,_this.updatePincodeAPICallback);
    }

    updatePincodeAPICallback(response, error) {
        _this.setState({isLoading: false});
        if (response) {
            _this.props.onClick(buybackManagerObj.getBuybackStatusData(), _this.props.index, _this.props.index, 'ENTER_PINCODE');
        } else {
            alert(error[0]?.errorMessage ?? TECHNICAL_ERROR_MESSAGE)
        }
    }

    callCheckPincodeApi() {
        let data = buybackManagerObj.getBuybackStatusDataFirstItem();
        checkPincodeServiceability(data?.quoteId, _this.state.answerValue, (response, error) => {
            _this.setState({isLoading: true});
            if (response) {
                if(response.data.available){
                    _this.setState({buttonText: 'Next', rightImage: images.green_right, isLoading: false, errorMessage: ''});
                }else{
                    this.setState({errorMessage: 'Pincode is not serviceable', isLoading: false});
                }
            } else {
                this.setState({errorMessage: 'Pincode is not serviceable', isLoading: false});
            }
        });
    }

    getSuccessText() {
        return this.state.buttonText !== buybackFlowStrings.check_service_availability ?
            <Text style={[TextStyle.text_12_normal, {color: colors.color_45B448, marginLeft: spacing.spacing36}]}>Great!
                We’re servicing in this pincode</Text> : null;
    }

    render() {
        return <View style={styles.container}>
            <DialogView ref={ref => (this.DialogViewRef = ref)}/>
            <View style={{flexDirection: 'row'}}>
                <Text style={[TextStyle.text_14_bold, styles.questionNumberStyle]}>{this.props.index + 1}</Text>
                <Text style={[TextStyle.text_16_bold, styles.questionTextStyle]}>Enter your pincode</Text>
            </View>
            <Text style={[TextStyle.text_12_normal, styles.questionSubTextStyle]}>Pincode is needed to check
                serviceability</Text>
            <InputBox
                ref={input => {
                    this.textInput = input;
                }}
                height={40}
                containerStyle={{justifyContent: 'space-between', marginLeft: spacing.spacing40}}
                inputHeading={'Enter Pincode'}
                keyboardType={'numeric'}
                multiline={false}
                autoFocus={true}
                errorMessage={this.state.errorMessage}
                maxLength={6}
                onChangeText={inputMessage => {
                    this.setState({answerValue: inputMessage}, () => {
                        this.setButtonState();
                    });
                }
                }
                editable={true}>
            </InputBox>
            {this.getSuccessText()}
            <View style={{marginTop: 24, marginLeft: spacing.spacing36}}>
                <ButtonWithLoader
                    isLoading={this.state.isLoading}
                    enable={this.state.submitEnabled}
                    Button={{
                        text: this.state.buttonText,
                        onClick: () => {
                            this.setState({isLoading: true});
                            if (this.state.buttonText === buybackFlowStrings.check_service_availability) {
                                this.callCheckPincodeApi();
                            } else {
                                this.callUpdatePincodeApi();
                            }
                        },
                    }}/>
            </View>
        </View>;
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        marginBottom: spacing.spacing12,
        paddingBottom: 60,
    },
    questionNumberStyle: {
        width: spacing.spacing24,
        height: spacing.spacing24,
        borderRadius: 24 / 2,
        backgroundColor: colors.blue,
        color: colors.white,
        textAlign: 'center',
        overflow: 'hidden',
    },
    questionTextStyle: {
        flex: 1,
        marginLeft: spacing.spacing16,
    },
    questionSubTextStyle: {
        marginLeft: spacing.spacing40,
        marginBottom: spacing.spacing8,
        marginTop: spacing.spacing8,
    },
    imageUploadView: {
        borderRadius: 4,
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: colors.greye0,
        marginLeft: spacing.spacing36,
        marginTop: spacing.spacing24,
    },
    uploadImageIcon: {
        height: spacing.spacing64,
        marginTop: spacing.spacing24,
        width: spacing.spacing64,
    },
    fileIcon: {
        height: spacing.spacing36,
        marginTop: spacing.spacing24,
        width: spacing.spacing28,
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    uploadImage1: {
        height: spacing.spacing150,
        width: spacing.spacing300,
        resizeMode: 'cover',

    },
    uploadText: {
        textAlign: 'center',
        marginTop: spacing.spacing20,
        color: colors.blue,
        marginBottom: spacing.spacing20,
    },
    fileUploadText: {
        textAlign: 'center',
        marginTop: spacing.spacing12,
        color: colors.charcoalGrey,
    },
    fileSizeText: {
        textAlign: 'center',
    },
    fileUploadedSuccessfullText: {
        textAlign: 'center',
        marginTop: spacing.spacing12,
        color: colors.seaGreen,
        marginBottom: spacing.spacing8,
    },
});
