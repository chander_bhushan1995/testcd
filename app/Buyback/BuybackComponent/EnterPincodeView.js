import React, {Component} from "react";
import {Modal, View, TouchableWithoutFeedback, StyleSheet} from 'react-native'
import EnterPincodeLayout from "./EnterPincodeLayout";
import spacing from "../../Constants/Spacing";

export default class EnterPincodeView extends Component {

    state = {
        visible: false,
        title: "",
        message: "",
        primaryButton: "",
        secondaryButton: "",
        cancelable: null,
        onClose: "",
        errorMessage: "",
        error: "",
        checkPincode: null,
        checkIMEIPermission:null,
    }

    constructor(props) {
        super(props);
    }

    showDailog(parms) {
        this.setState(
            {
                visible: true,
                title: parms.title,
                message: parms.message,
                primaryButton: parms.primaryButton,
                secondaryButton: parms.secondaryButton,
                cancelable: parms.cancelable,
                onClose: parms.onClose,
                checkPincode: this.props.checkPincode,
            }
        )
    }
    static getDerivedStateFromProps(props, state) {
        const newState = {};
        if (state.visible === true && props.pincodeServiceablityData !== null && props.pincodeServiceablityData.pincodeAvailable) newState.visible = false;
        return newState;
    }
    closePopup(isClosePopup) {
        if (isClosePopup == null || isClosePopup)
            this.setState({visible: false})
    }

    _renderOutsideTouchable(onTouch) {
        const view = <View style={{flex: 1, width: '100%'}}/>

        if (!onTouch) return view;

        return (
            <TouchableWithoutFeedback onPress={onTouch} style={{flex: 1, width: '100%'}}>
                {view}
            </TouchableWithoutFeedback>
        )
    }

    renderView() {
        return <View style={styles.container}>
            <EnterPincodeLayout style={styles.childContainer}
                                pincodeServiceablityData={this.props.pincodeServiceablityData}
                                propsData={this.state}
                                checkIMEIPermission={this.props.checkIMEIPermission}
                                closePopup={(isClosePopup) => this.closePopup(isClosePopup)}
            />
        </View>
    }

    render() {
        return (<Modal
                transparent={true}
                visible={this.state.visible}
                onRequestClose={() => {
                    if (this.props.onClose != null)
                        this.props.onClose();
                    this.closePopup(this.state.cancelable)

                }}>
                <View style={[{
                    flex: 1,
                    backgroundColor: "#000000AA",
                    padding: spacing.spacing24
                }]}>
                    {this._renderOutsideTouchable(() => {
                        if (this.props.onClose != null)
                            this.props.onClose();
                        this.closePopup(this.state.cancelable)
                    })}
                    {this.renderView()}
                    {this._renderOutsideTouchable(() => {
                        this.closePopup(this.state.cancelable)
                        if (this.props.onClose != null)
                            this.props.onClose();
                    })}

                </View>
            </Modal>

        );
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        opacity: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    childContainer: {}
});