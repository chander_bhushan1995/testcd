import React, {Component} from 'react';
import {BackHandler, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {TextStyle} from '../../Constants/CommonStyle';
import spacing from '../../Constants/Spacing';
import colors from '../../Constants/colors';
import InputBox from '../../Components/InputBox';
import {API_ERROR} from '../buybackActions/Constants';
import ButtonWithLoader from '../../CommonComponents/ButtonWithLoader';
import {MOBILE_BUYBACK, UPI} from '../../Constants/WebengageAttributes';
import {BUYBACK_PAYMENT_DETAILS} from '../../Constants/WebengageEvents';
import CheckBox from 'react-native-check-box';
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';
import {contactCreationApi, getAccountStatusApi, savePayment} from '../buybackActions/BuybackApiHelper';
import DialogView from '../../Components/DialogView';
import {IDFenceErrorApiErrorMsg} from '../../Constants/ApiError.message';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import {ABB, SERVED_BY} from '../../Constants/WebengageAttrKeys';
import {getServedByValueFromFirstItem} from '../BuyBackUtils/BuybackUtils';

let timeoutValue = 100;
let timeoutValueInterval = 3000;
export default class EnterUPIComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            upiValue: '',
            submitEnabled: false,
            isLoading: false,
            checked: false,
            username: '',
            errorMsg: '',
            rightImage: '',
        };
    }

    setLoadingState(isLoading) {
        this.setState({isLoading: isLoading});
    }

    onBackPress = () => {
        this.props.navigation.pop();
        return true;
    };

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }

    saveUpiDetails(saveUpiDetails, dispatch) {
        savePayment(saveUpiDetails, (response, error) => {
            if (response) {
                this.props.navigation.navigate('MobilePickupScheduledSuccess', {});
                this.setLoadingState(false);
            } else {
                this.setLoadingState(false);
                dispatch({type: API_ERROR, data: error});
            }
        });
    }

    callGetAccountStatusApi(fundId) {
        setTimeout(() => {
            try {
                getAccountStatusApi(fundId, (response, error) => {
                    if (response) {
                        if (response?.data?.status === 'active') {
                            this.setState({username: response?.data?.name, errorMsg: '', rightImage: require("../../images/green_right.webp")});
                            this.callPaymentDetailApi();
                        } else if (timeoutValue <= 30000) {
                            timeoutValue = timeoutValue + timeoutValueInterval;
                            this.callGetAccountStatusApi(fundId);
                        } else {
                            this.setState({errorMsg: 'This BHIM UPI ID doesn’t exist', isLoading: false});
                        }
                    } else {
                        this.DialogViewRef?.showDailog({
                            title: 'Technical Error',
                            message: IDFenceErrorApiErrorMsg.DEFAULT,
                            primaryButton: {
                                text: 'Okay, got it', onClick: () => {
                                    this.setLoadingState(false);
                                },
                            },
                            cancelable: false,
                            onClose: () => {
                                this.setLoadingState(false);
                            },
                        });
                    }
                });
            } catch (e) {
            }
        }, timeoutValueInterval);
    }

    callContactCreationApi() {
        this.setLoadingState(true);
        contactCreationApi({upiAddress: this.state.upiValue, accountType: 'UPI'}, (response, error) => {
            if (response) {
                this.callGetAccountStatusApi(response?.data?.fundId);
            } else {
                this.DialogViewRef?.showDailog({
                    title: 'Technical Error',
                    message: IDFenceErrorApiErrorMsg.DEFAULT,
                    primaryButton: {
                        text: 'Okay, got it', onClick: () => {
                            this.setLoadingState(false);
                        },
                    },
                    cancelable: false,
                    onClose: () => {
                        this.setLoadingState(false);
                    },
                });
            }
        });
    }

    callPaymentDetailApi() {
        let data = buybackManagerObj.getBuybackStatusDataFirstItem();
        let eventData = new Object();
        eventData['Location'] = MOBILE_BUYBACK;
        eventData['Type'] = UPI;
        eventData['QuoteID'] = data?.quoteId;
        eventData[ABB] = buybackManagerObj?.isABBFlow();
        eventData[SERVED_BY] = getServedByValueFromFirstItem();
        logWebEnageEvent(BUYBACK_PAYMENT_DETAILS, eventData);
        let saveUpiDetails = {
            'quoteId': data?.quoteId,
            'paymentMode': UPI,
            'accountNumber': this.state.upiValue,
            'bankIFSCCode': '',
        };
        this.saveUpiDetails(saveUpiDetails);
    }

    submitClick() {
        this.callContactCreationApi();
    }

    setButtonState() {
        this.setState({submitEnabled: this.state.checked && this.state.upiValue});
    }

    getRegisteredNameView = () => {
        return this.state.username ? <View style={{marginTop: spacing.spacing20}}>
            <Text style={[TextStyle.text_12_normal, {color: colors.color_212121}]}>Registered Name</Text>
            <Text style={[TextStyle.text_14_bold, {
                marginTop: spacing.spacing4,
                marginBottom: spacing.spacing20,
            }]}>{this.state.username}</Text>
        </View> : null;
    };

    render() {
        return (
            <SafeAreaView style={styles.SafeArea}>
                <DialogView ref={ref => (this.DialogViewRef = ref)}/>
                <View style={styles.container}>
                    <TouchableOpacity onPress={() => {
                        this.props.navigation.pop();
                    }}
                    >
                        <Image source={require('../../images/icon_cross.webp')} style={styles.crossIconStyle}/>
                    </TouchableOpacity>
                    <Text style={[TextStyle.text_20_bold, styles.headingStyle]}>Enter your BHIM UPI ID</Text>
                    <Text style={[TextStyle.text_12_normal, styles.descriptionStyle]}>You need to verify the BHIM UPI
                        ID. The name of the contact is returned on successful verification of the BHIM UPI ID.</Text>
                    <InputBox
                        ref={input => {
                            this.textInput = input;
                        }}
                        containerStyle={styles.textInputContainer}
                        inputHeading='Enter BHIM UPI ID'
                        multiline={false}
                        numberOfLines={1}
                        autoFocus={true}
                        errorMessage={this.state.errorMsg}
                        onChangeText={inputMessage => {
                            this.setState({
                                upiValue: inputMessage,
                            }, () => {
                                this.setButtonState();
                            });
                        }
                        }
                        editable={true}
                        rightImage={this.state.rightImage}>
                    </InputBox>
                    {this.getRegisteredNameView()}
                    <View style={styles.tncViewStyle}>
                        <CheckBox
                            style={styles.checkboxStyle}
                            checkedImage={<Image source={require('../../images/ic_check_box.webp')}
                                                 style={styles.checkbox}/>}
                            unCheckedImage={<Image source={require('../../images/ic_check_box_outline_blank.webp')}
                                                   style={styles.checkbox}/>}
                            isChecked={this.state.checked}
                            onClick={() => {
                                this.setState(
                                    {checked: !this.state.checked}, () => {
                                        this.setButtonState();
                                    });
                            }
                            }
                        />
                        <Text style={[TextStyle.text_12_normal]}>I agree to all </Text>
                        <TouchableOpacity onPress={() => {
                            this.props.navigation.navigate('TermsAndConditions', {});
                        }}
                        >
                            <Text style={[TextStyle.text_12_normal, styles.tncTextStyle]}>Terms & Conditions</Text>
                        </TouchableOpacity>
                        <Text style={[TextStyle.text_12_normal]}> and </Text>
                        <TouchableOpacity onPress={() => {
                            this.props.navigation.navigate('BuybackDeclaration', {});
                        }}
                        >
                            <Text style={[TextStyle.text_12_normal, styles.tncTextStyle]}>Declaration</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.nextButtonStyle]}>
                        <ButtonWithLoader
                            isLoading={this.state.isLoading}
                            enable={this.state.submitEnabled}
                            Button={{
                                text: 'Submit',
                                onClick: () => this.submitClick(),
                            }}/>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const
    styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: colors.white,
            padding: spacing.spacing16,
        },
        crossIconStyle: {
            height: 16,
            width: 16,
            resizeMode: 'contain',
            alignSelf: 'flex-end',
            justifyContent: 'flex-end',
        },
        headingStyle: {
            marginTop: spacing.spacing40,
        },
        descriptionStyle: {
            marginTop: spacing.spacing8,
        },
        submitButtonStyle: {
            padding: spacing.spacing12,
            borderRadius: 2,
            height: 48,
            alignItems: 'center',
        },
        textInputContainer: {
            justifyContent: 'space-between',
            marginTop: spacing.spacing24,
        },
        nextButtonStyle: {
            borderRadius: 2,
            marginTop: spacing.spacing16,
        },
        SafeArea: {
            flex: 1,
            backgroundColor: colors.white,
        },
        tncViewStyle: {
            flexDirection: 'row',
            marginTop: spacing.spacing20,
        },
        checkboxStyle: {
            width: 18,
            height: 18,
            marginRight: spacing.spacing4,
        },
        tncTextStyle: {
            marginLeft: spacing.spacing4,
            textDecorationLine: 'underline',
            color: colors.blue028,
        },
        checkbox: {
            height: 16,
            width: 16,
        },
    });
