import React, {Component} from 'react';
import {
    BackHandler,
    Image, NativeModules,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import {ButtonStyle, TextStyle} from '../../Constants/CommonStyle';
import spacing from '../../Constants/Spacing';
import colors from '../../Constants/colors';
import {StackActions} from 'react-navigation';
import ApiErrorComponent from '../../CommonComponents/ApiErrorComponent';
import ApiCallingComponent from '../../CommonComponents/ApiCallingComponent';
import {MOBILE_BUYBACK} from '../../Constants/WebengageAttributes';
import {SCHEDULE_PICKUP, APPLY_PROMO, BUYBACK_COUPON_SCREEN} from '../../Constants/WebengageEvents';
import NumberUtils from '../../commonUtil/NumberUtils';
import CouponCodeComponent from './CouponCodeComponent';
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import {APIData} from '../../../index';
import ABBNotEligible from './ABBNotEligible';
import {BuyBackStatus} from '../../AppForAll/HomeScreen/Reducers/HomeScreenReducer';
import {BuybackEventLocation, buybackFlowType, buybackPartnerType} from '../../Constants/BuybackConstants';
import {decideRetailersFlow} from '../BuyBackUtils/RetailersNavigationDecider';
import DialogView from '../../Components/DialogView';
import {
    getResalePriceText,
    getSchedulePickupButtonText,
    getServedByValueFromFirstItem,
    isThirdPartyFlow, sendNearbyStoresWebengageEvent,
} from '../BuyBackUtils/BuybackUtils';
import {ABB, SERVED_BY} from '../../Constants/WebengageAttrKeys';
import {cancelOnGoingRequest} from '../buybackActions/BuybackApiHelper';
import dimens from '../../Constants/Dimens';
import images from '../../images/index.image';
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

let buybackStatus;
const nativeBridgeRef = NativeModules.ChatBridge;
export default class FinalPriceSuccessComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            quoteId: '',
            isLoading: false,
        };
        this.openBuybackQuestionList = this.openBuybackQuestionList.bind(this);
        this.navigateToRetailersFlow = this.navigateToRetailersFlow.bind(this);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.backHandler);
    }

    /**
     * here we get response from retailers api
     * @param arrayOfRetailers
     */
    navigateToRetailersFlow(arrayOfRetailers, error) {
        this.setState({isLoading: false});
        if (error) {
            this.DialogViewRef.showDailog({
                title: 'ALERT',
                message: error,
                primaryButton: {
                    text: 'Retry', onClick: () => {
                        decideRetailersFlow(this.navigateToRetailersFlow);
                    },
                },
                secondaryButton: {
                    text: 'Cancel', onClick: () => {
                    },
                },
                cancelable: false,
                onClose: () => {
                },
            });
        } else {
            sendNearbyStoresWebengageEvent(arrayOfRetailers?.length);
            if (arrayOfRetailers?.length > 1) {
                this.props.navigation.navigate('NearByRetailers', {params: {data: arrayOfRetailers}});
            } else if(arrayOfRetailers?.length === 1){
                this.props.navigation.navigate('RetailerDetailScreen', {detailData: arrayOfRetailers[0] ?? {}});
            }else{
                this.props.navigation.navigate('RetailersEmptyScreen', {questionsData: this.props.stepsList[3].questionData});
            }
        }
    }

    onBackPress() {
        if (this.props.price != null) {
            this.props.navigation.dispatch(StackActions.popToTop());
        } else {
            let data = deepCopy(this.props.stepsList[2].questionData);
            for (let i = 0; i < data.length; i++) {
                data[i].isCompleted = true;
            }
            this.props.setQuoteQuestions(data);
            this.props.navigation.pop();
        }
    }

    componentDidMount() {
        this.props.resetData();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPress();
        });
        let data = buybackManagerObj.getBuybackStatusDataFirstItem();
        this.props.getPrice(data?.quoteId);
        buybackStatus = data?.buyBackStatus;
        this.setState({quoteId: data?.quoteId});
    }

    openBuybackQuestionList() {
        let data = this.props.stepsList[3].questionData;
        this.props.setOrderQuestions(data);
        let buybackData = buybackManagerObj.getBuybackStatusDataFirstItem();
        let subType = buybackData.subType;
        if (subType === buybackFlowType.RETAILER_PICKUP) {
            this.setState({isLoading: true});
            decideRetailersFlow(this.navigateToRetailersFlow);
            return;
        }
        let eventDataSchedulePickup = {};
        eventDataSchedulePickup['Location'] = BuybackEventLocation.FULL_SCREEN;
        eventDataSchedulePickup['QuoteID'] = this.state.quoteId;
        eventDataSchedulePickup['Stage'] = buybackStatus;
        eventDataSchedulePickup[ABB] = buybackManagerObj?.isABBFlow();
        eventDataSchedulePickup[SERVED_BY] = getServedByValueFromFirstItem();
        logWebEnageEvent(SCHEDULE_PICKUP, eventDataSchedulePickup);
        this.props.navigation.navigate('BuybackQuestions', {
            params: {
                currentQuestionType: 'ORDER_QUESTION',
                questionData: this.props.stepsList[3].questionData,
            },
        });
    }

    /**
     * method to start schedule pickup or exchange flow after checking price value
     */
    onClick() {
        if (this.props.price !== null) {
            this.openBuybackQuestionList();
        } else {
            // // let data = this.props.stepsList[2].questionData;
            // let data = deepCopy(this.props.stepsList[2].questionData);
            // for (let i = 0; i < data.length; i++) {
            //     data[i].isCompleted = true;
            // }
            // // this.props.setQuoteQuestions(data);
            // this.props.setQuoteQuestions(data);
            this.props.navigation.pop();
        }
    }

    callExtractPriceApi() {
        this.props.getPrice(this.state.quoteId);
    }

    render() {
        return this.getView();
    }

    restartBuybackFlow(isABBFlowFail) {
        let data = buybackManagerObj.getBuybackStatusDataFirstItem();
        data.buyBackStatus = BuyBackStatus.BUYBACK_NOT_STARTED;
        data.quoteId = '';
        data.pincode = '';
        data.cityName = '';
        data.mhcScore = '';
        buybackManagerObj.setIsABBFlowFailOnce(isABBFlowFail);
        buybackManagerObj.setBuybackStatusDataFirstItem(data);
        this.props.navigation.popToTop();
    }

    getTitleText() {
        let buybackData = buybackManagerObj.getBuybackStatusDataFirstItem();
        let subType = buybackData.subType;
        if (subType === buybackFlowType.RETAILER_PICKUP) {
            return 'Great! Exchange price is';
        }
        return 'Great! Resale price is';
    }

    getDescriptionText() {
        let buybackData = buybackManagerObj.getBuybackStatusDataFirstItem();
        let subType = buybackData.subType;
        if (subType === buybackFlowType.RETAILER_PICKUP) {
            return 'Exchange your phone for a new one at a nearby store & enjoy great savings.';
        }
        return 'Price is subject to change upon inspection by the courier agent\n' + 'at the time of pickup.';
    }

    getNonXiaomiContent() {
        let couponTitle = null;
        if (isThirdPartyFlow()) {
            couponTitle = <Text
                style={[
                    TextStyle.text_12_normal,
                    styles.couponDescriptionTextStyle,
                ]}
            >
                Apply code to add more to the final price.
            </Text>;
        }
        let couponCode;
        if (this.props.couponCodeInfo !== null && this.props.couponCodeInfo !== undefined) {
            couponCode = this.props.couponCodeInfo.couponCode;
        }
        return <View
            style={{
                justifyContent: 'center',
                alignItems: 'center',
                flex: 1,
            }}
        >
            <Image
                source={require('../../images/final_price_success.webp')}
                style={styles.finalPriceImageStyle}
            />
            <Text style={[TextStyle.text_14_bold, styles.headingStyle]}>{this.getTitleText()}</Text>
            <Text style={[TextStyle.text_24_bold, styles.priceTextStyle]}>
                {'\u20B9 ' + NumberUtils.getFormattedPrice(this.props.price)}
            </Text>
            {couponTitle}
            <CouponCodeComponent
                style={styles.couponCodeComponent}
                couponCode={couponCode}
                apply={() => {
                    let eventData = new Object();
                    eventData['Location'] = BUYBACK_COUPON_SCREEN;
                    eventData['Coupon Code'] = couponCode;
                    logWebEnageEvent(APPLY_PROMO, eventData);
                    this.props.applyCoupon(this.state.quoteId, couponCode);
                }}
                isApplying={this.props.isCouponApplying}
                isApplied={this.props.isCouponApplied}
                isError={this.props.isCouponError}
                errorMessage={this.props.errorMessage}
            />
            <Text style={[TextStyle.text_16_bold, styles.descriptionTextStyle]}>{this.getDescriptionText()}</Text>
            <TouchableOpacity
                style={[styles.primaryButton, ButtonStyle.BlueButton]}
                onPress={() => {
                    this.openBuybackQuestionList();
                }}
            >
                <Text style={ButtonStyle.primaryButtonText}>
                    {getSchedulePickupButtonText()}
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={[styles.secondaryButton]}
                onPress={() => {
                    this.props.navigation.dispatch(StackActions.popToTop());
                }}
            >
                <Text style={ButtonStyle.TextOnlyButton}>I’ll do it later</Text>
            </TouchableOpacity>
        </View>;
    }

    getXiaomiContent() {
        let buybackData = buybackManagerObj.getBuybackStatusDataFirstItem();
        return <View style={{flex: 1}}>
            <View style={styles.xiaomiPriceContainer}>
                <Image source={require('../../images/green_right.webp')} style={styles.imgRightTick}/>
                <Text style={[TextStyle.text_16_bold, {marginLeft: spacing.spacing12, color: colors.color_45B448}]}>{getResalePriceText()}</Text>
                <View style={{flex: 1}}>
                    <Text style={[TextStyle.text_14_bold, {color: colors.color_45B448, alignSelf: 'flex-end'}]}>{'Upto ' + '\u20B9' + NumberUtils.getFormattedPrice(buybackData?.price)}</Text>
                </View>
            </View>
            <View style={styles.xiaomiDataContainer}>
                <View style={styles.verticalLineView}/>
                <Text style={[TextStyle.text_14_normal_charcoalGrey, {marginLeft: spacing.spacing20,
                    marginTop: spacing.spacing12}]}>Price may vary upon inspection at the service centre</Text>
            </View>
            <View style={styles.cardContainer}>
                <View style={{flexDirection: "row", marginRight: spacing.spacing20,marginTop: spacing.spacing20}}>
                    <Image source={images.ongoing_timeline_icon}
                           style={[styles.imgRightTick, {marginLeft: spacing.spacing12}]}/>
                    <Text style={[TextStyle.text_16_bold, {color: colors.color_404040, marginLeft: spacing.spacing12}]}>Submit your Phone</Text>
                </View>
                <Text style={[TextStyle.text_14_normal_charcoalGrey, {marginTop: spacing.spacing8, marginLeft: spacing.spacing44}]}>
                    Submit your phone at the nearest Mi service centre. You will get a voucher of the resale value which can be redeemed while purchasing new phone.
                </Text>
                <View style={styles.xiaomiPrimaryButton}>
                    <TouchableOpacity
                        style={ButtonStyle.BlueButton}
                        onPress={() => {
                            this.setState({isLoading: true});
                            decideRetailersFlow(this.navigateToRetailersFlow);
                        }}
                    >
                        <Text style={ButtonStyle.primaryButtonText}>
                            View Nearby Mi Service Centre
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>;
    }

    getDetailContent() {
        let buybackData = buybackManagerObj.getBuybackStatusDataFirstItem();
        if (buybackData?.flow === buybackPartnerType.PARTNER_XIAOMI && buybackData?.subType !== buybackFlowType.BOTH) {
            return this.getXiaomiContent();
        } else {
            return this.getNonXiaomiContent();
        }
    }

    getView() {
        if (this.props.isError) {
            if (this.props.errorType === '1024') {
                return <ABBNotEligible
                    heading1="Sell your phone anyway"
                    heading2="Your phone is not eligible for assured value due to its condition. You can still sell it at a very good price."
                    onClose={() => {
                        nativeBridgeRef.goBack();
                    }}
                    buttons={{
                        primaryButton: {
                            text: 'Check resale price',
                            onClick: () => {
                                this.restartBuybackFlow(true);
                            },
                        },
                        secondaryButton: {
                            text: 'Try assured value again',
                            onClick: () => {
                                this.restartBuybackFlow(false);
                            },
                        },

                    }}
                />;
            }
            return (
                <ApiErrorComponent
                    isLoading={this.props.isLoading}
                    errorType={this.props.errorType}
                    onClose={() => {
                        this.onClick();
                    }}
                    onTryAgainClick={() => {
                        if (this.props.price === null) {
                            this.callExtractPriceApi();
                        } else {
                            this.props.applyCoupon(this.state.quoteId, this.props.couponCodeInfo.couponCode);
                        }
                    }}
                />
            );
        } else if (this.props.ABBNotEligible) {
            let buybackData = buybackManagerObj.getBuybackStatusDataFirstItem();
            return <ABBNotEligible
                heading1="Sell your phone anyway"
                heading2={'Your phone is not eligible for assured value due to its condition. You can still sell your phone for upto Rs.' + NumberUtils.getFormattedPrice(buybackData?.price)}
                onClose={() => {
                    nativeBridgeRef.goBack();
                }}
                buttons={{
                    primaryButton: {
                        text: 'Continue',
                        onClick: () => {
                            this.props.resetNotEligibleCondition();
                            if (this.props.subType === buybackFlowType.BOTH) {
                                this.props.navigation.pop();
                                this.props.navigation.navigate('BuySellSelection', {
                                    questionData: this.props.stepsList[3].questionData,
                                    currentQuestionType: 'ORDER_QUESTION',
                                });
                            }
                        },
                    },
                    secondaryButton: {
                        text: 'Cancel Request',
                        onClick: () => {
                            this.setState({isLoading: true});
                            cancelOnGoingRequest(this.state.quoteId, APIData.customer_id, (response, error) => {
                                this.setState({isLoading: false});
                                if (response) {
                                    nativeBridgeRef.goBack();
                                } else {

                                }
                            });
                        },
                    },

                }}
            />;
        } else if (this.props.isLoading) {
            return (
                <ApiCallingComponent
                    onClose={() => {
                        this.onClick();
                    }}
                />
            );
        } else if (this.props.subType === buybackFlowType.BOTH && !this.props.ABBNotEligible) {
            this.props.navigation.pop();
            this.props.navigation.navigate('BuySellSelection', {
                questionData: this.props.stepsList[3].questionData,
                currentQuestionType: 'ORDER_QUESTION',
            });
            return null;
        }
        else {
            return (
                <SafeAreaView style={styles.SafeArea}>
                    <View style={styles.container}>
                        <DialogView ref={ref => (this.DialogViewRef = ref)}/>
                        <TouchableOpacity
                            onPress={() => {
                                this.props.navigation.dispatch(StackActions.popToTop());
                            }}
                        >
                            <Image
                                source={require('../../images/icon_cross.webp')}
                                style={styles.crossIconStyle}
                            />
                        </TouchableOpacity>
                        {this.getDetailContent()}
                    </View>
                </SafeAreaView>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
        padding: spacing.spacing16,
    },
    crossIconStyle: {
        height: 16,
        width: 16,
        resizeMode: 'contain',
        alignSelf: 'flex-end',
    },
    finalPriceImageStyle: {
        height: 50,
        width: 50,
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    headingStyle: {
        marginTop: spacing.spacing24,
        color: colors.seaGreen,
    },
    priceTextStyle: {
        marginTop: spacing.spacing8,
        paddingTop: 8,
        color: colors.seaGreen,
    },
    couponDescriptionTextStyle: {
        marginTop: spacing.spacing24,
    },
    descriptionTextStyle: {
        marginTop: spacing.spacing64,
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing12,
        textAlign: 'center',
    },
    primaryButton: {
        marginTop: spacing.spacing40,
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing12,
        width: '100%',
    },
    secondaryButton: {
        marginTop: spacing.spacing24,
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing12,
    },
    SafeArea: {
        flex: 1,
        backgroundColor: colors.white,
    },
    couponCodeComponent: {
        width: '90%',
        marginLeft: spacing.spacing48,
        marginRight: spacing.spacing48,
        marginTop: spacing.spacing12,
    },
    xiaomiPriceContainer: {
        flexDirection: 'row',
        marginTop: spacing.spacing56,
        marginRight: spacing.spacing24,
        marginLeft: spacing.spacing16,
    },
    imgRightTick: {
        resizeMode: 'contain',
        width: dimens.dimen20,
        height: dimens.dimen20,
    },
    xiaomiDataContainer: {
        flexDirection: "row",
        marginRight: spacing.spacing20,
        marginLeft: spacing.spacing24,
        paddingBottom: 80,
    },
    verticalLineView: {
        backgroundColor: colors.color_E0E0E0,
        marginBottom: -80,
        width: spacing.spacing1,
    },
    cardContainer: {
        marginHorizontal: -16,
        paddingHorizontal: spacing.spacing20,
        borderColor: 'transparent',
        backgroundColor: colors.white,
        elevation: 4,
        shadowOpacity: 0.2,
        shadowOffset: {
            width: 1,
            height: 1,
        },
        shadowColor: '#000000',
    },
    xiaomiPrimaryButton: {
        marginTop: spacing.spacing24,
        marginLeft: spacing.spacing40,
        marginRight: spacing.spacing12,
        marginBottom: spacing.spacing24,
    },
});
