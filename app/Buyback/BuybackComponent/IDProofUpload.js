import React, {Component} from 'react';
import {Image, NativeModules, Platform, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {TextStyle} from '../../Constants/CommonStyle';
import colors from '../../Constants/colors';
import spacing from '../../Constants/Spacing';
import DocumentUploadUtils from '../../Components/DocumentUploadUtils';
import DialogView from '../../Components/DialogView';
import ButtonWithLoader from '../../CommonComponents/ButtonWithLoader';
import {DOCUMENT_UPLOAD} from '../../Constants/ApiUrls';
import futch from '../../network/futch';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import {buybackPartnerType} from '../../Constants/BuybackConstants';
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

const chatBrigeRef = NativeModules.ChatBridge;
const UPLOAD_NOT_STARTED = 'upload_not_started';
const UPLOAD_DONE = 'upload_done';

export default class IDProofUpload extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            submitEnabled: false,
            idProofUri: '',
            idProofType: '',
            idProofName: 'ID Proof',
            fileUploadStatus: UPLOAD_NOT_STARTED,
            docFileInfo: {type: 'image'},
        };
    }

    getImageUploadViewOrImage() {
        let view;
        if (this.state.idProofUri === '') {
            view = <Image style={styles.uploadImageIcon}
                                source={require('../../images/icon_camera.webp')}/>;
        } else if (this.state.docFileInfo.type === 'document') {
            view = <View><Image style={styles.fileIcon}
                                source={require('../../images/file_placehold.webp')}/>
            </View>;
        } else {
            view = <View style={{flexDirection: 'row'}}><Image style={styles.uploadImage1} source={{uri: this.state.idProofUri}}/>
                <Image style={styles.imageEdit} source={require('../../images/edit.webp')}/></View>;
        }
        return <TouchableOpacity onPress={() => {
            this.onClick();
        }}>
            {view}
        </TouchableOpacity>;
    }

    onClick() {
        new DocumentUploadUtils().plusIconClick(
            this.props.index,
            this,
            (index, docUri, docType, fileName, docSize, fileType) => {
                try {
                    if (Platform.OS === 'ios') {
                        let docDetail = docUri.substring(docUri.lastIndexOf('/') + 1);
                        let docName = docDetail.substring(0, docDetail.lastIndexOf('.'));
                        let docFormat = docDetail.substring(docDetail.lastIndexOf('.') + 1);
                        if (docUri !== null) {
                            this.setState({
                                submitEnabled: true,
                                idProofUri: docUri,
                                idProofType: fileType,
                                idProofName: docName,
                                docFileInfo: {type: docType, property: docSize + '|' + docFormat},
                            });
                        }
                    } else {
                        let docDetail = fileName.substring(fileName.lastIndexOf('/') + 1);
                        let docName = docDetail.substring(0, docDetail.lastIndexOf('.'));
                        let docFormat = docDetail.substring(docDetail.lastIndexOf('.') + 1);
                        if (docUri !== null) {
                            this.setState({
                                submitEnabled: true,
                                idProofUri: docUri,
                                idProofType: fileType,
                                idProofName: docName,
                                docFileInfo: {type: docType, property: docSize + '|' + docFormat},
                            });
                        }
                    }
                } catch (e) {
                }

            });
    }

    render() {
        return <View style={styles.container}>
            <DialogView ref={ref => (this.DialogViewRef = ref)}/>
            <View style={{flexDirection: 'row'}}>
                <Text style={[TextStyle.text_14_bold, styles.questionNumberStyle]}>{this.props.index + 1}</Text>
                <Text style={[TextStyle.text_16_bold, styles.questionTextStyle]}>Upload ID Proof</Text>
            </View>
            <Text style={[TextStyle.text_12_normal, {marginLeft: spacing.spacing40, marginBottom: spacing.spacing36}]}>ID
                Proof is needed to confirm your identity</Text>
            <View style={styles.imageUploadView}>
                <View style={styles.leftTextView}>
                    <Text style={[TextStyle.text_16_bold]}>Govt. ID</Text>
                    <Text style={[TextStyle.text_12_medium, {color: colors.darkGrey, marginTop: spacing.spacing8}]}>Upload
                        any - Passport, Voter ID, PAN, Aadhar, Driving License etc.</Text>
                </View>
                <View style={styles.imageView}>
                    {this.getImageUploadViewOrImage()}
                </View>
            </View>
            <View style={{marginTop: 24, marginLeft: spacing.spacing36}}>
                <ButtonWithLoader
                    isLoading={this.state.isLoading}
                    enable={this.state.submitEnabled}
                    Button={{
                        text: 'Next',
                        onClick: () => {
                            this.setState({isLoading: true});
                            this.uploadImage();
                        },
                    }}/>
            </View>
        </View>;
    }

    uploadImage() {
        let buyBackStatus = buybackManagerObj.getBuybackStatusDataFirstItem();
        chatBrigeRef.getApiProperty(apiProperty => {
            let _apiProperty = parseJSON(apiProperty);
            _apiProperty.apiHeader = { ..._apiProperty?.apiHeader, 'Content-Type': 'multipart/form-data' } 
            let url = _apiProperty.api_gateway_base_url + DOCUMENT_UPLOAD;
            const data = new FormData();
            data.append('file', {
                uri: this.state.idProofUri,
                name: this.state.idProofName,
                type: this.state.idProofType,
            });
            data.append('quoteId', buyBackStatus?.quoteId);
            data.append('documentType', 'IDENTITY_PROOF');
            futch(url, {
                method: 'post',
                body: data,
                headers: _apiProperty.apiHeader,
            }, (progressEvent) => {
                const progress = progressEvent.loaded / progressEvent.total;
            }).then((res) => {
                if (parseJSON(res._response).status === 'success') {
                    this.setState({isLoading: false, fileUploadStatus: UPLOAD_DONE});
                    if (buyBackStatus?.flow === buybackPartnerType.PARTNER_XIAOMI) {
                        this.props.navigation.navigate('MobilePickupScheduledSuccess', {});
                    } else {
                        this.props.onClick(this.props.data, '', this.props.index, 'INVOICE_UPLOAD');
                    }
                } else {
                    //    show error here
                    this.setState({isLoading: false});
                }
            }, (err) => {
                this.setState({isLoading: false});
            });
        });
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        marginBottom: spacing.spacing12,
        paddingBottom: 32,
    },
    questionNumberStyle: {
        width: spacing.spacing24,
        height: spacing.spacing24,
        borderRadius: 24 / 2,
        backgroundColor: colors.blue,
        color: colors.white,
        textAlign: 'center',
        overflow: 'hidden',
    },
    questionTextStyle: {
        flex: 1,
        marginLeft: spacing.spacing16,
        marginBottom: spacing.spacing8,
    },
    imageUploadView: {
        flex: 1,
        backgroundColor: colors.white,
        flexDirection: 'row',
        marginLeft: spacing.spacing36,
    },
    leftTextView: {
        width: '72%',
        justifyContent: 'flex-start',
    },
    imageView: {
        flex:1,
        flexDirection: 'row',
        overflow: 'hidden',
        justifyContent: 'flex-end'
    },
    uploadImageIcon: {
        height: 53,
        width: 53,
        alignItems: 'flex-end',
    },
    fileIcon: {
        height: spacing.spacing36,
        width: spacing.spacing28,
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    uploadImage1: {
        height: 45,
        width: 45,
        borderRadius: 8,
        resizeMode: 'cover',
    },
    imageEdit: {
        width: 16,
        height: 16,
        overflow: 'hidden',
        position: 'absolute',
        left: 30,
        top: 0,
    },
});


