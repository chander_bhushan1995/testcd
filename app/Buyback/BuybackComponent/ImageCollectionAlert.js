import React, { Component } from "react";
import { Modal, View, TouchableWithoutFeedback, StyleSheet, TouchableOpacity, Text, Image } from 'react-native'
import { TextStyle } from "../../Constants/CommonStyle"
import colors from "../../Constants/colors"
import spacing from "../../Constants/Spacing";
import images from '../../images/index.image';
import {buybackFlowStrings} from '../../Constants/BuybackConstants';


const TextImageView = (props) => {
    const { item, onClick = () => { } } = props;
    return <View style={{ flex: 0.5 }}>
        <Text style={[TextStyle.text_12_normal, { marginLeft: spacing.spacing12, marginBottom: spacing.spacing8 }]}>{item?.answer}</Text>
        <TouchableOpacity style={styles.itemContainerStyle} onPress={onClick}>
            <Image style={styles.deviceImageStyle} source={{ uri: item?.hintShortImage }} />
        </TouchableOpacity>
    </View>;
};

const ImageCollection = (props) => {
    const { data, onCancel = () => { }, onItemClick = () => { } } = props


    const deviceConditionsView = () => {
        let deviceConditionsView = [];
        let totalItems = data?.length;
        for (let sectionIndex = 0; sectionIndex < totalItems / 2; sectionIndex++) {
            let firstItemIndex = 2 * sectionIndex;
            let item = data[firstItemIndex];
            let view1 = <TextImageView item={item} key={firstItemIndex} onClick={() => onItemClick(firstItemIndex)} />
            let view2 = null;
            if ((2 * sectionIndex + 1) < totalItems) {
                let secondItemIndex = 2 * sectionIndex + 1;
                let anotherItem = data[secondItemIndex];
                view2 = <TextImageView item={anotherItem} key={secondItemIndex} onClick={() => onItemClick(secondItemIndex)} />
            }
            deviceConditionsView.push(
                <View key={sectionIndex} style={{ flexDirection: 'row' }}>
                    {view1}
                    {view2}
                </View>);
        }

        return deviceConditionsView
    }

    return (
        <View style={styles.rootViewStyle}>
            <TouchableOpacity style={styles.imageContainerStyle} onPress={onCancel}>
                <Image style={styles.imageStyle} source={images.ic_blackClose} />
            </TouchableOpacity>
            {deviceConditionsView()}
            <Text style={[TextStyle.text_12_normal, { alignSelf: 'center' }]}>
                {buybackFlowStrings.click_photo_msg}
            </Text>
        </View>
    );
}

export default class ImageCollectionAlert extends Component {

    state = {
        visible: false,
        collectionData: [],
    }

    showDialog(collectionData) {
        this.setState({
            visible: true,
            collectionData: collectionData,
        })
    }

    handleItemClick(index) {
        this.closePopup()
        this.props.clickedItemAtIndex(this.state.collectionData, index);
    }

    closePopup() {
        this.setState({ visible: false })
    }

    _renderOutsideTouchable() {
        return (
            <TouchableWithoutFeedback onPress={() => this.closePopup()} style={{ flex: 0.5, width: '100%' }}>
                <View style={{ flex: 0.5, width: '100%' }} />
            </TouchableWithoutFeedback>
        )
    }

    renderView() {
        return <View style={{ flex: 1 }}>
            <ImageCollection data={this.state.collectionData} onCancel={() => this.closePopup()} onItemClick={(index) => this.handleItemClick(index)} />
        </View>
    }

    render() {
        return (<Modal
            transparent={true}
            visible={this.state.visible}
            onRequestClose={() => {
                this.closePopup()
            }}>
            <View style={[{
                flex: 1,
                backgroundColor: "#777777AA",
                padding: spacing.spacing24,
            }]}>
                {this._renderOutsideTouchable()}
                {this.renderView()}
                {this._renderOutsideTouchable()}
            </View>
        </Modal>
        );
    }
}

const styles = StyleSheet.create({
    rootViewStyle: {
        backgroundColor: colors.white,
        borderRadius: 4,
        paddingVertical: spacing.spacing16,
    },
    imageContainerStyle: {
        height: 24,
        width: 24,
        backgroundColor: colors.transparent,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing8,
        alignSelf: 'flex-end',
    },
    imageStyle: {
        height: 24,
        width: 24,
        justifyContent: 'center',
    },
    itemContainerStyle: {
        borderRadius: 4,
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: colors.grey,
        marginBottom: spacing.spacing20,
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing12,
    },
    deviceImageStyle: {
        width: '100%',
        height: 100,
        resizeMode: 'contain',
    },
});
