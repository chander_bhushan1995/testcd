import React, {Component} from 'react';
import {Image, NativeModules, Platform, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {TextStyle} from '../../Constants/CommonStyle';
import colors from '../../Constants/colors';
import spacing from '../../Constants/Spacing';
import DocumentUploadUtils from '../../Components/DocumentUploadUtils';
import DialogView from '../../Components/DialogView';
import {MOBILE_BUYBACK} from '../../Constants/WebengageAttributes';
import {UPLOAD_INVOICE} from '../../Constants/WebengageEvents';
import ButtonWithLoader from '../../CommonComponents/ButtonWithLoader';
import {DOCUMENT_UPLOAD} from '../../Constants/ApiUrls';
import futch from '../../network/futch';
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import {ABB, SERVED_BY} from '../../Constants/WebengageAttrKeys';
import {getServedByValueFromFirstItem} from '../BuyBackUtils/BuybackUtils';
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

const chatBrigeRef = NativeModules.ChatBridge;
const UPLOAD_NOT_STARTED = 'upload_not_started';
const UPLOAD_DONE = 'upload_done';

export default class InvoiceUpload extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            submitEnabled: false,
            invoiceUri: '',
            invoiceType: '',
            invoiceName: 'Invoice',
            fileUploadStatus: UPLOAD_NOT_STARTED,
            docFileInfo: {type: 'image'},
        };
    }

    getImageUploadViewOrImage() {
        let view;
        if (this.state.invoiceUri === '') {
            view = <View><Image style={styles.uploadImageIcon}
                                source={require('../../images/icon_upload_image.webp')}/>
                <Text style={[TextStyle.text_14_bold, styles.uploadText]}>Upload</Text></View>;
        } else if (this.state.docFileInfo.type === 'document') {
            view = <View><Image style={styles.fileIcon}
                                source={require('../../images/file_placehold.webp')}/>
                <Text style={[TextStyle.text_12_normal, styles.fileUploadText]}>{this.state.invoiceName}</Text>
                <Text style={[TextStyle.text_12_normal, styles.fileSizeText]}>{this.state.docFileInfo.property}</Text>
                <Text
                    style={[TextStyle.text_12_normal, styles.fileUploadedSuccessfullText]}>{this.state.fileUploadStatus === UPLOAD_DONE ? 'Invoice has been uploaded successfully!' : ''}</Text>

            </View>;
        } else {
            view = <Image style={styles.uploadImage1} source={{uri: this.state.invoiceUri}}/>;
        }
        return <TouchableOpacity onPress={() => {
            this.onClick();
        }}>
            {view}
        </TouchableOpacity>;
    }

    onClick() {
        new DocumentUploadUtils().plusIconClick(
            this.props.index,
            this,
            (index, docUri, docType, fileName, docSize, fileType) => {
                try {
                    if (Platform.OS === 'ios') {
                        let docDetail = docUri.substring(docUri.lastIndexOf('/') + 1);
                        let docName = docDetail.substring(0, docDetail.lastIndexOf('.'));
                        let docFormat = docDetail.substring(docDetail.lastIndexOf('.') + 1);
                        if (docUri !== null) {
                            this.setState({
                                submitEnabled: true,
                                invoiceUri: docUri,
                                invoiceType: fileType,
                                invoiceName: docName,
                                docFileInfo: {type: docType, property: docSize + '|' + docFormat},
                            });
                        }
                    } else {
                        let docDetail = fileName.substring(fileName.lastIndexOf('/') + 1);
                        let docName = docDetail.substring(0, docDetail.lastIndexOf('.'));
                        let docFormat = docDetail.substring(docDetail.lastIndexOf('.') + 1);
                        if (docUri !== null) {
                            this.setState({
                                submitEnabled: true,
                                invoiceUri: docUri,
                                invoiceType: fileType,
                                invoiceName: docName,
                                docFileInfo: {type: docType, property: docSize + '|' + docFormat},
                            });
                        }
                    }
                } catch (e) {
                }
            });
    }

    getSubTitle(){
        return this.props?.data?.subtitle ?
            <Text style={[TextStyle.text_12_normal, styles.questionSubTextStyle]}>{this.props?.data?.subtitle}</Text> : null;
    }

    render() {
        return <View style={styles.container}>
            <DialogView ref={ref => (this.DialogViewRef = ref)}/>
            <View style={{flexDirection: 'row'}}>
                {/*TODO: send index as props from the flat lst*/}
                <Text style={[TextStyle.text_14_bold, styles.questionNumberStyle]}>{this.props.index + 1}</Text>
                <Text style={[TextStyle.text_16_bold, styles.questionTextStyle]}>{this.props?.data?.title}</Text>
            </View>
            {this.getSubTitle()}
            <View style={styles.imageUploadView}>
                {this.getImageUploadViewOrImage()}
            </View>
            <View style={{marginTop: 24, marginLeft: spacing.spacing36}}>
                <ButtonWithLoader
                    isLoading={this.state.isLoading}
                    enable={this.state.submitEnabled}
                    Button={{
                        text: 'Next',
                        onClick: () => {
                            this.setState({isLoading: true});
                            this.uploadImage();
                        },
                    }}/>
            </View>
        </View>;
    }

    uploadImage() {
        let buyBackStatus = buybackManagerObj.getBuybackStatusDataFirstItem();
        chatBrigeRef.getApiProperty(apiProperty => {
            let _apiProperty = parseJSON(apiProperty);
            _apiProperty.apiHeader = { ..._apiProperty?.apiHeader, 'Content-Type': 'multipart/form-data' } 
            let url = _apiProperty.api_gateway_base_url + DOCUMENT_UPLOAD;
            const data = new FormData();
            data.append('file', {
                uri: this.state.invoiceUri,
                name: this.state.invoiceName,
                type: this.state.invoiceType,
            });
            data.append('quoteId', buyBackStatus?.quoteId);
            futch(url, {
                method: 'post',
                body: data,
                headers: _apiProperty.apiHeader,
            }, (progressEvent) => {
                const progress = progressEvent.loaded / progressEvent.total;
            }).then((res) => {
                if (parseJSON(res._response).status === 'success') {
                    this.setState({isLoading: false, fileUploadStatus: UPLOAD_DONE});
                    let eventData = new Object();
                    eventData['Location'] = MOBILE_BUYBACK;
                    eventData[ABB] = buybackManagerObj?.isABBFlow();
                    eventData[SERVED_BY] = getServedByValueFromFirstItem();
                    logWebEnageEvent(UPLOAD_INVOICE, eventData);
                    this.props.onClick(this.props.data, '', this.props.index, 'INVOICE_UPLOAD');
                } else {
                    //    show error here
                    this.setState({isLoading: false});
                }
            }, (err) => {
                this.setState({isLoading: false});
            });
        });
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        marginBottom: spacing.spacing12,
        paddingBottom: 60,
    },
    questionNumberStyle: {
        width: spacing.spacing24,
        height: spacing.spacing24,
        borderRadius: 24 / 2,
        backgroundColor: colors.blue,
        color: colors.white,
        textAlign: 'center',
        overflow: 'hidden',
    },
    questionTextStyle: {
        flex: 1,
        marginLeft: spacing.spacing16,
    },
    questionSubTextStyle: {
        marginLeft: spacing.spacing40,
        marginBottom: spacing.spacing8,
        marginTop: spacing.spacing8,
    },
    imageUploadView: {
        borderRadius: 4,
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: colors.greye0,
        marginLeft: spacing.spacing36,
        marginTop: spacing.spacing24,
    },
    uploadImageIcon: {
        height: spacing.spacing64,
        marginTop: spacing.spacing24,
        width: spacing.spacing64,
    },
    fileIcon: {
        height: spacing.spacing36,
        marginTop: spacing.spacing24,
        width: spacing.spacing28,
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    uploadImage1: {
        height: spacing.spacing150,
        width: spacing.spacing300,
        resizeMode: 'cover',

    },
    uploadText: {
        textAlign: 'center',
        marginTop: spacing.spacing20,
        color: colors.blue,
        marginBottom: spacing.spacing20,
    },
    fileUploadText: {
        textAlign: 'center',
        marginTop: spacing.spacing12,
        color: colors.charcoalGrey,
    },
    fileSizeText: {
        textAlign: 'center',
    },
    fileUploadedSuccessfullText: {
        textAlign: 'center',
        marginTop: spacing.spacing12,
        color: colors.seaGreen,
        marginBottom: spacing.spacing8,
    },
});


