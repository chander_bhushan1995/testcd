import React, {Component} from "react";
import {NativeModules, StyleSheet, Text, View} from "react-native";
import {TextStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import InputBox from "../../Components/InputBox";
import {ORDER_QUESTIONS_SAVE} from "../buybackActions/Constants";
import ButtonWithLoader from "../../CommonComponents/ButtonWithLoader";
import * as BuybackApiHelper from '../buybackActions/BuybackApiHelper';
import {buybackManagerObj} from '../../Navigation/index.buyback';

let answersData = {questionType: "ORDER_QUESTION", data: []};
export default class IosIMEI extends Component {

    constructor(props) {
        super(props);
        let value = "";
        if (props.data.data !== null && props.data.data !== undefined && props.data.data.data !== undefined && props.data.data.data !== null) {
            let answerData = props.data.data.data;
            if (answerData[0] !== undefined && answerData[0] !== null && answerData[0].answer !== undefined && answerData[0].answer !== null) {
                value = answerData[0].answer;
            }
        }
        this.state = {
            answerValue: value,
            submitEnabled: this.valid_IMEI(value),
            isLoading:false,
            pincode:"",
            errorMessage:null
        }
        let data = buybackManagerObj.getBuybackStatusDataFirstItem();
        this.setState({pincode:data?.pincode});
    }


    render() {
        return (
            <View style={styles.container}>
                {this.renderItem(this.props.data)}
            </View>
        )
    }

    setLoadingState(isLoading) {
        this.setState({isLoading: isLoading})
    }

    executeSaveQuestionsApi(data, quoteId, questionType, dispatch) {
        BuybackApiHelper.saveAnswersForQuestions(quoteId, data, (response, error) => {
            if (response) {
                    this.onClick(data, 0, 0);
                    this.setLoadingState(false);
            } else {
                    this.setLoadingState(false);
            }
        });
    }

    onClick = (data, childindex, index,) =>
        this.props.onClick(data, childindex, index);

    renderItem = (data) => {
        return <View>
            <View style={{flexDirection: 'row'}}>
                <Text style={[TextStyle.text_14_bold, styles.questionNumberStyle]}>{1}</Text>
                <Text style={[TextStyle.text_16_bold, styles.questionTextStyle]}>{this.props.data.title}</Text>
            </View>
            <Text style={[TextStyle.text_12_normal, styles.questionDescriptionTextStyle]}>To find your IMEI, dial *#06# and press call.</Text>
            <InputBox
                value={this.state.answerValue}
                ref={input => {
                    this.textInput = input;
                }}
                containerStyle={{justifyContent: "space-between", marginLeft: spacing.spacing40}}
                inputHeading={'Enter your IMEI number'}
                placeholder={'Ex: 352066060926230'}
                multiline={true}
                autoFocus={true}
                errorMessage={this.state.errorMessage}
                keyboardType={"numeric"}
                maxLength={15}
                onChangeText={inputMessage => {
                    // var reg = new RegExp('^\\d+$');
                    // let validation=inputMessage.search(reg);
                    this.setState({
                        answerValue: inputMessage,
                    });

                    if (inputMessage.length == 15) {
                        let isValidImei =this.valid_IMEI(inputMessage);
                        this.setState({
                            errorMessage: isValidImei ? null : "Please enter valid IMEI number",
                            submitEnabled: isValidImei
                        });
                    }
                }}
                updateBlurChanges={ () => {
                    // var reg = new RegExp('^\\d+$');
                    // let validation=inputMessage.search(reg);
                    let isValidImei =this.valid_IMEI(this.state.answerValue);
                    this.setState({
                    errorMessage: isValidImei ? null : "Please enter valid IMEI number",
                    submitEnabled: this.state.answerValue.length == 15 && isValidImei
                    });
                    this.props.updateScrolling(true);
                }
                }
                updateFocusChanges = {()=>{
                    this.props.updateScrolling(false);
                }
                }
                editable={true}>
            </InputBox>
            <View style={[styles.nextButtonStyle]}>
                <ButtonWithLoader
                    isLoading={this.state.isLoading}
                    enable={this.state.submitEnabled}
                    Button={{
                        text: 'Next',
                        onClick: () => {
                            let buyBackStatusDatum = buybackManagerObj.getBuybackStatusDataFirstItem();
                            answersData.data = [];
                            let imei = this.state.answerValue.replace(/\s/g, '');
                            answersData.data.push({questionId: this.props.data.questionId, answer: imei});
                            this.executeSaveQuestionsApi(answersData, buyBackStatusDatum?.quoteId, ORDER_QUESTIONS_SAVE);
                            this.setLoadingState(true);
                        },
                    }
                    }/>
            </View>
        </View>
    }
     valid_IMEI(value) {
        // accept only digits, dashes or spaces
        if (/[^0-9-\s]+/.test(value) || value === null || value === undefined || value.length === 0) return false;
        // The Luhn Algorithm. It's so pretty.
        var nCheck = 0, nDigit = 0, bEven = false;
        value = value.replace(/\D/g, "");

        for (var n = value.length - 1; n >= 0; n--) {
            var cDigit = value.charAt(n),
                nDigit = parseInt(cDigit, 10);

            if (bEven) {
                if ((nDigit *= 2) > 9) nDigit -= 9;
            }

            nCheck += nDigit;
            bEven = !bEven;
        }

        return (nCheck % 10) == 0;
    }
}

const
    styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: colors.white,
            marginBottom: spacing.spacing24,
            paddingBottom:60

        },
        questionNumberStyle: {
            width: spacing.spacing24,
            height: spacing.spacing24,
            borderRadius: 24 / 2,
            paddingTop:2,
            backgroundColor: colors.blue,
            color: colors.white,
            textAlign: 'center',
            overflow:"hidden"
        },
        questionTextStyle: {
            flex:1,
            marginLeft: spacing.spacing16,
            marginBottom: spacing.spacing8,
        },
        questionDescriptionTextStyle: {
            marginLeft: spacing.spacing40,
            marginBottom: spacing.spacing36,
            color: colors.grey,
        },
        nextButtonStyle: {
            borderRadius: 2,
            marginTop: spacing.spacing24,
            marginLeft: spacing.spacing40,
        },

    });
