import React, {Component} from "react";
import {
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
    BackHandler,
    Image,
    Platform,
    NativeModules, SafeAreaView
} from "react-native";
import {ButtonStyle, TextStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";

import {HeaderBackButton} from "react-navigation";
import DialogView from "../../Components/DialogView";
import BlockingLoader from "../../CommonComponents/BlockingLoader";
import ApiErrorComponent from "../../CommonComponents/ApiErrorComponent";
import ApiCallingComponent from "../../CommonComponents/ApiCallingComponent";
import Toast from "react-native-easy-toast";
import {MOBILE_BUYBACK} from "../../Constants/WebengageAttributes";
import {RATING_POPUP} from "../../Constants/WebengageEvents";
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import {APIData} from '../../../index';
const nativeBrigeRef = NativeModules.ChatBridge;
let isRatingEventSubmit = false;
export default class MobilePickupScheduledSuccessComponent extends Component {

    static navigationOptions = ({navigation}) => {

        return {
            headerTitle: <View style={Platform.OS === "ios" ? {alignItems: "center"} : {alignItems: "flex-start"}}>
                <Text style={TextStyle.text_16_normal}>Mobile Condition</Text>
            </View>,
            headerLeft: <HeaderBackButton tintColor={colors.black}
                                          onPress={() => nativeBrigeRef.goBack()}/>,
            headerTitleStyle: {color: colors.white},
            headerTintColor: colors.white
        };
    };

    constructor(props) {
        super(props);
    }

    onBackPress = () => {
        if (this.props.ratingData !== undefined && this.props.ratingData.negativeRatingData !== undefined) {
            this.props.onCancel();
            return true;
        } else {
            this.onClick();
            return true;
        }

    };

    onClick() {
        if (this.props.orderID !== undefined) {
            this.props.navigation.navigate("BuybackRequestSuccessTimeline", {});
        } else {
            this.props.navigation.pop();
        }
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        this.callCreateOrderApi();

    }

    callCreateOrderApi() {
        let data = buybackManagerObj.getBuybackStatusDataFirstItem();
        this.props.executeCreateOrder(data?.quoteId);
    }

    render() {
        if (this.props.showRating) {
            let userName = APIData?.user_Name
            if(!APIData?.user_Name){
                userName = buybackManagerObj.getUserName()
            }
            nativeBrigeRef.refreshBuybackStatus();
            this.props.navigation.navigate('BuybackRequestSuccessTimeline', {});
        }
        return this.getView()
    }

    getView() {
        if (this.props.isError) {
            return <ApiErrorComponent isLoading={this.props.isLoading}
									  errorMessage={this.props.errorMessage}
                                      onClose={() => {
                                          this.onClick()
                                      }} onTryAgainClick={() => this.callCreateOrderApi()}/>
        } else if (this.props.orderID || (this.props.ratingData && this.props.ratingData.submitted)) {
            if (!isRatingEventSubmit) {
                isRatingEventSubmit = true;
                let eventData = new Object();
                eventData["Location"] = MOBILE_BUYBACK;
                logWebEnageEvent(RATING_POPUP, eventData);
            }
            return <SafeAreaView style={styles.SafeArea}>
                <View style={{flex: 1, backgroundColor: colors.white}}>
                    <Toast ref="toast" position="center"/>
                    <DialogView ref={ref => (this.DialogViewRef = ref)}/>
                    <BlockingLoader
                        visible={this.props.isLoading}
                        loadingMessage={'Loading...'}/>
                    <View style={styles.container}>
                        <TouchableOpacity onPress={() => {
                            this.onBackPress();
                        }}
                        >
                            <Image source={require("../../images/icon_cross.webp")} style={styles.crossIconStyle}/>
                        </TouchableOpacity>
                        <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
                            <Image source={require("../../images/green_right.webp")}
                                   style={styles.finalPriceImageStyle}/>
                            <Text style={[TextStyle.text_20_bold, styles.descriptionTextStyle]}>Great! Mobile pickup
                                scheduled.</Text>
                            <Text
                                style={[TextStyle.text_14_normal, styles.requestDetailsTextStyle]}>{this.props.orderID !== undefined ? "Your Order ID is " + this.props.orderID + ". A technician will visit you." : ""}</Text>
                            <TouchableOpacity
                                style={[styles.primaryButton, ButtonStyle.BlueButton]}
                                onPress={() => {
                                    this.props.navigation.navigate("BuybackRequestSuccessTimeline", {});
                                }}
                            >
                                <Text style={ButtonStyle.primaryButtonText}>View Status</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </SafeAreaView>;
        } else if (this.props.isLoading)
            return <ApiCallingComponent onClose={() => {
                this.onClick()
            }}/>
        else
            return null;
    }
}

const
    styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: colors.white,
            padding: spacing.spacing16,
        },
        crossIconStyle: {
            height: 16,
            width: 16,
            resizeMode: "contain",
            alignSelf: 'flex-end',
        },
        finalPriceImageStyle: {
            height: 50,
            width: 50,
            resizeMode: "contain",
            alignSelf: 'center',
        },
        descriptionTextStyle: {
            marginTop: spacing.spacing48,
            marginLeft: spacing.spacing12,
            marginRight: spacing.spacing12,
            textAlign: 'center',
        },
        requestDetailsTextStyle: {
            marginTop: spacing.spacing8,
            marginLeft: spacing.spacing12,
            marginRight: spacing.spacing12,
            textAlign: 'center',
        },
        primaryButton: {
            marginTop: spacing.spacing48,
            marginLeft: spacing.spacing12,
            marginRight: spacing.spacing12,
            width: '100%',
        },
        SafeArea: {
            flex: 1,
            backgroundColor: colors.white
        },
    });
