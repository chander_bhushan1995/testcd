import React, {useEffect, useReducer, useState} from 'react';
import {FlatList, NativeModules, Platform, SafeAreaView, Text, View, StyleSheet, BackHandler, Image} from 'react-native';
import BackButton from '../../CommonComponents/NavigationBackButton';
import {CommonStyle, TextStyle} from '../../Constants/CommonStyle';
import NearByRetailersReducer, {Actions} from './NearByRetailersReducer';
import Loader from '../../Components/Loader';
import DetailCard from '../../CustomComponent/DetailCard';
import {NEAR_BY_STORES} from '../../Constants/AppConstants';
import colors from '../../Constants/colors';
import spacing from '../../Constants/Spacing';
import {getDistance, getPreciseDistance} from 'geolib';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import images from "../../images/index.image";

let params = null;
const nativeBridgeRef = NativeModules.ChatBridge;

const initialState = {
    viewModel: [],
    isError: false,
    errorType: null,
    errorMessage: '',
    isLoading: true,
};

const NearByRetailers = props => {

    const [retailersState, retailersDispatch] = useReducer(NearByRetailersReducer, initialState);

    useEffect(() => {
        params = props.navigation.getParam('params');
        if ((params?.data ?? []).length > 0) {
            retailersDispatch({
                type: Actions.PREPARE_VIEW_MODEL, data: params?.data ?? [],
                userCoordinates: {
                    latitude: buybackManagerObj.getLocation()?.lat ?? 0,
                    longitude: buybackManagerObj.getLocation()?.lng ?? 0
                }, // here a default coordinate
            });
        } else {
            nativeBridgeRef.goBack();
        }

        BackHandler.addEventListener('hardwareBackPress', onBack);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', onBack);
        }
    }, []);

    const onBack = () => {
        props.navigation.pop();
        return true;
    }


    const LastSelected = props => {
        return (
            <View style={{flexDirection: 'row'}}>
                <Image source={images.time_icon} style={{width: 16,height: 16,alignSelf: 'center'}}/>
                <Text style={[TextStyle.text_12_normal,{color: colors.color_888F97,marginLeft: spacing.spacing4,alignSelf: 'center'}]}>Last Selected</Text>
            </View>
        )
    }

    const _renderItem = ({item}) => { // cell definition comes here
        return <DetailCard data={item} onClick={() => {
            props.navigation.navigate('RetailerDetailScreen', {detailData: item.data ?? {}})
        }} AccessoryView={item?.data?.isLastSelected ? LastSelected : null}/>;
    };

    const FlatListHeader = props => {
        return (
            <View>
                <Text style={[TextStyle.text_14_normal, {color: colors.charcoalGrey, margin: spacing.spacing16}]}>
                    {`Showing ${(params?.data ?? []).length} stores near you`}
                </Text>
            </View>
        );
    };

    return (
        <View style={styles.screen}>
            <FlatList
                data={retailersState?.viewModel ?? []}
                renderItem={_renderItem}
                ItemSeparatorComponent={() => <View
                    style={{backgroundColor: colors.transparent, height: spacing.spacing12}}/>}
                ListHeaderComponent={<FlatListHeader/>}
            />
            <Loader isLoading={retailersState.isLoading}/>
        </View>
    );
};

NearByRetailers.navigationOptions = (navigationData) => {
    return {
        headerLeft: <BackButton onPress={() => {
            navigationData.navigation.pop()
        }}/>,
        headerTitle: (
            <View style={Platform.OS === 'ios' ? {alignItems: 'center'} : {alignItems: 'flex-start'}}>
                <Text style={TextStyle.text_16_bold}>{NEAR_BY_STORES}</Text>
            </View>
        ),
    };
};

const styles = StyleSheet.create({
    screen: {
        backgroundColor: colors.white,
        flex: 1,
        paddingVertical: spacing.spacing12,
    },
    listingStyle: {
        flex: 1,
    },
});


export default NearByRetailers;
