import {getPreciseDistance} from 'geolib';
import {getDistance} from "../BuyBackUtils/BuybackUtils";

export const Actions = {
    PREPARE_VIEW_MODEL: "PREPARE_VIEW_MODEL"
}

const NearByRetailersReducer = (state = initialState, action) => {
    switch (action.type) {
        case Actions.PREPARE_VIEW_MODEL:
            let responseData = action.data ?? []
            let viewModel = []
            let lastSelectedRetailer = null
            responseData.map((data, index) => {
                let object = {}
                object.distance = getDistance(action.userCoordinates, {
                    latitude: data.latitude ?? 0,
                    longitude: data.longitude ?? 0
                })
                object.title = data.name ?? ""
                object.description = data.address ?? ""
                object.tag = object.distance + ' km'
                // object.note = "Open today : Closes at 6:30PM"
                object.data = data
                object.action = {actionText: "View Details"}
                viewModel.push(object)
            })
            viewModel = viewModel.filter((obj) => {
                if (obj?.data?.isLastSelected) {
                    lastSelectedRetailer = obj
                    return false
                }
                return true
            }).sort((object1, object2) => {
                if (object1.distance > object2.distance) {
                    return 1
                } else if (object1.distance < object2.distance) {
                    return -1
                } else {
                    return 0
                }
            })
            viewModel = lastSelectedRetailer ? [lastSelectedRetailer].concat(viewModel) : viewModel
            return {...state, viewModel: viewModel, isLoading: false}
        default:
            break;
    }
}


export default NearByRetailersReducer;
