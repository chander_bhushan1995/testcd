import React, {Component} from "react";
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import {TextStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import {API_ERROR, ORDER_QUESTIONS_SAVE, PAYMENT_MODE_IMPS} from "../buybackActions/Constants";
import {IMPS, MOBILE_BUYBACK, PAYTM, UPI} from "../../Constants/WebengageAttributes";
import {BUYBACK_PAYMENT_MODE} from "../../Constants/WebengageEvents";
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';
import * as BuybackApiHelper from '../buybackActions/BuybackApiHelper';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import {ABB, SERVED_BY} from '../../Constants/WebengageAttrKeys';
import {getServedByValueFromFirstItem} from '../BuyBackUtils/BuybackUtils';

let _this;
let buybackStatus;
export default class PaymentTypeQuestion extends Component {

    constructor(props) {
        super(props);
        _this = this;
    }

    componentDidMount() {
        let data = buybackManagerObj.getBuybackStatusDataFirstItem();
        buybackStatus = data?.buyBackStatus;
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderItem(this.props.data)}
            </View>
        )
    }

    onClick = (data, childindex, index,) =>
        this.props.onClick(data, childindex, index);

    getItem = (item) => {
        return <TouchableOpacity
            style={[styles.paymentItemStyle]}
            onPress={() => {
                let paymentMode;
                if(item.paymentMode === PAYMENT_MODE_IMPS){
                    paymentMode = IMPS;
                    _this.props.navigation.navigate("EnterBankDetails");
                }else if(item.paymentMode === PAYTM){
                    paymentMode = PAYTM;
                    _this.props.navigation.navigate("EnterPaytmNumber");
                }
                else
                {
                    paymentMode = UPI;
                    _this.props.navigation.navigate("EnterUPI");
                }
                let eventData = new Object();
                eventData["Location"] = MOBILE_BUYBACK;
                eventData["Type"] = paymentMode;
                eventData["Stage"] = buybackStatus;
                eventData[ABB] = buybackManagerObj?.isABBFlow();
                eventData[SERVED_BY] = getServedByValueFromFirstItem();
                logWebEnageEvent(BUYBACK_PAYMENT_MODE, eventData)
            }}>
            <Text style={[TextStyle.text_14_bold]}>{item.displayText}</Text>
            <Text style={[TextStyle.text_14_bold, {color: colors.blue028}]}>{item.buttonText}</Text>
        </TouchableOpacity>;
    };

    renderItem = (data) => {
        let paymentTypeView = [];
        let totalItems = data.data.length;
        for (let index = 0; index < totalItems; index++) {
            let item = data.data[index];
            let view = this.getItem(item);

            paymentTypeView.push(
                <View key={index}>
                    {view}
                </View>)
        }
        return <View>
            <View style={{flexDirection: 'row'}}>
                <Text style={[TextStyle.text_14_bold, styles.questionNumberStyle]}>{this.props.index + 1}</Text>
                <Text style={[TextStyle.text_16_bold, styles.questionTextStyle]}>{data.title}</Text>
            </View>
            <Text style={[TextStyle.text_12_normal, styles.questionDescriptionTextStyle]}>{data.description}</Text>
            <View style={styles.paymentViewStyle}>
                {paymentTypeView}
            </View>
        </View>
    }
}

const
    styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: colors.white
        },
        questionNumberStyle: {
            width: spacing.spacing24,
            height: spacing.spacing24,
            borderRadius: 24 / 2,
            backgroundColor: colors.blue,
            color: colors.white,
            paddingTop:2,
            textAlign: 'center',
            overflow:"hidden",
        },
        questionTextStyle: {
            flex:1,
            marginLeft: spacing.spacing16,
            marginBottom: spacing.spacing8,
        },
        questionDescriptionTextStyle: {
            marginLeft: spacing.spacing40,
            marginBottom: spacing.spacing24,
            color: colors.grey,
        },
        paymentViewStyle: {
            marginLeft: spacing.spacing40,
            marginBottom: spacing.spacing24,
        },
        paymentItemStyle: {
            padding: spacing.spacing12,
            borderRadius: 4,
            borderWidth: 1,
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: spacing.spacing20,
            borderColor: colors.greye0
        },

    });
