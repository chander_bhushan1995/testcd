

import React from 'react';
import { Image, SafeAreaView, StyleSheet, TouchableOpacity, View } from 'react-native';
import ScrollableTabView, { ScrollableTabBar } from '../../scrollableTabView/index';
import colors from '../../Constants/colors';
import spacing from '../../Constants/Spacing';

const ImageComponent = (props) => {
    const { imageUrl } = props;
    return (<View>
        <Image style={style.fullScreen} source={{ uri: imageUrl }} />
    </View>)
}

export default function QuestionHintScreen(props) {
    const { navigation } = props;
    const data = navigation.getParam('data', []);
    const selectedIndex = navigation.getParam('selectedIndex', 0);

    return (
        <SafeAreaView style={style.container}>
            <TouchableOpacity style={style.crossTouch} onPress={() => navigation.pop()}>
                <Image style={style.crossIcon} source={require("../../images/icon_cross.webp")} />
            </TouchableOpacity>
            <ScrollableTabView initialPage={selectedIndex}
                prerenderingSiblingsNumber={1}
                renderTabBar={() => <ScrollableTabBar tabsContainerStyle={style.tabContainerView}
                    hasShadow={true}
                    underlineStyle={style.tabBarUnderlineView}
                    textStyle={style.tabBarTextStyle}
                    activeTextColor={colors.blue028}
                    inactiveTextColor={colors.grey} />}
            >
                {data.map((hint, index) => {
                    const { answer, hintShortImage, hintLongImage } = hint
                    return <ImageComponent tabLabel={answer} key={index} imageUrl={hintLongImage} />
                })}
            </ScrollableTabView>
        </SafeAreaView>
    );
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    crossTouch: {
        width: 48,
        height: 48,
        marginRight: spacing.spacing0,
        marginTop: spacing.spacing0,
        padding: spacing.spacing16,
        alignSelf: "flex-end"
    },
    crossIcon: {
        width: 16,
        height: 16,
        tintColor: colors.black,
        resizeMode: "contain"
    },
    fullScreen: {
        width: "100%",
        height: "100%",
        resizeMode: "contain"
    },
    tabBarTextStyle: {
        fontSize: 13,
        fontFamily: "Lato-Semibold",
        lineHeight: 20
    },
    tabBarUnderlineView: {
        backgroundColor: colors.blue028,
        height: spacing.spacing2,
        borderRadius: spacing.spacing2
    },
    tabContainerView: {
        borderBottomColor: colors.grey,
    }
});