import React, { Component } from "react";
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import colors from "../../Constants/colors";
import spacing from "../../Constants/Spacing";

var maxRating = 5;

export default class RatingBuyback extends Component {

    constructor() {
        super();
        this.state = {
            defaultRating: maxRating,
        };
    }

    render() {
        if (this.props.startRating) {
            var Star = require("../images/star.webp");
            var Star_With_Border = require("../images/star_unselected.webp");
            let reactNativeRatingBar = [];
            for (var i = 1; i <= maxRating; i++) {
                let value = i;
                reactNativeRatingBar.push(
                    <TouchableOpacity activeOpacity={1.0} key={i} onPress={() => this.setState({ defaultRating: value })}>
                        <Image style={styles.StarImage} source={value <= this.state.defaultRating ? Star : Star_With_Border} />
                    </TouchableOpacity>
                );
            }
            return (
                <View style={styles.MainContainer}>
                    <Text style={styles.Heading}>How was your buyback experience?</Text>
                    <View style={styles.Rating}>{reactNativeRatingBar}</View>
                    <TouchableOpacity
                        // disabled={}
                        style={styles.submitButton}
                        onPress={() => {
                            this.props.ratingSubmit(this.state.defaultRating);
                        }}>
                        <Text style={styles.TextStyle}>Submit</Text>
                    </TouchableOpacity>
                </View>
            );
        } else {
            return (
                <View style={styles.MainContainer}>
                    <Text style={styles.Heading}>Thank you for your feedback. Please take some time to rate us on {Platform.OS == "ios" ? "Apple App" : "Google Play"} Store</Text>
                    <View style={styles.googlePlayReviewButtonsConatiner}>
                        <TouchableOpacity style={styles.notNowButton}
                            onPress={() => {
                                this.props.onClicknotNow("No, Thanks!");
                            }}>
                            <Text style={styles.notNowtextStyle}>Not Now</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.sureButton}
                            onPress={() => this.props.onClickSure()}>
                            <Text style={styles.TextStyle}>Sure</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    MainContainer: {
        marginBottom: spacing.spacing16,
        borderRadius: 4,
        overflow: "hidden",
        backgroundColor: colors.white
    },
    StarImage: {
        width: 40,
        height: 40,
        margin: spacing.spacing4,
        resizeMode: "cover"
    },
    notNowtextStyle: {
        textAlign: "center",
        fontSize: 16,
        fontFamily: "Lato",
        color: colors.blue
    },
    TextStyle: {
        color: colors.white,
        textAlign: "center",
        fontWeight: "bold",
        fontFamily: "Lato",
        fontSize: 16
    },
    Heading: {
        margin: spacing.spacing12,
        lineHeight: 20,
        color: colors.charcoalGrey,
        fontFamily: "Lato",
        fontSize: 16
    },
    Rating: {
        alignSelf: "center",
        flexDirection: "row",
        marginBottom: spacing.spacing8
    },
    submitButton: {
        padding: spacing.spacing12,
        margin: spacing.spacing16,
        backgroundColor: colors.blue,
        borderRadius: 4
    },
    notNowButton: {
        padding: spacing.spacing12,
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    sureButton: {
        padding: spacing.spacing12,
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
        backgroundColor: colors.blue,
        borderRadius: 4
    },
    googlePlayReviewButtonsConatiner: {
        margin: spacing.spacing16,
        alignItems: "center",
        flexDirection: "row"
    },
    arrowIcon: {
        width: 15,
        height: 12
    }
});
