import React, { Component } from "react";
import {
    FlatList,
    Image,
    KeyboardAvoidingView,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    ScrollView
} from "react-native";
import { CommonStyle } from "../../Constants/CommonStyle";
import colors from "../../Constants/colors";
import spacing from "../../Constants/Spacing";
import RatingQuestionnaireItem1 from "./RatingQuestionnaireItem1";

var selectedQuestionsId = [];
var ratingFeedbackComment = "";
let questionList;
let negativeRatingSubmit;
let onCancel;
export default class RatingQuestionnaire1 extends Component {
    constructor(props) {
        super(props);
         questionList = this.props.ratingQuestions;
         negativeRatingSubmit = this.props.negativeRatingSubmit;
         onCancel =this.props.onCancel;
        questionList.push("add_textinput");
        this.state = {
            keyboardAvoidingViewKey: "keyboardAvoidingViewKey",
            isChecked: false,
            color: colors.charcoalGrey,
            isSubmitButtonDisabled: true
        };
    }

    componentWillUnmount() {
        ratingFeedbackComment = "";
        selectedQuestionsId = [];
    }

    updateSubmitButtonState(isEnable) {
        this.setState({ isSubmitButtonDisabled: isEnable });
    }

    render() {
        return (
            <KeyboardAvoidingView
                style={styles.MainContainer1}>
                <SafeAreaView style={styles.SafeArea}>
                    <ScrollView style={styles.MainContainer}>
                        <TouchableOpacity
                            onPress={() => {
                                onCancel(false);
                                this.props.onCancel();
                            }}
                        >
                            <Image source={require("../../images/icon_cross.webp")} style={styles.crossIconContainer} />
                        </TouchableOpacity>
                        <Text style={styles.Heading}>Tell us what went wrong</Text>
                        <FlatList
                            contentContainerStyle={styles.QuestionList}
                            // data={this.props.data}
                            data={questionList}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={this.renderQuestionnaireList}
                            ItemSeparatorComponent={() => {
                                return <View style={CommonStyle.separator} />;
                            }}
                        />
                        <TouchableOpacity
                            disabled={this.state.isSubmitButtonDisabled}
                            style={[styles.submitButton, this.state.isSubmitButtonDisabled && styles.submitButtonDisable]}
                            onPress={() => {
                                this.props.negativeRatingSubmit(ratingFeedbackComment, selectedQuestionsId);
                                // this.props.onCancel(true);
                            }}
                        >
                            <Text style={styles.TextStyle}>Send Feedback</Text>
                        </TouchableOpacity>
                    </ScrollView>
                </SafeAreaView>
            </KeyboardAvoidingView>
        );
    }

    renderQuestionnaireList = ({ item, index }) => {
        return (
            <RatingQuestionnaireItem1
                index={index}
                selectedQuestionsId={selectedQuestionsId}
                disbaleSubmitButton={isEnable => {
                    this.updateSubmitButtonState(isEnable);
                }}
                ratingFeedbackComment={comment => {
                    ratingFeedbackComment = comment;
                }}
                questionListObj={item}
            />
        );
    };
}

const styles = StyleSheet.create({
    MainContainer: {
        paddingTop: spacing.spacing24,
        flex: 1
    },
    MainContainer1: {
        flex: 1
    },
    QuestionList: {
        marginTop: spacing.spacing24,
        justifyContent: "space-between",
        paddingBottom: spacing.spacing64,
        marginBottom: spacing.spacing24
    },
    Heading: {
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        color: colors.charcoalGrey,
        fontSize: 20,
        fontFamily: "Lato",
        fontWeight: "bold",
        marginTop: spacing.spacing16,
        justifyContent: "flex-end"
    },
    crossIconContainer: {
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing12,
        height: 20,
        width: 20,
        resizeMode: "contain",
        justifyContent: "flex-start"
    },
    submitButtonDisable: {
        backgroundColor: colors.lightGrey,
    },
    submitButton: {
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        paddingTop: 14,
        paddingBottom: 14,
        marginBottom: spacing.spacing24,
        backgroundColor: colors.blue,
        borderRadius: 4,
        // height: 48,
        justifyContent: "flex-end"
    },
    TextStyle: {
        color: colors.white,
        textAlign: "center",
        fontFamily: "Lato-Semibold",
        fontSize: 16,
    },
    SafeArea: {
        flex: 1,
        backgroundColor: colors.white
    }
});
