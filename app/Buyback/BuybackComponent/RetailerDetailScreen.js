import React, {useEffect, useState, useRef, useContext} from 'react';
import {
    View,
    Text,
    Platform,
    StyleSheet,
    Linking,
    requireNativeComponent, Image, SafeAreaView, NativeModules, BackHandler,
} from 'react-native';
import BackButton from '../../CommonComponents/NavigationBackButton';
import {TextStyle} from '../../Constants/CommonStyle';
import BottomDrawer from 'rn-bottom-drawer';
import colors from '../../Constants/colors';
import DetailMultiActionComponent from '../../CustomComponent/DetailMultiActionComponent';
import images from '../../images/index.image';
import spacing from '../../Constants/Spacing';
import CheckBox from 'react-native-check-box';
import {
    CALL_RETAILER,
    CONFIRM_VISIT,
    GET_DIRECTIONS,
    PLATFORM_OS,
    PURCHASED_FROM_SAME,
} from '../../Constants/AppConstants';
import {getBottomSpace} from 'react-native-iphone-x-helper';
import {createOrderWithRetailer} from '../buybackActions/BuybackApiHelper';
import {BuybackContext, buybackManagerObj} from '../../Navigation/index.buyback';
import {APIData} from '../../../index';
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';
import * as EventNames from '../../Constants/WebengageEvents'
import * as EventAttrs from '../../Constants/WebengageAttrKeys'
import {BuybackEventLocation, buybackFlowType} from '../../Constants/BuybackConstants';
import {getDistance, getServedByValueFromFirstItem} from "../BuyBackUtils/BuybackUtils";
import {buybackPartnerType} from '../../Constants/BuybackConstants';

const NativeMapView = requireNativeComponent('RCTGoogleMapView');
const nativeBridgeRef = NativeModules.ChatBridge;

const RetailerDetailScreen = props => {

    const [completeScreenLayout, setCompleteScreenLayout] = useState(null);
    const [mapViewHeight, setMapViewHeight] = useState(0);
    const [bottomSheetLayout, setBottomSheetLayout] = useState(null);
    const [termsConditionsAccepted, setTermsConditions] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const {Alert} = useContext(BuybackContext);
    const retailerData = props.navigation.getParam('detailData');
    const distance = getDistance(buybackManagerObj.getLocation(), {
        latitude: retailerData.latitude ?? 0,
        longitude: retailerData.longitude ?? 0
    }) ?? '';
    const data = buybackManagerObj.getBuybackStatusDataFirstItem();
    const navigation = props.navigation;

    useEffect(()=>{
        BackHandler.addEventListener('hardwareBackPress', onBack);
        return ()=>{
            BackHandler.removeEventListener('hardwareBackPress', onBack);
        }
    },[])

    const onBack = ()=>{
        navigation.pop();
        return true;
    }

    const confirmVisit = () => {
        setIsLoading(true);
        let _quoteId = data?.quoteId;
        let webEngageAttr = new Object()
        webEngageAttr[EventAttrs.LOCATION] = BuybackEventLocation.EXCHANGE_REQUEST
        webEngageAttr[EventAttrs.ABB] = buybackManagerObj?.isABBFlow()
        webEngageAttr[EventAttrs.SERVED_BY] = getServedByValueFromFirstItem();
        webEngageAttr[EventAttrs.OFFER_PRICE] = retailerData?.price
        webEngageAttr[EventAttrs.RETAILER_NAME] = retailerData?.name
        logWebEnageEvent(EventNames.VISIT_CONFIRMATION, webEngageAttr)

        createOrderWithRetailer(_quoteId, retailerData?.servicePartnerCode, retailerData?.servicePartnerBuCode, (response, error) => {
            setIsLoading(false);
            if (response !== null) {
                let data = buybackManagerObj.getBuybackStatusDataFirstItem();
                let userName = APIData?.user_Name
                data.subType = response.subType ?? buybackFlowType.RETAILER_PICKUP
                if(!APIData?.user_Name){
                    userName = buybackManagerObj.getUserName()
                }
                nativeBridgeRef.refreshBuybackStatus();
                props.navigation.navigate('BuybackRequestSuccessTimeline', {});
            } else {
                Alert.showAlert('Unable to proceed', error[0]?.message, {text: 'OK'});
            }
        });
    };

    const gpsAction = () => {

        let webEngageAttr = new Object()
        webEngageAttr[EventAttrs.LOCATION] = BuybackEventLocation.EXCHANGE_REQUEST
        logWebEnageEvent(EventNames.GET_DIRECTION, webEngageAttr)

        let location = buybackManagerObj.getLocation();
        nativeBridgeRef?.openOSMap({
            from: {latitude: location?.lat ?? 0, longitude: location?.lng ?? 0},
            to: {latitude: retailerData?.latitude ?? 0, longitude: retailerData?.longitude ?? 0},
        });
    };

    const callAction = () => {
        let phoneNumber = '';

        let webEngageAttr = new Object()
        webEngageAttr[EventAttrs.LOCATION] = BuybackEventLocation.EXCHANGE_REQUEST
        logWebEnageEvent(EventNames.CALL_RETAILER, webEngageAttr)

        if (Platform.OS === PLATFORM_OS.android) {
            phoneNumber = `tel:${retailerData?.mobile ?? 0}`;
        } else {
            phoneNumber = `telprompt:${retailerData?.mobile ?? 0}`;
        }
        Linking.canOpenURL(phoneNumber)
            .then((supported) => {
                if (supported) {
                    return Linking.openURL(phoneNumber);
                }
            })
            .catch((err) => console.error('An error occurred', err));
    };

    useEffect(() => { //whenever complete screen layyout or bottomsheet layout will change map height will be updated
        if (completeScreenLayout && bottomSheetLayout) {
            updateMapViewHeight();
        }
    }, [completeScreenLayout, bottomSheetLayout]);

    const updateMapViewHeight = () => { // if bottom view is expended then mapview height should little greater than bottom view height
        let difference = (completeScreenLayout?.height ?? 0) - (bottomSheetLayout?.height ?? 0);
        let mapViewHeight = difference + ((bottomSheetLayout?.height ?? 0) * 0.50);
        setMapViewHeight(mapViewHeight);
    };

    const onCollapsed = () => {
        setMapViewHeight(completeScreenLayout?.height ?? 0);
    };

    const getAgreementCheckboxComponent = props => {
        return (
            <View style={styles.agreeTermsViewStyle}>
                <CheckBox
                    style={styles.checkboxStyle}
                    checkedImage={<Image source={images.checkBox_checked}
                                         style={styles.checkboxStyle}/>}
                    unCheckedImage={<Image source={images.checkBox_unChecked}
                                           style={styles.checkboxStyle}/>}
                    isChecked={termsConditionsAccepted}
                    onClick={() => {
                        setTermsConditions(!termsConditionsAccepted);
                    }
                    }
                />
                <Text style={[TextStyle.text_12_bold]} onPress={() => {
                    setTermsConditions(!termsConditionsAccepted);
                }}>
                    {'I agree to all the '}
                    <Text style={[TextStyle.text_12_normal, styles.tncTextStyle]} onPress={() => {
                        navigation.navigate('TermsAndConditions', {});
                    }}>{'Terms and Conditions '} </Text>&
                    <Text style={[TextStyle.text_12_normal, styles.tncTextStyle]} onPress={() => {
                        navigation.navigate('BuybackDeclaration', {});
                    }}> {'Declaration '}</Text>
                </Text>
            </View>
        );
    };

    const InfoView = () => {
        return (
            <View style={styles.infoViewContainer}>
                <Image source={images.yellowInfo} style={styles.infoImage}/>
                <Text
                    style={[TextStyle.text_12_normal, styles.notes]}>{PURCHASED_FROM_SAME}</Text>
            </View>
        );
    };

    const renderContent = () => {
        const hasOrderId = data?.orderId?.length ? true : false
        const isViewOnly = data?.flow === buybackPartnerType.PARTNER_XIAOMI && hasOrderId
        return (
            <View
                onLayout={(event) => {
                    !bottomSheetLayout ? setBottomSheetLayout(event.nativeEvent.layout) : null;
                }}
            >
                <View style={styles.buttonStrip}/>
                <DetailMultiActionComponent
                    tag={distance+' km'}
                    title={retailerData?.name}
                    description={retailerData?.address}
                    primaryButton={isViewOnly ? null : {text: CONFIRM_VISIT, action: confirmVisit}}
                    actionFirst={{text: CALL_RETAILER, image: images.call, action: callAction}}
                    actionSecond={{text: GET_DIRECTIONS, image: images.ic_gps, action: gpsAction}}
                    notes={null}//{'Open today : Closes at 6:30PM'}
                    infoView={retailerData?.purchasedFromSame ? InfoView : null}
                    termsAndConditionsView={isViewOnly ? null : getAgreementCheckboxComponent}
                    isPrimaryBtnEnabled={termsConditionsAccepted}
                    isLoading={isLoading}
                />
            </View>
        );
    };

    return (
        <SafeAreaView
            style={{flex: 1, backgroundColor: colors.white}} onLayout={(e) => {
            if (!completeScreenLayout) {
                setCompleteScreenLayout(e.nativeEvent.layout);

            }
        }}>

            <NativeMapView
                location={{latitude: retailerData?.latitude ?? 0, longitude: retailerData?.longitude ?? 0}}
                style={{
                    width: '100%',
                    // height: ((completeScreenLayout?.height ?? 0) - (bottomSheetLayout?.height ?? 0)) + 200,
                    height: mapViewHeight,
                }}
                zoomLevel={15}
                onTappedMarker={() => {
                    // alert('tapped');
                }}
            />

            {bottomSheetLayout?.height
                ? <BottomDrawer
                    containerHeight={bottomSheetLayout?.height ?? 0}
                    offset={70 + getBottomSpace()}
                    downDisplay={(bottomSheetLayout?.height ?? 0) / 1.180}
                    onExpanded={updateMapViewHeight}
                    onCollapsed={onCollapsed}>
                    {renderContent()}
                </BottomDrawer>

                : renderContent()
            }

        </SafeAreaView>
    );

};

RetailerDetailScreen.navigationOptions = (navigationData) => {
    const retailerData = navigationData.navigation.getParam('detailData');
    const heading = retailerData?.name;
    return {
        headerLeft: <BackButton onPress={() => {
            navigationData.navigation.pop();
        }}/>,
        headerTitle: (
            <View style={Platform.OS === 'ios' ? {alignItems: 'center'} : {alignItems: 'flex-start'}}>
                <Text style={TextStyle.text_16_bold}>{heading}</Text>
            </View>
        ),
    };
};


let styles = StyleSheet.create({
    buttonStrip: {
        width: '11%',
        height: spacing.spacing4,
        backgroundColor: colors.lightGrey2,
        marginTop: spacing.spacing16,
        alignSelf: 'center',
    },
    infoViewContainer: {
        marginTop: spacing.spacing16,
        padding: spacing.spacing8,
        backgroundColor: colors.color_yellowTags,
        borderRadius: 4,
        overflow: 'hidden',
        flexDirection: 'row',
        alignItems: 'center',
    },
    infoImage: {
        height: 18,
        width: 18,
    },
    notes: {
        color: colors.charcoalGrey,
        marginHorizontal: spacing.spacing8,
    },
    agreeTermsViewStyle: {
        flexDirection: 'row',
        marginTop: spacing.spacing16,
    },
    checkboxStyle: {
        width: spacing.spacing18,
        height: spacing.spacing18,
        marginRight: spacing.spacing4,
    },
    tncTextStyle: {
        marginLeft: spacing.spacing4,
        marginRight: spacing.spacing4,
        color: colors.blue028,
    },
});

export default RetailerDetailScreen;
