import BackButton from '../../CommonComponents/NavigationBackButton';
import {
    NativeModules,
    View,
    Image,
    StyleSheet,
    Dimensions,
    Modal, BackHandler,
} from 'react-native';
import React, {useContext, useEffect, useState} from 'react';
import colors from '../../Constants/colors';
import spacing from '../../Constants/Spacing';
import images from '../../images/index.image';
import {TextStyle} from '../../Constants/CommonStyle';
import {
    BUYBACK_CANCEL_CONFIRM,
    CANCEL_REQUEST,
    NO_RETAILERS,
    SCHEDULE_PICKUP,
    WANT_TO_SELL,
} from '../../Constants/AppConstants';
import {BuybackContext, buybackManagerObj} from '../../Navigation/index.buyback';
import {APIData} from '../../../index';
import {cancelOnGoingRequest} from '../buybackActions/BuybackApiHelper';
import Loader from '../../Components/Loader';
import {TECHNICAL_ERROR_MESSAGE} from '../../Constants/ActionTypes';
import * as EventNames from '../../Constants/WebengageEvents'
import * as EventAttrs from '../../Constants/WebengageAttrKeys'
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';
import {BuybackEventLocation} from '../../Constants/BuybackConstants';
import CommonBuybackAlertUI from './CommonBuybackAlertUI';
import {getServedByValueFromFirstItem} from "../BuyBackUtils/BuybackUtils";
import {useDispatch} from 'react-redux';
import {RESET_QUESTIONS} from '../buybackActions/Constants';

const nativeBridgeRef = NativeModules.ChatBridge;

const RetailersEmptyScreen = props => {

    const [isLoading, setIsLoading] = useState(false);
    const [isModalVisible, setModalVisible] = useState(false);
    const questionsData = props.navigation.getParam('questionsData')
    const data = buybackManagerObj.getBuybackStatusDataFirstItem();
    const {Alert} = useContext(BuybackContext);
    const dispatch = useDispatch();


    useEffect(()=>{
        BackHandler.addEventListener('hardwareBackPress', onBack);
        return ()=>{
            BackHandler.removeEventListener('hardwareBackPress', onBack);
        }
    },[])

    const onBack = ()=>{
        props.navigation.pop();
        return true;
    }

    const showModal = () => {
        setModalVisible(true);

        let webEngageAttr = new Object()
        webEngageAttr[EventAttrs.LOCATION] = BuybackEventLocation.BUYBACK
        webEngageAttr[EventAttrs.ABB] = buybackManagerObj?.isABBFlow()
        webEngageAttr[EventAttrs.SERVED_BY] = getServedByValueFromFirstItem();
        webEngageAttr[EventAttrs.TAG] = BuybackEventLocation.EXCHANGE_REQUEST
        logWebEnageEvent(EventNames.CANCEL_REQUEST, webEngageAttr)
    };

    const cancelOrder = () => {
        setModalVisible(false);
        setIsLoading(true);
        let data = buybackManagerObj.getBuybackStatusDataFirstItem();

        let webEngageAttr = new Object()
        webEngageAttr[EventAttrs.LOCATION] = BuybackEventLocation.BUYBACK
        webEngageAttr[EventAttrs.EXCHANGE_PRICE] = data?.price
        webEngageAttr[EventAttrs.TAG] = BuybackEventLocation.EXCHANGE_REQUEST
        logWebEnageEvent(EventNames.CANCEL_REQUEST_CONFIRMATION, webEngageAttr)

        cancelOnGoingRequest(data?.quoteId, APIData.customer_id, (response, error) => {
            setIsLoading(false);
            if (response) {
                nativeBridgeRef.goBack();
            } else {
                Alert.showAlert('Alert', error[0]?.errorMessage ?? TECHNICAL_ERROR_MESSAGE, {text: 'OK'});
            }
        });
    };

    const shedulePickup = () => {
        setModalVisible(false);

        let webEngageAttr = new Object()
        webEngageAttr[EventAttrs.LOCATION] = BuybackEventLocation.NOT_FOR_EXCHANGE
        webEngageAttr[EventAttrs.ABB] = buybackManagerObj?.isABBFlow()
        webEngageAttr[EventAttrs.SERVED_BY] = getServedByValueFromFirstItem();
        webEngageAttr[EventAttrs.OFFER_PRICE] = data?.price
        logWebEnageEvent(EventNames.SCHEDULE_PICKUP, webEngageAttr)
        dispatch({ type: RESET_QUESTIONS})
        if (!questionsData) {
            return
        }
        props.navigation.navigate('BuybackQuestions', {
            params: {
                questionData: questionsData,
                currentQuestionType: 'ORDER_QUESTION',
            },
        });
    };


    return (
        <View style={styles.screen}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={isModalVisible}
                onRequestClose={() => {

                }}
            >
                <View style={styles.modalItemContainer}>
                    <View style={{backgroundColor: 'white', borderRadius: 4, marginHorizontal: spacing.spacing16}}>
                        <CommonBuybackAlertUI
                            data={data}
                            title={{
                                text: BUYBACK_CANCEL_CONFIRM,
                                style: {...TextStyle.text_16_semibold, color: colors.charcoalGrey},
                            }}
                            primaryButton={{text: WANT_TO_SELL, action: shedulePickup}}
                            secondaryButton={{text: CANCEL_REQUEST, action: cancelOrder}}
                        />
                    </View>
                </View>
            </Modal>


            {/*<ScrollView bounces={false}>*/}
            <Image style={styles.locationImage} source={images.area_not_servicable}/>
            <CommonBuybackAlertUI
                data={data}
                title={{text: NO_RETAILERS}}
                primaryButton={{text: SCHEDULE_PICKUP, action: shedulePickup}}
                secondaryButton={{text: CANCEL_REQUEST, action: showModal}}
            />
            {/*</ScrollView>*/}
            {isLoading
                ? <View style={styles.loaderContainer}><Loader isLoading={isLoading}/></View>
                : null
            }
        </View>
    );
};


RetailersEmptyScreen.navigationOptions = (navigationData) => {
    return {
        headerLeft: <BackButton onPress={() => {
            navigationData.navigation.pop();
        }}/>,
    };
};

const styles = StyleSheet.create({
    screen: {
        backgroundColor: colors.white,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    locationImage: {
        alignSelf: 'center',
        width: 60,
        height: 80,
        resizeMode: 'contain'
    },
    modalItemContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.color_8A000000,
        overflow: 'hidden',
    },
    loaderContainer: {
        position: 'absolute',
        top: 0,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        width: Dimensions.get('screen').width,
        height: Dimensions.get('screen').height,
        backgroundColor: 'transparent',
    },
});

export default RetailersEmptyScreen;
