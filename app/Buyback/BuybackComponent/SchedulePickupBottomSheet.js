import React, {useContext, useState} from 'react';
import {Text, View, StyleSheet, TouchableOpacity, NativeModules} from 'react-native';
import spacing from '../../Constants/Spacing';
import colors from '../../Constants/colors';
import {TextStyle} from '../../Constants/CommonStyle';
import {
    CANCEL_REQUEST, NOT_FOR_EXCHANGE,
    PRICE_EXP_IN,
    RESALE_PRICE,
    SCHEDULE_PICKUP,
    SCHEDULE_PICKUP_DOORSTEP,
} from '../../Constants/AppConstants';
import ButtonWithLoader from '../../CommonComponents/ButtonWithLoader';
import BlockingLoader from '../../CommonComponents/BlockingLoader';
import useLoader from '../../CardManagement/CustomHooks/useLoader';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import {daysDifference, getCurretDateMilliseconds} from '../../commonUtil/DateUtils';
import {TECHNICAL_ERROR_MESSAGE} from '../../Constants/ActionTypes';
import * as APIHelper from '../../network/APIHelper';
import * as EventAttrs from '../../Constants/WebengageAttrKeys';
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';
import * as EventNames from '../../Constants/WebengageEvents';
import {BuybackEventLocation} from '../../Constants/BuybackConstants';
import {getServedByValue} from "../BuyBackUtils/BuybackUtils";

const nativeBridgeRef = NativeModules.ChatBridge;

const SchedulePickupBottomSheet = props => {
    const buybackData = props.buybackData ?? {};
    const onSchedulePickup = props.onSchedulePickup ?? (() => {
    });
    const onCancel = props.onCancelSchedulePickup ?? (() => {
    });

    const [isBlockingLoader, blockingLoaderMessage, startBlockingLoader, stopBlockingLoader] = useLoader();

    const cancelBuybackRequest = () => {
        let webEngageAttr = new Object()
        webEngageAttr[EventAttrs.LOCATION] = BuybackEventLocation.BUYBACK
        webEngageAttr[EventAttrs.ABB] = buybackManagerObj?.isABBFlow()
        webEngageAttr[EventAttrs.SERVED_BY] = getServedByValue(buybackData?.subType)
        webEngageAttr[EventAttrs.TAG] = BuybackEventLocation.EXCHANGE_REQUEST
        logWebEnageEvent(EventNames.CANCEL_REQUEST,webEngageAttr)

        APIHelper.cancelRequest(buybackData?.quoteId, props.customerId, (response, error) => {
            stopBlockingLoader();
            if (response) {
                setTimeout(onCancel,200)
            } else {
                setTimeout(()=>{alert(error[0]?.errorMessage ?? TECHNICAL_ERROR_MESSAGE)},200)
            }
        });
    }


    return (
        <View style={{flex: 1}}>
            <BlockingLoader visible={isBlockingLoader} loadingMessage={blockingLoaderMessage}/>
            <View style={styles.buttonStrip}/>
            <Text style={[styles.texts, styles.title]}>{NOT_FOR_EXCHANGE}</Text>
            <Text style={[styles.texts, styles.description]}>{SCHEDULE_PICKUP_DOORSTEP}</Text>
            <Text style={[styles.texts, styles.resalePrice]}>{RESALE_PRICE}</Text>
            <Text style={[styles.texts, styles.price]}>{`₹ ${buybackData?.price}`}</Text>
            <Text
                style={[styles.texts, styles.priceExpires]}>{PRICE_EXP_IN(daysDifference(getCurretDateMilliseconds(), buybackData?.expireOn ?? getCurretDateMilliseconds()))}</Text>
            <View style={styles.primaryBtnContainer}>
                <ButtonWithLoader
                    isLoading={false}
                    enable={true}
                    Button={{
                        text: SCHEDULE_PICKUP,
                        onClick: () => {
                            onSchedulePickup();
                        },
                    }}
                />
                <View style={styles.cancelBtn}>
                    <TouchableOpacity onPress={() => {
                        startBlockingLoader('Please wait...');
                        cancelBuybackRequest()
                    }}>
                        <Text style={styles.cancelBtnText}>{CANCEL_REQUEST}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    buttonStrip: {
        width: '11%',
        height: spacing.spacing4,
        backgroundColor: colors.lightGrey2,
        marginTop: spacing.spacing16,
        alignSelf: 'center',
    },
    texts: {
        marginHorizontal: spacing.spacing24,
        textAlign: 'center',
        marginTop: spacing.spacing8,
    },
    title: {
        ...TextStyle.text_20_bold,
        color: colors.charcoalGrey,
        marginTop: spacing.spacing24,
    },
    description: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        marginTop: 8,
    },
    resalePrice: {
        ...TextStyle.text_14_bold,
        color: colors.seaGreen,
    },
    price: {
        ...TextStyle.text_24_bold,
        color: colors.seaGreen,
    },
    priceExpires: {
        ...TextStyle.text_14_normal,
        color: colors.grey,
    },
    primaryBtnContainer: {
        marginTop: spacing.spacing24,
        marginHorizontal: spacing.spacing16,
    },
    cancelBtn: {
        marginTop: spacing.spacing16,
        height: 48,
        justifyContent: 'center',
    },
    cancelBtnText: {
        textAlign: 'center',
        color: colors.blue028,
    },
});

export default SchedulePickupBottomSheet;

