import React, {Component} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {TextStyle} from '../../Constants/CommonStyle';
import spacing from '../../Constants/Spacing';
import colors from '../../Constants/colors';
import ButtonWithLoader from '../../CommonComponents/ButtonWithLoader';
import ImageCollectionAlert from './ImageCollectionAlert';

let infoIcon = null;

export default class SingleAndMultiChoiceQuestion extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isAnswerSelected: props?.data?.data?.filter(e => e.isSelected)?.length > 0,
        };
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderItem(this.props.data)}
            </View>
        );
    }

    onClickMultiChoiceItem = (data, answersIndex, index) => {
        this.props.onClickMultiChoiceItem(data, answersIndex, index);
    };

    onClick = (data, childindex, index, questionActionType) => {
        // answersData
        this.props.onClick(data, childindex, index, questionActionType);
    };


    getAnswerItem = (item, index, isSingleLineQuestion) => {
        let imageView = !(item.imgUrl === null || item.imgUrl === undefined || item.imgUrl.length == 0) ?
            <Image style={styles.answerItemImageStyle} source={{uri: item.imgUrl}}/> : null;
        let descriptionView = !(item.description === null || item.description === undefined || item.imgUrl.description == 0) ?
            <Text style={[TextStyle.text_12_normal, this.getAnswerDescriptionStyle(item)]}
                  numberOfLines={1}>{item.description}</Text> : null;
        return <TouchableOpacity style={this.getAnswerBackgroundStyle(item, isSingleLineQuestion)} onPress={() => {
            let dataSource = this.props.data;
            if (dataSource?.isSingleChoice) {
                for (let indexItem = 0; indexItem < dataSource?.data?.length; indexItem++) {
                    dataSource.data[indexItem].isSelected = false;
                }
                dataSource.data[index].isSelected = true;
                this.onClick(dataSource?.data, index, this.props.index);
                this.setState({isAnswerSelected: true});
            } else {
                dataSource.data[index].isSelected = !dataSource?.data[index]?.isSelected;
                let shouldDisable = false;
                for (let indexItem = 0; indexItem < dataSource?.data?.length; indexItem++) {
                    if (dataSource?.data[indexItem]?.isSelected) {
                        shouldDisable = true;
                    }
                    if (dataSource?.data[index].answerID === 'OPT_NONE' && dataSource?.data[index].isSelected === true && dataSource?.data[indexItem].answerID !== 'OPT_NONE') {
                        dataSource.data[indexItem].isSelected = false;
                    }
                    if (dataSource?.data[index].answerID !== 'OPT_NONE' && dataSource?.data[index].isSelected === true && dataSource?.data[indexItem].answerID === 'OPT_NONE') {
                        dataSource.data[indexItem].isSelected = false;
                    }
                }
                this.setState({isAnswerSelected: shouldDisable});
                this.onClick(dataSource.data, index, this.props.index, 'MULTIPLE_CHOICE');
            }

            // if (index == (dataSource.length-1)) this.props.data.navigation.navigate("SchedulePickup", {});
        }}>
            {imageView}
            <Text style={[TextStyle.text_14_normal, this.getAnswerTitleStyle(item)]}
                  numberOfLines={1}>{item.answer}</Text>
            {descriptionView}
        </TouchableOpacity>;
    };

    getAnswerBackgroundStyle = (item, isSingleLineQuestion) => {
        return [isSingleLineQuestion ? styles.answerItemSingleLineStyle : styles.answerItemStyle, item.isSelected ? {borderColor: colors.blue} : {borderColor: colors.greye0},
            item.isSelected && this.props.data.isAnswerTypeText ? {backgroundColor: colors.blue} : {backgroundColor: colors.white}];
    };

    getAnswerTitleStyle = (item) => {
        if (!item.isSelected) {
            return {color: colors.charcoalGrey};
        } else {
            if (this.props.data.isAnswerTypeText) {
                return {color: colors.white};
            }
            return {color: colors.blue};
        }
    };
    getAnswerDescriptionStyle = (item) => {
        if (!item.isSelected) {
            return {color: colors.grey};
        } else {
            if (this.props.data.isAnswerTypeText) {
                return {color: colors.white};
            }
            return {color: colors.blue};
        }
    };

    showImageCollection = () => {
        this.props.showImageCollection(this.props.data.data, this.props.index);
    }

    renderItem = (data, index) => {
        let isSelectedData = data.data.filter(data => (data.isSelected));

        let answerView = [];
        let totalItems = data.data.length;
        if (data.isSingleLineQuestion) {
            for (let sectionIndex = 0; sectionIndex < totalItems; sectionIndex++) {
                let item = data.data[sectionIndex];
                let view = this.getAnswerItem(item, sectionIndex, data.isSingleLineQuestion);
                answerView.push(
                    <View key={sectionIndex} style={{flexDirection: 'row'}}>
                        {view}
                    </View>);
            }
        } else {
            for (let sectionIndex = 0; sectionIndex < totalItems / 2; sectionIndex++) {
                let firstItemIndex = 2 * sectionIndex;
                let item = data.data[firstItemIndex];
                let view1 = this.getAnswerItem(item, firstItemIndex, data.isSingleLineQuestion);

                let view2 = null;
                if ((2 * sectionIndex + 1) < totalItems) {
                    let secondItemIndex = 2 * sectionIndex + 1;
                    let anotherItem = data.data[secondItemIndex];
                    view2 = this.getAnswerItem(anotherItem, secondItemIndex, data.isSingleLineQuestion);
                } else {
                    view2 = <View style={[styles.answerItemStyle, {
                        flex: 0.5,
                        borderColor: colors.white,
                        backgroundColor: colors.white,
                    }]}></View>;
                }
                answerView.push(
                    <View key={sectionIndex} style={{flexDirection: 'row'}}>
                        {view1}
                        {view2}
                    </View>);
            }
        }

        let buttonStyle;
        let buttonDisable;
        if (this.state.isAnswerSelected) {
            buttonDisable = false;
            buttonStyle = {backgroundColor: colors.blue};
        } else {
            buttonDisable = true;
            buttonStyle = {backgroundColor: colors.lightGrey};
        }
        let buttonView;
        if (this.props.isFinalQuestion && data.isSingleChoice) {
            buttonView = <View style={styles.buttonStyle}>
                <ButtonWithLoader
                    isLoading={this.props.isLoading}
                    enable={this.props.submitEnabled && (this.state.isAnswerSelected || isSelectedData.length > 0)}
                    Button={{
                        text: 'Continue',
                        onClick: () => {
                            this.props.onSubmitQuestions();
                        },
                    }
                    }/>
            </View>;
        } else if (!data.isSingleChoice) {
            buttonView = <View style={styles.buttonStyle}>
                <ButtonWithLoader
                    isLoading={this.props.isLoading}
                    enable={this.props.isFinalQuestion ? this.props.submitEnabled : this.state.isAnswerSelected}
                    Button={{
                        text: this.props.isFinalQuestion ? 'Continue' : 'Next',
                        onClick: () => {
                            let answersIndex = [];
                            for (let i = 0; i < data.data.length; i++) {
                                if (data.data[i].isSelected) {
                                    answersIndex.push(i);
                                }
                            }
                            if (this.props.isFinalQuestion) {
                                this.onClickMultiChoiceItem(this.props.data.data, answersIndex, this.props.index);
                                this.props.onSubmitQuestions();
                            } else {
                                this.onClickMultiChoiceItem(this.props.data.data, answersIndex, this.props.index);
                            }

                        },

                    }
                    }/>
            </View>;
        }
        let hintButton = null;
        if (this.props.data?.showHint) {
            hintButton = <TouchableOpacity
                onPress={() => this.showImageCollection()}
                ref={(ref) => {
                    infoIcon = ref;
                }}
                style={styles.infoIconContainer}>
                <Text style={[TextStyle.text_10_bold, {color: colors.white}]}>i</Text>
            </TouchableOpacity>;
        }
        return <View ref={ref => (this.parentRef = ref)}>
            <View style={{flexDirection: 'row'}}>
                {/*TODO: send index as props from the flat lst*/}
                <Text style={[TextStyle.text_14_bold, styles.questionNumberStyle]}>{this.props.index + 1}</Text>
                <Text style={[TextStyle.text_16_bold, styles.questionTextStyle]}>{data.title}</Text>
                {hintButton}
            </View>
            <Text style={[TextStyle.text_12_normal, styles.questionSubTextStyle]}>{data.subTitle}</Text>
            <View style={{marginLeft: spacing.spacing24}}>
                {answerView}
            </View>
            {buttonView}
        </View>;
    };
}

const
    styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: '#fff',
            paddingBottom: 60,
        },
        questionNumberStyle: {
            width: 24,
            height: 24,
            paddingTop: 2,
            borderRadius: 12,
            backgroundColor: colors.blue,
            color: colors.white,
            textAlign: 'center',
            overflow: 'hidden',
        },
        questionTextStyle: {
            marginLeft: spacing.spacing16,
            marginBottom: spacing.spacing24,
        },
        questionSubTextStyle: {
            marginLeft: spacing.spacing40,
            marginBottom: spacing.spacing8,
        },
        answerItemStyle: {
            padding: spacing.spacing12,
            borderRadius: 4,
            borderWidth: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginBottom: spacing.spacing20,
            marginLeft: spacing.spacing16,
            flex: 0.5,
        },
        answerItemSingleLineStyle: {
            padding: spacing.spacing12,
            borderRadius: 4,
            borderWidth: 1,
            marginBottom: spacing.spacing20,
            marginLeft: spacing.spacing16,
            flex: 1,
        },
        answerItemImageStyle: {
            width: 20,
            height: 30,
            marginBottom: spacing.spacing12,
            resizeMode: 'contain',
        },
        buttonStyle: {
            height: spacing.spacing48,
            marginLeft: spacing.spacing36,
            marginBottom: spacing.spacing16,
        },
        infoIconContainer: {
            marginTop: spacing.spacing4,
            width: 18,
            height: 18,
            borderRadius: 9,
            borderWidth: 2,
            backgroundColor: colors.grey,
            borderColor: colors.grey,
            alignItems: 'center',
            justifyContent: 'center',
            paddingBottom: spacing.spacing2,
            marginLeft: spacing.spacing12,
        },
    });
