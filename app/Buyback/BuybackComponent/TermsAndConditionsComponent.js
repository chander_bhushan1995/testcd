import React, {Component} from "react";
import {
    Text,
    View,
    StyleSheet,
    Platform,
    NativeModules, SafeAreaView,
} from "react-native";
import WebView from 'react-native-webview';
import {TextStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";

import {HeaderBackButton} from "react-navigation";
import Loader from "../../Components/Loader";
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

const nativeBrigeRef = NativeModules.ChatBridge;
export default class TermsAndConditionsComponent extends Component {

    static navigationOptions = ({navigation}) => {

        return {
            headerTitle: <View style={Platform.OS === "ios" ? {alignItems: "center"} : {alignItems: "flex-start"}}>
                <Text style={TextStyle.text_16_normal}>Terms and Conditions</Text>
            </View>,
            headerLeft: <HeaderBackButton tintColor={colors.black}
                                          onPress={() => navigation.pop()}/>,
            headerTitleStyle: {color: colors.white},
            headerTintColor: colors.white
        };
    };

    constructor(props) {
        super(props);
        this.state={tncUrlFromFirebase: "", isLoading: true};
    }

    onBackPress = () => {
        this.props.navigation.pop();
    };

    componentWillUnmount() {
    }

    onWebviewLoad(){
        this.setState({isLoading:false});
    }

    componentDidMount() {
        nativeBrigeRef.getBuybackTAndCUrl(tncUrl => {
            let tncUrlObj = parseJSON(tncUrl);
            this.setState({tncUrlFromFirebase:tncUrlObj.buyback_tnc});
        });
    }

        render() {
                let view = null;
                let webView = <WebView style={styles.webviewStyle} source={{uri: this.state.tncUrlFromFirebase}} onLoad= {()=>this.onWebviewLoad()}/>;
                let loader = <Loader isLoading={this.state.isLoading} size={40} color={colors.blue028}/>;
                if (this.state.tncUrlFromFirebase !== "") {
                    if (this.state.isLoading) {
                        view = <View>
                            {webView}
                            {this.state.isLoading ? loader : null}
                        </View>;
                    } else {
                    view = webView;
                    }
                } else {
                    view = loader;
                }
                return (
                    <SafeAreaView style={styles.SafeArea}>
                        {view}
                    </SafeAreaView>
                );
        }
    }

    const styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: colors.white,
            padding: spacing.spacing16,
        },
        crossIconStyle: {
            height: 16,
            width: 16,
            resizeMode: "contain",
            alignSelf: 'flex-end',
        },
        SafeArea: {
            flex: 1,
            backgroundColor: colors.white
        },
        webviewStyle: {
            flex: 1,
            backgroundColor: colors.white
        },
    });
