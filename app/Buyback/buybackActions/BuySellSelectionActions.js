import {connect} from 'react-redux';
import BuySellSelectionScreen from '../BuybackComponent/BuySellSelectionScreen';
import {RESET_QUESTIONS} from './Constants';

const mapStateToProps = state => ({
    data: state.BuySellSelectionReducer.data,
});

const mapDispatchToProps = dispatch => ({

    resetQuestionsData: () => {
        dispatch({type: RESET_QUESTIONS});
    },
});
export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(BuySellSelectionScreen);
