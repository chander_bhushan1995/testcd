import {
    PAYMENT,
    SAVE_QUESTIONS,
    GET_ACCOUNT_STATUS,
    CONTACT_CREATION,
    GET_RETAILERS_LIST,
    GET_STORES_LIST,
    RATING_SUBMIT,
    UPDATE_BUYBACK_ORDER,
    CREATE_ORDER,
    NEGATIVE_QUESTIONNAIRE,
    GET_COUPON_CODES,
    APPLY_COUPON,
    EXTRACT_PRICE,
    UPDATE_ONGOING_BUYBACK,
    CHECK_PINCODE_SERVICEABILITY,
    GET_BUYBACK,
    GET_CUSTOMER_DETAILS,
} from '../../Constants/ApiUrls';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import {APIData} from '../../../index';
import {QUESTION_LIST} from '../../Constants/ApiUrls';
import { parseJSON, deepCopy, getCDNDataVersioning} from '../../commonUtil/AppUtils';
import {executeApi, HTTP_GET, HTTP_POST} from '../../network/ApiManager';

export const getQuestionsListFromRemote = (callback) => {
    getCDNDataVersioning((data) => {
        let url = APIData.cdn_base_url + QUESTION_LIST;
        if (data.buybackQuestionsData) {
            url += '?' + data.buybackQuestionsData;
        }
        let apiParams = {
            httpType: HTTP_GET,
            url: url,
            callback: callback,
        };
        apiParams.apiProps = {baseUrl: '', header: APIData?.apiHeader};
        executeApi(apiParams).done();
    });
};

const getDataFromOA = (apiParams) => {
    let params = APIData;
    apiParams.apiProps = {baseUrl: params?.api_base_url, header: params?.apiHeader};
    return apiParams;
};

const getDataFromApiGateway = (apiParams) => {
    let params = APIData;
    apiParams.apiProps = {baseUrl: params?.api_gateway_base_url, header: params?.apiHeader};
    return apiParams;
};

export const saveAnswersForQuestions = (quoteId, data, callback) => {
    let params = APIData;
    data = {...data, quoteId: quoteId, createdBy: params?.customer_id, createdByType: 'CUSTOMER'};
    let apiParams = {
        httpType: HTTP_POST,
        url: SAVE_QUESTIONS,
        requestBody: data,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const savePayment = (body, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: PAYMENT + body?.quoteId + '/paymentDetails',
        requestBody: body,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const contactCreationApi = (bankAccountDetails, callback) => {
    let params = APIData;
    const userInfo = parseJSON(buybackManagerObj.getUserInfo());
    bankAccountDetails.action = "ADD" //it will always add for app end
    let data = {
        userDetails: {
            userId: params?.customer_id,
            name: userInfo?.userName,
            email: userInfo?.userEmail,
            contact: params?.mobile_number,
        },
        bankAccountDetails: bankAccountDetails,
        'verify': true,
    };
    let apiParams = {
        httpType: HTTP_POST,
        url: CONTACT_CREATION,
        requestBody: data,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getAccountStatusApi = (fundId, callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: GET_ACCOUNT_STATUS + fundId,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getAddressDetailsApi = (customerId, callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: GET_CUSTOMER_DETAILS(customerId),
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getRetailersList = (quoteId,lat, long, callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: GET_RETAILERS_LIST(lat, long, quoteId),
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getStoresList = (lat, long, callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: GET_STORES_LIST(lat, long, "1053,1054"),
        callback: callback,
    };
    executeApi(getDataFromOA(apiParams)).done();
};

export const createOrderWithRetailer = (quoteId, servicePartnerCode, servicePartnerBuCode, callback) => {
    let params = APIData;
    let data = {quoteId: quoteId, servicePartnerCode: servicePartnerCode ?? 0, servicePartnerBuCode: servicePartnerBuCode ?? 0, initiatingSystem: params?.initiatingSystem};
    let apiParams = {
        httpType: HTTP_POST,
        url: CREATE_ORDER,
        requestBody: data,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const submitRating = (quoteId, rating, tags,feedbackComment, callback) => {
    let tagsString = (tags ?? []).join(',');
    let body = {quoteId: quoteId, feedbackRating: rating, feedbackCode: tagsString, feedbackComments: feedbackComment};
    let apiParams = {
        httpType: HTTP_POST,
        url: RATING_SUBMIT,
        requestBody: body,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const cancelRequest = (orderId,status,subStatus,cancelReasonCode,customerId,modifiedByType,callback) => {
    let body;
    if(modifiedByType && modifiedByType.length > 0){
        body = {orderId: orderId, stage: status, stageStatus: subStatus,cancelReasonCode: cancelReasonCode,modifiedBy: customerId, modifiedByType:modifiedByType};
    }else{
        body = {orderId: orderId, stage: status, stageStatus: subStatus,cancelReasonCode: cancelReasonCode,modifiedBy: customerId};
    }
    let apiParams = {
        httpType: HTTP_POST,
        url: UPDATE_BUYBACK_ORDER,
        requestBody: body,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
}

export const checkPincodeAvailability = (pincode, callback) => {
    let params = APIData;
    let location = buybackManagerObj.getLocation();
    let data = {
        customerId: params?.customer_id,
        pincode: pincode,
        membershipId: buybackManagerObj.getMembershipId() ? buybackManagerObj.getMembershipId() : '',
        deviceDetail: {
            model: params?.modelName,
            ram: params?.RAM,
            storage: params?.ROM,
            serialNumber: buybackManagerObj.getImei(),
            identifier: params?.deviceIdentifier,
        },
        // latitude: 28.473333,
        // longitude: 77.055317,
        latitude: location?.lat,
        longitude: location?.lng,
    };
    let apiParams = {
        httpType: HTTP_POST,
        url: PAYMENT,
        requestBody: data,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getBuybackStatusData = (callback) => {
    let params = APIData;
    let apiParams = {
        httpType: HTTP_GET,
        url: GET_BUYBACK(params?.customer_id, params?.modelName, params?.RAM, params?.ROM, params?.deviceIdentifier),
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getQuoteStateApi = (quoteId, callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: PAYMENT + quoteId,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getBuybackFeedbackTags = (rating,callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: NEGATIVE_QUESTIONNAIRE + rating,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
}

export const getCouponCodes = (quoteId,callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: GET_COUPON_CODES + quoteId,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
}

export const applyCouponCode = (quoteId, couponCode, callback) => {
    let data = {
        couponCode: couponCode,
    };
    let apiParams = {
        httpType: HTTP_POST,
        url: APPLY_COUPON + quoteId,
        requestBody: data,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getExactPrice = (quoteId, callback) => {
    let params = APIData;
    let body = {
        quoteId: quoteId,
        createdBy: params?.customer_id,
        createdByType: 'CUSTOMER',
    };
    let apiParams = {
        httpType: HTTP_POST,
        url: EXTRACT_PRICE,
        requestBody: body,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const cancelOnGoingRequest = (quoteId,customerId,callback) => {
    let body = {quoteId: quoteId, modifiedBy: customerId, status:"EXPIRED"};
    let apiParams = {
        httpType: HTTP_POST,
        url: UPDATE_ONGOING_BUYBACK(quoteId),
        requestBody: body,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
}

export const checkPincodeServiceability = (quoteId, pincode, callback) => {
    const userLocation = buybackManagerObj.getLocation() ?? {}
    let apiParams = {
        httpType: HTTP_GET,
        url: CHECK_PINCODE_SERVICEABILITY(userLocation?.lat ?? 0,userLocation?.lng ?? 0, pincode, quoteId),
        requestBody: {},
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const updatePincodeApi = (quoteId,customerId,pincode,callback) => {
    let body = {quoteId: quoteId, modifiedBy: customerId, pincode: pincode};
    let apiParams = {
        httpType: HTTP_POST,
        url: UPDATE_ONGOING_BUYBACK(quoteId),
        requestBody: body,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
}
