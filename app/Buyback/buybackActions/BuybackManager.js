export default class BuybackManager {

    static myInstance = null;
    _questionsList = null;
    _isABBFlow = false;
    _membershipId = '';
    _imei = '';
    _address = '';
    _isABBFlowFailOnce = false;
    _buybackStatusData = [];
    _location = null;
    _userInfo = null;
    _userName = null;
    _abbNotEligible  = false;
    /**
     * @returns {BuybackManager}
     */
    static getInstance() {
        if (BuybackManager.myInstance === null) {
            BuybackManager.myInstance = new BuybackManager();
        }

        return this.myInstance;
    }

    setQuestionsList(data) {
        this._questionsList = data;
    }

    getQuestionsList() {
        return this._questionsList;
    }

    setIsABBFlow(isABBFlow) {
        this._isABBFlow = isABBFlow;
    }

    isABBFlow() {
        return this._isABBFlow;
    }

    setMembershipId(membershipId) {
        this._membershipId = membershipId;
    }

    getMembershipId() {
        return this._membershipId;
    }

    setImei(imei) {
        this._imei = imei;
    }

    getImei() {
        return this._imei;
    }

    setAddress(address) {
        this._address = address;
    }

    setUserName(name) {
        this._userName = name
    }

    getAddress() {
        return this._address;
    }

    getUserName() {
        return this._userName;
    }

    setIsABBFlowFailOnce(isABBFlowFailOnce) {
        this._isABBFlowFailOnce = isABBFlowFailOnce;
    }

    isABBFlowFailOnce() {
        return this._isABBFlowFailOnce;
    }

    setBuybackStatusData(buybackStatusData) {
        this._buybackStatusData = buybackStatusData;
    }

    getBuybackStatusData() {
        return this._buybackStatusData;
    }

    getBuybackStatusDataFirstItem() {
        return this._buybackStatusData[0];
    }

    setBuybackStatusDataFirstItem(buybackStatusData) {
        this._buybackStatusData[0] = buybackStatusData;
    }

    getLocation() {
        return this._location;
    }

    setLocation(location) {
        this._location = location;
    }

    getUserInfo() {
        return this._userInfo;
    }

    setUserInfo(userInfo) {
        this._userInfo = userInfo;
    }

    getABBNotEligible() {
        return this._abbNotEligible;
    }

    setABBNotEligible(abbNotEligible) {
        this._abbNotEligible = abbNotEligible;
    }

}
