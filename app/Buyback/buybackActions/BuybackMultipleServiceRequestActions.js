import {connect} from "react-redux";
import BuybackMultipleServiceRequestComponent from "../BuybackComponent/BuybackMultipleServiceRequestComponent";
import {BUYBACK_STATUS_DATA_LIST} from "./Constants";

const mapStateToProps = state => ({
    buybackStatusList: state.BuybackMultipleServiceRequestReducer.buybackStatusList
});
const mapDispatchToProps = dispatch => ({
    getBuybackStatusList: () => {
        dispatch({type: BUYBACK_STATUS_DATA_LIST});
    },
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BuybackMultipleServiceRequestComponent);