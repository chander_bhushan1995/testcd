import {connect} from "react-redux";
import BuybackRequestSuccessTimelineComponent from "../BuybackComponent/BuybackRequestSuccessTimelineComponent";
import {API_ERROR_SUCCESS_TIMELINE, ORDER_STATUS_DATA} from "./Constants";
import {GET_ORDER_STATUS} from "../../Constants/ApiUrls";
import {makeGetRequest} from '../../network/ApiManager';
import {buybackManagerObj} from '../../Navigation/index.buyback';

const mapStateToProps = state => ({
    orderStatus: state.BuybackRequestSuccessTimelineReducer.orderStatus,
    isLoading:state.BuybackRequestSuccessTimelineReducer.isLoading,
    isError:state.BuybackRequestSuccessTimelineReducer.isError,
    errorMessage:state.BuybackRequestSuccessTimelineReducer.errorMessage,
    errorMessageButtonTitle:state.BuybackRequestSuccessTimelineReducer.errorMessageButtonTitle,
    routeList:state.nav.routes,
});
const mapDispatchToProps = dispatch => ({
    getOrderStatus: (quoteId) => {
        makeGetRequest(GET_ORDER_STATUS + quoteId, (response, error) => {
                if (response) {
                    let data = buybackManagerObj.getBuybackStatusDataFirstItem();
                    data.subType = response.data.subType;
                    buybackManagerObj.setBuybackStatusDataFirstItem(data);
                    dispatch({type: ORDER_STATUS_DATA, data: response.data});
                } else {
                    dispatch({type: API_ERROR_SUCCESS_TIMELINE, data: error});
                }

        });
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BuybackRequestSuccessTimelineComponent);
