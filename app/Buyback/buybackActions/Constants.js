export const WorkFlowStage = {
    OG: "OG",
    OC: "OC",
    DSPT: "DSPT",
    CP: "CP",
    PUS: "PUS",
    SDP: "SDP",
    SPU: "SPU",
    DPC: "DPC",
    DR: "DR",
    IMEIC: "IMEIC",
    CC: "CC",
    RC: "RC"
}
export const REASON_CODE_CWOS = "CWOS";

export const PINCODE_SERVICEABLITY_RESPONSE = "pincode_serviceablity_response";
export const API_ERROR = 'api_error';
export const API_ERROR_SUCCESS_TIMELINE = 'api_error_success_timeline';
export const API_ERROR_PINCODE = 'api_error_pincode';
export const API_ERROR_QUESTION_ANSWER = 'api_error_question_answer';
export const BUY_BACK_STATUS_DATA = 'buy_back_status_data';
export const STATUS_DEFAULT_DATA = 'status_default_data';
export const MHC_QUESTIONS_SAVE = 'mhc_questions_save';
export const QUOTE_QUESTIONS_SAVE = 'quote_questions_save';
export const ORDER_QUESTIONS_SAVE = 'order_questions_save';
export const MOBILE_BRANDS_DATA = 'mobile_brands_data';
export const MOBILE_MODELS_DATA = 'mobile_models_data';
export const BUYBACK_STATUS_DATA = 'buyback_status_data';
export const BUYBACK_STATUS_DATA_LIST = 'buyback_status_data_list';
export const BUYBACK_STATUS_DECIDER_DATA_LIST = 'buyback_status_decider_data_list';
export const CREATE_ORDER_DATA = 'create_order_data';
export const SAVE_RATING_DATA = 'save_rating_data';
export const NEGATIVE_RATING_DATA = 'negative_rating_data';
export const RATING_SUBMIT_OR_CANCEL = 'rating_submit_or_cancel';
export const SAVE_RATING_ERROR = 'save_rating_error';
export const CREATE_ORDER_DATA_ERROR = 'create_order_data_error';
export const CREATE_ORDER_API_INIT = 'create_order_api_init';
export const SAVE_RATING_API_INIT = 'save_rating_api_init';
export const APPLY_COUPON_DATA = 'apply_coupon_data';
export const APPLY_COUPON_INIT = 'apply_coupon_init';
export const APPLY_COUPON_DATA_ERROR = 'apply_coupon_data_error';
export const EXTRACT_PRICE_API_ERROR = 'extract_price_api_error';
export const ABB_REQUEST_CANCEL = 'abb_request_cancel';
export const APPLY_COUPON_CODE_DATA_ERROR = 'apply_coupon_code_data_error';

export const DEVICE_INFO = 'device_info';

export const API_CALL_INIT = 'api_call_init';
export const API_CALL_INIT_QUESTION_AND_ANSWERS = 'api_call_init_question_and_answers';

export const UPDATE_QUESTIONS = 'update_questions';
export const ADD_QUESTIONS = 'add_questions';
export const INITIAL_QUOTE_QUESTIONS = 'initial_quote_questions';
export const INITIAL_QUOTE_QUESTIONS_FROM_PRICE = 'initial_quote_questions_from_price';
export const RESET_FINAL_PRICE_DATA = 'reset_final_price_data';
export const INITIAL_ORDER_QUESTIONS = 'initial_order_questions';
export const RESET_QUESTIONS = 'reset_questions';
export const RESET_BUYBACK_DATA = 'reset_buyback_data';
export const RESET_PIN_CODE_ERROR = 'reset_pin_code_error';
export const RESET_BUYBACK_DEEPLINK_TYPE = 'reset_buyback_deeplink_type';
export const DISMISS_ERROR_DAILOG = 'dismiss_error_dailog';
export const DISMISS_LOADER = 'DISMISS_LOADER'
export const SAVE_QUESTIONS_SUCCESS = 'save_questions_success';
export const SAVE_COMPLETE_QUOTE_QUESTIONS_DATA = 'save_complete_quote_questions_data';
export const SET_QUOTE_QUESTION_DEFAULT_LOADING = 'set_quote_question_default_loading';

export const EXTRACT_PRICE_API_CALL_INIT = 'extract_price_api_call_init';
export const RESET_NOT_ELIGIBLE = 'reset_not_eligible';
export const GET_PRICE = 'get_price';
export const GET_COUPON_CODES = 'get_coupon_codes';
export const ORDER_STATUS_DATA = 'order_status_data';
export const PAYMENT_MODE_IMPS = 'IMPS';
export const PAYMENT_MODE_PAYTM = 'Paytm';
export const SUGGESTION_VIEW_MARGIN_FROM_BOTTOM = 80;
export const DEFAULT_PAUSE_TIME_FOR_SUGGESTIONS = 200;

// SEND_VIEW types
export const SEND_VIEW_TAGS = 'SEND_VIEW_TAGS';

export const PINCODE_QUESTION = {
    "questionId": "Q_PINCODE",
    "isCompleted": false,
    "questionFormat": "TEXT",
    "questionType": "ORDER_QUESTION"
}
