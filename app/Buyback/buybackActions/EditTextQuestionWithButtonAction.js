import {connect} from "react-redux";
import EditTextQuestionWithButton from "../BuybackComponent/EditTextQuestionWithButton";
import {API_ERROR_QUESTION_ANSWER} from "./Constants";

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({

    dispatchError: (error) => {
        dispatch({type: API_ERROR_QUESTION_ANSWER, data: error});
    },
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditTextQuestionWithButton);