import {connect} from "react-redux";
import EnterBankDetailsComponent from "../BuybackComponent/EnterBankDetailsComponent";

const mapStateToProps = state => ({
    data: state.EnterBankDetailsReducer.data,
});

const mapDispatchToProps = dispatch => ({

    selectMobileBrand: (data) => {
        dispatch({type:"update",data:data});
    },
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EnterBankDetailsComponent);