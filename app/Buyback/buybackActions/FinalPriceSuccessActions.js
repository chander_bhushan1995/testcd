import {connect} from 'react-redux';
import {NativeModules} from 'react-native';
import FinalPriceSuccessComponent from '../BuybackComponent/FinalPriceSuccessComponent';
import {
    ABB_REQUEST_CANCEL,
    APPLY_COUPON_CODE_DATA_ERROR, APPLY_COUPON_DATA, APPLY_COUPON_DATA_ERROR, APPLY_COUPON_INIT,
    EXTRACT_PRICE_API_CALL_INIT, EXTRACT_PRICE_API_ERROR,
    GET_PRICE,
    INITIAL_ORDER_QUESTIONS, INITIAL_QUOTE_QUESTIONS_FROM_PRICE, RESET_FINAL_PRICE_DATA, RESET_NOT_ELIGIBLE,
} from './Constants';
import {GET_COUPON_CODES} from '../../Constants/ApiUrls';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import {buybackStatusConstants} from '../../Constants/BuybackConstants';
import {isThirdPartyFlow} from '../BuyBackUtils/BuybackUtils';
import {applyCouponCode, getCouponCodes, getExactPrice} from './BuybackApiHelper';

const chatBridgeRef = NativeModules.ChatBridge;
const mapStateToProps = state => ({
    data: state.FinalPriceSuccessReducer.data,
    price: state.FinalPriceSuccessReducer.price,
    couponPrice: state.FinalPriceSuccessReducer.couponPrice,
    isLoading: state.FinalPriceSuccessReducer.isLoading,
    isError: state.FinalPriceSuccessReducer.isError,
    isCouponError: state.FinalPriceSuccessReducer.isCouponError,
    errorMessage: state.FinalPriceSuccessReducer.errorMessage,
    errorType: state.FinalPriceSuccessReducer.errorType,
    couponCodeInfo: state.FinalPriceSuccessReducer.couponCodeInfo,
    isCouponApplied: state.FinalPriceSuccessReducer.isCouponApplied,
    isCouponApplying: state.FinalPriceSuccessReducer.isCouponApplying,
    errorMessageButtonTitle: state.FinalPriceSuccessReducer.errorMessageButtonTitle,
    stepsList: state.PincodeReducer.stepsList,
    subType: state.FinalPriceSuccessReducer.subType,
    ABBNotEligible: state.FinalPriceSuccessReducer.ABBNotEligible,
});

const mapDispatchToProps = dispatch => ({

    resetData: () => {
        dispatch({type: RESET_FINAL_PRICE_DATA});
    },

    getPrice: (quoteId) => {
        dispatch({type: EXTRACT_PRICE_API_CALL_INIT});
        executeGetPriceApi(quoteId, dispatch);
    },
    setOrderQuestions: (data) => {
        chatBridgeRef.getUserInfo(userInfo => {
            dispatch({type: INITIAL_ORDER_QUESTIONS, data: data, userInfo: userInfo});
        });
    },
    setQuoteQuestions: (data) => {
        dispatch({type: INITIAL_QUOTE_QUESTIONS_FROM_PRICE, data: data});
    },
    applyCoupon: (quoteId, couponCode) => {
        dispatch({type: APPLY_COUPON_INIT});
        applyCouponCode(quoteId, couponCode,(response, error) => {
            if (response) {
                let data = buybackManagerObj.getBuybackStatusDataFirstItem();
                let priceAfterApplyCoupon = parseInt(data?.price, 10) + parseInt(response.data.couponPrice, 10);
                data.price = priceAfterApplyCoupon;
                data.couponCodeInfo = {
                    couponCode: couponCode,
                    couponPrice: response.data.couponPrice,
                    isApplied: true,
                };
                buybackManagerObj.setBuybackStatusDataFirstItem(data);
                dispatch({type: APPLY_COUPON_DATA, priceAfterApplyCoupon: priceAfterApplyCoupon});
            } else {
                dispatch({type: APPLY_COUPON_CODE_DATA_ERROR, data: error});
            }
        });
    },
    resetNotEligibleCondition: () => {
        dispatch({type: RESET_NOT_ELIGIBLE});
    },
});

function getCouponCode(quoteId, dispatch) {
    getCouponCodes(quoteId, (response, error) => {
        if (response) {
            dispatch({type: GET_COUPON_CODES, couponCodes: response.data.couponCodes});
        } else {
            dispatch({type: APPLY_COUPON_DATA_ERROR, data: error});
        }
    });
}

function executeGetPriceApi(quoteId, dispatch) {
    getExactPrice(quoteId, (response, error) => {
        if (response) {
            let data = buybackManagerObj.getBuybackStatusDataFirstItem();
            data.buyBackStatus = buybackStatusConstants.QUOTE_GENERATED;
            data.price = response.data.price;
            data.subType = response.data.subType;
            data.expireOn = response.data.expireOn;
            buybackManagerObj.setBuybackStatusDataFirstItem(data);
            let abbNotEligible = false;
            if(response.data?.ABBEligible === false){
                abbNotEligible = true;
            }
            buybackManagerObj.setABBNotEligible(abbNotEligible);
            dispatch({type: GET_PRICE, price: response.data.price, subType: response.data.subType, ABBNotEligible: abbNotEligible});
            if (isThirdPartyFlow()) {
                getCouponCode(quoteId, dispatch);
            }
        } else {
            if (error?.error[0].errorCode === 1024) {
                dispatch({type: ABB_REQUEST_CANCEL});
            } else {
                dispatch({type: EXTRACT_PRICE_API_ERROR, data: error});
            }
        }
    });
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(FinalPriceSuccessComponent);
