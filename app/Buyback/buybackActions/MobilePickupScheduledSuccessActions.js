import {connect} from "react-redux";
import MobilePickupScheduledSuccessComponent from "../BuybackComponent/MobilePickupScheduledSuccessComponent";
import {CREATE_ORDER} from "../../Constants/ApiUrls";

import {NativeModules} from "react-native";
import {
    CREATE_ORDER_API_INIT, CREATE_ORDER_DATA, CREATE_ORDER_DATA_ERROR, RATING_SUBMIT_OR_CANCEL,
} from "./Constants";
import {makePostRequest} from "../../network/ApiManager";
import {Urls} from "../../idfence/constants/Urls";
import {FAIL_KEY, SUCCESS_KEY} from "../../idfence/dashboard/screens/IDFenceApiCalls";
import {APIData} from '../../../index';

const chatBridgeRef = NativeModules.ChatBridge;

const mapStateToProps = state => ({
    orderID: state.MobilePickupScheduledSuccessReducer.orderID,
    isLoading: state.MobilePickupScheduledSuccessReducer.isLoading,
    isError: state.MobilePickupScheduledSuccessReducer.isError,
    errorMessage: state.MobilePickupScheduledSuccessReducer.errorMessage,
    ratingData: state.MobilePickupScheduledSuccessReducer.ratingData,
    shouldAddStarsView: state.MobilePickupScheduledSuccessReducer.shouldAddStarsView,
    errorMessageButtonTitle: state.MobilePickupScheduledSuccessReducer.errorMessageButtonTitle,
    showRating: state.MobilePickupScheduledSuccessReducer.showRating,
});

const mapDispatchToProps = dispatch => ({

    executeCreateOrder: (quoteId) => {
        let params = APIData;
        let url = CREATE_ORDER;
        let body = {quoteId: quoteId, initiatingSystem: params?.initiatingSystem};
        dispatch({type: CREATE_ORDER_API_INIT});

        makePostRequest(url, body, (response, error) => {
            if (response) {
                dispatch({type: CREATE_ORDER_DATA, data: response.data});
            } else {
                dispatch({type: CREATE_ORDER_DATA_ERROR, data: error});
            }
        });
    },
    onCancel: () => {
        dispatch({type: RATING_SUBMIT_OR_CANCEL});
    }
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MobilePickupScheduledSuccessComponent);
