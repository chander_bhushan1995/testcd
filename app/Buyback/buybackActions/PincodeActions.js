import { connect } from "react-redux";
import { NativeModules } from "react-native";
import BuybackComponent from "../BuybackComponent/BuybackComponent";
import {
    API_CALL_INIT,
    API_ERROR_PINCODE,
    APPLY_COUPON_CODE_DATA_ERROR,
    APPLY_COUPON_DATA,
    APPLY_COUPON_DATA_ERROR,
    APPLY_COUPON_INIT,
    BUY_BACK_STATUS_DATA,
    DEVICE_INFO,
    MHC_QUESTIONS_SAVE,
    PINCODE_SERVICEABLITY_RESPONSE,
    RESET_BUYBACK_DATA,
    RESET_BUYBACK_DEEPLINK_TYPE,
    STATUS_DEFAULT_DATA,
    DISMISS_ERROR_DAILOG,
    RESET_PIN_CODE_ERROR, DISMISS_LOADER,
} from './Constants';
import {GET_COUPON_CODES} from '../../Constants/ApiUrls';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import { YES, NO, MOBILE_BUYBACK } from "../../Constants/WebengageAttributes";
import { CHECK_PINCODE_AVAILABILITY } from "../../Constants/WebengageEvents";
import {AVERAGE, buybackStatusConstants, EXCELLENT, GOOD, POOR} from '../../Constants/BuybackConstants';
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';
import {parseBuybackQuestion} from '../../Buyback/BuyBackUtils/BuybackQuestionsParser';
import {
    applyCouponCode,
    checkPincodeAvailability,
    getCouponCodes,
    getQuoteStateApi,
    saveAnswersForQuestions,
} from './BuybackApiHelper';
import {ABB, SERVED_BY} from '../../Constants/WebengageAttrKeys';
import {getServedByValueFromFirstItem} from '../BuyBackUtils/BuybackUtils';
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

const nativeBrigeRef = NativeModules.ChatBridge;
const mapStateToProps = state => ({
    pincodeServiceablityData: state.PincodeReducer.pincodeServiceablityData,
    deviceInfo: state.PincodeReducer.deviceInfo,
    data: state.PincodeReducer.data,
    isError: state.PincodeReducer.isError,
    errorMessage: state.PincodeReducer.errorMessage,
    stepsList: state.PincodeReducer.stepsList,
    isLoading: state.PincodeReducer.isLoading,
    errorMessageButtonTitle: state.PincodeReducer.errorMessageButtonTitle,
    errorType: state.PincodeReducer.errorType,
    couponCodeInfo: state.PincodeReducer.couponCodeInfo,
    isCouponApplied: state.PincodeReducer.isCouponApplied,
    isCouponApplying: state.PincodeReducer.isCouponApplying,
    isCouponError: state.PincodeReducer.isCouponError,
    price: state.PincodeReducer.price,
    routeList: state.nav.routes,
    type: state.PincodeReducer.type,
});
const mapDispatchToProps = dispatch => ({

    reset: () => {
        dispatch({ type: RESET_BUYBACK_DATA })
    },
    resetType: () => {
        dispatch({ type: RESET_BUYBACK_DEEPLINK_TYPE })
    },
    resetPincodePopup: () => {
        dispatch({ type: RESET_PIN_CODE_ERROR })
    },
    checkPincodeServiceablity: (pincode, price) => {
        dispatch({ type: API_CALL_INIT });
        checkPincodeAvailability(pincode, (response, error) => {
            dispatch({ type: DISMISS_LOADER });
            if (response) {
                let pincodeAvailable = NO;
                if (response.data.pincodeAvailable || (buybackManagerObj.isABBFlow() && response.data.quoteId)) {
                    pincodeAvailable = YES;
                    let data = buybackManagerObj.getBuybackStatusDataFirstItem();
                    data.quoteId = response.data.quoteId;
                    data.pincode = pincode;
                    data.cityName = response.data.cityName;
                    data.subType = response.data.subType;
                    data.mhcScore = response.data.mhcScore;
                    data.flow = response.data.flow;
                    let brand = data?.deviceInfo?.brand;
                    buybackManagerObj.setBuybackStatusDataFirstItem(data);
                    callGetQuoteStateQuestionsAPI(dispatch, {quoteId: response.data.quoteId, currentItemIndex: "", pincode: pincode,
                        price:data?.price, cityName: response.data.cityName, deviceBrand: brand, mhcScore: response.data.mhcScore});
                    shouldEnableMHC(callback = { onResult: (shouldMHCCheck) => { dispatch({ type: PINCODE_SERVICEABLITY_RESPONSE, data: response.data, pincode: pincode, price: price, cityName: response.data.cityName, mhcScore: response.data.mhcScore, shouldMHCCheck: shouldMHCCheck }); } })
                }
                else {
                    dispatch({ type: PINCODE_SERVICEABLITY_RESPONSE, data: response.data, pincode: pincode, price: price, cityName: response.data.cityName, mhcScore: response.data.mhcScore });
                }
                let eventData = new Object();
                eventData["Location"] = MOBILE_BUYBACK;
                eventData["Pincode"] = pincode;
                eventData["Serviceable"] = pincodeAvailable;
                eventData["QuoteID"] = response.data.quoteId;
                eventData[ABB] = buybackManagerObj?.isABBFlow();
                eventData[SERVED_BY] = getServedByValueFromFirstItem();
                logWebEnageEvent(CHECK_PINCODE_AVAILABILITY, eventData);
            } else {
                let eventData = new Object();
                eventData["Location"] = MOBILE_BUYBACK;
                eventData["Pincode"] = error;
                eventData[ABB] = buybackManagerObj?.isABBFlow();
                eventData[SERVED_BY] = getServedByValueFromFirstItem();
                logWebEnageEvent(CHECK_PINCODE_AVAILABILITY, eventData);
                dispatch({ type: API_ERROR_PINCODE, data: error });
            }
        });
    },
    getStatusDefaultData: () => {
        dispatch({ type: STATUS_DEFAULT_DATA })
    },
    getBuyStatusData: (buybackStatusData) => {
        //    CallApi here
        dispatch({ type: API_CALL_INIT });
        callGetQuoteStateQuestionsAPI(dispatch, buybackStatusData);
    },
    saveQuestions: (data, quoteID, questionType, pincode, price, cityName, mhcScore) => {
        dispatch({ type: API_CALL_INIT });
        excecuteSaveQuestionsApi(data, quoteID, questionType, dispatch, pincode, price, cityName, mhcScore)
    },
    getDeviceInfo: () => {
        let data = buybackManagerObj.getBuybackStatusDataFirstItem();
        dispatch({type: DEVICE_INFO, deviceInfo: data});
    },
    applyCoupon: (quoteId, couponCode) => {
        dispatch({ type: APPLY_COUPON_INIT });
        applyCouponCode(quoteId, couponCode,(response, error) => {
            if (response) {
                let data = buybackManagerObj.getBuybackStatusDataFirstItem();
                let priceAfterApplyCoupon = parseInt(data?.price, 10) + parseInt(response.data.couponPrice, 10);
                data.price = priceAfterApplyCoupon;
                data.couponCodeInfo = {
                    couponCode: couponCode,
                    couponPrice: response.data.couponPrice,
                    isApplied: true
                }
                buybackManagerObj.setBuybackStatusDataFirstItem(data);
                dispatch({ type: APPLY_COUPON_DATA, priceAfterApplyCoupon: priceAfterApplyCoupon });
            } else {
                dispatch({ type: APPLY_COUPON_CODE_DATA_ERROR, data: error });
            }
        });
    },
    getCouponCode: (quoteId) => {
        getCouponCodes(quoteId, (response, error) => {
            if (response) {
                dispatch({ type: GET_COUPON_CODES, couponCodes: response.data.couponCodes });
            } else {
                dispatch({ type: APPLY_COUPON_DATA_ERROR, data: error });
            }
        });
    },
    onCloseErrorDailog: () => {
        dispatch({type: DISMISS_ERROR_DAILOG});
    },
});

function callGetQuoteStateQuestionsAPI(dispatch, buybackStatusData) {
    executeGetQuoteStatusApi(dispatch, buybackStatusData);
}

function executeGetQuoteStatusApi(dispatch, buybackStatusData) {
    getQuoteStateApi(buybackStatusData?.quoteId, (response, error) => {
        if (response) {
            let filterQuesions = parseBuybackQuestion(response.data);
            let data = buybackManagerObj.getBuybackStatusDataFirstItem();
            shouldEnableMHC(callback = {
                onResult: (shouldMHCCheck) => {
                    if (data?.buyBackStatus === buybackStatusConstants.SERVICEABILITY_CHECKED || data?.buyBackStatus === buybackStatusConstants.QUOTE_CREATED) {
                        //If mhc needs to enable
                        let currentQuestionType = 'MHC';
                        checkMhc(currentQuestionType, filterQuesions.mhcQuestionList, (mhcStatus) => {
                            dispatch({
                                type: BUY_BACK_STATUS_DATA,
                                data: response.data,
                                currentItemIndex: buybackStatusData?.currentItemIndex,
                                pincode: buybackStatusData?.pincode,
                                price: buybackStatusData?.price,
                                cityName: buybackStatusData?.cityName,
                                deviceBrand: buybackStatusData?.deviceBrand,
                                mhcScore: buybackStatusData?.mhcScore,
                                shouldMHCCheck: shouldMHCCheck
                            });
                            if (mhcStatus != null && mhcStatus.mhcScore == 1) {
                                let mhcAnswers = { questionType: mhcStatus.questionType, data: mhcStatus.data, mhcScore: mhcStatus.mhcScore };
                                dispatch({ type: API_CALL_INIT });
                                excecuteSaveQuestionsApi(mhcAnswers, buybackStatusData?.quoteId, MHC_QUESTIONS_SAVE, dispatch, buybackStatusData?.pincode, buybackStatusData?.price, buybackStatusData?.cityName, mhcStatus.mhcScore, filterQuesions)
                            }
                            buybackManagerObj.setBuybackStatusDataFirstItem(data);
                        });
                    } else {
                        dispatch({
                            type: BUY_BACK_STATUS_DATA,
                            data: response.data,
                            currentItemIndex: buybackStatusData?.currentItemIndex,
                            pincode: buybackStatusData?.pincode,
                            price: buybackStatusData?.price,
                            cityName: buybackStatusData?.cityName,
                            deviceBrand: buybackStatusData?.deviceBrand,
                            mhcScore: getMhcStatus(buybackStatusData?.mhcScore),
                            shouldMHCCheck: shouldMHCCheck,
                            couponCodeInfo: data?.couponCodeInfo
                        });
                    }
                }
            });
            buybackManagerObj.setBuybackStatusDataFirstItem(data);
        } else {
            dispatch({ type: API_ERROR_PINCODE, data: error });
        }
    });
}

async function checkMhc(currentQuestionType, mhcList, onMHCCheched) {
    try {
        let mhcResult = await nativeBrigeRef.checkMHC(JSON.stringify({ questionType: currentQuestionType, data: mhcList, mhcScore: 0.0 }));
        onMHCCheched(parseJSON(mhcResult));
    } catch (e) {
        onMHCCheched({ questionType: currentQuestionType, data: mhcList, mhcScore: 0.0 });
        // if reject the
    }
}

function excecuteSaveQuestionsApi(data, quoteId, questionType, dispatch, pincode, price, cityName, mhcScore, filterQuesions) {
    try {
        if (data.data.length > 0) {
            saveAnswersForQuestions(quoteId, data, (response, error) => {
                if (response) {
                    shouldEnableMHC(callback = {
                        onResult: (shouldMHCCheck) => {
                            dispatch({
                                type: questionType,
                                data: response.data,
                                pincode: pincode,
                                price: price,
                                cityName: cityName,
                                mhcScore: getMhcStatus(mhcScore),
                                filterQuesions: filterQuesions,
                                shouldMHCCheck: shouldMHCCheck,
                            });
                        },
                    });
                } else {
                    dispatch({type: API_ERROR_PINCODE, data: error});
                }
            });
        }
    } catch (e) {
    }
}

function getMhcStatus(mhcScore) {
    if (mhcScore >= 1 || mhcScore === EXCELLENT) {
        return EXCELLENT;
    } else if (mhcScore >= 0.8 || mhcScore === GOOD) {
        return GOOD;
    } else if (mhcScore >= 0.6 || mhcScore === AVERAGE) {
        return AVERAGE;
    } else {
        return POOR;
    }
}

function shouldEnableMHC(callback) {
    let data = buybackManagerObj.getBuybackStatusDataFirstItem();
    nativeBrigeRef.getApiProperty(apiProperty => {
        let apiProperties = parseJSON(apiProperty);
        callback.onResult(data?.deviceInfo.deviceIdentifier === apiProperties.deviceIdentifier)
    });
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BuybackComponent);
