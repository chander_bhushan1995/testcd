import {connect} from 'react-redux';
import {NativeModules} from 'react-native';
import BuyBackQuestionList from '../BuybackComponent/BuyBackQuestionList';
import {
    ADD_QUESTIONS,
    API_CALL_INIT_QUESTION_AND_ANSWERS,
    API_ERROR_QUESTION_ANSWER,
    DISMISS_ERROR_DAILOG,
    INITIAL_ORDER_QUESTIONS,
    INITIAL_QUOTE_QUESTIONS,
    RESET_QUESTIONS, SAVE_COMPLETE_QUOTE_QUESTIONS_DATA, SET_QUOTE_QUESTION_DEFAULT_LOADING, UPDATE_QUESTIONS,
} from './Constants';
import * as BuybackApiHelper from './BuybackApiHelper';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import {buybackStatusConstants} from '../../Constants/BuybackConstants';


const mapStateToProps = state => ({
    data: state.QuestionAndAnswersReducer.data,
    nextQuestionInfo: state.QuestionAndAnswersReducer.nextQuestionInfo,
    isLoading: state.QuestionAndAnswersReducer.isLoading,
    isError: state.QuestionAndAnswersReducer.isError,
    errorMessage: state.QuestionAndAnswersReducer.errorMessage,
    questionType: state.QuestionAndAnswersReducer.questionType,
    shouldMoveAnotherScreen: state.QuestionAndAnswersReducer.shouldMoveAnotherScreen,
    errorMessageButtonTitle: state.QuestionAndAnswersReducer.errorMessageButtonTitle,
    stepsList: state.PincodeReducer.stepsList,
});
const chatBridgeRef = NativeModules.ChatBridge;
const mapDispatchToProps = dispatch => ({

    updateQuestion: (data, childIndex, index, shouldAddInvoiceUploadQuestion) => {
        dispatch({
            type: ADD_QUESTIONS,
            data: data,
            childIndex: childIndex,
            index: index,
            shouldAddInvoiceUploadQuestion: shouldAddInvoiceUploadQuestion,
        });
    },
    updateMultipleQuestion: (data, childIndex, index) => {
        dispatch({type: UPDATE_QUESTIONS, data: data, childIndex: childIndex, index: index});

    },
    resetData: () => {
        dispatch({type: RESET_QUESTIONS});

    },
    completeQuoteQuestionsData: (questionsData) => {
        dispatch({type: SAVE_COMPLETE_QUOTE_QUESTIONS_DATA, data: questionsData});
    },
    getManupulatedData: (data) => {
        dispatch({type: INITIAL_QUOTE_QUESTIONS, data: data});

    }, getManupulatedData1: (data) => {
        chatBridgeRef.getUserInfo(userInfo => {
            dispatch({type: INITIAL_ORDER_QUESTIONS, data: data, userInfo: userInfo});
        });

    },
    saveQuestions: (data, quoteID, shouldMoveAnotherScreen) => {
        dispatch({type: API_CALL_INIT_QUESTION_AND_ANSWERS});
        excecuteSaveQuestionsApi(data, quoteID, shouldMoveAnotherScreen, dispatch);
    },
    onCloseErrorDailog: () => {
        dispatch({type: DISMISS_ERROR_DAILOG});
    },
    setLoadingDefault: () =>{
        dispatch({type: SET_QUOTE_QUESTION_DEFAULT_LOADING});
    }

});

function excecuteSaveQuestionsApi(data, quoteId, shouldMoveAnotherScreen, dispatch) {
    BuybackApiHelper.saveAnswersForQuestions(quoteId, data, (response, error) => {
        if (response) {
            if (shouldMoveAnotherScreen) {
                let data = buybackManagerObj.getBuybackStatusDataFirstItem();
                data.buyBackStatus = buybackStatusConstants.BUYBACK_PICKUP_PENDING;
                buybackManagerObj.setBuybackStatusDataFirstItem(data);
                dispatch({
                    type: 'save_question',
                    data: response.data,
                    shouldMoveAnotherScreen: shouldMoveAnotherScreen,
                });
            }
        } else {
                dispatch({type: API_ERROR_QUESTION_ANSWER, data: error});
        }
    });
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(BuyBackQuestionList);
