import {BUYBACK_STATUS_DATA_LIST} from "../buybackActions/Constants";
import {buybackStatusConstants} from '../../Constants/BuybackConstants';
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

const data = {
    buybackStatusList: [
        {
            "buyBackStatus": buybackStatusConstants.BUYBACK_NOT_STARTED,
            "price": "4725",
            "deviceInfo": {
                "modelName": "CPH1861",
                "primaryMemory": "3072000000",
                "secondaryMemory": "32",
                "deviceType": "phone",
                "image": "https://instacash.blob.core.windows.net/static/img/products/Oppo_Realme_1_32GB.png",
                "deviceName": "Oppo Realme 1 32GB",
                "brand": "Oppo",
                "deviceId": "4786"
            },
            "quoteId": ""

        },
        {
            "buyBackStatus": buybackStatusConstants.BUYBACK_NOT_STARTED,
            "price": "4725",
            "deviceInfo": {
                "modelName": "CPH1861",
                "primaryMemory": "3072000000",
                "secondaryMemory": "32",
                "deviceType": "phone",
                "image": "https://instacash.blob.core.windows.net/static/img/products/Oppo_Realme_1_32GB.png",
                "deviceName": "Oppo Realme 1 32GB",
                "brand": "Oppo",
                "deviceId": "4786"
            },
            "quoteId": ""

        }
    ]
};
const buybackMultipleServiceRequestReducer = (state = data, action) => {
    switch (action.type) {
        case BUYBACK_STATUS_DATA_LIST:
            return {...state, buybackStatusList: deepCopy(data.buybackStatusList)};
        default:
            return {...state,isError: false};
    }

};
export default buybackMultipleServiceRequestReducer;
