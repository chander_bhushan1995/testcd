import {API_ERROR_SUCCESS_TIMELINE, ORDER_STATUS_DATA} from '../buybackActions/Constants';
import {INTERNET_ERROR_MESSAGE_BUYBACK, TECHNICAL_ERROR_MESSAGE} from '../../Constants/ActionTypes';
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

const data = {
    orderStatus: null,
    isLoading: true,
    isError: false,
    errorMessage: '',
    errorMessageButtonTitle: 'Ok Got It',


};
const BuybackRequestSuccessTimelineReducer = (state = data, action) => {

    switch (action.type) {
        case ORDER_STATUS_DATA:
            return {...state, isLoading: false, orderStatus: deepCopy(action.data)};
            break;

        case API_ERROR_SUCCESS_TIMELINE:
            if (action.data.message !== undefined && action.data.message === 'Network request failed') {
                return {
                    ...state,
                    isError: true,
                    errorMessage: INTERNET_ERROR_MESSAGE_BUYBACK,
                    errorMessageButtonTitle: 'OK',
                    isLoading: false,
                };
            } else {
                return {
                    ...state,
                    isError: true,
                    errorMessage: action.data.message !== undefined ? action.data.message : TECHNICAL_ERROR_MESSAGE,
                    errorMessageButtonTitle: 'Ok Got It',
                    isLoading: false,
                };
            }
            return {
                ...state, isLoading: false, isError: true,
                errorMessage: action.data.message !== undefined ? action.data.message : TECHNICAL_ERROR_MESSAGE,
            };
            break;

        default:
            return {...state, isError: false};
    }

};
export default BuybackRequestSuccessTimelineReducer;
