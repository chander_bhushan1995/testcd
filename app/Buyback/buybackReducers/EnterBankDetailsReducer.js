import * as Actions from '../buybackActions/Constants'

const data = { data: {
        title: "Enter bank details",
        buttonText: "Submit",
        bankNameTitle: "Bank name",
        accountNumberTitle: "Account number",
        confirmAccountNumberTitle: "Confirm account number",
        IFSCCodeTitle: "IFSC Code",
    }
};
const EnterBankDetailsReducer = (state = data, action) => {

    switch (action.type) {
        case "select":
            return {...state};
        default:
            return {...state};
    }

};
export default EnterBankDetailsReducer;
