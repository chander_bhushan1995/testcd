import {
    ABB_REQUEST_CANCEL,
    API_ERROR, APPLY_COUPON_CODE_DATA_ERROR,
    APPLY_COUPON_DATA,
    APPLY_COUPON_DATA_ERROR,
    APPLY_COUPON_INIT, EXTRACT_PRICE_API_CALL_INIT, EXTRACT_PRICE_API_ERROR,
    GET_PRICE, RESET_FINAL_PRICE_DATA, RESET_NOT_ELIGIBLE,
} from '../buybackActions/Constants';
import {INTERNET_ERROR_MESSAGE_BUYBACK, TECHNICAL_ERROR_MESSAGE} from "../../Constants/ActionTypes";
import {GET_COUPON_CODES} from "../../Constants/ApiUrls";

const data = {
    price: null,
    couponPrice: "",
    isLoading: true,
    isError: false,
    errorType:null,
    errorMessage: "",
    couponCodeInfo: {},
    isCouponApplied:false,
    isCouponApplying:false,
    isCouponError:false,
    errorMessageButtonTitle:'Ok Got It',
    subType:'',
    ABBNotEligible: false,
};
const FinalPriceSuccessReducer = (state = data, action) => {
    switch (action.type) {

        case RESET_FINAL_PRICE_DATA:
            return {
                price: null,
                couponPrice: "",
                isLoading: true,
                isError: false,
                errorType:null,
                errorMessage: "",
                couponCodeInfo: {},
                isCouponApplied:false,
                isCouponApplying:false,
                isCouponError:false,
                errorMessageButtonTitle:'Ok Got It'
            };
            break;

        case API_ERROR:
            if(action.data.message !== undefined  && action.data?.message==='Network request failed')
            {
                return {
                    ...state,
                    data: action.data,
                    isError: true,
                    errorMessage: INTERNET_ERROR_MESSAGE_BUYBACK,
                    errorMessageButtonTitle: 'OK',
                    isLoading: false,
                };
            }
            else
            {
                return {
                    ...state,
                    data: action.data,
                    isError: true,
                    errorMessage: action.data.message !== undefined ? action.data.message : TECHNICAL_ERROR_MESSAGE,
                    errorMessageButtonTitle: 'Ok Got It',
                    isLoading: false,
                };
            }
         break;
        case GET_PRICE:
            return {...state, isLoading: false, price: action.price,isError:false,isCouponError:false, subType: action.subType, ABBNotEligible: action.ABBNotEligible};
            break;
        case RESET_NOT_ELIGIBLE:
            return {...state, ABBNotEligible: false};
            break;
        case GET_COUPON_CODES:
            return {...state, isLoading: false, couponCodeInfo: { couponCode: action?.couponCodes[0].code}};
            break;

        case APPLY_COUPON_DATA:
            return {...state, isLoading: false, price:action.priceAfterApplyCoupon,isCouponApplying:false,isCouponApplied:true};
            break;

        case APPLY_COUPON_INIT:
            return {...state, isCouponApplying:true,isError:false,isCouponError:false};
            break;

        case EXTRACT_PRICE_API_ERROR :
            if(action.data.message !== undefined  && action.data.message==='Network request failed')
            {
                return {
                    ...state,
                    isError: true,
                    errorMessage: INTERNET_ERROR_MESSAGE_BUYBACK,
                    errorMessageButtonTitle: 'OK',
                    isLoading: false,
                    isCouponApplying:false
                };
            }
            else
            {
                return {
                    ...state,
                    isError: true,
                    errorMessage: action.data.message !== undefined ? action.data.message : TECHNICAL_ERROR_MESSAGE,
                    errorMessageButtonTitle: 'Ok Got It',
                    errorType: action.data?(action.data.error[0].errorCode===1001 || action.data.error[0].errorCode === 1102)?action.data.error[0].errorCode:null:null ,
                    isLoading: false,
                    isCouponApplying:false
                };
            }
            break;

        case ABB_REQUEST_CANCEL :
            return {
                ...state,
                isError: true,
                errorType: '1024',
            };
            break;

        case  APPLY_COUPON_DATA_ERROR:
            if(action.data.message !== undefined  && action.data.message==='Network request failed')
            {
                return {
                    ...state,
                    isError: true,
                    errorMessage: INTERNET_ERROR_MESSAGE_BUYBACK,
                    errorMessageButtonTitle: 'OK',
                    isLoading: false,
                    isCouponApplying:false
                };
            }
            else
            {
                return {
                    ...state,
                    isError: true,
                    errorMessage: action.data.message !== undefined ? action.data.message : TECHNICAL_ERROR_MESSAGE,
                    errorMessageButtonTitle: 'Ok Got It',
                    errorType: action.data !== undefined?action.data.error[0].errorCode===1001?action.data.error[0].errorCode:null:null ,
                    isLoading: false,
                    isCouponApplying:false
                };
            }
            break;

        case APPLY_COUPON_CODE_DATA_ERROR:
            if(action.data.message !== undefined  && action.data.message==='Network request failed')
            {
                return {
                    ...state,
                    isCouponError: true,
                    errorMessage: INTERNET_ERROR_MESSAGE_BUYBACK,
                    errorMessageButtonTitle: 'OK',
                    isLoading: false,
                    isCouponApplying:false
                };
            }
            else
            {
                return {
                    ...state,
                    isCouponError: true,
                    errorMessage: action.data.message !== undefined ? action.data.message : TECHNICAL_ERROR_MESSAGE,
                    errorMessageButtonTitle: 'Ok Got It',
                    isLoading: false,
                    isCouponApplying:false
                };
            }
            break;

        case EXTRACT_PRICE_API_CALL_INIT:
            return {...state,isLoading:true};
            break;

        default:
            return {...state};
    }

};
export default FinalPriceSuccessReducer;
