import {
    CREATE_ORDER_API_INIT, CREATE_ORDER_DATA, CREATE_ORDER_DATA_ERROR,
} from "../buybackActions/Constants";
import {INTERNET_ERROR_MESSAGE_BUYBACK, TECHNICAL_ERROR_MESSAGE} from "../../Constants/ActionTypes";

const data = {
    data: {
        orderID: "",
        isLoading: true,
        isError: false,
        errorMessage: "",
        ratingData: undefined,
        shouldAddStarsView: true,
        errorMessageButtonTitle: 'Ok Got It',
        showRating: false,
    }
};
const MobilePickupScheduledSuccessReducer = (state = data, action) => {

    switch (action.type) {
        case CREATE_ORDER_DATA:
            return {
                ...state,
                isLoading: false,
                orderID: action?.data?.partnerOrderId,
                isError: false,
                showRating: true
            };
            break;

        case CREATE_ORDER_API_INIT:
            return {
                ...state,
                isLoading: true,
                shouldAddStarsView: true,
            };
            break;

        case CREATE_ORDER_DATA_ERROR:
			let pincodeErrorIndex = action.data?.error?.findIndex((value) => value?.errorCode == 406)
            if (action.data?.message !== undefined && action.data?.message === 'Network request failed') {
                return {
                    ...state,
                    isError: true,
                    errorMessage: INTERNET_ERROR_MESSAGE_BUYBACK,
                    errorMessageButtonTitle: 'OK',
                    isLoading: false,
                };
            } else if (pincodeErrorIndex != -1) {
                return {
                    ...state,
                    isError: true,
                    errorMessage: action.data?.error[pincodeErrorIndex].errorMessage,
                    errorMessageButtonTitle: 'Ok Got It',
                    isLoading: false,
                };
			} else {
				return {
					...state,
					isError: true,
					errorMessage: action.data?.message !== undefined ? action.data?.message : TECHNICAL_ERROR_MESSAGE,
					errorMessageButtonTitle: 'Ok Got It',
					isLoading: false,
				};
			}
            break;
        default:
            return state;
    }

};
export default MobilePickupScheduledSuccessReducer;
