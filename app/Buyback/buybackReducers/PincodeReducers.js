import * as Actions from '../buybackActions/Constants';
import * as StepConstants from '../../Constants/BuybackConstants';
import {GET_COUPON_CODES} from '../../Constants/ApiUrls';
import {INTERNET_ERROR_MESSAGE_BUYBACK, TECHNICAL_ERROR_MESSAGE} from '../../Constants/ActionTypes';
import {
    API_CALL_INIT,
    API_ERROR_PINCODE,
    APPLY_COUPON_DATA_ERROR,
    DEVICE_INFO,
    RESET_BUYBACK_DATA,
    RESET_BUYBACK_DEEPLINK_TYPE,
    APPLY_COUPON_INIT,
    APPLY_COUPON_DATA,
    APPLY_COUPON_CODE_DATA_ERROR,
    DISMISS_ERROR_DAILOG, DISMISS_LOADER,
} from '../buybackActions/Constants';
import {EXCELLENT} from '../../Constants/BuybackConstants';
import NumberUtils from '../../commonUtil/NumberUtils';
import {buybackManagerObj} from '../../Navigation/index.buyback';
import {RESET_PIN_CODE_ERROR} from '../buybackActions/Constants';
import {buybackStatusConstants} from '../../Constants/BuybackConstants';
import {SAVE_COMPLETE_QUOTE_QUESTIONS_DATA} from '../buybackActions/Constants';
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

getStepsImage = (type, state) => {
    if (state === StepConstants.STEP_COMPLETED) {
        return require("../../images/green_right.webp");
    }
    switch (type) {
        case StepConstants.STEP_LOCATION:
            return require("../../images/step_location_enable.webp");
        case StepConstants.STEP_MHC:
            if (state === StepConstants.STEP_NOT_STARTED) {
                return require("../../images/step_setting_disable.webp");
            }
            return require("../../images/step_setting_enable.webp");
        case StepConstants.STEP_QUESTIONS:
            if (state === StepConstants.STEP_NOT_STARTED) {
                return require("../../images/step_question_disable.webp");
            }
            return require("../../images/step_question_enable.webp");
        case StepConstants.STEP_PICKUP:
            if (state === StepConstants.STEP_NOT_STARTED) {
                return require("../../images/step_ruppee_disable.webp");
            }
            return require("../../images/step_ruppee_enable.webp");
        default:
            return require("../../images/step_location_enable.webp");
    }
};
const defaultData = [{
    stepState: this.getStepsImage(StepConstants.STEP_LOCATION, StepConstants.STEP_IN_PROGRESS),
    heading1: 'Check serviceability',
    heading2: 'Tell us your pincode',
    isEnable: true,
    state: StepConstants.STEP_IN_PROGRESS,
    button: {
        text: 'Enter Pincode',
    },
},
    {
        stepState: this.getStepsImage(StepConstants.STEP_MHC, StepConstants.STEP_NOT_STARTED),
        heading1: 'Check mobile health',
        heading2: 'Get your mobile score.',
        questionData: [],
        isEnable: false,
        state: StepConstants.STEP_NOT_STARTED,
        button: {
            text: 'Check now',
        },
    },
    {
        stepState: this.getStepsImage(StepConstants.STEP_QUESTIONS, StepConstants.STEP_NOT_STARTED),
        heading1: 'Answer a few questions',
        heading2: 'Get the best resale price',
        isEnable: false,
        state: StepConstants.STEP_NOT_STARTED,
        questionData: [],
        button: {
            text: 'Start now',
        },
    },
    {
        stepState: this.getStepsImage(StepConstants.STEP_PICKUP, StepConstants.STEP_NOT_STARTED),
        heading1: 'Exchange/Schedule price',
        heading2: 'Check mobile health',
        isEnable: false,
        state: StepConstants.STEP_NOT_STARTED,
        questionData: [],
        button: {text: 'Start now'},
    }];

const defaultData1 = [
    {
        stepState: this.getStepsImage(StepConstants.STEP_LOCATION, StepConstants.STEP_IN_PROGRESS),
        heading1: 'Check serviceability',
        heading2: 'Tell us your pincode',
        isEnable: true,
        state: StepConstants.STEP_IN_PROGRESS,
        button: {
            text: 'Enter Pincode',
        },
    },
    {
        stepState: this.getStepsImage(StepConstants.STEP_MHC, StepConstants.STEP_NOT_STARTED),
        heading1: 'Check mobile health',
        heading2: 'Get your mobile score.',
        questionData: [],
        isEnable: false,
        state: StepConstants.STEP_NOT_STARTED,
        button: {
            text: 'Check now',
        },
    },
    {
        stepState: this.getStepsImage(StepConstants.STEP_QUESTIONS, StepConstants.STEP_NOT_STARTED),
        heading1: 'Answer a few questions',
        heading2: 'Get the best resale price',
        isEnable: false,
        state: StepConstants.STEP_NOT_STARTED,
        questionData: [],
        button: {
            text: 'Start now',
        },
    },
    {
        stepState: this.getStepsImage(StepConstants.STEP_PICKUP, StepConstants.STEP_NOT_STARTED),
        heading1: 'Exchange/Schedule price',
        heading2: 'Check mobile health',
        isEnable: false,
        state: StepConstants.STEP_NOT_STARTED,
        questionData: [],
        button: {text: 'Start now'},
    }];
const data = {
    pincodeServiceablityData: null,
    data: '',
    stepsList: [],
    deviceInfo: null,
    isError: false,
    errorMessage: '',
    isLoading: false,
    errorMessageButtonTitle: 'Ok Got It',
    type: '',
    errorType: null,
    couponCodeInfo: {},
    isCouponApplying: false,
    isCouponApplied: false,
    isCouponError: false,
    price: null,
};
const defaultState = {
    pincodeServiceablityData: null,
    data: '',
    stepsList: defaultData1,
    deviceInfo: null,
    isError: false,
    errorMessage: '',
    isLoading: false,
    errorMessageButtonTitle: 'Ok Got It',
    type: '',
    errorType: null,
    couponCodeInfo: {},
    isCouponApplying: false,
    isCouponApplied: false,
    isCouponError: false,
    price: null,
};

const pincodeReducer = (state = data, action) => {
    switch (action.type) {
        case API_CALL_INIT:
            return ({...state, isLoading: true});
        case RESET_BUYBACK_DATA:
            return defaultState;
        case DEVICE_INFO:
            return ({...state, deviceInfo: action.deviceInfo});
        case RESET_BUYBACK_DEEPLINK_TYPE:
            return ({...state, type: ''});
        case Actions.PINCODE_SERVICEABLITY_RESPONSE:
            if (action.data.pincodeAvailable || (buybackManagerObj.isABBFlow() && action.data.quoteId)) {
                updateBuyBackStatus(buybackStatusConstants.SERVICEABILITY_CHECKED, action.data);
                let stepsList = defaultData;
                enableDisbaleStepBasedOnStatus(1, stepsList, action.pincode, action.price, action.cityName, action.mhcScore, action.shouldMHCCheck);
            }
            return {
                ...state,
                pincodeServiceablityData: action.data,
                isError: false,
                errorMessage: '',
                price: action.price,
            };
        case RESET_PIN_CODE_ERROR:
            return {...state, pincodeServiceablityData: null, isError: false, errorMessage: ''};
        case API_ERROR_PINCODE:
            if (action.data.message !== undefined && action.data.message === 'Network request failed') {
                return {
                    ...state,
                    data: action.data,
                    isError: true,
                    errorMessage: INTERNET_ERROR_MESSAGE_BUYBACK,
                    errorMessageButtonTitle: 'OK',
                    isLoading: false,
                };
            } else {
                return {
                    ...state,
                    data: action.data,
                    isError: true,
                    errorMessage: action.data.message !== undefined ? action.data.message : TECHNICAL_ERROR_MESSAGE,
                    errorMessageButtonTitle: 'Ok Got It',
                    isLoading: false,
                };
            }
        case Actions.BUY_BACK_STATUS_DATA:
            let filterQuesions = extractQuestionData(action.data, action.deviceBrand);
            let updatedState = addQuestionsDataIntoState(filterQuesions);
            enableDisbaleStepBasedOnStatus(action.currentItemIndex, updatedState, action.pincode, action.price, action.cityName, action.mhcScore, action.shouldMHCCheck);
            if (filterQuesions !== null) {
                return {
                    ...state,
                    stepsList: updatedState,
                    data: filterQuesions,
                    isError: false,
                    errorMessage: '',
                    isLoading: false,
                    type: Actions.BUY_BACK_STATUS_DATA,
                    couponCodeInfo: {...state.couponCodeInfo, ...action.couponCodeInfo},
                    isCouponApplied: (action.couponCodeInfo !== null && action.couponCodeInfo !== undefined && action.couponCodeInfo.isApplied === true),
                    price: action.price,
                };
            } else {
                return {
                    ...state,
                    data: action.data,
                    isError: true,
                    errorMessage: action.data.message !== undefined ? data.message : TECHNICAL_ERROR_MESSAGE,
                    couponCodeInfo: {...state.couponCodeInfo, ...action.couponCodeInfo},
                    isCouponApplied: (action.couponCodeInfo !== null && action.couponCodeInfo !== undefined && action.couponCodeInfo.isApplied === true),
                    price: action.price,
                };
            }
        case Actions.STATUS_DEFAULT_DATA:
            return {...state, stepsList: defaultData1};
        case Actions.MHC_QUESTIONS_SAVE:
            updateBuyBackStatus(buybackStatusConstants.MHC_COMPLETED, null, action.mhcScore);
            let mchStepsList = state.stepsList;
            enableDisbaleStepBasedOnStatus(2, mchStepsList, action.pincode, action.price, action.cityName, action.mhcScore, action.shouldMHCCheck);
            return {...state, stepsList: mchStepsList, isLoading: false, price: action.price};
        case Actions.QUOTE_QUESTIONS_SAVE:
            let quoteQuestions = defaultData;
            enableDisbaleStepBasedOnStatus(3, quoteQuestions, action.pincode, action.price, action.cityName, action.mhcScore, action.shouldMHCCheck);
            return {
                ...state,
                stepsList: deepCopy(quoteQuestions),
                isLoading: false,
                price: action.price,
            };
        case GET_COUPON_CODES:
            return {...state, isLoading: false, couponCodeInfo: {couponCode: action.couponCodes[0].code}};
        case APPLY_COUPON_DATA:
            let list = state.stepsList;
            var dateFormat = require('dateformat');
            let quoteEndDate = dateFormat(new Date(Date.now() + 7 * 24 * 60 * 60 * 1000), 'dd-mmmm');
            list[3].heading1 = 'Resale price is ' + '\u20B9 ' + NumberUtils.getFormattedPrice(action.priceAfterApplyCoupon);
            list[3].heading2 = 'Price is subject to change upon inspection at the time of pickup. Schedule before ' + quoteEndDate + ' or you may lose this offer.';
            return {
                ...state,
                type: APPLY_COUPON_DATA,
                stepsList: list,
                isLoading: false,
                price: action.priceAfterApplyCoupon,
                isCouponApplying: false,
                isCouponApplied: true,
            };
        case APPLY_COUPON_INIT:
            return {...state, isCouponApplying: true, isError: false, isCouponError: false};
        case APPLY_COUPON_DATA_ERROR:
            let errorMessage = action.data.message;
            let errorMessageButtonTitle = 'OK';
            if (errorMessage !== undefined && errorMessage === 'Network request failed') {
                errorMessage = INTERNET_ERROR_MESSAGE_BUYBACK;
                errorMessageButtonTitle = 'OK';
            } else {
                errorMessage = errorMessage !== undefined ? errorMessage : TECHNICAL_ERROR_MESSAGE;
                errorMessageButtonTitle = 'Ok Got It';
            }
            return {
                ...state,
                isError: true,
                errorMessage: errorMessage,
                errorMessageButtonTitle: errorMessageButtonTitle,
                isLoading: false,
                isCouponApplying: false,
            };
        case APPLY_COUPON_CODE_DATA_ERROR:
            let errorMessage1 = action.data.message;
            let errorMessageButtonTitle1 = 'OK';
            if (errorMessage1 !== undefined && errorMessage1 === 'Network request failed') {
                errorMessage1 = INTERNET_ERROR_MESSAGE_BUYBACK;
                errorMessageButtonTitle1 = 'OK';
            } else {
                errorMessage1 = errorMessage1 !== undefined ? errorMessage1 : TECHNICAL_ERROR_MESSAGE;
                errorMessageButtonTitle1 = 'Ok Got It';
            }
            return {
                ...state,
                isCouponError: true,
                errorMessage: errorMessage1,
                errorMessageButtonTitle: errorMessageButtonTitle1,
                isLoading: false,
                isCouponApplying: false,
            };
        case DISMISS_LOADER:
            return {...state, isLoading: false}
        case SAVE_COMPLETE_QUOTE_QUESTIONS_DATA:
            // let stepsList = {...state.stepsList};
            // stepsList[2] = action.data;
            // state.stepsList = stepsList;
            return {...state, data: action.data};
        case DISMISS_ERROR_DAILOG:
            return {...state, isError: false};
        default:
            return {...state};
    }
};

function enableDisbaleStepBasedOnStatus(index, stepList, pincode, price, cityName, mhcStatus, shouldMHCCheck) {
    let enableStepType = StepConstants.STEP_LOCATION;
    switch (index) {
        case 1:
            enableStepType = StepConstants.STEP_MHC;
            break;
        case 2:
            enableStepType = StepConstants.STEP_QUESTIONS;
            break;
        case 3:
            enableStepType = StepConstants.STEP_PICKUP;
            break;
    }
    for (let i = 0; i <= index; i++) {
        if (i === index) {
            if (i === 3) {
                var dateFormat = require('dateformat');
                let quoteEndDate = dateFormat(new Date(Date.now() + 7 * 24 * 60 * 60 * 1000), 'dd-mmmm');
                stepList[i].heading1 = 'Resale price is ' + '\u20B9 ' + NumberUtils.getFormattedPrice(price);
                stepList[i].heading2 = 'Price is subject to change upon inspection at the time of pickup. Schedule before ' + quoteEndDate + ' or you may lose this offer.';
            }
            stepList[i].stepState = this.getStepsImage(enableStepType, StepConstants.STEP_IN_PROGRESS);
            stepList[i].state = StepConstants.STEP_IN_PROGRESS;
        } else {
            if (i === 0) {
                stepList[i].heading1 = 'We’re servicing in this pincode.';
                stepList[i].heading2 = pincode + '-' + cityName;
            } else if (i === 1) {
                // stepList[i].heading1='Great! Your mobile health score is';
                stepList[i].heading2 = mhcStatus;
            } else if (i === 2) {
                stepList[i].heading1 = 'Cool! We now have all the details.';
                stepList[i].heading2 = 'Mobile condition seems good.';
            }
            if (mhcStatus !== EXCELLENT && i === 1) {
                if (shouldMHCCheck) {
                    stepList[i].button = {
                        text: 'View Details',
                    };
                    stepList[i].stepState = this.getStepsImage(StepConstants.STEP_MHC, StepConstants.STEP_IN_PROGRESS);
                    stepList[i].state = StepConstants.STEP_IN_PROGRESS;
                } else {
                    stepList[i].stepState = this.getStepsImage(enableStepType, StepConstants.STEP_COMPLETED);
                    stepList[i].state = StepConstants.STEP_COMPLETED;
                }

            } else {
                if (i === 2) {
                    stepList[i - 1].stepState = this.getStepsImage(enableStepType, StepConstants.STEP_COMPLETED);
                    stepList[i - 1].state = StepConstants.STEP_COMPLETED;
                }
                stepList[i].stepState = this.getStepsImage(enableStepType, StepConstants.STEP_COMPLETED);
                stepList[i].state = StepConstants.STEP_COMPLETED;
            }
        }
    }
}

function extractQuestionData(data, deviceBrand) {
    const QuestionList = buybackManagerObj.getQuestionsList();
    let questionStepsRequired = data.questionStepsRequired;
    if (questionStepsRequired !== null) {
        let quoteQuestionList = [];
        let mhcQuestionList = [];
        let orderQuestionList = [];
        // if (deviceBrand === 'Apple' || (Platform.OS === "android" && Platform.Version >= 29)) {
        //     quoteQuestionList.push(QuestionList['Q_IMEI'])
        // }
        for (let i = 0; i < questionStepsRequired.length; i++) {
            let item = questionStepsRequired[i];
            if (item.questionType === 'QUOTE_QUESTION' || (item.questionType === 'DEVICE_AGE' && item?.isCompleted === false)) {
                let questionData = QuestionList[item.questionId];
                const userInfo = parseJSON(buybackManagerObj.getUserInfo());
                if (questionData !== undefined) {
                    if (item.questionId === 'Q-CN') {
                        if (userInfo?.userName === null || userInfo?.userName === undefined || userInfo?.userName === '') {
                            let questionItem = {
                                ...questionData,
                                isCompleted: item.isCompleted,
                                isSingleChoice: item.questionFormat === 'SS',
                            };
                            quoteQuestionList.push(questionItem);
                        }
                    } else {
                        let questionItem = {
                            ...questionData,
                            isCompleted: item.isCompleted,
                            isSingleChoice: item.questionFormat === 'SS',
                        };
                        quoteQuestionList.push(questionItem);
                    }
                }

            } else if (item.questionType === 'MHC') {
                let questionItem = {
                    questionId: item.questionId,
                    answer: '',
                    ...QuestionList[item.questionId],
                    // isCompleted: item.isCompleted,
                    // isSingleChoice: item.questionFormat === "SS"
                };
                mhcQuestionList.push(questionItem);
            } else if (item.questionType === 'ORDER_QUESTION') {
                if (item.questionId === 'Q-ADDR' || item.questionId === 'Q-PM') {
                    let questionItem = {
                        questionId: item.questionId,
                        isCompleted: item.isCompleted,
                    };
                    orderQuestionList.push(questionItem);
                } else {
                    let questionItem = {
                        questionId: item.questionId,
                        answer: '',
                        ...QuestionList[item.questionId],
                        // isCompleted: item.isCompleted,
                        // isSingleChoice: item.questionFormat === "SS"
                    };
                    orderQuestionList.push(questionItem);
                }

            }
        }
        return {
            quoteQuestionList: quoteQuestionList,
            mhcQuestionList: mhcQuestionList,
            orderQuestionList: orderQuestionList,
        };
    }
    return null;
}

//need to optimize
function addQuestionsDataIntoState(filterQuestionsData) {
    let data = deepCopy(defaultData);
    data[1].questionData = filterQuestionsData.mhcQuestionList;
    data[2].questionData = filterQuestionsData.quoteQuestionList;
    data[3].questionData = filterQuestionsData.orderQuestionList;
    return deepCopy(data);
}

function updateBuyBackStatus(status, data, mhcScore) {
    let buyBackStatusData = buybackManagerObj.getBuybackStatusDataFirstItem();
    let currentBuyBackStatusData;
    if (data !== undefined && data !== null) {
        currentBuyBackStatusData = {'buyBackStatus': status, quoteId: data?.quoteId};
    } else {
        currentBuyBackStatusData = {'buyBackStatus': status};
    }
    if (buyBackStatusData !== null) {
        buyBackStatusData.buyBackStatus = status;
        if (mhcScore) {
            buyBackStatusData.mhcScore = mhcScore;
        }
        buybackManagerObj.setBuybackStatusDataFirstItem(buyBackStatusData);
    } else {
        if (mhcScore) {
            currentBuyBackStatusData.mhcScore = mhcScore;
        }
        buybackManagerObj.setBuybackStatusDataFirstItem(buyBackStatusData);
    }
}


export default pincodeReducer;
