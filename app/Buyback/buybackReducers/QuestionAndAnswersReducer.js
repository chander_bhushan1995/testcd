import SingleAndMultiChoiceQuestion from '../BuybackComponent/SingleAndMultiChoiceQuestion'
import EditTextQuestionWithButtonAction from '../buybackActions/EditTextQuestionWithButtonAction'
import IosIMEI from '../BuybackComponent/IosIMEI'
import PaymentTypeQuestion from '../BuybackComponent/PaymentTypeQuestion'
import * as Actions from '../buybackActions/Constants'
import {
    ADD_QUESTIONS,
    API_CALL_INIT_QUESTION_AND_ANSWERS,
    DISMISS_ERROR_DAILOG,
    INITIAL_ORDER_QUESTIONS,
    INITIAL_QUOTE_QUESTIONS,
    INITIAL_QUOTE_QUESTIONS_FROM_PRICE, PINCODE_QUESTION,
    RESET_QUESTIONS,
    UPDATE_QUESTIONS
} from '../buybackActions/Constants'
import {INTERNET_ERROR_MESSAGE_BUYBACK, TECHNICAL_ERROR_MESSAGE} from "../../Constants/ActionTypes";
import InvoiceUpload from "../BuybackComponent/InvoiceUpload";
import {isEmailAddressValid,isTextAndSpaceOnly} from '../../commonUtil/Validator'
import {buybackManagerObj} from "../../Navigation/index.buyback";
import IDProofUpload from '../BuybackComponent/IDProofUpload';
import EnterPincodeQuestion from '../BuybackComponent/EnterPincodeQuestion';
import {buybackFlowType} from "../../Constants/BuybackConstants";
import {SAVE_COMPLETE_QUOTE_QUESTIONS_DATA} from '../buybackActions/Constants';
import {SET_QUOTE_QUESTION_DEFAULT_LOADING} from '../buybackActions/Constants';
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

let DefaultData = [];
const data = {
    nextQuestionInfo: {},
    data: [],
    isLoading: false,
    isError: false,
    errorMessage: "",
    shouldMoveAnotherScreen: false,
    questionType: null,
    errorMessageButtonTitle: 'Ok Got It',
};

const QuestionAndAnswerReducer = (state = data, action) => {
    switch (action.type) {
        case API_CALL_INIT_QUESTION_AND_ANSWERS:
            return ({...state, isLoading: true});
            break;
        case SET_QUOTE_QUESTION_DEFAULT_LOADING:
            return {
                ...state,
                isLoading: false,
                shouldMoveAnotherScreen: false,
            };
            break;
        case UPDATE_QUESTIONS :
            let data1 = state.data;
            data1[action.index].componentData[action.childIndex] = action.data;
            return Object.assign({}, state, {
                data: [...state.data],
            });
            break;
        case RESET_QUESTIONS :
            return {
                nextQuestionInfo: {},
                data: [],
                isLoading: false,
                isError: false,
                shouldMoveAnotherScreen: false,
            };
            break;

        case DISMISS_ERROR_DAILOG :
            return {...state, isError: false};
            break;

        case "save_question" :
            return {
                ...state,
                isLoading: false,
                shouldMoveAnotherScreen: action.shouldMoveAnotherScreen,
                // data: []
            };
            break;

        case ADD_QUESTIONS :
            let currentData = state.data;
            currentData[action.index].componentData.data = action.data;
            if(action.shouldAddInvoiceUploadQuestion===undefined)
            {
                if (action.index + 1 === DefaultData.length || action.index + 1 < state.data.length)
                    return Object.assign({}, state, {
                        data: [...state.data],
                    });
                else
                    return Object.assign({}, state, {
                        data: state.data.length === action.index + 1 ? [...currentData, DefaultData[action.index + 1]] : [...state.data],
                        nextQuestionInfo: {
                            title: DefaultData.length > action.index + 2 ? DefaultData[action.index + 2].componentData.title : "",
                            questionNumber: action.index + 3,
                            shouldShow: DefaultData.length >= action.index + 3
                        }
                    });
            }
            else if(action.shouldAddInvoiceUploadQuestion)
            {
                let filterdata = state.data.filter(function(value){

                    return value.ComponantName!==InvoiceUpload;

                });
                DefaultData = DefaultData.filter(function(value){

                    return value.ComponantName!==InvoiceUpload;

                });
                let invoiceData = state.data[action.index]?.componentData?.subData;
                if(action.index===state.data.length)
                    filterdata.push({ComponantName: InvoiceUpload, componentData: invoiceData});
                else
                {
                    filterdata.splice( action.index+1, 0, {ComponantName: InvoiceUpload, componentData:  invoiceData});
                }
                DefaultData.splice( action.index+1, 0, {ComponantName: InvoiceUpload, componentData:  invoiceData});
                return {...state,data:filterdata,
                    nextQuestionInfo: {
                        title: DefaultData.length > action.index + 2 ? DefaultData[action.index + 2].componentData.title : "",
                        questionNumber: filterdata.length + 1,
                        shouldShow: DefaultData.length >= action.index + 3
                    }}
            }
            else if(!action.shouldAddInvoiceUploadQuestion)
            {
                let removedInvoiceUploadLayout = state.data.filter(function(value,){

                    return value.ComponantName!==InvoiceUpload;

                });
                DefaultData= DefaultData.filter(function(value,){

                    return value.ComponantName!==InvoiceUpload;

                });

                // return {...state,data:removedInvoiceUploadLayout}
                if (action.index + 1 === DefaultData.length || action.index + 1 < removedInvoiceUploadLayout.length)
                    return Object.assign({}, state, {
                        data: removedInvoiceUploadLayout,
                        nextQuestionInfo: {
                            title: DefaultData.length > action.index + 2 ? DefaultData[action.index + 2].componentData.title : "",
                            questionNumber: removedInvoiceUploadLayout.length + 1,
                            shouldShow: DefaultData.length >= action.index + 3
                        }
                    });
                else
                    return Object.assign({}, state, {
                        data: removedInvoiceUploadLayout.length === action.index + 1 ? [...removedInvoiceUploadLayout, DefaultData[action.index + 1]] : removedInvoiceUploadLayout,
                        nextQuestionInfo: {
                            title: DefaultData.length > action.index + 2 ? DefaultData[action.index + 2].componentData.title : "",
                            questionNumber: action.index + 3,
                            shouldShow: DefaultData.length >= action.index + 3
                        }
                    });
            }

            break;

        // return [{data: {ComponantName:SingleAndMultiChoiceQuestion,componentData:deepCopy(data1)}}];
        case INITIAL_QUOTE_QUESTIONS :
            let data = getManupulatedData(action.data);
            DefaultData = data.filteredData;
            // return {...state, data: data.filteredData, nextQuestionInfo: data.nextQuestionInfo}
            return Object.assign({}, state, {
                data: [DefaultData[0]],
                questionType: "QUOTE_QUESTION",
                nextQuestionInfo: {title: DefaultData[1].componentData.title, questionNumber: 2, shouldShow: true}
            });
            break;
        case INITIAL_QUOTE_QUESTIONS_FROM_PRICE :
            let dataQuoteQuestions = getManupulatedData(action.data);
            DefaultData = dataQuoteQuestions.filteredData;
            let initData=DefaultData.filter((value) => {
                return value.componentData.isCompleted;
            });
            let initialDataLength=0;
            if(initData.length===DefaultData.length)
            {
                return Object.assign({}, state, {
                    data: DefaultData,
                    questionType: "QUOTE_QUESTION",
                });
            }
            else
            {
                initData.push(DefaultData[initData.length])
                let title='';
                try{
                    title = DefaultData[initData?.length+1]?.componentData?.title;
                }catch (e) {

                }
                return Object.assign({}, state, {
                    data: initData,
                    questionType: "QUOTE_QUESTION",
                    nextQuestionInfo: {title: title, questionNumber: initData?.length+1, shouldShow: true}
                });
            }
            // return {...state, data: data.filteredData, nextQuestionInfo: data.nextQuestionInfo}
            break;

        case INITIAL_ORDER_QUESTIONS :
            let orderQuestionsData = getOrderDataForUI(action.data,action.userInfo);
            DefaultData = orderQuestionsData.filteredData;
            let nextQuestionInfo = {};
            if(DefaultData && DefaultData.length > 1){
                nextQuestionInfo = {title: DefaultData[1].componentData.title, questionNumber: 2, shouldShow: true};
            }
            return Object.assign({}, state, {
                data: [DefaultData[0]],
                questionType: "ORDER_QUESTION",
                nextQuestionInfo: nextQuestionInfo
            });
            break;
        case Actions.API_ERROR_QUESTION_ANSWER:
            if (action.data.message !== undefined && action.data.message === 'Network request failed') {
                return {
                    ...state,
                    data: [...state.data],
                    isError: true,
                    errorMessage: INTERNET_ERROR_MESSAGE_BUYBACK,
                    errorMessageButtonTitle: 'OK',
                    isLoading: false,
                };
            }
            else {
                return {
                    ...state,
                    data: [...state.data],
                    isError: true,
                    errorMessage: action.data.message !== undefined ? action.data.message : TECHNICAL_ERROR_MESSAGE,
                    errorMessageButtonTitle: 'Ok Got It',
                    isLoading: false,
                };
            }
            break;
        default:
            return {...state, isError: false}
    }

    function getManupulatedData(data) {
        let filteredData = [];
        if(buybackManagerObj.isABBFlow()){
            filteredData.push({ComponantName: EnterPincodeQuestion, componentData: {data: PINCODE_QUESTION}})
        }
        const userInfo = parseJSON(buybackManagerObj.getUserInfo());
        for (let i = 0; i < data.length; i++) {
            // if (data[i].questionId === "Q_DEVICE_INVOICE") {
                // filteredData.push({ComponantName: InvoiceUpload, componentData: {data:[]}});
                // filteredData.push({ComponantName: IosIMEI, componentData: data[i]})
            // }
            // else
            if (data[i].questionId === 'Q-CN') {
                if(userInfo?.userName===null || userInfo?.userName===undefined || userInfo?.userName === ""){
                    filteredData.push({ComponantName: EditTextQuestionWithButtonAction, componentData: {
                            ...data[i],
                            inputValidate:isTextAndSpaceOnly,
                            errorMessage:"Please enter a valid name",
                            customerId:userInfo?.customerId
                        }});
                }
            } else if (data[i].questionId === 'Q-ADDR') {
                filteredData.push({ComponantName: EditTextQuestionWithButtonAction, componentData: {...data[i], userInfo: userInfo}});
            } else {
                filteredData.push({ComponantName: SingleAndMultiChoiceQuestion, componentData: data[i]});
            }
        }
        let nextQuestionInfo = {title: data[1].title, questionNumber: 2, shouldShow: true};
        return {filteredData, nextQuestionInfo};
    }

    function getOrderDataForUI(data,userInfo) {
        const userObj=parseJSON(userInfo);
        let filteredData = [];
        const QuestionList=buybackManagerObj.getQuestionsList();
        let shouldPaymentQuestionAdded = false;
        for (let i = 0; i < data?.length; i++) {
            const questionId=data[i]?.questionId;
            if(data[i].questionId === 'Q-CN' && (userObj?.userName===null || userObj?.userName===undefined || userObj?.userName === ""))
            {
                filteredData.push({
                    ComponantName: EditTextQuestionWithButtonAction,
                    componentData: {
                        ...QuestionList[questionId],
                        inputValidate:isTextAndSpaceOnly,
                        errorMessage:"Please enter a valid name",
                        customerId:userObj?.customerId

                    }
                });
            }
            else if(data[i].questionId === 'Q-EMAIL' && (userObj?.userEmail===null || userObj?.userEmail===undefined|| userObj?.userEmail === ""))
            {
                filteredData.push({
                    ComponantName: EditTextQuestionWithButtonAction,
                    componentData: {...QuestionList[questionId],
                        inputValidate:isEmailAddressValid,
                        errorMessage:"Please enter a valid Email Id",
                        customerId:userObj?.customerId
                    }
                });
            } else if (data[i].questionId === 'Q-ADDR') {
                filteredData.push({
                    ComponantName: EditTextQuestionWithButtonAction,
                    componentData: {...QuestionList[questionId], userInfo: userObj},
                });
            }
            else if (data[i].questionId === 'Q_IDENTITY_PROOF')
                filteredData.push({
                    ComponantName: IDProofUpload,
                    componentData: {...QuestionList['Q_IDENTITY_PROOF'],componentData: {data:[]}}
                });
            else if (data[i].questionId === 'Q-PM'){
                shouldPaymentQuestionAdded = true;
            }
        }
        if(shouldPaymentQuestionAdded){
            let paymentMode = {...QuestionList['Q-PM']};
            let filteredDataPayment = [];
            if(buybackManagerObj.getBuybackStatusDataFirstItem().subType !== buybackFlowType.THIRD_PARTY){
                filteredDataPayment = paymentMode?.data.filter((mode) => { return mode.paymentMode !== 'Paytm' })
            }else{
                filteredDataPayment = paymentMode?.data.filter((mode) => { return mode.paymentMode !== 'UPI' })
            }
            paymentMode.data = filteredDataPayment;
            filteredData.push({
                ComponantName: PaymentTypeQuestion,
                componentData: paymentMode,
            })
        }
        let nextQuestionInfo;
        if(filteredData?.length>=2)
        {
            nextQuestionInfo = {title: filteredData[1]?.title, questionNumber: 2, shouldShow: true};
        }
        nextQuestionInfo = {title: filteredData[1]?.title, questionNumber: 2, shouldShow: true};
        return {filteredData, nextQuestionInfo};
    }
};
export default QuestionAndAnswerReducer;
