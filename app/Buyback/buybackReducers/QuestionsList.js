export const Q1 = {
    title: "How’s the mobile and screen condition?",
    isSingleChoice: true,
    isAnswerTypeText: false,
    isSingleLineQuestion: false,
    questionId: 'Q1',
    data: [{answer: "Like new", isSelected: false, answerID: "OPT-FLWLS", description:"Recently bought", imgUrl:"https://uat1cdn.1atesting.in/static/oaapp/android/like_new.png"},
        {answer: "Minor Scratches", isSelected: false, answerID: "OPT-2-3-SCRTCH", description:"Wear & tear",imgUrl:"https://uat1cdn.1atesting.in/static/oaapp/android/minor_scratches.png"},
        {answer: "Average", isSelected: false, answerID: "OPT-SHADED", description:"Heavy scratch/dents",imgUrl:"https://uat1cdn.1atesting.in/static/oaapp/android/average.png"},
        {answer: "Poor", isSelected: false, answerID: "OPT-BRKN", description:"Broken or cracked", imgUrl:"https://uat1cdn.1atesting.in/static/oaapp/android/poor.png"}]
};

export const Q2 = {
    title: "Which original accessories would you sell along with the phone?",
    subTitle: "Include original accessories to get a higher price",
    isSingleChoice: true,
    isAnswerTypeText: true,
    isSingleLineQuestion: false,
    questionId: 'Q2',
    data: [{answer: "Earphone", isSelected: false, answerID: "OPT-OE"},
        {answer: "Box", isSelected: false, answerID: "OPT-OB"},
        {answer: "Original Charger", isSelected: false, answerID: "OPT-OC"},
        {answer: "None", isSelected: false, answerID: "OPT_NONE"}]
};

export const Q3 = {
    title: "Has the mobile undergone repairs?",
    isSingleChoice: true,
    isAnswerTypeText: true,
    isSingleLineQuestion: false,
    questionId: 'Q3',
    data: [
        {answer: "YES", isSelected: false, answerID: "OPT-YES"},
        {answer: "NO", isSelected: false, answerID: "OPT-NO"}
    ]
};

export const Q4 = {
    title: "How old is your mobile?",
    isSingleChoice: true,
    isAnswerTypeText: true,
    isSingleLineQuestion: true,
    questionId: 'Q4',
    data: [{answer: "0 - 3 Months (Invoice required)", isSelected: false, answerID: "OPT-03M"}, {
        answer: "3 - 10 Months (Invoice required)", isSelected: false, answerID: "OPT-310M"},
        {answer: "More than 10 Months", isSelected: false, answerID: "OPT-10PLUS"}]
};
export const Q5 = {
    title: "How’s the condition of back panel/cover?",
    isSingleChoice: true,
    isAnswerTypeText: true,
    isSingleLineQuestion: false,
    questionId: 'Q5',
    data: [{answer: "Flawless", isSelected: false, answerID: "OPT-FLWLS"}, {
        answer: "Scratched", isSelected: false, answerID: "OPT-SCRTCH"
    },
        {answer: "Cracked", isSelected: false, answerID: "OPT-CRCKD"},
        {answer: "Broken", isSelected: false, answerID: "OPT-BRKN"}]
};

export const Q6 = {
    title: "How’s the condition of volume keys, home key & other keys?",
    isSingleChoice: true,
    isAnswerTypeText: true,
    isSingleLineQuestion: false,
    questionId: 'Q6',
    data: [{answer: "Okay", isSelected: false, answerID: "OPT-OK"}, {
        answer: "Loose", isSelected: false, answerID: "OPT-LOOSE"
    },
        {answer: "Missing", isSelected: false, answerID: "OPT-MISSING"},
        {answer: "Broken", isSelected: false, answerID: "OPT-BRKN"}]
};

export const Q7 = {
    title: "How’s the frame/bezel condition around the screen?",
    isSingleChoice: true,
    isAnswerTypeText: true,
    isSingleLineQuestion: false,
    questionId: 'Q7',
    data: [{answer: "Okay", isSelected: false, answerID: "OPT-OK"}, {
        answer: "Discolored", isSelected: false, answerID: "OPT-DSCLRD"
    },
        {answer: "Dented", isSelected: false, answerID: "OPT-DNTD"},
        {answer: "Broken", isSelected: false, answerID: "OPT-BRKN"}]
};

export const Q8 = {
    title: "How’s the condition of main camera?",
    isSingleChoice: true,
    isAnswerTypeText: true,
    isSingleLineQuestion: false,
    questionId: 'Q8',
    data: [{answer: "Flawless", isSelected: false, answerID: "OPT-FLWLS"}, {
        answer: "Scratched", isSelected: false, answerID: "OPT-SCRTCH"
    },
        {answer: "Blur", isSelected: false, answerID: "OPT-BLR"},
        {answer: "Cracked", isSelected: false, answerID: "OPT-CRCKD"},
        {answer: "Broken", isSelected: false, answerID: "OPT-BRKN"}]
};

export const Q_BACKCAMERA = {
    title: "Main Camera (Camera Glass)",
    questionId: 'Q_BACKCAMERA',
};

export const Q_WIFI = {
    title: "Wifi ",
    questionId: 'Q_WIFI',
};

export const Q_VOLUMEDOWNBUTTON = {
    title: "Volum down button ",
    questionId: 'Q_VOLUMEDOWNBUTTON',
};
export const Q11 = {
    title: "Bluetooth ",
    questionId: 'Q11',
};
export const Q_GPS = {
    title: "GPS ",
    questionId: 'Q_GPS',
};
export const Q13 = {
    title: "Charging test (unable to charge the phone) ",
    questionId: 'Q13',
};
export const Q14 = {
    title: "Battery test (Faulty or Very Low Battery Back up)",
    questionId: 'Q14',
};
export const Q15 = {
    title: "Speakers test",
    questionId: 'Q15',
};
export const Q16 = {
    title: "Microphone test ",
    questionId: 'Q16',
};
export const Q17 = {
    title: "GSM (Call Function) test ",
    questionId: 'Q17',
};
export const Q18 = {
    title: "Earphone Jack test (is it damaged or not-working) ",
    questionId: 'Q18',
};
export const Q19 = {
    title: "Fingerprint Sensor test ",
    questionId: 'Q19',
};
export const Q20 = {
    title: "Volume Button not working ",
    questionId: 'Q20',
};
export const Q21 = {
    title: "Power/Home Button Faulty; Hard or Not Working ",
    questionId: 'Q21',
};
export const Q_ADDR = {
    title: "Enter Your address",
    questionId: 'Q-ADDR',
};
export const Q_CMOBL = {
    title: "Enter Your Mobile Number",
    questionId: 'Q-CMOBL',
};
export const Q_PM = {
    title: "Select payment mode",
    questionId: 'Q-PM',
};
export const Q_AHN = {
    title: "Account holder name",
    questionId: 'Q-AHN',
};
export const Q_IMEI = {
    title: "Enter your IMEI number",
    questionId: "Q-IMEI"
};

