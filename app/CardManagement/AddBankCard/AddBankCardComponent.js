import React, {useState, useEffect} from 'react';
import {NativeModules, Platform, SafeAreaView, StyleSheet, Text, View, ScrollView} from 'react-native';
import {HeaderBackButton} from 'react-navigation';
import colors from '../../Constants/colors';
import {style} from '../CardListing/CardListingStyles';
import useBackHandler from '../CustomHooks/useBackHandler';
import ButtonWithLoader from '../../CommonComponents/ButtonWithLoader';
import InputBox from '../../Components/InputBox';
import spacing from '../../Constants/Spacing';
import {CardType, getCardName, getCardTypeName} from '../CardUtils';
import {OATextStyle} from '../../Constants/commonstyles';
import BankCardHeader from '../CardListing/Components/BankCardHeader';
import useCommonComponent from '../CustomHooks/useCommonComponent';
import * as CardAPI from '../DataLayer/CardAPIHelper';
import SelectBankDropDownBox from './component/SelectBankDropDownBox';
import {addEvent, Events} from '../Analytics';
import {logAppsFlyerEvent} from '../../commonUtil/WebengageTrackingUtils';

const bridgeRef = NativeModules.ChatBridge;

export default function AddBankCardComponent(props) {
    const {Alert, BlockingLoader, reloadData, cardState} = useCommonComponent();
    const {navigation} = props;
    let cardType = navigation.getParam('cardType') ?? CardType.creditCard;
    let navigatingFrom = navigation.getParam('navigatingFrom') ?? 'My cards Screen';
    const showErrorMessageInitialValue = null;
    const [cardErrorMessage, setCardErrorMessage] = useState(showErrorMessageInitialValue);
    const [nameErrorMessage, setNameErrorMessage] = useState(showErrorMessageInitialValue);
    const [bankErrorMessage, setBankErrorMessage] = useState(showErrorMessageInitialValue);
    const [issuer, setIssuer] = useState(null);
    const [cardNumber, setCardNumber] = useState('');
    const [cardNumberPayUDetails, setCardNumberPayUDetails] = useState('');
    const [cardHolderName, setCardHolderName] = useState('');
    const {issuers, cardTypes} = cardState;

    console.ignoredYellowBox = ['Warning:'];
    console.disableYellowBox = true;

    useBackHandler(() => onBackPress(), [cardNumber, cardHolderName, issuer]);

    useEffect(() => {
        navigation.setParams({
            onBackPress: () => onBackPress(),
        });
    }, [cardNumber, cardHolderName, issuer]);

    const onBackPress = () => {
        if (cardNumber?.replace(/\W/gi, '')?.length || cardHolderName?.replace(/\W/gi, '')?.length || issuer) {
            Alert.showAlert('Wait! You forgot to save your card.', 'You’ll have to enter card details again next time.', { text: 'I will save this card' }, {
                text: 'Don’t want to save card',
                onClick: () => navigation.pop(),
            });
        } else {
            navigation.pop()
        }
        return true;
    };

    const navigateToSelectBank = () => {
        navigation.navigate('SelectBankComponent', {
            cardType: cardType,
            issuerSelected: setIssuer,
            selectedIssuer: issuer,
        });
    };

    const callCardDetailsApi = (cardBinNumber) => {
        CardAPI.getCardDetailsFromBin(cardBinNumber, (response) => {
            if (response?.status === 'success') {
                setCardNumberPayUDetails(response.data);
                let issuer = issuers.find(bankData => response?.data?.issuingBankCode === bankData?.cardIssuerCode);
                setIssuer(issuer);
                if (!issuer?.cardIssuerCode) {
                    addEvent(Events.payCardBillBankNotDetected, {});
                }
            }
        });
    };

    const incorrectTypeAlert = (correctCardType) => {
        let title, message;
        if (correctCardType === CardType.debitCard) {
            title = 'You entered a debit card number';
            message = 'This card will be saved under your debit card list.';
        } else {
            title = 'You entered a credit card number';
            message = 'This card will be saved under your credit card list.';
        }
        Alert.showAlert(title, message, {
            text: 'Okay, go ahead', onClick: () => {
                cardType = correctCardType;
                addCard();
            },
        }, {
            text: 'Edit details',
        });
    };

    const checkCardValidations = () => {
        let isValid = true;
        setCardErrorMessage(false);
        setNameErrorMessage(false);
        setBankErrorMessage(false);
        let actualCardNumber = cardNumber?.replace(/\W/gi, '');
        if (actualCardNumber?.length < 12 || actualCardNumber?.length > 19 || !/^\d+$/.test(actualCardNumber)) {
            setCardErrorMessage(true);
            isValid = false;
        }

        if (cardHolderName === null || cardHolderName === undefined || cardHolderName.length === 0) {
            setNameErrorMessage(true);
            isValid = false;
        }

        if (issuer === null || issuer === undefined) {
            setBankErrorMessage(true);
            isValid = false;
        }

        if (!isValid) {
            return;
        }
        if (cardNumberPayUDetails?.cardCategory?.toUpperCase() === CardType.creditCard && cardType === CardType.debitCard) {
            incorrectTypeAlert(CardType.creditCard);
            return;
        }

        if (cardNumberPayUDetails?.cardCategory?.toUpperCase() === CardType.debitCard && cardType === CardType.creditCard) {
            incorrectTypeAlert(CardType.debitCard);
            return;
        }
        addCard();
    };

    const addCard = () => {
        let association = cardNumberPayUDetails?.cardType?.length && cardNumberPayUDetails?.cardType.toLowerCase() !== 'unknown' ? cardNumberPayUDetails?.cardType : undefined;
        let issuerName = issuer?.cardIssuerName;
        let issuerCode = issuer?.cardIssuerCode;
        BlockingLoader.startLoader('Adding card');
        addEvent(Events.addCard, {Location: navigatingFrom, 'Card Type': getCardTypeName(cardType, cardTypes)});
        CardAPI.addBankCard(cardNumber.replace(/\W/gi, ''), cardType, issuerName, issuerCode, cardHolderName, (response, error) => {
            BlockingLoader.stopLoader();
            let result;
            if (response?.data[0]?.assetId) {
                result = 'success';
                bridgeRef.refreshCards();
                reloadData(() => navigation.replace('CardDetailComponent', {
                    params: response?.data[0]?.assetId,
                    showAddedCard: true,
                }));
            } else {
                result = 'failure';
                Alert.showAlert('Card not added', error?.error[0]?.message, { text: 'Edit details' }, {
                    text: 'View card list',
                    onClick: () => navigation.pop(),
                });
            }
            addEvent(Events.cardAdded, {
                Result: result,
                'Issuer Name': issuerName,
                'Card Type': getCardTypeName(cardType, cardTypes),
            });
            logAppsFlyerEvent(Events.cardAdded, {
                Result: result,
                'Issuer Name': issuerName,
                'Card Type': getCardTypeName(cardType, cardTypes),
            })
        }, association);
    };

    function setCardNumberValue(text) {
        var trimmedText = text?.replace(/\W/gi, '');
        var index = 0;
        var newText = '';
        while (index < trimmedText.length) {
            if (index % 4 == 0 && index != 0) {
                newText += ' ';
            }
            newText += trimmedText[index];
            index += 1;
        }
        setCardNumber(newText);
        if (trimmedText.length === 6) {
            callCardDetailsApi(trimmedText);
        }
    }

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView contentContainerStyle={{
                flexGrow: 1,
                justifyContent: 'space-between',
            }} keyboardShouldPersistTaps={'always'}  >
                <BankCardHeader/>
                <View style={styles.formViewStyle}>
                    <InputBox
                        containerStyle={[styles.textInputContainer, cardErrorMessage && {marginBottom: spacing.spacing8}]}
                        inputHeading={cardType === CardType.creditCard ? 'Enter Credit Card Number' : 'Enter Debit Card Number'}
                        placeholder={'eg. 5234 2345 4356 7890'}
                        placeholderTextColor={colors.placeHolderColor}
                        isMultiLines={false}
                        keyboardType={'number-pad'}
                        editable={true}
                        value={cardNumber}
                        contextMenuHidden={true}
                        errorMessage={cardErrorMessage ? 'Enter valid card number.' : null}
                        onChangeText={setCardNumberValue}
                        updateFocusChanges={() => setCardErrorMessage(false)}
                    />
                    <InputBox
                        containerStyle={[styles.textInputContainer, cardErrorMessage && {marginBottom: spacing.spacing8}]}
                        inputHeading={'Enter Name on Card'}
                        placeholder={'eg. John Doe'}
                        placeholderTextColor={colors.placeHolderColor}
                        isMultiLines={false}
                        keyboardType={'default'}
                        editable={true}
                        errorMessage={nameErrorMessage ? 'Enter valid name on card.' : null}
                        updateFocusChanges={() => setNameErrorMessage(showErrorMessageInitialValue)}
                        onChangeText={(text) => setCardHolderName(text.trim())}
                    />
                    <SelectBankDropDownBox
                        containerStyle={[styles.textInputContainer, cardErrorMessage && {marginBottom: spacing.spacing8}]}
                        inputHeading={'Select Bank Name'}
                        placeHolderText={'Select'}
                        placeHolderColor={colors.placeHolderColor}
                        value={issuer?.cardIssuerName}
                        errorMessage={bankErrorMessage && (issuer?.cardIssuerName === undefined || issuer?.cardIssuerName === null) ? 'Select valid bank name.' : null}
                        onPress={() => {
                            setBankErrorMessage(false);
                            navigateToSelectBank();
                        }}
                    />
                </View>
                <View style={styles.actionButtonContainer}>
                    <ButtonWithLoader
                        isLoading={false}
                        enable={true}
                        Button={{
                            text: 'Add Card',
                            onClick: checkCardValidations,
                        }}
                    />
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

AddBankCardComponent.navigationOptions = ({navigation}) => {
    const {onBackPress, cardType} = navigation.state.params;
    let title = cardType === CardType.creditCard ? 'Add Credit Card' : 'Add Debit Card';
    return {
        headerTitle: (
            <View style={Platform.OS === 'ios' ? {alignItems: 'center'} : {alignItems: 'flex-start'}}>
                <Text style={style.headerTitle}>{title}</Text>
            </View>
        ),
        headerLeft: (
            <HeaderBackButton tintColor={colors.black} onPress={onBackPress}/>
        ),
    };
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        flexDirection: 'column',
        flex: 1,
    },
    formViewStyle: {
        flex: 1,
        marginHorizontal: spacing.spacing16,
        marginTop: spacing.spacing32,
    }, title: {
        ...OATextStyle.TextFontSize_14_normal,
        lineHeight: 22,
        color: colors.color_212121,
    },
    textInputContainer: {
        width: '100%',
    },
    actionButtonContainer: {
        width: '100%',
        backgroundColor: colors.white,
        padding: spacing.spacing16,
    },
    dividerLineStyle: {
        backgroundColor: colors.color_888F97,
        opacity: 0.22,
        width: '100%',
        height: 1,
    },
});
