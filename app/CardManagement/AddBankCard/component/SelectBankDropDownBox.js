import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View, Image} from 'react-native';
import colors from '../../../Constants/colors';
import spacing from '../../../Constants/Spacing';
import {TextStyle} from '../../../Constants/CommonStyle';

export default function SelectBankDropDownBox(props) {
    const {containerStyle, inputHeading, onPress, numberOfLines, value, errorMessage, placeHolderText, placeHolderColor} = props;
    let borderColor = errorMessage ? colors.inoutErrorMessage : colors.grey;
    return (
        <View style={containerStyle}>
            <Text style={styles.inputHeading}>{inputHeading}</Text>
            <TouchableOpacity onPress={onPress}>
                <View style={[styles.inputBox, {borderColor: borderColor}]}>
                    <Text style={[styles.input, {
                        color: value ? colors.charcoalGrey : placeHolderColor ?? colors.lightGrey,
                    }]}
                          numberOfLines={numberOfLines ?? 1}>{value ?? placeHolderText}
                    </Text>
                    <Image source={require('../../../images/down_arrow.webp')}
                           style={styles.arrowIconStyle}/>
                </View>
            </TouchableOpacity>
            <Text style={[TextStyle.text_12_normal, styles.errorText]}>{errorMessage}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    arrowIconStyle: {
        height: 8,
        width: 8,
        marginRight: spacing.spacing4,
        resizeMode: 'contain',
        tintColor: colors.color_888F97,
    },
    inputBox: {
        marginTop: spacing.spacing12,
        padding: spacing.spacing8,
        borderColor: colors.grey,
        height: 48,
        borderWidth: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    input: {
        ...TextStyle.text_14_normal_charcoalGrey,
        alignSelf: 'center',
        flex: 1,
    },
    inputHeading: {
        fontSize: spacing.spacing12,
        color: colors.charcoalGrey,
        fontFamily: 'Lato-Regular',
    },
    errorText: {
        color: colors.inoutErrorMessage,
    },
});
