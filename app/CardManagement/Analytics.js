import {logWebEnageEvent} from '../commonUtil/WebengageTrackingUtils';

export const Events = {
    ratingPopup: 'Rating Popup',
    ratingSubmitted: 'Rating Submitted',
    payBill: 'Pay Bill',
    addCard: 'Add Card',
    cardAdded: 'Card Added',
    rateNow: 'Rate Now',
    notNow: 'Not Now',
    payBillReminder: 'Pay Bill Reminder',
    payBillReminderSubmit: 'Pay Bill Reminder Submit',
    payBillReminderTurnOff: 'Pay Bill Reminder Turn Off',
    cardRemoveFeedback: 'Card Remove Feedback',
    viewBankRewards: 'View Bank Rewards',
    payCardBillSubmit: 'Pay Card Bill Submit',
    cardBillAlreadyPaid: 'Card Bill Already Paid',
    newPayBillCard: 'New Pay bill card',
    cardTypeTabViews: 'Card Type Tab Views',
    payCardBillBankNotDetected: 'Pay_Card_Bill_Bank_Not_Detected',
    threeDotMenu: 'Three Dot Menu',
    payCardBillDropout: 'Pay Card Bill Dropout',
    payCardBillSuccessScreenview: 'Pay Card Bill Success Screenview',
    removeCard: 'Remove Card',
    editPayBillReminder: 'Edit Pay Bill Reminder',
    allOffers: 'All Offers',
    seeAllOffers: 'See All Offers',
    offerDetails: 'Offer Details',
};

export const addEvent = (event, eventParams, withAppsFlyer = false) => {
    logWebEnageEvent(event, eventParams, withAppsFlyer);
};
