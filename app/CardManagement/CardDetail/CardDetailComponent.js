import React, {useEffect, useRef, useState} from 'react';
import {Image, NativeModules, Platform, SafeAreaView, ScrollView, StyleSheet, Text, View} from 'react-native';
import {HeaderBackButton} from 'react-navigation';
import colors from '../../Constants/colors';
import {stringConstants} from '../DataLayer/StringConstants';
import {TextStyle} from '../../Constants/CommonStyle';
import spacing from '../../Constants/Spacing';
import BankCardHeader from '../CardListing/Components/BankCardHeader';
import BankCardComponent from '../CardListing/Components/BankCardComponent';
import SingleActionMessageComponent from './Components/SingleActionMessageComponent';
import HorizontalOfferListComponent from './Components/HorizontalOfferListComponent';
import {
    networkId,
    isCreditCard,
    cardTypeId,
    dateFromTime,
    addDaysToDate,
    formattedDate,
    getCardTypeName, cardType,
} from '../CardUtils';
import useAppearance from '../CustomHooks/useAppearance';
import * as CardAPI from '../DataLayer/CardAPIHelper';
import useBackHandler from '../CustomHooks/useBackHandler';
import ButtonWithLoader from '../../CommonComponents/ButtonWithLoader';
import PaymentReminderComponent from './Components/PaymentReminderComponent';
import useCommonComponent from '../CustomHooks/useCommonComponent';
import RBSheet from '../../Components/RBSheet';
import ReminderBottomSheetComponent from './Components/ReminderBottomSheetComponent';
import {PLATFORM_OS} from '../../Constants/AppConstants';
import {Menu, MenuTrigger, MenuOptions, MenuOption} from 'react-native-popup-menu';
import call from 'react-native-phone-call';
import {Events, addEvent} from '../Analytics';
import {logAppsFlyerEvent} from '../../commonUtil/WebengageTrackingUtils';

const bridgeRef = NativeModules.ChatBridge;

export default function CardDetailComponent(props) {
    const {Alert, BlockingLoader, cardState, reloadData} = useCommonComponent();
    const {navigation} = props;
    const walletId = navigation.getParam('params', null);
    const showAddedCard = navigation.getParam('showAddedCard', false);
    const showReminderAdded = navigation.getParam('showReminderAdded', false);
    const {allCards, issuers, cardTypes} = cardState;
    const cardData = allCards.find(card => card.walletId === walletId) ?? {};
    const {paymentLink = null, paymentStatusLink = null, contentType = null, reminder = null, bankRewardLink = null, issuerName = null, issuerCode = null, contentNo = null} = cardData;
    const [offerData, setOfferData] = useState([]);
    const [cardAddedVisible] = useAppearance(showAddedCard, false, 3000);
    const sheetRef = useRef(null);
    const dueDate = dateFromTime(reminder?.nextDueDate ?? 0);
    const reminderDate = addDaysToDate(dueDate, -1 * (reminder?.intensity ?? 0));
    const formattedDueDate = formattedDate(dueDate, 'dd mmm');
    const formattedReminderDate = formattedDate(reminderDate, 'dd mmm');
    const [offerLoaderState, setOfferLoaderState] = useState(true);
    const bankIssuer = issuers?.find((issuer) => issuer.cardIssuerCode === issuerCode);
    const issuerContact = bankIssuer?.helpline1 ?? bankIssuer?.helpline2 ?? bankIssuer?.landline1 ?? bankIssuer?.landline2;
    const headerTitle = (issuerName ?? issuerCode) + ' ' + (isCreditCard(contentType) ? 'Credit Card' : 'Debit Card');

    useEffect(() => {
        getOffers();
        updateNavigationHeader();
    }, []);

    useEffect(() => {
        updateNavigationHeader();
    }, [issuerContact]);

    useBackHandler(() => navigation.pop());

    const updateNavigationHeader = () => {
        navigation.setParams({
            walletId: walletId,
            headerTitle: headerTitle,
            issuerContact: issuerContact,
            bankRewardLink: bankRewardLink,
            deleteCard: deleteCard,
            viewOffers: viewOffers,
            viewBankBranches: viewBankBranches,
            viewRewards: viewRewards,
        });
    };

    const deleteCard = () => {
        Alert.showAlert('Delete your saved card?', 'You will not be able to use our additional app benefits for this card.', {text: 'Keep my card saved'}, {
            text: 'Delete card', onClick: () => {
                BlockingLoader.startLoader('Deleting card');
                CardAPI.removeCard(walletId, (response, error) => {
                    BlockingLoader.stopLoader();
                    if (response) {
                        addEvent(Events.removeCard, {'Card Type': getCardTypeName(contentType, cardTypes)});
                        bridgeRef.refreshCards();
                        reloadData(navigateToFeedbacks);
                    } else {
                        Alert.showAlert('Unable to delete card', error?.error[0]?.message, {text: 'OK'});
                    }
                });
            },
        });
    };

    const viewOffers = () => {
        bridgeRef.viewOffers(JSON.stringify(cardData));
    };

    const viewBankBranches = () => {
        bridgeRef.viewBankBranches(JSON.stringify(cardData));
    };

    const viewRewards = () => {
        navigation.navigate('WebViewComponent', {
            url: bankRewardLink,
            loaderMessage: 'You are being redirected to bank’s reward page.',
            headerTitle: headerTitle + ' Rewards',
        });
    };

    const navigateToFeedbacks = () => {
        navigation.replace('DeleteCardFeedbackComponent', {'walletId': walletId});
    };

    const getOffers = () => {
        CardAPI.getCardOffers(issuerCode, cardTypeId(contentType), networkId(contentNo), (response, error) => {
            setOfferLoaderState(false);
            if (response) {
                setOfferData(response.data);
            }
        });
    };

    const removeReminder = () => {
        Alert.showAlert('Turn off your upcoming bill reminder?', 'You may miss your credit card bill payments.', {
            text: 'No, keep me reminded', onClick: (checked) => {
                addEvent(Events.payBillReminderTurnOff, {Value: 'No'});
            },
        }, {
            text: 'Turn off reminder', onClick: (checked) => {
                BlockingLoader.startLoader('Deleting Reminder');
                addEvent(Events.payBillReminderTurnOff, {Value: checked ? 'Yes_All' : 'Yes'});
                CardAPI.deleteReminder(walletId, checked, (response, error) => {
                    BlockingLoader.stopLoader();
                    if (response) {
                        if (Platform.OS === PLATFORM_OS.ios) { // TODO: KAG to be done in Android??
                            bridgeRef.removeReminderForWalletId(Number(walletId), Number(checked));
                        }
                        reloadData();
                    } else {
                        Alert.showAlert('Unable to remove reminder', error?.error[0]?.message, {text: 'OK'});
                    }
                });
            },
        }, {text: 'Turn off all future reminders', selected: false});
    };

    const cardAddedComponent = () => {
        let cardCategory = isCreditCard(contentType) ? 'credit' : 'debit';
        return (
            <View style={style.cardAddedContainer}>
                <Image source={require('../../images/ic_tick_circular_white.webp')} style={style.tickIconStyle}/>
                <Text
                    style={[TextStyle.text_12_bold, style.cardAddedTextStyle]}>{`Great! Your ${cardCategory} card has been successfully added`}</Text>
            </View>
        );
    };

    const PayBillButtonComponent = () => {
        let buttonTitle = stringConstants.payBill;
        return (
            <View style={style.buttonStyle}>
                <ButtonWithLoader
                    isLoading={false}
                    enable={true}
                    Button={{
                        text: buttonTitle,
                        onClick: () => {
                            addEvent(Events.payBill, {Location: 'Card Details', Bank: cardData?.issuerCode});
                            navigation.navigate('PayBillComponent', {
                                params: walletId,
                                setShowReminderAdded: (showReminderAdded) => {
                                    if (showReminderAdded) {
                                        addEvent(Events.newPayBillCard, {Bank: issuerName ?? issuerCode});
                                        sheetRef.current.open();
                                    }
                                },
                            });
                        },
                    }}
                />
            </View>
        );
    };

    const isPaymentEnabled = isCreditCard(contentType) && paymentLink?.length && paymentStatusLink?.length;

    const reminderView = () => {
        if (isPaymentEnabled) {
            if (reminder?.reminderStatus === 'A' || reminder?.lastPaymentDate) {
                return <PaymentReminderComponent
                    style={style.actionMessageCardStyle}
                    dueDate={formattedDueDate}
                    reminderDate={formattedReminderDate}
                    onEdit={() => {
                        addEvent(Events.editPayBillReminder, {Location: 'Card Details'});
                        navigateToReminder('Edit Payment');
                    }}
                    onRemove={removeReminder}
                    isReminderSet={reminder?.reminderStatus === 'A'}/>;
            } else {
                return <SingleActionMessageComponent
                    style={{
                        ...style.actionMessageCardStyle,
                        backgroundColor: colors.color_card_background_light_yellow,
                    }}
                    title={stringConstants.neverMissYouPayment}
                    description={stringConstants.setMonthlyReminder}
                    actionText={stringConstants.setPaymentReminder}
                    onPress={() => {
                        addEvent(Events.payBillReminder, {Location: 'Card Details'});
                        navigateToReminder('Card Details');
                    }}/>;
            }
        } else {
            return null;
        }
    };

    const navigateToReminder = (navigatingFrom) => {
        navigation.navigate('SetReminderComponent', {params: cardData, navigatingFrom: navigatingFrom});
    };

    useEffect(() => {
        if (showReminderAdded) {
            addEvent(Events.newPayBillCard, {Bank: issuerName ?? issuerCode});
            sheetRef.current.open();
        }
    }, [showReminderAdded]);

    return (
        <SafeAreaView style={{flex: 1, backgroundColor: colors.white}}>
            {cardAddedVisible ? cardAddedComponent() : null}
            <BankCardHeader/>
            <ScrollView style={style.container}>
                <View style={style.container}>
                    <BankCardComponent showActions={false} cardData={cardData} style={style.bankCardStyle}/>
                    {reminderView()}
                    <HorizontalOfferListComponent style={style.offersContainerStyle} data={offerData}
                                                  onPress={() => {
                                                      addEvent(Events.seeAllOffers, {
                                                          Location: 'Card Details',
                                                          Bank: issuerName,
                                                      });
                                                      viewOffers();
                                                  }}
                                                  onOfferPress={(offerData) => {
                                                      addEvent(Events.offerDetails, {Location: 'Card Details'});
                                                      logAppsFlyerEvent(Events.offerDetails,{})
                                                      bridgeRef.viewOnlineOfferDetail(JSON.stringify(offerData));
                                                  }}
                                                  isLoadingData={offerLoaderState}/>
                    {bankRewardLink ? <SingleActionMessageComponent
                        style={{
                            ...style.actionMessageCardStyle,
                            marginBottom: spacing.spacing16,
                            backgroundColor: colors.color_card_background_blue,
                        }}
                        description={stringConstants.bankOfferingSomeExcitingRewards}
                        actionText={stringConstants.viewBankRewards}
                        onPress={() => {
                            addEvent(Events.viewBankRewards, {Location: 'Card Details'});
                            viewRewards();
                        }}/> : null}
                </View>
            </ScrollView>
            {isPaymentEnabled ? PayBillButtonComponent() : null}
            <RBSheet
                ref={sheetRef}
                duration={10}
                closeOnSwipeDown={true}
                onClose={() => {
                }}>
                <ReminderBottomSheetComponent
                    dueDate={formattedDueDate}
                    reminderDate={formattedReminderDate}
                    onEdit={() => {
                        sheetRef.current.close();
                        addEvent(Events.editPayBillReminder, {Location: 'Card Detail Bottomsheet'});
                        navigateToReminder('Edit Payment');
                    }}
                    onClose={() => sheetRef.current.close()}/>
            </RBSheet>
        </SafeAreaView>
    );
};

CardDetailComponent.navigationOptions = ({navigation}) => {
    const {
        headerTitle = null, issuerContact = null, bankRewardLink = null, deleteCard = () => {
        }, viewOffers = () => {
        }, viewBankBranches = () => {
        }, viewRewards = () => {
        },
    } = navigation.state.params;
    return {
        headerTitle: (
            <View style={Platform.OS === 'ios' ? {alignItems: 'center'} : {alignItems: 'flex-start'}}>
                <Text style={style.headerTitle}>{headerTitle}</Text>
            </View>
        ),
        headerLeft: (
            <HeaderBackButton tintColor={colors.black} onPress={() => navigation.pop()}/>
        ),
        headerRight: (<Menu>
            <MenuTrigger onPress={() => {
                addEvent(Events.threeDotMenu, {Location: 'Card Details'});
            }}>
                <View style={{padding: spacing.spacing10, marginRight: spacing.spacing18}}>
                    <Image source={require('../../images/ic_dot_menu.webp')}/>
                </View>
            </MenuTrigger>
            <MenuOptions customStyles={{optionsContainer: {marginRight: spacing.spacing18}}}>
                <View style={{marginVertical: spacing.spacing4, marginHorizontal: spacing.spacing18}}>
                    <MenuOption onSelect={() => {
                        addEvent(Events.allOffers, {Location: 'Card Details'});
                        viewOffers();
                    }}>
                        <Text style={style.menuOptionTextStyle}>{stringConstants.viewOffers}</Text>
                    </MenuOption>
                    {bankRewardLink ? <MenuOption onSelect={() => {
                        addEvent(Events.viewBankRewards, {Location: 'Three Dot'});
                        viewRewards();
                    }}>
                        <Text style={style.menuOptionTextStyle}>{stringConstants.viewRewards}</Text>
                    </MenuOption> : null}
                    <MenuOption onSelect={viewBankBranches}>
                        <Text style={style.menuOptionTextStyle}>{stringConstants.findBankBranch}</Text>
                    </MenuOption>
                    {issuerContact ? <MenuOption onSelect={() => call({number: issuerContact, prompt: false})}>
                        <Text style={style.menuOptionTextStyle}>{stringConstants.callYourBank}</Text>
                    </MenuOption> : null}
                    <MenuOption onSelect={deleteCard}>
                        <Text style={style.menuOptionTextStyle}>{stringConstants.deleteCard}</Text>
                    </MenuOption>
                </View>
            </MenuOptions>
        </Menu>),
    };
};

const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    headerTitle: {
        fontSize: 16,
        fontFamily: 'Lato',
        color: colors.charcoalGrey,
    },
    cardAddedContainer: {
        backgroundColor: colors.seaGreen,
        height: spacing.spacing40,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    cardAddedTextStyle: {
        color: colors.white,
        marginVertical: spacing.spacing10,
        marginLeft: spacing.spacing8,
    },
    tickIconStyle: {
        height: 16,
        width: 16,
        resizeMode: 'contain',
        alignSelf: 'center',
    }, bankCardStyle: {
        marginTop: spacing.spacing12,
        marginHorizontal: spacing.spacing16,
    }, actionMessageCardStyle: {
        marginTop: spacing.spacing28,
        marginHorizontal: spacing.spacing16,
    }, offersContainerStyle: {
        marginTop: spacing.spacing28,
    }, buttonStyle: {
        width: '100%',
        backgroundColor: colors.white,
        padding: spacing.spacing16,
        alignSelf: 'flex-end',
        shadowOffset: {
            width: 0,
            height: -2,
        },
        shadowRadius: 2,
        shadowOpacity: 0.07,
        shadowColor: colors.black,
        elevation: 4,
    }, menuOptionTextStyle: {
        ...TextStyle.text_14_normal_charcoalGrey,
        marginVertical: spacing.spacing10,
    },
});
