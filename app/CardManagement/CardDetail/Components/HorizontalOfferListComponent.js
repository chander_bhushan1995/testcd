import { FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import colors from '../../../Constants/colors';
import React, { useState } from 'react';
import spacing from '../../../Constants/Spacing';
import { stringConstants } from '../../DataLayer/StringConstants';
import { TextStyle, CommonStyle } from '../../../Constants/CommonStyle';
import Loader from '../../../Components/Loader';
import images from '../../../images/index.image';

function OfferComponent(props) {
    const { item, isLast, onPress } = props
    const defaultOfferImage = require("../../../images/icon_offer_default.webp")
    const [imageSource, setImageSource] = useState(item?.image?.length ? { uri: item.image } : defaultOfferImage)
    return (
        <TouchableOpacity style={[CommonStyle.cardContainerWithShadow, style.offerContainerStyle, isLast && { marginRight: 16 }]} onPress={onPress}>
            <View style={{ marginLeft: spacing.spacing12, flex: 1, borderRadius: 4 }}>
                <Text style={{ ...TextStyle.text_16_bold, marginVertical: spacing.spacing16 }}>{item.merchantName}</Text>
                <Text style={TextStyle.text_14_normal}>{item.offerTitle}</Text>
            </View>
            <Image style={style.offerIconStyle} source={imageSource} onError={() => setImageSource(defaultOfferImage)} />
        </TouchableOpacity>
    );
};

export default HorizontalOfferListComponent = props => {
    const { data, onPress, isLoadingData, onOfferPress } = props;
    const offersView = () => {
        if (isLoadingData) {
            return (<View style={style.loader}>
                <Loader size={30} isLoading={isLoadingData} />
            </View>)
        } else if (data?.length) {
            return <FlatList
                contentContainerStyle={style.offersContainer}
                data={data}
                nestedScrollEnabled={true}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item, index}) => <OfferComponent item={item} isLast={data.length - 1 === index} onPress={() => onOfferPress(item)} />} />
        } else {
            return (<View style={style.noOffersContainer}>
                <Text style={TextStyle.text_14_normal}>{stringConstants.noOffersDescription}</Text>
            </View>)
        }
    }

    return (
        <View style={[style.container, props.style]}>
            <View style={{ marginHorizontal: spacing.spacing16, flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={style.offerTextStyle}>{stringConstants.cardOffers}</Text>
                {!isLoadingData && data?.length > 0 && <TouchableOpacity onPress={onPress}>
                    <View style={style.headerView}>
                        <Text style={style.ctaTextStyle}>{stringConstants.viewAllOffers}</Text>
                        <Image source={images.rightBlueArrow} style={style.rightArrowStyle} />
                    </View>
                </TouchableOpacity>}
            </View>
            {offersView()}
        </View>
    );
};

const style = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        flexDirection: 'column',
    }, headerView: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
        alignItems: 'center',
        paddingVertical: spacing.spacing10
    }, offersContainer: {
        marginTop: spacing.spacing14,
    }, offerTextStyle: {
        ...TextStyle.text_16_bold,
        marginTop: spacing.spacing8,
    }, ctaTextStyle: {
        ...TextStyle.text_12_bold,
        color: colors.blue028,
        lineHeight: 16,
    }, rightArrowStyle: {
        tintColor: colors.blue028,
        height: 12,
        width: 7,
        resizeMode: 'contain',
        alignSelf: 'center',
        marginLeft: spacing.spacing4,
    }, noOffersContainer: {
        borderRadius: 2,
        backgroundColor: colors.white,
        borderColor: '#EDEDED',
        borderWidth: 1,
        marginTop: spacing.spacing14,
        marginHorizontal: spacing.spacing16,
        paddingHorizontal: spacing.spacing12,
        paddingVertical: spacing.spacing28,
    }, loader: {
        height: 20,
        width: 20,
        alignSelf: 'center',
        marginTop: spacing.spacing28
    }, offerContainerStyle: {
        width: 300,
        height: 100,
        marginLeft: 16,
        borderRadius: 4,
        flexDirection: 'row',
    }, offerIconStyle: {
        width: 100,
        height: 100,
        alignSelf: 'flex-end',
        resizeMode: 'contain',
    },
});
