import React, { useRef, useEffect } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TextStyle } from '../../../Constants/CommonStyle';
import { stringConstants } from '../../DataLayer/StringConstants';
import ReminderViewComponent from './ReminderViewComponent';
import spacing from '../../../Constants/Spacing';
import colors from '../../../Constants/colors';
import CustomSwitch from '../../../Components/CustomSwitch';

export default PaymentReminderComponent = props => {
    const { style, dueDate = '', reminderDate = '', onEdit, isReminderSet = true, onRemove } = props;
    const switchRef = useRef(null);

    useEffect(() => {
        switchRef.current.setValue(isReminderSet)
    }, [isReminderSet])

    return (<View style={[style, viewStyle.container]}>
        <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={TextStyle.text_16_bold}>{stringConstants.paymentReminder}</Text>
            <CustomSwitch
                ref={switchRef}
                defaultValue={isReminderSet}
                value={isReminderSet}
                activeBackgroundColor={colors.blue028}
                inactiveBackgroundColor={'rgba(33,33,33,0.15)'}
                activeButtonBackgroundColor={colors.white}
                inactiveButtonBackgroundColor={colors.white}
                onChangeValue={(value) => {
                    switchRef.current.setValue(isReminderSet)
                    if (value) {
                        onEdit();
                    } else {
                        onRemove();
                    }
                }}
                switchWidth={32}
                switchHeight={15}
                switchBorderRadius={13}
                switchBorderWidth={0}
                buttonWidth={20}
                buttonHeight={20}
                buttonBorderRadius={15}
                buttonBorderColor={'rgba(0, 0, 0, 0.1)'}
                buttonBorderWidth={0.5}
                animationTime={150}
                padding={false} />
        </View>
        {isReminderSet ? <ReminderViewComponent style={{ marginTop: spacing.spacing10 }} dueDate={dueDate}
            reminderDate={reminderDate} onEdit={onEdit} /> : null}
    </View>)
};

const viewStyle = StyleSheet.create({
    container: {
        flexDirection: 'column',
    },
});
