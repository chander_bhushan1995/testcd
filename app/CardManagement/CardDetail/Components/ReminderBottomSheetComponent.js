import React from 'react';
import { Image, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import ReminderViewComponent from './ReminderViewComponent';
import spacing from '../../../Constants/Spacing';
import { TextStyle } from '../../../Constants/CommonStyle';
import colors from '../../../Constants/colors';

export default function ReminderBottomSheetComponent(props) {
    const { dueDate, reminderDate, onEdit, onClose } = props;
    return (<View>
        <View style={{ marginHorizontal: spacing.spacing32, marginTop: spacing.spacing28, flexDirection: 'row' }}>
            <View style={{ flex: 1 }}>
                <Text style={{ ...TextStyle.text_12_normal, color: colors.color_808080 }}>Hey there!</Text>
                <Text style={TextStyle.text_16_regular}>We have set a reminder for you 😀</Text>
            </View>
            <TouchableOpacity style={{ padding: 6 }} onPress={onClose}>
                <Image source={require('../../../images/icon_cross_close_chat.webp')} style={style.crossIconStyle} />
            </TouchableOpacity>
        </View>
        <ReminderViewComponent style={{ marginBottom: spacing.spacing36, marginHorizontal: spacing.spacing16, marginTop: spacing.spacing24 }} dueDate={dueDate} reminderDate={reminderDate} onEdit={onEdit} />
    </View>);
}

const style = StyleSheet.create({
    container: {},
    crossIconStyle: {
        height: 14,
        width: 14,
        resizeMode: 'contain',
    },
});