import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {TextStyle} from '../../../Constants/CommonStyle';
import colors from '../../../Constants/colors';
import spacing from '../../../Constants/Spacing';
import {stringConstants} from '../../DataLayer/StringConstants';

const MessageDateComponent = props => {
    const {date = '', message = ''} = props;
    return (<View style={messageDateStyle.container}>
        <Text style={messageDateStyle.descriptionTextStyle}>{message}</Text>
        <Text style={[TextStyle.text_14_bold, {marginTop: spacing.spacing4}]}>{date}</Text>
    </View>);
};

const ReminderViewComponent = props => {
    const { style, dueDate = '', reminderDate = '', onEdit } = props;

    return (
        <View style={[style, viewStyle.container]}>
            <View style={{ marginHorizontal: spacing.spacing16, flexDirection: 'row' }}>
                <MessageDateComponent date={dueDate} message={stringConstants.yourNextDueDate} />
                <View style={viewStyle.verticleSepartor} />
                <MessageDateComponent date={reminderDate} message={stringConstants.youWillBeRemindedOn} />
            </View>
            <TouchableOpacity style={{
                alignSelf: 'flex-end',
                alignItems: 'center',
                padding: spacing.spacing16,
            }} onPress={onEdit}>
                <Text style={viewStyle.ctaTextStyle}>{stringConstants.edit}</Text>
            </TouchableOpacity>
        </View>
    );
};

const viewStyle = StyleSheet.create({
    container: {
        flexDirection: 'column',
        paddingTop: spacing.spacing16,
        borderRadius: 4,
        backgroundColor: colors.color_card_background_light_yellow,
        borderColor: colors.black,
    }, verticleSepartor: {
        width: 1,
        height: '100%',
        backgroundColor: colors.greye0,
        marginHorizontal: spacing.spacing16,
    }, ctaTextStyle: {
        ...TextStyle.text_12_bold,
        color: colors.blue028,
    }
});

const messageDateStyle = StyleSheet.create({
    container: {
        flexDirection: 'column',
        alignItems: 'center',
        flex: 1,
    }, descriptionTextStyle: {
        ...TextStyle.text_12_bold,
        color: colors.black,
        opacity: 0.54,
    },
});

export default ReminderViewComponent;
