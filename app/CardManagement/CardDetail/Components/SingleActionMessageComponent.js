import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { TextStyle } from '../../../Constants/CommonStyle';
import colors from '../../../Constants/colors';
import spacing from '../../../Constants/Spacing';

export default SingleActionMessageComponent = props => {
    const { style, title, description, actionText = '', onPress } = props;
    return (
        <TouchableOpacity activeOpacity={0.7} style={[style, { ...viewStyle.container }]} onPress={onPress}>
            {title ? <Text style={TextStyle.text_16_bold}>{title}</Text> : null}
            {description ? <Text style={viewStyle.descriptionTextStyle}>{description}</Text> : null}
            <View style={viewStyle.actionView}>
                <Text style={viewStyle.ctaTextStyle}>{actionText}</Text>
                <Image source={require('../../../images/right_arrow.webp')} style={viewStyle.rightArrowStyle} />
            </View>
        </TouchableOpacity>
    );
};

const viewStyle = StyleSheet.create({
    container: {
        flexDirection: 'column',
        paddingHorizontal: spacing.spacing12,
        paddingTop: spacing.spacing14,
        borderRadius: 4,
    }, descriptionTextStyle: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        marginTop: spacing.spacing8,
    }, actionView: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'flex-end',
        alignItems: 'center',
        paddingVertical: spacing.spacing10,
        marginVertical: spacing.spacing8
    }, ctaTextStyle: {
        ...TextStyle.text_12_bold,
        color: colors.blue028,
        lineHeight: 16,
    }, rightArrowStyle: {
        tintColor: colors.blue028,
        height: 12,
        width: 7,
        resizeMode: 'contain',
        alignSelf: 'center',
        marginLeft: spacing.spacing4,
    },

});