import React, { useEffect, useRef } from 'react';
import { NativeModules, Platform, Text, View } from 'react-native';
import { HeaderBackButton } from 'react-navigation';
import ScrollableTabView, { ScrollableTabBar } from '../../scrollableTabView/index';
import colors from '../../Constants/colors';
import { style } from './CardListingStyles';
import OtherCardListingComponent from './Components/OtherCardListingCompnent';
import BankCardListingComponent from './Components/BankCardListingComponent';
import useBackHandler from '../CustomHooks/useBackHandler';
import useCommonComponent from '../CustomHooks/useCommonComponent';
import { TextStyle } from "../../Constants/CommonStyle";
import {allCardsData, isBankCardCategory, isBankCard, categoryIndex, CardType} from '../CardUtils';
import { stringConstants } from '../DataLayer/StringConstants';
import {addEvent, Events} from '../Analytics';
import {APIData} from '../../../index';
import * as CardAPI from '../DataLayer/CardAPIHelper';

const bridgeRef = NativeModules.ChatBridge;

export default function CardListingComponent(props) {
    const { navigation } = props;
    const { Alert, Toast, cardState, markDeeplinkProcessed } = useCommonComponent();
    const { isInitialLoad, allCards, cardTypes, deeplink } = cardState;
    const cardsData = allCardsData(allCards, cardTypes)
    const tabRef = useRef(null)

    useBackHandler(() => bridgeRef.goBack());

    useEffect(() => {
        if (isInitialLoad && allCards.find(card => isBankCard(card.contentType.toUpperCase())) && !deeplink?.length) {
            Toast.showToast(stringConstants.initialLoadToastMessage, 3000);
        }
    }, [isInitialLoad]);

    useEffect(() => {
        if (deeplink?.length) {
            processDeeplink(deeplink);
            markDeeplinkProcessed()
        }
    }, [deeplink]);

    const processDeeplink = (deeplink) => {
        let deeplinkComponents = deeplink.split('/');
        switch (deeplinkComponents[0]) {
            case 'category': {
                let category = categoryIndex(deeplinkComponents[1])
                tabRef.current.goToPage(category);
            }
                break;
            case 'addcard': {
                let cardType = deeplinkComponents[1]?.toUpperCase() ?? CardType.creditCard;
                navigation.navigate('AddBankCardComponent', {
                    cardType: cardType,
                    navigatingFrom: APIData.navigatingFrom ?? 'My cards Screen',
                });
            }
                break;
            case 'paybill': {
                let walletId = deeplinkComponents[1];
                let cardData = allCards.find(card => card.walletId == walletId);
                if (cardData) {
                    addEvent(Events.payBill, {Location: 'My Cards', Bank: cardData?.issuerCode});
                    navigation.navigate('PayBillComponent', { params: cardData.walletId });
                }
            }
                break;
            case 'reminder': {
                let walletId = deeplinkComponents[1];
                let cardData = allCards.find(card => card.walletId == walletId);
                if (cardData) {
                    navigation.navigate('SetReminderComponent', {params: cardData, navigatingFrom: 'My Cards'});
                }
            }
                break;
            case 'detail': {
                let walletId = deeplinkComponents[1];
                let cardData = allCards.find(card => card.walletId == walletId);
                if (cardData) {
                    navigation.navigate('CardDetailComponent', { params: cardData.walletId });
                }
            }
                break;
        }
    };

    return (
        <View style={style.container}>
            <ScrollableTabView initialPage={0}
                prerenderingSiblingsNumber={3}
                onChangeTab={({ i, ref }) => {
                    addEvent(Events.cardTypeTabViews, { Value: ref.props.tabLabel ?? "Credit Cards" })
                }}
                ref={tabRef}
                renderTabBar={() => <ScrollableTabBar tabsContainerStyle={style.tabContainerView}
                    underlineStyle={style.tabBarUnderlineView}
                    textStyle={style.tabBarTextStyle}
                    activeTextColor={colors.blue028}
                    inactiveTextColor={colors.grey} />}
            >
                {cardsData.map((categoryData, index) => {
                    const { category, cardsData } = categoryData
                    return isBankCardCategory(category)
                        ? <BankCardListingComponent tabLabel={category} key={index} dataSource={cardsData[0]} navigation={navigation} />
                        : <OtherCardListingComponent tabLabel={category} key={index} dataSource={cardsData} />
                })}
            </ScrollableTabView>
        </View>
    );
}

CardListingComponent.navigationOptions = () => {
    return {
        headerStyle: { borderBottomWidth: 0, elevation: 0 },
        headerTitle: <View style={Platform.OS === 'ios' ? { alignItems: 'center' } : { alignItems: 'flex-start' }}>
            <Text style={TextStyle.text_16_bold}>{stringConstants.cardListingTitle}</Text>
        </View>,
        headerLeft: <HeaderBackButton tintColor={colors.black} onPress={() => bridgeRef.goBack()} />,
        headerTitleStyle: { color: colors.white },
        headerTintColor: colors.white,
    };
};
