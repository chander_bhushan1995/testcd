import { StyleSheet } from "react-native";
import colors from "../../Constants/colors"
import { TextStyle } from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";

export const style = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: colors.white
    },
    headerTitle: {
        fontSize: 16,
        fontFamily: "Lato",
        color: colors.charcoalGrey
    },
    tabBarTextStyle: {
        fontSize: 13,
        fontFamily: "Lato-Semibold",
        lineHeight: 20
    },
    tabBarUnderlineView: {
        backgroundColor: colors.blue028, 
        height: spacing.spacing4, 
        borderRadius: spacing.spacing2
    },
    tabContainerView: {
        borderBottomColor: colors.grey,
    }
});