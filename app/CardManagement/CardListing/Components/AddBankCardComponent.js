import React from "react";
import {
    StyleSheet,
    View,
    TouchableOpacity,
    Text,
} from 'react-native';
import spacing from "../../../Constants/Spacing";
import colors from "../../../Constants/colors";
import {TextStyle} from "../../../Constants/CommonStyle";

export default function AddBankCardComponent(props) {
    const { title, subtitle, onClick, style } = props;

    return (
        <View style={[style, styles.container]}>
            <TouchableOpacity onPress={onClick} activeOpacity={0.6}>
                <View style={[styles.grayContainer, title?.length && { paddingVertical: spacing.spacing32 }]}>
                    { title ? <Text style={styles.title}>{title}</Text> : null}
                    <Text style={styles.subtitle}>+ {subtitle}</Text>
                </View>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        borderRadius: spacing.spacing4,
        borderWidth: spacing.spacing1,
        borderColor: colors.greye0,
        borderStyle: 'dashed',
        padding: 10
    },
    grayContainer: {
        borderRadius: spacing.spacing4,
        backgroundColor: colors.greyf2,
        paddingVertical: spacing.spacing40,
    },
    title: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        marginLeft: spacing.spacing20,
        marginRight: spacing.spacing20,
        marginBottom: spacing.spacing20,
    },
    subtitle: {
        ...TextStyle.text_16_bold,
        color: colors.blue028,
        alignSelf: "center"
    }
});
