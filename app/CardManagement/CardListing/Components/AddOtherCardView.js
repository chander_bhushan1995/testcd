import React, { Component, useState } from 'react';
import { Modal, View, SafeAreaView, StyleSheet, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';
import colors from "../../../Constants/colors";
import { OATextStyle } from "../../../Constants/commonstyles";
import spacing from "../../../Constants/Spacing";
import InputBox from "../../../Components/InputBox";
import ButtonWithLoader from "../../../CommonComponents/ButtonWithLoader";

export default class AddOtherCardView extends Component {
    state = { modalVisible: false };

    setBottomSheetVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    render() {
        return (
            <View>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => this.setBottomSheetVisible(!this.state.modalVisible)}>
                    <TouchableOpacity
                        activeOpacity={1}
                        style={{
                            flex: 1,
                            flexDirection: 'row',
                            alignItems: 'flex-end',
                            backgroundColor: '#00000077'
                        }}
                        onPress={() => this.setBottomSheetVisible(false)}
                    >
                        <SafeAreaView style={styles.container}>
                            <View style={{
                                alignSelf: 'flex-start',
                                alignItems: 'flex-start',
                                justifyContent: 'flex-start',
                                maxHeight: '80%',
                                flexDirection: 'row',
                                backgroundColor: colors.color_FFFFFF,
                                padding: 0
                            }}>
                                <TouchableWithoutFeedback style={{ flex: 1 }}>
                                    <View style={{ flex: 1 }}>
                                        <AddOtherCardBottomSheet {...this.props} dismiss={() => this.setBottomSheetVisible(false)} />
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                        </SafeAreaView>
                    </TouchableOpacity>
                </Modal>
            </View>
        );

    }
}

function AddOtherCardBottomSheet(props) {
    const { textInputs, onPress, dismiss, buttonTitle } = props
    const showErrorMessageInitialValue = textInputs.map(() => false) 
    const [showErrorMessage, setShowErrorMessage] = useState(showErrorMessageInitialValue);
    const [inputs, setInputs] = useState(textInputs.map(() => ""));

    const setTextAt = (index, text) => {
        let texts = inputs
        texts[index] = text
        setInputs(texts)
    }

    return (
        <View style={styles.bottomsheetContainer}>
            {textInputs.map((item, index) =>
                <InputBox
                    key={index}
                    containerStyle={[styles.textInputContainer, showErrorMessage[index] && { marginBottom: spacing.spacing8 }]}
                    inputHeading={item.title}
                    placeholder={item.placeholder}
                    placeholderTextColor={colors.placeHolderColor}
                    isMultiLines={false}
                    keyboardType={'default'}
                    editable={true}
                    errorMessage={showErrorMessage[index] ? item.error : null}
                    updateFocusChanges={() => setShowErrorMessage(showErrorMessageInitialValue)}
                    onChangeText={(text) => setTextAt(index, text)}
                />
            )}
            <View style={styles.ActionButtonContainer}>
                <ButtonWithLoader
                    isLoading={false}
                    enable={true}
                    Button={{
                        text: buttonTitle,
                        onClick: () => {
                            let outputs = inputs.map((item, index) => !textInputs[index].isValid(item))
                            if (outputs.includes(true)) {
                                setShowErrorMessage(outputs)
                            } else {
                                dismiss()
                                onPress(inputs)
                            }
                        },
                    }}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    bottomsheetContainer: {
        marginHorizontal: spacing.spacing20,
        marginTop: spacing.spacing24,
        marginBottom: spacing.spacing16
    },
    title: {
        ...OATextStyle.TextFontSize_14_normal,
        lineHeight: 22,
        color: colors.color_212121
    },
    textInputContainer: {
        width: '100%',
    },
    ActionButtonContainer: {
        height: 48,
    }
});