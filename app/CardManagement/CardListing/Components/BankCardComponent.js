import React from "react";
import {
    StyleSheet,
    View,
    TouchableOpacity,
    Text,
    Image,
    ImageBackground
} from 'react-native';
import spacing from "../../../Constants/Spacing";
import colors from "../../../Constants/colors";
import { TextStyle } from "../../../Constants/CommonStyle";
import { isCreditCard, daysFromTodayForTime, cardSkin, cardAssociationType } from "../../CardUtils";
import { stringConstants } from "../../DataLayer/StringConstants";

export default function BankCardComponent(props) {
    const { cardData = {}, index, onClick, payBill, setReminder, updatePaymentStatus, style, showActions = true } = props;
    const { reminder = null, paymentLink = null, paymentStatusLink = null, contentType = null, issuerName = null, issuerCode = null, cardSkinType = null, association = null, contentNo = null, nameOnCard = null } = cardData
    const setReminderPayBillView = () => {
        return (
            <View style={styles.doubleButtonView}>
                <TouchableOpacity style={{ width: "50%" }} onPress={setReminder}>
                    <Text style={styles.doubleButton}>{stringConstants.setReminder}</Text>
                </TouchableOpacity>
                <View style={styles.buttonSeparator} />
                <TouchableOpacity style={{ width: "50%" }} onPress={payBill}>
                    <Text style={styles.doubleButton}>{stringConstants.payBill}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    const dueDays = daysFromTodayForTime(reminder?.nextDueDate)

    const payBillView = () => {
        let lastRecordedDateText = null;
        let timeStamp = reminder?.lastPaymentDate;
        if (timeStamp != null, timeStamp != undefined) {
            var nextDueDate = new Date()
            nextDueDate.setTime(timeStamp)
            var dateFormat = require('dateformat');
            lastRecordedDateText = stringConstants.lastPaymentRecorderd + dateFormat(nextDueDate, "dd/mm/yy");
        }
        return (
            <View style={styles.doubleButtonView}>
                <View style={{ alignSelf: "flex-start", flex: 1 }}>
                    <Text style={styles.lastPaymentText}>{lastRecordedDateText}</Text>
                </View>
                <TouchableOpacity style={{ alignSelf: "flex-end" }} onPress={payBill}>
                    <Text style={styles.paymentDueButton}>{stringConstants.payBill}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    const dueDatePayBillView = () => {
        var dueDayTag = ""
        if (dueDays == 0) {
            dueDayTag = "DUE TODAY"
        } else if (dueDays == 1) {
            dueDayTag = "DUE TOMORROW"
        } else {
            dueDayTag = "DUE IN " + dueDays + " DAYS"
        }

        return (
            <View style={styles.doubleButtonView}>
                <View style={{ alignSelf: "flex-start", flex: 1 }} >
                    <Text style={styles.dueTag}>{dueDayTag}</Text>
                </View>
                <TouchableOpacity style={{ alignSelf: "flex-end" }} onPress={payBill}>
                    <Text style={styles.paymentDueButton}>{stringConstants.payBill}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    const overDuePayBillView = () => {
        return (
            <View>
                <View style={styles.doubleButtonView}>
                    <View style={{ alignSelf: "flex-start", flex: 1 }} onPress={setReminder}>
                        <Text style={styles.overDueTag}>OVERDUE</Text>
                    </View>
                    <TouchableOpacity style={{ alignSelf: "flex-end" }} onPress={payBill}>
                        <Text style={styles.paymentDueButton}>{stringConstants.payBill}</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.overDueView}>
                    <Text style={styles.alreadyPaidText}>
                        <Text>{stringConstants.crossedDueDate}</Text>
                        <Text style={{ color: colors.blue028, fontWeight: "bold" }} onPress={updatePaymentStatus}>{stringConstants.alreadyPaid}</Text>
                    </Text>
                </View>
            </View>
        )
    }

    const paymentNotAvailable = () => {
        return (
            <View style={styles.paymentNotAvailable}>
                <Image style={styles.warningImage} source={require("../../../images/icon_warning.webp")} />
                <Text style={styles.paymentNotAvailableText}>{stringConstants.paymentNotAvailable}</Text>
            </View>
        )
    }

    const creditCardActionView = () => {
        if (!(paymentLink?.length && paymentStatusLink?.length)) {
            return paymentNotAvailable()
        } else if (reminder?.reminderStatus !== "A") {
            return setReminderPayBillView()
        } else if (dueDays < 0) {
            return overDuePayBillView()
        } else if (dueDays <= 10) {
            return dueDatePayBillView()
        } else {
            return payBillView()
        }
    }

    const actionView = () => {
        return (showActions && isCreditCard(contentType)) ? creditCardActionView() : null;
    }

    return (
        <View style={[style, styles.shadowContainer]}>
            <View style={styles.container}>
                <TouchableOpacity onPress={onClick} activeOpacity={0.7} disabled={!showActions}>
                    <ImageBackground style={styles.skinImage} imageStyle={styles.skinImageStyle} source={cardSkin(cardSkinType ?? 0)} >
                        <View style={styles.bankView}>
                            <Text style={styles.bankName}>{issuerName?.toUpperCase() ?? issuerCode?.toUpperCase()}</Text>
                            <Image style={styles.cardTypeImageViewLogo} source={cardAssociationType(association, contentNo)} />
                        </View>
                        <Text style={styles.cardNumber}>{contentNo?.replace(/X/gi, "*").replace(/^(.{4})(.{4})(.{4})(.*)$/, "$1 $2 $3 $4")}</Text>
                        <Text style={styles.nameOnCard}>{nameOnCard?.toUpperCase()}</Text>
                    </ImageBackground>
                </TouchableOpacity>
                {actionView()}
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    shadowContainer: {
        shadowColor: colors.grey,
        shadowOffset: { width: 0, height: 1 },
        shadowRadius: 4,
        shadowOpacity: 0.72,
        elevation: 2,
        backgroundColor: colors.white,
        borderRadius: spacing.spacing12,
    },
    container: {
        borderRadius: spacing.spacing12,
        borderWidth: 0.5,
        borderColor: colors.lightGrey2,
        overflow: "hidden"
    },
    skinImage: {
        overflow: "hidden",
    },
    skinImageStyle: {
        margin: -24,
        resizeMode: "cover",
    },
    bankName: {
        ...TextStyle.text_18_bold,
        flex: 1,
        color: colors.white,
        marginTop: spacing.spacing12,
    },
    cardNumber: {
        ...TextStyle.text_18_bold,
        color: colors.white,
        marginTop: spacing.spacing2,
        marginLeft: spacing.spacing16,
    },
    nameOnCard: {
        ...TextStyle.text_12_bold,
        color: colors.white,
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing16,
        marginBottom: spacing.spacing16,
    },
    bankView: {
        flexDirection: "row",
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16
    },
    cardTypeImageViewLogo: {
        width: 50,
        marginRight: 0,
        height: 50,
        alignItems: 'center',
        resizeMode: "contain"
    },
    paymentNotAvailable: {
        flexDirection: "row",
        margin: 4,
        padding: 12,
        borderRadius: 12,
        borderTopRightRadius: 4,
        borderTopLeftRadius: 4,
        backgroundColor: "rgba(255,171,0,0.2)"
    },
    warningImage: {
        width: 24,
        height: 24,
        marginLeft: 8,
        marginRight: 12,
        alignItems: 'center',
        resizeMode: "contain"
    },
    paymentNotAvailableText: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        flex: 1,
        marginRight: 8,
    },
    doubleButtonView: {
        flexDirection: "row",
        alignSelf: "center",
    },
    overDueView: {
        borderTopWidth: 1,
        borderTopColor: colors.greye0,
    },
    alreadyPaidText: {
        ...TextStyle.text_14_normal_charcoalGrey,
        marginBottom: spacing.spacing16,
        marginTop: spacing.spacing12,
        marginHorizontal: spacing.spacing16,
    },
    buttonSeparator: {
        width: 1,
        height: "100%",
        backgroundColor: colors.lightGrey2
    },
    lastPaymentText: {
        ...TextStyle.text_12_medium,
        marginBottom: spacing.spacing16,
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing16,
        color: colors.grey,
        alignSelf: "flex-start",
    },
    dueTag: {
        ...TextStyle.text_10_normal,
        padding: spacing.spacing4,
        borderRadius: spacing.spacing4,
        borderWidth: 1,
        marginBottom: spacing.spacing16,
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing16,
        alignSelf: "flex-start",
        color: colors.saffronYellow,
        borderColor: colors.saffronYellow
    },
    overDueTag: {
        ...TextStyle.text_10_normal,
        padding: spacing.spacing4,
        borderRadius: spacing.spacing4,
        borderWidth: 1,
        marginBottom: spacing.spacing16,
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing16,
        alignSelf: "flex-start",
        color: colors.inoutErrorMessage,
        borderColor: colors.inoutErrorMessage
    },
    paymentDueButton: {
        ...TextStyle.text_14_bold,
        paddingBottom: spacing.spacing16,
        paddingTop: spacing.spacing16,
        paddingRight: spacing.spacing4,
        paddingLeft: spacing.spacing4,
        color: colors.blue,
        marginRight: spacing.spacing16,
        textAlign: "center",
    },
    doubleButton: {
        ...TextStyle.text_14_bold,
        paddingBottom: spacing.spacing16,
        paddingTop: spacing.spacing16,
        paddingRight: spacing.spacing4,
        paddingLeft: spacing.spacing4,
        color: colors.blue,
        textAlign: "center",
    },
});