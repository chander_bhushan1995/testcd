import React from "react";
import {
    StyleSheet,
    View,
    Text,
    Image
} from 'react-native';
import spacing from "../../../Constants/Spacing";
import colors from "../../../Constants/colors";
import { TextStyle } from "../../../Constants/CommonStyle";

export default function BankCardHeader(props) {
    const { style } = props;

    return (
        <View style={[style, styles.container]}>
            <Image style={styles.shieldView} source={require("../../../images/icon_secureShield.webp")} />
            <View style={styles.textView}>
                <Text style={styles.titleView}>
                    <Text style={TextStyle.text_12_normal}>100% Secured: </Text>
                    <Text style={TextStyle.text_12_bold}>PCI DSS approved</Text>
                </Text>
                <Text style={TextStyle.text_10_normal}>powered by Billdesk</Text>
            </View>
            <Image style={styles.pciView} source={require("../../../images/icon_pci_dss.webp")} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        borderBottomWidth: 1,
        borderBottomColor: colors.lightGrey2
    },
    shieldView: {
        marginTop: spacing.spacing16,
        marginBottom: spacing.spacing16,
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing12,
        width: 18,
        height: 25,
        alignItems: 'center',
    },
    textView: {
        flex:1,
    },
    pciView: {
        margin: spacing.spacing16,
        width: 58,
        height: 30
    },
    titleView: {
        marginTop: spacing.spacing12,
    }
});