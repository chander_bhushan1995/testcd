import React, {useRef} from 'react';
import {
    StyleSheet,
    View,
    SafeAreaView,
    ScrollView,
    TouchableOpacity,
    Text,
} from 'react-native';
import spacing from '../../../Constants/Spacing';
import colors from '../../../Constants/colors';
import AddBankCardComponent from './AddBankCardComponent';
import BankCardComponent from './BankCardComponent';
import {TextStyle} from '../../../Constants/CommonStyle';
import BankCardHeader from './BankCardHeader';
import {creditCardRanking, isCreditCard} from '../../CardUtils';
import * as CardAPI from '../../DataLayer/CardAPIHelper';
import useCommonComponent from '../../CustomHooks/useCommonComponent';
import {stringConstants} from '../../DataLayer/StringConstants';
import {addEvent, Events} from '../../Analytics';

export default function BankCardListingComponent(props) {
    const {Alert, BlockingLoader, reloadData} = useCommonComponent();
    const {navigation, dataSource} = props;
    const {cardType, data} = dataSource;
    const addCardText = isCreditCard(cardType) ? stringConstants.addCreditCard : stringConstants.addDebitCard;
    const addCardTitle = isCreditCard(cardType) ? stringConstants.addCreditCardBeneift : stringConstants.addDebitCardBeneift;

    const cardListView = (list) => {
        let sortedList = list;
        if (isCreditCard(cardType)) {
            sortedList?.sort((card1, card2) => creditCardRanking(card1) > creditCardRanking(card2) ? 1 : -1);
        }
        let expandedCell = sortedList?.map((item, index) =>
            <BankCardComponent
                style={[styles.card, index === 0 && { marginTop: 0 }]}
                key={index}
                index={index}
                cardData={item}
                onClick={() => navigateToCardDetail(item.walletId)}
                payBill={() => navigateToPayBill(item)}
                setReminder={() => {
                    addEvent(Events.payBillReminder, {Location: 'My Cards'});
                    navigateToReminder(item);
                }}
                updatePaymentStatus={() => {
                    addEvent(Events.cardBillAlreadyPaid, {Location: 'My Cards'});
                    markBillPaid(item)}}
            />) ?? [];
        expandedCell.push(addMoreCardView(list?.length));
        return expandedCell;
    };

    const addMoreCell = () => {
        return (<TouchableOpacity style={styles.addMoreCell} onPress={navigateToAddCard}>
            <Text style={styles.addMoreCellText}>+ {addCardText}</Text>
        </TouchableOpacity>);
    };

    const addMoreCardView = (listLength) => {
        return <AddBankCardComponent key={listLength + 1} style={styles.card}
                                     title={listLength === 0 ? addCardTitle : null} subtitle={addCardText}
                                     onClick={navigateToAddCard}/>;
    };

    const navigateToAddCard = () => {
        navigation.navigate('AddBankCardComponent', { cardType: cardType });
    };

    const navigateToCardDetail = (walletId) => {
        navigation.navigate('CardDetailComponent', { params: walletId });
    };

    const navigateToPayBill = (data) => {
        addEvent(Events.payBill, {Location: 'My Cards', Bank: data?.issuerCode});
        navigation.navigate('PayBillComponent', {params: data.walletId});
    };

    const navigateToReminder = (data) => {
        navigation.navigate('SetReminderComponent', {
            params: data,
            navigatingFrom:'My Cards'
        });
    };

    const markBillPaid = (data) => {
        BlockingLoader.startLoader(stringConstants.updatingStatus);
        CardAPI.markBillPaid(data.walletId, (response, error) => {
            BlockingLoader.stopLoader();
            if (response) {
                reloadData();
            } else {
                Alert.showAlert(stringConstants.unableToUpdateStatus, error?.error[0]?.message, {text: stringConstants.ok});
            }
        });
    };

    return (
        <SafeAreaView style={styles.SafeArea}>
            <BankCardHeader/>
            <ScrollView style={{flex: 1}}>
                {data?.length > 0 ? addMoreCell() : null}
                {cardListView(data)}
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    SafeArea: {
        flex: 1,
        backgroundColor: 'white',
    },
    addMoreCell: {
        backgroundColor: colors.white,
        alignSelf: 'flex-end',
        marginVertical: spacing.spacing8,
        paddingHorizontal: spacing.spacing16,
        paddingVertical: spacing.spacing10,
    },
    addMoreCellText: {
        ...TextStyle.text_14_bold,
        color: colors.blue028,
    },
    card: {
        margin: spacing.spacing16,
    }
});
