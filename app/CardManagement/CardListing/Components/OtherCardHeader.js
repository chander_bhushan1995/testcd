import React from "react";
import {
    StyleSheet,
    View,
    Text,
    Image
} from 'react-native';
import spacing from "../../../Constants/Spacing";
import colors from "../../../Constants/colors";
import { TextStyle } from "../../../Constants/CommonStyle";

export default function OtherCardHeader(props) {
    const { style } = props;

    return (
        <View style={[style, styles.container]}>
            <Image style={styles.shieldView} source={require("../../../images/icon_secureShield.webp")} />
            <Text style={styles.textView}>100% Secured</Text>
            <Image style={styles.nortonLogo} source={require("../../../images/icon_norton_secured.webp")} />
            <View style={styles.verticleSepartor} />
            <Image style={styles.encryptionLogo} source={require("../../../images/icon_256_bit_encryption.webp")} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        borderBottomWidth: 1,
        borderBottomColor: colors.lightGrey2
    },
    shieldView: {
        marginTop: spacing.spacing16,
        marginBottom: spacing.spacing16,
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing12,
        width: 18,
        height: 25,
    },
    textView: {
        ...TextStyle.text_12_normal,
        flex: 1,
        marginTop: spacing.spacing20,
        marginRight: spacing.spacing16,
    },
    nortonLogo: {
        marginTop: spacing.spacing20,
        marginBottom: spacing.spacing16,
        resizeMode: "contain",
        width: 41,
        height: 20,
    },
    encryptionLogo: {
        marginTop: spacing.spacing20,
        marginBottom: spacing.spacing16,
        marginRight: spacing.spacing16,
        resizeMode: "contain",
        width: 48,
        height: 20,
    },
    verticleSepartor: {
        width: 1,
        backgroundColor: colors.color_F6F6F6,
        marginTop: spacing.spacing20,
        marginBottom: spacing.spacing16,
        marginHorizontal: spacing.spacing8,
    }
});
