import React, { useState, useRef } from "react";
import {
    StyleSheet,
    View,
    SafeAreaView,
    ScrollView,
    TouchableOpacity,
    Text,
} from 'react-native';
import spacing from "../../../Constants/Spacing";
import colors from "../../../Constants/colors";
import { CellWithLeftTitleAndSubtitle, ClickableCell } from "../../../CustomComponent/customcells";
import OtherCardHeader from "./OtherCardHeader";
import { OAImageSource } from "../../../Constants/OAImageSource"
import * as CardAPI from "../../DataLayer/CardAPIHelper"
import useCommonComponent from "../../CustomHooks/useCommonComponent";
import AddOtherCardView from "../Components/AddOtherCardView";
import {addOtherCardData, getCardTypeName} from '../../CardUtils';
import {addEvent, Events} from '../../Analytics';
import {OATextStyle} from '../../../Constants/commonstyles';
import {stringConstants} from '../../DataLayer/StringConstants';
import {logAppsFlyerEvent} from '../../../commonUtil/WebengageTrackingUtils';

export default function OtherCardListingComponent(props) {
    const [expandedIndex, setExpandedIndex] = useState(-1);
    const [addCardType, setAddCardType] = useState("");
    const [addCardTypeId, serAddCardTypeId] = useState(0);

    const { Alert, BlockingLoader, reloadData, cardState } = useCommonComponent()
    const {issuers, cardTypes} = cardState;
    const { dataSource } = props
    const addCardBottomSheetRef = useRef(null);

    const categoryList = (list) => {
        return list?.map((item, index) => {
            return (
                <View style={{ flex: 1 }} key={index}>
                    <ClickableCell
                        item={{
                            title: item.title,
                            imageSource: expandedIndex === index ? OAImageSource.chevron_up : OAImageSource.chevron_down
                        }}
                        index={index}
                        onPress={() => setExpandedIndex(expandedIndex == index ? -1 : index)}
                        titleStyle={{ ...OATextStyle.TextFontSize_14_normal, color: colors.charcoalGrey }}
                        subTitleStyle={{ color: colors.color_EEAA00 }}
                    />
                    {getExpandedViewOfCell(item, index)}
                </View>
            )
        })
    }

    const getExpandedViewOfCell = (categoryData, categoryIndex) => {
        let expandedCell = null;
        if (categoryIndex === expandedIndex) {
            expandedCell = categoryData.data?.map((item, index) => {
                return <CellWithLeftTitleAndSubtitle
                    item={{
                        title: item.issuerName,
                        subTitle: item.contentNo,
                        imageSource: OAImageSource.delete
                    }}
                    index={index}
                    key={index}
                    onPress={() => deleteCard(categoryData, index)}
                    titleStyle={{ ...OATextStyle.TextFontSize_14_bold, color: colors.color_DE000000 }}
                    subTitleStyle={{ color: colors.grey }}
                    containerStyle={{ backgroundColor: colors.color_FAFAFA }}
                />
            })
            if (!(categoryData.isSingleCard && categoryData.data.length >= 1)) {
                expandedCell.push(getAddMoreCell(categoryData, categoryData.data.length));
            }
        }
        return expandedCell;
    };

    const getAddMoreCell = (item, index) => {
        return (<View style={styles.addMoreCell} key={index}>
            <TouchableOpacity style={{ flex: 1 }} onPress={() => openAddCardBottomSheet(item)}>
                <Text style={styles.addMoreCellText}>{index === 0 ? "Add" : "Add More"}</Text>
            </TouchableOpacity>
        </View>)
    }

    const deleteCard = (categoryData, index) => {
        Alert.showAlert("Delete your saved card?", "You may not remember your card details in case of an unexpected situation.", { text: 'Keep my card saved' }, { text: 'Delete card', onClick: () => {
                BlockingLoader.startLoader("Deleting card")
                CardAPI.removeCard(categoryData.data[index].walletId, (response, error) => {
                    BlockingLoader.stopLoader()
                    let result;
                    if (response) {
                        result = 'Success';
                        reloadData();
                    } else {
                        result = 'Failure';
                        Alert.showAlert('Unable to delete card', error?.error[0]?.message, {text: 'OK'});
                    }
                    addEvent(Events.removeCard, {'Card Type': categoryData?.title, Result: result});
                })
            }
        })
    };

    const addCard = (cardNumber, cardType, issuer) => {
        let issuerName = issuer
        let issuerCode = "OTHR"
        if (!issuerName) {
            const supportedIssuers = issuers.filter(issuer => issuer.cardTypeId === addCardTypeId);
            if (supportedIssuers.length === 1) {
                issuerName = supportedIssuers[0].cardIssuerName
                issuerCode = supportedIssuers[0].cardIssuerCode
            }
        }
        BlockingLoader.startLoader("Adding card")
        addEvent(Events.addCard, {Location: 'Add more', 'Card Type' : getCardTypeName(cardType, cardTypes)});
        CardAPI.addCard(cardNumber, cardType, issuerName, issuerCode, "", (response, error) => {
            BlockingLoader.stopLoader()
            let result;
            if (response) {
                result = 'success';
                reloadData()
            } else {
                result = 'failure';
                Alert.showAlert('Unable to add card', error?.error[0]?.message, {text: 'OK'});
            }
            addEvent(Events.cardAdded, {
                Result: result,
                'Issuer Name': issuerName,
                'Card Type': getCardTypeName(cardType, cardTypes),
            });
            logAppsFlyerEvent(Events.cardAdded, {
                Result: result,
                'Issuer Name': issuerName,
                'Card Type': getCardTypeName(cardType, cardTypes),
            })
        })
    };

    const openAddCardBottomSheet = (item) => {
        setAddCardType(item.cardType)
        serAddCardTypeId(item.cardTypeId)
        addCardBottomSheetRef.current.setBottomSheetVisible(true);
    }

    return (
        <SafeAreaView style={styles.SafeArea}>
            <OtherCardHeader />
            <ScrollView style={{ flex: 1 }}>
                {categoryList(dataSource)}
            </ScrollView>
            <AddOtherCardView
                ref={addCardBottomSheetRef}
                textInputs={addOtherCardData(addCardType)}
                buttonTitle={"Add Card"}
                onPress={([cardNumber, issuerName]) => {
                    setTimeout(() => addCard(cardNumber, addCardType, issuerName), 500)
                }}
            />
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    SafeArea: {
        flex: 1,
        backgroundColor: "white"
    },
    addMoreCell: {
        padding: spacing.spacing8,
        backgroundColor: colors.color_FAFAFA,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    addMoreCellText: {
        flex: 1,
        margin: spacing.spacing8,
        color: colors.blue028,
    }
});
