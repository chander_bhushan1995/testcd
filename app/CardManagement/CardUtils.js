export const CardType = {
    creditCard: "CRC",
    debitCard: "DBC",
    aadharCard: "ADC",
    drivingLicense: "DRL",
    panCard: "PNC",
    passport: "PSP",
    registrationCard: "RTC",
    voterIDCard: "VIC",
    clubCard: "CLC",
    healthCard: "HLC",
    loyaltyCard: "LTC",
    prepaidCard: "PRC",
}

export const CardCategories = {
    creditCards: "Credit Cards",
    debitCards: "Debit Cards",
    govtIds: "Govt. IDs",
    otherCards: "Other Cards"
}

export const CardData = [
    {
        category: CardCategories.creditCards,
        cardTypes: [CardType.creditCard]
    },
    {
        category: CardCategories.debitCards,
        cardTypes: [CardType.debitCard]
    },
    {
        category: CardCategories.govtIds,
        cardTypes: [CardType.aadharCard, CardType.drivingLicense, CardType.panCard, CardType.passport, CardType.registrationCard, CardType.voterIDCard]
    },
    {
        category: CardCategories.otherCards,
        cardTypes: [CardType.clubCard, CardType.healthCard, CardType.loyaltyCard, CardType.prepaidCard]
    }
]

export const categoryIndex = (category) => {
    switch (category.toUpperCase()) {
        case "CREDIT": return 0
        case "DEBIT": return 1
        case "GOVTID": return 2
        case "OTHER": return 3
        default: return 0
    }
}

export const isBankCardCategory = (cardCategory) => {
    return [CardCategories.creditCards, CardCategories.debitCards].includes(cardCategory)
}

export const isBankCard = (cardType) => {
    return [CardType.creditCard, CardType.debitCard].includes(cardType)
}

export const isSingleCardType = (cardType) => {
    return [CardType.aadharCard, CardType.drivingLicense, CardType.panCard, CardType.passport, CardType.voterIDCard].includes(cardType.toUpperCase())
}

export const allCardsData = (cards, types) => {
    return CardData.map(item => ({
        category: item.category,
        cardsData: item.cardTypes.map(cardType => {
            const cardTypeObj = types.find((type) => type.cardTypeCode.toUpperCase() === cardType)
            return {
                title: cardTypeObj?.cardTypeName,
                cardTypeId: cardTypeObj?.cardTypeID,
                cardType: cardType,
                isSingleCard: isSingleCardType(cardType),
                data: cards.filter(card => cardType === card.contentType.toUpperCase())
            }
        })
    }))
}

export const isCreditCard = cardType => cardType?.toUpperCase() === CardType.creditCard

export const addOtherCardData = (cardType) => {
    switch (cardType) {
        case CardType.aadharCard: return [{
            title: "Enter Aadhar Card Number",
            placeholder: "e.g. 333333333333",
            error: "Enter valid card number.",
            isValid: (input) => {
                let output = input.replace(/ /g, "")
                return output.length > 0 && output.length <= 25
            }
        }]
        case CardType.drivingLicense: return [{
            title: "Enter Driving License Number",
            placeholder: "e.g. 333333333333",
            error: "Enter valid card number.",
            isValid: (input) => {
                let output = input.replace(/ /g, "")
                return output.length > 0 && output.length <= 25
            }
        }]
        case CardType.panCard: return [{
            title: "Enter PAN Card Number",
            placeholder: "e.g. 333333333333",
            error: "Enter valid card number.",
            isValid: (input) => {
                let output = input.replace(/ /g, "")
                return output.length > 0 && output.length <= 25
            }
        }]
        case CardType.passport: return [{
            title: "Enter Passport Number",
            placeholder: "e.g. 333333333333",
            error: "Enter valid card number.",
            isValid: (input) => {
                let output = input.replace(/ /g, "")
                return output.length > 0 && output.length <= 25
            }
        }]
        case CardType.registrationCard: return [{
            title: "Enter Registration Card Number",
            placeholder: "e.g. 333333333333",
            error: "Enter valid card number.",
            isValid: (input) => {
                let output = input.replace(/ /g, "")
                return output.length > 0 && output.length <= 25
            }
        }]
        case CardType.voterIDCard: return [{
            title: "Enter Voter Id Card Number",
            placeholder: "e.g. 333333333333",
            error: "Enter valid card number.",
            isValid: (input) => {
                let output = input.replace(/ /g, "")
                return output.length > 0 && output.length <= 25
            }
        }]
        case CardType.clubCard: return [{
            title: "Enter Club Card Number",
            placeholder: "e.g. 333333333333",
            error: "Enter valid card number.",
            isValid: (input) => {
                let output = input.replace(/ /g, "")
                return output.length > 0 && output.length <= 25
            }
        }, {
            title: "Enter Club Card Issuer’s Name",
            placeholder: "e.g. Club Mahindra",
            error: "Enter valid issuer name",
            isValid: (input) => {
                let output = input.replace(/ /g, "")
                return output.length > 0
            }
        }]
        case CardType.healthCard: return [{
            title: "Enter Health Card Number",
            placeholder: "e.g. 333333333333",
            error: "Enter valid card number.",
            isValid: (input) => {
                let output = input.replace(/ /g, "")
                return output.length > 0 && output.length <= 25
            }
        }, {
            title: "Enter Health Card Issuer’s Name",
            placeholder: "e.g. ICICI Lombard",
            error: "Enter valid issuer name",
            isValid: (input) => {
                let output = input.replace(/ /g, "")
                return output.length > 0
            }
        }]
        case CardType.loyaltyCard: return [{
            title: "Enter Loyalty Card Number",
            placeholder: "e.g. 333333333333",
            error: "Enter valid card number.",
            isValid: (input) => {
                let output = input.replace(/ /g, "")
                return output.length > 0 && output.length <= 25
            }
        }, {
            title: "Enter Loyalty Card Issuer’s Name",
            placeholder: "e.g. Shopper's Stop",
            error: "Enter valid issuer name",
            isValid: (input) => {
                let output = input.replace(/ /g, "")
                return output.length > 0
            }
        }]
        case CardType.prepaidCard: return [{
            title: "Enter Prepaid Card Number",
            placeholder: "e.g. 333333333333",
            error: "Enter valid card number.",
            isValid: (input) => {
                let output = input.replace(/ /g, "")
                return output.length > 0 && output.length <= 25
            }
        }, {
            title: "Enter Prepaid Card Issuer’s Name",
            placeholder: "e.g. Airtel",
            error: "Enter valid issuer name",
            isValid: (input) => {
                let output = input.replace(/ /g, "")
                return output.length > 0
            }
        }]
        default: return null
    }
}

export const addBankCardData = (cardType) => {
    switch (cardType) {
        case CardType.creditCard: return [{
            title: "Enter Credit Card Number",
            placeholder: "e.g. 333333333333",
            error: "Enter valid card number."
        },{
            title: "Enter Name on Card",
            placeholder: "e.g. Rakesh Aggarwal",
            error: "Enter valid name on card."
        }, {
            title: "Select Bank Name",
            placeholder: "e.g. ICICI Bank",
            error: "Select valid bank name."
        }];
        case CardType.debitCard: return [{
            title: "Enter Debit Card Number",
            placeholder: "e.g. 333333333333",
            error: "Enter valid card number."
        },{
            title: "Enter Name on Card",
            placeholder: "e.g. Rakesh Aggarwal",
            error: "Enter valid name on card."
        }, {
            title: "Select Bank Name",
            placeholder: "e.g. ICICI Bank",
            error: "Select valid bank name."
        }]
    }
};

export const creditCardRanking = (cardData) => {
    const { paymentLink, paymentStatusLink, reminder } = cardData
    if (!(paymentLink?.length && paymentStatusLink?.length)) {
        return 10000
    } else if (reminder?.reminderStatus !== "A") {
        return -10000
    } else {
        return daysFromTodayForTime(reminder?.nextDueDate)
    }
}

export const BankCardCategory = {
    invalid: 'INVALID',
    unknown: 'UNKNOWN',
    rupay: "RUPAY",
    diners: 'DINERS',
    amex: 'AMEX',
    visa: 'VISA',
    master: 'MASTER',
    maestro: 'MAESTRO'
};

export const cardType = (cardNumber) => {
    let type = BankCardCategory.invalid
    let length = cardNumber?.length ?? 0
    if (length >= 12 && length <= 19) {
        type = BankCardCategory.unknown
        if (length == 15 && (cardNumber.startsWith("34") || cardNumber.startsWith("37"))) {
            type = BankCardCategory.amex
        } else if (length == 16 && cardNumber.startsWith("4")) {
            type = BankCardCategory.visa
        } else if (length == 16 && (checkForRange(cardNumber, 4, 2221, 2720) || checkForRange(cardNumber, 2, 51, 55))) {
            type = BankCardCategory.master
        } else if (length == 16 && (cardNumber.startsWith("60") || cardNumber.startsWith("6521") || cardNumber.startsWith("6522"))) {
            type = BankCardCategory.rupay
        } else if ((length >= 14 && (cardNumber.startsWith("36"))) || (length >= 16 && (checkForRange(cardNumber, 3, 300, 305) || cardNumber.startsWith("3095") || checkForRange(cardNumber, 2, 38, 39)))) {
            type = BankCardCategory.diners
        } else if (cardNumber.startsWith("6759") || cardNumber.startsWith("676770") || cardNumber.startsWith("676774") || cardNumber.startsWith("50") || checkForRange(cardNumber, 2, 56, 69)) {
            type = BankCardCategory.maestro
        }
    }

    return type
}

const checkForRange = (number, startingDigits, rangeStart, rangeEnd) => {
    let intNumber = parseInt(number.slice(0, startingDigits))
    return intNumber >= rangeStart && intNumber <= rangeEnd
}

const testLuhn = (value) => {
    // The Luhn Algorithm. It's so pretty.
    // accept only digits, dashes or spaces
    if (/[^0-9-\s]+/.test(value) || value === null || value === undefined || value.length === 0) return false;
    var nCheck = 0, bEven = false;
    value = value.replace(/\D/g, "");

    for (var n = value.length - 1; n >= 0; n--) {
        var nDigit = parseInt(value.charAt(n), 10);

        if (bEven && (nDigit *= 2) > 9) {
            nDigit -= 9;
        }

        nCheck += nDigit;
        bEven = !bEven;
    }

    return (nCheck % 10) == 0;
}

export const networkId = (cardNumber) => {
    let category = 0
    if (cardNumber.length == 16) {
        let firstNumber = cardNumber.first
        if (firstNumber == "4") {
            category = 1
        } else if (firstNumber == "5" || firstNumber == "6") {
            category = 2
        }
    }

    return category
}

export const cardTypeId = (contentType) => {
    let cardTypeId = 0
    if (contentType.toUpperCase() === CardType.creditCard) {
        cardTypeId = 1
    } else if (contentType.toUpperCase() === CardType.debitCard) {
        cardTypeId = 2
    }

    return cardTypeId
}

/// MARK: Images

export const cardSkin = (imageId) => {
    switch (imageId) {
        case 1: return require("../images/card_skin_1.webp")
        case 2: return require("../images/card_skin_2.webp")
        case 3: return require("../images/card_skin_3.webp")
        case 4: return require("../images/card_skin_4.webp")
        case 5: return require("../images/card_skin_5.webp")
        case 6: return require("../images/card_skin_6.webp")
        case 7: return require("../images/card_skin_7.webp")
        case 8: return require("../images/card_skin_8.webp")
        case 9: return require("../images/card_skin_9.webp")
        case 10: return require("../images/card_skin_10.webp")
        case 11: return require("../images/card_skin_11.webp")
        case 12: return require("../images/card_skin_12.webp")
        case 13: return require("../images/card_skin_13.webp")
        default: return require("../images/card_skin_0.webp")
    }
}

export const cardAssociationType = (association, contentNo) => {
    switch (association ?? cardType(contentNo)) {
        case BankCardCategory.amex: return require("../images/icon_card_amex.webp")
        case BankCardCategory.diners: return require("../images/icon_card_diners.webp")
        case BankCardCategory.maestro: return require("../images/icon_card_maestro.webp")
        case BankCardCategory.master: return require("../images/icon_card_master.webp")
        case BankCardCategory.rupay: return require("../images/icon_card_rupay.webp")
        case BankCardCategory.visa: return require("../images/icon_card_visa.webp")
        default: return null
    }
}


/// MARK: DATE FUNCTIONS

const MS_PER_DAY = 1000 * 60 * 60 * 24;

export const formattedDate = (date, format) => {
    let dateFormat = require('dateformat');
    return dateFormat(date, format)
}

export const dateFromTime = (time) => {
    var date = new Date()
    date.setTime(time)
    return date
}

export const dateDiffInDays = (startDate, endDate) => {
    const utc1 = Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());
    const utc2 = Date.UTC(endDate.getFullYear(), endDate.getMonth(), endDate.getDate());
    return Math.floor((utc2 - utc1) / MS_PER_DAY);
}

export const daysFromToday = (endDate) => {
    var now = new Date()
    return dateDiffInDays(now, endDate)
}

export const daysFromTodayForTime = (time) => {
    return daysFromToday(dateFromTime(time))
}

export const addYearsToDate = (date, years) => {
    return new Date(date.getFullYear() + years, date.getMonth(), date.getDate());
}

export const addMonthsToDate = (date, months) => {
    var month = date.getMonth()
    var year = date.getFullYear()
    var increasedMonth = month + months
    var updatedMonth = increasedMonth % 12
    var updatedYear = year + (increasedMonth / 12)
    return new Date(updatedYear, updatedMonth, date.getDate());
}

export const addDaysToDate = (date, days) => {
    var time = date.getTime()
    var updatedTime = time + (days * MS_PER_DAY)
    return dateFromTime(updatedTime)
}

export const addYearsToToday = (years) => {
    return addYearsToDate(new Date(), years)
}

export const addMonthsToToday = (months) => {
    return addMonthsToDate(new Date(), months)
}

export const addDaysToToday = (days) => {
    return addDaysToDate(new Date(), days)
}

export const isExpired = (cacheDate, cacheDays) => {
    return addDaysToDate(dateFromTime(cacheDate), cacheDays) < new Date()
}

export const getCardTypeName = (cardType, types) => {
    return types.find((type) => type.cardTypeCode.toUpperCase() === cardType)?.cardTypeName;
}
