import { useState, useEffect } from "react";

export default function useAppearance(initialIsVisible = false, updatedIsVisible = true, time = 3000) {
    const [isVisible, setIsVisible] = useState(initialIsVisible)

    useEffect(() => {
        if (initialIsVisible !== updatedIsVisible) {
            setTimeout(() => setIsVisible(updatedIsVisible), time);
        }
    }, []);

    return [isVisible]
}