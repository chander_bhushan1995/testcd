import { useEffect } from "react";
import { BackHandler } from 'react-native';

export default function useBackHandler(onBackPress, inputArray = []) {
    useEffect(() => {
        BackHandler.addEventListener("hardwareBackPress", onBackPress);
        return () => {
            BackHandler.removeEventListener("hardwareBackPress", onBackPress);
        }
    }, inputArray)
}