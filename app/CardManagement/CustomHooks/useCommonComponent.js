import { useContext } from "react";
import { CardContext } from "../Navigation/index.cardmanagement";


export default function useCommonComponent() {
   return useContext(CardContext)
}