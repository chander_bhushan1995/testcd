import { useState } from "react";

export default function useLoader(initialIsLoading = false, initialLoaderMessage = "") {
    const [isLoading, setIsLoading] = useState(initialIsLoading)
    const [loaderMessage, setLoaderMessage] = useState(initialLoaderMessage)

    const startLoading = (message) => {
        setLoaderMessage(message)
        setIsLoading(true)
    }

    const stopLoading = () => {
        setIsLoading(false)
        setLoaderMessage("")
    }
    
    return [isLoading, loaderMessage, startLoading, stopLoading]
}