export const RESET_DEEPLINK = 'RESET_DEEPLINK';
export const GET_CARDS = 'GET_CARDS';
export const GET_ISSUERS = 'GET_ISSUERS';
export const GET_CARD_TYPES = 'GET_CARD_TYPES';
export const ERROR = 'ERROR';