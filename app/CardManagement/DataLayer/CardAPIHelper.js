import { HTTP_GET, HTTP_POST, executeApi } from '../../network/ApiManager';

import {APIData} from '../../../index';
import {updateReminder, cardTypes, cardOffers, allCards, issuers, cardDetailFromBin, feedbacks   } from './CardAPIUrls';
import {cardType} from '../CardUtils';

const getDataFromApiGateway = (apiParams) => {
    let params = APIData;
    apiParams.apiProps = {baseUrl: params?.api_gateway_base_url, header: params?.apiHeader};
    return apiParams;
};

const custId = () => {
    return APIData?.customer_id;
};

export const getAllCards = (callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: allCards + custId() + '/cards?status=A',
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams));
};

export const addCard = (number, type, issuerName, issuerCode, name, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: allCards + custId() + '/card',
        requestBody: {
            assets: [{
                assetName: '',
                association: cardType(number),
                contentNo: number,
                contentType: type,
                issuerName: issuerName,
                issuerCode: issuerCode,
                nameOnCard: name,
                prodCode: 'WP01',
            }],
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const addBankCard = (number, type, issuerName, issuerCode, name, callback, association) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: allCards + custId() + '/card',
        requestBody: {
            assets: [{
                association: association ?? cardType(number),
                contentNo: number,
                contentType: type,
                issuerName: issuerName,
                issuerCode: issuerCode,
                nameOnCard: name,
                prodCode: 'WP01',
            }],
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const removeCard = (walletId, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: allCards + custId() + '/card',
        requestBody: {
            assets: [{
                assetId: walletId,
                prodCode: 'WP01',
                status: 'X',
            }],
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getBankCardTypes = (callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: cardTypes,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getCardOffers = (cardIssuerCode, cardTypeId, networkId, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: cardOffers,
        requestBody: {
            query: {
                addedCards: [],
                otherCards: [{
                    cardIssuerCode: cardIssuerCode,
                    cardTypeId: cardTypeId,
                    networkId: networkId
                }],
                customerId: custId(),
                offerType: 'Online',
                pageNo: 1,
                pageSize: 10,
            }
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const setReminder = (walletId, nextDueDate, intensity, callback) => {
    const requestBody = {
        reminderStatus: 'A',
    };
    if (nextDueDate !== null) {
        requestBody.nextDueDate = nextDueDate;
    }
    if (intensity !== null) {
        requestBody.intensity = intensity;
    }

    let apiParams = {
        httpType: HTTP_POST,
        url: updateReminder + walletId + '/update-reminder',
        requestBody: requestBody,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const deleteReminder = (walletId, deleteAll, callback) => {
    const requestBody = {
        reminderStatus: 'X',
    };

    if (!deleteAll) {
        requestBody.disableForCurrentMonth = true;
    }

    let apiParams = {
        httpType: HTTP_POST,
        url: updateReminder + walletId + '/update-reminder',
        requestBody: requestBody,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const markBillPaid = (walletId, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: updateReminder + walletId + '/update-reminder',
        requestBody: {
            paymentStatus: 'DONE',
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getIssuers = (callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: issuers,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getCardTypes = (callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: cardTypes,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getCardDetailsFromBin = (cardBin, callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: cardDetailFromBin + '?bin=' + cardBin,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getFeedbackReasons = (callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: feedbacks + custId() + '/question-answer?categoryCode=CARD_DELETE',
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const submitFeedback = (walletId, questionId, reducedOptionIds, ratingFeedbackText, callback) => {
    let requestBody = {
        questionId: questionId,
        categoryCode: 'CARD_DELETE',
        assetId: walletId,
        selectedOptionList: reducedOptionIds,
    };
    if (ratingFeedbackText.length) {
        requestBody = { ...requestBody, otherAnswerList: [{ text: ratingFeedbackText }] };
    }

    let apiParams = {
        httpType: HTTP_POST,
        url: feedbacks + custId() + '/question-answer',
        requestBody: requestBody,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};
