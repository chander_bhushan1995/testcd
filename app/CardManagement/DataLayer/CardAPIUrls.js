export const allCards = 'myaccount/api/customer/';
export const updateReminder = 'myaccount/api/customer/card/';
export const addRemoveCard = 'myaccount/api/membership/';
export const cardOffers = 'cardgenie/api/v1/aggregated-offers';
export const cardTypes = 'cardgenie/cards/types';
export const issuers = 'cardgenie/cards/issuers';
export const cardDetailFromBin = 'payment-service/api/payment/getCardDetail';
export const feedbacks = 'myaccount/api/customer/';

