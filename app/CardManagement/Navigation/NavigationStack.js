import {createStackNavigator} from 'react-navigation';

import CardDetailComponent from '../CardDetail/CardDetailComponent';
import CardListingComponent from '../CardListing/CardListingComponent';
import AddBankCardComponent from '../AddBankCard/AddBankCardComponent';
import PayBillComponent from '../PayBill/PayBillComponent';
import WebViewComponent from '../PayBill/WebViewComponent';
import SetReminderComponent from '../SetReminder/SetReminderComponent';
import PayBillSuccessComponent from '../PayBill/PayBillSuccessComponent';
import SelectBankComponent from '../SelectBank/SelectBankComponent';

import Dialog from '../../Components/DialogView';
import DeleteCardFeedbackComponent from '../deletefeedback/DeleteCardFeedbackComponent';

const navigator = createStackNavigator({
    CardListingComponent: {
        screen: CardListingComponent,
    },
    CardDetailComponent: {
        screen: CardDetailComponent,
    },
    AddBankCardComponent: {
        screen: AddBankCardComponent,
    },
    PayBillComponent: {
        screen: PayBillComponent,
        navigationOptions: {
            gesturesEnabled: false
        }
    },
    WebViewComponent: {
        screen: WebViewComponent,
    },
    DeleteCardFeedbackComponent: {
        screen: DeleteCardFeedbackComponent,
    },
    PayBillSuccessComponent: {
        screen: PayBillSuccessComponent,
        navigationOptions: {
            gesturesEnabled: false,
            headerMode: "none",
            mode: 'model',
            header: null,
        }
    },
}, {});

const navigator1 = createStackNavigator({
    Main: {
        screen: navigator,
    },
    SetReminderComponent: {
        screen: SetReminderComponent,
    },
    Dialog: {
        screen: Dialog,
    },
    SelectBankComponent: {
        screen: SelectBankComponent,
    },
}, {
    mode: 'modal',
    headerMode: 'none',
    initialRouteName: 'Main',
});

export default navigator1;
