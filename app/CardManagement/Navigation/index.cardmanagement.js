import React, { useRef, useReducer, useEffect } from 'react';
import { Platform, NativeModules } from 'react-native';
import AppNavigator from './NavigationStack';
import { PLATFORM_OS } from '../../Constants/AppConstants';
import useLoader from '../CustomHooks/useLoader';
import BlockingLoader from '../../CommonComponents/BlockingLoader';
import DialogView from '../../Components/DialogView';
import Toast, { DURATION } from '../../CommonComponents/Toast';
import { MenuProvider } from 'react-native-popup-menu';
import colors from '../../Constants/colors';
import { TextStyle } from '../../Constants/CommonStyle';
import * as CardAPI from '../DataLayer/CardAPIHelper';
import * as Actions from '../DataLayer/Actions';
import { isExpired } from '../CardUtils';
import codePush from "react-native-code-push";
import * as Store from '../../commonUtil/Store';
import { parseJSON } from "../../commonUtil/AppUtils";
import {setAPIData, updateAPIData} from "../../../index";

const bridgeRef = NativeModules.ChatBridge;

export const CardContext = React.createContext();

const initialCardsState = {
    isInitialLoad: false,
    allCards: [],
    cardTypes: [],
    issuers: [],
    deeplink: null,
    error: null,
};

const reducer = (state = initialCardsState, action) => {
    switch (action.type) {
        case Actions.GET_CARDS:
            return {
                ...state,
                allCards: action.data?.cards ?? [],
                isInitialLoad: action.isInitialLoad ?? false,
                deeplink: action.deeplink ?? null,
            };
        case Actions.GET_ISSUERS:
            return {
                ...state,
                issuers: action.data
            };
        case Actions.GET_CARD_TYPES:
            return {
                ...state,
                cardTypes: action.data
            };
        case Actions.RESET_DEEPLINK:
            return {
                ...state,
                deeplink: null
            };
        case Actions.ERROR:
            return state;
        default:
            return state;
    }
};

function Root(props) {
    setAPIData(props);
    console.ignoredYellowBox = ['Warning:'];
    console.disableYellowBox = true;

    const [isBlockingLoader, blockingLoaderMessage, startBlockingLoader, stopBlockingLoader] = useLoader();

    const toastRef = useRef(null);
    const showToast = (message, timeout = DURATION.LENGTH_SHORT) => toastRef.current.show(message, timeout, true);

    const alertRef = useRef(null);
    const showAlert = (title, alertMessage, primaryButton = { text: 'Yes', onClick: () => { } }, secondaryButton, checkBox) => {
        alertRef.current.showDailog({
            title: title,
            message: alertMessage,
            primaryButton: primaryButton,
            secondaryButton: secondaryButton,
            checkBox: checkBox,
            cancelable: true,
            onClose: () => { },
        });
    };

    const [cardState, cardDispatch] = useReducer(reducer, initialCardsState);

    useEffect(() => {
        updateAPIData(props)
    }, [props])
    
    useEffect(() => {
        startBlockingLoader('Fetching cards');
        getCardTypes();
        getCards(true, props.deeplink_url_string);
        getIssuers();
    }, []);

    const getCards = (isInitialLoad, deeplink = null, callback = null) => {
        CardAPI.getAllCards((response, error) => {
            stopBlockingLoader();
            if (response) {
                cardDispatch({ type: Actions.GET_CARDS, data: response?.data, isInitialLoad: isInitialLoad, deeplink: deeplink });
                if (callback) callback()
            } else {
                showAlert("Unable to get cards", "Please check your internet connection", { text: "Ok" })
            }
        });
    };

    const getIssuers = () => {
        if (Platform.OS === PLATFORM_OS.ios) {
            bridgeRef.getIssuers(data => {
                let issuersData = parseJSON(data)
                if (issuersData && !isExpired(issuersData?.cacheDate, 2) && issuersData?.data) {
                    cardDispatch({ type: Actions.GET_ISSUERS, data: issuersData?.data });
                } else {
                    CardAPI.getIssuers((response, error) => {
                        if (response?.data) {
                            cardDispatch({ type: Actions.GET_ISSUERS, data: response?.data });
                            bridgeRef.saveIssuers(JSON.stringify(({
                                cacheDate: (new Date()).getTime(),
                                data: response?.data
                            })))
                        } else {
                            showAlert("Something went wrong!", "Please check your internet connection", { text: "Ok" })
                        }
                    });
                }
            })
        } else {
            Store.getIssuers((error, issuersData) => {
                if (issuersData && !isExpired(issuersData?.cacheDate, 2) && issuersData?.data) {
                    cardDispatch({ type: Actions.GET_ISSUERS, data: issuersData?.data });
                } else {
                    CardAPI.getIssuers((response, error) => {
                        if (response?.data) {
                            cardDispatch({ type: Actions.GET_ISSUERS, data: response?.data });
                            Store.setIssuers(({
                                cacheDate: (new Date()).getTime(),
                                data: response?.data
                            }))
                        } else {
                            showAlert("Something went wrong!", "Please check your internet connection", { text: "Ok" })
                        }
                    });
                }
            })
        }
    };

    const getCardTypes = () => {
        CardAPI.getCardTypes((response, error) => {
            if (response?.data) {
                cardDispatch({ type: Actions.GET_CARD_TYPES, data: response?.data });
            } else {
                showAlert("Something went wrong!", "Please check your internet connection", { text: "Ok" })
            }
        });
    }

    const reloadData = (callback = null) => {
        startBlockingLoader('Updating cards');
        getCards(false, null, callback);
    };

    const markDeeplinkProcessed = () => {
        cardDispatch({ type: Actions.RESET_DEEPLINK })
    }

    return (
        <CardContext.Provider value={{
            Toast: { showToast: showToast },
            Alert: { showAlert: showAlert },
            BlockingLoader: { startLoader: startBlockingLoader, stopLoader: stopBlockingLoader },
            cardState: cardState,
            cardDispatch: cardDispatch,
            reloadData: reloadData,
            markDeeplinkProcessed: markDeeplinkProcessed
        }}>
            <BlockingLoader visible={isBlockingLoader} loadingMessage={blockingLoaderMessage} />
            <MenuProvider>
                <AppNavigator />
            </MenuProvider>
            <Toast ref={toastRef}
                style={{ backgroundColor: colors.color_404040 }}
                position='bottom'
                textStyle={{ ...TextStyle.text_14_bold, color: colors.white }}
            />
            <DialogView ref={alertRef} />
        </CardContext.Provider>
    );
}

export default codePush(Root);
