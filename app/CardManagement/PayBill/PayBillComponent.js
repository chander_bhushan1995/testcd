import React, { useRef, useEffect, useState } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import {WebView} from 'react-native-webview';
import colors from '../../Constants/colors';
import { TextStyle } from "../../Constants/CommonStyle";
import { HeaderBackButton } from "react-navigation";
import { PLATFORM_OS } from "../../Constants/AppConstants";
import useBackHandler from "../CustomHooks/useBackHandler";
import useCommonComponent from '../CustomHooks/useCommonComponent';
import Loader from '../../Components/Loader';
import {addEvent, Events} from '../Analytics';

export default function PayBillComponent(props) {
    const { Alert, cardState } = useCommonComponent();
    const { navigation } = props
    const walletId = navigation.getParam('params', null);
    const setShowReminderAdded = navigation.getParam('setShowReminderAdded', null);
    const { allCards } = cardState;
    const cardData = allCards.find(card => card.walletId === walletId);
    const paymentUrl = cardData.paymentLink;
    const paymentStatusUrl = cardData.paymentStatusLink;
    let url = { uri: paymentUrl }
    const webViewRef = useRef(null)
    const [isSuccess, setIsSuccess] = useState(false);

    useBackHandler(() => onBackPress(), [isSuccess])

    useEffect(() => {
        navigation.setParams({
            onBackPress: () => onBackPress()
        })
    }, [isSuccess])

    const onBackPress = () => {
        if (isSuccess) {
            navigateToPaymentSuccess();
        } else {
            Alert.showAlert('Don’t leave! You are just one step away!', 'A late payment may result in extra charges on your card.',  {
                text: 'Pay bill now',
                onClick: () => {
                    addEvent(Events.payCardBillDropout, {Result: 'No'});
                },
            }, {
                text: 'Quit bill payment',
                onClick: () => {
                    addEvent(Events.payCardBillDropout, {Result: 'Yes'});
                    navigation.pop();
                },
            });
        }
        return true
    };

    const getActivityIndicatorLoadingView = () => {
        return (
            <Loader isLoading={true} loadingMessage={"You are being redirected to billdesk to complete your payment."} loaderStyle={{ backgroundColor: colors.white }} />
        );
    }

    const onShouldStartLoadWithRequest = (navigator) => {
        if (navigator.url.includes(paymentStatusUrl)) {
            setIsSuccess(true);
        }
        return true;
    }

    const navigateToPaymentSuccess = () => {
        navigation.replace('PayBillSuccessComponent', { params: cardData.walletId, setShowReminderAdded: setShowReminderAdded });
    }

    return (
        <View style={{ flex: 1 }}>
            <WebView
                ref={webViewRef}
                style={styles.WebViewStyle}
                source={url}
                thirdPartyCookiesEnabled={true}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                scalesPageToFit={true}
                renderLoading={getActivityIndicatorLoadingView}
                startInLoadingState={true}
                onShouldStartLoadWithRequest={onShouldStartLoadWithRequest}
                onNavigationStateChange={onShouldStartLoadWithRequest}
                dataDetectorTypes={'none'}
            />
        </View>
    )
}

PayBillComponent.navigationOptions = ({ navigation }) => {
    const { onBackPress } = navigation.state.params;
    return {
        headerTitle: <View style={Platform.OS === PLATFORM_OS.ios ? { alignItems: "center" } : { alignItems: "flex-start" }}>
            <Text style={TextStyle.text_16_bold}>Pay Bill</Text>
        </View>,
        headerLeft: <HeaderBackButton tintColor={colors.black} onPress={onBackPress} />,
        headerTitleStyle: { color: colors.white },
        headerTintColor: colors.white
    };
};

const styles = StyleSheet.create({
    WebViewStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
});
