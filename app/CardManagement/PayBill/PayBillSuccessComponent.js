import React, { useState, useEffect } from "react";
import {
    Image,
    Text,
    TouchableOpacity,
    StyleSheet,
    NativeModules,
    Platform,
    View,
    SafeAreaView
} from "react-native";
import colors from "../../Constants/colors"
import * as CardAPI from "../DataLayer/CardAPIHelper";
import { TextStyle, CommonStyle } from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import { PLATFORM_OS } from "../../Constants/AppConstants";
import useBackHandler from "../CustomHooks/useBackHandler";
import { stringConstants } from "../DataLayer/StringConstants";
import useCommonComponent from "../CustomHooks/useCommonComponent";
import { Events, addEvent } from "../Analytics";

const bridgeRef = NativeModules.ChatBridge;

export default function PayBillSuccessComponent(props) {
    const { Alert, BlockingLoader, Toast, cardState, reloadData } = useCommonComponent();
    const { navigation } = props;
    const walletId = navigation.getParam('params', null);
    const setShowReminderAdded = navigation.getParam('setShowReminderAdded', null);
    const { allCards } = cardState;
    const cardData = allCards.find(card => card.walletId === walletId);
    const paymentDate = new Date()
    const [hasReminder, setHasReminder] = useState(false);
    const [isRatingVisible, setIsRatingVisible] = useState(true);
    const [rating, setRating] = useState(5);

    useBackHandler(onBackPress)

    useEffect(() => {
        addEvent(Events.ratingPopup, { Location: "PayBill_PaymentResponse" });
        addEvent(Events.payCardBillSuccessScreenview, {});
        setHasReminder(cardData?.reminder?.reminderStatus === "A")
        markBillPaid()
    }, [])

    const onBackPress = () => {
        if (setShowReminderAdded) {
            navigation.pop()
            setShowReminderAdded(!hasReminder)
        } else {
            navigation.replace("CardDetailComponent", {
                params: cardData.walletId,
                showReminderAdded: !hasReminder
            })
        }
    }

    const markBillPaid = () => {
        BlockingLoader.startLoader('Updating status');
        addEvent(Events.payCardBillSubmit, {Result: 'Success'});
        CardAPI.markBillPaid(cardData.walletId, (response, error) => {
            BlockingLoader.stopLoader();
            if (response) {
                reloadData();
            } else {
                Alert.showAlert("Unable to update payment status", error?.error[0]?.message, { text: "OK" })
            }
        });
    };

    const lastRecordedText = () => {
        var dateFormat = require('dateformat');
        return (<Text style={styles.headerSubtitle}>{stringConstants.lastPaymentRecorderd + dateFormat(paymentDate, "dd/mm/yy")}</Text>)
    }

    const submitRatingClicked = () => {
        setIsRatingVisible(false)
        addEvent(Events.ratingSubmitted, {
            Location: "PayBill_PaymentResponse",
            'Star Rating': rating
        });

        Toast.showToast("Rating successfully submitted.", 1500);

        if (rating > 3) {
            Alert.showAlert("Would you like to rate us on " + (Platform.OS === PLATFORM_OS.ios ? "App store" : "Play store"), null, {
                text: "Yes, Sure!",
                onClick: () => {
                    bridgeRef.submitRatingOnAppStore()
                }
            }, { text: "No, thanks" })
        }
    }

    const ratingView = () => {
        var Star = require("../../images/star.webp");
        var Star_With_Border = require("../../images/star_unselected.webp");
        let reactNativeRatingBar = [1, 2, 3, 4, 5].map(num => {
            return (
                <TouchableOpacity activeOpacity={1.0} key={num} onPress={() => setRating(num)}>
                    <Image style={styles.StarImage} source={num <= rating ? Star : Star_With_Border} />
                </TouchableOpacity>
            )
        })

        return (<View>
            <View style={CommonStyle.seprater} />
            <Text style={styles.ratingTitle}>How was your bill payment experience?</Text>
            <Text style={styles.ratingSubtitle}>Rate us on a  scale of 1 to 5</Text>
            <View style={styles.starsView}>{reactNativeRatingBar}</View>
            <TouchableOpacity onPress={submitRatingClicked}>
                <Text style={styles.setReminderButton}>Submit</Text>
            </TouchableOpacity>
        </View>
        )
    }

    return (
        <SafeAreaView style={styles.safeArea}>
            <TouchableOpacity style={styles.crossTouch} onPress={onBackPress}>
                <Image style={styles.crossIcon} source={require("../../images/icon_cross.webp")} />
            </TouchableOpacity>
            <Text style={styles.headerTitle}>Your payment has been recorded.</Text>
            <Text style={styles.headerSubtitle}>It might take upto 48 hours for your payment to get processed at bank’s end.</Text>
            {lastRecordedText()}
            <View style={{ flex: 1 }} />
            {isRatingVisible ? ratingView() : null}
        </SafeAreaView>
    );
}

export const styles = StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: colors.white
    },
    crossTouch: {
        width: 56,
        height: 56,
        marginRight: spacing.spacing0,
        marginTop: spacing.spacing0,
        alignSelf: "flex-end"
    },
    crossIcon: {
        width: 24,
        height: 24,
        marginRight: spacing.spacing16,
        marginTop: spacing.spacing16,
        resizeMode: "contain"
    },
    headerTitle: {
        ...TextStyle.text_18_bold,
        marginTop: spacing.spacing12,
        marginBottom: spacing.spacing20,
        marginHorizontal: spacing.spacing16
    },
    headerSubtitle: {
        ...TextStyle.text_16_normal,
        color: colors.grey,
        marginTop: spacing.spacing8,
        marginHorizontal: spacing.spacing16
    },
    ratingTitle: {
        ...TextStyle.text_16_bold,
        marginTop: spacing.spacing32,
        marginHorizontal: spacing.spacing16,
        textAlign: "center"
    },
    ratingSubtitle: {
        ...TextStyle.text_14_normal,
        marginTop: spacing.spacing4,
        marginHorizontal: spacing.spacing16,
        marginBottom: spacing.spacing16,
        textAlign: "center",
    },
    starsView: {
        flexDirection: "row",
        alignSelf: "center",
        alignContent: "center"
    },
    StarImage: {
        width: 32,
        height: 32,
        margin: spacing.spacing4,
        resizeMode: "cover"
    },
    setReminderButton: {
        ...TextStyle.text_16_bold,
        borderRadius: spacing.spacing2,
        marginTop: spacing.spacing32,
        paddingHorizontal: spacing.spacing64,
        marginVertical: spacing.spacing16,
        paddingVertical: spacing.spacing12,
        color: colors.white,
        backgroundColor: colors.blue028,
        textAlign: "center",
        overflow: "hidden",
        alignSelf: "center"
    }
});
