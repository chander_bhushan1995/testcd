import React from 'react';
import { View, StyleSheet, Platform, Text } from 'react-native';
import WebView from 'react-native-webview';
import colors from '../../Constants/colors';
import { HeaderBackButton } from "react-navigation";
import { PLATFORM_OS } from "../../Constants/AppConstants";
import { TextStyle } from "../../Constants/CommonStyle";
import Loader from '../../Components/Loader';
import BackButton from '../../CommonComponents/NavigationBackButton';

export default function WebViewComponent(props) {
    const { navigation } = props;
    const link = navigation.getParam('url', null);
    const loaderMessage = navigation.getParam('loaderMessage', null);
    const url = { uri: link }

    const getActivityIndicatorLoadingView = () => {
        return (
            <Loader isLoading={true} loadingMessage={loaderMessage} loaderStyle={{ backgroundColor: colors.white }} />
        );
    }

    return (
        <View style={{ flex: 1 }}>
            <WebView
                style={styles.WebViewStyle}
                source={url}
                thirdPartyCookiesEnabled={true}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                scalesPageToFit={true}
                renderLoading={getActivityIndicatorLoadingView}
                startInLoadingState={true}
                dataDetectorTypes={'none'}
            />
        </View>
    )
}

WebViewComponent.navigationOptions = ({ navigation }) => {
    return {
        headerTitle: <View style={Platform.OS === PLATFORM_OS.ios ? { alignItems: "center" } : { alignItems: "flex-start" }}>
            <Text style={TextStyle.text_16_bold}>{navigation.state.params.headerTitle}</Text>
        </View>,
        headerLeft: <BackButton onPress={() => navigation.pop()} />,
        headerTitleStyle: { color: colors.white },
        headerTintColor: colors.white
    };
};

const styles = StyleSheet.create({
    WebViewStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    }
});
