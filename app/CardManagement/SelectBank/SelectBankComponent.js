import React, {useState} from 'react';
import {
    Image,
    SafeAreaView,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    SectionList,
} from 'react-native';
import colors from '../../Constants/colors';
import {stringConstants} from '../DataLayer/StringConstants';
import {TextStyle} from '../../Constants/CommonStyle';
import spacing from '../../Constants/Spacing';
import {cardTypeId, CardType} from '../CardUtils';
import useBackHandler from '../CustomHooks/useBackHandler';
import useCommonComponent from '../CustomHooks/useCommonComponent';
import InputBox from '../../Components/InputBox';
import RadioButton from '../../Components/RadioButton';

const OtherBankItem = (props) => {
    const {item, selectedIssuer = null, setSelectedIssuer} = props;
    const radioButtonData = (item?.cardIssuerCode?.toUpperCase() === selectedIssuer?.cardIssuerCode?.toUpperCase())
        ? {
            color: colors.blue028,
            selected: true,
            size: 18,
        }
        : {
            color: colors.color_E0E0E0,
            selected: false,
            size: 18,
        };

    return (
        <TouchableOpacity style={{flexDirection: 'row', width: '100%'}} onPress={() => setSelectedIssuer(item)}>
            <Text style={style.otherBankName}>{item?.cardIssuerName}</Text>
            <RadioButton data={radioButtonData} onClick={() => setSelectedIssuer(item)}/>
        </TouchableOpacity>);
};

const TopBankItem = (props) => {
    let {item, selectedIssuer = null, setSelectedIssuer} = props;
    let borderColor, borderWidth;
    if (item?.cardIssuerCode?.toUpperCase() === selectedIssuer?.cardIssuerCode?.toUpperCase()) {
        borderColor = colors.blue028;
        borderWidth = 1;
    } else {
        borderColor = colors.color_E0E0E0;
        borderWidth = 0.5;
    }

    return (
        <TouchableOpacity style={{marginVertical: spacing.spacing6, width: 72}} onPress={() => setSelectedIssuer(item)}>
            <View style={{...style.bankImageContainer, borderWidth, borderColor}}>
                <Image style={style.bankImageStyle} source={{uri: item?.imagePath2}}/>
            </View>
            <Text style={style.topBankName}>{item?.issuerShortName}</Text>
        </TouchableOpacity>);
};

const TopBankRow = (props) => {
    let {item, selectedIssuer = null, setSelectedIssuer} = props;
    let rowsRequired = Math.max(Math.ceil(item.length / 4), 2);
    let segregatedData = [[]];
    for (let i = 0; i < rowsRequired; i++) {
        let rowArray = [];
        for (let j = 0; j < 4; j++) {
            rowArray.push(item[4 * i + j]);
        }
        segregatedData.push(rowArray);
    }
    return (
        <View>
            {segregatedData.map((item, index) => {
                return (<View key={index} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
                    {item.map((singleItem, index) => {
                        return <TopBankItem
                            key={index}
                            item={singleItem}
                            selectedIssuer={selectedIssuer}
                            setSelectedIssuer={setSelectedIssuer}/>;
                    })}
                </View>);
            })}
        </View>);
};

export default function SelectBankComponent(props) {
    const {navigation} = props;
    const cardType = navigation.getParam('cardType') ?? CardType.creditCard;
    const issuerSelected = navigation.getParam('issuerSelected', null);
    const selectedIssuer = navigation.getParam('selectedIssuer', null);
    const {cardState} = useCommonComponent();
    const {issuers} = cardState;
    const cardIssuers = issuers.filter(card => card?.cardTypeId === cardTypeId(cardType)).sort((issuer1, issuer2) => issuer1.cardIssuerName > issuer2.cardIssuerName ? 1 : -1);
    const topIssuers = cardIssuers.filter(cardIssuers => cardIssuers.ranking).sort((issuer1, issuer2) => {
        return issuer1.ranking > issuer2.ranking ? 1 : -1;
    }).slice(0, 8);
    const initialSectionListData = [{
        title: stringConstants.topBanks,
        isTopBank: true,
        data: [topIssuers],
    }, {
        title: stringConstants.others,
        isTopBank: false,
        data: cardIssuers,
    }];

    const [sectionListData, setSectionListData] = useState(initialSectionListData);

    useBackHandler(() => navigation.pop());

    const setSelectedIssuer = (selectedIssuer) => {
        issuerSelected(selectedIssuer);
        navigation.pop();
    };

    const setSearchableText = (text) => {
        let searchText = text.replace(/ /g, '');
        if (searchText !== '') {
            let searchResults = cardIssuers.filter(card => {
                return card.cardIssuerName?.replace(/ /g, '')?.toLowerCase().includes(searchText.toLowerCase())
                    || card.cardIssuerCode?.replace(/ /g, '')?.toLowerCase().includes(searchText.toLowerCase());
            });
            setSectionListData([{
                isTopBank: false,
                data: searchResults,
            }]);
        } else {
            setSectionListData(initialSectionListData);
        }
    };

    return (<SafeAreaView style={style.container}>
        <View style={{flexDirection: 'row', width: '100%', justifyContent: 'space-between'}}>
            <Text style={{...TextStyle.text_16_bold, marginLeft: spacing.spacing16, marginTop: spacing.spacing36}}>
                {stringConstants.selectYourBank}</Text>
            <TouchableOpacity style={style.crossAreaStyle} onPress={() => navigation.pop()}>
                <Image source={require('../../images/icon_cross.webp')} style={style.crossIconStyle}/>
            </TouchableOpacity>
        </View>
        <InputBox
            containerStyle={style.textInputContainer}
            placeholder={stringConstants.searchYourBank}
            placeholderTextColor={colors.placeHolderColor}
            isMultiLines={false}
            keyboardType={'default'}
            editable={true}
            onChangeText={setSearchableText}
        />
        <SectionList
            sections={sectionListData}
            ItemSeparatorComponent={() => <View style={{height: 1, backgroundColor: colors.color_E0E0E0, width: '100%'}}/>}
            renderSectionHeader={({ section: { title = null } }) => {
                return title ? <View style={{
                    borderBottomWidth: title.toUpperCase() === 'OTHERS' ? 1 : 0,
                    borderBottomColor: colors.color_E0E0E0,
                }}><Text style={style.titleStyle}>{title}</Text>
                </View> : null;
            }}
            renderItem={({item, index, section}) => {
                return section.isTopBank
                    ? <TopBankRow item={item} selectedIssuer={selectedIssuer} setSelectedIssuer={setSelectedIssuer}/>
                    :
                    <OtherBankItem item={item} selectedIssuer={selectedIssuer} setSelectedIssuer={setSelectedIssuer}/>;
            }}
            keyExtractor={(item, index) => index.toString()}
        />
    </SafeAreaView>);
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    crossAreaStyle: {
        height: 48,
        width: 48,
        padding: spacing.spacing16,
        margin: spacing.spacing4,
    },
    crossIconStyle: {
        height: 16,
        width: 16,
        resizeMode: 'contain',
        tintColor: colors.black,
    },
    textInputContainer: {
        marginHorizontal: spacing.spacing16,
    },
    titleStyle: {
        ...TextStyle.text_10_bold,
        backgroundColor: colors.white,
        paddingTop: spacing.spacing20,
        paddingBottom: spacing.spacing8,
        paddingHorizontal: spacing.spacing16,
    },
    bankImageStyle: {
        width: 40,
        height: 40,
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    bankImageContainer: {
        backgroundColor: colors.white,
        padding: spacing.spacing12,
        borderRadius: 6,
        width: 72,
        height: 72,
    },
    topBankName: {
        ...TextStyle.text_14_normal_charcoalGrey,
        marginTop: spacing.spacing12,
        textAlign: 'center',
    },
    otherBankName: {
        ...TextStyle.text_16_normal,
        flex: 1,
        marginVertical: spacing.spacing16,
        marginHorizontal: spacing.spacing20,
    },
});
