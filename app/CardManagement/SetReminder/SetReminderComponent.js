import React, { useState } from "react";
import {
    Image,
    Text,
    TouchableOpacity,
    StyleSheet,
    NativeModules,
    Platform,
    View,
    SafeAreaView
} from "react-native";
import colors from "../../Constants/colors"
import * as CardAPI from "../DataLayer/CardAPIHelper";
import { TextStyle } from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import DateInputBox from '../../Components/DateInputBox'
import { PLATFORM_OS } from "../../Constants/AppConstants";
import useBackHandler from "../CustomHooks/useBackHandler";
import { stringConstants } from "../DataLayer/StringConstants";
import { addMonthsToToday, addDaysToToday } from "../CardUtils";
import useCommonComponent from "../CustomHooks/useCommonComponent";
import {addEvent, Events} from '../Analytics';
import {formatDateDD_MM_YYYY} from "../../commonUtil/Formatter";

const bridgeRef = NativeModules.ChatBridge;

const intensityDays = [7, 5, 3]
const defaultDueDays = 15
const maximumDueDateMonth = 2

export default function SetReminderComponent(props) {
    const { Alert, BlockingLoader, reloadData } = useCommonComponent()
    const { navigation } = props;
    const cardData = navigation.getParam('params', null);
    const navigatingFrom = navigation.getParam('navigatingFrom', '');

    useBackHandler(() => navigation.pop())

    const maximumDate = addMonthsToToday(maximumDueDateMonth)

    const dateFrom = (time) => {
        if (time == null) return addDaysToToday(defaultDueDays)
        var nextDueDate = new Date()
        nextDueDate.setTime(time)
        return nextDueDate
    }

    const intensityIndex = (intensity) => {
        if (intensity == null) return 0
        const index = intensityDays.indexOf(intensity)
        if (index == -1) {
            index = 0
        }
        return index
    }

    const [daysIndex, setDaysIndex] = useState(intensityIndex(cardData?.reminder?.intensity))
    const [date, setDate] = useState(dateFrom(cardData?.reminder?.nextDueDate));

    const setReminder = () => {
        BlockingLoader.startLoader(stringConstants.savingReminder)

        addEvent(Events.payBillReminderSubmit, {
            Location: navigatingFrom,
            Value: intensityDays[daysIndex],
            Date: formatDateDD_MM_YYYY(date.getTime()),
        });
        CardAPI.setReminder(cardData.walletId, date.getTime(), intensityDays[daysIndex], (response, error) => {
            BlockingLoader.stopLoader()
            if (Platform.OS === PLATFORM_OS.ios) { // TODO: KAG to be done in Android??
                bridgeRef.saveReminderForWalletId(Number(cardData.walletId), Number(date.getTime()), "Pay bill for " + cardData.issuerName + " credit card ending with " + cardData.contentNo?.slice(-4))
            }
            if (response) {
                navigation.pop()
                reloadData()
            } else {
                Alert.showAlert('Unable to save reminder', error?.error[0]?.message, {text: 'OK'});
            }
        })
    }

    return (
        <SafeAreaView style={styles.safeArea}>
            <TouchableOpacity style={styles.crossTouch} onPress={() => navigation.pop()}>
                <Image style={styles.crossIcon} source={require("../../images/icon_cross.webp")} />
            </TouchableOpacity>
            <Text style={styles.headerTitle}>{cardData?.reminder?.reminderStatus === "A" ? stringConstants.editReminder : stringConstants.addReminder}</Text>
            <Text style={styles.headerSubtitle}>{stringConstants.preventFromMissingPayment}</Text>
            <DateInputBox
                containerStyle={styles.dateInputContainer}
                inputHeading={stringConstants.nextDueDate}
                minimumDate={new Date()}
                date={date}
                maximumDate={maximumDate}
                onDateChange={setDate}
            />
            <View style={styles.intensityView}>
                <Text style={TextStyle.text_14_normal_charcoalGrey}>{stringConstants.intesityViewTitle}</Text>
                <View style={styles.daysView}>
                    {intensityDays.map((day, index) => (
                        <TouchableOpacity style={{ flex: 1 }} onPress={() => setDaysIndex(index)}>
                            <Text style={[styles.dayView, (index == daysIndex ? styles.selectedDayView : styles.unselectedDayView)]}>{day} {stringConstants.days}</Text>
                        </TouchableOpacity>
                    ))}
                </View>
            </View>
            <View style={{ flex: 1 }} />
            <TouchableOpacity onPress={setReminder}>
                <Text style={styles.setReminderButton}>{stringConstants.setPaymentReminder}</Text>
            </TouchableOpacity>
        </SafeAreaView>
    );
}

export const styles = StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: colors.white
    },
    crossTouch: {
        width: 48,
        height: 48,
        marginRight: spacing.spacing0,
        marginTop: spacing.spacing0,
        padding: spacing.spacing16,
        alignSelf: "flex-end"
    },
    crossIcon: {
        width: 16,
        height: 16,
        tintColor: colors.black,
        resizeMode: "contain"
    },
    headerTitle: {
        ...TextStyle.text_18_bold,
        marginTop: spacing.spacing12,
        marginHorizontal: spacing.spacing16
    },
    headerSubtitle: {
        ...TextStyle.text_16_normal,
        marginTop: spacing.spacing8,
        marginBottom: spacing.spacing20,
        marginHorizontal: spacing.spacing16
    },
    dateInputContainer: {
        justifyContent: "space-between",
        margin: spacing.spacing16,
    },
    intensityView: {
        marginHorizontal: spacing.spacing16,
        marginVertical: spacing.spacing12,
    },
    daysView: {
        flexDirection: "row",
        marginTop: spacing.spacing16,
        marginHorizontal: -6,
    },
    dayView: {
        ...TextStyle.text_14_normal,
        borderRadius: spacing.spacing4,
        marginHorizontal: spacing.spacing6,
        paddingVertical: spacing.spacing12,
        textAlign: "center",
        overflow: "hidden"
    },
    selectedDayView: {
        color: colors.white,
        backgroundColor: colors.blue028
    },
    unselectedDayView: {
        borderColor: colors.greye0,
        borderWidth: spacing.spacing1,
        color: colors.charcoalGrey,
        backgroundColor: colors.white
    },
    setReminderButton: {
        ...TextStyle.text_16_bold,
        borderRadius: spacing.spacing2,
        marginHorizontal: spacing.spacing16,
        marginVertical: spacing.spacing16,
        paddingVertical: spacing.spacing12,
        color: colors.white,
        backgroundColor: colors.blue028,
        textAlign: "center",
        overflow: "hidden",
    }
});
