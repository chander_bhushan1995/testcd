import React, {useEffect, useState} from 'react';
import {FlatList, Platform, SafeAreaView, StyleSheet, Text, View, ScrollView} from 'react-native';
import {HeaderBackButton} from 'react-navigation';
import colors from '../../Constants/colors';
import {CommonStyle, TextStyle} from '../../Constants/CommonStyle';
import spacing from '../../Constants/Spacing';
import * as CardAPI from '../DataLayer/CardAPIHelper';
import useBackHandler from '../CustomHooks/useBackHandler';
import ButtonWithLoader from '../../CommonComponents/ButtonWithLoader';
import useCommonComponent from '../CustomHooks/useCommonComponent';
import DeleteFeedbackItem, {commentBox} from './DeleteFeedbackItem';
import {addEvent, Events} from '../Analytics';

export default function DeleteCardFeedbackComponent(props) {
    const {navigation} = props;
    const walletId = navigation.getParam('walletId');
    const [ratingFeedbackText, setRatingFeedbackText] = useState('');
    const [questionId, setQuestionId] = useState('');
    const [feedbackOptions, setFeedbackOptions] = useState([]);
    const {BlockingLoader, Alert, Toast} = useCommonComponent();
    const [questionText, setQuestionText] = useState('');
    const [isLoading, setLoading] = useState(false);

    useBackHandler(() => navigation.pop());

    useEffect(() => {
        getFeedbackReasons();
    }, []);

    let isOtherOptionSelected = false;
    let currentSelectedFeedbackText = '';
    let reducedOptionIds = feedbackOptions.reduce(function (filtered, option) {
        if (option.isSelected) {
            filtered.push(option.optionId);
            if (currentSelectedFeedbackText.length > 0) {
                currentSelectedFeedbackText = currentSelectedFeedbackText + ', ';
            }
            currentSelectedFeedbackText = currentSelectedFeedbackText + option.optionText;
        }
        if (option.optionCode?.toUpperCase() === 'OTHER') {
            isOtherOptionSelected = option.isSelected;
        }
        return filtered;
    }, []);

    const getFeedbackReasons = () => {
        BlockingLoader.startLoader('Fetching reasons');
        CardAPI.getFeedbackReasons((response, error) => {
            BlockingLoader.stopLoader();
            if (response) {
                let question = response.data[0];
                setQuestionId(question?.questionId ?? '');
                setQuestionText(question?.questionText ?? '');
                let feedbackOptions = question?.optionList;
                feedbackOptions.push({type: commentBox});
                setFeedbackOptions(feedbackOptions);
            } else {
                Alert.showAlert('Unable to get feedback reasons', 'Please check your internet connection', {text: 'Ok'});
            }
        });
    };

    const submitFeedback = () => {
        let feebackText = ratingFeedbackText.trim();
        if (isOtherOptionSelected && feebackText.length === 0) {
            Toast.showToast('Please mention your reason(s)');
            return;
        }
        addEvent(Events.cardRemoveFeedback, {
            Location: 'Card Remove Feedback',
            Feedback: currentSelectedFeedbackText + ', Extra Comments : ' + ratingFeedbackText.trim(),
        });
        setLoading(true);
        CardAPI.submitFeedback(walletId, questionId, reducedOptionIds, feebackText, (response, error) => {
            setLoading(false);
            if (response) {
                navigation.pop();
            } else {
                Alert.showAlert('Unable to submit feedback', 'Please check your internet connection', {text: 'Ok'});
            }
        });
    };

    const renderFeedbackList = ({item, index}) => {
        return (
            <DeleteFeedbackItem
                item={item}
                onClick={(isSelected) => {
                    setFeedbackOptions(prevOptions => {
                        let options = prevOptions;
                        options[index] = {
                            ...options[index],
                            isSelected: isSelected,
                        };
                        return [...options];
                    });
                }}
                setRatingFeedback={setRatingFeedbackText}
            />
        );
    };

    return (<SafeAreaView style={componentStyle.container} >
        <Text style={componentStyle.helpTextStyle}>{questionText}</Text>
        <ScrollView contentContainerStyle={{
            flexGrow: 1,
            justifyContent: 'space-between',
        }} >
            <FlatList
                keyboardShouldPersistTaps={'always'}
                contentContainerStyle={componentStyle.reasonListStyle}
                data={feedbackOptions}
                showsVerticalScrollIndicator={false}
                keyExtractor={(item, index) => index.toString()}
                renderItem={renderFeedbackList}
                ItemSeparatorComponent={() => {
                    return <View style={CommonStyle.seprater}/>;
                }}/>
            <View style={componentStyle.buttonStyle}>
                <ButtonWithLoader
                    isLoading={isLoading}
                    enable={reducedOptionIds.length > 0}
                    Button={{
                        text: 'Submit',
                        onClick: () => submitFeedback(),
                    }}
                />
            </View>
        </ScrollView>
    </SafeAreaView>);
}

DeleteCardFeedbackComponent.navigationOptions = ({navigation}) => {
    return {
        headerTitle: (
            <View style={Platform.OS === 'ios' ? {alignItems: 'center'} : {alignItems: 'flex-start'}}>
                <Text style={TextStyle.text_16_regular}>Feedback</Text>
            </View>
        ),
        headerLeft: (
            <HeaderBackButton tintColor={colors.black} onPress={() => navigation.pop()}/>
        ),
    };
};

const componentStyle = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        flex: 1
    },
    helpTextStyle: {
        ...TextStyle.text_18_bold,
        marginTop: spacing.spacing24,
        marginHorizontal: spacing.spacing16,
        marginBottom: spacing.spacing16,
    },
    reasonListStyle: {
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing14,
    },
    commentsTextStyle: {
        ...TextStyle.text_14_semibold,
        color: colors.color_b3b3b3,
        width: '100%',
        marginTop: spacing.spacing12,
    },
    buttonStyle: {
        width: '100%',
        backgroundColor: colors.white,
        padding: spacing.spacing16,
        alignSelf: 'flex-end',
    },
});
