import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity } from 'react-native';
import CheckBox from 'react-native-check-box';
import colors from '../../Constants/colors';
import spacing from '../../Constants/Spacing';
import InputBox from '../../Components/InputBox';
import { TextStyle } from '../../Constants/CommonStyle';

export const commentBox = 'COMMENT_BOX';

export default function DeleteFeedbackItem(props) {
    const { onClick, item, setRatingFeedback } = props;
    const { isSelected = false } = item;

    if (item.type === commentBox) {
        return (
            <InputBox
                height={100}
                containerStyle={styles.textInputContainer}
                inputHeading="Comments (Optional)"
                multiline={true}
                headingTextColor={colors.color_b3b3b3}
                textAlignVertical={'top'}
                editable={true}
                onChangeText={ratingFeedbackText => setRatingFeedback(ratingFeedbackText)}
            />
        );
    } else {
        return (
            <TouchableOpacity style={styles.MainContainer} onPress={() => onClick(!isSelected)}>
                <Text
                    style={[styles.textContainer, isSelected ? TextStyle.text_14_semibold : TextStyle.text_14_normal_charcoalGrey]}>
                    {item.optionText}</Text>
                <CheckBox
                    style={styles.checkboxContainer}
                    isChecked={isSelected}
                    onClick={() => onClick(!isSelected)}
                    checkedImage={<Image source={require('../../images/ic_check_box.webp')} style={styles.checkbox} />}
                    unCheckedImage={<Image source={require('../../images/ic_check_box_outline_blank.webp')} style={styles.checkbox} />}
                    checkedCheckBoxColor={colors.blue}
                    uncheckedCheckBoxColor={colors.grey}
                />
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    MainContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
    },
    checkboxContainer: {
        padding: 3,
        marginLeft: spacing.spacing20,
        height: 24,
        width: 24,
        alignSelf: 'center',
    },
    textInputContainer: {
        marginTop: spacing.spacing12,
    },
    checkbox: {
        height: 18,
        width: 18,
    }, textContainer: {
        flex: 1,
        justifyContent: 'space-between',
        marginTop: 16,
        marginBottom: 16,
    },
});
