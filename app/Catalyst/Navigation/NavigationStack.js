import { createStackNavigator } from "react-navigation";
import CatalystScreen from '../actions/CatalystActions';

import colors from "../../Constants/colors";

const navigator = createStackNavigator({
    CatalystScreen: {
        screen: CatalystScreen,
        navigationOptions: {
            gesturesEnabled: false,
            title: "",
            headerTitleStyle: {
                color: colors.charcoalGrey,
            },
            headerStyle: {
                backgroundColor: colors.white,
                elevation: 4,
                shadowOpacity: 0.1, 
                shadowOffset: {
                    width: 1,
                    height: 1,
                  },
                  shadowColor: '#000000',
            },
            headerTintColor: colors.blue,
        }
    },
    
}, {
});

export default navigator;
