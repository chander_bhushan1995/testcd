import { Platform, NativeModules } from 'react-native';
import { CATALYST, CATALYST_VIEW_PLAN } from "../../Constants/WebengageEvents";
import { logWebEnageEvent } from '../../commonUtil/WebengageTrackingUtils';
import WebEngage from "react-native-webengage";

export const ButtonAction = {
    ROUTE_TO_HAPin: "ROUTE_TO_HAPin",
    ROUTE_TO_PLAN_RECOMMENDATION: "ROUTE_TO_PLAN_RECOMMENDATION",
    ROUTE_TO_PICK_PLAN: "ROUTE_TO_PICK_PLAN",
    ROUTE_TO_IDFENCE_SUGGESTPLAN: "ROUTE_TO_IDFENCE_SUGGESTPLAN",
    ROUTE_TO_HA_EW: "ROUTE_TO_HA_EW"
}

const nativeBrigeRef = NativeModules.ChatBridge;
export const routeToExplorePlans = (actionUrl, topMostParentIndex, buttonIndex) => {

    if (actionUrl) {
        var parser = new URL(actionUrl);
        let eventData = new Object();
        eventData["commId"] = parser.comm_id || ""
        eventData["position"] = parseInt(topMostParentIndex)
        eventData["appType"] = CATALYST
        eventData["btnURL"] = actionUrl
        eventData["index"] = parseInt(buttonIndex)
        logWebEnageEvent(CATALYST_VIEW_PLAN,eventData)
        nativeBrigeRef.routeToExplorePlans(actionUrl)
    }
}
