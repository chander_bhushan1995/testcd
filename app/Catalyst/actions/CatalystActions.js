import { connect } from "react-redux";
import CatalystScreen from "../screens/CatalystScreen";
import { REQUEST_CATALYST_DATA, REQUEST_CATALYST_DATA_SUCCESS, REQUEST_CATALYST_DATA_FAILURE } from './ActionTypes';
import CatalystRawData from '../data/CatalystRawData'
import { makeGetRequest } from "../../network/ApiManager";
import {GET_CATALYST_COMPONENTS} from '../../Constants/ApiUrls';

const mapStateToProps = state => ({
    catalystData: state.CatalystReducer,
    isError:state.CatalystReducer.isError,
});
const mapDispatchToProps = dispatch => ({
    getCatalystData: (catalystDeepLinkQueryParams) => {
        dispatch({ type: REQUEST_CATALYST_DATA });

        var queryParams = "";
        for (var key in catalystDeepLinkQueryParams) {
            if (queryParams != "") {
                queryParams += "&";
            }
            queryParams += key + "=" + encodeURIComponent(catalystDeepLinkQueryParams[key]);
        }

            makeGetRequest(GET_CATALYST_COMPONENTS +'?'+ queryParams, (response, error) => {
                if (response) {
                    dispatch({ type: REQUEST_CATALYST_DATA_SUCCESS, data: response });
                } else {
                    dispatch({ type: REQUEST_CATALYST_DATA_FAILURE, data: error });
                }
            })
    },
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CatalystScreen);
