import React from 'react';
import {View, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {TextStyle} from "../../Constants/CommonStyle";
import colors from "../../Constants/colors";
import spacing from "../../Constants/Spacing";
import TextView from "./common/TextView";
import {OAImageSource} from "../../Constants/OAImageSource";

const ActionViewComponent = props => {
    const {
        onClick = () => {
        }, actionData = {}, fontColor = colors.color_FFFFFF
    } = props;
    const {alignmentType, value, action} = actionData
    const catArrowImg = (fontColor === colors.color_FFFFFF) ? OAImageSource.cta_arrow_white : OAImageSource.cta_arrow_black;
    return (/*<TouchableOpacity onPress={() => onClick(action)}>*/
        <View style={{
            flexDirection: 'row',
            alignItems: 'center',
            paddingRight: getPadding(alignmentType),
            justifyContent: getJustifyContent(alignmentType),
            marginTop: spacing.spacing20,
        }}>
            <TextView style={[styles.actionTextStyle, {color: fontColor}]} texts={{value: value}}/>
            <Image style={catArrowImg.dimensions} source={catArrowImg.source}/>
        </View>
    /*</TouchableOpacity>*/);
    function getPadding(type) {
        let btnPadding;
        switch (type) {
            case 'CENTER':
                btnPadding = 0;
                break;
            case 'LEFT':
                btnPadding = spacing.spacing16;
                break;/*
            case 'RIGHT':
                alignBtn = 'flex-end';
                break;*/
            default:
                btnPadding = spacing.spacing16;

        }
        return btnPadding;
    }
    function getJustifyContent(type) {
        let alignBtn;
        switch (type) {
            case 'CENTER':
                alignBtn = 'center';
                break;
            case 'LEFT':
                alignBtn = 'flex-start';
                break;/*
            case 'RIGHT':
                alignBtn = 'flex-end';
                break;*/
            default:
                alignBtn = 'flex-end';

        }
        return alignBtn;
    }
}

const styles = StyleSheet.create({
    actionTextStyle: {
        ...TextStyle.text_14_bold,
        paddingVertical: spacing.spacing5,
        color: colors.white
    }

})
export default ActionViewComponent;
