import React, { Component, useRef, useEffect } from 'react';
import { StyleSheet, Text, View, Dimensions, Animated } from 'react-native';
import CarouselContainer from './common/CarouselContainer'
import colors from '../../Constants/colors';
import { getComponent } from './ComponentsFactory';
import spacing from '../../Constants/Spacing';

const Carousel = props => {
    
    //below check is to rectify carousel behaviour on android when user scrolls screen by tapping carousel 
    //it stops but don't restart
    const carouselContainerRef = useRef()
    if (carouselContainerRef.current) {
        if(carouselContainerRef.current.isProgressStopped){carouselContainerRef.current.updateCarousel()}
    }

    return (
        <CarouselContainer
            ref={carouselContainerRef}
            dotStyle={styles.dotStyle} q
            _renderItem={props.renderItem}
            progressAnimationDelay={6000}
            dataSource={props.dataSource}
            carouselContainerStyle={styles.carouselContainerStyle}
            containerWidth={Dimensions.get("screen").width - 40}
        />
    );
}

export default CarouselComponent = props => {

    const data = props.data || {};
    const components = data.components || [];
    const FirstComponent = getComponent(components.length > 0 ? components[0].type : null)
    const FirstComponentData = components.length > 0 ? components[0] : null
    const isSingleItem = (props.data.components.length == 1) || 0
    const topMostParentIndex = props.topMostParentIndex || "0"
    return (
        <View>
            {isSingleItem
                ? <FirstComponent
                    data={FirstComponentData}
                    topMostParentIndex={topMostParentIndex}
                />
                : <Carousel dataSource={components} renderItem={({ item }, index) => {
                    const Component = getComponent(item.type);
                    return <Component style={{ paddingHorizontal: 0, paddingBottom: 20, marginTop: 0, marginHorizontal: 0 }} data={item}
                        topMostParentIndex={topMostParentIndex}
                        componentIndex={index}
                    />
                }}
                />
            }
        </View>
    )
}

const styles = StyleSheet.create({
    dotStyle: {
        height: 6,
        width: 6,
        borderRadius: 3,
    },
    carouselContainerStyle: {
        marginTop: spacing.spacing24,
        borderRadius: 8,
        backgroundColor: colors.white,
        elevation: 4,
        shadowColor: colors.black,
        shadowOpacity: 0.1,
        shadowRadius: 8,
        shadowOffset: {
            height: 1,
            width: 1
        },
        marginHorizontal: spacing.spacing20,
    }
});
