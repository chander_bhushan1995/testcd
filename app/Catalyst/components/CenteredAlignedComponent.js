import React from 'react';
import { View, StyleSheet,Dimensions } from 'react-native';
import TextView from './common/TextView';
import { TextStyle } from '../../Constants/CommonStyle';
import colors from '../../Constants/colors';

import spacing from '../../Constants/Spacing';
import Image from "../../lib/react-native-scalable-image";

//constant for layout
var CONTAINER_HORIZONTAL_MARGIN = 16

const CenteredAlignedComponent = props => {

    const data = props.data ?? {};
    const headerText = data.header ?? {}
    const descriptionText = data.paragraph ?? {}
    const image = data.image ?? '';
    const defaultImageHeight = data.imageHeight ?? 0

    return (
      <View style={[styles.component,props.style]}>
          {image.length > 0 ? <Image source={{ uri: image }}
                                     resizeMode="contain"
                                     width={Dimensions.get('screen').width - (2*CONTAINER_HORIZONTAL_MARGIN)}
                                     defaultHeight={defaultImageHeight}
          /> : null}
          <TextView style={styles.primaryText} texts={headerText} />
          <TextView style={styles.secondaryText} texts={descriptionText} />
      </View>
    )
}

const styles = StyleSheet.create({
    component: {
        alignItems: 'center',
        marginHorizontal: CONTAINER_HORIZONTAL_MARGIN,
        marginTop: spacing.spacing28,
    },
    primaryText: {
        ...TextStyle.text_18_normal,
        textAlign: "center",
    },
    secondaryText: {
        ...TextStyle.text_16_normal,
        color: colors.grey,
        marginTop: spacing.spacing8,
        textAlign: "center"
    }
});

export default CenteredAlignedComponent;
