import React from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';
import TextView from './common/TextView';
import {TextStyle} from '../../Constants/CommonStyle';
import colors from '../../Constants/colors';

import spacing from '../../Constants/Spacing';
import Image from "../../lib/react-native-scalable-image";

//constant for layout
const CenteredAlignedComponentV2 = props => {

    const data = props.data || {};
    const headerText = data.header || {}
    const descriptionText = data.paragraph || {}
    const image = data.image || '';
    const textColor = props?.data?.fontColor || colors.white
    return (
        <View style={styles.component}>
            {image ?  <Image source={{uri: image}} height={160} defaultWidth={Dimensions.get('screen').width - 32} resizeMode="contain" /> : null}
            <TextView style={[styles.primaryText, {color: textColor}]} texts={headerText}/>
            <TextView style={[styles.secondaryText, {color: textColor}]} texts={descriptionText}/>
        </View>
    )
}

const styles = StyleSheet.create({
    component: {
        alignItems: 'center',
        marginTop: spacing.spacing10,
    },
    primaryText: {
        ...TextStyle.text_12_bold,
        marginTop: spacing.spacing20,
        opacity: .70,
        lineHeight: spacing.spacing16,
        textAlign: "center",
    },
    secondaryText: {
        ...TextStyle.text_16_bold,
        color: colors.grey,
        lineHeight: spacing.spacing24,
        marginTop: spacing.spacing12,
        marginBottom: spacing.spacing10,
        marginRight:16,
        textAlign: "center"
    }
});

export default CenteredAlignedComponentV2;
