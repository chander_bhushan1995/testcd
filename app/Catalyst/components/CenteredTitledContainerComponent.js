import React from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import TextView from './common/TextView';
import { TextStyle } from '../../Constants/CommonStyle';
import colors from '../../Constants/colors';
import { getComponent } from '../components/ComponentsFactory'
import spacing from '../../Constants/Spacing';

const CenteredTitledContainerComponent = props => {

    const data = props.data || {};
    const texts = data.header || {};
    const components = data.components || [];
    const backgroundColor = data.bgColor;
    const FirstComponent = getComponent(components.length > 0 ? components[0].type : null)
    const FirstComponentData = components.length > 0 ? components[0] : null

    const topMostParentIndex = props.topMostParentIndex || "0"

    return (
        <View style={[styles.component, { backgroundColor: backgroundColor || colors.soulitudeGrey }]}>
            <TextView
                style={{ ...TextStyle.text_22_normal, textAlign: 'center', marginTop: 0, marginHorizontal: 16,}}
                texts={texts}
            />
            {
                components.length > 1 ?
                    <FlatList
                        style={styles.listStyle}
                        data={data.components}
                        // horizontal={(data.components.length > 2)}
                        horizontal = {false}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                        renderItem={({ item, index }) => {
                            var Component = getComponent(item.type);
                            return <Component data={item} style={{marginTop:0}} topMostParentIndex={topMostParentIndex} componentIndex={index}/>
                        }}
                        keyExtractor={(item, index) => {
                            return index.toString()
                        }}
                    />
                    : <View style={styles.listStyle}><FirstComponent data={FirstComponentData} topMostParentIndex={topMostParentIndex} componentIndex={0}/></View>
            }
        </View>
    );
}

const styles = StyleSheet.create({
    component: {
        paddingLeft: 0,
        paddingVertical: spacing.spacing32,
        marginTop: spacing.spacing20,
    },
    listStyle: {
        marginTop: spacing.spacing24,
    }
});

export default CenteredTitledContainerComponent;