
import React from 'react';
import { StyleSheet, View, Image, FlatList } from 'react-native';
import TextView from './common/TextView';
import { TextStyle } from '../../Constants/CommonStyle';
import colors from "../../Constants/colors";
import spacing from '../../Constants/Spacing';
import images from '../../images/index.image';

const SingleListItem = props => {
    const texts = props.text || {}
    const isChecked = texts.isChecked || false

    return (
        <View style={styles.singleItem}>
            {
                <Image source={isChecked ? images.compareTick : images.compareCross} style={styles.imageStyle} />
            }
            <TextView style={styles.textStyle} texts={texts} />
        </View>
    )
}

const CheckListComponent = props => {

    const data = props.data || {}
    const texts = data.texts || []

    return (
        <View>
            <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={texts}
                horizontal={false}
                ItemSeparatorComponent={() => <View style={{ backgroundColor: 'transparent', height: 24 }}></View>}
                renderItem={({ item, index }) => {
                    return <SingleListItem text={texts[index]} />
                }}
            />
        </View>
    )
}


const styles = StyleSheet.create({
    singleItem: {
        marginLeft: spacing.spacing48,
        marginRight: spacing.spacing36,
        // marginTop: spacing.spacing24,
        flexDirection: "row",
    },
    textStyle: {
        ...TextStyle.text_16_normal,
        color: colors.charcoalGrey,
        flexWrap: 'wrap',
        flexShrink: 1,
    },
    imageStyle: {
        height: 15,
        width: 15,
        marginRight: spacing.spacing12,
        marginTop: spacing.spacing5,
    }
})

export default CheckListComponent;