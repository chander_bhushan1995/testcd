import CarouselComponent from './CarouselComponent';
import PersonalizedMessageComponent from './PersonalizedMessageComponent';
import RatingCardComponent from './RatingCardComponent';
import HighlightedMessageComponent from './HighlightedMessageComponent';
import RecommendationContainerComponent from './RecommendationContainerComponent';
import VerticalRecommendationCardComponent from './VerticalRecommendationCardComponent';
import HorizontalRecommendationCardComponent from './HorizontalRecommendationCardComponent';
import CenteredAlignedComponent from './CenteredAlignedComponent';
import CenteredTitledContainerComponent from './CenteredTitledContainerComponent'
import StepCardComponent from './StepCardComponent';
import ImageGridComponent from './ImageGridComponent';
import ScreenFooterComponent from './ScreenFooterComponent';
import ProductRatingComponent from './ProductRatingComponent';
import TestimonialComponent from './TestimonialComponent';
import CheckListComponent from './CheckListComponent'
import LeftImageComponent from "./LeftImageComponent";
import TestimonialComponentV2 from "./TestimonialComponentV2";
import CenteredAlignedComponentV2 from "./CenteredAlignedComponentV2";
import {View} from 'react-native';
import React from 'react';
import FullWidthContainer from '../../BuyPlan/Components/FullWidthContainer';
import LeftImageTextComponent from '../../BuyPlan/Components/LeftImageTextComponent';
import ServiceCardContainer from '../../BuyPlan/Components/ServiceCardContainer';
import DualButtonComponent from '../../BuyPlan/Components/DualButtonComponent';
import SectionTabContainer from '../../BuyPlan/Components/SectionTabContainer';
import TestimonialListContainer from "../../BuyPlan/Components/TestimonialListContainer";
import GadgetListComponent from "../../BuyPlan/Components/GadgetListComponent";
import QuestionCardComponent from '../../BuyPlan/Components/QuestionCardComponent';
import BlogCardComponent from '../../BuyPlan/Components/BlogCardComponent';
import HorizontalGridItemComponent from "../../BuyPlan/Components/HorizontalGridItemComponent";
import VerticalGridItemComponent from "../../BuyPlan/Components/VerticalGridItemComponent";
import GridContainer from "../../BuyPlan/Components/GridContainer";
import SimpleImageTextComponent from "../../BuyPlan/Components/SimpleImageTextComponent";
import ClickableCellComponent from '../../BuyPlan/Components/ClickableCellComponent';
import SODServiceListComponent from '../../BuyPlan/Components/SODComponents/SODServiceListComponent';
import TitleWithOfferView from '../../BuyPlan/Components/SODComponents/TitleWithOfferView';
import ProductQuantitySelectionComponent
    from '../../BuyPlan/Components/SODComponents/ProductQuantitySelectionComponent';
import TimeSlotsComponent from '../../BuyPlan/Components/SODComponents/TimeSlotsComponent';
import PleaseNoteComponent from '../../BuyPlan/Components/SODComponents/PleaseNoteComponent';
import LeftBigImageTextComponent from "../../BuyPlan/Components/LeftBigImageTextComponent";
import HeaderParagraphComponent from "../../BuyPlan/Components/HeaderParagraphComponent";
import VideoThumbnailComponent from "../../BuyPlan/Components/VideoThumbnailComponent";
import HighlightedTestimonialComponent from "../../BuyPlan/Components/HighlightedTestemonialComponent";
import EmptyComponent from "../../BuyPlan/Components/EmptyComponent";
import EmptyContainer from "../../BuyPlan/Components/EmptyContainer";
import PlanListingComponent from "../../BuyPlan/Components/PlanListingComponent";
import HowPlanCoverageWorksComponent from "../../BuyPlan/Components/HowPlanCoverageWorksComponent";


const Components = {
    CarouselComponent: CarouselComponent,
    PersonalizedMessageComponent: PersonalizedMessageComponent,
    RatingCardComponent: RatingCardComponent,
    HighlightedMessageComponent: HighlightedMessageComponent,
    RecommendationContainerComponent: RecommendationContainerComponent,
    VerticalRecommendationCardComponent: VerticalRecommendationCardComponent,
    HorizontalRecommendationCardComponent: HorizontalRecommendationCardComponent,
    CenteredAlignedComponent: CenteredAlignedComponent,
    CenteredAlignedComponentV2: CenteredAlignedComponentV2,
    CenteredTitledContainerComponent: CenteredTitledContainerComponent,
    StepCardComponent: StepCardComponent,
    ImageGridComponent: ImageGridComponent,
    ScreenFooterComponent: ScreenFooterComponent,
    ProductRatingComponent: ProductRatingComponent,
    TestimonialComponent: TestimonialComponent,
    TestimonialComponentV2: TestimonialComponentV2,
    CheckListComponent: CheckListComponent,
    FullWidthContainer: FullWidthContainer,
    LeftImageTextComponent: LeftImageTextComponent,
    ServiceCardContainer: ServiceCardContainer,
    DualButtonComponent: DualButtonComponent,
    SectionTabContainer: SectionTabContainer,
    TestimonialListContainer: TestimonialListContainer,
    GadgetListComponent: GadgetListComponent,
    QuestionCardComponent: QuestionCardComponent,
    BlogCardComponent: BlogCardComponent,
    HorizontalGridItemComponent: HorizontalGridItemComponent,
    VerticalGridItemComponent: VerticalGridItemComponent,
    GridContainer: GridContainer,
    SimpleImageTextComponent: SimpleImageTextComponent,
    ClickableCellComponent: ClickableCellComponent,
    SODServiceListComponent: SODServiceListComponent,
    TitleWithOfferView: TitleWithOfferView,
    ProductQuantitySelectionComponent: ProductQuantitySelectionComponent,
    TimeSlotsComponent: TimeSlotsComponent,
    PleaseNoteComponent: PleaseNoteComponent,
    LeftBigImageTextComponent: LeftBigImageTextComponent,
    HeaderParagraphComponent: HeaderParagraphComponent,
    VideoThumbnailComponent: VideoThumbnailComponent,
    HighlightedTestimonialComponent: HighlightedTestimonialComponent,
    EmptyComponent: EmptyComponent,
    EmptyContainer: EmptyContainer,
    PlanListingComponent: PlanListingComponent,
    LeftImageComponent: LeftImageComponent,
    PlanCoverageComponent: HowPlanCoverageWorksComponent
}

const DefaultComponent = props => <View/>

export const getComponent = (componentName) => {
    return Components[componentName] ?? DefaultComponent;
}

export const FormField = {
    BRAND_SELECTION_INPUT: "BrandSelectionInput",
    PURCHASE_AMOUNT_RANGE_INPUT: "PurchaseAmountRangeInput",
    GADGET_SELECTION_INPUT: "GadgetSelectionInput",
    PURCHASE_AMOUNT_INPUT: "PurchaseAmountInput",
    LEFT_IMAGE_TEXT_COMPONENT: "LeftImageTextComponent",
    DATE_SELECTION_INPUT: "DateSelectionInput",
    CHECK_BOX_INPUT: "CheckBoxInput"
}
