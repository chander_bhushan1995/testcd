import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import {TextStyle} from "../../Constants/CommonStyle";
import ButtonWithLoader from "../../CommonComponents/ButtonWithLoader";

const ExploreViewComponent = props => {
    const {data, onClick} = props
    const {title, subtitle, actionTitle} = data

    return (
        <View style={styles.containerStyle}>
            {title ? <Text numberOfLines={2} style={styles.headingStyle}>{title}</Text> : null}
            {subtitle ? <Text numberOfLines={4} style={styles.subheadingStyle}>{subtitle}</Text> : null}
            <View style={styles.buttonStyle}>
                <ButtonWithLoader
                    isLoading={false}
                    enable={true}
                    customTextStyle={[TextStyle.text_16_bold, {color: colors.white, lineHeight: spacing.spacing22}]}
                    Button={{
                        text: actionTitle,
                        onClick: onClick,
                    }}/>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    containerStyle: {
        minHeight: spacing.spacing180,
        marginTop: spacing.spacing70,
    },
    buttonStyle: {
        alignSelf: 'center',
        marginTop: spacing.spacing24,
        width: spacing.spacing150,
        height: spacing.spacing40,
    },
    headingStyle: {
        ...TextStyle.text_18_bold,
        flexDirection: 'row',
        color: colors.color_212121,
        textAlign: 'center',
        lineHeight: spacing.spacing26,
        marginBottom: spacing.spacing12
    },
    subheadingStyle: {
        ...TextStyle.text_14_normal,
        flexDirection: 'row',
        color: colors.color_212121,
        textAlign: 'center',
        lineHeight: spacing.spacing22,

    }
})
export default ExploreViewComponent;
