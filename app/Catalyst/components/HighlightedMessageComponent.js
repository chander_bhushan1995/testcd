import React from 'react';
import { View, Image, StyleSheet, ImageBackground } from 'react-native';
import TextView from './common/TextView';
import { TextStyle } from '../../Constants/CommonStyle';
import colors from '../../Constants/colors';
import spacing from '../../Constants/Spacing';

const HighlightedMessageComponent = props => {

    const data = props.data || {};
    // const texts = data.texts || [];
    const headerText = data.header || {}
    const paragraphText = data.paragraph || {}
    const footerText = data.footer || {}
    const backgroundColor = data.bgColor || ""
    const backgroundImage = data.bgImage || ""
    const image            = data.image || ""


    return (
        <ImageBackground source={{ uri: backgroundImage || '' }} style={[styles.component, { backgroundColor: backgroundColor || colors.lightSky }, props.style]} >

            {/* TODO: - remove hardcoded image url and height width of image*/}
            { image.length > 0 ? <Image source={{ uri: image }} style={styles.image} /> : null}

            <View style={styles.textsContainer}>
                <TextView
                    style={styles.headerText}
                    texts={headerText}
                />

                <TextView
                    style={[styles.paragraphText,{color: paragraphText?.fontColor ?? colors.charcoalGrey}]}
                    texts={paragraphText}
                />

                <TextView
                    style={styles.footerText}
                    texts={footerText}
                />

            </View>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    component: {
        padding: spacing.spacing8,
        flexDirection: "row",
        borderRadius: 5,
        overflow: 'hidden',
        marginHorizontal: spacing.spacing16,
    },
    textsContainer: {
        flexDirection: "column",
        paddingLeft: spacing.spacing8,
        justifyContent: 'center',
        flex: 1,
    },
    headerText: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey
    },
    paragraphText: {
        ...TextStyle.text_14_normal,
    },
    footerText: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        marginTop: spacing.spacing8,
    },
    image: {
        height: 48,
        width: 48,
        borderRadius: 24,
    }
})

export default HighlightedMessageComponent;
