import React from 'react';
import { View, Image, StyleSheet, Platform, NativeModules } from 'react-native';
import TextView from './common/TextView';
import { TextStyle } from '../../Constants/CommonStyle';
import colors from '../../Constants/colors';
import TagsComponent from '../components/common/TagsComponent';
import RightArrowButton from '../../CommonComponents/RightArrowButton'
import { routeToExplorePlans } from '../actions/ButtonsActions'
import spacing from '../../Constants/Spacing';

const HorizontalRecommendationCardComponent = props => {

    const data = props.data || {};
    const headerText = data.header || {}
    const paragraphText = data.paragraph || {}
    const imageURL = data.image || ""
    const tags = data.tags || [];
    const buttonsArray = data.button || []
    const buttonObject = buttonsArray.length > 0 ? buttonsArray[0] : {}
    const button = {"value":buttonObject.text,"action":buttonObject.action}
    const componentIndex = props.componentIndex || "0"
    const topMostParentIndex = props.topMostParentIndex

    return (
        <View style={[styles.component, props.style]}>

            {
                imageURL.length > 0
                    ?
                    <Image source={{ uri: imageURL }} style={styles.image} />
                    : null
            }

            {tags.length > 0 ? <TagsComponent tags={tags} /> : null}

            <TextView
                style={styles.primaryText}
                texts={headerText}
                numberOfLines={3}
            />

            <TextView
                style={styles.secondaryText}
                texts={paragraphText}
                numberOfLines={3}
            />

            {/* TODO REMOVE hardcoded button action */}
            <RightArrowButton text={button.value} textStyle={styles.buttonText} style={{position: 'absolute'}} onPress={() => {
                routeToExplorePlans(button.action, topMostParentIndex, componentIndex)
            }} />

        </View>
    )
}

const styles = StyleSheet.create({
    component: {
        backgroundColor: colors.white,
        marginTop: spacing.spacing20,
        marginRight: spacing.spacing8,
        borderRadius: 4,
        width: 180,
        paddingRight: spacing.spacing12,
        minHeight: 280,
        borderWidth: 1,
        borderColor: colors.color_E0E0E0
    },
    image: {
        marginTop: spacing.spacing20,
        marginLeft: spacing.spacing12,
        height: 56,
        width: 56,
        // borderRadius: spacing.spacing24,
        resizeMode:"contain",
    },
    primaryText: {
        ...TextStyle.text_16_regular,
        marginLeft: spacing.spacing12,
        marginTop: spacing.spacing20,
        color: colors.charcoalGrey,
    },
    secondaryText: {
        ...TextStyle.text_16_normal,
        marginLeft: spacing.spacing12,
        marginTop: spacing.spacing8,
        // marginBottom: spacing.spacing60,
        color: colors.grey
    },
    buttonText: {
        ...TextStyle.text_16_bold,
        color: colors.blue028
    }
})

export default HorizontalRecommendationCardComponent;
