
import React from 'react';
import { View, StyleSheet, Text, Image, Dimensions } from "react-native";
import ImageGridTemplate from './common/ImageGridTemplate';
import TextView from './common/TextView';
import { TextStyle } from '../../Constants/CommonStyle';
import colors from '../../Constants/colors';
import spacing from '../../Constants/Spacing';


const ImageGridComponent = props => {

    const data = props.data || {};
    const images = data.images || [];
    const text = data.footer || {};

    return (
        <View style={styles.imageGrid}>
            {(images.length > 0) ? <ImageGridTemplate images={images} imageHeight={35} imageWidth={(Dimensions.get("screen").width-94)/3} /> : null}
            <TextView texts={text} style={styles.footerText} />
        </View>
    );
}

const styles = StyleSheet.create({
    imageGrid: {
        marginHorizontal: spacing.spacing24,
        marginVertical: spacing.spacing20,
    },
    footerText: {
        ...TextStyle.text_16_normal,
        color: colors.partnerGreyText,
        alignSelf: "center",
        marginBottom: spacing.spacing32,
        marginTop: spacing.spacing20,
    }

})

export default ImageGridComponent
