import React from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';

import TextView from "./common/TextView";
import spacing from "../../Constants/Spacing";
import {TextStyle} from "../../Constants/CommonStyle";
import colors from "../../Constants/colors";
import {OAImageSource} from "../../Constants/OAImageSource";

const LeftImageComponent = props => {
    const texts = props?.data?.header || {}
    const textColor = props?.data?.fontColor || colors.white
   const leftImage = props?.data?.image;
    return (
        <View style={styles.singleItem}>
            {
                <Image source={leftImage?{uri: leftImage} : OAImageSource.tick_left_image.source}
                       style={[OAImageSource.tick_left_image.dimensions, {marginRight: spacing.spacing4}]}/>
            }
            <TextView style={[styles.textStyle, {color: textColor}]} texts={texts}/>
        </View>
    )
}


const styles = StyleSheet.create({
    singleItem: {
        marginRight: spacing.spacing36,
        marginTop: 8,
        flexDirection: "row",
    },
    textStyle: {
        ...TextStyle.text_14_normal,
        color: colors.white,
        flexWrap: 'wrap',
        flexShrink: 1,
    },
    imageStyle: {
        height: 15,
        width: 15,
        marginRight: spacing.spacing12,

    }
})

export default LeftImageComponent;
