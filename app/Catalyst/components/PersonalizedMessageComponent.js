
import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import TextView from './common/TextView';
import { TextStyle } from '../../Constants/CommonStyle';
import spacing from '../../Constants/Spacing';
import colors from '../../Constants/colors';


const PersonalizedMessageComponent = props => {
  const data = props.data || {};
  const image = data.image || ""
  const headerText = data.header || {}
  const paragraphText = data.paragraph || {}
  let viewStyle=styles.component;
  let primaryTextStyle=styles.primaryText;
  let secondaryTextStyle=styles.secondaryText;
  if(props.style !==undefined && props.style !==null)
  {
    viewStyle = [styles.component, props.style]
  }
  if(props.headerStyle !==undefined && props.headerStyle !==null)
  {
    primaryTextStyle = props.headerStyle
  }
  if(props.paragraphStyle !==undefined && props.paragraphStyle !==null)
  {
    secondaryTextStyle = props.paragraphStyle
  }


  return (
    <View style={viewStyle}>
      {image ? <Image source={{ uri: image }} style={{height:40,width:120,}} resizeMode="contain"/> : null}
      <TextView style={primaryTextStyle} texts={headerText} />
      <TextView style={secondaryTextStyle} texts={paragraphText} />
    </View>
  );
};



const styles = StyleSheet.create({
  component: {
    flexDirection: 'column',
    marginHorizontal: spacing.spacing16,
    marginTop: spacing.spacing24
  },
  primaryText: {
    ...TextStyle.text_20_normal,
    marginTop: spacing.spacing20,
  },
  secondaryText: {
    ...TextStyle.text_16_normal,
    marginTop: spacing.spacing8,
  },
});

export default PersonalizedMessageComponent;
