import React from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
import TextView from './common/TextView';
import { TextStyle } from '../../Constants/CommonStyle';
import RatingBarTemplate from './common/RatingBarTemplate'
import colors from '../../Constants/colors';
import spacing from '../../Constants/Spacing';

const SingleProductRating = props => {

    const data = props.data || {}
    const ratings = data.rating || 4.0
    const intRating = Number(ratings) || 4.0
    const rating = intRating >= 0 ? (intRating > 5 ? 4.0 : intRating) : 4.0
    const platform = data.platform || ""

    return (
        <View style={styles.singleRatingContainr}>
            <Text style={TextStyle.text_20_normal}>
                {Number(rating).toFixed(1)}
            </Text>
            <View style={{ marginLeft: spacing.spacing8 }}>
                <RatingBarTemplate starImageStyle={styles.starImage} rating={rating} />
                <Text style={TextStyle.text_12_normal}>{platform}</Text>
            </View>
        </View>
    )
}

const ProductRatingRow = props => {
    const ratings = props.ratings || []
    const index = (props.startIndex > -1) ? props.startIndex : null
    const firstRating = ratings[index] || undefined
    const secondRating = ratings[index + 1] || undefined

    return (
        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
            {firstRating ? <SingleProductRating data={firstRating} /> : null}
            {secondRating ? <SingleProductRating data={secondRating} /> : null}
        </View>
    )
}


const ProductRatingComponent = props => {
    const data = props.data || {}
    const ratings = data.ratings || []
    const headerText = data.header || {}
    const ratingRows = []

    for (let index = 0; index < ratings.length; index += 2) {
        ratingRows.push(<ProductRatingRow ratings={ratings} startIndex={index} key={index} />)
    }

    return (
        <View style={styles.component}>
            <TextView texts={headerText} style={styles.headerText} />
            <View style={styles.productRatingsContainer}>
                {ratingRows.map((row, index, array) => {
                    return row;
                })}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({

    component: {
        marginTop: spacing.spacing20,
    },
    headerText: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        marginHorizontal: spacing.spacing20,
        marginTop: spacing.spacing20,
        textAlign: "center",
    },
    productRatingsContainer: {
        marginHorizontal: spacing.spacing26,
        marginTop: spacing.spacing8,
        marginBottom: spacing.spacing20,
    },
    singleRatingContainr: {
        paddingHorizontal: spacing.spacing14,
        paddingVertical: spacing.spacing12,
        flexDirection: "row"
    },
    starImage: {
        height: 10,
        width: 10,
        marginRight: spacing.spacing2,
    },

});

export default ProductRatingComponent;