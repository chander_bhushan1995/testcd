import React from 'react';
import { View, StyleSheet, ImageBackground } from 'react-native';
import TextView from './common/TextView';
import { TextStyle } from '../../Constants/CommonStyle';
import colors from '../../Constants/colors';
import RatingBarTemplate from './common/RatingBarTemplate';
import spacing from '../../Constants/Spacing';


const RatingCardComponent = props => {

    const data = props.data || {};

    const ratings = data.rating || 5;
    const intRating = Number(ratings) || 5
    const rating = intRating >= 0 ? (intRating > 5 ? 5 : intRating) : 5
    
    const headerText = data.header || {}
    const paragraphText = data.paragraph || {}
    const backgroundImage = data.bgImage || "";

    return (
        <ImageBackground source={{ uri: backgroundImage }} style={[styles.component, props.style]} >
            <View style={styles.startsContainer}>
                <RatingBarTemplate starImageStyle={styles.starImage} rating={rating} />
            </View>
            <TextView style={styles.primaryText} texts={headerText} />
            <TextView style={styles.secondaryText} texts={paragraphText} />
        </ImageBackground >
    )
}

const styles = StyleSheet.create({
    component: {
        marginHorizontal: spacing.spacing16,
        // marginTop: 24,
        marginTop: spacing.spacing36,
        backgroundColor: colors.lightSky,
        borderRadius: spacing.spacing8,
    },
    startsContainer: {
        marginHorizontal: spacing.spacing12,
        marginTop: spacing.spacing16,
        flexDirection: 'row'
    },
    starImage: {
        height: 14,
        width: 14,
    },
    primaryText: {
        ...TextStyle.text_16_normal,
        marginHorizontal: spacing.spacing12,
        marginTop: spacing.spacing12,
    },
    secondaryText: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        marginHorizontal: spacing.spacing12,
        marginTop: spacing.spacing12,
        marginBottom: spacing.spacing16,
    }
});

export default RatingCardComponent;