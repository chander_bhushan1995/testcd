import React, {useEffect, useRef} from 'react';

import {View,Animated, Text, Image} from 'react-native';
import styles from '../styles/RecommendationComponent.style';
import RecommendationsListComponent from "./RecommendationsListComponent";
import spacing from "../../Constants/Spacing";
import {
    STR_EXPLORE_RECO_APP_ACTION_TITLE,
    STR_EXPLORE_RECO_SUBTITLE,
    STR_EXPLORE_FAV_TITLE,
    STR_EXPLORE_FAV_SUBTITLE,
    STR_EXPLORE_FAV_APP_ACTION_TITLE
} from "../data/CatalystConstants";

import ExploreViewComponent from "./ExploreViewComponent";
import InViewPort from '../../commonUtil/InViewPort';

const SwippableRecommendationsListComponent = React.memo((props) => {
    const {
        data = {}, markFavorite = () => {
        }, onClick = () => {
        }, dismissAction = () => {
        }, isFromFav
    } = props;

    let opacity = new Animated.Value(1);
    const animate = (menuData) => {
        Animated.timing(opacity, {
            toValue: 0,
            duration: 300,
            useNativeDriver: true,
        }).start(() => {dismissAction(menuData);});
    };
    function onDeleteCard (menuData){
        animate(menuData);
    }
    return (<Animated.View style={[{opacity:opacity},{transform: [
            {
                scale: opacity.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0.85, 1],
                })
            },
        ]}]} >
        <RecommendationsListComponent
            data={data}
            isFromFav={isFromFav}
            markFavorite={markFavorite}
            onDeleteCard={onDeleteCard}
            onClick={onClick} />
    </Animated.View>)
})

const ComponentHeader = React.memo((props) => {
    const { text } = props
    return (<Text style={styles.componentHeaderStyle}>{text}</Text>);
})

const FavouritesAndDismissHintView =  React.memo((props) => {
    const { favouritesHintData } = props;
    const { imageUrl, title } = favouritesHintData;
    return (<View style={styles.favouritesAndDismissHintViewStyle}>
        <Image source={imageUrl} style={{ width: spacing.spacing28, height: spacing.spacing32 }} />
        <Text style={styles.favouritesAndDismissHintTextStyle}>{title}</Text>
    </View>);
})

const RecommendationComponent = props => {
    const {
        headerTitle, isFromFav = false, favouritesHintData, data = {}, markFavorite = () => {
        }, onClick = () => {
        }, deleteRecommendation = () => {
        }, componentVisible = () => {
        }, onExploreViewClick = () => {
        }, updateRecoViewPosition = () => {
        }, scrollViewSize = {
        }, scrollContentSize={ }
    } = props;
    const componentList = data?.components;
    const viewRef = useRef(null);

    useEffect(() => {
        viewRef?.current?.measureInWindow((x, y, width, height) => updateRecoViewPosition(x, y))
    }, [scrollContentSize]);

    const isExploreViewVisible = () => {
        return !(isFromFav && componentList?.length)
    }

    const exploreViewData = () => {
        if (!isFromFav) {
            return { subtitle: STR_EXPLORE_RECO_SUBTITLE, actionTitle: STR_EXPLORE_RECO_APP_ACTION_TITLE }
        } else if (!(componentList?.length)) {
            return { title: STR_EXPLORE_FAV_TITLE, subtitle: STR_EXPLORE_FAV_SUBTITLE, actionTitle: STR_EXPLORE_FAV_APP_ACTION_TITLE }
        } else {
            return null
        }
    }

    return (
        <View style={styles.component} ref={viewRef} onLayout={() => viewRef.current.measureInWindow((x, y, width, height) => updateRecoViewPosition(x, y))}>
            {headerTitle && componentList?.length ? <ComponentHeader text={headerTitle} /> : null}
            {favouritesHintData && componentList?.length ? <FavouritesAndDismissHintView favouritesHintData={favouritesHintData} /> : null}

            {
                componentList?.map((data, index, array) => {
                    return (<View key={index.toString()} style={{ marginVertical: spacing.spacing16, flex: 1 }}>
                        <InViewPort
                            parentViewSize={scrollViewSize}
                            onChange={(isVisible) => {
                                if (isVisible) {
                                    componentVisible(index + 1, data)
                                }
                            }}>
                            {isFromFav
                                ? <RecommendationsListComponent data={data} isFromFav={isFromFav}
                                    markFavorite={() => markFavorite(index + 1, data)}
                                    onClick={(actionUrl) => onClick(index + 1, data, actionUrl)} />
                                : <SwippableRecommendationsListComponent data={data} isFromFav={isFromFav}
                                    markFavorite={() => markFavorite(index + 1, data)}
                                    onClick={(actionUrl) => onClick(index + 1, data, actionUrl)}
                                    dismissAction={(menuData) => deleteRecommendation(index + 1, data, menuData)} />
                            }
                        </InViewPort>
                    </View>)
                })
            }
            {isExploreViewVisible() ? <ExploreViewComponent data={exploreViewData()} onClick={onExploreViewClick} /> : null}
        </View>
    );



}

export default React.memo(RecommendationComponent);
