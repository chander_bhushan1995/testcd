import React from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import TextView from './common/TextView';
import { TextStyle } from '../../Constants/CommonStyle';
import colors from '../../Constants/colors';
import { getComponent } from '../components/ComponentsFactory'
import spacing from '../../Constants/Spacing';

const RecommendationContainerComponent = props => {

    const data = props.data || {};
    const texts = data.header || {};
    const components = data.components || [];
    const backgroundColor = data.bgColor
    const topMostParentIndex = props.topMostParentIndex || "0"

    const VERTICAL_ITEM_SPACING = spacing.spacing20

    return (
        <View style={[styles.component, { backgroundColor: backgroundColor || colors.blue028, }]}>
            <TextView
                style={{ ...TextStyle.text_16_regular, color: colors.white, marginHorizontal: spacing.spacing16, marginVertical: spacing.spacing24 }}
                texts={texts}
            />
            <FlatList
                style={[styles.listStyle, { marginLeft: components.length <= 2 ? spacing.spacing16 : 0 }]}
                data={components}
                contentContainerStyle={{ paddingLeft: 20 }}
                horizontal={(components.length > 2)}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                renderItem={({ item, index }) => {
                    var Component = getComponent(item.type);
                    return <Component data={item} style={{ marginTop: (components.length > 2) ? 0 : VERTICAL_ITEM_SPACING, marginRight: 12 }} topMostParentIndex={topMostParentIndex} componentIndex={index} />
                }}
                keyExtractor={(item, index) => {
                    return index.toString()
                }}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    component: {
        paddingBottom: spacing.spacing24,
        marginTop: spacing.spacing48,
    },
});

export default RecommendationContainerComponent;
