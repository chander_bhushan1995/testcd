import React from 'react';
import {View,StyleSheet, Text,Image,TouchableOpacity, ImageBackground} from 'react-native';
import {TextStyle} from "../../Constants/CommonStyle";
import colors from "../../Constants/colors";
import spacing from "../../Constants/Spacing";
import FavoriteComponent from "./common/FavoriteComponent";
import ActionViewComponent from "./ActionViewComponent";
import styles from '../styles/RecommendationComponent.style';
import TagComponent from "./common/TagsComponent";
import {getComponent} from "./ComponentsFactory";
import {OAImageSource} from "../../Constants/OAImageSource";
import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
} from 'react-native-popup-menu';
const RecommendationsListComponent = props => {
    const {
        data = {}, isFromFav, markFavorite = () => {
        }, onClick = () => {
        }, onDeleteCard = () => {
        }
    } = props;
    const fontColor = data?.fontColor || colors.color_FFFFFF;
    const defaultBGImage = (fontColor === colors.color_FFFFFF ? OAImageSource.deafult_dark_bg_card : OAImageSource.default_white_bg_card);
    const imageAlignment = data?.imageAlignment || 'LEFT';
    function MenuItemView(data){
    return (
        <View>
        <Menu>
            <MenuTrigger customStyles={triggerStyles}>
                <Image source={OAImageSource.more_menu.source} style={OAImageSource.more_menu.dimensions}  />
            </MenuTrigger>
            <MenuOptions customStyles={optionsStyles}>
                <MenuOption onSelect={() => onDeleteCard(data?.rightMenu)}>
                    <Text style={[TextStyle.text_16_normal, { color: colors.color_212121,paddingLeft:spacing.spacing16 }]}>Dismiss</Text>
                </MenuOption>
            </MenuOptions>
        </Menu>
        </View>
    )
    }
    return (
        <ImageBackground resizeMode='stretch'
                         defaultSource={defaultBGImage}
                         imageStyle={{borderRadius: spacing.spacing4,}}
                         source={data?.bgImage ? {uri: data?.bgImage} : defaultBGImage}
                         style={styles.imageBackgroundStyle}>
            <View style={styles.imageBackgroundViewContainer}>
                <View style={styles.topCardViewContainerStyle}>
                    <View style={styles.tagsViewContainerStyle}>
                        {TagView(data?.tags)}
                    </View>
                    <View style={styles.viewFavContainerStyle}>
                        <FavoriteComponent favorite={data?.favorite} onClick={markFavorite}/>
                    </View>
                    {!isFromFav ? MenuItemView(data) : null}
                </View>
                <TouchableOpacity activeOpacity={1} onPress={() => onClick(data?.button?.[0]?.action)}>
                <View style={styles.componentContainerParentStyle}>
                    {imageAlignment === 'LEFT' ? LeftImageView(data?.image) : null}
                    <View style={[styles.componentContainerStyle, {
                        width: (getComponentContainerWith(data?.image))
                    }]}>
                        {ComponentContainer(data?.components)}
                    </View>
                    {imageAlignment === 'RIGHT' ? RightImageView(data?.image) : null}
                </View>
                <ActionViewComponent actionData={data?.button?.[0]} fontColor={fontColor} />
                </TouchableOpacity>
            </View>
        </ImageBackground>);


    function ComponentContainer(components) {
        let componentViewList = components?.map((item, index, array) => {
            let Component = getComponent(item.type);
            return <Component key={index.toString()} data={item}
                              headerStyle={{
                                  ...TextStyle.text_18_bold,
                                  color: fontColor,
                                  lineHeight: spacing.spacing26,
                                  marginTop: spacing.spacing0
                              }}
                              paragraphStyle={
                                  {
                                      ...TextStyle.text_14_normal,
                                      color: fontColor,
                                      lineHeight: spacing.spacing22,
                                      marginTop: spacing.spacing12
                                  }
                              } style={styles.componentStyle}
                              componentIndex={index}/>
        });
        return (<View style={{flexDirection: 'column'}}>{componentViewList}</View>);
    }

    function TagView(tagView) {
        return (<TagComponent tags={tagView || []}
                              style={styles.tagViewContainerStyle}
                              singleTagStyle={styles.singleTagStyle}
                              textStyle={{...TextStyle.text_12_bold, color: colors.white}}/>);
    }

    function getComponentContainerWith(rightImage) {
        return ((rightImage === null || rightImage?.value === "") ? '100%' : '70%');
    }

    function LeftImageView(rightImage) {
        if (rightImage === null || rightImage === "") {
            return;
        }
        return (<View
            style={{
                justifyContent: 'flex-start',
                flexDirection: "row",
                width: '30%'
            }}><Image
            resizeMode={'contain'} source={{uri: rightImage}} style={styles.imageLeftStyle}/></View>);
    }

    function RightImageView(rightImage) {
        if (rightImage === null || rightImage === "") {
            return;
        }
        return (<View
            style={{
                marginRight: 0,
                flexDirection: "row",
                width: '30%'
            }}><Image
            resizeMode={'contain'} source={{uri: rightImage}} style={styles.imageRightStyle}/></View>);
    }
}

const triggerStyles = {
    triggerText: {
        color: 'white',
    },
    triggerOuterWrapper: {

        flex: 1,
    },
    triggerWrapper: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    triggerTouchable: {
        activeOpacity: 70,
        style : {
            flex: 1,
        },
    },
};
const optionsStyles = {
    optionsContainer: {
        paddingTop: 10,
        marginTop: 40,
        height:60
    },
    optionsWrapper: {

    },
    optionWrapper: {

        margin: 5,
    },
    optionTouchable: {

        activeOpacity: 70,
    },
    optionText: {

    },
};

export default React.memo(RecommendationsListComponent);
