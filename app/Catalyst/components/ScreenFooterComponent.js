
import React from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
import ImageGridTemplate from './common/ImageGridTemplate';
import TextView from './common/TextView';
import { TextStyle } from '../../Constants/CommonStyle';
import colors from '../../Constants/colors';
import spacing from '../../Constants/Spacing';


const ScreenFooterComponent = props => {
    const data = props.data || {};

    const images = data.images || [];
    const text = data.header || {};
    const isLocalImages = data.isLocalImages || false

    return (
        <View style={[styles.component,props.style]}>
            <TextView texts={text} style={styles.headerText} />
            {(images.length > 0) ? <ImageGridTemplate images={images} isLocalImages={isLocalImages} imageHeight={20} imageWidth={55} style={styles.imageGrid}/> : null}
        </View>
    );
}

const styles = StyleSheet.create({
    component: {
        marginHorizontal: spacing.spacing40,
        marginVertical: spacing.spacing32,
    },
    imageGrid: {
        marginHorizontal: 0,
        marginVertical: 0,
        paddingHorizontal: spacing.spacing32,
        paddingVertical: spacing.spacing16,
    },
    headerText: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        alignSelf: "center",
    }

})

export default ScreenFooterComponent
