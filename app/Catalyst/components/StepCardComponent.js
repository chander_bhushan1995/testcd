import React from 'react';
import { View, StyleSheet } from 'react-native';
import TextView from './common/TextView';
import { TextStyle } from '../../Constants/CommonStyle';
import colors from '../../Constants/colors';
import spacing from '../../Constants/Spacing';

const StepCardComponent = props => {

    const data = props.data || {};
    // const texts = data.texts || [];
    const headerText = data.header || {}
    const paragraph = data.paragraph || {}

    return (
        <View style={[styles.component, props.style]}>
            <View style={styles.primaryTextContainer}>
                <TextView texts={headerText} style={styles.primaryText} />
                <View style={styles.underlineView} />
            </View>
            <TextView texts={paragraph} style={styles.secondaryText} />
        </View>
    );
}

const styles = StyleSheet.create({
    component: {
        marginHorizontal: spacing.spacing20,
        backgroundColor: colors.white,
    },
    primaryTextContainer: {
        marginTop: spacing.spacing24,
        alignSelf: 'center',
    },
    primaryText: {
        ...TextStyle.text_24_semibold,
        color: colors.blue,
        padding: spacing.spacing4,
        textAlign: 'center',
    },
    secondaryText: {
        ...TextStyle.text_16_bold,
        marginTop: spacing.spacing24,
        marginBottom: spacing.spacing32,
        marginHorizontal: spacing.spacing36,
        textAlign: 'center'
    },
    underlineView: {
        height: 2,
        backgroundColor: colors.greye0,
    }
});

export default StepCardComponent;