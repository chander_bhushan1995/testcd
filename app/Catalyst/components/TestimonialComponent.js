
import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, FlatList } from 'react-native';
import RatingBarTemplate from './common/RatingBarTemplate'
import TextView from './common/TextView';
import { TextStyle } from '../../Constants/CommonStyle';
import colors from '../../Constants/colors';
import Carousel from 'react-native-snap-carousel';
import spacing from '../../Constants/Spacing';

const RenderItem = props => {
    const data = props.data || {}
    const bgImage = props.bgImage || ""
    const headerText = data.header || {}
    const paragraph = data.paragraph || {}

    const ratings = data.rating || 5;
    const intRating = Number(ratings) || 5
    const rating = intRating >= 0 ? (intRating > 5 ? 5 : intRating) : 5

    return (
        <View style={[styles.testimonialContainer, props.style]}>
            <ImageBackground source={{ uri: bgImage }} style={{ flex: 1, alignItems: "center", marginTop: spacing.spacing16, paddingVertical: spacing.spacing8 }}>
                <RatingBarTemplate rating={rating} style={{ marginTop: spacing.spacing16, justifyComponent: "center" }} />
                <TextView texts={headerText} style={styles.headerText} />
                <TextView texts={paragraph} style={styles.paragraphText} />
            </ImageBackground>
        </View>
    )
}

const TestimonialComponent = props => {
    const data = props.data || {}
    const bgImage = data.bgImage || ""
    const testimonials = data.testimonials || []
    return (
        <View style={[styles.component, props.style]}>
            {testimonials.length != 1 ?
                <Carousel
                    data={testimonials}
                    sliderWidth={Dimensions.get("screen").width}
                    itemWidth={Dimensions.get("screen").width / 1.2}
                    containerCustomStyle={{ marginTop: -spacing.spacing20 }}
                    renderItem={({ item, index }) => {
                        return <RenderItem
                            data={item}
                            bgImage={bgImage}
                            style={{
                                marginVertical: spacing.spacing10,
                            }}
                        />
                    }
                    }
                />
                : <RenderItem data={testimonials[0]} bgImage={bgImage} />
            }
        </View>
    )
}

const styles = StyleSheet.create({
    component: {
        marginTop: spacing.spacing32,
    },
    testimonialContainer: {
        paddingHorizontal: spacing.spacing12,
        width: Dimensions.get("screen").width / 1.2,
        alignSelf: "center",
        backgroundColor: colors.white,
        borderRadius: spacing.spacing8,
        elevation: 4,
        shadowColor: "#000000",
        shadowOpacity: 0.1,
        shadowRadius: spacing.spacing8,
        shadowOffset: {
            height: 1,
            width: 1
        },
    },
    headerText: {
        ...TextStyle.text_16_normal,
        marginTop: spacing.spacing16,
        color: colors.charcoalGrey
    },
    paragraphText: {
        ...TextStyle.text_14_normal,
        marginTop: spacing.spacing8,
        marginBottom: spacing.spacing12,
        textAlign: "center",
    },

})

export default TestimonialComponent;