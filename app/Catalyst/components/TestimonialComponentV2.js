import React from 'react';
import {View, Text, StyleSheet, ImageBackground, Dimensions, FlatList} from 'react-native';
import spacing from "../../Constants/Spacing";
import RatingBarTemplate from "./common/RatingBarTemplate";
import TextView from "./common/TextView";
import colors from "../../Constants/colors";
import {TextStyle} from "../../Constants/CommonStyle";

const TestimonialComponentV2 = props => {
    const data = props.data || {}
    const textColor = props?.data?.fontColor || colors.white
    const headerText = data.header || {}
    const paragraph = data.paragraph || {}

    const ratings = data.rating || 5;
    const intRating = Number(ratings) || 5
    const rating = intRating >= 0 ? (intRating > 5 ? 5 : intRating) : 5

    return (
        <View style={{marginTop: spacing.spacing32}}>
            <RatingBarTemplate rating={rating} style={{
                marginTop: spacing.spacing32,
                justifyComponent: "flex-start"
            }}
                               starImageStyle={{height: spacing.spacing16, width: spacing.spacing16}}
            />
            <TextView texts={headerText} style={styles.headerText}/>
            <TextView texts={paragraph} style={[styles.paragraphText, {color: textColor}]}/>
        </View>
    )
}

const styles = StyleSheet.create({
    component: {
        marginTop: spacing.spacing32,
    },
    headerText: {
        ...TextStyle.text_16_normal,
        marginTop: spacing.spacing10,
        color: colors.white
    },
    paragraphText: {
        opacity: .80,
        ...TextStyle.text_14_normal,
        color: colors.white,
        paddingRight: spacing.spacing30,
        marginTop: spacing.spacing8,
        marginBottom: spacing.spacing12,
        textAlign: "left",
    },

})
export default TestimonialComponentV2;
