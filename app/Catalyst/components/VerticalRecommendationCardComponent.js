import React from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity, Dimensions, Platform } from 'react-native';
import TextView from './common/TextView';
import { TextStyle } from '../../Constants/CommonStyle';
import colors from '../../Constants/colors';
import TagsComponent from '../components/common/TagsComponent';
import spacing from '../../Constants/Spacing';
import { routeToExplorePlans } from '../actions/ButtonsActions'

const VerticalRecommendationCardComponent = props => {
    const data = props.data || {};
    const tags = data.tags || [];
    const buttonsArray = data.button || []
    const buttonObject = buttonsArray.length > 0 ? buttonsArray[0] : {}
    const button = { "value": buttonObject.text, "action": buttonObject.action, "isHTML": true }
    const headerText = data.header || {}
    const paragraphText = data.paragraph || {}
    const noteText = data.note || {}
    const subnoteText = data.subnote || {}
    const image = data.image || ""
    const componentIndex = props.componentIndex || "0"
    const topMostParentIndex = props.topMostParentIndex

    return (
        <View style={[styles.component, props.style, { marginRight: 18 }]}>

            {tags.length > 0 ? <TagsComponent tags={tags} style={{ marginLeft: -10, marginTop: 18, }} /> : null}

            <View style={styles.textContainer}>
                <View style={{ maxWidth: Dimensions.get('screen').width / 1.5, }}>
                    <TextView
                        style={styles.headerText}
                        texts={headerText}
                    />
                    <TextView
                        style={styles.paragraphText}
                        texts={paragraphText}
                    />
                </View>

                <View style={styles.imageContainer}>
                    {/* TODO: - remove hardcoded image url and height width of image*/}
                    {image.length > 0 ? <Image source={{ uri: image }} style={styles.image} /> : null}
                </View>
            </View>

            <TextView
                style={styles.noteText}
                texts={noteText}
            />

            <TextView
                style={styles.subnoteText}
                texts={subnoteText}
            />


            <TouchableOpacity style={styles.button} onPress={() => {
                routeToExplorePlans(button.action, topMostParentIndex, componentIndex)
            }}>
                <TextView
                    style={styles.buttonText}
                    texts={button} //passing button text as HTML on for this component
                />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    component: {
        borderRadius: spacing.spacing4,
        marginTop: spacing.spacing20,
        marginRight: spacing.spacing15,
        paddingHorizontal: spacing.spacing16,
        paddingBottom: spacing.spacing16,
        minHeight: spacing.recommendationCardMinHeight,
        justifyContent: 'space-between',
        backgroundColor: colors.white,
    },
    textContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    button: {
        borderRadius: spacing.spacing4,
        backgroundColor: colors.blue028,
        marginTop: spacing.spacing20,
        paddingVertical: spacing.spacing15,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    headerText: {
        ...TextStyle.text_18_normal,
        marginTop: spacing.spacing24,
    },
    paragraphText: {
        ...TextStyle.text_16_normal,
        color: colors.grey,
        marginTop: spacing.spacing8,
    },
    noteText: {
        ...TextStyle.text_14_normal,
        marginTop: spacing.spacing20,
        color: colors.charcoalGrey
    },
    subnoteText: {
        ...TextStyle.text_14_normal,
        marginTop: spacing.spacing36,
        color: colors.seaGreen,
        textAlign: 'left',
        alignSelf: 'center'
    },
    imageContainer: {

    },
    image: {
        marginVertical: spacing.spacing10,
        height: 40,
        width: 40,
        marginTop: spacing.spacing20,
        resizeMode: "contain",
    },
    buttonText: {
        ...TextStyle.text_16_bold,
        color: colors.white,
    }
})

export default VerticalRecommendationCardComponent;
