import React, { Component, PureComponent } from 'react';
import { StyleSheet, Text, View, Dimensions, Animated } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import Pagination from './pagination/Pagination'
import colors from '../../../Constants/colors';


let _this;
export default class CarouselContainer extends Component {

    static defaultProps = {
        isProgressVisible: true,
        isPaginationVisible: true,
        dataSource: ["testing1", "testing2"],
        autoplay: false,
        autoplayDelay: 2000,
        loop: true,
        progressAnimationDelay: 2000,
        containerWidth: Dimensions.get('screen').width - 40
    }

    constructor(props) {
        super(props)
        _this = this;
        _this.progressAnimation = new Animated.Value(0);
        _this.currentVisibleIndex = 0
        _this.containerStyle = _this.props.carouselContainerStyle || {}
        _this.CONTAINER_WIDTH = props.containerWidth ? props.containerWidth : (Dimensions.get('screen').width - 40)
    }

    componentDidMount() {
        _this.props.isProgressVisible ? this.StartProgressBarAnimation(0) : null
    }

    componentWillUnmount() {
        _this.progressAnimation.stopAnimation();
    }

    StartProgressBarAnimation = (value = 0) => {
        _this.progressAnimation.setValue(value);
        _this.progressAnimationValue = 0;
        
        Animated.timing(
            _this.progressAnimation,
            {
                toValue: 1,
                duration: _this.props.progressAnimationDelay * (1 - value),
            }
        ).start((result) => {
            if (result.finished) {
                _this._carousel.snapToNext()
            }
        });
    }

    updateCarousel() {
        if (_this.props.isProgressVisible) {
            _this.isProgressStopped = false
            _this.StartProgressBarAnimation(_this.animationValue)
        }
    }

    get pagination() {
        return (
            <Pagination
                ref={v => _this.slider1Pagination = v}
                dotsLength={_this.props.dataSource.length || 4}
                containerStyle={_this.props.paginationContainerStyle || styles.paginationContainerStyle}
                dotContainerStyle={_this.props.dotContainerStyle || styles.dotContainerStyle}
                dotStyle={_this.props.dotStyle || styles.dotStyle}
                inactiveDotStyle={_this.props.inactiveStyle || styles.inactiveDotStyle}
                inactiveDotOpacity={1}
                inactiveDotScale={1}
                carouselRef={_this._carousel}
                dotColor={_this.props.dotColor || "#008DF6"}
                inactiveDotColor={_this.props.inactiveDotColor || "#E0E0E0"}
            />
        );
    }

    _renderItem = ({ item, index }) => {
        return (
            <View style={{ padding: 80, backgroundColor: 'grey' }}>
                <Text>Please Pass Render Item In Props.</Text>
                <Text>Item Index {item}</Text>
            </View>
        );
    }

    _renderSnapCarousel = () => {
        return <Carousel
            loopClonesPerSide={5}
            ref={(c) => { this._carousel = c; }}
            data={_this.props.dataSource}
            renderItem={_this.props._renderItem || _this._renderItem}
            sliderWidth={_this.CONTAINER_WIDTH}
            itemWidth={_this.CONTAINER_WIDTH}
            layout={'default'}
            autoplay={_this.props.autoplay}
            autoplayDelay={_this.props.autoplayDelay}
            loop={_this.props.loop}
            inactiveSlideOpacity={1}
            inactiveSlideScale={1}
            onSnapToItem={(index) => {
                _this.currentIndex = index
                _this.props.isPaginationVisible ? _this.slider1Pagination.setActiveIndexDot(index) : null
                // if progressview visible then restart progress animation
                if (_this.props.isProgressVisible) {
                    _this.progressAnimation.stopAnimation();
                    _this.StartProgressBarAnimation(0)
                }
            }}
            onTouchStart={(event) => {
                if (_this.props.isProgressVisible) {
                    _this.isProgressStopped = true
                    _this.progressAnimation.stopAnimation(number => _this.animationValue = number)
                }
            }}
            onScrollEndDrag={(event) => {
                _this.props.isProgressVisible ? _this.StartProgressBarAnimation(_this.animationValue) : null
            }}
            onTouchEndCapture={(event) => {
                if (_this.props.isProgressVisible) {
                    _this.isProgressStopped = false
                    _this.StartProgressBarAnimation(_this.animationValue)
                }
            }}
        >

        </Carousel>
    };

    render() {
        const progressWidth = _this.progressAnimation.interpolate(
            {
                inputRange: [0, 1],
                outputRange: [0, _this.CONTAINER_WIDTH]
            });
        return (
            <View style={[styles.container, _this.containerStyle]}
                onLayout={(event) => {
                    _this.CONTAINER_WIDTH = event.nativeEvent.layout.width
                }}>
                <View style={{ overflow: "hidden", borderRadius: _this.containerStyle.borderRadius ? _this.containerStyle.borderRadius : 0 }}>
                    <this._renderSnapCarousel />
                    {_this.props.isPaginationVisible ? _this.pagination : null}

                    {_this.props.isProgressVisible ? <View style={{ backgroundColor: colors.color_E0E0E0, width: "100%" }}><Animated.View style={[styles.progressView, { width: progressWidth }]} /></View> : null}
                </View>
            </View>
        );
    }
}



const styles = StyleSheet.create({
    container: {// default styles for carousel container
        marginTop: 8,
        borderRadius: 8,
        backgroundColor: colors.white,
        shadowColor: "#000000",
        shadowOpacity: 1,
        shadowRadius: 8,
        shadowOffset: {
            height: 1,
            width: 1
        },
        marginHorizontal: 16
    },
    progressView: {// default styles for progress view
        marginHorizontal: 0,
        height: 3,
        backgroundColor: '#008DF6',
        width: '0%',
    },
    paginationContainerStyle: {// default styles for paginationContainer view
        height: 30,
        paddingVertical: 10,
        marginTop: -30,
        marginHorizontal: 0,
        alignItems: "center",
    },
    dotContainerStyle: {
        height: 0,
        padding: 0,
        margin: 0
    },
    dotStyle: {
        width: 6,
        height: 6,
        borderRadius: 5,
        marginHorizontal: 0,
    },
    inactiveDotStyle: {
        width: 6,
        height: 6,
        borderRadius: 5,
        marginHorizontal: 0,
    }
});

