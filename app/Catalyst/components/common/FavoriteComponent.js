import React, {useEffect, useState} from 'react';
import {TouchableOpacity, Image} from 'react-native';
import {OAImageSource} from "../../../Constants/OAImageSource";

const FavoriteComponent = props => {
    const { onClick = () => { }, favorite = {} } = props;
    const [favoriteStatus, setFavoriteStatus] = useState(favorite?.value ?? false );
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoading(false);
        setFavoriteStatus(favorite?.value ?? false)
    }, [favorite])

    return (<TouchableOpacity onPress={() => {
        setIsLoading(true);
        onClick()
    }}>
        <Image
            source={favoriteStatus
                ? (isLoading ? OAImageSource.fav_recommendation_loading_unselected.source : OAImageSource.fav_recommendation_selected.source) 
                : (isLoading ? OAImageSource.fav_recommendation_loading_selected.source : OAImageSource.fav_recommendation_unselected.source)}
            style={isLoading ? OAImageSource.fav_recommendation_loading_selected.dimensions : OAImageSource.fav_recommendation_selected.dimensions} />
    </TouchableOpacity>);
}
export default FavoriteComponent;
