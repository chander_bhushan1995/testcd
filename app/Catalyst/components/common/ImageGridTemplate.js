import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import ImagesIndex from '../../../images/index.image';

const SingleRowOfThreeImages = props => {
    const startingIndex = props.startingIndex || 0
    const images    = props.images
    const isLocalImages = props.isLocalImages || false

    let firstImageURL = images[startingIndex] || undefined
    let secondImageURL = images[startingIndex+1] || undefined
    let thirdImageURL = images[startingIndex+2] || undefined

    if(isLocalImages == true){
        firstImageURL =  ImagesIndex[images[startingIndex]]
        secondImageURL =  ImagesIndex[images[startingIndex+1]]
        thirdImageURL =  ImagesIndex[images[startingIndex+2]]
    }


    return (
        <View style={[styles.singleRow,props.rowStyle]}>
            {firstImageURL ? <Image source={isLocalImages ? firstImageURL : { uri: firstImageURL }} style={{ height: props.imageHeight, width: props.imageWidth }} /> : null}
            {secondImageURL ? <Image source={isLocalImages ? secondImageURL : { uri: secondImageURL }} style={{ height: props.imageHeight, width: props.imageWidth }} /> : null}
            {thirdImageURL ? <Image source={isLocalImages ? thirdImageURL : { uri: thirdImageURL }} style={{ height: props.imageHeight, width: props.imageWidth }} /> : null}
        </View>
    )
}

const ImageGridTemplate = props => {

    const images = props.images
    const imageRows = []
    const imageHeight = props.imageHeight || 20//single image height
    const imageWidth = props.imageWidth || 48//single image height
    const isLocalImages = props.isLocalImages || false

    for (let index = 0; index < images.length; index+=3) {
        imageRows.push(<SingleRowOfThreeImages startingIndex={index} images={images} isLocalImages={isLocalImages} imageHeight={imageHeight} imageWidth={imageWidth} rowStyle={props.rowStyle} key={index}/>)
    }


    return (
        <View style={[styles.component, props.style]}>
            {imageRows.map((row,index,array)=>{
                return row;
            })}

        </View>
    )
}

const styles = StyleSheet.create({
    component: {
        // marginHorizontal: 24,
        // marginVertical: 20,
    },
    singleRow:{
        flex: 1,
        flexDirection:"row",
        justifyContent: "space-between"
    }
})

export default ImageGridTemplate;
