import React from 'react';
import { View, StyleSheet, Image } from 'react-native';


const RatingBarTemplate = props => {
    const rating = Number(props.rating)
    return (
        <View style={{ flexDirection: "row" }}>
            {Array.from(Array(Math.floor(rating)), (value, key) => {
                return <Image source={require('../../../images/star.webp')} style={[styles.starImage, props.starImageStyle]} key={key} />
            })}
            {(rating > Math.floor(rating) && rating < Math.ceil(rating)) ? <Image source={require('../../../images/ic_HalfStar.webp')} style={[styles.starImage, props.starImageStyle]} /> : null}
            {Array.from(Array(5 - Math.ceil(rating)), (value, key) => {
                return <Image source={require('../../../images/star_unselected.webp')} style={[styles.starImage, props.starImageStyle]} key={key} />
            })}
        </View>
    )
}

const styles = StyleSheet.create({

    starImage: {
        height: 10,
        width: 10,
        marginRight: 2,
    },
})

export default RatingBarTemplate;