import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { TextStyle } from '../../../Constants/CommonStyle';
import colors from '../../../Constants/colors';

const SingleTagComponent = props => {
    return (
        <View style={[styles.tags,props.singleTagStyle]}>
            <Text style={[TextStyle.text_12_normal, { color: colors.blue028 },props.textStyle]}>{props.text}</Text>
        </View>
    );
}

const TagsComponent = props => {

    return (
        <View style={[styles.tagsContainer,props.style]}>
            {props.tags.map((value,index) => {
                return <SingleTagComponent text={value} key={index} singleTagStyle={props.singleTagStyle} textStyle={props.textStyle}/>
            })}
        </View>
    )
}

const styles = StyleSheet.create({
    tags: {
        padding: 4,
        borderColor: colors.blue028,
        borderWidth: 1,
        borderRadius: 2,
        marginLeft: 10,
    },
    tagsContainer: {
        flexDirection: "row",
        flexWrap: "wrap",
        marginTop: 8,
    }
})

export default TagsComponent;
