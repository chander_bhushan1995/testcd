import React, { useEffect, useState } from "react";
import { Text, NativeModules, Clipboard, Alert } from "react-native";
import HTMLView from "react-native-htmlview";
import TextStyles from "../../styles/CatalystStyles";
import { useConstructor } from "../../../BuyPlan/Helpers/CustomHooks";
import colors from "../../../Constants/colors";


const nativeBrigeRef = NativeModules.ChatBridge;
var username = "default username";
const placeholderPattern = /\{\{(\w+)}}/g;
const template = (tpl, args) => tpl.replace(placeholderPattern, (_, v) => args[v]); //replace string template placeholders

const HTMLViewComponent = props => {
    return (
        <HTMLView
            value={"<p>" + props.texts.value + "</p>"}
            stylesheet={{
                p: {
                    ...TextStyles[props.texts.type],      //style for text type
                    ...props.style,                        //style for font size
                },
                b: {
                    fontFamily: "Lato-Bold",
                },
                a: {},
                n: {
                    fontFamily: "Lato-Regular",
                },
            }}
            onLinkPress={(href) => {
                Clipboard.setString(href); //copy href to clipboard where href delegates to Coupon code
                Alert.alert("Alert", "Coupon code(" + href + ") Copied To Clipboard");
            }}
        />
    );
};

const TextComponent = props => {
    return (
        <Text style={[props.style]} numberOfLines={props.numberOfLines}>
            {props.texts.value}
        </Text>
    );
};

const ReadMoreTextComponent = props => {
    const { texts, numberOfLines, onClickReadMore = () => { } } = props;
    const [showReadMore, setShowReadMore] = useState(false);
    const [lines, setLines] = useState(0);
    const [finalLength, setFinalLength] = useState(texts.value.length)
    const dotsText = "... "
    const clickableText = "read more"

    return (
        <Text style={[props.style]} numberOfLines={lines}
            onTextLayout={({ nativeEvent: { lines } }) => {
                if (lines.length > numberOfLines && numberOfLines != 0) {
                    let length = 0
                    lines.slice(0, numberOfLines).map(line => line.text.length).forEach(value => {
                        length += value
                    })
                    setFinalLength(length - dotsText.length - clickableText.length - 3); // 3 extra space;
                    setLines(numberOfLines);
                    setShowReadMore(true);
                }
            }}>
            {texts.value.slice(0, finalLength)}
            {showReadMore ? <Text>{dotsText}<Text style={{ color: colors.blue }} onPress={onClickReadMore}>{clickableText}</Text></Text> : null}
        </Text>
    );
};

const TextView = props => {
    const textsObject = props.texts || {};
    const textValue = textsObject.value || "";
    const [textWithoutPlaceholder, setTextWithoutPlaceholder] = useState(template(textValue, { username: username }));

    useEffect(() => {
        if (placeholderPattern.test(props?.texts?.value ?? "")) {
            nativeBrigeRef.getUsername(name => {
                username = name;
                setTextWithoutPlaceholder(template(textValue, { username: username }));
            });
        } else {
            setTextWithoutPlaceholder(template(textValue, { username: username }));
        }
    }, [props]);

    return (
        (textValue.trim().length > 0)
            ? ((textsObject.isHTML || false)
            ? <HTMLViewComponent {...props} texts={{ value: textWithoutPlaceholder, isHTML: textsObject.isHTML }} />
            : (props?.showReadMore || false)
            ? <ReadMoreTextComponent {...props} numberOfLines={props.numberOfLines || 0} 
                             texts={{ value: textWithoutPlaceholder, isHTML: textsObject.isHTML }} />
            : <TextComponent {...props} numberOfLines={props.numberOfLines || 0}
                             texts={{ value: textWithoutPlaceholder, isHTML: textsObject.isHTML }} />)
            : null);
};


export default TextView;
