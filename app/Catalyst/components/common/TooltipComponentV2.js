import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    NativeModules,
    Platform
} from 'react-native';
import React, {PureComponent} from "react";
import {TextStyle} from "../../../Constants/CommonStyle";
import colors from "../../../Constants/colors";
import {PLATFORM_OS} from "../../../Constants/AppConstants";
import {AppForAppFlowStrings} from '../../../AppForAll/Constant/AppForAllConstants';
import spacing from "../../../Constants/Spacing";

const nativeBrigeRef = NativeModules.ChatBridge;
export default class TooltipComponentV2 extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            isCancelled: false,
            height: 0
        }
    }

    onCancelClick = () => {
        this.setState({
            isCancelled: true
        });
        this.props.onReloadRecommendationsClick()
    }

    render() {
        if (this.state.isCancelled) {
            if (Platform.OS === PLATFORM_OS.android) {
                nativeBrigeRef.tooltipCancelled();
            }
            return null;
        }
        let {data} = this.props;

        const text = data.text ?? AppForAppFlowStrings.yourTextHere
        const bottomHeight = Number(data.bottomHeight) ?? 110
        return (
            <View style={{
                bottom: bottomHeight,
                left: spacing.spacing0,
                right: spacing.spacing0,
                position: 'absolute',
                alignSelf: 'center',
                marginLeft: '25%',
                marginRight: '25%',
            }}>
                <View style={TooltipStyle.rootViewStyle}
                      onLayout={(event) => {
                          this.setState({height: event.nativeEvent.layout.height})
                      }}>

                    <TouchableOpacity style={TooltipStyle.imageContainerStyle} onPress={this.onCancelClick}>
                        <Text
                            allowFontScaling={false}
                            style={{...TextStyle.text_12_bold, textAlign: 'center', flexGrow: 1, color: colors.white,}}>
                            {text}
                        </Text>
                    </TouchableOpacity>
                </View>
                <DownTriangle/>
            </View>
        );
    }
}

const DownTriangle = (prop) => {
    return (
        <View style={[TooltipStyle.triangle, prop.style]}/>
    );
};

const TooltipStyle = StyleSheet.create({
    rootViewStyle: {
        flexDirection: "row",
        backgroundColor: colors.charcoalGrey,
        alignItems: 'center',
        justifyContent: "center",
        borderRadius: spacing.spacing22,
        paddingBottom: spacing.spacing6,
        paddingTop: spacing.spacing10,
        elevation: spacing.spacing5,
        flex: 1
    },

    imageContainerStyle: {
        minHeight: spacing.spacing24,
        backgroundColor: colors.transparent,
        marginLeft: spacing.spacing5,
        alignItems: "center",
        justifyContent: "center"
    },

    triangle: {
        width: spacing.spacing0,
        height: spacing.spacing0,
        backgroundColor: colors.transparent,
        borderStyle: 'solid',
        borderTopWidth: spacing.spacing20,
        borderLeftColor: colors.transparent,
        borderRightColor: colors.transparent,
        alignSelf: 'flex-end',
        marginTop: spacing.spacing_neg_8,
        marginRight: spacing.spacing20
    }
});


