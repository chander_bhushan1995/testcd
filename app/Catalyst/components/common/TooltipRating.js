import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    NativeModules,
    Platform,
    Image
} from 'react-native';
import React, { useState, useEffect } from "react";
import { TextStyle } from "../../../Constants/CommonStyle";
import colors from "../../../Constants/colors";
import { PLATFORM_OS } from "../../../Constants/AppConstants";
import { AppForAppFlowStrings } from '../../../AppForAll/Constant/AppForAllConstants';
import spacing from "../../../Constants/Spacing";

const nativeBrigeRef = NativeModules.ChatBridge;

export const TooltipRating = (props) => {
    const { data, onClick = () => { }, onCrossClick = () => {} } = props
    const { text, ratingText, leftImage, crossImage } = data
    const bottomHeight = Number(data.bottomHeight) ?? 110
    const [height, setHeight] = useState(0)
    const [isCancelled, setIsCancelled] = useState(false)

    useEffect(() => {
        if (isCancelled) {
            if (Platform.OS === PLATFORM_OS.android) {
                nativeBrigeRef.tooltipCancelled();
            }
        }
    }, [isCancelled])

    const DownTriangle = (prop) => {
        return (
            <View style={[TooltipStyle.triangle, prop.style]} />
        );
    };

    return isCancelled ? null : (
        <View style={{
            bottom: bottomHeight,
            left: spacing.spacing0,
            right: spacing.spacing0,
            position: 'absolute',
            alignSelf: 'center',
            marginLeft: '5%',
            marginRight: '5%',
        }}>
            <View style={TooltipStyle.rootViewStyle}
                onLayout={(event) => setHeight(event.nativeEvent.layout.height)}>
                <TouchableOpacity style={[TooltipStyle.imageContainerStyle]} onPress={() => {
                    // setIsCancelled(true)
                    onClick()
                }}>
                    {leftImage ? <Image style={TooltipStyle.leftImageStyle} source={leftImage} /> : null}
                    <View style={{ flexDirection: "column", paddingHorizontal: spacing.spacing12 }}>
                        <Text allowFontScaling={false} style={{ ...TextStyle.text_14_bold, textAlign: 'left', flexGrow: 1, color: colors.white, }}>
                            {text}
                        </Text>
                        <Text allowFontScaling={false} style={{ ...TextStyle.text_14_bold, textAlign: 'left', flexGrow: 1, color: colors.white, }}>
                            {ratingText}
                        </Text>
                    </View>
                </TouchableOpacity>
                {crossImage
                    ? <TouchableOpacity style={TooltipStyle.crossImageContainerStyle} onPress={onCrossClick}>
                        <Image style={TooltipStyle.imageStyle} source={crossImage} />
                    </TouchableOpacity>
                    : null}
            </View>
            <DownTriangle />
        </View>
    );
}

const TooltipStyle = StyleSheet.create({
    rootViewStyle: {
        flexDirection: "row",
        backgroundColor: colors.charcoalGrey,
        alignItems: 'center',
        paddingLeft: spacing.spacing12,
        borderRadius: spacing.spacing48,
        elevation: spacing.spacing5,
        flex: 1
    },
    imageContainerStyle: {
        flex: 1,
        flexDirection: "row",
        paddingVertical: spacing.spacing8,
        minHeight: spacing.spacing24,
        paddingLeft: spacing.spacing8,
        alignItems: "flex-start",
        justifyContent: "flex-start"
    },
    crossImageContainerStyle: {
        minHeight: spacing.spacing24,
        paddingHorizontal: spacing.spacing12,
        paddingVertical: spacing.spacing18,
        alignItems: "flex-end",
        justifyContent: "flex-end"
    },
    triangle: {
        width: spacing.spacing0,
        height: spacing.spacing0,
        backgroundColor: colors.transparent,
        borderStyle: 'solid',
        borderTopWidth: spacing.spacing20,
        borderLeftColor: colors.transparent,
        borderRightColor: colors.transparent,
        alignSelf: 'flex-end',
        marginTop: spacing.spacing_neg_8,
        marginRight: spacing.spacing20
    },
    leftImageStyle: {
        height: 48,
        width: 48,
        justifyContent: 'center',
    },
    imageStyle: {
        height: 24,
        width: 24,
        justifyContent: 'center',
    },
});