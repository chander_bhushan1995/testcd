
export default CatalystRawData = {
    "components": [
        {
            "type": "PersonalizedMessageComponent",
            "image": "https://ws.oneassist.in/static/oaapp/Catalyst/OALogo@3x.png",
            "header": {
                "value": "<b>Hi {{userName}},</b> ",
                "isHTML": true
            },
            "paragraph": {
                "value": "Now you can save money on gadget repairs with OneAssist protection plans.",
                "isHTML": false
            }
        },
        {
            "type": "HighlightedMessageComponent",
            "bgImage": "",
            "image": "https://ws.oneassist.in/static/oaapp/Catalyst/factsIcons/ic_peoplePersona@3x.png",
            "paragraph": {
                "value": "<b>Rajesh from Gurgaon</b> bought <b>iPhone Damage</b> Protection plan in 2019 and <b>saved ₹ 15,000</b> on repairs with just one claim.",
                "isHTML": true
            }
        },
        {
            "type": "RecommendationContainerComponent",
            "header": {
                "value": "Get a <b>protection plans at 20% OFF</b> for limited time only. <b>Use code: <a href=OA20>OA20</a></b> on payment.",
                "isHTML": true
            },
            "components": [
                {
                    "type": "HorizontalRecommendationCardComponent",
                    "image": "https://ws.oneassist.in/static/oaapp/Catalyst/productIcons/Mobile@3x.png",
                    "header": {
                        "value": "<b>Mobile protection plans</b>",
                        "isHTML": "true"
                    },
                    "paragraph": {
                        "value": "For mobile phones bought in last 30 days.",
                        "isHTML": false
                    },
                    "button": {
                        "text": "See Plans",
                        "action": "https://oneassist.in/sales/recommendation?category=PE&product=MP01&productName=mobile"
                    }
                },
                {
                    "type": "HorizontalRecommendationCardComponent",
                    "image": "https://ws.oneassist.in/static/oaapp/Catalyst/productIcons/Laptop@3x.png",
                    "header": {
                        "value": "<b>Laptop protection plans</b>",
                        "isHTML": "true"
                    },
                    "paragraph": {
                        "value": "For laptops bought in last 30 days.",
                        "isHTML": false
                    },
                    "button": {
                        "text": "See Plans",
                        "action": "https://oneassist.in/sales/recommendation?category=PE&product=LAP&productName=Laptop"
                    }
                },
                {
                    "type": "HorizontalRecommendationCardComponent",
                    "image": "https://ws.oneassist.in/static/oaapp/Catalyst/productIcons/Laptop@3x.png",
                    "header": {
                        "value": "<b>Tablet protection plans</b>",
                        "isHTML": "true"
                    },
                    "paragraph": {
                        "value": "For Tablet bought in last 30 days.",
                        "isHTML": false
                    },
                    "button": {
                        "text": "See Plans",
                        "action": "https://oneassist.in/sales/recommendation?category=PE&product=TBL&productName=Tablet"
                    }
                }
            ]
        },
        {
            "type": "ProductRatingComponent",
            "header": {
                "value": "Rated Highly for our Exceptional Customer Service",
                "isHTML": false
            },
            "ratings": [
                {
                    "platform": "Google Ratings",
                    "rating": 4.5
                },
                {
                    "platform": "Amazon Ratings",
                    "rating": 4.0
                }
            ]
        },
        {
            "type": "CenteredTitledContainerComponent",
            "header": {
                "value": "OneAssist for Gadgets gives you stress free protection",
                "isHTML": true
            },
            "components": [
                {
                    "type": "CenteredAlignedComponent",
                    "image": "https://i.ibb.co/8DD4Hps/icons8-cloud-network-100.png",
                    "header": {
                        "value": "<b>Save time and money</b>",
                        "isHTML": true
                    },
                    "paragraph": {
                        "value": "Get free doorstep device pickup and drop when you raise a claim. No more running around repair centres.",
                        "isHTML": false
                    }
                },
                {
                    "type": "CenteredAlignedComponent",
                    "image": "https://i.ibb.co/8DD4Hps/icons8-cloud-network-100.png",
                    "header": {
                        "value": "<b>Hassle-free & Super fast claims</b>",
                        "isHTML": true
                    },
                    "paragraph": {
                        "value": "Raising a claim made easy with our artificial intelligence chat bot. You can also raise claim from our App/Website or call us.",
                        "isHTML": false
                    }
                },
                {
                    "type": "CenteredAlignedComponent",
                    "image": "https://i.ibb.co/8DD4Hps/icons8-cloud-network-100.png",
                    "header": {
                        "value": "<b>OneAssist authorized service centres</b>",
                        "isHTML": true
                    },
                    "paragraph": {
                        "value": "To ensure 100% genuine spare parts are used to repair your device.",
                        "isHTML": false
                    }
                }
            ]
        },
        {
            "type": "CenteredTitledContainerComponent",
            "header": {
                "value": "Our claims process is hassle-free.",
                "isHTML": true
            },
            "components": [
                {
                    "type": "CarouselComponent",
                    "bgColor": " ",
                    "components": [
                        {
                            "type": "StepCardComponent",
                            "header": {
                                "value": "1",
                                "isHTML": false
                            },
                            "paragraph": {
                                "value": "Easily raise a claim with our AI Chat Bot on App or Website.",
                                "isHTML": false
                            }
                        },
                        {
                            "type": "StepCardComponent",
                            "header": {
                                "value": "2",
                                "isHTML": false
                            },
                            "paragraph": {
                                "value": "Easily raise a claim with our AI Chat Bot on App or Website.",
                                "isHTML": false
                            }
                        }
                    ]
                }
            ]
        },
        {
            "type": "CenteredTitledContainerComponent",
            "header": {
                "value": "What our customers say",
                "isHTML": true
            },
            "components": [
                {
                    "type": "TestimonialComponent",
                    "bgImage": "",
                    "testimonials": [
                        {
                            "rating": 3,
                            "header": {
                                "value": "<b>Sameer Kaushik said </b>",
                                "isHTML": true
                            },
                            "paragraph": {
                                "value": "My Samsung A7 was repaired quickly. I appreciate the service of OneAssist",
                                "isHTML": false
                            }
                        },
                        {
                            "rating": 5,
                            "header": {
                                "value": "<b>Sameer Kaushik said </b>",
                                "isHTML": true
                            },
                            "paragraph": {
                                "value": "My Samsung A7 was repaired quickly. I appreciate the service of OneAssist",
                                "isHTML": false
                            }
                        },
                        {
                            "rating": 4,
                            "header": {
                                "value": "<b>Sameer Kaushik said </b>",
                                "isHTML": true
                            },
                            "paragraph": {
                                "value": "My Samsung A7 was repaired quickly. I appreciate the service of OneAssist",
                                "isHTML": false
                            }
                        }
                    ]
                },
                {
                    "type": "RecommendationContainerComponent",
                    "header": {
                        "value": "Get a <b>protection plans at 20% OFF</b> for limited time only. <b>Use code: <a href=OA20>OA20</a></b> on payment.",
                        "isHTML": true
                    },
                    "components": [
                        {
                            "type": "HorizontalRecommendationCardComponent",
                            "image": "https://ws.oneassist.in/static/oaapp/Catalyst/productIcons/Mobile@3x.png",
                            "header": {
                                "value": "<b>Mobile protection plans</b>",
                                "isHTML": "true"
                            },
                            "paragraph": {
                                "value": "For mobile phones bought in last 30 days.",
                                "isHTML": false
                            },
                            "button": {
                                "text": "See Plans",
                                "action": "https://oneassist.in/sales/recommendation?category=PE&product=MP01&productName=mobile"
                            }
                        },
                        {
                            "type": "HorizontalRecommendationCardComponent",
                            "image": "https://ws.oneassist.in/static/oaapp/Catalyst/productIcons/Laptop@3x.png",
                            "header": {
                                "value": "<b>Laptop protection plans</b>",
                                "isHTML": "true"
                            },
                            "paragraph": {
                                "value": "For laptops bought in last 30 days.",
                                "isHTML": false
                            },
                            "button": {
                                "text": "See Plans",
                                "action": "https://oneassist.in/sales/recommendation?category=PE&product=LAP&productName=Laptop"
                            }
                        },
                        {
                            "type": "HorizontalRecommendationCardComponent",
                            "image": "https://ws.oneassist.in/static/oaapp/Catalyst/productIcons/Laptop@3x.png",
                            "header": {
                                "value": "<b>Tablet protection plans</b>",
                                "isHTML": "true"
                            },
                            "paragraph": {
                                "value": "For Tablet bought in last 30 days.",
                                "isHTML": false
                            },
                            "button": {
                                "text": "See Plans",
                                "action": "https://oneassist.in/sales/recommendation?category=PE&product=TBL&productName=Tablet"
                            }
                        }
                    ]
                },
                {
                    "type": "CenteredTitledContainerComponent",
                    "header": {
                        "value": "Partner brands",
                        "isHTML": true
                    },
                    "components": [
                        {
                            "type": "ImageGridComponent",
                            "images": [
                                "https://ws.oneassist.in/static/oaapp/Catalyst/partnersIcons/ic_amazon@3x.png",
                                "https://ws.oneassist.in/static/oaapp/Catalyst/partnersIcons/ic_apple@3x.png",
                                "https://ws.oneassist.in/static/oaapp/Catalyst/partnersIcons/ic_axis@3x.png",
                                "https://ws.oneassist.in/static/oaapp/Catalyst/partnersIcons/ic_amazon@3x.png",
                                "https://ws.oneassist.in/static/oaapp/Catalyst/partnersIcons/ic_apple@3x.png",
                                "https://ws.oneassist.in/static/oaapp/Catalyst/partnersIcons/ic_axis@3x.png"
                                
                            ],
                            "footer": {
                                "value": "And many more",
                                "isHTML": false
                            }
                        }
                    ]
                },
                {
                    "type": "ScreenFooterComponent",
                    "images": [
                        "https://ws.oneassist.in/static/oaapp/Catalyst/256bit@3x.png",
                        "https://ws.oneassist.in/static/oaapp/Catalyst/ic_norton@3x.png",
                        "https://ws.oneassist.in/static/oaapp/Catalyst/ic_pcidss@3x.png"
                    ],
                    "header": {
                        "value": "100% Safe and Secure | PCI-DSS Compliant",
                        "isHTML": false
                    }
                }
            ]
        }
    ]
}
