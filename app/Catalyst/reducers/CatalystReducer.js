import * as Actions from '../actions/ActionTypes'
import { INTERNET_ERROR_MESSAGE_BUYBACK, TECHNICAL_ERROR_MESSAGE,NETWORK_FAILED } from "../../Constants/ActionTypes";

//INITIAL STATE
const catalystData = {
    //Success data state
    isLoading: true,
    data: { components: [] },
    // Failure error state
    isError: false,
    errorType: null,
    errorMessage: "",
};

const CatalystReducer = (state = catalystData, action) => {

    switch (action.type) {
        case Actions.REQUEST_CATALYST_DATA:
            return { ...state, isLoading: true };
        case Actions.REQUEST_CATALYST_DATA_SUCCESS:
            return { ...state,isError: false, isLoading: false, data: action.data };
        case Actions.REQUEST_CATALYST_DATA_FAILURE:
            if (action.data.message !== undefined && action.data.message === NETWORK_FAILED) {
                return {
                    ...state,
                    data: action.data,
                    isError: true,
                    errorMessage: INTERNET_ERROR_MESSAGE_BUYBACK,
                    errorMessageButtonTitle: 'OK',
                    isLoading: false,
                };
            }
            else {
                return {
                    ...state,
                    data: action.data,
                    isError: true,
                    errorMessage: action.data.message !== undefined ? action.data.message : TECHNICAL_ERROR_MESSAGE,
                    errorMessageButtonTitle: 'Ok Got It',
                    isLoading: false,
                };
            }
            break;
        default:
            return { ...state };
    }

};
export default CatalystReducer;
