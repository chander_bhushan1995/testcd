import React, { useEffect, useState } from 'react';
import {
    StyleSheet,
    View,
    FlatList,
    TouchableOpacity,
    Image,
    NativeModules,
    Platform,
    BackHandler
} from 'react-native';
import { getComponent } from '../components/ComponentsFactory'
import Loader from '../../Components/Loader'
import colors from '../../Constants/colors';
import spacing from '../../Constants/Spacing';
import { CATALYST_RECOMMENDATION_SCREEN, CATALYST, CATALYST_LANDING_SCREENVIEW } from "../../Constants/WebengageEvents";
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';

const nativeBridgeRef = NativeModules.ChatBridge;

const goBack = () => {
    nativeBridgeRef.goBack();
}


const _renderComponent = ({ item, index }) => {
    const Component = getComponent(item.type)
    return <Component data={item} topMostParentIndex={index} />
}

const CatalystScreen = props => {
    BackHandler.addEventListener("hardwareBackPress", goBack);
    const catalystData = props.catalystData || {}
    const catalystQueryParams = props.navigation.getParam('catalystQueryParams', null)
    const [listScrolled, setListScrolled] = useState(false)

    useEffect(() => { //load first time data
        let eventData = new Object();
        eventData["commId"] = catalystQueryParams.comm_id || ""
        eventData["appType"] = CATALYST
        logWebEnageEvent(CATALYST_LANDING_SCREENVIEW, eventData);

        props.getCatalystData(catalystQueryParams)
    }, [props.getCatalystData, catalystQueryParams])


    if (props.isError || (!catalystData.isLoading && catalystData.data.components.length == 0)) { //if api error or components length is zero then go back
        goBack()
    }

    return (
        <View style={styles.screen}>
            {!catalystData.isLoading
                ? <FlatList
                    data={catalystData.data.components}
                    renderItem={_renderComponent}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item, index) => { return index.toString() }}
                    extraData={listScrolled}
                    onScrollEndDrag={(event) => { setListScrolled(!listScrolled) }}
                />
                : <Loader isLoading={catalystData.isLoading} />
            }
        </View>
    )

}


const BackButton = ({ onPress }) => (
    <TouchableOpacity onPress={
        () => {
            goBack()
        }
    }>
        <Image
            source={require('../../images/ic_back.webp')}
            style={{
                height: 30,
                width: 30,
                marginLeft: 9,
                marginRight: 12,
                marginVertical: 12,
                resizeMode: 'contain',
            }}
        />
    </TouchableOpacity>
);


CatalystScreen.navigationOptions = (navigationData) => {
    return {
        headerLeft: <BackButton />
    };
};


const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: colors.white,
    },
})

export default CatalystScreen;
