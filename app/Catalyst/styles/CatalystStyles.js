import colors from "../../Constants/colors";

/*
T1  =>  Regular Black
T2	=> 	Bold Black
T3	=> 	Regular Grey
T4	=> 	Bold Grey
T5	=> 	Regular White
T6	=>  Bold White
T7	=> 	Regular Blue
T8	=> 	Bold Blue
T9	=> 	Regular Green
T10	=> 	Bold Green
*/

const TextStyles = {}
        TextStyles['T1'] = {
            color: colors.charcoalGrey,
            fontFamily: 'Lato-Regular',
        },
        TextStyles['T2'] = {
            color: colors.charcoalGrey,
            fontFamily: 'Lato-Bold'
        },
        TextStyles['T3'] = {
            color: colors.grey,
            fontFamily: 'Lato-Regular',
        },
        TextStyles['T4'] = {
            color: colors.grey,
            fontFamily: 'Lato-Bold'
        },
        TextStyles['T5'] = {
            color: colors.white,
            fontFamily: 'Lato-Regular',
        },
        TextStyles['T6'] = {
            color: colors.white,
            fontFamily: 'Lato-Bold',
        },
        TextStyles['T7'] = {
            color: colors.blue,
            fontFamily: 'Lato-Regular',
        },
        TextStyles['T8'] = {
            color: colors.blue,
            fontFamily: 'Lato-Bold',
        },
        TextStyles['T9'] = {
            color: colors.seaGreen,
            fontFamily: 'Lato-Regular',
        },
        TextStyles['T10'] = {
            color: colors.seaGreen,
            fontFamily: 'Lato-Bold',
        }

export default TextStyles
