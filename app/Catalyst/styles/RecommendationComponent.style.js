import {StyleSheet} from "react-native";
import spacing from "../../Constants/Spacing";
import {TextStyle} from "../../Constants/CommonStyle";
import colors from "../../Constants/colors";

export default StyleSheet.create({
  component: {
    flex: 1,
    flexDirection: "column",
    marginTop: spacing.spacing24,
    marginHorizontal: spacing.spacing16
  },
  cardViewStyle: {
    marginVertical: spacing.spacing10,
    borderRadius: spacing.spacing8,
  },
  imageBackgroundStyle: {
    flex: 1,
    minHeight: 200,
    borderRadius: spacing.spacing8,
    resizeMode: "cover",

    paddingTop: spacing.spacing8,
    paddingBottom: spacing.spacing16,
    paddingRight: 0,
    paddingLeft: spacing.spacing16,
    shadowColor: colors.grey,
    shadowOffset: { width: 0, height: 1 },
    shadowRadius: 4,
    shadowOpacity: 0.72,
    elevation: 8,

  },

  imageBackgroundViewContainer: {
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },
  viewFavContainerStyle: {
    flexDirection: 'column',
    marginRight: spacing.spacing8
  },
  imageRightStyle: {
    height: 120,
    width: '100%',
  },
  imageLeftStyle: {
    height: 120,
    width: '100%',
    marginLeft:-20,
  },
  componentHeaderStyle: {
    ...TextStyle.text_16_normal,
    lineHeight: spacing.spacing24,
    color: colors.color_212121,
    marginVertical: spacing.spacing5,
  },
  favouritesAndDismissHintViewStyle: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: spacing.spacing12,
    paddingVertical: spacing.spacing20,
    paddingHorizontal: spacing.spacing12,
    borderWidth: spacing.spacing1,
    borderColor: colors.color_E0E0E0,
    borderRadius: spacing.spacing8,
    backgroundColor: colors.color_FFFFFF,
  },
  favouritesAndDismissHintTextStyle: {
    ...TextStyle.text_14_normal,
    color: colors.color_212121,
    flex: 1,
    lineHeight: spacing.spacing22,
  },
  primaryText: {
    ...TextStyle.text_18_bold,
    color: colors.white,
    lineHeight: spacing.spacing26,
    marginTop: spacing.spacing0
  },
  secondaryText: {
    ...TextStyle.text_16_normal,
    color: colors.white,
    lineHeight: spacing.spacing22,
    marginTop: spacing.spacing12
  },
  tagViewContainerStyle: {
    flexDirection: 'row',
    marginLeft: spacing.spacing_neg_5
  },
  singleTagStyle: {
    backgroundColor: colors.color_FFAB00,
    borderWidth: spacing.spacing2,
    minHeight: spacing.spacing26,
    padding: spacing.spacing5,
    borderColor: colors.transparent,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: spacing.spacing2,
    borderRadius: spacing.spacing4,
  },
  componentStyle: {
    marginTop: spacing.spacing0,
    marginLeft: spacing.spacing0
  },
  componentContainerStyle: {
    alignSelf: 'flex-start',
    flexDirection: 'column',
    flexGrow: 1,
    marginTop: spacing.spacing10,
  },
  componentContainerParentStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  topCardViewContainerStyle: {
    flexDirection: 'row',
    minHeight: spacing.spacing24,
    paddingRight: spacing.spacing8,
    justifyContent: 'space-between'
  },
  tagsViewContainerStyle: {
    flexDirection: 'row',
    flexGrow: 1,
    marginLeft: spacing.spacing_neg_5,
  },
  standaloneRowBack: {
    alignItems: 'center',
    backgroundColor: colors.color_212121,
    borderRadius: spacing.spacing8,
    marginBottom: spacing.spacing4,
    marginTop: spacing.spacing4,
    marginHorizontal: spacing.spacing10,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  leftDeleteViewStyle: {
    marginLeft: spacing.spacing40,
    flex: 1,
    padding: spacing.spacing12,
    justifyContent: 'center'
  },
  rightDeleteViewStyle: {
    padding: spacing.spacing12,
    marginRight: spacing.spacing40,
    flex: 1,
    justifyContent: 'center'
  },
  backTextWhite: {
    color: '#FFF',
  },
  containerMenuItem: {
    flexDirection: 'column',

  }
});

