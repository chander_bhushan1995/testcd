import { HTTP_GET, HTTP_POST, executeApi, executeDocUploadRequest } from '../network/ApiManager';
import {APIData} from '../../index';

export const chatDepartmentsURL = '/lhcoarestapi/departments?showbotdep=false';
export const chatHistoryURL = '/lhcoarestapi/custchathistory';
export const chatStartURL = '/jcm/api/v4/chats';
export const chatCloseURL = '/jcm/api/v1/chats/';
export const chatStatusURL = '/lhcoarestapi/checkchatstatus';
export const chatSendMessageURL = '/lhcoarestapi/addmsguser';
export const chatSubmitRatingURL = (chatId) => { return `/jcm/api/v2/chats/${chatId}/ratings`;}
export const chatFeedbackQuestionsURL = '/lhcoarestapi/questions';
export const chatSubmitFeedbackURL = '/lhcoarestapi/capturefeedback';
export const chatAuthTokenURL = '/jcmoarestapi/v1/suggestions/auth';
export const chatAutoSuggestion = 'jcmoarestapi/v1/suggestions';
export const chatSPDocUploadURL = '/serviceplatform/api/draft/documents';
export const chatSPDraftDetailURL = '/serviceplatform/api/draft/detail?id=';
export const chatVisitorURL = '/jcm/api/v2/account?user_type=VISITOR';
export const chatTranslationURL = 'translation-ws/api/v1/translate/';


const getDataFromApiGateway = (apiParams) => {
    let params = APIData;
    apiParams.apiProps = { baseUrl: params?.api_gateway_base_url, header: params?.apiHeader };
    return apiParams;
};

const getDataFromLHC = (apiParams, authToken) => {
    let params = APIData;
    apiParams.apiProps = { baseUrl: params?.lhc_base_url, header: { ...params?.apiHeader, token: authToken } };
    return apiParams;
};

const getUploadDataFromApiGateway = (apiParams) => {
    let params = APIData;
    apiParams.apiProps = { baseUrl: params?.api_gateway_base_url, header: { ...params?.apiHeader, 'Content-Type': 'multipart/form-data' } };
    return apiParams;
};

export const username = () => {
    return APIData?.user_Name===null?"User":APIData?.user_Name;
};

export const logChatEvents = () => {
    return APIData?.logChatEvents;
};

export const mobileNumber = () => {
    return APIData?.mobile_number;
};

export const getAuthToken = (callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: chatAuthTokenURL,
        requestBody: "",
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getHashCodeForSocketConnection = (callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: chatVisitorURL,
        requestBody: "",
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
}

export const getDepartments = (callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: chatDepartmentsURL,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams));
};

export const getTranslationTexts = (inputTextArray, toLanguage, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: chatTranslationURL,
        requestBody: {
            input_list: inputTextArray,
            from_language: 'english',
            to_language: toLanguage,
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams));
};

export const getChatHistory = (callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: chatHistoryURL,
        requestBody: {
            phone: mobileNumber(),
            include_messages: true,
            msg_limit: 50,
            limit: 1
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams));
};

export const startChat = ( close_existing_chat, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: chatStartURL + "?closeExistingChat=" + (close_existing_chat ? "true" : "false"),
        requestBody: {
            userName: username(),
            phone: mobileNumber(),
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams));
};

export const startChatFTR = (callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: chatStartURL,
        requestBody: {
            intentType: "RAISE_CLAIM_FOR_PE",
            userName: username(),
            membershipId: APIData?.membership_id,
            actionType: APIData?.actionType,
            draftSrId: APIData?.draftSrId
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams));
};

export const chatClose = (chat_id, hash, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: chatCloseURL + chat_id + "/status",
        requestBody: {
            status: "CLOSED",
            hash: hash,
            dispositionType: 2,
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams));
};

export const chatExit = (chat_id, hash, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: chatCloseURL + chat_id + "/status",
        requestBody: {
            status: "EXITED",
            hash: hash,
            dispositionType: 2,
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams));
};

export const checkChatStatus = (chat_id, hash, wait_counter, callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: chatStatusURL + "?chat_id=" + chat_id + "&hash=" + hash + "&wait_counter=" + wait_counter,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams));
};

export const sendMessage = (chat_id, hash, message, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: chatSendMessageURL,
        requestBody: {
            chat_id: chat_id,
            hash: hash,
            msg: message
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams));
};

export const submitChatRating = (chat_id, stars, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: chatSubmitRatingURL(chat_id),
        requestBody: {
            stars: stars
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams));
};

export const getFeedbackQuestions = (callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: chatFeedbackQuestionsURL + "?name=RATING QUESTIONS",
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams));
};

export const submitChatFeedback = (chat_id, question_id, reason, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: chatSubmitFeedbackURL,
        requestBody: {
            chat_id: chat_id,
            questionIds: question_id,
            reason: reason,
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams));
};

export const getAutoSuggestions = (authToken, chatId, inputText, inputData, callback) => {
    let pathParam = inputData ? ("/" + inputData) : "";
    let apiParams = {
        httpType: HTTP_GET,
        url: chatAutoSuggestion + pathParam + "?query=" + inputText + "&chat_id=" + chatId,
        callback: callback,
    };
    executeApi(getDataFromLHC(apiParams, authToken));
};

export const srDraftDetail = (draftId, callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: chatSPDraftDetailURL + draftId,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams));
};

export const spDocUpload = (uploadData, callback) => {
    let data = new FormData();
    data.append('file', {
        uri: uploadData.uri,
        name: uploadData.fileName,
        type: uploadData.docType,
    });
    data.append("documentTypeId", uploadData.documentTypeId);
    data.append("draftId", uploadData.draftId);
    data.append("documentId", uploadData.documentId);

    let apiParams = {
        httpType: HTTP_POST,
        url: chatSPDocUploadURL,
        requestBody: data,
        callback: callback,
    };

    let updatedAPIParams = getUploadDataFromApiGateway(apiParams)
    setTimeout(() => {
        try {
            executeDocUploadRequest(updatedAPIParams);
        } catch (e) {
            callback(null, null);
        }
    }, 500);
}
