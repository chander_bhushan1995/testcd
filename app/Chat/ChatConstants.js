export const chatFlowStrings = {
    closing_chat: "Closing Chat",
    close_chat: "Close chat?",
    close_chat_des: "Are you sure you want to close your ongoing chat.",
    yes: "Yes",
    cancel: "Cancel",
    start_new_chat: "Start New chat",
    start_new_chat_des: "Are you sure you want to start a new conversation? This will close your ongoing chat.",
}
