import { NativeModules, AppState, Platform } from "react-native";
import {PICKED_FROM_CAMERA} from '../Constants/AppConstants';
import * as ChatAPI from "./ChatAPIHelper";
import * as ChatUtils from "./ChatUtils";
import {chatManager, localEventEmitter} from "../Navigation/index";

import {
    CHAT_HISTORY_ERROR,
    CHAT_HISTORY_REQUEST,
    CHAT_HISTORY_RESPONSE,
    START_CHAT_V2_REQUEST,
    START_CHAT_V2_RESPONSE,
    START_CHAT_V1_REQUEST,
    START_CHAT_V1_RESPONSE,
    SP_DOC_UPLOAD_FAILURE, CREATE_ACCOUNT_V2_INIT, CREATE_ACCOUNT_V2_REQUEST, CREATE_ACCOUNT_V2_RESPONSE
} from '../Constants/WebengageEvents';
import {logChatWebEngageEvent, logWebEnageEvent} from "../commonUtil/WebengageTrackingUtils";
import {chatFlowStrings} from './ChatConstants';
import { parseJSON, deepCopy} from "../commonUtil/AppUtils";

const pollingTimeInterval = 5000 //ms
const nativeBrigeRef = NativeModules.ChatBridge;

export default class ChatManager {
    static sharedInstance = null;
    _authToken = "";
    _chatId = null
    _hash = ""
    _agentId = ""
    _pollingEnabled = false
    _inputEnabled = true
    _msgIds = [];
    _waitCounter = 1;
    _depId = null;
    _isChatAssigned = false;
    _isCheckStatusInProgress = false;
    _autoSuggestionTimeStamp = null;
    _shouldCallVisitorAPI = true;
    _preferredLanguage = '';
    _ratingData = null;
    _sentAtText = '';
    _translatedData=[];
    _firstSocketMessageShown = false;
    /**
     * @returns {ChatManager}
     */
    static getInstance() {
        if (ChatManager.sharedInstance == null) {
            ChatManager.sharedInstance = new ChatManager();
        }

        return this.sharedInstance;
    }

    resetActiveDeptID() {
        this._depId = null
    }
    resetHash() {
        this._hash = ""
    }
    resetChatId() {
        this._chatId = null
    }

    isInputEnabled() {
        return this._inputEnabled
    }

    isFirstSocketMessageShown() {
        return this._firstSocketMessageShown
    }

    setFirstSocketMessageShown(value) {
        this._firstSocketMessageShown = value
    }

    getAuthToken() {
        ChatAPI.getAuthToken((response, error) => {
            let authToken = response?.data?.token;
            if (authToken) {
                this._authToken = authToken
            }
        })
    }

    createSocketConnection(callback) {
        logChatWebEngageEvent(ChatAPI.logChatEvents, CREATE_ACCOUNT_V2_INIT);
        if(this._shouldCallVisitorAPI)
        {
            logChatWebEngageEvent(ChatAPI.logChatEvents, CREATE_ACCOUNT_V2_REQUEST);
            this._shouldCallVisitorAPI=false;
            ChatAPI.getHashCodeForSocketConnection((response, error) => {
                logChatWebEngageEvent(ChatAPI.logChatEvents, CREATE_ACCOUNT_V2_RESPONSE);
                this._shouldCallVisitorAPI=true;
                let hashCode = response?.data?.hashCode ?? ""
                if (hashCode.length) {
                    nativeBrigeRef.connectSocketWithPassword(hashCode, (connected) => {
                        callback((connected == "true"))
                    })
                } else {
                    callback(false)
                }
            })
        }
        else
        {
            callback(false);
        }

    }

    history(callback) {
        ChatAPI.getChatHistory((response, error) => {
            if (error) {
                callback(null, error)
            } else {
                callback(response?.data?.chats[0], null)
            }
        })
    }

    getTranslations(callback) {
        let array = [chatFlowStrings.closing_chat, chatFlowStrings.close_chat, chatFlowStrings.close_chat_des, chatFlowStrings.yes, chatFlowStrings.cancel, chatFlowStrings.start_new_chat, chatFlowStrings.start_new_chat_des];
        ChatAPI.getTranslationTexts(array, this._preferredLanguage, callback);
    }

    getHistoryData(callback) {
        logChatWebEngageEvent(ChatAPI.logChatEvents, CHAT_HISTORY_REQUEST);
        this.history((chat, error) => {
            if (error) {
                logChatWebEngageEvent(ChatAPI.logChatEvents, CHAT_HISTORY_ERROR);
                callback(null, error, false)
            } else {
                logChatWebEngageEvent(ChatAPI.logChatEvents, CHAT_HISTORY_RESPONSE);
                if (chat && (chat?.status != 2)) {
                    // The chat is not closed
                    this._chatId = chat?.id
                    this._hash = chat?.hash ?? ""
                    this._depId = chat?.dep_id
                    this._pollingEnabled = true
                    this._msgIds = chat?.messages?.map(message => message?.ejb_msg_id) ?? []
                    this._waitCounter = 1;
                    this._pollingEnabled = true
                    callback(ChatUtils.formatHistory(chat), error, false)
                    this.startCheckingChatStatus()
                } else {
                    // The chat is closed
                    callback(null, error, true)
                }
            }
        })
    }

    startChat(callback) {
        this._ratingResponse=null;
        logChatWebEngageEvent(ChatAPI.logChatEvents, START_CHAT_V1_REQUEST);
        this._pollingEnabled = false
        ChatAPI.startChat(false, (response, error) => {
            this.startChatHandler(START_CHAT_V1_RESPONSE, response, error, callback)
        })
    }

    startChatFTR(callback) {
        this._ratingResponse=null;
        logChatWebEngageEvent(ChatAPI.logChatEvents, START_CHAT_V2_REQUEST);
        this._pollingEnabled = false
        ChatAPI.startChatFTR((response, error) => {
            this.startChatHandler(START_CHAT_V2_RESPONSE, response, error, callback)
        })
    }

    startChatHandler(event, response, error, callback) {
        let chat = response?.data
        if (chat?.id) {
            logChatWebEngageEvent(ChatAPI.logChatEvents, event);
            this._chatId = chat?.id
            this._hash = chat?.hash ?? ""
            this._depId = chat?.departmentId
            this._inputEnabled = chat?.isInputEnabled
            this._agentId = chat?.supportUserJID
            this._isChatAssigned = ((chat?.supportUserJID?.length ?? 0) !== 0)
            this._msgIds = [];
            this._waitCounter = 1;
            this._pollingEnabled = true
            localEventEmitter.emit("InputBoxEvent")
            this.startCheckingChatStatus()
        }
        callback(response, error)
    }

    checkChatStatus(callback) {
        if (this._chatId && this._hash?.length) {
            ChatAPI.checkChatStatus(this._chatId, this._hash, this._waitCounter, (response, error) => {
                let data = response?.data
                if (data) {
                    if (data?.jid_support?.length)
                        this._agentId = data?.jid_support
                    this._inputEnabled = !(data?.inputdisabled ?? false)
                    localEventEmitter.emit("InputBoxEvent")
                    this._isChatAssigned = data?.status != 0
                    let message = data?.message
                    if (message?.length) {
                        localEventEmitter.emit("LocalMessage", { message: message, from: this._agentId, chatId: this._chatId })
                    }
                }
                this._waitCounter = data?.wait_counter ?? this._waitCounter
                callback()
            })
        } else {
            callback()
        }
    }

    startCheckingChatStatus() {
        if (!this._isChatAssigned && this._pollingEnabled) {
            this.checkChatStatus(() => setTimeout(() => this.startCheckingChatStatus(), pollingTimeInterval))
        } else {
            this._pollingEnabled = false
        }
    }

    checkAgentMismatch(agentId) {
        if (agentId != this._agentId && !this._isCheckStatusInProgress) {
            this._isCheckStatusInProgress = true
            this.checkChatStatus(() => {
                this._isCheckStatusInProgress = false
            })
        }
    }

    sendMessage(message, callback) {
        if (this._chatId && this._hash?.length) {
            ChatAPI.sendMessage(this._chatId, this._hash, message, callback)
        } else {
            callback(null, null)
        }
    }

    submitRating(stars, callback) {
        if (this._chatId) {
            ChatAPI.submitChatRating(this._chatId, stars, (response, error) => {
                callback(response, error)
            })
        } else {
            callback(null, null)
        }
    }

    feedbackQuestions(callback) {
        ChatAPI.getFeedbackQuestions(callback)
    }

    submitFeedback(questionIds, reason, callback) {
        if (this._chatId) {
            ChatAPI.submitChatFeedback(this._chatId, questionIds, reason, (response, error) => {
                this._chatId = null
                callback(response, error)
            })
        } else {
            callback(null, null)
        }
    }

    closeChat(callback) {
        if (this._chatId && this._hash?.length) {
            ChatAPI.chatClose(this._chatId, this._hash, callback)
        } else {
            let chatExit={"isChatExit":true};
            callback(chatExit, null)
        }
    }

    exitChat() {
        if (this._chatId && this._hash?.length) {
            ChatAPI.chatExit(this._chatId, this._hash, () => { })
        }
    }

    getAutoSuggestions(inputText, inputData, callback) {
        let CurrentTime = Date.now();
        this._autoSuggestionTimeStamp = CurrentTime;
        ChatAPI.getAutoSuggestions(this._authToken, this._chatId, inputText, inputData, (response, error) => {
            if (this._autoSuggestionTimeStamp === CurrentTime) {
                callback(response, error)
            } else {
                callback(null, null)
            }
        });
    }

    srDraftDetail(draftId, callback) {
        ChatAPI.srDraftDetail(draftId, callback)
    }

    spDocUpload(uploadData, callback) {
        ChatAPI.spDocUpload(uploadData, (response, error) => {
            if (error) {
                logWebEnageEvent(SP_DOC_UPLOAD_FAILURE, { message: error?.message, fileName: uploadData.fileName });
            }
            callback(response, error)
        })
    }

    spDocumentDownload(downloadRequest) {
        nativeBrigeRef.downloadDocumentFromSP(downloadRequest.fileTypeRequired, downloadRequest.storageRefId, uriPath => {
            downloadRequest.callback(uriPath, downloadRequest.index);
        });
    }

    uploadDocument(uri, pickedFrom, callback) {
        const responseCallback = (response) => {
            let result = parseJSON(response)
            if (result.status == 1) {
                this.sendMessage(result.url, (response, error) => {
                    if (error) {
                        callback(false)
                    } else {
                        callback(true)
                    }
                })
            } else {
                callback(false)
            }
        }
        if (Platform.OS === "ios") {
            nativeBrigeRef.uploadDocument(uri, responseCallback)
        } else {
            if (pickedFrom === PICKED_FROM_CAMERA) {
                nativeBrigeRef.uploadDocumentFromCamera(uri, responseCallback);
            } else {
                nativeBrigeRef.uploadDocumentFromGallery(uri, responseCallback);
            }
        }
    }

    downloadTemplate(downloadRequest) {
        nativeBrigeRef.downloadTemplate(downloadRequest.draftId, uriPath => {
            // downloadRequest.callback(uriPath, downloadRequest.index);
        });
    }

    checkAndShowLocalNotification(message) {
        if (AppState.currentState === "background" && message?.length) {
            var notificationMessage = message
            if (ChatUtils.isDocURL(message)|| ChatUtils.isJson(message)) {
                notificationMessage = "You have one new message!"
            }

            nativeBrigeRef.showLocalNotification(notificationMessage)
        }
    }

    showMessage(msgId, chatId) {
        var show = true
        if (chatId && chatId != this._chatId) {
            show = false
        } else if (msgId?.length) {
            if (this._msgIds.includes(msgId)) {
                show = false
            } else {
                this._msgIds.push(msgId)
            }
        }
        return show
    }

    clearPropsAndCloseConnection() {
        this._depId = null;
        this._chatId = null
        this._hash = ""
        this._agentId = ""
        this._pollingEnabled = false
        this._isChatAssigned = false
        this._inputEnabled = true
        this._msgIds = [];
        this._waitCounter = 1;
        this._isCheckStatusInProgress = false;
        this._autoSuggestionTimeStamp = null;
        this._shouldCallVisitorAPI = true;
        this._preferredLanguage = '';
        this._ratingData = null;
        this._translatedData=[];
    }

    getPreferredLanguage() {
        return this._preferredLanguage;
    }
    getTranslatedData() {
        return this._translatedData;
    }

    setPreferredLanguage(language) {
        this._preferredLanguage = language;
        chatManager.getTranslations((response, error) => {
            if (error) {
            } else {
                this._translatedData=response?.data
            }
        })
    }

    getRatingData() {
        return this._ratingData;
    }

    setRatingData(ratingData) {
        this._ratingData = ratingData;
    }

    setRatingResponse(ratingResponse)
    {
        this._ratingResponse = ratingResponse;
    }
    getRatingResponse() {
        return this._ratingResponse;
    }

    getSentAtText() {
        return this._sentAtText;
    }

    setSentAtText(sentAtText) {
        this._sentAtText = sentAtText;
    }
}
