
import { supportedDocumentType } from "../Constants/DocumentTypeConstant"
import * as Actions from "../Constants/ActionTypes";
import { BottomViewType } from "../Constants/BottomViewType";
import ChatbotResponseParser from "../ChatbotComponent/ChatbotResponseParser";
import { DOCUMENT_TYPE, IMAGE_TYPE } from "../Constants/AppConstants";
import { parseJSON, deepCopy} from "../commonUtil/AppUtils";

export const chatAnimationTimes = {
    logo: { delay: 100, duration: 300 },
    typing: { delay: 250, duration: 250 },
    resize: { delay: 1500, duration: 250 },
    contentAppear: { delay: 100, duration: 500 }
}

export const isJson = (str) => {
    try {
        JSON.parse(str)
        return (typeof (JSON.parse(str)) === "object");
    } catch (e) {
        return false;
    }
    return true
}

export const documentType = (format) => {
    if (["jpg", "jpeg", "png"].includes(format)) {
        return IMAGE_TYPE;
    } else {
        return DOCUMENT_TYPE;
    }
}

export const documentDataFromDocUrl = (docUrl) => {
    var docDetail = docUrl.substring(docUrl.lastIndexOf("/") + 1);
    var docFormat = docDetail.substring(docDetail.lastIndexOf(".") + 1);
    var docName = docDetail.substring(0, docDetail.lastIndexOf("."));
    var docType = documentType(docFormat.toString().toLowerCase());
    return { docType: docType, docFormat: docFormat, docName: docName };
}

export const dateFromTime = (time) => {
    var date = new Date()
    date.setTime(time*1000)
    return date
}

export const dateTimePickerType = (type) => {
    switch (type) {
        case 1: return "time";
        case 2: return "date";
        case 3: return "datetime";
        default: return "datetime";
    }
}

export const isToday = (date) => {
    const today = new Date();
    return today.getFullYear() == date.getFullYear() && today.getMonth() == date.getMonth() && today.getDate() == date.getDate()
}

export const formattedMessageDate = (date) => {
    var dateFormat = require('dateformat');
    return dateFormat(date, isToday(date) ? "hh:MM TT" : "dd-mmm, hh:MM TT");
}

export const isDocURL = (str) => {
    var expression = /^(https?:\/\/)[-a-zA-Z0-9.+:]{2,256}\/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.+[a-zA-Z]{2,4}$/gi;
    var regex = new RegExp(expression);
    return regex.test(str);
};

export const docExtension = (filePath) => {
    return filePath.substring(filePath.lastIndexOf(".") + 1);
}

export const isSupportedDoc = (filePath) => {
    return supportedDocumentType.find((docType) => { return docType == docExtension(filePath) })
};

export const formatHistory = (chat) => {
    var updatedHistoryData = [];
    let bottomViewData = null;
    let bottomViewType = BottomViewType.None;
    let messages = chat?.messages
    if (chat?.dep_id && messages?.length)
        for (var i = 0; i < messages.length; i++) {
            let message = messages[i]
            let value;
            if (message?.meta_msg?.length) {
                value = message?.meta_msg
            } else {
                let msg = message?.msg
                value = isJson(msg) ? (parseJSON(msg)?.text ?? msg) : msg
            }

            if (value && message.user_id != -1) {
                var timeStamp = formattedMessageDate(dateFromTime(message?.time))

                if (isDocURL(value) && isSupportedDoc(value)) {
                    var docUrl = value;
                    let docData = documentDataFromDocUrl(docUrl);
                    updatedHistoryData.push({
                        type: message.user_id == 0 ? Actions.DOCUMENT : Actions.AGENT_SIDE_DOCUMENT,
                        docUrl: docUrl,
                        docType: docData.docType,
                        fileType: docData.docFormat,
                        fileName: docData.docName,
                        timeStamp: timeStamp,
                        animationCompleted: true
                    });
                } else if (isJson(value)) {
                    var chatbotResponseParser = new ChatbotResponseParser();
                    var chatbotData = chatbotResponseParser.chatbotDataFromResponse(value);
                    bottomViewType = BottomViewType.None;
                    var bottomView = chatbotData?.bottomView
                    if (bottomView) {
                        let actionType = bottomView.actionType;
                        bottomViewData = bottomView.componentData;
                        if (actionType === "input" || actionType === "dateTimeInput") {
                            bottomViewType = BottomViewType.ChatbotInput
                        } else if (actionType === "formData") {
                            bottomViewType = BottomViewType.ChatbotForm
                        } else if (actionType === "uploadDoc") {
                            bottomViewType = BottomViewType.ChatbotUploadForm
                        } else if (actionType === "tagInput") {
                            bottomViewType = BottomViewType.ChatbotTagInput
                        }
                    }
                    updatedHistoryData.push({
                        type: Actions.BOT,
                        value: chatbotData.list,
                        timeStamp: timeStamp,
                        animationCompleted: true
                    });
                } else {
                    updatedHistoryData.push({
                        type: message.user_id == 0 ? Actions.SEND_MESSAGE : Actions.RECEIVE_MESSAGE,
                        value: value,
                        timeStamp: timeStamp,
                        animationCompleted: true
                    });
                }
            }
        }

    return {
        list: updatedHistoryData.reverse(),
        bottomViewType: bottomViewType,
        bottomViewData: bottomViewData,
        autoSuggestionEnabled: chat?.auto_suggestion_enabled
    }
}