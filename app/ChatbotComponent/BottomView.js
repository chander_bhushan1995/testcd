import InputLayout from "../Components/InputLayout";
import ChatbotFormBottomView from "./ChatbotFormBottomView";
import ChatbotInput from "./ChatbotInput";
import React, {Component} from "react";
import {BottomViewType} from "../Constants/BottomViewType";
import ChatbotTagInput from './ChatbotTagInput';
import { chatManager } from "../Navigation/index";
import ChatbotSimpleButton from "./ChatbotSimpleButton";

export default class BottomView extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.bottomViewType === BottomViewType.Input) {
            return <InputLayout
                selectedSuggestionData={this.props.selectedSuggestionData}
                plusIconClick={this.props.plusIconClick}
                sendMessage={this.props.sendMessage}
                autoSuggestionVisible={this.props.autoSuggestionVisible}
                autoSuggestionEnabled={this.props.autoSuggestionEnabled}
                getAutosuggestions={this.props.getAutosuggestions}
                updateBottomMarginAutoSuggestionView={this.props.updateBottomMarginAutoSuggestionView}
                resetSelectedSuggestion={this.props.resetSelectedSuggestion}
                delayAfterPauseTypingTime={this.props.delayAfterPauseTypingTime}
				shouldClearInput={this.props.shouldClearInput}
            />;
        }
        else if (this.props.bottomViewType === BottomViewType.ChatbotInput) {
            return <ChatbotInput
                data={this.props.bottomViewData}
                sendMessage={this.props.sendMessage}
                updateBottomView={this.props.updateBottomView}
                selectedSuggestionData={this.props.selectedSuggestionData}
                autoSuggestionVisible={this.props.autoSuggestionVisible}
                autoSuggestionEnabled={this.props.autoSuggestionEnabled}
                plusIconClick={this.props.plusIconClick}
                getAutosuggestions={this.props.getAutosuggestions}
                updateBottomMarginAutoSuggestionView={this.props.updateBottomMarginAutoSuggestionView}
                resetSelectedSuggestion={this.props.resetSelectedSuggestion}
                delayAfterPauseTypingTime={this.props.delayAfterPauseTypingTime}
                shouldClearInput={this.props.shouldClearInput}
                updateAutoSuggestions={this.props.updateAutoSuggestions}
            />
        } else if (this.props.bottomViewType === BottomViewType.ChatbotForm ||
            this.props.bottomViewType === BottomViewType.ChatbotUploadForm) {
            return <ChatbotFormBottomView
                data={this.props.bottomViewData}
                sendMessage={this.props.sendMessage}
                formType={this.props.bottomViewType}
                handleChevornUpTap={this.props.handleChevornUpTap}
            />
        }else if (this.props.bottomViewType === BottomViewType.ChatbotTagInput) {
            return <ChatbotTagInput
                data={this.props.bottomViewData}
                sendMessage={this.props.sendMessage}
                updateBottomView={this.props.updateBottomView}
                selectedSuggestionData={this.props.selectedSuggestionData}
                autoSuggestionVisible={this.props.autoSuggestionVisible}
                autoSuggestionEnabled={this.props.autoSuggestionEnabled}
                plusIconClick={this.props.plusIconClick}
                getAutosuggestions={this.props.getAutosuggestions}
                updateBottomMarginAutoSuggestionView={this.props.updateBottomMarginAutoSuggestionView}
                resetSelectedSuggestion={this.props.resetSelectedSuggestion}
                shouldClearInput={this.props.shouldClearInput}
                updateAutoSuggestions={this.props.updateAutoSuggestions}
            />
        }
        else if (this.props.bottomViewType === BottomViewType.StartChat && chatManager?.getRatingResponse()?.showStartChatBtn ) {
            return <ChatbotSimpleButton onClick={this?.props?.startNewChat} text={chatManager?.getRatingResponse()?.startChatBtnLabel ?? "Start a new chat"}/>
        } else
            return null;
    }
}
