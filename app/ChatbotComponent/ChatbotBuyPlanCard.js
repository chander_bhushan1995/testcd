import React, { Component } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";
import { TextStyle, CommonStyle, ButtonStyle } from "../Constants/CommonStyle";

// var data = {
//     id: 3,
//     value: {
//         text: 'Extended warranty plan',
//         heading1: '\\u20b9 2899 only/year',
//         listTitle: 'Key Benefits',
//         listPoints: [
//             'Docusafe - Get an e-locker to store all your personal data & documents. Access all your important documents, anywhere, anytime',
//             'Extended Warranty Service'
//         ]
//     },
//     buttons_inline: [
//         {
//             button_id: 0,
//             value: {
//                 label: 'See Benefits : Prolong for Mobile 60K to 80K (\\u20b9 2899 only/year)',
//                 text: 'See All Benefits'
//             },
//             params: {
//                 key: 'see benefits of plan',
//                 planCode: 294
//             }
//         },
//         {
//             button_id: 1,
//             value: {
//                 label: 'Buy Plan : Prolong for Mobile 60K to 80K (\\u20b9 2899 only/year)',
//                 text: 'Buy Now'
//             },
//             params: {
//                 key: 'buy plan',
//                 planCode: 294
//             }
//         }
//     ]
// }

export default class ChatbotBuyPlanCard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={style.MainContainer}>
                <Text style={[style.text, TextStyle.text_14_bold]}>{this.props.data.value.text}</Text>
                <Text style={[style.heading1, TextStyle.text_12_medium]}>{this.props.data.value.heading1}</Text>
                <Text style={[style.listTitle, TextStyle.text_10_bold]}>{this.props.data.value.listTitle.toUpperCase()}</Text>
                {this.props.data.value.listPoints.map(point =>
                    <View style={style.listView}>
                        <Text style={TextStyle.text_12_medium}>-</Text>
                        <Text style={[style.listText, TextStyle.text_12_medium]}>{point}</Text>
                    </View>
                )}
                <View style={[CommonStyle.separator, { marginTop: spacing.spacing16 }]} />
                <View style={style.bottomView}>
                    <TouchableOpacity style={style.touchableArea} onPress={() => {
                        this.props.onClick(this.props.data.buttons_inline[0].value.label, this.props.data.buttons_inline[0].params, null);
                    }}>
                        <Text style={[style.claimRaiseButton, ButtonStyle.TextOnlyButton]}>{this.props.data.buttons_inline[0].value.text}</Text>
                    </TouchableOpacity>
                    <View style={style.buttonSeprater} />
                    <TouchableOpacity style={style.touchableArea} onPress={() => {
                        this.props.onClick(this.props.data.buttons_inline[1].value.label, this.props.data.buttons_inline[1].params, null);
                    }}>
                        <Text style={[style.claimRaiseButton, ButtonStyle.TextOnlyButton]}>{this.props.data.buttons_inline[1].value.text}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
const style = StyleSheet.create({
    MainContainer: {
        backgroundColor: colors.white,
        borderRadius: spacing.spacing4,
        overflow: "hidden",
    },
    buttonSeprater: {
        width: 1,
        height: "100%",
        backgroundColor: colors.lightGrey2,
    },
    bottomView: {
        flexDirection: "row",
        alignContent: "space-around"
    },
    text: {
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing12,
        marginTop: spacing.spacing20
    },
    heading1: {
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing12,
        marginTop: spacing.spacing4
    },
    listTitle: {
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing12,
        marginTop: spacing.spacing12,
        lineHeight: 16,
        letterSpacing: 1,
    },
    listView: {
        flexDirection: "row",
        marginTop: spacing.spacing4,
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing12
    },
    listText: {
        marginLeft: spacing.spacing4,
    },
    touchableArea: {
        width: "50%",
        padding: 14,
    },
    claimRaiseButton: {
        textAlign: "center",
    }
});
