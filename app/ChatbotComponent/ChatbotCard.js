import React from "react";
import {FlatList, StyleSheet, Text, View, Image} from "react-native";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";
import { TextStyle} from "../Constants/CommonStyle";

// const data = {
//     list: [
//         {
//             heading1: "Please upload an image of the damaged part / area in focus",
//             heading2: "Please make sure image is not blurry",
//             docKey: "SCREEN_IMAGE",
//             docId: 1282,
//             isUploaded: false
//         },
//         {
//             heading1: "Please upload an image of Government issued ID proof",
//             heading2: "Please make sure image is not blurry",
//             docKey: "ID_PROOF",
//             docId: 1280,
//             isUploaded: false
//         },
//         {
//             heading1: "To continue with your request, please upload the invoice copy.",
//             heading2: "Please make sure image is not blurry.",
//             docKey: "INVOICE_IMAGE",
//             docId: 1290,
//             isUploaded: false
//         }
//     ],
//     note: "We might need few additional documents"
// }

const ChatbotCard = props => {
    let points = props.data.list.filter(data => (data.isUploaded === true));
    let noteView = props.data.note !== null && props.data.note !== undefined && points?.length > 0? <View style={style.listView}>
        <Image source={require("../images/yellow_indicator.webp")}
               style={style.noteImageView}/>
        <View style={style.rightView}>
            <Text style={[ TextStyle.text_12_bold, {color: colors.charcoalGrey}]}>{props.data.note}</Text>
        </View>
    </View> : null;
    return (
        <View style={style.MainContainer}>
            <FlatList
                data={props.data.list}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item, index}) => {
                    let heading1 = <Text style={[{color: colors.charcoalGrey},TextStyle.text_14_bold]}>{item.heading1}</Text> ;
                    let heading2 = <Text style={[{marginTop: spacing.spacing4},TextStyle.text_12_normal]}>{item.heading2}</Text>;
                    let greenTick = item.isUploaded?<Image source={require("../images/green_right.webp")}
                                                           style={style.greenTickImageView}/> : null;
                    return (
                        <View style={style.listView}>
                            <View style={style.leftView}/>
                            <View style={style.rightView}>
                                {heading1}
                                {heading2}
                            </View>
                            {greenTick}
                        </View>
                    )
                }}
            />
            {noteView}
        </View>
    );
};

const style = StyleSheet.create({
    MainContainer: {
        borderRadius: spacing.spacing8,
        overflow: "hidden",
        backgroundColor: colors.lightBlue,
        paddingBottom: spacing.spacing12,
    },
    listView: {
        flexDirection: "row",
        paddingRight: spacing.spacing16
    },
    leftView: {
        marginLeft: spacing.spacing12,
        marginTop: spacing.spacing20,
        marginRight: spacing.spacing8,
        width: 6,
        height: 6,
        backgroundColor: colors.charcoalGrey,
        borderRadius: 3,
    },
    rightView: {
        marginTop: spacing.spacing12,
        flex: 1,
    },
    noteImageView: {
        height: 14,
        width: 14,
        resizeMode: "contain",
        alignSelf: 'center',
        marginLeft: spacing.spacing8,
        marginTop: spacing.spacing12,
        marginRight: spacing.spacing4,
    },
    greenTickImageView: {
        height: 20,
        width: 20,
        resizeMode: "contain",
        alignSelf: 'center',
    },
});

export default ChatbotCard;
