import React, {useState} from 'react';
import {Image, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import colors from '../Constants/colors';
import spacing from '../Constants/Spacing';
import {TextStyle} from '../Constants/CommonStyle';
import CheckBox from 'react-native-check-box';

// TODO: KAG, check here?
const ChatbotCheckboxWithText = props => {
    const [value, setValue] = useState(false);
    let checkbox = props.type === 'Others' ? null : <CheckBox
        style={style.checkboxStyle}
        isChecked={value}
        checkedImage={<Image source={require('../images/ic_check_box.webp')} style={style.checkbox}/>}
        unCheckedImage={<Image source={require('../images/ic_check_box_outline_blank.webp')} style={style.checkbox}/>}
        onClick={() => {
            onItemClick();
        }
        }
    />;

    function onItemClick() {
        setValue(prevState => {
            const newState = !prevState;
            const clickedData = {text: props.text, type: props.type, isSelected: newState};
            props.onClick(clickedData);
            return newState;
        });
    }

    return (
        <View style={{flex: 1}}>
            <TouchableOpacity onPress={() => {
                if (props.type === 'Others') {
                    const clickedData = {text: props.text, type: props.type, isSelected: 'true'};
                    props.onClick(clickedData);
                } else {
                    onItemClick();
                }
            }}>
                <View style={props.type === 'Others' ? style.containerWithNone : style.containerWithCheckBox}>
                    <Text
                        style={value ? [TextStyle.text_12_bold, style.textStyleSelected] : [TextStyle.text_12_medium, style.textStyleUnSelected]}>{props.text}</Text>
                    {checkbox}
                </View>
            </TouchableOpacity>
        </View>
    );
};

const style = StyleSheet.create({
    containerWithCheckBox: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: spacing.spacing16,
    },
    containerWithNone: {
        flexDirection: 'row',
        justifyContent: 'center',
        padding: spacing.spacing16,
    },
    textStyleUnSelected: {
        color: colors.darkGrey,
    },
    textStyleSelected: {
        color: colors.charcoalGrey,
    },
    checkboxStyle: {
        width: 18,
        height: 18,
        marginRight: spacing.spacing4,
    },
    checkbox: {
        height: 16,
        width: 16,
    },
});

export default ChatbotCheckboxWithText;
