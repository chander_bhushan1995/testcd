import React, { useState, useEffect } from "react";
import { StyleSheet, View } from "react-native";
import { createAnimatableComponent } from 'react-native-animatable';
import TypingAnimatedComponent from "./TypingAnimatedComponent";
import ChatbotSimpleCard from "./ChatbotSimpleCard";
import ChatbotSimpleCardHorizontal from "./ChatbotSimpleCardHorizontal";
import ChatbotDateTimeCard from "./ChatbotDateTimeCard";

const AnimatableView = createAnimatableComponent(View);

export default function ChatbotComponent(props) {
    const { show, animationCompleted, animationCompletion, botData, onSendMessage, onOpenWebViewAndSendMessage } = props;
    const initialAnimationIndex = () => animationCompleted ? (botData?.length ?? 0) : (show ? 0 : -1)
    const [animationDoneIndex, setAnimationDoneIndex] = useState(initialAnimationIndex());

    useEffect(() => setAnimationDoneIndex(initialAnimationIndex()), [show])

    useEffect(() => {
        if (isAnimationComplete())
            animationCompletion()
    }, [animationDoneIndex])

    const onClick = (text, params) => onSendMessage({ "text": text, "params": params ?? "" })
    const onOpenWebView = (option) => onOpenWebViewAndSendMessage(option)
    const isAnimationComplete = () => animationDoneIndex == (botData?.length ?? 0)
    const showView = (index) => animationDoneIndex >= index
    const animationDone = (index) => animationDoneIndex >= index + 1
    const withoutAnimation = (item) => item.ComponantName == ChatbotSimpleCard || item.ComponantName == ChatbotSimpleCardHorizontal || item.ComponantName == ChatbotDateTimeCard

    const chatbotComponent = (item) => {
        return <item.ComponantName data={item.componentData} actionType={item.actionType} onClick={onClick} onOpenWebView={onOpenWebView} />
    }

    const componentView = (item, index) => {
        if (showView(index)) {
            if (withoutAnimation(item)) {
                // TODO: KAG, remove animatable view and directly setAnimationDoneIndex
                return (<AnimatableView
                    key={index}
                    animation={animationDone(index) ? null : "fadeIn"}
                    duration={10}
                    style={{ flex: 1, paddingLeft: 8 }}
                    onAnimationEnd={() => setAnimationDoneIndex(animationDoneIndex + 1)}
                >
                    {chatbotComponent(item)}
                </AnimatableView>)
            } else {
                return (<TypingAnimatedComponent
                    key={index}
                    isTyping={false}
                    isAlreadyAnimated={animationDone(index)}
                    animationCompleted={() => setAnimationDoneIndex(animationDoneIndex + 1)}
                >
                    {chatbotComponent(item)}
                </TypingAnimatedComponent>)
            }
        } else {
            return null;
        }
    }

    const selfView = () => {
        let botViewList = [];
        for (let index = 0; index < botData.length; index++) {
            let item = botData[index]
            if (index !== 0) {
                botViewList.push(<View key={Math.floor(Math.random() * 1000) + 1} style={style.chatbotComponentSeparator} />);
            }
            botViewList.push(componentView(item, index));
        }

        return (
            <View>
                {botViewList}
            </View>
        );
    }

    return selfView();
}

const style = StyleSheet.create({
    chatbotComponentSeparator: {
        height: 8
    }
});
