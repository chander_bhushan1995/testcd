import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import colors from '../Constants/colors';
import spacing from '../Constants/Spacing';
import {TextStyle} from '../Constants/CommonStyle';
import * as Actions from "../Constants/ActionTypes";

// const data = {
//     "cardType": "text",
//     "placeHolder": "Select date & time"
// }

const ChatbotDateTimeCard = props => {
    return (
        <View style={style.MainContainer}>
            <TouchableOpacity onPress={() => {
                props.data.params = {actionType: Actions.DATE_TIME, dataTimePickerType: props.data.dateTimePickerType}
                props.onClick(props.data.placeHolder, props.data.params, null);
            }}>
                <View style={style.ButtonContainer}>
                    <Text style={[TextStyle.text_14_bold,style.text]}>{props.data.placeHolder}</Text>
                </View>
            </TouchableOpacity>
        </View>
    );
}

const style = StyleSheet.create({
    MainContainer: {
        alignSelf: "flex-end",
        marginTop: spacing.spacing8,
    },
    ButtonContainer: {
        alignSelf:'center',
        borderRadius: spacing.spacing36,
        backgroundColor: colors.white,
        borderWidth: spacing.spacing1,
        marginLeft: spacing.spacing12,
        borderColor: colors.blue,
        marginBottom:36,
        overflow: "hidden"
    },
    text: {
        color: colors.blue,
        alignContent: "center",
        marginVertical: spacing.spacing8,
        marginHorizontal: spacing.spacing16,
    },
});

export default ChatbotDateTimeCard;
