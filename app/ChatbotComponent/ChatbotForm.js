import React, {Component} from "react";
import {FlatList, Image, NativeModules, SafeAreaView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import DateTimePicker from 'react-native-modal-datetime-picker';
import ChatbotFormSingleSelectCell from "./ChatbotFormSingleSelectCell"
import ChatbotFormTextCell from "./ChatbotFormTextCell";
import {TextStyle, CommonStyle, ButtonStyle} from '../Constants/CommonStyle';
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";
import ChatbotFormMultiSelectCell from './ChatbotFormMultiSelectCell';
import DialogView from "../Components/DialogView";
import { parseJSON, deepCopy} from "../commonUtil/AppUtils";
import { dateTimePickerType } from "../Chat/ChatUtils";

const chatBrigeRef = NativeModules.ChatBridge;
const FUTURE_TIME_ERROR="You’re providing a future time of the incident. Please provide an accurate time.";
const TITLE="Invalid Time!";
export default class ChatbotForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isDateTimePickerVisible: false,
            datePickerType: "datetime",
            formItemIndexForDateTime: -1,
            productIcon: require("../images/product.webp"),
            formData: this.props.navigation.getParam("data", null)
        };
        chatBrigeRef.getImageUrl(this.state.formData.iconCode.substring(this.state.formData.iconCode.lastIndexOf("-") + 1), imageUrl => {
            this.setState({productIcon: imageUrl != null ? {uri: imageUrl} : require("../images/product.webp")});
        });
    }

    _showDateTimePicker = (index, type) => this.setState({
        isDateTimePickerVisible: true,
        datePickerType: type,
        formItemIndexForDateTime: index
    });

    _hideDateTimePicker = () => this.setState({
        isDateTimePickerVisible: false,
        datePickerType: "datetime",
        formItemIndexForDateTime: -1
    });

    _updateFormItem = (data, index) => {
        let newData = this.state.formData;
        newData.formItems[index] = data;
        this.setState({formData: newData});
    };

    render() {
        var onFormButtonPress = this.props.navigation.getParam("onFormButtonPress", null);

        let button0;
        let button1;
        if (this.state.formData.buttons.length === 1) {
            button0 = null;
            button1 = <TouchableOpacity style={[style.buttonTouchArea, ButtonStyle.BlueButton]} onPress={() => {
                this.props.navigation.goBack();
                var data = {
                    text: this.state.formData.buttons[0].value.label,
                    params: this.state.formData
                };
                onFormButtonPress(data);
            }}>
                <Text
                    style={ButtonStyle.primaryButtonText}>{this.state.formData.buttons[0].value.text}</Text>
            </TouchableOpacity>;

        } else {
            button0 = <TouchableOpacity style={style.buttonTouchArea} onPress={() => {
                this.props.navigation.goBack();
                var data = {
                    text: this.state.formData.buttons[0].value.label,
                };
                onFormButtonPress(data);
            }}>
                <Text style={style.formButton}>{this.state.formData.buttons[0].value.text}</Text>
            </TouchableOpacity>;
            button1 = <TouchableOpacity style={style.buttonTouchArea} onPress={() => {
                this.props.navigation.goBack();
                var data = {
                    text: this.state.formData.buttons[1].value.label,
                    params: this.state.formData
                };
                onFormButtonPress(data);
            }}>
                <Text
                    style={ButtonStyle.primaryButtonText}>{this.state.formData.buttons[1].value.text}</Text>
            </TouchableOpacity>;
        }
        return (
            <SafeAreaView style={style.SafeArea}>
                <DialogView ref={ref => (this.DialogViewRef = ref)}/>
                <View style={style.MainContainer}>
                    <View style={style.topView}>
                        <TouchableOpacity activeOpacity={1} onPress={() => this.props.navigation.goBack()}>
                            <View style={style.headerView}>
                                <Text style={style.headerViewTitle}>{this.state.formData.formLabel.toUpperCase()}</Text>
                                <Image source={require("../images/icon_chevornDown.webp")} style={style.minimiseImage} />
                            </View>
                        </TouchableOpacity>
                        <View style={style.subHeaderView}>
                            <Image source={this.state.productIcon}
                                style={style.assetImage}/>
                            <View style={style.rightHeadingView}>
                                <Text style={[TextStyle.text_14_bold, style.heading1]}>{this.state.formData.heading1}</Text>
                                <Text style={[TextStyle.text_14_normal, style.heading2]}>{this.state.formData.heading2}</Text>
                            </View>
                        </View>
                        <View style={CommonStyle.separator}/>
                    </View>
                    <FlatList
                        extraData={this.state}
                        contentContainerStyle={style.listStyle}
                        data={this.state.formData.formItems}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({item, index}) => {
                            let background = (index % 2 === 0) ? colors.white : colors.soulitudeGrey;
                            if (item.actionType === "input" || item.actionType === "dateTimeInput") {
                                return <ChatbotFormTextCell
                                    data={item}
                                    index={index}
                                    backgroundColor={background}
                                    onEditPress={(data, index) => {
                                        if (data.actionType === "dateTimeInput") {
                                            this._showDateTimePicker(index, dateTimePickerType(data?.show_version_type));
                                        } else {
                                            this.props.navigation.navigate("EditChatbotFormText", {
                                                data: deepCopy(data),
                                                index: index,
                                                onSavePress: (data, index) => {
                                                    this._updateFormItem(data, index);
                                                }
                                            });
                                        }
                                    }}
                                />
                            } else if (item.actionType === 'singleSelect') {
                                return <ChatbotFormSingleSelectCell
                                    data={item}
                                    index={index}
                                    backgroundColor={background}
                                    onEditPress={(data, index) => {
                                        this.props.navigation.navigate("EditChatbotFormSingleSelect", {
                                            data: deepCopy(data),
                                            index: index,
                                            onSavePress: (data, index) => {
                                                this._updateFormItem(data, index);
                                            }
                                        });
                                    }}
                                />
                            }else if (item.actionType === 'multiSelect') {
                                return <ChatbotFormMultiSelectCell
                                    data={item}
                                    index={index}
                                    backgroundColor={background}
                                    onEditPress={(data, index) => {
                                        this.props.navigation.navigate("EditChatbotFormMultiSelect", {
                                            data: deepCopy(data),
                                            index: index,
                                            onSavePress: (data, index) => {
                                                this._updateFormItem(data, index);
                                            }
                                        });
                                    }}
                                />
                            } else {
                                return <View/>
                            }
                        }}
                    />
                    <View style={style.buttonView}>
                        {button0}
                        {button1}
                    </View>
                    <DateTimePicker
                        maximumDate={new Date()}
                        isVisible={this.state.isDateTimePickerVisible}
                        mode={this.state.datePickerType}
                        onConfirm={(date) => {
                            if(new Date()<date)
                            {
                                this.showErrorPopup(FUTURE_TIME_ERROR,TITLE);
                            }
                            else
                            {
                                var dateFormat = require('dateformat');
                                var dateTimeItem = this.state.formData.formItems[this.state.formItemIndexForDateTime];
                                let dateStringFormat = "dd-mmm-yyyy HH:MM:ss"
                                if (this.state.datePickerType == "date") {
                                    dateStringFormat = "dd-mmm-yyyy"
                                } else if (this.state.datePickerType == "time") {
                                    dateStringFormat = "HH:MM:ss"
                                }
                                dateTimeItem.response[0].value = dateFormat(date, dateStringFormat);
                                this._updateFormItem(dateTimeItem, this.state.formItemIndexForDateTime);

                            }
                            this._hideDateTimePicker();

                        }}
                        onCancel={() => {
                            this._hideDateTimePicker();
                        }}
                    />
                </View>
            </SafeAreaView>
        );

    }
    showErrorPopup(errorMessage,title)
    {
        if (this.DialogViewRef !== undefined)
            this.DialogViewRef.showDailog({
                title:title,
                message: errorMessage,
                primaryButton: {
                    text: "OKAY", onClick: () => {
                    }
                },
                cancelable: false,
                onClose: () => {
                }
            });
    }
}

const style = StyleSheet.create({
    SafeArea: {
        flex: 1,
        backgroundColor: colors.white
    },
    MainContainer: {
        backgroundColor: colors.white,
        flex: 1,
    },
    topView: {},
    headerView: {
        padding: spacing.spacing16,
        flexDirection: "row",
    },
    headerViewTitle: {
        letterSpacing: 1,
        marginTop: spacing.spacing4,
        marginBottom: spacing.spacing4,
        height: 16,
        fontSize: 10,
        fontFamily: "Lato",
        width: "90%",
        fontWeight: "bold",
        color: colors.grey,
    },
    minimiseImage: {
        marginRight: 0,
        height: 24,
        width: 24,
        marginLeft: 2,
    },
    subHeaderView: {
        flexDirection: "row",
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        marginBottom: spacing.spacing12,
    },
    assetImage: {
        height: 40,
        width: 40,
        resizeMode: "contain",
    },
    rightHeadingView: {
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing40,
    },
    heading2: {
        marginTop: spacing.spacing4,
    },
    listStyle: {
        justifyContent: "space-between"
    },
    buttonView: {
        backgroundColor: colors.white,
        justifyContent: "center",
        flexDirection: "row",
        height: 88,
        shadowOffset: {height: -4},
        shadowOpacity: 0.1,
        shadowRadius: 2,
        shadowColor: colors.black,
        elevation: 12,
        borderRadius:spacing.spacing4,
        borderColor:colors.lightGrey,
    },
    buttonTouchArea: {
        marginTop: spacing.spacing20,
        marginBottom: spacing.spacing20,
        marginLeft: spacing.spacing20,
        marginRight: spacing.spacing20,
        flex: 1,
    },
    formButton: {
        color: colors.blue,
        backgroundColor: colors.soulitudeGrey,
        paddingLeft: 32,
        paddingRight: 32,
        paddingTop: 14,
        paddingBottom: 14,
        borderRadius: 4,
        overflow: "hidden",
        fontSize: 14,
        fontFamily: "Lato",
        fontWeight: "600",
        textAlign: "center",
    },
});
