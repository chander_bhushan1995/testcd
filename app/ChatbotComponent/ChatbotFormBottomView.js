import React, {Component} from "react";
import {Image, NativeModules, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {BottomViewType} from "../Constants/BottomViewType";
import {TextStyle} from "../Constants/CommonStyle";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";

export default class ChatbotFormBottomView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            productIcon: require("../images/product.webp")
        }
    }

    componentDidMount() {
        if (this.props.data != null) {
            if (this.props.data.iconCode != null && this.props.formType === BottomViewType.ChatbotForm) {
                let chatBrigeRef = NativeModules.ChatBridge;
                chatBrigeRef.getImageUrl(this.props.data.iconCode.substring(this.props.data.iconCode.lastIndexOf("-") + 1), imageUrl => {
                    this.setState({productIcon: imageUrl != null ? {uri: imageUrl} : require("../images/product.webp")});
                });
            }
        }
    }

    render() {
        var iconView = null;
        var subHeadingView = null;
        if (this.props.data.iconCode != null && this.props.formType === BottomViewType.ChatbotForm) {
            iconView = <Image source={this.state.productIcon} style={style.assetImage} />;
        }
        if (this.props.data.heading2 != null) {
            subHeadingView = <Text style={[TextStyle.text_14_normal, style.heading2]}>{this.props.data.heading2}</Text>;
        }

        return (
            <View style={style.MainContainer}>
                <TouchableOpacity activeOpacity={1} onPress={this.props.handleChevornUpTap}>
                    <View style={style.headerView}>
                        <Text style={style.headerViewTitle}>{this.props.data.formLabel.toUpperCase()}</Text>
                        <Image source={require("../images/icon_chevornUp.webp")} style={style.minimiseImage} />
                    </View>
                </TouchableOpacity>
                <View style={style.subHeaderView}>
                    {iconView}
                    <View style={[style.rightHeadingView, (iconView == null) && { marginLeft: 0 }]}>
                        <Text style={[TextStyle.text_14_bold, (subHeadingView == null) && TextStyle.text_16_bold]}>{this.props.data.heading1}</Text>
                        {subHeadingView}
                    </View>
                </View>
            </View>
        );
    }
}

const style = StyleSheet.create({
    MainContainer: {
        backgroundColor: colors.white,
        width: "100%",
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        shadowOffset: {height: -4},
        shadowOpacity: 0.1,
        shadowRadius: 2,
        shadowColor: colors.black,
        elevation: 12,
        paddingBottom: spacing.spacing20,
    },
    headerView: {
        padding: spacing.spacing16,
        flexDirection: "row",
    },
    headerViewTitle: {
        letterSpacing: 1,
        marginTop: spacing.spacing4,
        marginBottom: spacing.spacing4,
        height: 16,
        fontSize: 12,
        fontFamily: "Lato",
        width: "90%",
        fontWeight: "bold",
        color: colors.grey,
    },
    minimiseImage: {
        marginRight: 0,
        height: 24,
        width: 24,
        marginLeft: 2,
    },
    subHeaderView: {
        flexDirection: "row",
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
    },
    assetImage: {
        height: 40,
        width: 40,
        resizeMode: "contain",
    },
    rightHeadingView: {
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing40,
    },
    heading2: {
        marginTop: spacing.spacing4,
    }
});
