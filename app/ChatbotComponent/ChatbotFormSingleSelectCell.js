import React, {Component} from "react";
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";

export default class ChatbotFormSingleSelectCell extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var editView;
        if (this.props.data.editable) {
            editView = <TouchableOpacity style={style.editArea} onPress={() => {
                this.props.onEditPress(this.props.data, this.props.index);
            }}>
                <Image source={require("../images/icon_edit.webp")} style={style.editImage}/>
                <Text style={style.editStyle}>{this?.props?.data?.editText ?? "Edit"}</Text>
            </TouchableOpacity>
        } else {
            editView = null
        }
        return (
            <View style={[style.MainContainer, {backgroundColor: this.props.backgroundColor}]}>
                <View style={style.headerView}>
                    <Text style={style.heading}>{this.props.data.question}</Text>
                    {editView}
                </View>
                <View style={style.contentView}>
                    <Text style={style.textStyle}>{this.props.data.response[0].value}</Text>
                </View>
            </View>
        );
    }
}
const style = StyleSheet.create({
    MainContainer: {
        paddingBottom: spacing.spacing16,
        flex: 1,
    },
    headerView: {
        flexDirection: "row",
        marginTop: spacing.spacing12,
        marginLeft: spacing.spacing24,
        marginRight: spacing.spacing16
    },
    heading: {
        paddingTop: spacing.spacing12,
        paddingBottom: spacing.spacing12,
        marginRight: spacing.spacing12,
        color: colors.charcoalGrey,
        fontSize: 16,
        fontFamily: "Lato",
        fontWeight: "bold",
        textAlignVertical: "center",
        width: "80%"
    },
    editArea: {
        flexDirection: "row",
        alignSelf: 'flex-end',
        padding: spacing.spacing16,
        height: 48,
        marginTop: -4,
    },
    editImage: {
        alignSelf: "flex-end",
        marginRight: spacing.spacing4,
        marginTop: 3,
        width: spacing.spacing12,
        height: spacing.spacing12,
    },
    editStyle: {
        alignSelf: "flex-end",
        color: colors.blue,
        fontSize: 12,
        fontFamily: "Lato",
        marginTop: spacing.spacing2,
    },
    contentView: {
        marginLeft: spacing.spacing24,
        marginRight: spacing.spacing24,
        marginTop: spacing.spacing8,
        marginBottom: spacing.spacing8,
    },
    textStyle: {
        fontSize: 14,
        fontFamily: "Lato",
        color: colors.charcoalGrey,
    }
});
