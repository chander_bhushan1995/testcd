import React, { Component } from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";

export default class ChatbotFormTextCell extends Component {
    state = {
        isExpanded: false
    };

    constructor(props) {
        super(props);
    }

    render() {
        let readMoreView = null;
        let numberOfLines = 0;
        if ((this.props.data.response[0].value?.length ?? 0) > 80) {
            if (this.state.isExpanded) {
                readMoreView = <Text style={style.readMoreLessText} onPress={() => this.setState({ isExpanded: false })}>... {this?.props?.data?.showLessText ?? "Show Less"}</Text>;
            } else {
                readMoreView = <Text style={style.readMoreLessText} onPress={() => this.setState({ isExpanded: true })}>... {this?.props?.data?.showMoreText ?? "Show More"} </Text>;
                numberOfLines = 3;
            }
        }

        let editView = this.props.data.editable ?
            <TouchableOpacity style={style.editArea} onPress={() => {
                this.props.onEditPress(this.props.data, this.props.index);
            }}>
                <Image source={require("../images/icon_edit.webp")} style={style.editImage} />
                <Text style={style.editStyle}>{this?.props?.data?.editText ?? "Edit"}</Text>
            </TouchableOpacity> : null;
        return (
            <View style={[style.MainContainer, { backgroundColor: this.props.backgroundColor }]}>
                <View style={style.headerView}>
                    <Text style={style.heading}>{this.props.data.question}</Text>
                    {editView}
                </View>
                <View style={style.textCell}>
                    <Text style={style.textCellValue} numberOfLines={numberOfLines}>{this.props.data.response[0].value}</Text>
                    {readMoreView}
                </View>
            </View>
        );
    }
}
const style = StyleSheet.create({
    MainContainer: {
        paddingBottom: spacing.spacing16,
        flex: 1,
    },
    headerView: {
        flexDirection: "row",
        marginTop: spacing.spacing12,
        marginLeft: spacing.spacing24,
        marginRight: spacing.spacing16,
    },
    heading: {
        paddingTop: spacing.spacing12,
        paddingBottom: spacing.spacing12,
        marginRight: spacing.spacing12,
        color: colors.charcoalGrey,
        fontSize: 16,
        fontFamily: "Lato",
        fontWeight: "bold",
        textAlignVertical: "center",
        width: "80%"
    },
    editArea: {
        flexDirection: "row",
        alignSelf: 'flex-end',
        padding: spacing.spacing16,
        height: 48,
        marginTop: -4,
    },
    editImage: {
        alignSelf: "flex-end",
        marginRight: spacing.spacing4,
        marginTop: 3,
        width: spacing.spacing12,
        height: spacing.spacing12,
    },
    editStyle: {
        alignSelf: "flex-end",
        color: colors.blue,
        fontSize: 12,
        fontFamily: "Lato",
        marginTop: spacing.spacing2,
    },
    textCell: {
        marginLeft: spacing.spacing24,
        marginRight: spacing.spacing24,
        marginTop: spacing.spacing8,
        marginBottom: spacing.spacing8,
    },
    textCellValue: {
        color: colors.charcoalGrey,
        fontSize: 14,
        fontFamily: "Lato",
    },
    textCellLabel: {
        marginTop: 8,
        color: colors.grey,
        fontSize: 12,
        fontFamily: "Lato",
    },
    readMoreLessText: {
        color: colors.blue,
        fontSize: 14,
        fontFamily: "Lato",
    }
});
