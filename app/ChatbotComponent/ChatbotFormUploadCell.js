import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import PlatformActivityIndicator from '../CustomComponent/PlatformActivityIndicator.js';
import * as DocumentUploadEvent from '../Constants/UploadDocumentEvent';
import * as DocumentType from '../Constants/DocumentTypeConstant';
import colors from '../Constants/colors';
import spacing from '../Constants/Spacing';
import {TextStyle} from '../Constants/CommonStyle';
import {
    DOWNLOAD_TEMPLATE, DOWNLOAD_TEMPLATE_TEXT,
    FMIP,
    FMIP_TEXT,
    MI_LOGOUT,
    MI_LOGOUT_TEXT,
} from '../Constants/FormUploadCellActionType';
import {OATextStyle} from "../Constants/commonstyles";

// let data =  {
//     heading1: 'Damaged Part Photo',
//     heading2: 'Please upload an image of damaged part',
// }

const ChatbotFormUploadCell = props => {
    let clickAction = props.data.clickAction;
    let docUri;

    if (props.data.documentUri != null && props.data.documentUri.length > 0) {
        if (DocumentType.DOCUMENT === props.data.documentType) {
            docUri = require('../images/icon_file.webp');
        }else{
            docUri = {uri: props.data.documentUri};
        }
    } else {
        docUri = require('../images/icon_upload.webp');
    }

    let heading1 = <Text style={[TextStyle.text_16_bold, style.heading1]}>{props.data.heading1}</Text> ;
    let heading2 = <Text style={[TextStyle.text_14_normal, style.heading2]}>{props.data.heading2}</Text>;
    let heading3 = null;
    if(clickAction !== "" && clickAction !==undefined){
        if(clickAction === FMIP){
            heading3 = FMIP_TEXT
        }else if(clickAction === MI_LOGOUT){
            heading3 = MI_LOGOUT_TEXT
        }else if(clickAction === DOWNLOAD_TEMPLATE){
            heading3 = DOWNLOAD_TEMPLATE_TEXT
        }
    }
    let heading3View = heading3 !== null ? <TouchableOpacity
            onPress={() => {
                props.onLinkClick(clickAction, props.data.url, props.data.fileName, props.index);
            }}>
            <Text style={[TextStyle.text_14_normal, style.heading3]}>{heading3}</Text>
        </TouchableOpacity> : null;
    let uploadStatusText = null;
    let uploadSuccessfullTick = null;
    let uploadSuccessfullEdit = null;
    let uploadTextLabel = <Text style={[OATextStyle.TextFontSize_10_bold_888F97, {color: colors.blue,textAlignVertical: "center",textAlign: "center"}]}>{props?.data?.uploadText ?? 'Upload'}</Text>;
    if (props.data.documentUploadEvent === DocumentUploadEvent.COMPLETE) {
        uploadStatusText = props?.data?.uploadCompletedText ?? 'Uploaded successfully';
        uploadSuccessfullTick = <Image style={style.imageRightTick} source={require('../images/green_right.webp')}/>;
        uploadSuccessfullEdit = <Image style={style.imageEdit} source={require('../images/edit.webp')}/>;
        uploadTextLabel=null;
    } else if (props.data.documentUploadEvent === DocumentUploadEvent.UPLOADING) {
        uploadStatusText = props?.data?.uploadInProcessText?.concat("...") ?? 'Uploading...';
        uploadTextLabel=null;
    }
    let uploadStatusTextView = uploadStatusText !== null ?
        <Text style={[TextStyle.text_14_normal, style.heading2UploadedSuccesfull]}>{uploadStatusText}</Text> : null;
    let activityIndicatorView = props.data.documentUploadEvent === DocumentUploadEvent.UPLOADING ?
        <View style={style.loaderContainer}>
            <PlatformActivityIndicator size={24}/>
        </View> : null;
    return (
        <View style={style.MainContainer}>
            <View style={style.headerView}>
                {heading1}
                {heading2}
                {uploadStatusTextView}
                {heading3View}
            </View>
            <View style={style.uploadLabel}>
            <View style={style.imageView}>
                <TouchableOpacity
                    disabled={props.data.documentUploadEvent === DocumentUploadEvent.UPLOADING}
                    onPress={() => {
                        props.onImageClick(props.index);
                    }}>
                    <Image style={style.image} source={docUri}/>
                    {uploadSuccessfullTick}
                    {uploadSuccessfullEdit}
                </TouchableOpacity>
                {activityIndicatorView}
            </View>
                {uploadTextLabel}
            </View>
        </View>
    );
};

const style = StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: colors.white,
        flexDirection: 'row',
        paddingTop: spacing.spacing12,
        paddingBottom: spacing.spacing12,
    },
    headerView: {
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        width: '72%',
        justifyContent: 'flex-start',
    },
    heading1: {
        marginTop: spacing.spacing4,
    },
    heading2: {
        marginTop: spacing.spacing4,
    },
    heading3: {
        marginTop: spacing.spacing4,
        color: colors.blue_2196f3,
    },
    heading2UploadedSuccesfull: {
        marginTop: spacing.spacing4,
        color: colors.seaGreen,
    },
    imageView: {
        marginRight: spacing.spacing20,
        height: 53,
        width: 53,
        borderRadius: 4,
        overflow: 'hidden',
        justifyContent: 'flex-end',
    },
    uploadLabel:
        {   width: 53,
            borderRadius: 4,
            overflow: 'hidden',
        },
    image: {
        width: 48,
        height: 48,
        borderRadius: 4,
        overflow: 'hidden',
    },
    imageRightTick: {
        width: 16,
        height: 16,
        overflow: 'hidden',
        position: 'absolute',
        left: 16,
        top: 16,
    },
    imageEdit: {
        width: 16,
        height: 16,
        overflow: 'hidden',
        position: 'absolute',
        left: 37,
        top: -5,
    },
    loaderContainer: {
        backgroundColor: 'rgba(0, 0, 0, 0.54)',
        position: 'absolute',
        width: 48,
        height: 48,
        borderRadius: 4,
        justifyContent: 'center',
    },
});

export default ChatbotFormUploadCell;
