import React, {useState} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import colors from '../Constants/colors';
import spacing from '../Constants/Spacing';
import {TextStyle} from '../Constants/CommonStyle';

const ChatbotHorizontalImageText = props => {
    let imageView = <Image source={{uri: props.item.image}} style={style.imageStyle}/>;
    let textViews = <View style={style.textContainerStyle}>
        <Text
            style={[TextStyle.text_12_normal, style.textStyleHeading1]}>{props.item.heading1}</Text>
        <Text
            style={TextStyle.text_16_normal}>{props.item.heading2}</Text>
    </View>;
    let finalView = props.index % 2 === 0 ? <View style={style.mainContainer}>{imageView}{textViews}</View> :
        <View style={style.mainContainer}>{textViews}{imageView}</View>;
    return (
        <View>
            {finalView}
        </View>
    );
};

const style = StyleSheet.create({
    mainContainer: {
        flexDirection: 'row',
        flex: 1,
        marginLeft: spacing.spacing24,
        marginRight: spacing.spacing24,
    },
    imageStyle: {
        flex: 1,
        height: 185,
        resizeMode: 'contain',
    },
    textContainerStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        marginLeft: spacing.spacing4,
    },
    textStyleHeading1: {
        color: colors.darkGrey,
    },
});

export default ChatbotHorizontalImageText;
