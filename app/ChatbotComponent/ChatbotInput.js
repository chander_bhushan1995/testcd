import React, {Component} from 'react';
import {Image, StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native';
import colors from '../Constants/colors';
import spacing from '../Constants/Spacing';
import {removeSpecialCharacters} from "../commonUtil/AppUtils";
import * as Actions from '../Constants/ActionTypes';

// let data = {
//     show: true,
//     minLength: 123,
//     maxLength: 342,
//     placeHolder: 'Enter incident description',
// }
let handleAutoSuggestion;
let _this;
const DEFAULT_INPUT_TEXT_HEIGHT = 40;
const CONSTRAINT_LAYOUT_HEIGHT = 36;
const INPUTBOX_PADDING = 20;

export default class ChatbotInput extends Component {
    constructor(props) {
        super(props);
        props.updateAutoSuggestions(props.data?.inputType === 'autoSuggestion');
        this.state = {
            height: DEFAULT_INPUT_TEXT_HEIGHT,
            inputMessage: '',
            submitEnabled: false,
        };
        _this = this;
    }

    updateSize = height => {
        //add constraint layout height also in bottomview margin from autosuggestion
        let bottomContainerHeight = 0;
        if (_this.props.data.minLength > 3) {
            bottomContainerHeight = height + CONSTRAINT_LAYOUT_HEIGHT;
        }else{
            bottomContainerHeight = height;
        }
        if (height < 135) {
            this.setState({
                height,
            });
            if (this.props.autoSuggestionEnabled) {
                this.props.updateBottomMarginAutoSuggestionView(INPUTBOX_PADDING + bottomContainerHeight);
            }
        } else {
            this.setState({
                height: 135,
            });
        }
    };


    static getDerivedStateFromProps(props, state) {
        const newState = {...state};
        if (props.shouldClearInput) {
            if (this.textInput !== null && this.textInput !== undefined) {
                this.textInput.clear();
            }
            newState.inputMessage = '';
            return newState;
        } else {
            return state;
        }
    }

    componentDidMount() {
        // let delayAfterPauseTypingTime=this.props.delayAfterPauseTypingTime !==null && this.props.delayAfterPauseTypingTime!==undefined?this.props.delayAfterPauseTypingTime:DEFAULT_PAUSE_TIME_FOR_SUGGESTIONS;
        let delayAfterPauseTypingTime = 0;
        handleAutoSuggestion = this.debounce(function (text) {
            _this.props.getAutosuggestions(text, _this.props.data?.inputData);
        }, delayAfterPauseTypingTime);
    }


    debounce(func, wait, immediate) {
        let timeout;
        return function () {
            let context = this, args = arguments;
            let later = function () {
                timeout = null;
                if (!immediate) {
                    func.apply(context, args);
                }
            };
            let callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) {
                func.apply(context, args);
            }
        };
    };

    sendChatMessage(msg) {
        if (msg !== '') {
            msg = removeSpecialCharacters(msg);
            this.props.sendMessage(JSON.stringify({
                'text': msg.trim(),
                'params': '',
            }), msg.trim(), Actions.SEND_MESSAGE);
            _this.resetTextInput();
        }
    }

    resetTextInput(){
        this.props.updateBottomView();
        this.textInput.clear();
        this.setState({inputMessage: '', submitEnabled: false});
        this.props.autoSuggestionVisible(false);
    }

    render() {
        const {height} = this.state;
        var checkIcon = this.state.submitEnabled ? require('../images/roundCheckEnable.webp')
            : require('../images/roundCheckDisable.webp');
        var submitIcon = this.state.submitEnabled ? require('../images/icon_send.webp')
            : require('../images/icon_send_disabled.webp');
        var constraintBox = this.props.data.minLength > 3 ? <View style={styles.constraintContainer}>
            <Image style={styles.checkIcon} source={checkIcon}/>
            <Text style={styles.constraintText}>{this.props?.data?.thresholdText ?? "Should be atleast ".concat(this.props.data.minLength).concat(" characters")} </Text>
            <Text
                style={styles.characterText}>{this.state.inputMessage.length}/{this.props.data.maxLength}</Text>
        </View> : null;
        if (_this.props.selectedSuggestionData !== null && _this.props.selectedSuggestionData !== undefined) {
            let msg = _this.props.selectedSuggestionData.text;
            if(_this.props.selectedSuggestionData.intent_id === null || _this.props.selectedSuggestionData.intent_id === undefined){
                _this.sendChatMessage(msg);
            }else{
                if(msg !== ''){
                    _this.props.sendMessage(JSON.stringify({
                        'text': msg.trim(),
                        'intent_id': _this.props.selectedSuggestionData.intent_id,
                        'params': '',
                    }), msg.trim(), Actions.SEND_MESSAGE);
                    _this.resetTextInput();
                }
            }
        }
        return (
            <View style={styles.MainContainer}>
                {constraintBox}
                <View style={styles.sendContainer}>
                    <TextInput
                        ref={input => {
                            this.textInput = input;
                        }}
                        underlineColorAndroid="transparent"
                        onChangeText={inputMessage => {
                            this.setState({
                                inputMessage: inputMessage,
                                submitEnabled: inputMessage.length >= this.props.data.minLength,
                            });
                            this.props.autoSuggestionVisible(inputMessage !== '');
                            if (this.props.autoSuggestionEnabled && inputMessage !== '') {
                                handleAutoSuggestion(removeSpecialCharacters(inputMessage));
                            }
                        }}
                        style={[styles.textInputContainer, {height: height}]}
                        editable={true}
                        height={this.state.height}
                        multiline={true}
                        autoFocus={true}
                        value={this.state.inputMessage}
                        maxLength={this.props.data.maxLength}
                        numberOfLines={6}
                        placeholder={this.props.data.placeHolder}
                        placeholderTextColor={colors.lightGrey}
                        onContentSizeChange={e => {
                            this.updateSize(e.nativeEvent.contentSize.height);
                        }
                        }
                    />
                    <TouchableOpacity
                        disabled={!this.state.submitEnabled}
                        onPress={() => {
                            _this.sendChatMessage(this.state.inputMessage);
                        }}
                    >
                        <Image source={submitIcon} style={styles.iconContainer}/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    MainContainer: {
        alignItems: 'center',
        backgroundColor: colors.white,
        width: '100%',
        shadowOffset: {height: -4},
        shadowOpacity: 0.1,
        shadowRadius: 2,
        shadowColor: colors.black,
        elevation: 12,
    },
    sendContainer: {
        flexDirection: 'row',
        padding: spacing.spacing10,
        backgroundColor: colors.white,
    },
    textInputContainer: {
        flex: 1,
        justifyContent: 'space-between',
        minHeight: spacing.spacing24,
        fontFamily: 'Lato',
    },
    iconContainer: {
        marginLeft: spacing.spacing16,
        height: spacing.spacing24,
        width: spacing.spacing24,
        marginTop: spacing.spacing8,
        justifyContent: 'flex-end',
        resizeMode: 'contain',
    },
    constraintContainer: {
        backgroundColor: colors.soulitudeGrey,
        width: '100%',
        flexDirection: 'row',
        height: CONSTRAINT_LAYOUT_HEIGHT,
        paddingTop: spacing.spacing8,
        paddingBottom: spacing.spacing8,
        paddingLeft: spacing.spacing20,
        paddingRight: spacing.spacing20,
    },
    checkIcon: {
        width: 18,
        height: 18,
    },
    constraintText: {
        fontSize: 12,
        fontFamily: 'Lato',
        height: 18,
        lineHeight: 18,
        fontWeight: '500',
        marginLeft: spacing.spacing4,
        width: '80%',
    },
    characterText: {
        fontSize: 12,
        fontFamily: "Lato",
        height: 18,
        lineHeight: 19,
        marginRight: 0,
    }
});
