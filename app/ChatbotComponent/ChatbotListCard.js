import React, { Component } from "react";
import {StyleSheet, Text, View} from 'react-native';
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";
import { TextStyle } from "../Constants/CommonStyle";

const ChatbotListCard = props => {
    return (
        <View style={style.MainContainer}>
            <Text style={[style.text, TextStyle.text_14_semibold]}>{props.data.text}</Text>
            {props.data.listPoints.map(point =>
                <View style={style.listView}>
                    <Text style={TextStyle.text_12_medium}>-</Text>
                    <Text style={[style.listText, TextStyle.text_12_medium]}>{point}</Text>
                </View>
            )}
        </View>
    );
};

const style = StyleSheet.create({
    MainContainer: {
        backgroundColor: colors.white,
        borderRadius: spacing.spacing4,
        overflow: "hidden",
        paddingBottom: spacing.spacing12
    },
    text: {
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing12,
        marginTop: spacing.spacing12,
        marginBottom: spacing.spacing4
    },
    listView: {
        flexDirection: "row",
        marginTop: spacing.spacing4,
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing12
    },
    listText: {
        marginLeft: spacing.spacing4,
    },
});

export default ChatbotListCard;
