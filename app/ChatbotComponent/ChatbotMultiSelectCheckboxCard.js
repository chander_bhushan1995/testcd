import React , {useEffect, useContext } from 'react';
import {ChatContext} from "../Components/ChatComponent";
import {FlatList, StyleSheet, View} from 'react-native';
import colors from '../Constants/colors';
import spacing from '../Constants/Spacing';
import {CommonStyle} from '../Constants/CommonStyle';
import ChatbotCheckboxWithText from './ChatbotCheckboxWithText';
import * as Actions from "../Constants/ActionTypes";

// const data = {
//     "listPoints": [
//         {
//             "text": "Front Camera",
//             "type": "Checkbox"
//         },
//         {
//             "text": "Back Camera",
//             "type": "Checkbox"
//         },
//         {
//             "text": "Speaker",
//             "type": "Checkbox"
//         },
//         {
//             "text": "Others",
//             "type": "Others"
//         }
//     ],
//     "params": {
//         "param1": "param1"
//     }
// };

let listPoints = [

];

const ChatbotMultiSelectCheckboxCard = props => {
    const {dispatch} = useContext(ChatContext);
    function onClick(clickedData) {
        if(clickedData.type === 'Others'){
            dispatch({ type: Actions.TAGS_INPUT_OTHERS, otherTagData: {isOthersOptionSelected: true, params: props.data.params}})
        }else{
            if(clickedData.isSelected){
                listPoints.push(clickedData)
                listPoints = listPoints.filter(data => (data.isSelected === true));
            }else{
                listPoints = listPoints.filter(data => (data.text !== clickedData.text));
            }
            dispatch({ type: Actions.TAGS_INPUT, selectedTagData: {selectedTags: getTagsData(listPoints), params: props.data.params}})
        }
    }

    /**
     * reset listpoints on componentwillunmount
     */
    useEffect(() => {
        return () => {
            listPoints = [];
        };
    }, []);

    //get tags list in which only tags text is there not extra info
    function getTagsData(listPoints) {
        let tagsList = [];
        for (let i = 0; i < listPoints.length; i++){
            tagsList.push(listPoints[i].text);
        }
        return tagsList;
    }

    const renderCheckboxItem = ({ item, index }) => {
        return (
            <View>
                <ChatbotCheckboxWithText text={item.text} type={item.type} onClick={onClick} />
            </View>
        );
    };
    return (
        <View style={style.MainContainer}>
            <FlatList
                data={props.data.listPoints}
                keyExtractor={(item, index) => index.toString()}
                renderItem={renderCheckboxItem}
                ItemSeparatorComponent={() => {
                    return <View style={CommonStyle.separator} />;
                }}
            />
        </View>
    );
};

const style = StyleSheet.create({
    MainContainer: {
        backgroundColor: colors.white,
        borderRadius: spacing.spacing4,
        overflow: 'hidden',
    },
});

export default ChatbotMultiSelectCheckboxCard;
