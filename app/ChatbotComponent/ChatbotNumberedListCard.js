import React from "react";
import { StyleSheet, Text, View } from "react-native";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";
import { TextStyle } from "../Constants/CommonStyle";

// const data = {
//     heading: 'Please Note: ',
//     listPoints: [
//         "You have these on-going service requests in your account.",
//         "You have these on-going service requests in your account.",
//         "You have these on-going service requests in your account.",
//         "You have these on-going service requests in your account."
//     ],
// };

const ChatbotNumberedListCard = props => {
    return (
        <View style={style.MainContainer}>
            <Text style={[TextStyle.text_14_bold]}>{props.data.heading}</Text>
            {props.data.listPoints.map((point,key)=>
                <View key={key} style={style.listView}>
                    <Text style={[style.listText, TextStyle.text_12_medium]}>{(key+1)+ ". "} {point}</Text>
                </View>
            )}
        </View>
    );
}

const style = StyleSheet.create({
    MainContainer: {
        backgroundColor: colors.white,
        borderRadius: spacing.spacing4,
        overflow: "hidden",
        paddingBottom: spacing.spacing12,
        paddingLeft: spacing.spacing16,
        paddingRight: spacing.spacing32,
        paddingTop: spacing.spacing12,
    },
    listView: {
        flexDirection: "row",
        marginTop: spacing.spacing4,
    },
    listText: {
        color: colors.darkGrey,
        marginBottom: spacing.spacing8,
    },
});

export default ChatbotNumberedListCard;
