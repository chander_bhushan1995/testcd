import React, { useState, useEffect } from "react";
import { Image, NativeModules, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";
import { TextStyle, ButtonStyle, CommonStyle, margin } from "../Constants/CommonStyle";
import images from "../images/index.image";


// var data = {
//     "id": "123000123000",
//     "value": {
//         "text": "Accidental and Liquid Damage Protection Plan for Mobile",
//         "heading1": "Lenovo E480",
//         "heading2": "IMEI No. - 123000123000",
//         "heading3": "Valid till Jan 12, 2021",
//         "iconCode": "PE-MP01"
//     },
//     "params": {
//         "serialNo": "123000123000",
//         "membershipId": "1002038611",
//         "modelName": "E480",
//         "brand": "Lenovo",
//         "prodCode": "MP01",
//         "serviceName": "ADLD"
//     },
//     "buttons_inline": [
//         {
//             "id": "1",
//             "value": {
//                 "text": "Raise a new Claim"
//             }
//         },
//         {
//             "id": "2",
//             "value": {
//                 "text": "Resume"
//             }
//         }
//     ]
// }
const chatBrigeRef = NativeModules.ChatBridge;

const ChatbotPlanCard = props => {
    const { data, onClick } = props
    const { value, buttons_inline = [] } = data;
    const { text, heading1, heading2, heading3, iconCode } = value
    const [imageSource, setImageSource] = useState(images.memSmallBlue);

    useEffect(() => {
        chatBrigeRef.getImageUrl(iconCode.substring(iconCode.lastIndexOf("-") + 1), imageUrl => {
            setImageSource(imageUrl ? { uri: imageUrl } : images.memSmallBlue);
        });
    }, [])

    let buttonsView = null;
    if (buttons_inline?.length) {
        let buttonsContainerView = null;
        let buttons = buttons_inline;
        if (buttons.length === 1) {
            buttonsContainerView = <View style={style.singleButtonView}>
                <TouchableOpacity onPress={() => onClick(buttons[0].value.text, buttons[0].params, null)}>
                    <Text style={[style.singleButton, ButtonStyle.TextOnlyButton]}>{buttons[0].value.text}</Text>
                </TouchableOpacity>
            </View>;
        } else if (buttons.length === 2) {
            buttonsContainerView = <View style={style.doubleButtonView}>
                <TouchableOpacity style={{ width: "50%" }}
                    onPress={() => onClick(buttons[0].value.text, buttons[0].params, null)}>
                    <Text style={[style.doubleLeftButton, ButtonStyle.TextOnlyButton]}>{buttons[0].value.text}</Text>
                </TouchableOpacity>
                <View style={style.buttonSeparator} />
                <TouchableOpacity style={{ width: "50%", backgroundColor: colors.blue }}
                    onPress={() => onClick(buttons[1].value.text, buttons[1].params, null)}>
                    <Text style={[TextStyle.text_14_normal, style.doubleRightButton]}>{buttons[1].value.text}</Text>
                </TouchableOpacity>
            </View>;
        }
        buttonsView = <View>
            <View style={CommonStyle.separator} />
            {buttonsContainerView}
        </View>;
    }

    return (
        <View style={style.MainContainer}>
            <View style={style.topView}>
                <Image source={imageSource} style={style.assetImage} />
                <View style={style.headingView}>
                    <Text style={[style.text, TextStyle.text_14_bold]}>{text}</Text>
                    {heading1 ? <Text style={[style.heading1, TextStyle.text_14_semibold]}>{heading1}</Text> : null}
                    {heading2 ? <Text style={[style.heading2, TextStyle.text_12_normal]}>{heading2}</Text> : null}
                    {heading3 ? <Text style={[style.heading3, TextStyle.text_12_normal]}>{heading3}</Text> : null}
                </View>
            </View>
            {buttonsView}
        </View>
    );
};

const style = StyleSheet.create({
    MainContainer: {
        backgroundColor: colors.white,
        borderRadius: spacing.spacing4,
        overflow: "hidden",
    },
    topView: {
        flexDirection: "row",
        marginBottom: spacing.spacing16,
    },
    bottomView: {
        alignItems: "flex-end",
    },
    headingView: {
        flex: 1,
        marginTop: spacing.spacing20,
        marginRight: spacing.spacing16,
    },
    text: {
        alignSelf: "flex-start",
        textAlign: "left",
    },
    heading1: {
        marginTop: spacing.spacing8,
        textAlign: "left",
        color: colors.charcoalGrey,
    },
    heading2: {
        marginTop: spacing.spacing2,
        textAlign: "left",
    },
    heading3: {
        marginTop: spacing.spacing2,
        textAlign: "left",
    },
    assetImage: {
        width: spacing.spacing32,
        height: spacing.spacing32,
        marginLeft: spacing.spacing20,
        marginRight: spacing.spacing12,
        marginTop: spacing.spacing24,
        resizeMode: "contain",
    },
    claimRaiseButton: {
        marginBottom: spacing.spacing16,
        marginTop: spacing.spacing16,
        marginRight: spacing.spacing16
    },
    singleButtonView: {
        flexDirection: "row",
        alignSelf: "center",
    },
    doubleButtonView: {
        flexDirection: "row",
        alignSelf: "center",
    },
    singleButton: {
        margin: spacing.spacing12,
    },
    doubleLeftButton: {
        paddingBottom: spacing.spacing12,
        paddingTop: spacing.spacing12,
        paddingRight: spacing.spacing4,
        paddingLeft: spacing.spacing4,
        color: colors.blue,
    },
    doubleRightButton: {
        paddingBottom: spacing.spacing12,
        paddingTop: spacing.spacing12,
        paddingRight: spacing.spacing4,
        paddingLeft: spacing.spacing4,
        color: colors.white,
        textAlign: "center",
        borderBottomRightRadius: 4,
    },
});

export default ChatbotPlanCard;
