import ChatbotText from "./ChatbotText";
import ChatbotPlanCard from "./ChatbotPlanCard";
import ChatbotSimpleCard from "./ChatbotSimpleCard";
import ChatbotSimpleCardHorizontal from "./ChatbotSimpleCardHorizontal";
import ChatbotInput from "./ChatbotInput";
import ChatbotForm from "./ChatbotForm";
import ChatbotCard from "./ChatbotCard";
import ChatbotStatusCard from "./ChatbotStatusCard";
import ChatbotUploadDocumentForm from "./ChatbotUploadDocumentForm";
import ChatbotBuyPlanCard from "./ChatbotBuyPlanCard";
import ChatbotListCard from "./ChatbotListCard";
import ChatbotNumberedListCard from './ChatbotNumberedListCard';
import ChatbotDateTimeCard from './ChatbotDateTimeCard';
import ChatbotMultiSelectCheckboxCard from './ChatbotMultiSelectCheckboxCard';
import ChatbotTagInput from './ChatbotTagInput';
import DepartmentList from "../Components/DepartmentList";
import {logChatWebEngageEvent} from '../commonUtil/WebengageTrackingUtils';
import {ENABLE_CHAT_BOT_INPUT} from '../Constants/WebengageEvents';
import { logChatEvents } from "../Chat/ChatAPIHelper";
import { chatManager } from "../Navigation/index";
import { parseJSON, deepCopy} from "../commonUtil/AppUtils";
import { dateTimePickerType } from "../Chat/ChatUtils";

export default class ChatbotResponseParser {

    chatbotDataFromResponse(response) {
        let Components = [];
        var extraView = {bottomView: null, ratingView: null};
        let chatResponse = parseJSON(response);
        console.log("chatbotDataFromResponse", JSON.stringify(chatResponse, null, 4));
        if (chatResponse === null || chatResponse === undefined || chatResponse.chatResponse === null || chatResponse.chatResponse === undefined) {
            return {list: [], bottomView: null, ratingView: null};
        }
        chatResponse.chatResponse.map(item => {
            let view = this.extractComponent(item, Components);
            let bottomView = view.bottomView;
            let ratingView = view.ratingView;
            if (bottomView != null) {
                extraView.bottomView = bottomView
            }
            if (ratingView != null) {
                extraView.ratingView = ratingView
            }
        });
        return {list: Components, bottomView: extraView.bottomView, ratingView: extraView.ratingView};
    }

    extractComponent(item, Components) {
        let bottomView = null;
        let ratingView = null;
        let cardType = item?.data?.cardType;
        let actionType = item?.actionType;
        //save preferred language type in chat manager
        if(item?.actionType === 'language' && item?.data?.preferredLanguage && item?.data?.preferredLanguage.length>0){
            chatManager.setPreferredLanguage(item?.data?.preferredLanguage);
            chatManager.setSentAtText(item?.data?.sentAtText);
        }
        if (actionType === "text" && cardType === "text") {
            item.data.options.map(option => {
                Components.push({
                    ComponantName: ChatbotText,
                    componentData: option,
                    actionType: actionType
                })
            });
        } else if (
            (actionType === "singleSelect" || actionType === "multiSelect") &&
            cardType === "simpleCard") {
            if (item.data?.showHorizontal ?? false) {
                Components.push({
                    ComponantName: ChatbotSimpleCardHorizontal,
                    componentData: item.data.options,
                    actionType: actionType
                })
            } else {
                item.data.options.map(option => {
                    Components.push({
                        ComponantName: ChatbotSimpleCard,
                        componentData: option,
                        actionType: actionType
                    })
                });
            }
        } else if (
            (actionType === "singleSelect" || actionType === "multiSelect") &&
            cardType === "planCard") {
            item.data.options.map(option => {
                Components.push({
                    ComponantName: ChatbotPlanCard,
                    componentData: option,
                    actionType: actionType
                })
            });
        } else if (actionType === "singleSelect" && cardType === "statusCard") {
            item.data.options.map(option => {
                Components.push({
                    ComponantName: ChatbotStatusCard,
                    componentData: option,
                    actionType: actionType
                })
            });
        }
        else if (actionType === "singleSelect" && cardType === "questionsCard") {
            item.data.options.map(option => {
                Components.push({
                    ComponantName: DepartmentList,
                    componentData: option,
                    actionType: actionType
                })
            });
        }
        else if (
            (actionType === "singleSelect" || actionType === "multiSelect") &&
            cardType === "planCardPE") {
            item.data.options.map(option => {
                Components.push({
                    ComponantName: ChatbotBuyPlanCard,
                    componentData: option,
                    actionType: actionType
                })
            });
        }else if (
            (actionType === "multiSelect") && item.cardType === "checkboxCard") {
            item.data.options.map(option => {
                Components.push({
                    ComponantName: ChatbotMultiSelectCheckboxCard,
                    componentData: option,
                    actionType: actionType,
                })
            });
        } else if (actionType === "card") {
            if (cardType === "listCard") {
                item.data.options.map(option => {
                    Components.push({
                        ComponantName: ChatbotListCard,
                        componentData: option,
                        actionType: actionType
                    })
                });
            }else if(cardType === "numberListCard"){
                item.data.options.map(option => {
                    Components.push({
                        ComponantName: ChatbotNumberedListCard,
                        componentData: option,
                        actionType: actionType
                    })
                });
            } else {
                item.data.options.map(option => {
                    Components.push({
                        ComponantName: ChatbotCard,
                        componentData: option,
                        actionType: actionType
                    })
                });
            }
        }else if(actionType === "dateTimeInput" && cardType === "text"){
            Components.push({
                ComponantName: ChatbotDateTimeCard,
                componentData: { ...item.data, dateTimePickerType: dateTimePickerType(item?.show_version_type) },
                actionType: actionType
            })
        } else if (actionType === "input" || actionType === "dateTimeInput") {
            if(actionType === "input"){
                logChatWebEngageEvent(logChatEvents, ENABLE_CHAT_BOT_INPUT);
            }
            bottomView = {
                ComponantName: ChatbotInput,
                componentData: item.data,
                actionType: actionType
            }
        }else if (actionType === "tagInput") {
            bottomView = {
                ComponantName: ChatbotTagInput,
                componentData: item.data,
                actionType: actionType
            }
        } else if (actionType === "formData") {
            bottomView = {
                ComponantName: ChatbotForm,
                componentData: item.data,
                actionType: actionType
            }
        } else if (actionType === "uploadDoc") {
            bottomView = {
                ComponantName: ChatbotUploadDocumentForm,
                componentData: item.data,
                actionType: actionType
            }
        } else if (actionType === "rating") {
            ratingView = item.data
            let startNewChat={showStartChatBtn:item.data?.showStartChatBtn,startChatBtnLabel:item.data?.startChatBtnLabel}
            chatManager.setRatingResponse(startNewChat);
        }
        return {bottomView: bottomView, ratingView: ratingView}
    }
}
