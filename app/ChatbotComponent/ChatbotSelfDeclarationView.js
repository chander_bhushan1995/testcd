import React, {useState} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import colors from '../Constants/colors';
import spacing from '../Constants/Spacing';
import {TextStyle} from '../Constants/CommonStyle';
import CheckBox from 'react-native-check-box';

const ChatbotSelfDeclarationView = props => {
    const [value, setValue] = useState(false);
    return (
        <View style={style.mainContainer}>
            <Text
                style={TextStyle.text_16_bold}>{props.data.heading1}</Text>
            <View style={style.checkboxTextContainer}>
                <CheckBox
                    style={style.checkboxStyle}
                    isChecked={value}
                    checkedImage={<Image source={require('../images/ic_check_box.webp')} style={style.checkbox}/>}
                    unCheckedImage={<Image source={require('../images/ic_check_box_outline_blank.webp')}
                                           style={style.checkbox}/>}
                    onClick={() => {
                        setValue(prevState => {
                            const newState = !prevState;
                            props.updateItemState(props.index, newState);
                            return newState;
                        });
                    }
                    }
                />
                <Text
                    style={[TextStyle.text_16_semibold, style.descriptionStyle]}>{props.data.heading2}</Text>
            </View>
        </View>
    );
};

const style = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colors.white,
        paddingLeft: spacing.spacing20,
        paddingRight: spacing.spacing28,
        paddingTop: spacing.spacing12,
        paddingBottom: spacing.spacing12,
    },
    checkboxTextContainer: {
        flexDirection: 'row',
        marginTop: spacing.spacing12,
    },
    checkboxStyle: {
        width: 18,
        height: 18,
        marginRight: spacing.spacing4,
        marginTop: spacing.spacing4,
    },
    checkbox: {
        height: 16,
        width: 16,
    },
    descriptionStyle: {
        color: colors.black,
        marginLeft: spacing.spacing12,
    },
});

export default ChatbotSelfDeclarationView;
