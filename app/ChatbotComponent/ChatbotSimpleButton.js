import {StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {TextStyle} from "../Constants/CommonStyle";
import React from "react";
import spacing from "../Constants/Spacing";
import colors from "../Constants/colors";

const ChatbotSimpleButton = props =>
{
    const {onClick}=props;
    const{text}=props

    return (<TouchableOpacity onPress={() => {
        onClick();
    }}>
        <View style={style.ButtonContainer}>
            <Text style={[TextStyle.text_14_bold, style.text]}>{text}</Text>
        </View>
    </TouchableOpacity>)
}

const style = StyleSheet.create({

    ButtonContainer: {
        alignSelf:'center',
        borderRadius: spacing.spacing36,
        backgroundColor: colors.white,
        borderWidth: spacing.spacing1,
        marginLeft: spacing.spacing12,
        borderColor: colors.blue,
        marginBottom:36,
        overflow: "hidden"
    },
    text: {
        color: colors.blue,
        alignContent: "center",
        marginVertical: spacing.spacing8,
        marginHorizontal: spacing.spacing16,
    },
});

export default ChatbotSimpleButton;