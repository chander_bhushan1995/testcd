import React, {Component} from "react";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";
import {TextStyle} from "../Constants/CommonStyle";

// let data = {
//     id: 'PE_ADLD',
//     value: {
//         text: 'There is an accidental damage',
//     },
//     params: {
//         claimType: 'PE_ADLD',
//     },
// }

export default class ChatbotSimpleCard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={style.MainContainer}>
                <TouchableOpacity onPress={() => {
                    if (this.props.data?.deepLinkUrl?.length) {
                        this.props.onOpenWebView(this.props.data);
                    } else {
                        this.props.onClick(this.props.data.value.text, this.props.data.params, null);
                    }
                }}>
                    <View style={style.ButtonContainer}>
                        <Text style={[TextStyle.text_14_bold,style.text]}>{this.props.data.value.text}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}
const style = StyleSheet.create({
    MainContainer: {
        alignSelf: "flex-end",
        marginTop: spacing.spacing8,
    },
    ButtonContainer: {
        borderRadius: spacing.spacing36,
        backgroundColor: colors.white,
        borderWidth: spacing.spacing1,
        borderColor: colors.blue,
        overflow: "hidden"
    },
    text: {
        color: colors.blue,
        alignContent: "center",
        marginVertical: spacing.spacing8,
        marginHorizontal: spacing.spacing16,
    },
});
