import React, { Component } from "react";
import { StyleSheet, Text, TouchableOpacity, View, ScrollView, Dimensions } from "react-native";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";
import { TextStyle } from "../Constants/CommonStyle";

// let data = [{
//     id: 'PE_ADLD',
//     value: {
//         text: 'There is an accidental damage',
//     },
//     params: {
//         claimType: 'PE_ADLD',
//     },
// },
// {
//     id: 'PE_ADLD',
//     value: {
//         text: 'There is an accidental damage',
//     },
//     params: {
//         claimType: 'PE_ADLD',
//     },
// }]

const screenWidth = Dimensions.get("screen").width
export default class ChatbotSimpleCardHorizontal extends Component {

    state = {
        scrollEnabled: true
    }

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ScrollView ref={ref => this.ScrollRef = ref} scrollEnabled={this.state.scrollEnabled} style={style.MainContainer} horizontal={true} showsHorizontalScrollIndicator={false} onLayout={() => {
                this.ScrollRef?.scrollToEnd({ animated: false })
            }} onContentSizeChange={(contentWidth, H) => {
                this.setState({ scrollEnabled: (contentWidth > (screenWidth - 32)) })
            }}>
                <View style={{width:16}}/>
                {this.props.data.map((item, index) => {
                    return (<TouchableOpacity onPress={() => {
                        if (item?.deepLinkUrl?.length) {
                            this.props.onOpenWebView(item);
                        } else {
                            this.props.onClick(item.value.text, item.params, null);
                        }
                    }}>
                        <View style={[style.ButtonContainer, (index == 0) && { marginLeft: 0 }]}>
                            <Text style={[TextStyle.text_14_bold, style.text]}>{item.value.text}</Text>
                        </View>
                    </TouchableOpacity>)
                })}
                <View style={{width:16}}/>
            </ScrollView>
        );
    }
}
const style = StyleSheet.create({
    MainContainer: {
        flex: 1,
        flexDirection: "row",
        alignSelf: "flex-end",
        marginTop: spacing.spacing8,
        marginLeft: -72,
        marginRight: -16
    },
    ButtonContainer: {
        borderRadius: spacing.spacing36,
        backgroundColor: colors.white,
        borderWidth: spacing.spacing1,
        marginLeft: spacing.spacing12,
        borderColor: colors.blue,
        overflow: "hidden"
    },
    text: {
        color: colors.blue,
        alignContent: "center",
        marginVertical: spacing.spacing8,
        marginHorizontal: spacing.spacing16,
    },
});
