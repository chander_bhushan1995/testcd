import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image, Linking} from 'react-native';
import colors from '../Constants/colors';
import spacing from '../Constants/Spacing';
import {TextStyle, CommonStyle, ButtonStyle} from '../Constants/CommonStyle';
import RejectedDocumentList from './RejectedDocumentList';

// let data1 = {
//     'heading': 'Service Request No.  2436167',
//     'heading1': 'Raised for E480 Accidental and Liquid Damage Protection Plan for Mobile  on 17-Jul-2019 16:17:56',
//     'heading2': 'Document Upload Stage.',
//     'heading3': 'Please upload mandatory documents to proceed with the service request.',
//     "heading4": "Expected closure by today.",
//     'heading4': {
//         'title': 'Please find the list of Rejected Documents -',
//         'reasons': [
//             {
//                 'heading1': 'Please upload an image of Government issued ID proof',
//                 'heading2': 'id proof image not clear 23',
//             },
//             {
//                 'heading1': 'To continue with your request, please upload the invoice copy.',
//                 'heading2': 'invoice copy not clear 23',
//             },
//             {
//                 'heading1': 'Please upload an image of the damaged part / area in focus',
//                 'heading2': 'damaged part image not clear 23',
//             },
//             {
//                 'heading1': 'Please upload image of the device with IMEI number displayed on the screen',
//                 'heading2': 'Please upload image of the device with IMEI number displayed on the screen 23',
//             },
//             {
//                 'heading1': 'Incidence Date',
//                 'heading2': 'wrong data in Date and time of Loss/Damage updated  2',
//             },
//             {
//                 'heading1': 'Where was the device kept at the time of incident?',
//                 'heading2': 'Wrong text Where was the device kept at the time of incident 3',
//             },
//             {
//                 'heading1': 'Who was using the device at the time of damage?',
//                 'heading2': 'Wrong text Who was using the handset at the time of damage 3',
//             },
//             {
//                 'heading1': 'Where was the device user and what was he doing at the time of incident?',
//                 'heading2': 'Wrong text Where was the device user and what was he doing at the time of incident 3',
//             },
//             {
//                 'heading1': 'Explain the incident due to which your device was damaged, highlighting the events which led up to the incident',
//                 'heading2': 'Wrong text Explain the incident 3',
//             },
//         ],
//     },
//     'heading5': 'Provide bank details for transfer of net approved amount to your account',
//     'anchorObj': {
//         'anchorHyerRef': 'https://www.oneassist.in/srid222367/?uniqueid=2226645/bankdetails',
//         'anchorText': 'Click here to Enter bank details',
//         'iconLink': 'https://ws.oneassist.in/static/portal_v2/hash/hash-files/oneassist-logo.1029a5f2.png',
//         'heading': 'Provide bank details for transfer of net approved amount to your account',
//     },
//     'documentList': [
//         {
//             'docName': 'Please upload an image of Government issued ID proof',
//             'docKey': 'ID_PROOF',
//             'reason': 'Not clear 1',
//         },
//         {
//             'docName': 'To continue with your request, please upload the invoice copy.',
//             'docKey': 'INVOICE_IMAGE',
//             'reason': 'Not clear 3',
//         },
//         {
//             'docName': 'Please upload an image of the damaged part / area in focus',
//             'docKey': 'SCREEN_IMAGE',
//             'reason': 'Not clear 2',
//         },
//     ],
//     'buttons_inline': [
//         {
//             'id': 1,
//             'params': {
//                 'test': 'test',
//             },
//             'value': {
//                 'text': 'I have a query',
//                 'label': 'I have a query',
//             },
//         },
//         {
//             'id': 2,
//             'params': {
//                 'service_request_id': 129956,
//             },
//             'value': {
//                 'text': 'Upload Documents',
//                 'label': 'I want to upload documents for my claim',
//             },
//         },
//     ],
// };

export default class ChatbotStatusCard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let data = this.props.data;
        let heading5 = null;
        if (data.heading5 != null && data.heading5 !== '') {
            heading5 = <Text style={[TextStyle.text_14_bold, style.heading5]}>{data.heading5}</Text>;
        }
        let heading4 = null;
        if (data.heading4 != null && data.heading4 !== "") {
            heading4 = <Text style={[TextStyle.text_12_normal, style.heading4]}>{data.heading4}</Text>
        }

        let heading3 = null;
        if (data.heading3 != null && data.heading3 !== '') {
            heading3 = <Text style={[style.heading3, TextStyle.text_12_normal]}>{data.heading3}</Text>;
        }
        let anchorView = null;
        if (data.anchorObj != null) {
            let headingView = null;
            if (data.anchorObj.heading != null && data.anchorObj.heading !== '') {
                headingView = <Text style={[style.anchorText, TextStyle.text_14_bold]}>{data.anchorObj.heading}</Text>;
            }
            anchorView = <View style={style.anchorView}>
                <Image style={style.anchorIcon} source={{uri: data.anchorObj.iconLink}}/>
                {headingView}
                <TouchableOpacity onPress={() => Linking.openURL(data.anchorObj.anchorHyerRef)}>
                    <Text style={TextStyle.text_12_semibold}>{data.anchorObj.anchorText}</Text>
                </TouchableOpacity>
            </View>;
        }
        let rejectedDocumentList = null;
        if (data.rejectedDcouments !== null && data.rejectedDcouments !== undefined) {
            rejectedDocumentList = <RejectedDocumentList
                heading={data.rejectedDcouments.heading}
                data={data.rejectedDcouments.documentList}/>;
        }
        let buttonsView = null;
        if (data.buttons_inline !== null && data.buttons_inline !== undefined && data.buttons_inline.length !== 0) {
            let buttonsContainerView = null;
            let buttons = data.buttons_inline.reverse();
            if (buttons.length === 1) {
                buttonsContainerView = <View style={style.singleButtonView}>
                    <TouchableOpacity
                        onPress={() => this.props.onClick(buttons[0].value.label, buttons[0].params, null)}>
                        <Text style={[style.singleButton, ButtonStyle.TextOnlyButton]}>{buttons[0].value.text}</Text>
                    </TouchableOpacity>
                </View>;
            } else if (buttons.length === 2) {
                buttonsContainerView = <View style={style.doubleButtonView}>
                    <TouchableOpacity style={{width: '50%'}}
                                      onPress={() => this.props.onClick(buttons[0].value.label, buttons[0].params, null)}>
                        <Text style={[style.doubleButton, ButtonStyle.TextOnlyButton]}>{buttons[0].value.text}</Text>
                    </TouchableOpacity>
                    <View style={style.buttonSeparator}/>
                    <TouchableOpacity style={{width: '50%'}}
                                      onPress={() => this.props.onClick(buttons[1].value.label, buttons[1].params, null)}>
                        <Text style={[style.doubleButton, ButtonStyle.TextOnlyButton]}>{buttons[1].value.text}</Text>
                    </TouchableOpacity>
                </View>;
            }

            buttonsView = <View>
                <View style={CommonStyle.separator}/>
                {buttonsContainerView}
            </View>;
        }
        return (
            <View style={style.MainContainer}>
                <Text style={[style.heading, TextStyle.text_12_bold]}>{data.heading.toUpperCase()}</Text>
                <Text style={[style.heading1, TextStyle.text_14_bold]}>{data.heading1}</Text>
                <View style={style.statusView}>
                    <View style={style.dotView}/>
                    <Text style={[style.heading2, TextStyle.text_14_bold]}>{data.heading2}</Text>
                </View>
                {heading3}
                {heading4}
                {rejectedDocumentList}
                {heading5}
                {anchorView}
                {buttonsView}
            </View>
        );
    }
}
const style = StyleSheet.create({
    MainContainer: {
        backgroundColor: colors.white,
        borderRadius: spacing.spacing4,
        overflow: 'hidden',
    },
    heading: {
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing12,
        marginTop: spacing.spacing12,
        letterSpacing: 1.0,
        color: colors.grey,
    },
    heading1: {
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing12,
        marginTop: spacing.spacing12,
        textAlign: 'left',
    },
    statusView: {
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing12,
        flexDirection: 'row',
        marginTop: spacing.spacing20,
        marginBottom: spacing.spacing12,
    },
    dotView: {
        width: 6,
        height: 6,
        borderRadius: 3,
        backgroundColor: colors.charcoalGrey,
        marginTop: 7,
    },
    heading2: {
        marginLeft: spacing.spacing8,
    },
    heading3: {
        marginTop: -spacing.spacing8,
        marginLeft: 26,
        marginRight: spacing.spacing12,
        marginBottom: spacing.spacing12,
    },
    heading4: {
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing12,
        marginTop: spacing.spacing4,
        marginBottom: spacing.spacing12,
        color: colors.seaGreen,
    },
    heading5: {
        marginTop: spacing.spacing4,
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing12,
        marginBottom: spacing.spacing16,
        textAlign: 'left',
    },
    singleButtonView: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
    },
    doubleButtonView: {
        flexDirection: 'row',
        alignSelf: 'center',
    },
    buttonSeparator: {
        width: 1,
        height: '100%',
        backgroundColor: colors.lightGrey2,
    },
    singleButton: {
        margin: spacing.spacing16,
    },
    doubleButton: {
        paddingBottom: spacing.spacing16,
        paddingTop: spacing.spacing16,
        paddingRight: spacing.spacing4,
        paddingLeft: spacing.spacing4,
    },
    anchorView: {
        backgroundColor: colors.lightBlue,
        padding: spacing.spacing12,
    },
    anchorIcon: {
        width: 72,
        height: 24,
        marginBottom: spacing.spacing4,
    },
    anchorText: {
        marginTop: spacing.spacing4,
        marginBottom: spacing.spacing4,
    },
});
