import React, { useState, useEffect, useContext } from 'react';
import { Image, Keyboard, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import colors from '../Constants/colors';
import spacing from '../Constants/Spacing';
import { SUGGESTION_VIEW_MARGIN_FROM_BOTTOM, SEND_VIEW_TAGS } from '../Buyback/buybackActions/Constants';
import { TextStyle } from '../Constants/CommonStyle';
import { ChatContext } from "../Components/ChatComponent";
import * as Actions from "../Constants/ActionTypes";
import {removeSpecialCharacters} from '../commonUtil/AppUtils';

// const data = {
//     show: true,
//     minLength: 20,
//     maxLength: 342,
//     placeHolder: 'Enter incident description',
//     tags: [
//         'You have',
//         'these on-going',
//         'service requests',
//     ],
// };
let handleAutoSuggestion;
let autoSuggestedTagData = [];
const DEFAULT_INPUT_TEXT_HEIGHT = 40;
const CONSTRAINT_LAYOUT_HEIGHT = 36;

const ChatbotTagInput = props => {
    const { selectedTagData, otherTagData } = useContext(ChatContext);
    useEffect(() => {
        props.updateAutoSuggestions(props.data?.inputType === 'autoSuggestion');
    }, [])
    let selectedTags = selectedTagData?.selectedTags ?? [];
    let tagData = [...selectedTags, ...autoSuggestedTagData];
    const isTextInputEnabled = otherTagData?.isOthersOptionSelected;
    const [inputMessage, setInputMessage] = useState('');
    updateAutoSuggestedTagData();
    const submitEnabled = checkDataToSend();
    const [height, setHeight] = useState(DEFAULT_INPUT_TEXT_HEIGHT);
    const [bottomViewHeight, setBottomViewHeight] = useState(0);

    function updateSize(height) {
        if (height < 135) {
            setHeight(height);
        } else {
            setHeight(135);
        }
        updateAutoSuggestionMargin(bottomViewHeight);
    }

    function updateAutoSuggestionMargin(height) {
        //add constraint layout height also in bottomview margin from autosuggestion
        let constraintViewHeight = ((props.data.minLength > 3) ? CONSTRAINT_LAYOUT_HEIGHT : 0);
        props.updateBottomMarginAutoSuggestionView(constraintViewHeight + height);
    }

    function updateAutoSuggestedTagData() {
        if (props.selectedSuggestionData?.text) {
            let index = autoSuggestedTagData.indexOf(props.selectedSuggestionData?.text);
            if (index === -1) {
                autoSuggestedTagData.push(props.selectedSuggestionData?.text);
                setInputMessage('');
            }
        }
    }

    function checkDataToSend() {
        return tagData?.length || inputMessage?.length
    }

    // Similar to componentDidMount and componentDidUpdate:
    useEffect(() => {
        let delayAfterPauseTypingTime = 0;
        handleAutoSuggestion = debounce(function (text) {
            props.getAutosuggestions(text, props.data?.inputData);
        }, delayAfterPauseTypingTime);
    });

    function debounce(func, wait, immediate) {
        let timeout;
        return function () {
            let context = this, args = arguments;
            let later = function () {
                timeout = null;
                if (!immediate) {
                    func.apply(context, args);
                }
            };
            let callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) {
                func.apply(context, args);
            }
        };
    };

    function onSendButtonClick() {
        if (checkDataToSend()) {
            let inputMsgTrimmed = inputMessage.trim();
            inputMsgTrimmed = removeSpecialCharacters(inputMsgTrimmed);
            let params = selectedTagData?.params;
            if (params === null || params === undefined) {
                params = otherTagData.params;
            }
            params['damaged_parts'] = tagData;
            params['others'] = inputMsgTrimmed;
            let tagDataText = tagData?.length === 0 ? "" : tagData?.toString();
            let otherText = tagDataText === '' || tagDataText === undefined ? inputMsgTrimmed : "," + inputMsgTrimmed;
            let sendDataText = tagDataText + (inputMsgTrimmed === '' || inputMsgTrimmed === undefined ? "" : otherText);
            let extraData = { 'text': sendDataText, 'params': params };
            props.sendMessage(JSON.stringify(extraData), params, Actions.SEND_MESSAGE_TAGS);
            props.updateBottomView();
            // Keyboard.dismiss();
            if (this.textInput !== null && this.textInput !== undefined) {
                this.textInput.clear();
            }
            setInputMessage('');
            props.autoSuggestionVisible(false);
        }
    }

    const checkIcon = submitEnabled ? require('../images/roundCheckEnable.webp') : require('../images/roundCheckDisable.webp');
    const submitIcon = submitEnabled ? require('../images/icon_send.webp') : require('../images/icon_send_disabled.webp');

    const constraintBox = () => {
        return props.data.minLength > 3 ? <View style={styles.constraintContainer}>
            <Image style={styles.checkIcon} source={checkIcon} />
            <Text style={[styles.constraintText, submitEnabled && styles.constraintPassText]}>{props?.data?.thresholdText ?? "Should be atleast ".concat(this.props.data.minLength).concat(" characters")}</Text>
            <Text style={[styles.characterText, submitEnabled && styles.constraintPassText]}>{inputMessage.length}/{props.data.maxLength}</Text>
        </View> : null;
    }

    const tagView = () => {
        return tagData ? (<View style={styles.tagContainer}>
            {tagData.map((tag, key) =>
                <View key={key} style={styles.tagStyle}>
                    <Text style={TextStyle.text_12_medium}>{tag}</Text>
                </View>,
            )}
        </View>) : null;
    }

    return (
        <View style={styles.MainContainer}>
            {constraintBox}
            <View onLayout={(event) => {
                let {x, y, width, height} = event.nativeEvent.layout;
                setBottomViewHeight(height);
                updateAutoSuggestionMargin(height);
            }} style={styles.sendContainer}>
                <View style={{ flex: 1, justifyContent: 'space-between'}}>
                    {tagView()}
                    {isTextInputEnabled ? (<TextInput
                        underlineColorAndroid="transparent"
                        onChangeText={inputMsg => {
                            setInputMessage(inputMsg);
                            props.autoSuggestionVisible(inputMsg !== '');
                            if (props.autoSuggestionEnabled && inputMsg !== '') {
                                handleAutoSuggestion(removeSpecialCharacters(inputMsg));
                            }
                        }}
                        style={[styles.textInputContainer]}
                        editable={true}
                        height={Math.max(spacing.spacing40, height)}
                        multiline={true}
                        value={inputMessage}
                        autoFocus={true}
                        maxLength={props.data.maxLength}
                        numberOfLines={6}
                        placeholder={props.data.placeHolder}
                        placeholderTextColor={colors.lightGrey}
                        onContentSizeChange={e => updateSize(e.nativeEvent.contentSize.height)}
                    />) : null}
                </View>
                <TouchableOpacity style={styles.iconContainer}
                    disabled={!submitEnabled}
                    onPress={onSendButtonClick}
                >
                    <Image source={submitIcon} style={styles.iconStyle} />
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    MainContainer: {
        backgroundColor: colors.white,
        width: '100%',
        shadowOffset: { height: -4 },
        shadowOpacity: 0.1,
        shadowRadius: 2,
        shadowColor: colors.black,
        elevation: 12,
    },
    sendContainer: {
        flexDirection: 'row',
        paddingLeft: spacing.spacing16,
        paddingRight: spacing.spacing20,
        backgroundColor: colors.white,
    },
    tagContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: spacing.spacing12,
        marginBottom: spacing.spacing4,
    },
    tagStyle: {
        marginRight: spacing.spacing8,
        marginBottom: spacing.spacing8,
        backgroundColor: colors.tagBgColor,
        borderRadius: 20,
        paddingLeft: spacing.spacing12,
        paddingRight: spacing.spacing12,
        paddingTop: spacing.spacing8,
        paddingBottom: spacing.spacing8,
    },
    textInputContainer: {
        marginTop: spacing.spacing4,
        fontFamily: 'Lato',
        marginBottom: spacing.spacing4,
    },
    iconContainer: {
        alignSelf: 'flex-end',
        marginTop: spacing.spacing16,
        marginBottom: spacing.spacing16,
    },
    iconStyle: {
        marginLeft: spacing.spacing16,
        height: spacing.spacing24,
        width: spacing.spacing24,
        alignSelf: 'center',
        resizeMode: 'contain',
    },
    constraintContainer: {
        backgroundColor: colors.soulitudeGrey,
        width: '100%',
        flexDirection: 'row',
        height: CONSTRAINT_LAYOUT_HEIGHT,
        paddingTop: spacing.spacing8,
        paddingBottom: spacing.spacing8,
        paddingLeft: spacing.spacing20,
        paddingRight: spacing.spacing20,
    },
    checkIcon: {
        width: 18,
        height: 18,
    },
    constraintText: {
        fontSize: 12,
        fontFamily: 'Lato',
        height: 18,
        lineHeight: 18,
        fontWeight: '500',
        marginLeft: spacing.spacing4,
        width: '80%',
    },
    characterText: {
        fontSize: 12,
        fontFamily: 'Lato',
        height: 18,
        lineHeight: 19,
        marginRight: 0,
    },
    constraintPassText: {
        color: colors.seaGreen
    }
});
export default ChatbotTagInput;
