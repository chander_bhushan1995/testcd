import React, {useState} from 'react';
import {Image, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import colors from '../Constants/colors';
import spacing from '../Constants/Spacing';
import {CommonStyle, TextStyle} from '../Constants/CommonStyle';
import CheckBox from 'react-native-check-box';
import {SendMessageStatus} from '../Constants/SendMessageStatus';
import Hyperlink from 'react-native-hyperlink';
import {chatManager} from '../Navigation';

// var data = {
//     'text': 'cns,.nc/,smc/smc;sm',
//     "params": [
//         {text: 'No Sound', type: 'Checkbox', isSelected: true},
//         {text: 'Not Charging', type: 'Checkbox', isSelected: true},
//         {text: 'Not', type: 'Checkbox', isSelected: true}
//         ],
// };

const ChatbotTagsSendView = props => {
    var statusView = <View/>;
    var isRetryEnabled = false;
    if (props.status === SendMessageStatus.Pending) {
        props.sendMessage();
    } else if (props.status === SendMessageStatus.Sending) {
        statusView = <View style={CommonStyle.statusView}>
            <Image style={CommonStyle.statusImage} source={require("../images/clock.webp")}/>
            <Text style={CommonStyle.messageTimeSendMessage}>Sending</Text>
        </View>
    } else if (props.status === SendMessageStatus.NoInternet || props.status === SendMessageStatus.Failure) {
        if (props.retryCount <= 3) {
            if (props.internetStatus) {
                props.sendMessage();
            } else {
                statusView = <View style={CommonStyle.statusView}>
                    <Image style={CommonStyle.statusImage} source={require("../images/clock.webp")}/>
                    <Text style={CommonStyle.messageTimeSendMessage}>Sending</Text>
                </View>
            }
        } else {
            isRetryEnabled = true;
            statusView = <View style={CommonStyle.statusView}>
                <Text style={[CommonStyle.messageTimeSendMessage, CommonStyle.messageError]}>Message not delivered. Tap to retry</Text>
            </View>
        }
    } else if (props.status === SendMessageStatus.CannotSend) {
        statusView = <View style={CommonStyle.statusView}>
            <Text style={[CommonStyle.messageTimeSendMessage, CommonStyle.messageError]}>Message not delivered.</Text>
        </View>
    } else {
        let sentAtText = (chatManager?.getSentAtText() ?? "Sent at ") + " " + props.timeStamp;
        statusView = <View style={CommonStyle.statusView}>
            <Image style={CommonStyle.statusImage} source={require("../images/doubleTick.webp")}></Image>
            <Text style={CommonStyle.messageTimeSendMessage}>{sentAtText}</Text>
        </View>
    }

    const tagView = props.messageBody.damaged_parts !== null && props.messageBody.damaged_parts !== undefined ? <View style={style.tagContainer}>
        {props.messageBody.damaged_parts.map((tag, key) =>
            <View key={key} style={style.tagStyle}>
                <Text style={TextStyle.text_12_semibold}>{tag}</Text>
            </View>,
        )}
    </View> : null;
    return (
        <TouchableOpacity disabled={!isRetryEnabled}
                          onPress={() => {
                              if (isRetryEnabled) {
                                  props.sendMessage();
                              }
                          }}
        >
            <Hyperlink linkDefault={true} linkStyle={{ textDecorationLine: 'underline' }}>
                <View style={style.mainContainer}>
                    {tagView}
                    <Text style={[TextStyle.text_14_semibold, style.msgStyle]}>{props.messageBody.others}</Text>
                </View>
            </Hyperlink>
            {statusView}
        </TouchableOpacity>
    );
};

const style = StyleSheet.create({
    mainContainer: {
        paddingTop: spacing.spacing12,
        paddingBottom: spacing.spacing4,
        paddingLeft: spacing.spacing12,
        paddingRight: spacing.spacing12,
        marginTop: spacing.spacing16,
        marginRight: spacing.spacing16,
        marginLeft: 80,
        borderRadius: 8,
        alignSelf: "flex-end",
        backgroundColor: colors.blue,
    },
    tagContainer: {
        marginBottom: spacing.spacing4,
        flexWrap: "wrap",
        flexDirection: 'row',
    },
    tagStyle: {
        marginRight: spacing.spacing8,
        marginBottom: spacing.spacing8,
        backgroundColor: colors.white,
        borderRadius: 20,
        paddingLeft: spacing.spacing12,
        paddingRight: spacing.spacing12,
        paddingTop: spacing.spacing8,
        paddingBottom: spacing.spacing8,
    },
    msgStyle: {
        color: colors.white,
        textAlign: "center",
        overflow: "hidden",
    }
});

export default ChatbotTagsSendView;
