import React, {Component} from "react";
import {StyleSheet, Text, View} from "react-native";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";
import Hyperlink from 'react-native-hyperlink';

// let data = {
//     text: "your service requedst id is 123123",
//     info: "time should be in 24 hour format"
//     }

export default class ChatbotText extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var messageInfo = (this.props.data.info != null) ?
			<Hyperlink linkDefault={true} linkStyle={{ textDecorationLine: 'underline' }}>
				<Text style={style.messageInfo}>{this.props.data.info}</Text>
			</Hyperlink>
			: null;
        return (
            <View style={style.MainContainer}>
				<Hyperlink linkDefault={true} linkStyle={{ textDecorationLine: 'underline' }}>
					<Text style={style.messageText}>{this.props.data.text}</Text>
				</Hyperlink>
				{messageInfo}
            </View>
        );
    }
}

const style = StyleSheet.create({
    MainContainer: {
        borderRadius: 4,
        overflow: "hidden",
        backgroundColor: colors.white,
        alignItems: "flex-start",
    },
    messageText: {
        margin: spacing.spacing12,
        flexWrap: "wrap",
        color: colors.charcoalGrey,
        fontSize: 14,
        fontFamily: "Lato",
    },
    messageInfo: {
        color: colors.grey,
        marginLeft: spacing.spacing12,
        marginBottom: spacing.spacing12,
        marginRight: spacing.spacing12,
        fontSize: 14,
        fontFamily: "Lato",
    },
});
