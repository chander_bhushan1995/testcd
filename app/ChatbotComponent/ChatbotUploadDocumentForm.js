import React, {Component} from 'react';
import {
    FlatList,
    Image,
    NativeEventEmitter,
    NativeModules,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform, Alert, PermissionsAndroid,
} from 'react-native';

import ChatbotFormUploadCell from './ChatbotFormUploadCell';
import * as DocumentUploadEvent from '../Constants/UploadDocumentEvent';
import BatchedBridge from 'react-native/Libraries/BatchedBridge/BatchedBridge';
import colors from '../Constants/colors';
import spacing from '../Constants/Spacing';
import {ButtonStyle, TextStyle} from '../Constants/CommonStyle';
import * as FormUploadCellActionType from '../Constants/FormUploadCellActionType';
import ChatbotSelfDeclarationView from './ChatbotSelfDeclarationView';
import ImageDetailComponent from './ImageDetailComponent';
import DocumentsUtils from '../commonUtil/DocumentsUtils';
import {THUMBNAIL_IMAGE_FILE} from '../Constants/UploadDocumentEvent';
import {BYTEARRAYFILE} from '../Constants/UploadDocumentEvent';
import * as DocumentType from '../Constants/DocumentTypeConstant';
import { parseJSON, deepCopy} from "../commonUtil/AppUtils";

const reactBridgeRef = NativeModules.ChatBridge;
let _this;
export default class ChatbotUploadDocumentForm extends Component {

    constructor(props) {
        super(props);
        _this = this;
        createSubmitReq = createSubmitReq.bind(this);
        let mandatoryDocList = this.props.navigation.getParam('data', null).formItems[0].list.filter(data => (data.isMandatory === true));
        this.state = {
            data: this.props.navigation.getParam('data', null),
            docList: mandatoryDocList,
            dialogContext: this.props.navigation.getParam('dialogContext', null),
            submitDocReq: [],
            modalVisible: false,
            modalLoadingVisible: false,
            imageDetailDocumentType: DocumentType.IMAGE,
            imageDetailUrl: '',
            index: -1,
            nonMandatoryDocsAddedInList: false,
            lastMandatoryDocIndex: mandatoryDocList.length - 1,
        };
    }

    // / To send getUrl in IOS to action
    updateGetUrl = (getUrl, index, status) => {
        let docList = this.state.docList;
        let listItem = docList[index];
        if (status !== -1) {
            listItem.documentUploadEvent = DocumentUploadEvent.COMPLETE;
            if (getUrl !== null) {
                listItem.isStateDone = true;
            }
            listItem = {...listItem, getUrl: getUrl};
        } else {
            listItem.documentUploadEvent = DocumentUploadEvent.NOT_STARTED;
            listItem.documentUri = null;
        }
        docList[index] = listItem;
        this.setState({docList: docList});
    };

    updateItemState = (index, value) => {
        let docList = this.state.docList;
        let listItem = docList[index];
        listItem.isStateDone = value;
        docList[index] = listItem;
        this.setState({docList: docList});
    };

    //to open tutorial view for fmip or mi
    onLinkClick = (clickActionType, url, fileName, index) => {
        if (clickActionType === FormUploadCellActionType.FMIP) {
            this.props.navigation.navigate('FMIPTutorialComponent', {});
        } else if (clickActionType === FormUploadCellActionType.MI_LOGOUT) {
            this.props.navigation.navigate('MILogoutTutorialComponent', {});
        } else if (clickActionType === FormUploadCellActionType.VIEW_TEMPLATE) {
            let urlTemplate = 'https://docs.google.com/gview?embedded=true&url=' + url;
            this.props.navigation.navigate('DocPreviewComponent', {
                url: urlTemplate,
            });
        } else if (clickActionType === FormUploadCellActionType.DOWNLOAD_TEMPLATE) {
            let docList = this.state.docList;
            let listItem = docList[index];
            let callDownloadTemplate = _this.props.navigation.getParam('callDownloadTemplate', null);
            if(Platform.OS === 'android' && Platform.Version >= 22){
                this.askStoragePermission(listItem.draft_id);
            }else{
                callDownloadTemplate({draftId: listItem.draft_id});
            }
        }
    };

    async askStoragePermission(draftId){
        let callDownloadTemplate = _this.props.navigation.getParam('callDownloadTemplate', null);
        try{
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                callDownloadTemplate({draftId: draftId});
            }
        }catch (err) {
            console.warn(err);
        }
    }

    //to open image detail view
    onImageClick = (index) => {
        let docList = this.state.docList;
        let listItem = docList[index];
        let uri = listItem.documentUri;
        if (uri !== null && uri !== undefined && listItem.documentType === DocumentType.IMAGE) {
            this.setState({modalVisible: true, imageDetailUrl: uri, index: index, modalLoadingVisible: listItem.isAlreadyUploaded, imageDetailDocumentType: listItem.documentType});
            if(listItem.isAlreadyUploaded){
                _this.callDownloadAlreadyUploadedImageFromSP({
                    documentId: listItem.documentId,
                    storageRefId: listItem.storageRefId,
                    index: index,
                    fileTypeRequired: BYTEARRAYFILE,
                    callback: _this.updateImageDetailUri,
                });
            }
        } else {
            this.onImageUpload(index);
        }
    };

    updateImageDetailUri(response, index){
        let documentType = _this.getDocumentTypeFromUrl(response)
        _this.setState({imageDetailUrl: response, modalLoadingVisible: false, imageDetailDocumentType: documentType});
    }

    //to open image detail view
    onImageDetailComponentClose = () => {
        this.setState({modalVisible: false});
    };

    //to open image detail view
    onReUploadClicked = () => {
        this.onImageUpload(this.state.index);
    };

    uploadProgress = (index, documentId, errorMsg) => {
        let docList = this.state.docList;
        let listItem = docList[index];
        if (documentId !== '') {
            listItem.documentUploadEvent = DocumentUploadEvent.COMPLETE;
            listItem.isStateDone = true;
            listItem.document_id = documentId;
        } else {
            listItem.documentUploadEvent = DocumentUploadEvent.NOT_STARTED;
            listItem.documentUri = null;
            Alert.alert(errorMsg);
        }
        docList[index] = listItem;
        this.setState({docList: docList});

    };

    onImageUpload = (index) => {
        const sectionDocumentParams = {
            context: () => {
                return _this;
            }, onSuccess: (selectedDocumentObject) => {
                let uri = selectedDocumentObject.docUrl;
                let onUploadImage = this.props.navigation.getParam('onUploadImage', null);
                let docList = this.state.docList;
                let listItem = docList[index];
                onUploadImage({
                    uri: uri,
                    index: index,
                    documentTypeId: listItem.docId,
                    draftId: listItem.draft_id,
                    documentId: listItem.document_id,
                    docType: selectedDocumentObject.docType,
                    fileName: selectedDocumentObject.fileName,
                    callback: this.uploadProgress,
                });
                // reactBridgeRef.uploadClaimsBotDocument(uri, index);
                listItem = {
                    ...listItem,
                    documentUploadEvent: DocumentUploadEvent.UPLOADING,
                    documentUri: uri,
                    documentType: selectedDocumentObject.type,
                    isAlreadyUploaded: false,
                };
                docList[index] = listItem;
                this.setState({docList: docList});
            },
        };
        try {
            new DocumentsUtils().onDocumentSelection(sectionDocumentParams);
        } catch (e) {
            Alert.alert(e.message);
        }
    };

    onDocumentSelect = (index, docUri, docType, docinfo, docSize) => {
        reactBridgeRef.uploadDocument(docUri, response => {
            let result = parseJSON(response)
            _this.updateGetUrl(result.url, index, result.status);
        })
        let docList = this.state.docList;
        let listItem = docList[index];
        listItem = {
            ...listItem,
            documentUploadEvent: DocumentUploadEvent.UPLOADING,
            documentUri: docUri,
            documentType: docType,
        };
        docList[index] = listItem;
        this.setState({docList: docList});
    };

    componentDidMount() {
        //call draft detail api
        let docList = this.state.docList;
        let listItem = docList[0];
        if (listItem.draft_id !== '' && listItem.draft_id !== undefined) {
            let callDraftDetailApi = this.props.navigation.getParam('callDraftDetailApi', null);
            callDraftDetailApi({
                draftId: listItem.draft_id,
                callback: this.setDraftDetailData,
            });
        }
    }

    setDraftDetailData(response) {
        let serviceDocuments = response?.data[0]?.request?.serviceDocuments;
        let shouldUpdate = false;
        if (serviceDocuments !== null) {
            let docList = _this.state.docList;
            for (let i = 0; i < serviceDocuments.length; i++) {
                let serviceDocument = serviceDocuments[i];
                for (let j = 0; j < docList.length; j++) {
                    let listItem = docList[j];
                    if (serviceDocument.documentTypeId === listItem.docId) {
                        listItem.document_id = serviceDocument.documentId;
                        listItem.storageRefId = serviceDocument.storageRefId;
                        listItem = {
                            ...listItem,
                            documentUploadEvent: DocumentUploadEvent.UPLOADING,
                        };
                        _this.callDownloadAlreadyUploadedImageFromSP({
                            documentId: serviceDocument.documentId,
                            storageRefId: serviceDocument.storageRefId,
                            index: j,
                            fileTypeRequired: THUMBNAIL_IMAGE_FILE,
                            callback: _this.updateAlreadyUploadedImage,
                        });
                        docList[j] = listItem;
                        shouldUpdate = true;
                        break;
                    }
                }
            }
            if (shouldUpdate) {
                _this.setState({docList: docList});
            }
        }
    }

    callDownloadAlreadyUploadedImageFromSP(request){
        let callDownloadAlreadyUploadedImage = _this.props.navigation.getParam('callDownloadAlreadyUploadedImage', null);
        callDownloadAlreadyUploadedImage({
            documentId: request.documentId,
            storageRefId: request.storageRefId,
            index: request.index,
            fileTypeRequired: request.fileTypeRequired,
            callback: request.callback,
        });
    }

    updateAlreadyUploadedImage(response, index) {
        let docList = _this.state.docList;
        let listItem = docList[index];
        listItem.isStateDone = true;
        let documentType = _this.getDocumentTypeFromUrl(response)
        listItem = {
            ...listItem,
            documentUploadEvent: DocumentUploadEvent.COMPLETE,
            documentUri: response,
            isAlreadyUploaded: true,
            documentType: documentType,
        };
        docList[index] = listItem;
        _this.setState({docList: docList});
    }

    getDocumentTypeFromUrl(imageUrl){
        let documentType = DocumentType.DOCUMENT;
        let imageUrlArr = imageUrl.split('.');
        let lastIndex = imageUrlArr.length - 1;
        if(imageUrlArr[lastIndex] === null || imageUrlArr[lastIndex] === undefined || ['jpg', 'jpeg', 'png'].includes(imageUrlArr[lastIndex].toString().toLowerCase())){
            documentType = DocumentType.IMAGE;
        }
        return documentType;
    }

    /**
     * add non mandatory documents in doc list
     */
    addNonMandatoryDocumentsInList = () => {
        let nonMandatoryList = this.state.data.formItems[0].list.filter(data => (data.isMandatory !== true));
        let docList = this.state.docList;
        docList.push(...nonMandatoryList);
        this.setState({docList: docList, nonMandatoryDocsAddedInList: true});
    };

    renderItem = (item, index) => {
        let view;
        if (item.type === 'doc_upload') {
            view = <ChatbotFormUploadCell
                data={item}
                index={index}
                dialogContext={this.state.dialogContext}
                backgroundColor={colors.white}
                onLinkClick={_this.onLinkClick}
                onImageClick={_this.onImageClick}
            />;
        } else if (item.type === 'self_declaration') {
            view = <ChatbotSelfDeclarationView
                data={item}
                index={index}
                updateItemState={this.updateItemState}
            />;
        }
        return <View>
            {view}
            {this.getAddMoreImagesView(index)}
        </View>;
    };

    getAddMoreImagesView = (index) => {
        //add more images or optional documents textview
        let addMoreImagesText = this.state.nonMandatoryDocsAddedInList ?
            <Text style={[TextStyle.text_10_bold, style.optionalDocsText]}>{this?.state?.data?.extraImages?.label ?? "OPTIONAL DOCUMENTS"}</Text> :
            <Text onPress={() => {
                this.addNonMandatoryDocumentsInList();
            }} style={[TextStyle.text_12_bold, style.addMoreImagesText]}>+ {this?.state?.data?.extraImages?.text ?? "Add more images"}</Text>;
        return index === this.state.lastMandatoryDocIndex ? addMoreImagesText : null;
    };

    render() {
        let onFormButtonPress = this.props.navigation.getParam('onFormButtonPress', null);
        let isSubmitEnabled =
            this.state.docList.filter(data => data.isMandatory === true).length === this.state.docList.filter(
            data =>
                data.isMandatory === true &&
                data.isStateDone === true,
            ).length;
        let editPreviousDetailButton;
        let actionButtonData = this.state.data.actionButton;
        if (actionButtonData !== null && actionButtonData !== undefined) {
            editPreviousDetailButton = <TouchableOpacity
                style={[style.editPreviousDetailContainer]}
                onPress={() => {
                    this.props.navigation.goBack();
                    let data = {
                        text: actionButtonData.params.input_text,
                    };
                    onFormButtonPress(data);
                }}
            >
                <Text
                    style={[TextStyle.text_12_bold, style.editPreviousDetailStyle]}>
                    {actionButtonData.text}
                </Text>
            </TouchableOpacity>;
        }
        let button0;
        let button1;
        if (this.state.data.buttons.length === 1) {
            button0 = null;
            button1 = <TouchableOpacity
                disabled={!isSubmitEnabled}
                style={[style.buttonTouchArea, ButtonStyle.BlueButton, isSubmitEnabled
                    ? {backgroundColor: colors.blue}
                    : {backgroundColor: colors.lightGrey}]}
                onPress={() => {
                    this.props.navigation.goBack();
                    let data = {
                        text: this.state.data.buttons[0].value.label,
                        documentUrl: createSubmitReq(),
                    };
                    onFormButtonPress(data);
                }}
            >
                <Text
                    style={ButtonStyle.primaryButtonText}>
                    {this.state.data.buttons[0].value.text}
                </Text>
            </TouchableOpacity>;
        } else {
            button0 = <TouchableOpacity
                style={style.buttonTouchArea}
                onPress={() => {
                    this.props.navigation.goBack();
                    let data = {
                        text: this.state.data.buttons[0].value.label,
                    };
                    onFormButtonPress(data);
                }}
            >
                <Text style={[ButtonStyle.TextOnlyButton, style.formButton]}>
                    {this.state.data.buttons[0].value.text}
                </Text>
            </TouchableOpacity>;

            button1 = <TouchableOpacity
                disabled={!isSubmitEnabled}
                style={[style.buttonTouchArea, ButtonStyle.BlueButton, isSubmitEnabled
                    ? {backgroundColor: colors.blue}
                    : {backgroundColor: colors.lightGrey}]}
                onPress={() => {
                    this.props.navigation.goBack();
                    let data = {
                        text: this.state.data.buttons[1].value.label,
                        documentUrl: createSubmitReq(),
                    };
                    onFormButtonPress(data);
                }}>
                <Text
                    style={ButtonStyle.primaryButtonText}>
                    {this.state.data.buttons[1].value.text}
                </Text>
            </TouchableOpacity>;
        }
        return (
            <SafeAreaView style={style.SafeArea}>
                <ImageDetailComponent visible={this.state.modalVisible}
                                      imageDetailUrl={this.state.imageDetailUrl}
                                      onImageDetailComponentClose={this.onImageDetailComponentClose}
                                      onReUploadClicked={this.onReUploadClicked}
                                      modalLoadingVisible={this.state.modalLoadingVisible}
                                      documentType={this.state.imageDetailDocumentType}/>
                <View style={style.MainContainer}>
                    <View style={style.topView}>
                        <TouchableOpacity activeOpacity={1} onPress={() => this.props.navigation.goBack()}>
                            <View style={style.headerView}>
                                <Text
                                    style={[TextStyle.text_12_bold, style.headerViewTitle]}>{this.state.data.formLabel.toUpperCase()}</Text>
                                <Image source={require('../images/icon_chevornDown.webp')} style={style.minimiseImage}/>
                            </View>
                        </TouchableOpacity>
                        <Text style={style.heading1}>{this.state.data.heading1}</Text>
                        {editPreviousDetailButton}
                    </View>
                    <FlatList
                        contentContainerStyle={style.listStyle}
                        extraData={this.state}
                        data={this.state.docList}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({item, index}) => this.renderItem(item, index)}
                    />
                    <View style={style.buttonView}>
                        {button0}
                        {button1}
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}


function createSubmitReq() {
    let submitDocReq = [];
    this.state.docList.map(item => {
        let requestItem = {
            docKey: item.docKey,
            docId: item.docId,
            path: item.getUrl,
        };
        submitDocReq.push(requestItem);
    });
    return submitDocReq;
}

const style = StyleSheet.create({
    SafeArea: {
        flex: 1,
        backgroundColor: colors.white,
    },
    MainContainer: {
        backgroundColor: colors.white,
        flex: 1,
    },
    topView: {},
    headerView: {
        padding: spacing.spacing16,
        flexDirection: 'row',
    },
    headerViewTitle: {
        letterSpacing: 1,
        marginTop: spacing.spacing4,
        marginBottom: spacing.spacing12,
        width: '90%',
        color: colors.darkGrey,
    },
    minimiseImage: {
        marginRight: 0,
        height: spacing.spacing24,
        width: spacing.spacing24,
        marginLeft: spacing.spacing2,
    },
    heading1: {
        ...TextStyle.text_14_bold,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing64,
        marginBottom: spacing.spacing16,
    },
    separatorView: {
        marginTop: spacing.spacing24,
        height: 1,
        backgroundColor: '#e0e0e0',
    },
    listStyle: {
        justifyContent: 'space-between',
    },
    buttonView: {
        backgroundColor: colors.white,
        flexDirection: 'row',
        justifyContent: 'center',
        height: 88,
        marginLeft: 0,
        marginRight: 0,
        marginBottom: 0,
        marginTop: 0,
        borderRadius: spacing.spacing4,
        borderColor: colors.lightGrey,
        shadowOffset: {height: -4},
        shadowRadius: 2,
        shadowOpacity: 0.12,
        shadowColor: colors.black,
        elevation: 12,
    },
    buttonTouchArea: {
        marginLeft: spacing.spacing20,
        marginRight: spacing.spacing20,
        marginTop: spacing.spacing20,
        marginBottom: spacing.spacing20,
        flex: 1,
    },
    editPreviousDetailContainer: {
        paddingTop: spacing.spacing2,
        paddingBottom: spacing.spacing2,
        paddingLeft: spacing.spacing8,
        paddingRight: spacing.spacing8,
        backgroundColor: colors.lightBlue,
        borderRadius: spacing.spacing4,
        marginLeft: spacing.spacing16,
        alignSelf: 'flex-start',
    },
    editPreviousDetailStyle: {
        color: colors.blue028,
    },
    formButton: {
        paddingLeft: spacing.spacing32,
        paddingRight: spacing.spacing32,
        paddingTop: spacing.spacing12,
        paddingBottom: spacing.spacing12,
    },
    addMoreImagesText: {
        letterSpacing: 1,
        marginTop: spacing.spacing24,
        marginBottom: spacing.spacing12,
        marginLeft: spacing.spacing32,
        color: colors.blue028,
        alignSelf: 'flex-start',
    },
    optionalDocsText: {
        letterSpacing: 1,
        marginTop: spacing.spacing24,
        marginBottom: spacing.spacing12,
        marginLeft: spacing.spacing16,
        color: colors.darkGrey,
        alignSelf: 'flex-start',
    },
});
