import React, {useState} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import colors from '../Constants/colors';
import spacing from '../Constants/Spacing';
import {TextStyle} from '../Constants/CommonStyle';

const ChatbotVerticalImageText = props => {
    return (
        <View style={style.mainContainer}>
            <Image source={{uri: props.item.image}} style={style.imageStyle}/>
            <Text
                style={[TextStyle.text_16_normal]}>{props.item.heading2}</Text>
        </View>
    );
};

const style = StyleSheet.create({
    mainContainer: {
        flex: 1,
        marginLeft: spacing.spacing24,
        marginRight: spacing.spacing24,
    },
    imageStyle: {
        height: 236,
        resizeMode: "contain",
    },
});

export default ChatbotVerticalImageText;
