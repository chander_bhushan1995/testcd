import React, {useState} from 'react';
import {
    SafeAreaView,
    StyleSheet,
} from 'react-native';
import WebView from 'react-native-webview';
import spacing from '../Constants/Spacing';
import colors from '../Constants/colors';
import * as FormUploadCellActionType from '../Constants/FormUploadCellActionType';

const DocPreviewComponent = props => {
    const { navigation } = props;
    const [value, setValue] = useState(navigation.getParam('url', ""));
    return (
        <SafeAreaView style={styles.SafeArea}>
            <WebView
                style={styles.WebViewStyle}
                thirdPartyCookiesEnabled={true}
                source={{ uri: value }}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                scalesPageToFit={true}
                startInLoadingState={true} />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    SafeArea: {
        flex: spacing.spacing1,
        backgroundColor: colors.white,
    },
    WebViewStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: spacing.spacing1,
    }
});

export default DocPreviewComponent;
