import React, {useState} from 'react';
import {FlatList, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View, ScrollView} from 'react-native';
import ModelNavigationHeader from '../Components/ModelNavigationHeader';
import colors from '../Constants/colors';
import spacing from '../Constants/Spacing';
import {CommonStyle, TextStyle} from '../Constants/CommonStyle';
import CheckBox from 'react-native-check-box';
import InputBox from '../Components/InputBox';

const EditChatbotFormMultiSelect = props => {

    // EditChatbotFormMultiSelect.navigationOptions = ({navigation}) => {
    //     let data = navigation.getParam('data', null);
    //     return {
    //         title: 'Edit '.concat(data.question),
    //         headerTitleStyle: {
    //             color: colors.white,
    //         },
    //         headerStyle: {
    //             backgroundColor: '#2E8B57',
    //         },
    //         headerTintColor: colors.white,
    //     };
    // };

    let index = props.navigation.getParam('index', null);
    let onSavePress = props.navigation.getParam('onSavePress', null);
    let data = props.navigation.getParam('data', null);
    let array = [];
    if (data.response[0]?.value !== null && data.response[0]?.value !== undefined && data.response[0]?.value.length > 1) {
        array = data.response[0].value.split(',');
    }
    let optionsData = Object.assign([], data.options);
    let extraText = '';
    let arrayStartIndex = -1;
    let arrayEndIndex = -1;
    for (let i = 0; i < array.length; i++) {
        array[i] = array[i].trim();
        let matchingOption = optionsData.filter(data => data.text === array[i]);
        let comma = extraText.length > 0 ? ',' : '';
        if(matchingOption?.length === 0){
            extraText = extraText + comma +  array[i]
            arrayEndIndex = arrayEndIndex === -1 ? i : arrayEndIndex;
        }else{
            arrayStartIndex = arrayStartIndex === -1 ? i : arrayStartIndex;
        }
    }
    arrayEndIndex = arrayEndIndex === -1 ? array.length: arrayEndIndex;
    array = array.slice(arrayStartIndex, arrayEndIndex);
    optionsData.push({'text': '', 'type': 'Input'});
    optionsData = optionsData.filter(data => (data.type === 'Input' || data.type === 'Checkbox'));

    const [valueArray, setValueArray] = useState(array);
    const [inputMessage, setInputMessage] = useState(extraText);

    function onItemClick(item) {
        let newArray = [...valueArray];
        let index = newArray.indexOf(item.text);
        index === -1 ? newArray.push(item.text) : newArray.splice(index, 1);
        setValueArray(newArray);
    }

    function getValueArrayString() {
        let value = '';
        for (let i = 0; i < valueArray.length; i++) {
            let comma = value.length > 0 ? ', ' : '';
            value = value + comma + valueArray[i];
        }
        let comma = value.length > 0 ? ', ' : '';
        value = value + comma + inputMessage;
        return value;
    }

    function renderItem(item, index) {
        if (item.type === 'Checkbox') {
            return (
                <TouchableOpacity style={style.cellTouchArea} onPress={() => {
                    onItemClick(item);
                }}>
                    <View style={style.containerWithCheckBox}>
                        <Text
                            style={valueArray.indexOf(item.text) !== -1 ? [TextStyle.text_14_bold, style.textStyleSelected] : [TextStyle.text_14_normal, style.textStyleUnSelected]}>{item.text}</Text>
                        <CheckBox
                            style={style.checkboxStyle}
                            isChecked={valueArray.indexOf(item.text) !== -1}
                            checkedImage={<Image source={require('../images/ic_check_box.webp')}
                                                 style={style.checkbox}/>}
                            unCheckedImage={<Image source={require('../images/ic_check_box_outline_blank.webp')}
                                                   style={style.checkbox}/>}
                            onClick={() => {
                                onItemClick(item);
                            }
                            }
                        />
                    </View>
                </TouchableOpacity>
            );
        } else if (item.type === 'Input') {
            let additionalCommentsLabel=data?.additionalText ?? "Additional comments";
            return (
                <View style={style.inputBoxContainer}>
                    <InputBox
                        inputHeading={additionalCommentsLabel}
                        multiline={true}
                        value={inputMessage}
                        onChangeText={text => {
                            setInputMessage(text)
                        }}
                    />
                </View>
            );
        } else {
            return null;
        }
    }
    // let EditLabel=data?.editText ?? "Edit";

    return (
        <SafeAreaView style={style.SafeArea}>
            <ScrollView style={style.MainContainer}>
                <ModelNavigationHeader
                    headerText={data?.question}
                    backTapped={() => props.navigation.goBack()}
                />
                <FlatList
                    contentContainerStyle={style.listStyle}
                    extraData={valueArray}
                    data={optionsData}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={() => {
                        return <View style={CommonStyle.separator}/>;
                    }}
                    renderItem={({item, index}) => renderItem(item, index)}
                />
                <View style={style.buttonView}>
                    <TouchableOpacity onPress={() => {
                        data.response[0].value = getValueArrayString();
                        onSavePress(data, index);
                        props.navigation.goBack();
                    }}>
                        <Text style={style.submitButtonEnabled}>{data?.saveText ?? "Save"}</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};
export default EditChatbotFormMultiSelect;

const style = StyleSheet.create({
    SafeArea: {
        flex: 1,
        backgroundColor: colors.white,
    },
    MainContainer: {
        backgroundColor: colors.white,
        flex: 1,
    },
    listStyle: {
        marginTop: 20,
    },
    cellTouchArea: {
        marginTop: 6,
        marginBottom: 6,
    },
    containerWithCheckBox: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: spacing.spacing16,
        marginLeft: 16,
        marginRight: 16,
    },
    textStyleUnSelected: {
        color: colors.darkGrey,
    },
    textStyleSelected: {
        color: colors.charcoalGrey,
    },
    checkboxStyle: {
        width: 18,
        height: 18,
        marginRight: spacing.spacing4,
    },
    checkbox: {
        height: 16,
        width: 16,
    },
    buttonView: {
        backgroundColor: colors.white,
        justifyContent: 'flex-end',
        margin: spacing.spacing20,
    },
    submitButtonEnabled: {
        color: colors.blue,
        backgroundColor: colors.white,
        paddingTop: 14,
        paddingBottom: 14,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: colors.blue,
        overflow: 'hidden',
        fontSize: 14,
        fontFamily: 'Lato',
        fontWeight: '600',
        textAlign: 'center',
    },
    inputBoxContainer: {
        marginLeft: 16,
        marginRight: 16,
        marginTop: 28,
    },
    tapToEditStyle: {
        ...TextStyle.text_12_normal,
        color: colors.darkGrey,
    },
});
