import React, {Component} from "react";
import {FlatList, SafeAreaView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import ModelNavigationHeader from "../Components/ModelNavigationHeader";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";

export default class EditChatbotFormSingleSelect extends Component {

    state = {
        // isSubmitEnabled: false,
        data: this.props.navigation.getParam("data", null)
    };

    constructor(props) {
        super(props);
    }

    static navigationOptions = ({navigation}) => {
        let data = navigation.getParam("data", null);
        let editLable=data?.editText ?? "Edit";
        return {
            title: editLable.concat(data.question),
            headerTitleStyle: {
                color: colors.white,
            },
            headerStyle: {
                backgroundColor: "#2E8B57"
            },
            headerTintColor: colors.white,
        }
    };

    componentDidMount() {
        this.setState({
            // isSubmitEnabled: false,
            data: this.props.navigation.getParam("data", null)
        })

    }

    componentWillUnmount() {
        this.state.data = null;
        this.setState({
            // isSubmitEnabled: false,
            data: null
        })
    }

    render() {

        var index = this.props.navigation.getParam("index", null);
        var onSavePress = this.props.navigation.getParam("onSavePress", null);

        // if(this.data.response.length != 0) {
        //     this.setState({ isSubmitEnabled: true  });
        // }
        // let EditLabel=this?.state?.data?.editText ?? "Edit";
        return (
            <SafeAreaView style={style.SafeArea}>
                <View style={style.MainContainer}>
                    <ModelNavigationHeader
                        headerText={this.state.data.question}
                        backTapped={() => this.props.navigation.goBack()}
                    />
                    <FlatList
                        contentContainerStyle={style.listStyle}
                        extraData={this.state}
                        data={this.state.data.options}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({item, index}) => {
                            return <TouchableOpacity style={style.cellTouchArea} onPress={() => {
                                var data = this.state.data;
                                data.response[0].value = item.value.text;
                                this.setState({data: data})
                            }}>
                                <Text style={[style.textStyle, (item.value.text === this.state.data.response[0].value) && style.textStyleSelected]}>✓ {item.value.text}</Text>
                            </TouchableOpacity>;
                        }}
                    />
                    <View style={style.buttonView}>
                        <TouchableOpacity onPress={() => {
                            onSavePress(this.state.data, index);
                            this.props.navigation.goBack();
                        }}>
                            <Text style={style.submitButtonEnabled}>{this?.state?.data?.saveText ?? "Save"}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}
const style = StyleSheet.create({
    SafeArea: {
        flex: 1,
        backgroundColor: colors.white
    },
    MainContainer: {
        backgroundColor: colors.white,
        flex: 1,
    },
    listStyle: {
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
    },
    cellTouchArea: {
        marginTop: 6,
        marginBottom: 6,
    },
    textStyle: {
        padding: 12,
        borderRadius: 4,
        fontSize: 12,
        fontFamily: "Lato",
        fontWeight: "600",
        overflow: "hidden",
        borderWidth: 1,
        borderColor: colors.lightGrey2,
        color: colors.charcoalGrey,
        backgroundColor: colors.white,
        alignSelf: 'flex-start',
    },
    textStyleSelected: {
        borderColor: colors.blue,
        color: colors.blue,
    },
    buttonView: {
        backgroundColor: colors.white,
        justifyContent: "flex-end",
        margin: spacing.spacing20,
    },
    submitButtonEnabled: {
        color: colors.blue,
        backgroundColor: colors.white,
        paddingTop: 14,
        paddingBottom: 14,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: colors.blue,
        overflow: "hidden",
        fontSize: 14,
        fontFamily: "Lato",
        fontWeight: "600",
        textAlign: "center",
    }
});
