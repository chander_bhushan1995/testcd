import React, { Component } from "react";
import {
    FlatList,
    Image,
    Keyboard,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from "react-native";
import ModelNavigationHeader from "../Components/ModelNavigationHeader";
import colors from "../Constants/colors";
import InputBox from '../Components/InputBox'

export default class EditChatbotFormText extends Component {
    state = {
        data: { ...this.props.navigation.getParam("data", null) },
        showConstraintBox: false,
        keyboardHeight: 0,
        selectedIndex: 0,
        isSubmitEnabled: []
    };

    constructor(props) {
        super(props);
    }

    static navigationOptions = ({ navigation }) => {
        let data = { ...navigation.getParam("data", null) };
        return {
            title: "Edit ".concat(data.question),
            headerTitleStyle: {
                color: colors.white,
            },
            headerStyle: {
                backgroundColor: "#2E8B57"
            },
            headerTintColor: colors.white,
        }
    };

    componentDidMount() {
        this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
        this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
        data = { ...this.props.navigation.getParam("data", null) };
        let submitData = [];
        for (let i = 0; i < data.response.length; i++) {
            submitData.push((data.response[i].value.length >= data.options[i].minLength));
        }
        this.setState({
            data: data,
            isSubmitEnabled: submitData
        })
    }

    componentWillUnmount() {
        this.state.data = null;
        this.setState({
            data: null,
            isSubmitEnabled: null
        });
        this.keyboardWillShowSub.remove();
        this.keyboardWillHideSub.remove();
    }

    keyboardWillShow = (event) => {
        this.setState({ showConstraintBox: true, keyboardHeight: event.endCoordinates.height });
    };

    keyboardWillHide = (event) => {
        this.setState({ showConstraintBox: false, keyboardHeight: 0 });
    };

    render() {
        let selectedIndex = this.state.selectedIndex;
        let index = this.props.navigation.getParam("index", null);
        let onSavePress = this.props.navigation.getParam("onSavePress", null);
        let checkIcon = this.state.isSubmitEnabled[selectedIndex] ? require("../images/roundCheckEnable.webp") : require("../images/roundCheckDisable.webp");
        let constraintBox = (this.state.data.options[selectedIndex].minLength > 3) && this.state.showConstraintBox ? <View style={[style.constraintContainer, { bottom: this.state.keyboardHeight }]}>
            <Image style={style.checkIcon} source={checkIcon} />
            <Text style={[style.constraintText, this.state.isSubmitEnabled[selectedIndex] && style.constraintPassText]}> {this.state?.data?.options[selectedIndex]?.thresholdText}</Text>
        </View> : null;
        // let EditLabel=this?.state?.data?.editText ?? "Edit";
        return (
            <SafeAreaView style={style.SafeArea}>
                <View style={style.MainContainer}>
                    <ModelNavigationHeader
                        headerText={this.state.data.question}
                        backTapped={() => this.props.navigation.goBack()}
                    />
                    <FlatList
                        ref="list"
                        extraData={this.state}
                        data={this.state.data.response}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item, index }) => {
                            let maxLength = this.state.data.options[index].maxLength;
                            let minLength = this.state.data.options[index].minLength;
                            return <View style={style.textCell}>
                                <Text style={style.textCellLabel}>{item.label}</Text>
                                <InputBox
                                    containerStyle={style.textInputContainer}
                                    inputHeading={item.label}
                                    multiline={true}
                                    value={item.value}
                                    numberOfLines={3}
                                    maxLength={maxLength}
                                    editable={true}
                                    autofocus={true}
                                    onTouchStart={() => this.setState({ selectedIndex: index })}
                                    onChangeText={text => {
                                        let data = this.state.data;
                                        data.response[index].value = text.toString();
                                        let submitData = this.state.isSubmitEnabled;
                                        submitData[index] = text.length >= minLength;
                                        this.setState({ isSubmitEnabled: submitData, data: data })
                                    }}
                                />
                            </View>
                        }}
                    />
                    <View style={style.buttonView}>
                        <TouchableOpacity disabled={this.state.isSubmitEnabled.indexOf(false) !== -1}
                            style={style.buttonTouchArea} onPress={() => {
                                let data = this.state.data;
                                this.setState({ data: data });
                                onSavePress(this.state.data, index);
                                this.props.navigation.goBack();
                            }}>
                            <Text style={[style.submitButtonEnabled, (this.state.isSubmitEnabled.indexOf(false) !== -1) && style.submitButtonDisabled]}>{this?.state?.data?.saveText ?? "Save"}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                {constraintBox}
            </SafeAreaView>
        );
    }
}

const style = StyleSheet.create({
    SafeArea: {
        flex: 1,
        backgroundColor: colors.white
    },
    MainContainer: {
        backgroundColor: colors.white,
        flex: 1,
    },
    heading: {
        marginLeft: 0,
        marginRight: 26,
        color: colors.charcoalGrey,
        fontSize: 16,
        fontFamily: "Lato",
        fontWeight: "bold",
    },
    textCell: {
        marginLeft: 24,
        marginRight: 24,
        marginBottom: 8,
        flexDirection: "column",
    },
    textCellValue: {
        color: colors.charcoalGrey,
        fontSize: 15,
        fontFamily: "Lato",
    },
    textCellLabel: {
        color: colors.grey,
        fontSize: 12,
        fontFamily: "Lato",
    },
    buttonView: {
        backgroundColor: colors.white,
        justifyContent: "flex-end",
        height: 88,
        marginLeft: 0,
        marginRight: 0,
        marginBottom: 0,
        marginTop: 0,
    },
    buttonTouchArea: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 20,
    },
    submitButtonDisabled: {
        color: colors.lightGrey,
        backgroundColor: colors.white,
        paddingTop: 14,
        paddingBottom: 14,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: colors.lightGrey,
        overflow: "hidden",
        fontSize: 14,
        fontFamily: "Lato",
        fontWeight: "600",
        textAlign: "center",
    },
    submitButtonEnabled: {
        color: colors.blue,
        backgroundColor: colors.white,
        paddingTop: 14,
        paddingBottom: 14,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: colors.blue,
        overflow: "hidden",
        fontSize: 14,
        fontFamily: "Lato",
        fontWeight: "600",
        textAlign: "center",
    },
    constraintContainer: {
        backgroundColor: colors.soulitudeGrey,
        width: "100%",
        position: "absolute",
        flexDirection: "row",
        height: 36,
        paddingTop: 9,
        paddingBottom: 9,
        paddingLeft: 20,
        paddingRight: 20,
        justifyContent: "center"
    },
    checkIcon: {
        width: 18,
        height: 18,
    },
    constraintText: {
        fontSize: 12,
        fontFamily: "Lato",
        height: 18,
        lineHeight: 18,
        fontWeight: "500",
        marginLeft: 4,
    },
    characterText: {
        fontSize: 12,
        fontFamily: "Lato",
        height: 18,
        lineHeight: 19,
        marginRight: 0,
    },
    constraintPassText: {
        color: colors.seaGreen
    }
});
