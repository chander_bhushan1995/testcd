import React from 'react';
import {
    Image,
    NativeModules,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    SafeAreaView,
    ScrollView, Platform,
} from 'react-native';
import spacing from '../Constants/Spacing';
import colors from '../Constants/colors';
import FMIPTutorialExpandableListView from './FMIPTutorialExpandableListView';
import { parseJSON, deepCopy} from "../commonUtil/AppUtils";

const nativeBrigeRef = NativeModules.ChatBridge;

// let data = [{
//     heading: 'Turn off Find My iPhone via Mobile?',
//     expanded: false,
//     steps: [
//         {heading1: "Step 1", heading2: "Go to settings", image: ""},
//         {heading1: "Step 2", heading2: "Tap on your name", image: ""},
//         {heading1: "Step 3", heading2: "Tap on ‘icloud'", image: ""},
//         {heading1: "Step 4", heading2: "Tap on ‘Find My iPhone’", image: ""},
//         {heading1: "Step 5", heading2: "Turn Off ‘Find My iPhone’", image: ""}
//     ],
// },{
//     heading: 'Turn off Find My iPhone via Website?',
//     expanded: false,
//     steps: [
//         {heading1: "", heading2: "1. Sign in to your Apple ID on iCloud.com", image: ""},
//         {heading1: "", heading2: "2. Click on ‘Find iPhone’", image: ""},
//         {heading1: "", heading2: "3. Click on your device name on the center top of the screen and click on ‘close’ icon next to your device name", image: ""},
//         {heading1: "", heading2: "4. Click on ‘Remove’ to successfully turn off ‘Find My iPhone’ service", image: ""}
//     ],
// }
// ];

export default class FMIPTutorialComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = { data: null }
    }

    updateLayout = (index) => {
        const array = [...this.state.data];
        array[index]['expanded'] = !array[index]['expanded'];
        this.setState(() => {
            return {
                data: array,
            };
        });
    };

    componentDidMount() {
        nativeBrigeRef.getFMIPData(data => {
            let fmipParsedData = parseJSON(data);
            let fmipData = parseJSON(fmipParsedData['fmip_data']).fmipData;
            this.setState({data: fmipData}, () => {
                this.updateLayout(0);
            });
        });
    }

    render() {
        let expandableView = this.state.data !== null && this.state.data !== undefined ? this.state.data.map((item, key) =>
            (
                <FMIPTutorialExpandableListView heading={item.heading} onClickFunction={this.updateLayout.bind(this, key)} item={item}/>
            )) : null;
        return (
            <SafeAreaView style={styles.SafeArea}>
                <View style={{paddingBottom: spacing.spacing36,}}>
                    <View style={styles.topCrossContainer}>
                        <TouchableOpacity activeOpacity={0.5} onPress={this.onCrossButtonClicked}>
                            <Image source={require('../images/icon_cross.webp')} style={styles.imgCross}/>
                        </TouchableOpacity>
                    </View>
                    <ScrollView contentContainerStyle={styles.scrollViewStyle}>
                        {expandableView}
                    </ScrollView>
                </View>
            </SafeAreaView>
        );
    }

    onCrossButtonClicked = () => {
        this.props.navigation.pop();
    };
}

const styles = StyleSheet.create({
    SafeArea: {
        flex: spacing.spacing1,
        backgroundColor: colors.white,
    },
    topCrossContainer: {
        marginTop: spacing.spacing40,
        marginLeft: spacing.spacing24,
    },
    imgCross: {
        width: spacing.spacing24,
        height: spacing.spacing24,
    },
    scrollViewStyle: {
        paddingTop: spacing.spacing24,
        paddingBottom: spacing.spacing36,
    },
});
