import React, {Component} from 'react';
import colors from '../Constants/colors';
import spacing from '../Constants/Spacing';
import {FlatList, Image, StyleSheet, Text, TouchableOpacity, View,} from 'react-native';
import {TextStyle} from '../Constants/CommonStyle';
import ChatbotHorizontalImageText from './ChatbotHorizontalImageText';
import ChatbotVerticalImageText from './ChatbotVerticalImageText';

export default class FMIPTutorialExpandableListView extends Component {

    constructor() {
        super();
        this.state = {
            layoutHeight: 0,
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.item.expanded) {
            this.setState(() => {
                return {
                    layoutHeight: null,
                };
            });
        } else {
            this.setState(() => {
                return {
                    layoutHeight: 0,
                };
            });
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.layoutHeight !== nextState.layoutHeight) {
            return true;
        }
        return false;
    }

    renderItem = ({ item, index }) => {
        return (
            this.props.item.type === 'horizontal' ?
                <ChatbotHorizontalImageText item={item} index={index}></ChatbotHorizontalImageText> :
                <ChatbotVerticalImageText item={item}></ChatbotVerticalImageText>
        );
    };

    render() {
        let icon = this.props.item.expanded ? require('../images/up_arrow.webp') : require('../images/down_arrow.webp');
        return (
            <View style={styles.MainContainer}>
                <TouchableOpacity activeOpacity={0.8} onPress={this.props.onClickFunction} style={styles.headingView}>
                    <Text style={[TextStyle.text_16_normal, styles.headingText]}>{this.props.heading} </Text>
                    <Image source={icon}
                           style={styles.iconStyle}/>
                </TouchableOpacity>

                <View style={{height: this.state.layoutHeight, overflow: 'hidden'}}>
                    <FlatList
                        data={this.props.item.steps}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={this.renderItem}
                        ItemSeparatorComponent={() => {
                            return <View />;
                        }}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        justifyContent: 'center',
    },
    headingView: {
        marginBottom: spacing.spacing20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: colors.tagBgColor,
        alignItems: 'center',
    },
    headingText: {
        textAlign: 'left',
        marginLeft: spacing.spacing24,
        marginTop: spacing.spacing16,
        marginBottom: spacing.spacing16,
    },
    iconStyle: {
        width: 14,
        height: 8,
        resizeMode: 'contain',
        marginRight: spacing.spacing24,
    },
});
