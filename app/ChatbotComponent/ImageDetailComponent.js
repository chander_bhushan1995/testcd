import React, {useState, useEffect} from 'react';
import {
    Image,
    StyleSheet,
    TouchableOpacity,
    View,
    TouchableWithoutFeedback, Modal, SafeAreaView, Text, Platform,
} from 'react-native';
import spacing from '../Constants/Spacing';
import {TextStyle} from '../Constants/CommonStyle';
import colors from '../Constants/colors';
import Loader from '../Components/Loader';
import * as DocumentType from '../Constants/DocumentTypeConstant';

const ImageDetailComponent = props => {
    let isVisible = props.visible;
    const [visible, setVisible] = useState(isVisible);

    useEffect(() => {
            if (visible !== props.visible) {
                setVisible(isVisible);
            }
        },
    );

    function closePopup(isClosePopup) {
        if (isClosePopup == null || isClosePopup) {
            props.onImageDetailComponentClose();
        }
    }

    function onReUploadClicked() {
        if (Platform.OS === 'ios') {
            setTimeout(() => {
                props.onReUploadClicked();
            }, 100);
        } else {
            props.onReUploadClicked();
        }
        closePopup(true);
    }

    function _renderOutsideTouchable(onTouch) {
        const view = <View style={{flex: 1, width: '100%'}}/>;
        if (!onTouch) {
            return view;
        }
        return (
            <TouchableWithoutFeedback onPress={onTouch} style={{flex: 1, width: '100%'}}>
                {view}
            </TouchableWithoutFeedback>
        );
    }

    function renderView() {
        return <ImageDetailLayout
            imageDetailUrl={props.imageDetailUrl}
            isLoading={props.modalLoadingVisible}
            documentType={props.documentType}
            closePopup={(isClosePopup) => closePopup(isClosePopup)}
            onReUploadClicked={onReUploadClicked}
        />;

    }

    return (
        <Modal
            transparent={true}
            visible={visible}
            onRequestClose={() => {
                if (props.onClose != null) {
                    props.onClose();
                }
            }}>
            <View style={[{
                flex: 1,
                backgroundColor: '#000000AA',
            }]}>
                {_renderOutsideTouchable(() => {
                    if (props.onClose != null) {
                        props.onClose();
                    }
                })}
                {renderView()}
                {_renderOutsideTouchable(() => {
                    if (props.onClose != null) {
                        props.onClose();
                    }
                })}
            </View>
        </Modal>
    );
};

const ImageDetailLayout = props => {
    let docUri = {uri: props.imageDetailUrl};
    if (DocumentType.DOCUMENT === props.documentType) {
        docUri = require('../images/icon_file.webp');
    }

    function onCrossButtonClicked() {
        if (props.closePopup != null) {
            props.closePopup(true);
        }
    }

    function onReUploadClicked() {
        if (props.onReUploadClicked != null) {
            props.onReUploadClicked();
        }
    }

    const loaderView = () => {
        return props.isLoading ?
            <View style={{justifyContent: 'center', flex:1}}><Loader loaderStyle={{height: 50}} isLoading={props.isLoading} size={50} color={colors.blue028}/></View> : null;
    };

    const detailView = () => {
        return props.isLoading ? null : (<View style={{flex: 1}}>
            <View style={styles.docContainerStyle}>
                <Image style={styles.imageStyle} source={docUri}/>
            </View>
            <View style={{justifyContent: 'flex-end', flex: 1}}>
                <TouchableOpacity onPress={onReUploadClicked} style={styles.reUploadBackgroundStyle}>
                    <Text style={[TextStyle.text_14_normal, styles.reUploadTextStyle]}>Re-upload</Text>
                </TouchableOpacity>
            </View>
        </View>);
    };

    return (
        <SafeAreaView style={styles.SafeArea}>
            <View style={styles.topCrossContainer}>
                <TouchableOpacity activeOpacity={0.5} onPress={onCrossButtonClicked}>
                    <Image source={require('../images/icon_cross_overlay.webp')} style={styles.imgCross}/>
                </TouchableOpacity>
            </View>
            <View style={{backgroundColor: colors.red, width: '100%', height: 100}}>
            </View>
            <View style={{flex:1}}>
                {detailView()}
                {loaderView()}
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    SafeArea: {
        backgroundColor: 'transparent',
        opacity: 1,
        height: '100%',
        width: '100%',
        justifyContent: 'space-between',
    },
    topCrossContainer: {
        marginTop: spacing.spacing48,
        marginRight: spacing.spacing16,
        alignSelf: 'flex-end',
    },
    imgCross: {
        width: spacing.spacing24,
        height: spacing.spacing24,
    },
    docContainerStyle: {
        alignSelf: 'center',
        alignItems: 'center',
        width: '100%',
        paddingLeft: spacing.spacing28,
        paddingRight: spacing.spacing28,
    },
    imageStyle: {
        width: '100%',
        height: 300,
        resizeMode: 'contain',
        borderRadius: spacing.spacing8,
    },
    reUploadBackgroundStyle: {
        alignSelf: 'center',
        justifyContent: 'center',
        width: '50%',
        backgroundColor: colors.white,
        paddingTop: spacing.spacing14,
        paddingBottom: spacing.spacing14,
        paddingLeft: spacing.spacing56,
        paddingRight: spacing.spacing56,
        borderRadius: spacing.spacing24,
        marginBottom: spacing.spacing20,
    },
    reUploadTextStyle: {
        color: colors.blue,
        alignSelf: 'center',
    },
});

export default (ImageDetailComponent);
