import React from 'react';
import {
    Image,
    NativeModules,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    SafeAreaView,
    FlatList,
} from 'react-native';
import spacing from '../Constants/Spacing';
import colors from '../Constants/colors';
import {TextStyle} from '../Constants/CommonStyle';
import ChatbotHorizontalImageText from './ChatbotHorizontalImageText';
import { parseJSON, deepCopy} from "../commonUtil/AppUtils";

const nativeBridgeRef = NativeModules.ChatBridge;

// let data = [{
//     heading: 'Turn off Find My iPhone via Mobile?',
//     expanded: false,
//     steps: [
//         {heading1: "Step 1", heading2: "Go to settings", image: ""},
//         {heading1: "Step 2", heading2: "Tap on your name", image: ""},
//         {heading1: "Step 3", heading2: "Tap on ‘icloud'", image: ""},
//         {heading1: "Step 4", heading2: "Tap on ‘Find My iPhone’", image: ""},
//         {heading1: "Step 5", heading2: "Turn Off ‘Find My iPhone’", image: ""}
//     ],
// },{
//     heading: 'Turn off Find My iPhone via Website?',
//     expanded: false,
//     steps: [
//         {heading1: "", heading2: "1. Sign in to your Apple ID on iCloud.com", image: ""},
//         {heading1: "", heading2: "2. Click on ‘Find iPhone’", image: ""},
//         {heading1: "", heading2: "3. Click on your device name on the center top of the screen and click on ‘close’ icon next to your device name", image: ""},
//         {heading1: "", heading2: "4. Click on ‘Remove’ to successfully turn off ‘Find My iPhone’ service", image: ""}
//     ],
// }
// ];

export default class MILogoutTutorialComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = { data: null }
    }

    componentDidMount() {
        nativeBridgeRef.getMILogoutData(data => {
            let milogoutParsedData = parseJSON(data);
            let miLogoutData = parseJSON(milogoutParsedData['milogout_data']).miLogoutData;
            this.setState({
                data: miLogoutData[0]
            });
        });
    }

    renderItem = ({ item, index }) => {
        return (
            <ChatbotHorizontalImageText item={item} index={index}></ChatbotHorizontalImageText>
        );
    };

    render() {
        let headingView = null;
        let listView = null;
        if(this.state.data!== null && this.state.data !== undefined){
            headingView = <Text style={[TextStyle.text_16_normal, styles.headingText]}>{this.state.data.heading} </Text>
            listView = <FlatList
                style={styles.listStyle}
                data={this.state.data.steps}
                keyExtractor={(item, index) => index.toString()}
                renderItem={this.renderItem}
                ItemSeparatorComponent={() => {
                    return <View />;
                }}
            />
        }
        return (
            <SafeAreaView style={styles.SafeArea}>
                <View>
                    <View style={styles.topCrossContainer}>
                        <TouchableOpacity activeOpacity={0.5} onPress={this.onCrossButtonClicked}>
                            <Image source={require('../images/icon_cross.webp')} style={styles.imgCross}/>
                        </TouchableOpacity>
                    </View>
                    {headingView}
                    {listView}
                </View>
            </SafeAreaView>
        );
    }

    onCrossButtonClicked = () => {
        this.props.navigation.pop();
    };
}

const styles = StyleSheet.create({
    SafeArea: {
        flex: spacing.spacing1,
        backgroundColor: colors.white,
    },
    topCrossContainer: {
        marginTop: spacing.spacing40,
        marginLeft: spacing.spacing24,
    },
    imgCross: {
        width: spacing.spacing24,
        height: spacing.spacing24,
    },
    headingText: {
        marginTop: spacing.spacing36,
        marginLeft: spacing.spacing24,
    },
    listStyle: {
        marginLeft: spacing.spacing24,
        marginRight: spacing.spacing24,
        paddingBottom: spacing.spacing36,
    },
});
