import React, { Component } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image, Linking,FlatList } from "react-native";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";
import { TextStyle, CommonStyle, ButtonStyle } from "../Constants/CommonStyle";

// let data = {
// 'documentList': [
//     {
//         'docName': 'Please upload an image of Government issued ID proof',
//         'docKey': 'ID_PROOF',
//         'reason': 'Not clear 1'
//     },
//     {
//         'docName': 'To continue with your request, please upload the invoice copy.',
//         'docKey': 'INVOICE_IMAGE',
//         'reason': 'Not clear 3'
//     },
//     {
//         'docName': 'Please upload an image of the damaged part / area in focus',
//         'docKey': 'SCREEN_IMAGE',
//         'reason': 'Not clear 2'
//     }
// ]
// }

export default class RejectedDocumentList extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <View style={style.MainContainer}>
                <Text style={TextStyle.text_14_bold}>{this.props.heading}</Text>
                 <FlatList
                      data={this.props.data}
                      keyExtractor={(item, index) => index.toString()}
                      renderItem={this.renderItem} />
            </View>
        );

    }
    renderItem = ({ item, index }) => {
        return (
            <View style={style.listItemStyle}>
                <Text style={TextStyle.text_14_bold}>{index+1 +") "+item.docName}</Text>
                <Text style={TextStyle.text_12_normal}>{item.reason}</Text>
            </View>
        );
    };
}

const style = StyleSheet.create({
    MainContainer: {
        backgroundColor: colors.greyf2,
        padding: spacing.spacing12,
    },
    listItemStyle:{
        marginTop:spacing.spacing8,
    },
});