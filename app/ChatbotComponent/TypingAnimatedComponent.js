import React, { useState, useEffect } from "react";
import { StyleSheet, View } from "react-native";
import { createAnimatableComponent } from 'react-native-animatable';
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";
import Typing from "./../Components/Typing";
import { chatAnimationTimes } from "./../Chat/ChatUtils";

const AnimatableView = createAnimatableComponent(View);

const AnimationStage = {
    typing: 0,
    viewResize: 1,
    contentAppear: 2,
    animationDone: 3
}
const typingSize = { width: 64, height: 40 }

export default function TypingAnimatedComponent(props) {
    const { isAlreadyAnimated, isTyping, animationCompleted = () => { } } = props;
    const [animationStage, setAnimationStage] = useState(isAlreadyAnimated ? AnimationStage.animationDone : AnimationStage.typing);
    const [contentSize, setContentSize] = useState(typingSize);

    useEffect(() => {
        if (animationStage == AnimationStage.animationDone && !isAlreadyAnimated) {
            animationCompleted()
        }
    }, [animationStage]);

    const animationView = () => {
        switch (animationStage) {
            case AnimationStage.typing:
                return <AnimatableView
                    animation={"fadeInLeft"}
                    delay={chatAnimationTimes.typing.delay}
                    duration={chatAnimationTimes.typing.duration}
                    style={style.animatedView}
                    onAnimationEnd={() => setAnimationStage(isTyping ? AnimationStage.animationDone : AnimationStage.viewResize)}
                >
                    <Typing />
                </AnimatableView>
            case AnimationStage.viewResize:
                return <AnimatableView
                    animation={{ 0: { translateX: (typingSize.width - contentSize.width) / 2, translateY: (typingSize.height - contentSize.height) / 2, scaleX: (typingSize.width / contentSize.width), scaleY: (typingSize.height / contentSize.height) }, 1: { translateX: 0, translateY: 0, scaleX: 1, scaleY: 1 } }}
                    delay={chatAnimationTimes.resize.delay}
                    duration={chatAnimationTimes.resize.duration}
                    style={[style.blankView, contentSize]}
                    onAnimationEnd={() => setAnimationStage(AnimationStage.contentAppear)}
                />
            case AnimationStage.contentAppear:
                return <AnimatableView
                    animation={{ from: { opacity: 1 }, to: { opacity: 0 } }}
                    delay={chatAnimationTimes.contentAppear.delay}
                    duration={chatAnimationTimes.contentAppear.duration}
                    style={[style.blankView, contentSize]}
                    onAnimationEnd={() => setAnimationStage(AnimationStage.animationDone)}
                />
            default:
                return null
        }
    }

    return (<View style={style.mainView}>
        {animationView()}
        <View
            style={{ zIndex: 1, opacity: animationStage > AnimationStage.viewResize ? 1 : 0 }}
            onLayout={({ nativeEvent }) => setContentSize({ width: nativeEvent.layout.width, height: nativeEvent.layout.height })}
        >
            {props.children}
        </View>
    </View>);
}

const style = StyleSheet.create({
    mainView: {
        zIndex: 0,
        marginRight: spacing.spacing32,
    },
    animatedView: {
        flex: 1,
        position: "absolute",
        zIndex: 2,
        top: 0,
        left: 0,
    },
    blankView: {
        flex: 1,
        position: "absolute",
        zIndex: 2,
        top: 0,
        left: 0,
        borderRadius: 4,
        overflow: "hidden",
        backgroundColor: colors.white,
    },
});