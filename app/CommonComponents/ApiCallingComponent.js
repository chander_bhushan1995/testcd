import React, {Component} from "react";
import {
    BackHandler,
    Image,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import {TextStyle} from "../Constants/CommonStyle";
import spacing from "../Constants/Spacing";
import colors from "../Constants/colors";
import Loader from "../Components/Loader";

export default class ApiCallingComponent extends Component {


    constructor(props) {
        super(props);
    }


    onBackPress = () => {
        this.props.onClose();
        return true;
    };

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }


    render() {
        return (
            <SafeAreaView style={styles.SafeArea}>
                <View style={styles.container}>
                    <TouchableOpacity onPress={() => {
                        this.props.onClose();
                    }}>
                    <Image source={require("../images/icon_cross.webp")} style={styles.crossIconStyle}/>
                    </TouchableOpacity>
                    <View style={styles.descContainer}>
                        <Loader loaderStyle ={{height:50}} isLoading={true} size={50} color={colors.blue028}/>
                        <Text style={[TextStyle.text_16_normal, styles.headingStyle]}>Submitting request...</Text>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const
    styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: colors.white,
            padding: spacing.spacing16,
        },
        crossIconStyle: {
            height: 16,
            width: 16,
            resizeMode: "contain",
            alignSelf: 'flex-end',
            justifyContent: "flex-end"
        },
        headingStyle: {
            marginTop: spacing.spacing24,
            textAlign: 'center'
        },
        descContainer: {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginLeft: spacing.spacing16,
            marginRight: spacing.spacing16,
        },
        SafeArea: {
            flex: 1,
            backgroundColor: colors.white
        },
        secondaryButton: {
            marginTop: spacing.spacing24,
        },

    });
