import React, {Component} from 'react';
import {BackHandler, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {ButtonStyle, TextStyle} from '../Constants/CommonStyle';
import spacing from '../Constants/Spacing';
import colors from '../Constants/colors';
import ButtonWithLoader from '../CommonComponents/ButtonWithLoader';

export default class ApiErrorComponent extends Component {

	onBackPress = () => {
		this.props.onClose();
		return true;
	};

	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
	}

	componentDidMount() {
		BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
	}

	render() {
		const {errorType, errorMessage, isLoading, onClose, onTryAgainClick} = this.props
		let title = errorType ? 'Oh snap! Request not submitted due to poor mobile condition.' : 'Oh snap! something went wrong.';
		let subtitle = errorMessage ?? (errorType ? 'Skipped out on a few mobile health tests or entered incorrect details about your mobile? We suggest you to try again.' : 'Our servers are not responding at the moment. Please try again.');
		let buttonTitle = errorType ? 'Ok Got It' : 'Try Again';
		if(errorType === 1102){
			title = 'Oh Snap! We\'re unable to offer a price for your mobile due to its condition';
			subtitle = 'You can re-test your mobile\'s performance';
			buttonTitle = 'Try Again';
		}
		const buttonAction = () => {
			if (errorType && errorType !== 1102) {
				onClose();
			} else {
				onTryAgainClick();
			}
		};
		return (
			<SafeAreaView style={styles.SafeArea}>
				<View style={styles.container}>
					<TouchableOpacity onPress={onClose}>
						<Image source={require('../images/icon_cross.webp')} style={styles.crossIconStyle}/>
					</TouchableOpacity>
					<View style={styles.descContainer}>
						<Image source={require('../images/icon_api_error.webp')} style={styles.errorIconStyle}/>
						<Text style={[TextStyle.text_20_bold, styles.headingStyle]}>{title}</Text>
						<Text style={[TextStyle.text_14_normal, styles.subHeadingStyle]}>{subtitle}</Text>
						<View style={[styles.nextButtonStyle]}>
							<ButtonWithLoader
								isLoading={isLoading}
								enable={!isLoading}
								Button={{
									text: isLoading ? 'Submitting request…' : buttonTitle,
									onClick: buttonAction,
								}}/>
						</View>
						<TouchableOpacity style={styles.secondaryButton} onPress={onClose}>
							<Text style={ButtonStyle.TextOnlyButton}>I’ll do it later</Text>
						</TouchableOpacity>
					</View>
				</View>
			</SafeAreaView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: colors.white,
		padding: spacing.spacing16,
	},
	crossIconStyle: {
		height: 16,
		width: 16,
		resizeMode: 'contain',
		alignSelf: 'flex-end',
		justifyContent: 'flex-end',
	},
	errorIconStyle: {
		height: spacing.spacing48,
		width: spacing.spacing48,
		resizeMode: 'contain',
		alignSelf: 'center',
		justifyContent: 'center',
	},
	headingStyle: {
		marginTop: spacing.spacing36,
		textAlign: 'center',
	},
	subHeadingStyle: {
		marginTop: spacing.spacing12,
		textAlign: 'center',
	},
	submitButtonStyle: {
		padding: spacing.spacing12,
		borderRadius: 2,
		height: 48,
		alignItems: 'center',
	},
	descContainer: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		marginLeft: spacing.spacing16,
		marginRight: spacing.spacing16,
	},
	nextButtonStyle: {
		width: '100%',
		borderRadius: 2,
		marginTop: spacing.spacing56,
	},
	SafeArea: {
		flex: 1,
		backgroundColor: colors.white,
	},
	secondaryButton: {
		marginTop: spacing.spacing24,
	},

});
