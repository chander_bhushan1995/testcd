import React, {Component} from "react";
import InputBox from '../Components/InputBox'
import {
    FlatList,
    TouchableOpacity,
    SafeAreaView,
    Text,
    StyleSheet,
    Image,
    View,
} from "react-native";
import spacing from "../Constants/Spacing"
import {ButtonStyle,TextStyle,CommonStyle} from "../Constants/CommonStyle"
import colors from "../Constants/colors"

export default class AutoComplete extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [{modelName: "apple", icon: "1"}, {modelName: "moto", icon: "1"}, {
                modelName: "poco",
                icon: "1"
            }, {modelName: "lenove", icon: "1"}, {modelName: "apple", icon: "1"}],
            filteredData: [],
            inputValue: "",
        }
    }

    render() {
        return <SafeAreaView style={style.SafeArea}>
            <View style={style.MainContainer}>
                <View style={style.serachBarContainer}>
                    <InputBox
                        containerStyle={{justifyContent:'space-between',flex:1}}
                        value={this.state.inputValue}
                        editable={true}
                        onTouchStart={() => this.setState({selectedIndex: index})}
                        onChangeText={text => {
                            const regex = new RegExp(`${text.trim()}`, 'i');
                            this.setState({inputValue: text,filteredData: this.state.data.filter(modelName => modelName.modelName.search(regex) >= 0)});
                        }}
                    />
                    <TouchableOpacity style={style.cancelButtonContianer} onPress={() => {
                        this.setState({filteredData: [], inputValue: ""});
                    }}>
                        <Text style={[style.cancelButton, ButtonStyle.TextOnlyButton]}>Cancel</Text>
                    </TouchableOpacity>
                </View>
                <FlatList
                    contentContainerStyle={style.listStyle}
                    extraData={this.state}
                    ItemSeparatorComponent={() => {
                        return <View style={CommonStyle.separator} />
                    }}
                    data={this.state.filteredData}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({item, index}) => {
                        return <View style={style.listItemStyle}>
                            <Image source={require("../images/icon_call.webp")} style={style.modelLogo} />
                            <Text style={[style.modelName,TextStyle.text_16_normal]}>{item.modelName}</Text>
                        </View>
                    }}
                />
            </View>
        </SafeAreaView>
    }
}
const style = StyleSheet.create({
    SafeArea: {
        flex: 1,
        backgroundColor: colors.white
    },
    MainContainer: {
        backgroundColor: colors.white,
        flex: 1,
    },
    serachBarContainer: {
        flexDirection: 'row',
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
    },
    cancelButtonContianer: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginLeft: spacing.spacing16,
    },
    listStyle:{
        marginTop:spacing.spacing40,
        paddingLeft:spacing.spacing12,
        paddingRight:spacing.spacing16,
    },
    listItemStyle:{
        flexDirection: 'row',
    },
    cancelButton: {
        paddingTop:spacing.spacing16,
        paddingBottom:spacing.spacing16,
        paddingLeft:spacing.spacing12,
        paddingRight:spacing.spacing16,
    },
    modelName: {
        marginTop:spacing.spacing16,
        marginBottom:spacing.spacing16,
        marginLeft:spacing.spacing8
    },

    modelLogo: {
        marginTop:spacing.spacing16,
        marginBottom:spacing.spacing16,
        width: spacing.spacing32,
        height: spacing.spacing24,
    },

});
