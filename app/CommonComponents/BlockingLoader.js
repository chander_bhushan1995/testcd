import React from 'react';
import PropTypes from 'prop-types';
import {Modal, StyleSheet, Text, View} from 'react-native';
import {TextStyle} from "../Constants/CommonStyle";
import colors from '../Constants/colors'
import PlatformActivityIndicator from "../CustomComponent/PlatformActivityIndicator.js";


const transparent = 'transparent';
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:  "#000000AA",
    },
    background: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textContainer: {
        justifyContent: 'center',
        alignItems: 'center',

    },
    textContent: {
        marginTop:8,
        marginLeft:12
    },
    loadingContainer:{
        flexDirection:'row',
        backgroundColor:'#fff',
        paddingTop:16,
        paddingBottom:16,
        width:'50%',
        borderRadius:2,
        overflow: 'hidden'
    }
});

const ANIMATION = ['none', 'slide', 'fade'];
const SIZES = ['small', 'normal', 'large'];

export default class BlockingLoader extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            visible: this.props.visible,
            loadingMessage: this.props.loadingMessage
        };
    }

    close() {
        this.setState({ visible: false });
    }
    static propTypes = {
        cancelable: PropTypes.bool,
        color: PropTypes.string,
        animation: PropTypes.oneOf(ANIMATION),
        overlayColor: PropTypes.string,
        size: PropTypes.oneOf(SIZES),
        textContent: PropTypes.string,
        visible: PropTypes.bool,
        indicatorStyle: PropTypes.object,
        customIndicator: PropTypes.element,
        children: PropTypes.element,
        spinnerKey: PropTypes.string
    };

    static defaultProps = {
        visible: false,
        cancelable: true,
        textContent: '',
        animation: 'none',
        color: 'white',
        size: 'large', // 'normal',
        overlayColor: 'rgba(0, 0, 0, 0.25)'
    };

    static getDerivedStateFromProps(props, state) {
        const newState = {};
        if (state.visible !== props.visible) newState.visible = props.visible;
        return newState;
    }

    _handleOnRequestClose() {
        if (this.props.cancelable) {
            this.close();
        }
    }

    _renderDefaultContent() {
        return (
            <View style={styles.background}>
                <View style={styles.loadingContainer} >
                    <View style={{marginLeft:8}} >
                    <PlatformActivityIndicator
                        size={40}
                        color={colors.blue}
                    />
                    </View>
                    <Text style={[TextStyle.text_14_normal,styles.textContent]}>
                        {this.props.loadingMessage}
                    </Text>
            </View>
            </View>
        );
    }

    _renderSpinner() {
        if (!this.state.visible) return null;

        const spinner = (
            <View
                style={[styles.container, { backgroundColor: this.props.overlayColor }]}
                key={this.props.spinnerKey ? this.props.spinnerKey : `spinner_${Date.now()}`}
            >
                {this.props.children
                    ? this.props.children
                    : this._renderDefaultContent()}
            </View>
        );

        return (
            <Modal
                animationType={this.props.animation}
                onRequestClose={() => this._handleOnRequestClose()}
                supportedOrientations={['landscape', 'portrait']}
                transparent
                visible={this.state.visible}>
                {spinner}
            </Modal>
        );
    }

    render() {
        return this._renderSpinner();
    }
}