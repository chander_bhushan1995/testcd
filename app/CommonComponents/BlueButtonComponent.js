import React from 'react';
import {
    StyleSheet, Text, TouchableOpacity, View,
} from 'react-native';
import {ButtonViewStyle, TextStyle} from '../Constants/CommonStyle';
import colors from '../Constants/colors';
import spacing from '../Constants/Spacing';

export default function BlueButtonComponent(props) {
    const getViews = () => {
        return !props.disable ?
            <TouchableOpacity style={[props.style,props.isSelected ? ButtonViewStyle.buttonSelectedStyle : ButtonViewStyle.buttonUnselectedStyle]} onPress={() => props.onButtonClick(props.index)}>
                <Text style={[TextStyle.text_14_semibold, props.isSelected ? styles.selectedColor : styles.unselectedColor]}>{props.text}</Text>
            </TouchableOpacity> : <View style={[props.style, ButtonViewStyle.buttonDisabledStyle]}>
                <Text style={[TextStyle.text_14_semibold, styles.unselectedColor]}>{props.text}</Text>
            </View>;
    };
    return (
        getViews()
    );
}

const styles = StyleSheet.create({
    selectedColor: {
        color: colors.white,
    },
    unselectedColor: {
        color: colors.charcoalGrey,
    },
});
