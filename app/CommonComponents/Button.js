import React, {Component} from "react";
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {ButtonStyle} from "../Constants/CommonStyle"
import colors from "../Constants/colors"
import spacing from "../Constants/Spacing";

export default class Button extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let buttonStyle;
        let buttonDisable;
        if (!this.props.enable) {
            buttonDisable = true;
            buttonStyle = {backgroundColor: colors.lightGrey};
        } else {
            buttonDisable = false;
            buttonStyle = {backgroundColor: colors.blue};
        }
        return <TouchableOpacity
            style={styles.primaryButton}
            activeOpacity={.8}
            disabled={buttonDisable}
            onPress={() => {
                if (this.props.Button.onClick != null)
                    this.props.Button.onClick();
            }}>
            <View style={styles.container}>
                <Text style={[styles.text, ButtonStyle.primaryButtonTextWithoutLoader]}>{this.props.Button.text}</Text>
            </View>

        </TouchableOpacity>
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    primaryButton: {
        flex: 1,
        backgroundColor: colors.blue,
        borderRadius: 2,
        alignItems: 'center',
        paddingTop: 8,
        paddingBottom: 8,
    },
    text: {
        color: colors.color_FFFFFF,
        flex: 1,
        alignSelf: 'center',
        textAlign: 'center',
    }
});


