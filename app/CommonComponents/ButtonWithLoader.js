import React, {Component} from "react";
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {ButtonStyle} from "../Constants/CommonStyle"
import colors from "../Constants/colors"
import spacing from "../Constants/Spacing";
import Loader from "../Components/Loader";
import TextView from '../Catalyst/components/common/TextView';

export default class ButtonWithLoader extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let buttonStyle;
        let buttonDisable;
        if (this.props.isLoading) {
            buttonDisable = true;
            buttonStyle = {backgroundColor: colors.lightGrey};
        } else if (!this.props.enable) {
            buttonDisable = true;
            buttonStyle = {backgroundColor: colors.lightGrey};
        } else {
            buttonDisable = false;
            buttonStyle = {backgroundColor: colors.blue};
        }
        return <TouchableOpacity
            style={[ButtonStyle.BlueButton, styles.primaryButton, buttonStyle]}
            activeOpacity={.8}
            disabled={buttonDisable}
            onPress={() => {
                if (this.props.Button.onClick != null)
                    this.props.Button.onClick();
            }}>
            <View style={styles.container}>
                {this.props.isLoading ? <View style={styles.loader}>
                    <Loader
                        size={30}
                        color={colors.white}
                        isLoading={this.props.isLoading}/>
                </View> : null}
                {this.props.isHTMLText
                    ? <View style={{alignItems: 'center',justifyContent:'center',flex: 1}}><TextView style={{...styles.text, ...ButtonStyle.primaryButtonText}} texts={{value: this.props.Button.text,isHTML: true}}/></View>
                    : <Text style={[styles.text, ButtonStyle.primaryButtonText]}>{this.props.Button.text}</Text> }
            </View>
        </TouchableOpacity>
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
    },
    loader: {
        height: 20,
        width: 20,
        marginTop: spacing.spacing16,
        marginLeft: 8,
        justifyContent: 'flex-start'
    },
    primaryButton: {
        height: 48,
    },
    text: {
        flex: 1,
        justifyContent: 'center',
        // marginLeft: -40,
    },

});


