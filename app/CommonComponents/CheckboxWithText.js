import React, {useState} from 'react';
import {Image, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import colors from '../Constants/colors';
import spacing from '../Constants/Spacing';
import {TextStyle} from '../Constants/CommonStyle';
import CheckBox from 'react-native-check-box';

const CheckboxWithText = props => {
    const value = props.item.isSelected;
    let opacity = props.item.isEditable ? 1: 0.3;
    let checkbox = <CheckBox
        style={style.checkboxStyle}
        isChecked={value}
        checkedImage={<Image source={require('../images/ic_check_box.webp')} style={[style.checkbox, {opacity: opacity}]}/>}
        unCheckedImage={<Image source={require('../images/ic_check_box_outline_blank.webp')} style={style.checkbox}/>}
        disabled={!props.item.isEditable}
        onClick={() => {
            onItemClick();
        }
        }
    />;

    function onItemClick() {
        if(!(props.item.optionText === (props.currentBrand + ' ' + props.currentModel))) {
            props.onPressCheckbox(props.section, props.row)
        }
    }

    return (
        <View style={{height: 56}}>
            <TouchableOpacity disabled={!props.item.isEditable} onPress={() => {
                onItemClick();
            }}>
                <View style={style.containerWithCheckBox}>
                    {checkbox}
                    <Text
                        style={[TextStyle.text_16_normal, {
                            lineHeight: 24,
                            marginLeft: spacing.spacing16,
                            marginTop: spacing.spacing16,
                        }]}>{props.item.optionText}</Text>
                </View>
            </TouchableOpacity>
        </View>
    );
};

const style = StyleSheet.create({
    containerWithCheckBox: {
        flexDirection: 'row',
    },
    textStyleUnSelected: {
        color: colors.darkGrey,
    },
    textStyleSelected: {
        color: colors.charcoalGrey,
    },
    checkboxStyle: {
        width: 24,
        height: 24,
        marginLeft: spacing.spacing16,
        marginTop: spacing.spacing20,
    },
    checkbox: {
        height: 18,
        width: 18,
    },
});

export default CheckboxWithText;
