import React from 'react'
import {View, Text, StyleSheet, TextInput} from "react-native";
import spacing from "../../Constants/Spacing";
import {TextStyle} from "../../Constants/CommonStyle";
import colors from "../../Constants/colors";
import ButtonWithLoader from "../ButtonWithLoader";

const FlowRatingBottomSheet = props => {

    const primaryButton = props.primaryButton ?? {}
    const onTextChange = props.onTextChange ?? (()=>{})
    const initialText = props.initialText ?? ""

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Add Feedback</Text>
            <TextInput
                style={styles.TextInputStyleClass}
                underlineColorAndroid="transparent"
                placeholder={"Type Something in Text Area."}
                placeholderTextColor={"#9E9E9E"}
                numberOfLines={10}
                multiline={true}
                onChangeText={(text)=>{
                    onTextChange(text)
                }}
                defaultValue={initialText}
            />
            <View style={[styles.primaryButtonContainer, {
                marginBottom: spacing.spacing32,
                marginTop: spacing.spacing24
            }]}>
                <ButtonWithLoader
                    isLoading={false}
                    enable={true}
                    Button={{
                        text: primaryButton?.text ?? "",
                        onClick: primaryButton?.action,
                    }}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: spacing.spacing18,
        paddingTop: spacing.spacing24,
        paddingBottom: spacing.spacing16,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        borderRadius: 2,
    },
    title: {
        ...TextStyle.text_12_normal,
        color: colors.charcoalGrey,
    },
    TextInputStyleClass: {
        borderWidth: 1,
        borderColor: colors.grey,
        borderRadius: 2,
        backgroundColor: "#FFFFFF",
        height: 140,
        marginTop: spacing.spacing8,
        padding: 4
    },
    primaryButtonContainer: {
        marginTop: spacing.spacing12,
        marginBottom: spacing.spacing18,
    },
})

export default FlowRatingBottomSheet