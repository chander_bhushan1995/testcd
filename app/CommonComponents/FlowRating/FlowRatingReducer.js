import {PREPARE_BUYBACK_TAGS} from "../../Constants/ActionTypes";

//INITIAL STATE
export const flowRatingsInitialState = {
    tags: [],
};

const prepareBuybackTags = (responseData = []) => {
    let tags = []
    responseData?.forEach((item, index) => {
        let tagObject = {}
        tagObject.tag = item?.value
        tagObject.tagId = item?.valueId ?? index
        tagObject.isSelected = false
        tags.push(tagObject)
    })
    return tags
}


export const FlowRatingReducer = (state = initialState, action) => {
    switch (action.type) {
        case PREPARE_BUYBACK_TAGS:
            return {...flowRatingsInitialState, tags: prepareBuybackTags(action.data)}
            break;
        default:
            return flowRatingsInitialState
            break;
    }
}