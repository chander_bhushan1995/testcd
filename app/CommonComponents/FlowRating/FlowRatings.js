import React, {useContext, useEffect, useReducer, useRef, useState} from 'react';
import {
    View,
    Text,
    ScrollView,
    Animated,
    TouchableOpacity,
    Image,
    StyleSheet,
    NativeModules,
    Platform,
    StatusBar, Dimensions, BackHandler,
} from 'react-native';
import colors from '../../Constants/colors';
import spacing from '../../Constants/Spacing';
import BackButton from '../NavigationBackButton';
import images from '../../images/index.image';
import {
    CHAT,
    RATING_DESCRIPTION,
    RATING_HEADER,
    THANKS_USER,
    MOBILE_EXCHANGE_REQ_PLACED,
    MOBILE_RESALE_REQ_PLACED,
    MOBILE, EXCHANGE_PRICE, VIEW_DETAIL, PLATFORM_OS,
} from '../../Constants/AppConstants';
import {TextStyle} from '../../Constants/CommonStyle';
import RatingCard from './RatingCard';
import RightArrowButton from '../RightArrowButton';
import {getBuybackFeedbackTags, submitRating} from '../../Buyback/buybackActions/BuybackApiHelper';
import {BuybackContext, buybackManagerObj} from '../../Navigation/index.buyback';
import {PREPARE_BUYBACK_TAGS, TECHNICAL_ERROR_MESSAGE} from '../../Constants/ActionTypes';
import * as EventAttrs from '../../Constants/WebengageAttrKeys';
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';
import * as EventNames from '../../Constants/WebengageEvents';
import {BuybackEventLocation, buybackFlowType} from '../../Constants/BuybackConstants';
import RBSheet from "../../Components/RBSheet";
import FlowRatingBottomSheet from "./FlowRatingBottomSheet";
import useLoader from "../../CardManagement/CustomHooks/useLoader";
import BlockingLoader from "../BlockingLoader";
import {FlowRatingReducer, flowRatingsInitialState} from "./FlowRatingReducer";
import {getServedByValueFromFirstItem} from "../../Buyback/BuyBackUtils/BuybackUtils";

const nativeBrigeRef = NativeModules.ChatBridge;

export const RATE_STAGES = {
    NOT_RATED: "NOT_RATED",
    RATE_ON_APPSTORE: "RATE_ON_APPSTORE",
    GIVE_FEEDBACK: "GIVE_FEEDBACK",
    FEEDBACK_SUBMITTED: "FEEDBACK_SUBMITTED"
}

const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const HEADER_MIN_HEIGHT = 55;
let userName = null

function StatusBarPlaceHolder() {
    return (
        <View style={{
            width: '100%',
            height: STATUS_BAR_HEIGHT,
            backgroundColor: colors.seaGreen,
        }}>
            <StatusBar
                barStyle="light-content" backgroundColor={colors.seaGreen}
            />
        </View>
    );
}


const RatingBuybackCard = props => {

    const model = props.model ?? '';
    const price = props.price ?? '';
    const imageURL = props.image ?? '';
    const button = props.button ?? {};
    const cardTitle = props.subType === buybackFlowType.RETAILER_PICKUP ? MOBILE_EXCHANGE_REQ_PLACED:MOBILE_RESALE_REQ_PLACED;

    return (
        <View style={styles.ratingBuybackCard}>
            <Text style={styles.buybackCardTitle}>{cardTitle}</Text>
            <View style={{flexDirection: 'row', marginTop: spacing.spacing24}}>
                <View style={{flex: 2, marginHorizontal: spacing.spacing12}}>
                    <View style={{flexDirection: 'row', flex: 1}}>
                        <View style={{flex: 1}}>
                            <Text style={[TextStyle.text_12_normal, {color: colors.charcoalGrey}]}>{MOBILE}</Text>
                            <Text style={[TextStyle.text_14_bold, {color: colors.charcoalGrey}]}>{model}</Text>
                        </View>
                        <View style={{flex: 1}}>
                            <View style={{marginLeft: spacing.spacing24}}>
                                <Text
                                    style={[TextStyle.text_12_normal, {color: colors.charcoalGrey}]}>{EXCHANGE_PRICE}</Text>
                                <Text style={[TextStyle.text_14_bold, {color: colors.charcoalGrey}]}>₹ {price}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.buttonContainer}>
                        <RightArrowButton image={images.rightBlueArrow} text={button.text ?? ''}
                                          imageStyle={styles.rightArrowStyle}
                                          textStyle={styles.buttonText}
                                          style={{marginBottom: 0, alignSelf: 'flex-start'}}
                                          onPress={button.action ?? (() => {})}
                        />
                    </View>
                </View>
                <View style={{flex: 1}}>
                    <Image source={{'uri': imageURL}} style={styles.deviceImage}/>
                </View>
            </View>
        </View>
    );
};


const FlowRatings = props => {

    const [headerHeight, setHeaderHeight] = useState(120);
    const headerHeightRef = useRef(headerHeight).current;
    const animatedHeight = useRef(new Animated.Value(0)).current;
    const feedbackBottomSheet = useRef(null)
    const DialogViewRef = useRef(null)
    const [isBlockingLoader, blockingLoaderMessage, startBlockingLoader, stopBlockingLoader] = useLoader();
    const [reducerState, dispatch] = useReducer(FlowRatingReducer, flowRatingsInitialState)
    const [selectedRating, setRating] = useState(5);
    const [selectedTags, setSelectedTags] = useState(null)
    const [rateState, setRateStage] = useState(RATE_STAGES.NOT_RATED)
    const [isLoading, setIsLoading] = useState(false);
    const [feedbackText, setFeedbackText] = useState(null)
    const {Alert} = useContext(BuybackContext);

    const data = buybackManagerObj.getBuybackStatusDataFirstItem();
    const userName = props.navigation.getParam('userName')

    useEffect(() => {
        getBuybackRatingTags()
        nativeBrigeRef.refreshBuybackStatus();
        BackHandler.addEventListener('hardwareBackPress', onBack);
        return ()=>{
            BackHandler.removeEventListener('hardwareBackPress', onBack);
        }
    }, []);

    const onBack = () => {
        nativeBrigeRef.goBack();
    }

    const headerTranslateY = animatedHeight.interpolate({
        inputRange: [0, (headerHeightRef - HEADER_MIN_HEIGHT)],
        outputRange: [headerHeightRef, HEADER_MIN_HEIGHT],
        extrapolate: 'clamp',
        useNativeDriver: true,
    });

    const getBuybackRatingTags = () => {
        startBlockingLoader()
        getBuybackFeedbackTags(selectedRating, (response, error) => {
            stopBlockingLoader()
            if (response) {
                dispatch({type: PREPARE_BUYBACK_TAGS, data: response.data})
            } else {
                Alert.showAlert('Unable to proceed', error[0]?.errorMessage ?? TECHNICAL_ERROR_MESSAGE, {text: 'OK',onClick: ()=>{nativeBrigeRef.goBack()}});
            }
        })
    }

    const onRating = (value) => {
        setRating(value);
    };

    const onRatingSubmit = (selectedTags) => {
        let _quoteId = data?.quoteId;
        setSelectedTags(selectedTags)

        if (selectedRating < 4 && rateState != RATE_STAGES.GIVE_FEEDBACK) { // if user selected rating but feedback is pending
            setRateStage(RATE_STAGES.GIVE_FEEDBACK)
            return
        }
        setIsLoading(true);
        let selectedTagIDs = selectedTags.map((item)=>{return item.tagId})
        submitRating(_quoteId, selectedRating, selectedTagIDs, feedbackText, (response, error) => { // api call for submission of rating
            setIsLoading(false);
            setTimeout(()=>{stopBlockingLoader()},100)
            if (error == null) {
                let webEngageAttr = new Object()
                webEngageAttr[EventAttrs.LOCATION] = BuybackEventLocation.BUYBACK
                webEngageAttr[EventAttrs.ABB] = buybackManagerObj?.isABBFlow()
                webEngageAttr[EventAttrs.SERVED_BY] = getServedByValueFromFirstItem();
                webEngageAttr[EventAttrs.STAR_RATING] = selectedRating
                webEngageAttr[EventAttrs.TAGS] = selectedTags.map((item)=>{return item.tag}).join(',')
                logWebEnageEvent(EventNames.RATING_SUBMITTED, webEngageAttr)
                if (rateState == RATE_STAGES.NOT_RATED) {
                    setRateStage(RATE_STAGES.RATE_ON_APPSTORE)
                } else {
                    setRateStage(RATE_STAGES.FEEDBACK_SUBMITTED)
                }
            } else {
                Alert.showAlert('Unable to proceed', error[0]?.errorMessage ?? TECHNICAL_ERROR_MESSAGE, {text: 'OK'});
            }
        });
    };

    const onGiveFeedback = () => {
        feedbackBottomSheet.current.open()
    }

    const onSubmitFeedback = () => {
        feedbackBottomSheet.current.close()
        setTimeout(() => {
            startBlockingLoader('Please wait...')
        }, 100)
        onRatingSubmit(selectedTags)
    }

    return (
        <View style={styles.screen}>

            {/*status bar of screen*/}
            <StatusBarPlaceHolder/>


            {/*body of screen*/}
            <ScrollView
                contentContainerStyle={{
                    alignItems: 'center',
                    paddingTop: headerHeightRef,
                }}
                showsVerticalScrollIndicator={false}
                scrollEventThrottle={16}
                bounces={false}
                onScroll={Animated.event(
                    [
                        {
                            nativeEvent: {contentOffset: {y: animatedHeight}},
                        },
                    ],
                )}>

                <RatingCard
                    primaryButton={{text: 'Submit', action: onRatingSubmit}}
                    onRating={onRating}
                    ratingHeader={RATING_HEADER}
                    ratingDescription={RATING_DESCRIPTION}
                    isPrimaryBtnEnabled={true}
                    rateStage={rateState}
                    setRateStage={setRateStage}
                    tagData={reducerState?.tags ?? []}
                    rating={selectedRating}
                    isLoading={isLoading}
                    onGiveFeedback={onGiveFeedback}
                />

                <RatingBuybackCard
                    image={data?.deviceInfo?.image}
                    model={data?.deviceInfo?.deviceName}
                    price={data?.price}
                    subType={data?.subType}
                    button={{
                        text: VIEW_DETAIL, action: () => {
                            props.navigation.navigate('BuybackRequestSuccessTimeline', {});
                        },
                    }}
                />

            </ScrollView>

            <RBSheet
                ref={ref => {
                    feedbackBottomSheet.current = ref;
                }}
                duration={10}
                height={280}
                closeOnSwipeDown={false}
                closeOnPressMask={true}
            >
                <FlowRatingBottomSheet
                    primaryButton={{text: 'Submit', action: onSubmitFeedback}}
                    onTextChange={setFeedbackText}
                    initialText={feedbackText}
                />
            </RBSheet>

            <BlockingLoader visible={isBlockingLoader} loadingMessage={blockingLoaderMessage}/>

            {/*header comes here*/}
            <Animated.View style={[styles.header, {height: headerTranslateY}]} onLayout={(e) => {
                headerHeight ? setHeaderHeight(e.nativeEvent.layout.height) : null;
            }}>
                <View style={styles.collapsedHeader}>
                    <BackButton imageStyle={styles.backImageStyle} onPress={() => {
                        nativeBrigeRef.goBack();
                    }}/>
                    <TouchableOpacity style={styles.chatButtonContainer} onPress={() => {
                        nativeBrigeRef.openChat();
                    }}>
                        <Image source={images.chatSmall} style={styles.chatIcon}/>
                        <Text style={styles.chatButtonText}>{CHAT}</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.imageTextContainer}>
                    <Image source={images.result_success} style={styles.checkImage}/>
                    <Text style={styles.title}>{THANKS_USER(userName)}</Text>
                </View>
                <Text style={styles.description}>You will soon get a call from our partner store.</Text>
            </Animated.View>
        </View>
    );
};


const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    header: {
        backgroundColor: colors.seaGreen,
        paddingTop: spacing.spacing16,
        paddingHorizontal: spacing.spacing16,
        paddingBottom: spacing.spacing24,
        overflow: 'hidden',
        position: 'absolute',
        top: STATUS_BAR_HEIGHT,
        left: 0,
        right: 0,
        zIndex: 10,
    },
    collapsedHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    backImageStyle: {
        marginLeft: spacing.spacing0,
        marginRight: spacing.spacing0,
        marginVertical: spacing.spacing0,
        tintColor: colors.white,
    },
    chatButtonContainer: {
        flexDirection: 'row',
    },
    chatIcon: {
        height: 16,
        width: 16,
        resizeMode: 'contain',
        alignSelf: 'center',
        tintColor: colors.white,
    },
    chatButtonText: {
        ...TextStyle.text_12_normal,
        color: colors.white,
        marginLeft: 4,
        alignSelf: 'center',
    },
    imageTextContainer: {
        marginTop: spacing.spacing12,
        flexDirection: 'row',
    },
    checkImage: {
        height: 16,
        width: 16,
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    title: {
        ...TextStyle.text_14_bold,
        color: colors.white,
        marginLeft: spacing.spacing4,
        alignSelf: 'center',
    },
    description: {
        ...TextStyle.text_14_normal,
        color: colors.white,
        marginTop: spacing.spacing6,
    },
    ratingBuybackCard: {
        width: Dimensions.get('screen').width - spacing.spacing24,
        marginTop: spacing.spacing16,
        marginBottom: spacing.spacing16,
        // marginHorizontal: spacing.spacing12,
        borderRadius: spacing.spacing4,
        backgroundColor: colors.white,
        shadowOpacity: 0.1,
        shadowOffset: {
            width: 1,
            height: 1,
        },
        shadowColor: '#000000',
    },
    buybackCardTitle: {
        ...TextStyle.text_16_bold,
        color: colors.charcoalGrey,
        marginTop: spacing.spacing16,
        marginHorizontal: spacing.spacing12,
    },
    buttonContainer: {
        marginTop: spacing.spacing24,
        marginBottom: spacing.spacing16,
    },
    rightArrowStyle: {
        marginRight: 0, marginLeft: 4,
        width: 8,
        height: 12,
        resizeMode: 'stretch',
    },
    buttonText: {
        ...TextStyle.text_14_bold,
        color: colors.blue028,
    },
    deviceImage: {
        width: 62,
        height: 90,
        resizeMode: 'cover',
        marginBottom: 0,
        marginRight: spacing.spacing24,
        alignSelf: 'flex-end',
    },
});

export default FlowRatings;
