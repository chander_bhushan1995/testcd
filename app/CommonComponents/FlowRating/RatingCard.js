import {View, StyleSheet, Text, Image, TouchableOpacity, NativeModules, Platform} from 'react-native';
import React, {useEffect, useState} from 'react';
import spacing from '../../Constants/Spacing';
import {TextStyle} from '../../Constants/CommonStyle';
import colors from '../../Constants/colors';
import {
    ADD_FEEDBACK,
    GIVE_FEEdBACK, PLATFORM_OS,
    RATE_APPSTORE, RATE_NOW,
    RATING_DESCRIPTION,
    RATING_HEADER, RATING_NOTE,
    THANKS_FOR_FEEDBACK
} from '../../Constants/AppConstants';
import AirbnbRating from '../../CustomComponent/Rating/AirbnbRating';
import ButtonWithLoader from '../ButtonWithLoader';
import images from '../../images/index.image';
import {RATE_STAGES} from "./FlowRatings";
import {WebViewFor} from "../../AppForAll/MyAccount/Actions/MyAccountButtonActions";
import {BuybackEventLocation} from "../../Constants/BuybackConstants";

const nativeBrigeRef = NativeModules.ChatBridge;

const RatingCard = props => {
    const onRating = props.onRating ?? (() => {});
    const isLoading = props.isLoading ?? false;
    const isPrimaryBtnEnabled = props.isPrimaryBtnEnabled ?? true;
    const ratingHeader = props.ratingHeader ?? RATING_HEADER;
    const ratingDescription = props.ratingDescription ?? RATING_DESCRIPTION;
    const ratingNote = props.ratingNote ?? RATING_NOTE;
    const primaryBtn = props.primaryButton ?? {};
    const initialRating = props.rating ?? 0;
    const rateStage = props.rateStage ?? RATE_STAGES.NOT_RATED
    const onGiveFeedback = props.onGiveFeedback ?? (() => {
    })
    let confettiImage = images.reward_confetti
    let confettiText = RATE_APPSTORE
    let confettiButton = null

    const [tagData, setTagData] = useState(props.tagData ?? []);

    useEffect(() => {
        setTagData(props.tagData)
    }, [props.tagData])

    const updateTagData = (tagId) => {
        let data = [];
        tagData.map((obj) => {
            if (obj?.tagId == tagId) {
                obj.isSelected = !(obj.isSelected);
            }
            data.push(obj)
        })
        setTagData(data);
    };

    const rateOnAppStore = () => {
        nativeBrigeRef.showPopUpForStoreRating(BuybackEventLocation.BUYBACK,(params)=>{
            nativeBrigeRef.goBack();
        })
    }

    const TagView = () => {
        return tagData ? (<View style={styles.tagContainer}>
            {tagData.map((tag, key) => {
                    const tagText = tag.tag ?? '';
                    return (
                        <TouchableOpacity key={key}
                                          style={[styles.tagStyle, {backgroundColor: tag.isSelected ? colors.blue028 : colors.white}]}
                                          onPress={() => {
                                              updateTagData(tag.tagId);
                                          }}>
                            <Text
                                style={[TextStyle.text_12_bold, {color: tag.isSelected ? colors.white : colors.blue028}]}>{tagText}</Text>
                        </TouchableOpacity>
                    );
                },
            )}
        </View>) : null;
    };

    const getSelectedTags = () => {
        let selectedTags = tagData.filter((item,index)=>{return item.isSelected}) ?? [];
        return selectedTags
    }

    if (rateStage == RATE_STAGES.RATE_ON_APPSTORE) {
        confettiImage = images.reward_confetti
        confettiText = RATE_APPSTORE(Platform.OS == PLATFORM_OS.android ? 'play store' : 'app store')
        confettiButton = {text: RATE_NOW, action: rateOnAppStore}
    } else if (rateStage == RATE_STAGES.GIVE_FEEDBACK) {
        confettiImage = images.star_sad
        confettiText = GIVE_FEEdBACK
        confettiButton = {text: ADD_FEEDBACK, action: onGiveFeedback}
    } else if (rateStage == RATE_STAGES.FEEDBACK_SUBMITTED) {
        confettiImage = images.star_confetti_normal
        confettiText = THANKS_FOR_FEEDBACK
        confettiButton = null
    }

    return (
        <View style={styles.container}>
            {rateStage != RATE_STAGES.NOT_RATED
                ? <>
                    <Image style={styles.confetti} source={confettiImage}/>
                    <Text style={[TextStyle.text_14_normal, {
                        color: colors.charcoalGrey,
                        marginHorizontal: spacing.spacing16,
                        textAlign: 'center',
                        marginBottom: confettiButton ? spacing.spacing0 : spacing.spacing32
                    }]}>
                        {confettiText}
                    </Text>
                    {confettiButton
                        ? <View style={[styles.primaryButtonContainer, {
                            marginBottom: spacing.spacing32,
                            marginTop: spacing.spacing24
                        }]}>
                            <ButtonWithLoader
                                isLoading={false}
                                enable={true}
                                Button={{
                                    text: confettiButton.text,
                                    onClick: confettiButton.action,
                                }}
                            />
                        </View>
                        : null
                    }
                </>
                : <>
                    <Text style={styles.ratingTitle}>{ratingHeader}</Text>
                    <Text
                        style={[styles.ratingDescription, {marginBottom: spacing.spacing16}]}>{ratingDescription}</Text>
                    <AirbnbRating
                        count={5}
                        defaultRating={initialRating}
                        size={30}
                        showRating={false}
                        onFinishRating={onRating}
                    />
                    <Text style={styles.ratingDescription}>{ratingNote}</Text>
                    <TagView/>
                    <View style={styles.primaryButtonContainer}>
                        <ButtonWithLoader
                            isLoading={isLoading}
                            enable={isPrimaryBtnEnabled}
                            Button={{
                                text: primaryBtn?.text ?? '',
                                onClick: (() => {
                                    primaryBtn?.action(getSelectedTags(), null);
                                }),
                            }}
                        />
                    </View>
                </>
            }
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginTop: spacing.spacing16,
        elevation: 4,
        backgroundColor: colors.white,
        shadowOpacity: 0.1,
        width: '100%',
        shadowOffset: {
            width: 1,
            height: 1,
        },
        shadowColor: '#000000',
    },
    ratingTitle: {
        ...TextStyle.text_16_bold,
        color: colors.charcoalGrey,
        marginTop: 24,
        marginHorizontal: 16,
        textAlign: 'center',
    },
    ratingDescription: {
        ...TextStyle.text_14_normal,
        color: colors.grey,
        marginTop: spacing.spacing16,
        marginHorizontal: 16,
        textAlign: 'center',
    },
    primaryButtonContainer: {
        marginTop: spacing.spacing12,
        marginHorizontal: spacing.spacing16,
        marginBottom: spacing.spacing18,
    },
    confetti: {
        marginTop: spacing.spacing32,
        alignSelf: 'center',
        height: 64,
        width: 150,
        resizeMode: 'contain',
        marginBottom: 10,
    },
    tagContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: spacing.spacing16,
        marginBottom: spacing.spacing4,
        marginHorizontal: spacing.spacing32,
        justifyContent: 'center',

    },
    tagStyle: {
        marginRight: spacing.spacing8,
        marginBottom: spacing.spacing8,
        paddingHorizontal: spacing.spacing8,
        paddingVertical: spacing.spacing2,
        borderRadius: 2,
        borderColor: colors.blue028,
        borderWidth: 1,
    },
});

export default RatingCard;