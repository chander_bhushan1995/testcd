import {View} from "react-native";
import React, { useEffect, useReducer, useRef, useState } from "react";
import {FIREBASE_KEYS} from "../../Constants/AppConstants";
import useLoader from "../../CardManagement/CustomHooks/useLoader";
import Toast, {DURATION} from "../Toast";
import BlockingLoader from "../BlockingLoader";
import colors from "../../Constants/colors";
import {TextStyle} from "../../Constants/CommonStyle";
import DialogView from "../../Components/DialogView";
import CommonNavigator, {CommonMainNavigatorStacks} from "../Navigation/CommonNavigationStack";
import {NavigationActions} from "react-navigation";
import {applyMiddleware, combineReducers, createStore} from "redux";
import {createReactNavigationReduxMiddleware, reduxifyNavigator} from "react-navigation-redux-helpers";

import {connect, Provider} from "react-redux";
import { getFirebaseData } from "../../commonUtil/AppUtils";
import Loader from "../../Components/Loader";
import {getRouteAndDataForDeeplink} from "../../BuyPlan/Helpers/BuyPlanUtils";
import * as Actions from "../../Constants/ActionTypes";
import codePush from "react-native-code-push";
import {APIData, setAPIData, updateAPIData} from "../../../index";

export const CommonContext = React.createContext();

let apiArray = [];

const initialState = CommonNavigator.router.getStateForAction(CommonNavigator.router.getActionForPathAndParams("MainNavigator"));

const navReducer = (state = initialState, action) => {
    if (action.type === NavigationActions.SET_PARAMS) {
        try {
            const lastRoute = state.routes[0].routes.find(route => route.routeName === action.params.routeName);
            action.key = lastRoute.key;
        } catch (e) {
        }
    }
    const nextState = CommonNavigator.router.getStateForAction(action, state);
    // Simply return the original `state` if `nextState` is null or undefined.
    return nextState || state;
};

const appReducer = combineReducers({
    nav: navReducer,
});

const middleware = createReactNavigationReduxMiddleware(
    "MainNavigator",
    state => state.nav,
);

const App = reduxifyNavigator(CommonNavigator, "MainNavigator");
const mapStateToProps = (state) => ({
    state: state.nav,
});

const AppWithNavigationState = connect(mapStateToProps)(App);

export const store = createStore(
    appReducer,
    applyMiddleware(middleware),
);

const commonIndexInitialState = {
    gadgetsData: [],
    moduleFlowSequencing: {},
    isLoadedDataForNavigation: false,
};

const commonIndexReducer = (state = commonIndexInitialState, action) => {
    switch (action.type) {
        case Actions.BUY_PLAN_INDEX_API_DATA:
            return {
                ...state,
                gadgetsData: action?.data?.gadgetsData,
                moduleFlowSequencing: action?.data?.moduleSequencingData,
                isLoadedDataForNavigation: true,
            };
            break;
        default:
            return state;
            break;
    }
};

const Root:  () => React$Node = props => {
    setAPIData(props);

    const [isBlockingLoader, blockingLoaderMessage, startBlockingLoader, stopBlockingLoader] = useLoader();

    const toastRef = useRef(null);
    const alertRef = useRef(null);
    const [isLoading, setIsLoading] = useState(true);
    const [commonIndexState, commonIndexDispatch] = useReducer(commonIndexReducer, commonIndexInitialState);

    useEffect(() => {
        updateAPIData(props)
    }, [props])

    useEffect(() => {
        if (APIData?.deeplinkURL) {
            callAPIForDeeplink();
        } else if (APIData?.routeName) {
            let routeName = APIData?.routeName;
            let routeData = APIData?.data;
            initialDecidedRoute(routeName, routeData);
        }
        // TODO: KAG, why on every APIData change?. check logs and test.
    }, [APIData]);

    useEffect(() => {
        if (commonIndexState?.isLoadedDataForNavigation) {
            getRouteAndDataForDeeplink(APIData?.deeplinkURL ?? "", commonIndexState, initialDecidedRoute) ?? {};
        }
    }, [commonIndexState?.isLoadedDataForNavigation]);

    const callAPIForDeeplink = () => {
        apiArray.push(getGadgetsData());
        apiArray.push(getModuleSequencing());
        Promise.all(apiArray).then((values) => {
            apiArray = [];
            const [gadgetsData, moduleSequencing] = values;
            commonIndexDispatch({
                type: Actions.BUY_PLAN_INDEX_API_DATA,
                data: {gadgetsData: gadgetsData, moduleSequencingData: moduleSequencing},
            });
        });
    };

    const getGadgetsData = async () => {
        return await new Promise((resolve, reject) => {
            getFirebaseData(FIREBASE_KEYS.SERVICE_GADGETS, (data) => {
                resolve(data);
            });
        });
    };

    const getModuleSequencing = async () => {
        return await new Promise((resolve, reject) => {
            getFirebaseData(FIREBASE_KEYS.MODULE_FLOW_SEQUENCING, (data) => {
                resolve(data);
            });
        });
    };

    const showToast = (message, timeout = DURATION.LENGTH_SHORT) => toastRef.current.show(message, timeout, true);

    const showAlert = (title, alertMessage, primaryButton = {
        text: "Yes", onClick: () => {
        },
    }, secondaryButton, checkBox) => {
        alertRef.current.showDailog({
            title: title,
            message: alertMessage,
            primaryButton: primaryButton,
            secondaryButton: secondaryButton,
            checkBox: checkBox,
            cancelable: true,
            onClose: () => {
            },
        });
    };

    const showAlertWithImage = (image, title, alertMessage, primaryButton = {
        text: "Ok", onClick: () => {
        },
    }, secondaryButton, checkBox) => {
        alertRef.current.showDailog({
            title: title,
            message: alertMessage,
            primaryButton: primaryButton,
            secondaryButton: secondaryButton,
            checkBox: checkBox,
            cancelable: false,
            imageUrl: image,
            isCrossEnabled: true,
            onClose: () => {
            },
        });
    };

    const initialDecidedRoute = (routeName, routeData) => {
        setIsLoading(false);
        if(!routeName){
            return
        }
        if (CommonMainNavigatorStacks.includes(routeName)) {
            initialState.routes[0].routes[0].params = routeData;
            initialState.routes[0].routes[0].routeName = routeName;
        } else {
            initialState.routes[0].params = routeData;
            initialState.routes[0].routeName = routeName;
        }
    };

    return (
        <Provider store={store}>
            <CommonContext.Provider value={{
                Toast: {showToast: showToast},
                Alert: {showAlert: showAlert, showAlertWithImage: showAlertWithImage, ref: alertRef},
                BlockingLoader: {startLoader: startBlockingLoader, stopLoader: stopBlockingLoader},
            }}>
                <BlockingLoader visible={isBlockingLoader} loadingMessage={blockingLoaderMessage}/>

                {!isLoading
                    ? <AppWithNavigationState/>
                    : <View>
                        <Loader isLoading={isLoading}/>
                    </View>
                }
                <Toast ref={toastRef}
                       style={{backgroundColor: colors.color_404040}}
                       position="bottom"
                       textStyle={{...TextStyle.text_14_bold, color: colors.white}}
                />
                <DialogView ref={alertRef}/>
            </CommonContext.Provider>

        </Provider>
    );
}
export default codePush(Root)
