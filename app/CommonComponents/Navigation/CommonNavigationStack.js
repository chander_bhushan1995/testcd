import { createStackNavigator } from "react-navigation";


import ServiceDetailScreen from "../../BuyPlan/Screens/ServiceDetailScreen";
import colors from "../../Constants/colors";

import PinCodeScreen from "../../BuyPlan/Screens/PinCodeScreen";
import NotifyMeFormScreen from "../../BuyPlan/Screens/NotifyMeFormScreen";
import WebViewComponent from "../../CardManagement/PayBill/WebViewComponent";
import SODPlanDetailScreen from "../../BuyPlan/Screens/SODPlanDetailScreen";
import ProductInfoInputScreen from "../../BuyPlan/Screens/ProductInfoInputScreen";
import BrandSelectionScreen from "../../BuyPlan/Screens/BrandSelectionScreen";
import TermsAndConditionsComponent from "../../Buyback/BuybackComponent/TermsAndConditionsComponent";
import PlanListingScreen from "../../BuyPlan/Screens/PlanListingScreen";
import ApplianceCountSelectionScreen from "../../BuyPlan/Screens/ApplianceCountSelectionScreen";
import BottomSheetScreen from "../../BuyPlan/Screens/BottomSheetScreen";
import VideoViewComponent from "../../BuyPlan/Screens/VideoViewComponent";
import OnGoingServicesScreen from "../../MembershipTab/Screens/OnGoingServicesScreen";

export const CommonMainNavigatorStacks = ["ProductInfoInputScreen", "ServiceDetailScreen", "TermsAndConditionsComponent", "PinCodeScreen", "ApplianceCountSelectionScreen", "SODPlanDetailScreen", "SODPProductServiceScreen", "SODProductServiceScreen", "WebViewComponent", "KnowMoreScreen", "PlanListingScreen","OnGoingServicesScreen"];

const MainNavigator = createStackNavigator({
    ServiceDetailScreen: {
        screen: ServiceDetailScreen,
    },
    ProductInfoInputScreen: {
        screen: ProductInfoInputScreen,
    },
    TermsAndConditionsComponent: {
        screen: TermsAndConditionsComponent,
    },
    WebViewComponent: {
        screen: WebViewComponent,
    },
    SODPlanDetailScreen: {
        screen: SODPlanDetailScreen,
    },
    SODProductServiceScreen: {
        screen: SODPlanDetailScreen,
    },
    KnowMoreScreen: {
        screen: ServiceDetailScreen,
    },
    PlanListingScreen: {
        screen: PlanListingScreen,
    },
    ApplianceCountSelectionScreen: {
        screen: ApplianceCountSelectionScreen,
    },
    PinCodeScreen: {
        screen: PinCodeScreen,
    },
    OnGoingServicesScreen: {
        screen: OnGoingServicesScreen
    }
}, {
    initialRouteName: "ServiceDetailScreen",
    gesturesEnabled: false,
    navigationOptions: {
        headerStyle: {
            backgroundColor: colors.white,
            flex:1,
            elevation: 4,
            shadowOpacity: 0.1,
            shadowOffset: {
                width: 1,
                height: 1,
            },
            shadowColor: "#000000",
        },
        headerTitleStyle: {
            color: colors.charcoalGrey,
            flex:1
        },
        headerTintColor: colors.blue,
    },
});

const CommonNavigator = createStackNavigator({
    MainNavigator: {
        screen: MainNavigator,
    },
    NotifyMeFormScreen: {
        screen: NotifyMeFormScreen,
    },
    BottomSheetScreen: {
        screen: BottomSheetScreen,
    },
    BrandSelectionScreen: {
        screen: BrandSelectionScreen,
    },
    VideoViewComponent: {
        screen: VideoViewComponent,
    },
}, {
    mode: "modal",
    headerMode: "none",
    transparentCard: true,
    cardStyle: {
        backgroundColor: "transparent",
        opacity: 1,
    },
    transitionConfig: () => ({
        containerStyle: {
            backgroundColor: "transparent",
        },
    }),
});

export default CommonNavigator;
