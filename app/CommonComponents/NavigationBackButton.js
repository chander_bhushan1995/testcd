import {Image, TouchableOpacity, StyleSheet} from 'react-native';
import Images from '../images/index.image';
import React from 'react';

const BackButton = ({onPress, imageStyle}) => (
    <TouchableOpacity onPress={
        () => {
            if (onPress) {
                onPress();
            }
        }
    }>
        <Image
            source={Images.backImage}
            style={[styles.backImageStyle,imageStyle]}
        />
    </TouchableOpacity>
);


const styles = StyleSheet.create({
    backImageStyle: {
        height: 30,
        width: 30,
        marginLeft: 9,
        marginRight: 12,
        marginVertical: 12,
        resizeMode: 'contain',
    },
});

export default BackButton;
