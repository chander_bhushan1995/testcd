import React from "react";
import {Dimensions} from 'react-native';
import Carousel from "react-native-snap-carousel";
import spacing from '../Constants/Spacing';

export default function OACarousel(props) {
  let deviceWidth = Dimensions.get("window").width;
  let scaleFactor = props.data.length == 1 ? 0.90 : 0.75;
  return (
    <Carousel
      ref={c => {
        this._carousel = c;
      }}
      data={props.data}
      renderItem={_renderCarouselItem}
      sliderWidth={deviceWidth * 0.9}
      itemWidth={deviceWidth * scaleFactor}
      contentContainerCustomStyle={{ paddingLeft: 0}}
      // firstItem={1}
      // fadeDuration={1}
      // parallaxFactor={1}
      // dotsLength={4}
      // currentScrollPosition={0}
      // lockScrollTimeoutDuration={1}
      // autoplay={true}
      // loop={true}
    />
  );
}
const _renderCarouselItem = ({ item, index }) => {
  return item;
};
