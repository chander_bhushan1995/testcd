import {Dimensions, FlatList, StyleSheet, View} from "react-native";
import React from "react";
import spacing from "../Constants/Spacing";
import {TextStyle} from "../Constants/CommonStyle";
import colors from "../Constants/colors";
import {getComponent} from "../AppForAll/HomeScreen/Components/ComponentsFactory";

const OAHorizontalFlatList = (props) => {
    let data = props.data;
    let renderItem = props.renderItem;

    return (
        <FlatList
            showsHorizontalScrollIndicator={false}
            style={styles.listStyle}
            nestedScrollEnabled={true}
            ItemSeparatorComponent={() => {
                return <View style={{width: 12, backgroundColor: 'transparent'}}/>
            }}
            contentContainerStyle={[styles.listContentContainerStyle]}
            horizontal={true}
            data={data}
            renderItem={renderItem}
            keyExtractor={(item, index) => {
                return index.toString()
            }}
        />
    );
}

const styles = StyleSheet.create({
    listStyle: {
        marginVertical: spacing.spacing16,
    },
    listContentContainerStyle: {
        paddingRight: spacing.spacing20
    }

});
export default OAHorizontalFlatList;

