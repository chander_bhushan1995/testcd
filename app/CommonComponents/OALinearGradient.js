import React from "react";
import LinearGradient from "react-native-linear-gradient";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";
export default function OALinearGradient(props) {
  return (
    <LinearGradient
      useAngle={true}
      angle={props.degree ?? 225.64}
      colors={props.colors ?? [colors.color_FF505E, colors.color_CA236A]}
      style={props.style}
    >
      {props.child}
    </LinearGradient>
  );
}
