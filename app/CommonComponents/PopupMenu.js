import React, { Component} from 'react'
import { View, UIManager,Image, findNodeHandle, TouchableOpacity } from 'react-native'
import {OAImageSource} from "../Constants/OAImageSource";

const ICON_SIZE = 24

export default class PopupMenu extends Component {
    constructor (props) {
        super(props)
        this.state = {
            icon: null
        }
    }

    onError () {
    }

    onPress = () => {
        if (this.state.icon) {
            UIManager.showPopupMenu(
                findNodeHandle(this.state.icon),
                this.props.actions,
                this.onError,
                this.props.onPress
            )
        }
    }

    render () {
        return (
            <View>
                <TouchableOpacity onPress={this.onPress}>
                    <Image ref={this.onRef}
                        source={OAImageSource.more_menu.source} style={OAImageSource.more_menu.dimensions}  />
                    {/*<Icon
                        name='more-vert'
                        size={ICON_SIZE}
                        color={'grey'}
                        ref={this.onRef} />*/}
                </TouchableOpacity>
            </View>
        )
    }

    onRef = icon => {
        if (!this.state.icon) {
            this.setState({icon})
        }
    }
}
