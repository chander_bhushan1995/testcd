import React from "react";
import {View, Animated, Alert, StyleSheet} from "react-native";

export default function ProgressBar(props) {
    let completedPercent = (props.data?.completed ?? 10).toString() + "%";
    return (
        <View
            style={[
                {backgroundColor: (props.theme === colors.color_FFFFFF ? colors.color_F6F6F6 : colors.whiteDim)},
                props.style?.containerStyle
            ]}
        >
            <Animated.View
                style={[styles.AnimatedViewStyle, props.style?.AnimatedViewStyle, {width: completedPercent}]}
            >
                <View
                    style={[styles.progressBarStyle, props.style?.progressBarStyle]}
                />
            </Animated.View>
        </View>
    );
}

import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";

const styles = StyleSheet.create({
    AnimatedViewStyle: {
        width: "30%",
        height: spacing.spacing4,
        borderRadius: spacing.spacing2,
        overflow: "hidden"
    },
    progressBarStyle: {
        backgroundColor: colors.white,
        flex: 1
    }
});
