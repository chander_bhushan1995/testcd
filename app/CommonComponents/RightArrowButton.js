import React from 'react';
import { View, Image, TouchableOpacity, StyleSheet, Text } from 'react-native';
import { TextStyle } from '../Constants/CommonStyle';
import colors from '../Constants/colors';
import spacing from "../Constants/Spacing";
import Images from '../images/index.image'

const RightArrowButton = props => {
    const image = props.image || Images.rightBlueArrow
    const imageStyle = props.imageStyle || styles.rightArrowStyle

    return (
        <View style={[styles.touchableContainer,props.style]}>
            <TouchableOpacity onPress={props.onPress || null}>
                <View style={styles.buttonContainer}>
                    <Text style={[TextStyle.text_18_bold, { color: colors.blue028, marginBottom: 3 },props.textStyle]}>{props.text}</Text>
                    <Image style={imageStyle} source={image} />
                </View>
            </TouchableOpacity>
        </View>
    );
}

RightArrowButton.defaultProps = {
    style:{},
    textStyle:{},
    text:'',
}

const styles = StyleSheet.create({
    rightArrowStyle: {
        width: 8,
        height: 12,
        marginLeft: spacing.spacing4,
        marginRight: spacing.spacing16,
        resizeMode: "stretch"
    },
    touchableContainer: {
        flexDirection: "row",
        alignSelf: 'flex-end',
        marginBottom: 16,
        bottom: 0,
    },
    buttonContainer: {
        flexDirection: "row",
        alignItems: "center"
    }
})


export default RightArrowButton;
