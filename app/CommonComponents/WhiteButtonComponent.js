import React, {Component} from "react";
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {ButtonStyle, TextStyle} from "../Constants/CommonStyle"
import colors from "../Constants/colors"
import spacing from "../Constants/Spacing";

export default class WhiteButtonComponent extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <TouchableOpacity
            style={styles.primaryButton}
            disabled={false}
            onPress={() => {
                if (this.props.Button.onClick != null)
                    this.props.Button.onClick();
            }}>
            <View style={styles.container}>
                <Text style={[styles.text]}>{this.props.Button.text}</Text>
            </View>

        </TouchableOpacity>
    }
}
const styles = StyleSheet.create({
    container: {
        alignSelf: 'center',
    },

    primaryButton: {
        flex: 1,
        backgroundColor: colors.white,
        borderRadius: 2,
        borderColor:colors.lightGrey2,
        borderWidth:1,
        justifyContent:'center',
        width: 106,
        height: 48,
    },
    text: {
        ...TextStyle.text_16_bold,
        color: colors.blue,
    }
});


