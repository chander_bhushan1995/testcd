import React, { Component } from "react";
import { StyleSheet, TouchableOpacity, View, Text, Image } from "react-native";
import { TextStyle, ButtonStyle, CommonStyle } from "../../Constants/CommonStyle";
import colors from "../../Constants/colors";
import spacing from "../../Constants/Spacing";
import SeparatorView from '../../CustomComponent/separatorview';
import CheckBox from "react-native-check-box";

export default class AlertLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isChecked: props.propsData?.checkBox?.selected ?? false
    }
  }

  render() {
    let titleLayout = this.props.propsData.title ? (
      <Text style={[styles.titleLayout, TextStyle.text_16_bold]}>
        {this.props.propsData.title}
      </Text>
    ) : null;
    let descLayout = this.props.propsData.message ? (
      <Text style={[styles.descLayout, TextStyle.text_14_normal]}>
        {this.props.propsData.message}
      </Text>
    ) : null;
    let checkBox = this.props.propsData.checkBox ? (<TouchableOpacity style={styles.checkboxViewContainer} onPress={()=>this.setState({isChecked: !this.state.isChecked})}>
      <CheckBox
        style={styles.checkboxContainer}
        isChecked={this.state.isChecked}
        onClick={() => this.setState({isChecked: !this.state.isChecked})}
        checkedImage={<Image source={require("../../images/ic_check_box.webp")} style={styles.checkbox} />}
        unCheckedImage={<Image source={require("../../images/ic_check_box_outline_blank.webp")} style={styles.checkbox} />}
        checkedCheckBoxColor={colors.blue}
        uncheckedCheckBoxColor={colors.grey}
      />
      <Text style={[styles.textContainer, {color: this.state.isChecked ? colors.grey : colors.lightGrey}]}>{this.props.propsData.checkBox.text}</Text>
    </TouchableOpacity>) : null;
    let primaryButton = this.props.propsData.primaryButton ? (
      <TouchableOpacity
        style={[styles.primaryButton, !this.props.propsData.secondaryButton && { width: "100%" }]}
        activeOpacity={0.8}
        onPress={() => {
          if (this.props.propsData.primaryButton.onClick != null)
            this.props.propsData.primaryButton.onClick(this.state.isChecked);
          if (this.props.closePopup != null) this.props.closePopup(true);
        }}
      >
        <Text style={ButtonStyle.TextOnlyButton}>
          {this.props.propsData.primaryButton.text}
        </Text>
      </TouchableOpacity>
    ) : null;
    let secondaryButton = this.props.propsData.secondaryButton ? (
      <TouchableOpacity
        style={[styles.secondaryButton]}
        activeOpacity={0.8}
        onPress={() => {
          if (this.props.propsData.secondaryButton.onClick != null)
            this.props.propsData.secondaryButton.onClick(this.state.isChecked);
          if (this.props.closePopup != null) this.props.closePopup(true);
        }}
      >
        <Text style={ButtonStyle.TextOnlyButton}>
          {this.props.propsData.secondaryButton.text}
        </Text>
      </TouchableOpacity>
    ) : null;
    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={() => {
            if (this.props.propsData.onClose != null)
              this.props.propsData.onClose();
            if (this.props.closePopup != null) this.props.closePopup(true);
          }}
        >
        </TouchableOpacity>
        {titleLayout}
        {descLayout}
        {checkBox}
        <SeparatorView separatorStyle={styles.SeparatorViewStyle} />
        <View style={styles.actionContainerStyle}>
          {primaryButton}
          {this.props.propsData.secondaryButton ? <View style={styles.verticalSeparator} /> : null}
          {secondaryButton}
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    width: "100%",
    paddingTop: spacing.spacing32,
    alignItems: "flex-start",
    borderRadius: spacing.spacing4
  },
  actionContainerStyle: {
    flexDirection: "row",
    justifyContent: "center",
  },
  titleLayout: {
    textAlign: "left",
    marginHorizontal: spacing.spacing24
  },
  descLayout: {
    textAlign: "left",
    marginTop: spacing.spacing12,
    marginHorizontal: spacing.spacing24
  },
  primaryButton: {
    width: "50%",
    paddingVertical: spacing.spacing16
  },
  secondaryButton: {
    width: "50%",
    paddingVertical: spacing.spacing16
  },
  SeparatorViewStyle: {
    marginTop: spacing.spacing24
  },
  verticalSeparator: {
    width: 1,
    height: '100%',
    backgroundColor: colors.color_E0E0E0
  },
  checkboxViewContainer: {
    marginHorizontal: spacing.spacing24,
    marginTop: spacing.spacing12,
    flexDirection: "row",
  },
  checkboxContainer: {
    marginRight: spacing.spacing8,
    height: 20,
    width: 20,
    padding: spacing.spacing4,
    alignSelf: "flex-start",
  },
  checkbox: {
    height: 16,
    width: 16
  },
  textContainer: {
    ...TextStyle.text_14_normal,
    flex: 1,
  }
});
