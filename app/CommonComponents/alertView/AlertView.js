import React, { Component } from "react";
import { Modal, View, TouchableWithoutFeedback, StyleSheet } from 'react-native'
import AlertLayout from "../../CommonComponents/alertView/AlertLayout"

export default class AlertView extends Component {

    state = {
        visible: false,
        title: "",
        message: "",
        primaryButton: "",
        secondaryButton: "",
        checkBox: null,
        cancelable: null,
        onClose: ""
    }

    constructor(props) {
        super(props);
    }

    showDailog(parms) {
        this.setState(
            {
                visible: true,
                title: parms.title,
                message: parms.message,
                primaryButton: parms.primaryButton,
                secondaryButton: parms.secondaryButton,
                checkBox: parms.checkBox,
                cancelable: parms.cancelable,
                onClose: parms.onClose,
            }
        )
    }

    closePopup(isClosePopup) {
        if (isClosePopup == null || isClosePopup)
            this.setState({ visible: false })
    }
    _renderOutsideTouchable(onTouch) {
        const view = <View style={{ flex: 1, width: '100%' }} />

        if (!onTouch) return view;

        return (
            <TouchableWithoutFeedback onPress={onTouch} style={{ flex: 1, width: '100%' }}>
                {view}
            </TouchableWithoutFeedback>
        )
    }

    renderView() {
        return <View style={styles.container}>
            <AlertLayout style={styles.childContainer}
                          propsData={this.state}
                          closePopup={(isClosePopup) => this.closePopup(isClosePopup)}
            />
        </View>
    }

    render() {
        return (<Modal
                transparent={true}
                visible={this.state.visible}
                onRequestClose={() => {
                    if (this.props.onClose != null)
                        this.props.onClose();
                    this.closePopup(this.state.cancelable)

                }}>
                <View style={[{
                    flex: 1,
                    backgroundColor: "#000000AA",
                    padding: 24
                }]}>
                    {this._renderOutsideTouchable(() => {
                        if (this.props.onClose != null)
                            this.props.onClose();
                        this.closePopup(this.state.cancelable)
                    })}
                    {this.renderView()}
                    {this._renderOutsideTouchable(() => {
                        this.closePopup(this.state.cancelable)
                        if (this.props.onClose != null)
                            this.props.onClose();
                    })}

                </View>
            </Modal>

        );
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        opacity: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    childContainer: {}
});
