import React, { Component } from "react";
import {
    ActivityIndicator,
    Image,
    Linking,
    PixelRatio,
    NativeModules,
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import PlatformActivityIndicator from "../CustomComponent/PlatformActivityIndicator.js";
import { DOCUMENT_TYPE, IMAGE_TYPE } from "../Constants/AppConstants";
import { CommonStyle } from "../Constants/CommonStyle";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";

const chatBrigeRef = NativeModules.ChatBridge;

export default class AgentSideDocument extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isImageLoading: true,
        };
    }

    render() {
        var containerView;
        if (this.props.documentType == IMAGE_TYPE) {
            let docUrl = { uri: this.props.docUrl };
            let loaderContainer = this.state.isImageLoading ? <View style={CommonStyle.loaderContainer}>
                <PlatformActivityIndicator />
            </View> : null;
            containerView = <TouchableOpacity disabled={this.state.isImageLoading} onPress={() => this.props.onImageClick(docUrl)}>
                <Image style={CommonStyle.ImageContainer} source={docUrl} onLoadEnd={() => this.setState({ isImageLoading: false })} />
                {loaderContainer}
            </TouchableOpacity>;

        } else if (this.props.documentType == DOCUMENT_TYPE) {
            let FileDetail = this.props.fileType.toUpperCase();
            var openText = Platform.OS == "ios" ? null : <Text style={style.openBrowser}>Please open this document in a web browser</Text>;
            containerView = <TouchableOpacity style={style.touchableContainer}
                onPress={() => {
                    if (Platform.OS === "ios") {
                        chatBrigeRef.openURLiOS(this.props.docUrl);
                    } else {
                        Linking.openURL(this.props.docUrl);
                    }
                }}>
                <View style={style.documentConatiner}>
                    <Image source={require("../images/icon_file.webp")} style={style.fileIcon} />
                    <View style={style.fileDetailConatiner}>
                        <Text style={style.fileName} ellipsizeMode="tail" numberOfLines={1}>{this.props.fileName}</Text>
                        <Text style={style.fileType} ellipsizeMode="tail" numberOfLines={1}>{FileDetail}</Text>
                    </View>
                    <Text style={style.OpenDocument}>View</Text>
                </View>
                {openText}
            </TouchableOpacity>;
        } else {
            containerView = null;
        }

        return (
            <View style={style.MainContainer}>
                {containerView}
            </View>
        );
    }
}

const style = StyleSheet.create({
    MainContainer: {
    },
    touchableContainer: {
        padding: spacing.spacing12,
        backgroundColor: colors.white,
        borderRadius: 4
    },
    documentConatiner: {
        flexDirection: "row",
    },
    fileDetailConatiner: {
        flex: 1,
        justifyContent: "space-between",
    },
    fileIcon: {
        marginRight: spacing.spacing8,
        width: 28,
        height: 34,
        padding: spacing.spacing4,
    },
    fileName: {
        fontSize: 12,
        fontFamily: "Lato",
        color: colors.charcoalGrey
    },
    fileType: {
        fontSize: 10,
        fontFamily: "Lato",
        color: colors.grey,
    },
    openBrowser: {
        marginTop: spacing.spacing12,
        fontSize: 12,
        fontFamily: "Lato",
    },
    OpenDocument: {
        marginLeft: spacing.spacing12,
        padding: spacing.spacing8,
        color: colors.blue,
        fontSize: 14,
        fontFamily: "Lato",
        fontWeight: "bold",
    }
});
