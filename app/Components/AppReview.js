import React, { Component } from "react";
import { Platform, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";

export default class AppReview extends Component {

    constructor() {
        super();
    }

    render() {
        let afterRatingText = this.props?.data?.afterRatingText;
        let heading1 = afterRatingText?.heading1 ?? 'Thank you for your feedback.';
        let heading2 = Platform.OS == "ios"?afterRatingText?.heading2?.ios:afterRatingText?.heading2?.android;
        heading2 = heading2 ?? `Please take some time to rate us on ${Platform.OS == "ios" ? "Apple App" : "Google Play"} Store`;
        return (
            <View style={styles.MainContainer}>
                <Text style={styles.Heading}>{heading1 + heading2}</Text>
                <View style={styles.googlePlayReviewButtonsConatiner}>
                    <TouchableOpacity style={styles.notNowButton} onPress={() => this.props.onClick(false, this.props.data?.ratingCardText?.notNowText ?? "No, Thanks!")}>
                        <Text style={styles.notNowtextStyle}>{this.props.data?.ratingCardText?.notNowText ?? 'Not Now'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.sureButton} onPress={() => this.props.onClick(true, null)}>
                        <Text style={styles.TextStyle}>{afterRatingText?.sureText ?? 'Sure'}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    MainContainer: {
        marginBottom: spacing.spacing16,
        borderRadius: 4,
        overflow: "hidden",
        backgroundColor: colors.white
    },
    notNowtextStyle: {
        textAlign: "center",
        fontSize: 16,
        fontFamily: "Lato",
        color: colors.blue
    },
    TextStyle: {
        color: colors.white,
        textAlign: "center",
        fontWeight: "bold",
        fontFamily: "Lato",
        fontSize: 16
    },
    Heading: {
        margin: spacing.spacing12,
        lineHeight: 20,
        color: colors.charcoalGrey,
        fontFamily: "Lato",
        fontSize: 16
    },
    notNowButton: {
        padding: spacing.spacing12,
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    sureButton: {
        padding: spacing.spacing12,
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
        backgroundColor: colors.blue,
        borderRadius: 4
    },
    googlePlayReviewButtonsConatiner: {
        margin: spacing.spacing16,
        alignItems: "center",
        flexDirection: "row"
    },

});
