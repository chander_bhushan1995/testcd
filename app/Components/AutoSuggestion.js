import React, {Component} from "react";
import PropTypes from "prop-types";
import {Animated, PanResponder, TouchableOpacity, View, StyleSheet, Text, FlatList, Image} from "react-native";
import {CommonStyle, TextStyle} from "../Constants/CommonStyle";
import spacing from "../Constants/Spacing";
import colors from "../Constants/colors";
import Highlighter from "react-native-highlight-words";
import * as Actions from "../Constants/ActionTypes";

let _this;
export default class AutoSuggestion extends Component {
    constructor() {
        super();
        _this = this;
        this.state = {
            modalVisible: false,
            animatedHeight: new Animated.Value(0),
            pan: new Animated.ValueXY()
        };

        this.createPanResponder();
    }

    createPanResponder() {
        this.panResponder = PanResponder.create({
            onStartShouldSetPanResponder: () => this.props.closeOnSwipeDown,
            onPanResponderMove: (e, gestureState) => {
                gestureState.dy < 0
                    ? null
                    : Animated.event([null, {dy: this.state.pan.y}])(e, gestureState);
            },
            onPanResponderRelease: (e, gestureState) => {
                if (this.props.height / 4 - gestureState.dy < 0) {
                    this.setModalVisible(false);
                } else {
                    Animated.spring(this.state.pan, {toValue: {x: 0, y: 0}}).start();
                }
            }
        });
    }

    setModalVisible(visible) {
        const {height, duration, onClose} = this.props;
        if (visible) {
            this.setState({modalVisible: visible});
            Animated.timing(this.state.animatedHeight, {
                toValue: height,
                duration: duration
            }).start();
        } else {
            Animated.timing(this.state.animatedHeight, {
                toValue: 0,
                duration: duration
            }).start(() => {
                this.setState({modalVisible: visible});
                this.state.pan.setValue({x: 0, y: 0});
                if (typeof onClose === "function") onClose();
            });
        }
    }

    open() {
        this.setModalVisible(true);
    }

    close() {
        this.setModalVisible(false);
    }

    render() {
        if (this.props.autoSuggestionVisible) {
            return (
                <TouchableOpacity
                    activeOpacity={1}
                    style={styles.wrapper} onPress={() => {
                    this.props.onClose();
                }}>
                    <View style={styles.container}>
                        <TouchableOpacity
                            activeOpacity={.8}
                            onPress={() => {
                                this.props.onClose();
                            }}
                        >
                            <Image source={require("../images/icon_cross.webp")} style={styles.crossIconContainer}/>
                        </TouchableOpacity>
                        <FlatList
                            keyboardDismissMode='on-drag'
                            keyboardShouldPersistTaps={"always"}
                            data={this.props.suggestionsList}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={this.renderSuggestionItem}
                            ItemSeparatorComponent={() => {
                                return <View style={CommonStyle.separator}/>;
                            }}
                        />
                    </View>
                </TouchableOpacity>)
        } else {
            return null;
        }
    }

    renderSuggestionItem(item, index) {
        const inputText=_this.props.inputTextForAutoSuggestions;
        return <TouchableOpacity
            activeOpacity={.8}
            style={styles.itemStyle} onPress={() => {
            _this.props.onSelectionSuggestion(item.item);
            // if (typeof item.item === 'string' || item.item instanceof String){
            //      _this.props.sendMessage(item.item, item.item, Actions.SEND_MESSAGE);
            // }
            // else{
            //      _this.props.sendMessage(JSON.stringify(item.item), item.item.text, Actions.SEND_MESSAGE);
            // }
            _this.setModalVisible(false);
        }}>
            <Highlighter
                style={[TextStyle.text_14_normal]}
                highlightStyle={[TextStyle.text_14_bold, {color: colors.blue028}]}
                searchWords={inputText=== undefined ||inputText===null?"":inputText.split(" ")}
                textToHighlight={item.item.text}
        />
        </TouchableOpacity>
    }

}

AutoSuggestion.propTypes = {
    height: PropTypes.number,
    duration: PropTypes.number,
    closeOnSwipeDown: PropTypes.bool,
    closeOnPressMask: PropTypes.bool,
    customStyles: PropTypes.object,
    onClose: PropTypes.func
};

AutoSuggestion.defaultProps = {
    height: 260,
    duration: 300,
    closeOnSwipeDown: true,
    closeOnPressMask: true,
    customStyles: {}
};

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        width: "100%",
        justifyContent: "flex-end",
        backgroundColor: "transparent"
    },
    mask: {
        flex: 1,
        backgroundColor: "transparent"
    },
    container: {
        paddingTop: spacing.spacing12,
        backgroundColor: "white",
        overflow: "hidden"
    },
    itemStyle: {
        paddingRight: spacing.spacing12,
        paddingLeft: spacing.spacing12,
        paddingTop: spacing.spacing16,
        paddingBottom: spacing.spacing16,
    },
    crossIconContainer: {
        marginRight: spacing.spacing12,
        paddingTop: spacing.spacing12,
        height: spacing.spacing16,
        width: spacing.spacing16,
        alignSelf: 'flex-end',
        resizeMode: "contain",
        justifyContent: "flex-end"
    },
});
