import React, { Component } from "react";
import { FlatList, StyleSheet, View } from "react-native";
import CloseChatItem from './CloseChatItem'
import { CommonStyle } from "../Constants/CommonStyle";
import RBSheet from "./RBSheet";

export default class BottomSheetCloseChat extends Component {

    state = { scrollEnabled: true }

    open() {
        this.RBSheet.open();
        this.props.getData();
    }

    close() {
        this.RBSheet.close();
    }

    render() {
        return (
            <RBSheet
                ref={ref => { this.RBSheet = ref }}
                duration={10}
                height={56}
                closeOnSwipeDown={false}
                onClose={() => {}}
            >
                <View style={style.MainContainer}>
                    <FlatList
                        scrollEnabled={this.state.scrollEnabled}
                        data={this.props.data ?? []}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={this.renderListItem}
                        ItemSeparatorComponent={() => <View style={CommonStyle.separator} />}
                    />
                </View>
            </RBSheet>
        );
    }

    renderListItem = ({ item, index }) => {
        return <CloseChatItem
            onItemClick={() => {
                this.RBSheet.close();
                setTimeout(() => this.props.callCloseChat(), 50);
            }}
            data={item}
            translatedData={this?.props?.translatedData}/>
    };
}

const style = StyleSheet.create({
    MainContainer: {
        width: "100%",
    }
});
