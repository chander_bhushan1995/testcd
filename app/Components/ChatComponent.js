import React, {Component} from "react";
import {
    Alert,
    BackHandler,
    FlatList,
    Image,
    Keyboard,
    Linking,
    NativeEventEmitter,
    NativeModules,
    Platform,
    StyleSheet,
    Text,
    ToastAndroid,
    TouchableOpacity,
    View,
    SafeAreaView
} from "react-native";
import NetInfo from '@react-native-community/netinfo';
import * as Actions from "../Constants/ActionTypes";
import SendView from "./SendView";
import ReceiveView from "./ReceiveView";
import BatchedBridge from "react-native/Libraries/BatchedBridge/BatchedBridge";
import Loader from "./Loader";
import Toast from "react-native-easy-toast";
import {HeaderBackButton, NavigationActions} from "react-navigation";
import BottomSheetCloseChat from "./BottomSheetCloseChat";
import {BottomViewType} from "../Constants/BottomViewType";
import BottomView from "../ChatbotComponent/BottomView";
import InternetBar from "./InternetBar";
import * as StoreReview from "react-native-store-review";
import colors from "../Constants/colors";
import DialogView from "./DialogView"
import DocumentsUtils from "../commonUtil/DocumentsUtils";
import AutoSuggestion from "./AutoSuggestion";
import DateTimePicker from 'react-native-modal-datetime-picker';
import { logChatWebEngageEvent, logWebEnageEvent } from '../commonUtil/WebengageTrackingUtils';
import { CHAT_EXIT, USER_CHAT_EXIT_WITHOUT_FIRST_MSG} from '../Constants/WebengageEvents';
import { parseJSON, deepCopy, removeSpecialCharacters} from '../commonUtil/AppUtils';
import CloseChatView from "./CloseChatView";
import { isJson } from "../Chat/ChatUtils";
import {chatManager, localEventEmitter} from "../Navigation/index";
import { logChatEvents } from "../Chat/ChatAPIHelper";
import ChatManager from "../Chat/ChatManager";
import {TextStyle} from "../Constants/CommonStyle";

let dateTimeIndex;
let unsubscribe = () => {};
let delayAfterPauseTypingTime = 200;
let startTime = new Date();

// It is class which is export to bridge to get the data from websocket
export class ReceiveMessageFromWebSocket {
    // it is method of get the messages from websocket
    receiveMessage(message, from, chatId, msgId) {
        receiveMessageFromAgent(message, from, chatId, msgId);
    }
    // it is method of get the typing tanza
    typingStanza(stanza, from) {
        receiveStatusFromAgent(stanza, from);
    }
}

//  this method is resposible to populate the response msg from web socket
function receiveMessageFromAgent(message, from, chatId, msgId) {
    this.props?.receivedMessage({ message: message, from: from, chatId: chatId, msgId: msgId })
}

//  this method is resposible to populate the typing status from web socket
function receiveStatusFromAgent(status, from) {
    this.props?.receivedStatus({ status: status, from: from })
}

// Create the bridge to connect the native code
const receiveMessageFromWebSocket = new ReceiveMessageFromWebSocket();
BatchedBridge.registerCallableModule("JavaScriptVisibleToJava", receiveMessageFromWebSocket);

const chatBrigeRef = NativeModules.ChatBridge;
const eventEmitter = new NativeEventEmitter(chatBrigeRef);
var _this;

export const ChatContext = React.createContext();

export default class ChatComponent extends Component {

    constructor(props) {
        super(props);
        startTime = new Date();
        console.ignoredYellowBox = ["Warning:"];
        console.disableYellowBox = true;
        receiveMessageFromAgent = receiveMessageFromAgent.bind(this);
        receiveStatusFromAgent = receiveStatusFromAgent.bind(this);
    }

    static navigationOptions = ({navigation}) => {
        let restartChatImage = <Text style={[TextStyle.text_14_bold,{color:colors.blue}]}>CLOSE</Text>
        return {
            headerTitle: <View style={Platform.OS === "ios" ? {alignItems: "center"} : {alignItems: "flex-start"}}>
                <Text style={style.headerTitle}>Chat with us</Text>
                <Text style={style.headerSubtitle}>Ask anything. We're happy to assist!</Text>
            </View>,
            headerLeft: <HeaderBackButton tintColor={colors.black} onPress={() => {
                if (!chatManager.isFirstSocketMessageShown()) {
                    let endTime = new Date();
                    let difference = (endTime.getTime() - startTime.getTime())/1000;
                    logWebEnageEvent(USER_CHAT_EXIT_WITHOUT_FIRST_MSG, { TimeDiff : difference })
                }
                chatManager.exitChat();
                chatBrigeRef.goBack();
            }}/>,
            headerRight: <View style={{flexDirection: 'row'}}>
                <TouchableOpacity style={style.NavigationBarRightButton} onPress={() => _this.CloseChatViewRef?.showDailog()}>
                    {restartChatImage}
                </TouchableOpacity>
            </View>,
            headerTitleStyle: {color: colors.white},
            headerTintColor: colors.white
        };
    };

    keyboardWillHide = (event) => {
        if (this.props.autoSuggestionVisible) {
            this.props.resetSuggestionView();
        }
    };

    componentDidMount() {
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide);
        this.props.resetState(true);
        this.props.createSocketConnection();
        NetInfo.fetch().then(state => _this.props.updateConnectionStatus(state.isConnected, true));
        chatBrigeRef.getAutoSuggestionsData(autoSuggestionsObj => {
            if (isJson(autoSuggestionsObj)) {
                let autoSuggestionsData = parseJSON(autoSuggestionsObj);
                if (autoSuggestionsData) {
                    try {
                        delayAfterPauseTypingTime = autoSuggestionsData.api_delay_time;
                    } catch (e) {
                    }
                }
            }
        });
        unsubscribe = NetInfo.addEventListener((state) => {
            _this.props.updateConnectionStatus(state.isConnected, true);
            if (state.isConnected) {
                chatBrigeRef.reconnectSocket();
            }
        })
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        if (Platform.OS === "ios") {
            eventEmitter.addListener("ReceiveEvent", params => this.props.receivedMessage(parseJSON(params)));
            eventEmitter.addListener("StatusEvent", params => this.props.receivedStatus(parseJSON(params)));
        }
        localEventEmitter.addListener("InputBoxEvent", () => this.props.defaultInputType());
        localEventEmitter.addListener("LocalMessage", params => this.props.receivedMessage(params));
    }

    componentWillUnmount() {
        logChatWebEngageEvent(logChatEvents, CHAT_EXIT);
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
        this.props.resetState(false);
        chatBrigeRef.disconnectSocket();
        unsubscribe();
        if (Platform.OS === "ios") {
            eventEmitter.removeListener("ReceiveEvent", () => { });
            eventEmitter.removeListener("StatusEvent", () => { });
        }
        localEventEmitter.removeListener("InputBoxEvent", () => { })
        localEventEmitter?.removeListener("LocalMessage", () => { })
    }

    onBackPress = () => {
        try {
            let routeList = deepCopy(this.props.routeList)
            let routesListObj = routeList.pop();
            let routes = routesListObj.routes.pop();
            if (routes !== null || routes !== undefined) {
                let topRouteName = routes.routeName;
                if (topRouteName === "screen1") {
                    if (!chatManager.isFirstSocketMessageShown()) {
                        let endTime = new Date();
                        let difference = (endTime.getTime() - startTime.getTime())/1000;
                        logWebEnageEvent(USER_CHAT_EXIT_WITHOUT_FIRST_MSG, { TimeDiff : difference })
                    }
                    chatManager.exitChat()
                    chatBrigeRef.goBack();
                }
            }
        } catch (e) {
        }
        this.props.back(NavigationActions);
        return true;
    };

    render() {
        _this = this;
        if (this.props.type === Actions.UPDATE_CONNECTION_STATUS) {
            if (this.props.isInternetConnected && this.props.showInternetBar) {
                setTimeout(() => this.props.updateConnectionStatus(true, false), 3000)
            }
        } else if (this.props.type === Actions.TECHNICAL_ERROR) {
            if (this.props.errorMesage === Actions.CLOSED_CHAT_ERROR_MESSAGE) {
                this.DialogViewRef?.showDailog({
                    title: "Chat is closed",
                    message: this.props.errorMesage,
                    primaryButton: {
                        text: "Okay, got it", onClick: () => this.props.resetDataAndShowStartChat()
                    },
                    cancelable: false,
                    onClose: () => this.props.resetDataAndShowStartChat()
                })
            } else {
                this.DialogViewRef?.showDailog({
                    title: "Technical Error",
                    message: this.props.errorMesage,
                    primaryButton: {
                        text: "Okay, got it", onClick: () => { }
                    },
                    cancelable: false,
                    onClose: () => { }
                })
            }
        } else if (this.props.type === Actions.RATING_SUBMIT_SUCCESS) {
            const ratingSubmittedMessage=ChatManager?.getInstance()?.getRatingResponse()?.message ?? "Rating submitted succesfully"
            if (Platform.OS === "ios") {
                this.refs.toast.show(ratingSubmittedMessage);
            } else {
                ToastAndroid.show(ratingSubmittedMessage, ToastAndroid.SHORT);
            }
        } else if (this.props.type === Actions.NEAGATIVE_RATING_QUESTIONS) {
            this.showFeedbackQeustions(this.props.ratingData, false);
        }
        let autoSuggestionsView = null;
        if (this.props.autoSuggestionEnabled && this.props.autoSuggestionVisible && this.props.autoSuggestionData.length)
            autoSuggestionsView = <View style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: this.props.suggestionViewMarginFromBottom,
                justifyContent: 'flex-end',
                alignItems: 'center'
            }}>
                <AutoSuggestion
                    ref={ref => { this.AutoSuggestion = ref }}
                    sendMessage={(sendData, displayData, type) => this.addSendMessageView(sendData, displayData, type)}
                    autoSuggestionVisible={this.props.autoSuggestionVisible}
                    suggestionsList={this.props.autoSuggestionData}
                    inputTextForAutoSuggestions={this.props.inputTextForAutoSuggestions}
                    onSelectionSuggestion={(selectedItem) => this.props.onSelectionSuggestion(selectedItem)}
                    onClose={() => this.props.resetSuggestionView()}
                />
            </View>;

        return (
            // <SafeAreaView style={this.props.bottomViewType == BottomViewType.None ? style.SafeArea : style.SafeAreaBottomView}>
            <ChatContext.Provider value={{
                selectedTagData: this.props.selectedTagData,
                otherTagData: this.props.otherTagData,
                dispatch: (action) => this.props.callDispatch(action)
            }}>
            <View style={style.container}>
                <DialogView ref={ref => (this.DialogViewRef = ref)}/>
                <CloseChatView
                    ref = {ref => (this.CloseChatViewRef = ref)}
                    storeReview={() => this.openStoreReview()}
                    exitChat={() => chatBrigeRef.goBack()}
                    showFeedbackQeustions={(questions) => this.showFeedbackQeustions(questions, true)}
                    translatedData={chatManager.getTranslatedData()}
                />
                <InternetBar isConnected={this.props.isInternetConnected} showInternetBar={this.props.showInternetBar}/>
                <Loader isLoading={this.props.isLoading} loadingMessage={"We're setting up to help you in the best way\n(Usually takes 8 - 10sec)"}/>
                <FlatList
                    contentContainerStyle={style.chatContainer}
                    renderItem={this._renderItem}
                    data={this.props.dataItem}
                    ref={ref => (this.sectionListRef = ref)}
                    onContentSizeChange={() => {
                        try {
                                if (Platform.OS === "ios") {
                                    setTimeout(() => {
                                        try {
                                            if (this != null && this.sectionListRef != null) {
                                                this.sectionListRef.scrollToEnd({animated:true})
                                            }
                                        } catch (e) {
                                        }
                                    }, 100);
                                } else {
                                    this.sectionListRef.scrollToEnd({animated: true}, 500);
                                }
                        } catch (e) {
                        }
                    }}
                    // Auto scroll to botom while added the view in list
                    onLayout={() => {
                        // need to validate for 200.
                        try {
                                if (Platform.OS === "ios") {
                                    setTimeout(() => {
                                        try {
                                            if (this != null && this.sectionListRef != null) {
                                                this.sectionListRef.scrollToEnd({animated:true})
                                            }
                                        } catch (e) {
                                        }
                                    }, 100);
                                } else {
                                    this.sectionListRef.scrollToEnd({animated: true}, 500);
                                }
                        } catch (e) {
                        }
                    }}
                    keyExtractor={(item, index) => index.toString()}
                />
                <BottomView
                    bottomViewType={this.props.bottomViewType}
                    updateAutoSuggestions={this.props.updateAutoSuggestions}
                    plusIconClick={() => this.plusIconClick()}
                    autoSuggestionEnabled={this.props.autoSuggestionEnabled}
                    autoSuggestionVisible={(isVisible) => {
                        if (this.props.autoSuggestionVisible !== isVisible) {
                            this.props.showAutoSuggestionView(isVisible);
                        }
                    }}
                    delayAfterPauseTypingTime={delayAfterPauseTypingTime}
                    selectedSuggestionData={this.props.selectedSuggestionData}
                    getAutosuggestions={(inputText,inputData) => this.props.getAutosuggestions(inputText,inputData)}
                    resetSelectedSuggestion={() => this.props.resetSelectedSuggestion}
                    updateBottomMarginAutoSuggestionView={(newMargin) => this.props.updateBottomMarginAutoSuggestionView(newMargin)}
                    shouldClearInput={this.props.shouldClearInput}
                    sendMessage={(sendData, displayData, type) => this.addSendMessageView(sendData, displayData, type)}
                    updateBottomView={this.props.updateBottomView}
                    bottomViewData={this.props.bottomViewData}
                    formType={this.props.bottomViewType}
                    handleChevornUpTap={() => {
                        if (this.props.bottomViewType === BottomViewType.ChatbotForm) {
                            this.props.navigation.navigate("ChatbotForm", {
                                data: this.props.bottomViewData,
                                onFormButtonPress: (data) => {
                                    //remove special characters from every item of response value array
                                    let formItemArray = data?.params?.formItems;
                                    for (let i = 0; i < formItemArray?.length; i++){
                                        let formItem = formItemArray[i];
                                        let responseArray = formItem?.response;
                                        for (let j = 0; j < responseArray?.length; j++){
                                            let response = responseArray[j];
                                            response.value = removeSpecialCharacters(response.value);
                                        }
                                    }
                                    this.addSendMessageView( JSON.stringify({"text": data.text, "params": data.params, "documentUrl": data.documentUrl}), data.text, Actions.SEND_MESSAGE)
                                    this.props.updateBottomView();
                                }
                            });
                        } else if (this.props.bottomViewType === BottomViewType.ChatbotUploadForm) {
                            this.props.navigation.navigate("ChatbotUploadDocumentForm", {
                                data: this.props.bottomViewData,
                                dialogContext: this.DialogViewRef,
                                onFormButtonPress: (data) => {
                                    this.addSendMessageView(JSON.stringify({
                                        'text': data.text,
                                        'params': data.params,
                                        'documentUrl': data.documentUrl,
                                    }), data.text, Actions.SEND_MESSAGE);
                                    this.props.updateBottomView();
                                },
                                onUploadImage: (uploadData) => {
                                    this.props.callApiForImageUpload(uploadData);
                                },
                                callDraftDetailApi: (draftRequest) => {
                                    this.props.callDraftDetailApi(draftRequest);
                                },
                                callDownloadAlreadyUploadedImage: (downloadRequest)=>{
                                    this.props.callDownloadAlreadyUploadedImage(downloadRequest);
                                },
                                callDownloadTemplate: (downloadRequest)=>{
                                    this.props.callDownloadTemplate(downloadRequest);
                                },
                            });
                        }
                    }}
                    startNewChat={() => {
                        this.props.resetBottomView();
                        this.props.startChatApi(true).done()
                    }}
                />
                <Toast ref="toast" position="center"/>
                <BottomSheetCloseChat
                    ref={ref => { this.bottomSheetRef = ref }}
                    data={this.props.BottomSheetCloseChatData}
                    callCloseChat={() => this.CloseChatViewRef?.showDailog()}
                    getData={this.props.getDataForCloseChat}
                    translatedData={this?.props?.translatedData}
                />
                {autoSuggestionsView}
                <DateTimePicker
                    maximumDate={new Date()}
                    isVisible={this.props?.isDateTimePickerVisible}
                    mode={this.props?.dateTimePickerType}
                    onConfirm={(date) => this.dateTimeCallback(date, this.props?.dateTimePickerType)}
                    onCancel={this.props.hideDateTimePicker}
                />
            </View>
            </ChatContext.Provider>
            // </SafeAreaView>
        );
    };

    dateTimeCallback = (date, type) => {
        this.props.hideDateTimePicker();
        var dateFormat = require('dateformat');
        let dateStringFormat = "dd/mm/yyyy, HH:MM"
        if (type == "date") {
            dateStringFormat = "dd/mm/yyyy"
        } else if (type == "time") {
            dateStringFormat = "HH:MM"
        }
        let formattedDate = dateFormat(date, dateStringFormat);
        let sendData = {text: formattedDate, params: ""};
        this.addSendMessageView(JSON.stringify(sendData), formattedDate, Actions.SEND_MESSAGE);
        this.props.removeChatBotView(dateTimeIndex, "");
    }

    addSendMessageView(sendData, displayData, type) {
        this.props.addSendMessageView(sendData, displayData, type);
    }

    // To selcect the documents from device
    plusIconClick() {
        const sectionDocumentParams = {
            context: () => { return _this },
            onSuccess: (selectedDocumentObject) => _this.props.addDocumentUploadView(selectedDocumentObject)
        };
        try {
            new DocumentsUtils().onDocumentSelection(sectionDocumentParams);
        } catch (e) {
            Alert.alert(e.message);
        }
    }

    openStoreReview() {
        if (Platform.OS === "ios") {
            if (StoreReview.isAvailable) {
                StoreReview.requestReview();
            } else {
                chatBrigeRef.openURLiOS("itms://itunes.apple.com/in/app/one-assist-for-iphone/id1074619069?mt=8");
            }
        } else {
            Linking.openURL("market://details?id=com.oneassist");
        }
    }

    showFeedbackQeustions(questions, isCloseChat) {
        const completion = () => {
            if (isCloseChat) {
                chatBrigeRef.goBack();
            }
        }
        this.props.navigation.navigate("RatingQuestionnaireModal", {
            ratingQuestions: questions,
            onCancel: completion,
            negativeRatingSubmit: (comment, questionsArr) => this.props.callForCaptureFeedback(comment, questionsArr, completion)
        });
    }

    onImageClick(imageUri) {
        this.props.navigation.navigate("ImagePreview", { imageUri: imageUri });
    }

    // TO render the views in the flat list based on the item type
    _renderItem = ({item, index}) => {
        try {
            //TODO: ANDAND please review below mentioed condition
            // if (index > this.props.animatedIndex + 1) return null; // Don't show all the animating view at once.
            if (item.type === Actions.TYPING || item.type === Actions.RECEIVE_MESSAGE || item.type === Actions.AGENT_SIDE_DOCUMENT || item.type === Actions.RATING || item.type === Actions.REVIEW || item.type === Actions.BOT) {
                return (
                    <ReceiveView
                        data={item}
                        animationCompletion={() => this.props.receiveViewAnimationDone(index)}
                        action={(action, actionType, params) => {
                            if (action === Actions.AGENT_SIDE_DOCUMENT) {
                                this.onImageClick(params.imageUri)
                            } else if (action === Actions.RATING) {
                                this.props.removeViewFromList(index);
                                this.props.callForCaptureRating(params.rating);
                            } else if (action === Actions.REVIEW) {
                                this.props.removeViewFromList(index);
                                if (params.message) {
                                    this.props.addSendMessageStaticView(params.message);
                                }
                                if (params.takeReview) {
                                    this.openStoreReview()
                                }
                                this.props.showStartChatButton();
                            } else if (action === Actions.BOT) {
                                if (actionType === Actions.DATE_TIME) {
                                    dateTimeIndex = index;
                                    this.props.showDateTimePicker(params?.messageText?.params?.dataTimePickerType);
                                } else if (actionType === Actions.BOT_WEBVIEW) {
                                    this.props.navigation.navigate("ChatWebViewComponent", {
                                        title: params?.value.text,
                                        deeplinkUrl: params?.deepLinkUrl,
                                        successUrl: params?.successUrl,
                                        failureUrl: params?.failureUrl,
                                        isComplete: (success) => {
                                            if (success !== null) {
                                                let finalParam = ""
                                                if (params?.params) {
                                                    finalParam = {
                                                        ...params?.params,
                                                        isSuccess: success
                                                    }
                                                }
                                                this.addSendMessageView(JSON.stringify({ "text": params?.value?.text, "params": finalParam }), params?.value?.text, Actions.SEND_MESSAGE);
                                                this.props.removeChatBotView(index, params?.params?.displayMessage);
                                            }
                                        }
                                    });
                                } else {
                                    this.addSendMessageView(JSON.stringify(params.messageText), params.messageText.text, Actions.SEND_MESSAGE);
                                    this.props.removeChatBotView(index,params?.messageText?.params?.displayMessage);
                                }
                            }
                        }}
                    />
                );
            } else if (item.type == Actions.SEND_MESSAGE || item.type == Actions.DOCUMENT || item.type === Actions.SEND_MESSAGE_TAGS) {
                return (
                    <SendView
                        data={item}
                        animationCompletion={() => this.props.receiveViewAnimationDone(index)}
                        internetStatus={this.props.isInternetConnected}
                        action={(action, actionType, params) => {
                            if (action === Actions.SEND_MESSAGE) {
                                this.props.sendMessage(params.data, index);
                            } else if (action === Actions.DOCUMENT) {
                                if (actionType === "onImageClick") {
                                    this.onImageClick(params.imageUri)
                                } else if (actionType === "uploadDocument") {
                                    this.props.uploadDocument(index, params.docUrl, params.pickedFrom);
                                }
                            }
                        }}
                    />
                );
            } else {
                return null;
            }
        } catch (error) {
            return null;
        }
    };
}

const style = StyleSheet.create({
    // SafeArea: {
    //     flex: 1,
    //     backgroundColor: colors.chatBackground
    // },
    // SafeAreaBottomView: {
    //     flex: 1,
    //     backgroundColor: colors.white
    // },
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: colors.chatBackground
    },
    restartChatIOS: {
        width: 28,
        height: 28,
        justifyContent: 'center',
        alignItems: 'center',
        resizeMode: "contain",
    },
    restartChat: {
        width: 24,
        height: 24,
        justifyContent: 'center',
        alignItems: 'center',
        resizeMode: "contain",
    },
    chatContainer: {
        justifyContent: "space-between"
    },
    NavigationBarRightButton: {
        paddingRight: 16,
    },
    headerTitle: {
        fontSize: 16,
        fontFamily: "Lato",
        color: colors.charcoalGrey
    },
    headerSubtitle: {
        fontSize: 12,
        fontFamily: "Lato",
        color: colors.grey
    }
});
