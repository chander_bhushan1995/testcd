import React, { useRef, useEffect, useState } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import {WebView} from 'react-native-webview';
import colors from '../Constants/colors';
import { TextStyle } from "../Constants/CommonStyle";
import { HeaderBackButton } from "react-navigation";
import { PLATFORM_OS } from "../Constants/AppConstants";
import useBackHandler from "../CardManagement/CustomHooks/useBackHandler";
import Loader from '../Components/Loader';

export default function ChatWebViewComponent(props) {
    const { navigation } = props
    const title = navigation.getParam('title', null);
    const loadingMessage = navigation.getParam('loadingMessage', null);
    const paymentUrl = navigation.getParam('deeplinkUrl', null);
    const successUrl = navigation.getParam('successUrl', null);
    const failureUrl = navigation.getParam('failureUrl', null);
    const isComplete = navigation.getParam('isComplete', () => {});
    const webViewRef = useRef(null)
    const [isDeeplinkSuccessful, setIsDeeplinkSuccessful] = useState(null);

    useBackHandler(() => onBackPress(), [isDeeplinkSuccessful])

    useEffect(() => {
        navigation.setParams({
            title: title,
            onBackPress: () => onBackPress()
         })
    }, [isDeeplinkSuccessful])

    const onBackPress = () => {
        isComplete(isDeeplinkSuccessful);
        navigation.pop();
        return true
    };

    const getActivityIndicatorLoadingView = () => {
        return (
            <Loader isLoading={true} loadingMessage={loadingMessage ?loadingMessage:"You are being redirected to complete your payment."} loaderStyle={{ backgroundColor: colors.white }} />
        );
    }

    const onShouldStartLoadWithRequest = (navigator) => {
        if (successUrl?.trim()?.length && navigator.url.includes(successUrl)) {
            setIsDeeplinkSuccessful(true);
        } else if (failureUrl?.trim()?.length && navigator.url.includes(failureUrl)) {
            setIsDeeplinkSuccessful(false);
        }
        return true;
    }

    return (
        <View style={{ flex: 1 }}>
            <WebView
                ref={webViewRef}
                style={styles.WebViewStyle}
                source={{ uri: paymentUrl }}
                thirdPartyCookiesEnabled={true}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                scalesPageToFit={true}
                renderLoading={getActivityIndicatorLoadingView}
                startInLoadingState={true}
                onShouldStartLoadWithRequest={onShouldStartLoadWithRequest}
                onNavigationStateChange={onShouldStartLoadWithRequest}
                dataDetectorTypes={'none'}
            />
        </View>
    )
}

ChatWebViewComponent.navigationOptions = ({ navigation }) => {
    const { title, onBackPress } = navigation.state.params;
    return {
        headerTitle: <View style={Platform.OS === PLATFORM_OS.ios ? { alignItems: "center" } : { alignItems: "flex-start" }}>
            <Text style={TextStyle.text_16_bold}>{title}</Text>
        </View>,
        headerLeft: <HeaderBackButton tintColor={colors.black} onPress={onBackPress} />,
        headerTitleStyle: { color: colors.white },
        headerTintColor: colors.white
    };
};

const styles = StyleSheet.create({
    WebViewStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
});
