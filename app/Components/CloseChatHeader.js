import React, {Component} from "react";
import {StyleSheet, Text, View} from "react-native";
import { CommonStyle } from "../Constants/CommonStyle";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";

export default class CloseChatHeader extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.isHeaderNeeded)
            return (
                <View>
                    <Text style={style.textStyle}>{this.props.headerMessage}</Text>
                    <View style={CommonStyle.separator} />
                </View>
            );
        else
            return null;
    }
}
const style = StyleSheet.create({
    textStyle: {
        justifyContent: "center",
        margin: spacing.spacing16,
        fontSize: 14,
        fontFamily: "Lato",
        fontWeight: "bold",
        color: colors.charcoalGrey
    }
});
