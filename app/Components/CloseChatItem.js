import React, {Component} from "react";
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";

export default class CloseChatItem extends Component {
    render() {
        return (
            <TouchableOpacity disabled={this.props.data.isDisabled} onPress={this.props.onItemClick}>
                <View style={style.container}>
                    <Image source={require("../images/icon_cross_close_chat.webp")} style={style.crossImage} />
                    <Text style={[style.title, this.props.data.isDisabled && { color: colors.color_b3b3b3 }]}> {this.props.data.name} </Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const style = StyleSheet.create({
    container: {
        flexDirection: "row",
        backgroundColor: colors.white,
    },
    title: {
        lineHeight: 20,
        flexDirection: "row",
        flexWrap: "wrap",
        color: colors.charcoalGrey,
        padding: spacing.spacing16,
        flex: 1,
        justifyContent: "space-between",
        alignSelf: "flex-start",
        fontSize: 14,
        fontFamily: "Lato",
    },
    crossImage: {
        width: 14,
        height: 14,
        marginLeft: 30,
        marginTop: 20,
        justifyContent: 'flex-start',
        resizeMode: "contain"
    },
});
