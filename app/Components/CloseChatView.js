import React, { Component, useState } from "react";
import { Modal, View, TouchableWithoutFeedback, StyleSheet, TouchableOpacity, Text, Image } from 'react-native'
import { TextStyle, ButtonStyle, CommonStyle } from "../Constants/CommonStyle"
import colors from "../Constants/colors"
import spacing from "../Constants/Spacing";
import Loader from "./Loader";
import * as Actions from "../Constants/ActionTypes";
import { OAImageSource } from "../Constants/OAImageSource";
import { chatManager } from "../Navigation/index";
import {chatFlowStrings} from '../Chat/ChatConstants';

function AlertView(props) {
    const { title, message, cancellable = true, hasStar = false, primaryButton, secondaryButton, onClose } = props
    const maxRating = 5
    const ratingArray = Array.from(Array(maxRating), (_, index) => index + 1);
    const [rating, setRating] = useState(maxRating);
   const primaryButtonView = () => {
        if (primaryButton) {
            const { text, onClick = () => { } } = primaryButton
            return (<TouchableOpacity style={styles.primaryButton} activeOpacity={.8} onPress={() => onClick(rating)} >
                <Text style={ButtonStyle.primaryButtonText}>{text}</Text>
            </TouchableOpacity>);
        } else {
            return null;
        }
    }

    const secondaryButtonView = () => {
        if (secondaryButton) {
            const { text, onClick = () => { } } = secondaryButton
            return (<TouchableOpacity style={[styles.secondaryButton]} activeOpacity={.8} onPress={() => onClick(rating)}>
                <Text style={ButtonStyle.TextOnlyButton}>{text}</Text>
            </TouchableOpacity>);
        } else {
            return null;
        }
    }

    const starView = () => {
        return (<View style={styles.Rating}>
            {ratingArray.map(value => {
                return (<TouchableOpacity activeOpacity={1.0} key={value} onPress={() => setRating(value)}>
                    <Image style={styles.StarImage} source={value <= rating ? OAImageSource.star_selected : OAImageSource.star_unselected} />
                </TouchableOpacity>)
            })}
        </View>);
    }

    const crossView = () => {
        if (cancellable) {
            return (<TouchableOpacity onPress={onClose}>
                <Image source={require("../images/icon_cross.webp")} style={CommonStyle.crossIconContainer} />
            </TouchableOpacity>);
        } else {
            return null;
        }
    }

    return (
        <View style={styles.container}>
            {crossView()}
            {title ? <Text style={[styles.titleLayout, !cancellable && { marginTop: spacing.spacing32 }]}>{title}</Text> : null}
            {message ? <Text style={styles.descLayout}>{message}</Text> : null}
            {hasStar ? starView() : null}
            {primaryButtonView()}
            {secondaryButtonView()}
        </View>
    );
}

export default class CloseChatView extends Component {
    state = {
        visible: false,
        isChatClosed: false,
        isRatingSubmitted: false,
        isError: false,
        isTechnicalError: false,
        loaderText: null,
        closeChatData: null,
    }

    constructor(props) {
        super(props);
    }

    showDailog() {
        this.setState({
            visible: true,
            isChatClosed: false,
            isRatingSubmitted: false,
            isError: false,
            isTechnicalError: false,
            loaderText: null,
            closeChatData: null,
        })
    }

    handleStoreRating() {
        this.props.storeReview()
        this.closePopup()
    }

    handleRatingSubmit(rating) {
        let ratingCardText = this.state.closeChatData?.ratingCardText;
        let loaderText = ratingCardText?.loaderText ?? 'Submitting Rating';
        this.setState({loaderText: loaderText + ' ...', isError: false});
        chatManager.submitRating(rating, (response, error) => {
            if (error) {
                this.setState({ isError: true, isTechnicalError: error.message === "10001", loaderText: null })
            } else {
                if (rating > 3) {
                    this.setState({ isRatingSubmitted: true, isError: false, loaderText: null })
                } else {
                    chatManager.feedbackQuestions((response, error) => {
                        if (error) {
                            this.setState({ isError: true, isTechnicalError: error.message === "10001", loaderText: null })
                        } else {
                            this.setState({ visible: false })
                            this.props.showFeedbackQeustions(response)
                        }
                    })
                }
            }
        })
    }

    handleCloseChat() {
        let translatedData = this.props.translatedData ?? [];
        let loaderText = translatedData[0] ?? chatFlowStrings.closing_chat;
        this.setState({loaderText: loaderText + ' ...', isError: false});
        chatManager.closeChat((response, error) => {
            if (error) {
                this.setState({ isError: true, isTechnicalError: error.message === "10001", loaderText: null })
            } else {
                if(response?.isChatExit)
                {
                    this.setState({ isChatClosed: true, isError: false },()=> this.closePopup());
                }
                else if ((response?.status === "success" && response?.data?.update) || (response?.status === "failure" && response?.error[0]?.errorCode === "ACC_5002")) {
                    if (response?.data?.showRating) {
                        this.setState({ isChatClosed: true, isError: false, loaderText: null, closeChatData: response?.data});
                    } else {
                        this.setState({ isChatClosed: true, isError: false });
                        this.closePopup();
                    }
                } else {
                    this.setState({ isError: true, isTechnicalError: true, loaderText: null })
                }
            }
        })
    }

    closePopup() {
        this.setState({ visible: false })
        if (this.state.isChatClosed)
            this.props.exitChat()
    }

    dismissPopup() {
        if (!this.state.isChatClosed && this.state.loaderText === null) {
            this.closePopup()
        }
    }

    _renderOutsideTouchable() {
        return (
            <TouchableWithoutFeedback onPress={() => this.dismissPopup()} style={{ flex: 1, width: '100%' }}>
                <View style={{ flex: 1, width: '100%' }} />
            </TouchableWithoutFeedback>
        )
    }

    alertView() {
        if (this.state.loaderText) {
            return <Loader isLoading={true} loadingMessage={this.state.loaderText} loaderStyle={{ height: 200 }} />
        } else if (this.state.isError) {
            return <AlertView style={styles.childContainer}
                title={this.state.isTechnicalError ? 'Technical Error' : "Network Error"}
                message={this.state.isTechnicalError ? Actions.TECHNICAL_ERROR_MESSAGE : Actions.INTERNET_ERROR_MESSAGE_BUYBACK}
                primaryButton={{ text: "Okay, got it", onClick: () => this.closePopup() }}
                onClose={() => this.closePopup()}
            />
        } else if (this.state.isRatingSubmitted) {
            let afterRating = this.state.closeChatData?.afterRatingText;
            let heading2 = Platform.OS == "ios"?afterRating?.heading2?.ios:afterRating?.heading2?.android;
            return <AlertView style={styles.childContainer}
                title={afterRating?.heading1??'Thank you for your feedback.'}
                message={heading2??`Please take some time to rate us on ${Platform.OS == "ios" ? "Apple App" : "Google Play"} Store`}
                primaryButton={{ text: afterRating?.sureText??"Sure", onClick: () => this.handleStoreRating() }}
                secondaryButton={{ text: this.state.closeChatData?.ratingCardText?.notNowText??'Not now', onClick: () => this.closePopup() }}
                onClose={() => this.closePopup()}
                cancellable={false}
            />
        } else if (this.state.isChatClosed) {
            return <AlertView style={styles.childContainer}
                title={this.state.closeChatData?.ratingCardText?.heading1??'How was your chat experience?'}
                message={this.state.closeChatData?.ratingCardText?.heading2??'Rate us on a  scale of 1 to 5'}
                hasStar={true}
                primaryButton={{ text: this.state.closeChatData?.ratingCardText?.submitText??'Submit', onClick: (rating) => this.handleRatingSubmit(rating) }}
                secondaryButton={{ text: this.state.closeChatData?.ratingCardText?.notNowText??'Not now', onClick: () => this.closePopup() }}
                onClose={() => this.closePopup()}
                cancellable={false}
            />
        } else {
            let translatedData = this.props.translatedData ?? [];
            return <AlertView style={styles.childContainer}
                title={translatedData[1] ?? chatFlowStrings.close_chat}
                message={translatedData[2] ?? chatFlowStrings.close_chat_des}
                primaryButton={{ text: translatedData[3] ?? chatFlowStrings.yes, onClick: () => this.handleCloseChat() }}
                secondaryButton={{ text: translatedData[4] ?? chatFlowStrings.cancel, onClick: () => this.closePopup() }}
                onClose={() => this.closePopup()}
            />
        }
    }

    renderView() {
        return <View style={styles.container}>
            {this.alertView()}
        </View>
    }

    render() {
        return (<Modal
            transparent={true}
            visible={this.state.visible}
            onRequestClose={() => {
                this.dismissPopup()
            }}>
            <View style={[{
                flex: 1,
                backgroundColor: "#000000AA",
                padding: 24
            }]}>
                {this._renderOutsideTouchable()}
                {this.renderView()}
                {this._renderOutsideTouchable()}
            </View>
        </Modal>
        );
    }
}

const styles = StyleSheet.create({
    childContainer: {},
    container: {
        backgroundColor: colors.white,
        width: '100%',
        paddingBottom: spacing.spacing16
    },
    titleLayout: {
        ...TextStyle.text_16_bold,
        textAlign: "center",
        marginTop: spacing.spacing8,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
    },
    descLayout: {
        ...TextStyle.text_14_normal,
        textAlign: "center",
        marginTop: spacing.spacing12,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
    },
    primaryButton: {
        ...ButtonStyle.BlueButton,
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
    },
    secondaryButton: {
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
    },
    Rating: {
        alignSelf: "center",
        flexDirection: "row",
        marginVertical: spacing.spacing16
    },
    StarImage: {
        width: 40,
        height: 40,
        margin: spacing.spacing4,
        resizeMode: "cover"
    },
});
