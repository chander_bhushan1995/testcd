import React, { useState } from 'react'
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native'
import colors from '../Constants/colors'
import spacing from '../Constants/Spacing';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { TextStyle } from "../Constants/CommonStyle";

export default function DateInputBox(props) {
    const { errorMessage, containerStyle, inputHeading, minimumDate, date, maximumDate, onDateChange, format = "dd mmm", mode = "date"} = props
    const [showPicker, setShowPicker] = useState(false);

    const visibleDate = () => {
        var dateFormat = require('dateformat');
        return dateFormat(date, format);
    }

    const picker = () => {
        return showPicker ? <DateTimePicker
            minimumDate={minimumDate}
            date={date}
            maximumDate={maximumDate}
            isVisible={showPicker}
            mode={mode}
            onConfirm={(date) => {
                setShowPicker(false)
                onDateChange(date)
            }}
            onCancel={() => setShowPicker(false)}
        /> : null;
    }

    return (
        <View style={containerStyle}>
            <Text style={styles.inputHeading}>{inputHeading}</Text>
            <TouchableOpacity onPress={() => setShowPicker(true)}>
                <View style={styles.inputView}>
                    <Text style={styles.dateText}>{visibleDate()}</Text>
                    <Image style={styles.calender} source={require("../images/ic_calander.webp")} />
                </View>
            </TouchableOpacity>
            {errorMessage?.length ? <Text style={styles.errorText}>{errorMessage}</Text> : null}
            {picker()}
        </View>
    )
}

const styles = StyleSheet.create({
    inputHeading: {
        ...TextStyle.text_14_normal_charcoalGrey,
    },
    errorText: {
        ...TextStyle.text_12_normal,
        color: colors.inoutErrorMessage,
    },
    inputView: {
        borderColor: colors.grey,
        borderWidth: spacing.spacing1,
        borderRadius: spacing.spacing2,
        marginTop: spacing.spacing8,
        flexDirection: "row",
        alignSelf: "center",
    },
    dateText: {
        ...TextStyle.text_14_normal_charcoalGrey,
        flex: 1,
        marginVertical: spacing.spacing12,
        marginLeft: spacing.spacing12
    },
    calender: {
        width: spacing.spacing24,
        height: spacing.spacing24,
        alignSelf: "flex-end",
        marginHorizontal: spacing.spacing12,
        marginVertical: spacing.spacing10
    }
})
