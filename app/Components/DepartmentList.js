import React, {Component} from "react";
import {FlatList, StyleSheet, Text, View} from "react-native";
import DepartmentListItem from './DepartmentListItem'
import { CommonStyle } from "../Constants/CommonStyle";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";

export default class DepartmentList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={style.MainContainer}>
                <FlatList
                    data={this.props.data.questionList}
                    keyExtractor={(item, index) => index.toString()}
                    ListHeaderComponent={this.renderHeader}
                    renderItem={this.renderDepartmentList}
                    ItemSeparatorComponent={() => {
                        return <View style={CommonStyle.separator} />
                    }}
                />
            </View>
        );
    }

    renderHeader = () => {
        return this.props.data.text ? <View>
            <Text style={style.heading}>{this.props.data.text}</Text>
            <View style={CommonStyle.seprater} />
        </View>:null;
    };

    renderDepartmentList = ({item, index}) => {
        return <DepartmentListItem
            onItemClick={() => this.props.onClick(item.text,{...item.params,displayMessage:this.props.data.text})}
            image={item.image}
            title={item.text}
            subtitle={item.subText}/>
    };
}
const style = StyleSheet.create({
    MainContainer: {
        borderRadius: spacing.spacing4,
        overflow: "hidden"
    },
    heading: {
        backgroundColor: colors.soulitudeGrey,
        padding: spacing.spacing16,
        color: colors.charcoalGrey
    }
});
