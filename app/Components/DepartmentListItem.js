import React, {Component} from "react";
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";
import {TextStyle} from "../Constants/CommonStyle";

export default class DepartmentListItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <TouchableOpacity onPress={() => this.props.onItemClick()}>
                <View style={style.MainContainer}>
                    { this.props.image ? <Image source={{uri: this.props.image}} style={style.circleImage}/> : null}
                    <View style={style.textView}>
                        <Text style={[TextStyle.text_14_bold, style.title]}>{this.props.title}</Text>
                        {this.props.subtitle ? <Text style={[TextStyle.text_12_normal, style.subtitle]}>{this.props.subtitle}</Text> : null}
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

const style = StyleSheet.create({
    MainContainer: {
        flexDirection: "row",
        backgroundColor: colors.white,
    },
    textView: {
        flex:1,
        padding: spacing.spacing16,
    },
    title: {
        color: colors.blue
    },
    subtitle: {
        color: colors.grey
    },
    circleImage: {
        width: spacing.spacing28,
        height: spacing.spacing28,
        marginLeft: spacing.spacing16,
        marginTop: spacing.spacing16,
        resizeMode: "contain"
    },
});
