import React, { Component } from "react";
import { StyleSheet, TouchableOpacity, View, Text, Image } from "react-native";
import { TextStyle, ButtonStyle, CommonStyle } from "../Constants/CommonStyle"
import colors from "../Constants/colors"
import spacing from "../Constants/Spacing";
import CheckBox from "react-native-check-box";

export default class DialogLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isChecked: props.propsData?.checkBox?.selected ?? false
        }
    }

    render() {
        let imageUrl = this.props.propsData.imageUrl;
        let image = imageUrl?<Image style={styles.image} source={imageUrl}></Image> : null;
        let titleLayout = this.props.propsData.title ? <Text style={[styles.titleLayout, TextStyle.text_16_bold]}>{this.props.propsData.title}</Text> : null;
        let descLayout = this.props.propsData.message ? <Text style={[styles.descLayout, TextStyle.text_14_normal]}>{this.props.propsData.message}</Text> : null;
        let checkBox = this.props.propsData.checkBox ? (<TouchableOpacity style={styles.checkboxViewContainer} onPress={() => this.setState({ isChecked: !this.state.isChecked })}>
            <CheckBox
                style={styles.checkboxContainer}
                isChecked={this.state.isChecked}
                onClick={() => this.setState({ isChecked: !this.state.isChecked })}
                checkedImage={<Image source={require("../images/ic_check_box.webp")} style={styles.checkbox} />}
                unCheckedImage={<Image source={require("../images/ic_check_box_outline_blank.webp")} style={styles.checkbox} />}
                checkedCheckBoxColor={colors.blue}
                uncheckedCheckBoxColor={colors.grey}
            />
            <Text style={[TextStyle.text_14_normal, { color: this.state.isChecked ? colors.grey : colors.lightGrey }]}>{this.props.propsData.checkBox.text}</Text>
        </TouchableOpacity>) : null;
        let primaryButton = this.props.propsData.primaryButton ? <TouchableOpacity
            style={[styles.primaryButton, ButtonStyle.BlueButton]}
            activeOpacity={.8}
            onPress={() => {
                if (this.props.propsData.primaryButton.onClick != null)
                    this.props.propsData.primaryButton.onClick(this.state.isChecked);
                if (this.props.closePopup != null)
                    this.props.closePopup(true);
            }}
        >
            <Text style={ButtonStyle.primaryButtonText}>{this.props.propsData.primaryButton.text}</Text>
        </TouchableOpacity> : null;
        let secondaryButton = this.props.propsData.secondaryButton ? <TouchableOpacity
            style={[styles.secondaryButton]}
            activeOpacity={.8}
            onPress={() => {
                if (this.props.propsData.secondaryButton.onClick != null)
                    this.props.propsData.secondaryButton.onClick(this.state.isChecked);
                if (this.props.closePopup != null)
                    this.props.closePopup(true);
            }}
        >
            <Text style={ButtonStyle.TextOnlyButton}>{this.props.propsData.secondaryButton.text}</Text>
        </TouchableOpacity> : null;
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => {
                    if (this.props.propsData.onClose != null)
                        this.props.propsData.onClose();
                    if (this.props.closePopup != null)
                        this.props.closePopup(true);
                }}
                >
                    {this.props?.propsData?.isCrossEnabled ? <Image source={require("../images/icon_cross.webp")} style={CommonStyle.crossIconContainer} /> : null}
                </TouchableOpacity>
                {image}
                {titleLayout}
                {descLayout}
                {checkBox}
                {primaryButton}
                {secondaryButton}
            </View>);
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        width: '100%',
        paddingBottom: spacing.spacing16
    },
    image: {
        marginTop: spacing.spacing32,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        marginBottom: spacing.spacing16,
        alignSelf: 'center',
        width: spacing.spacing48,
        height: spacing.spacing48,
        resizeMode: 'contain'
    },
    titleLayout: {
        textAlign: "center",
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
    },
    descLayout: {
        textAlign: "center",
        marginTop: spacing.spacing12,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
    },
    primaryButton: {
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
    },
    secondaryButton: {
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
    },
    checkboxViewContainer: {
        marginHorizontal: spacing.spacing24,
        marginVertical: spacing.spacing12,
        flexDirection: "row",
        alignSelf: "center",
    },
    checkboxContainer: {
        marginRight: spacing.spacing8,
        height: 20,
        width: 20,
        padding: spacing.spacing4,
    },
    checkbox: {
        height: 16,
        width: 16
    }
});
