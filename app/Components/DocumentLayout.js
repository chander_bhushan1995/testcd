import React, { Component } from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import PlatformActivityIndicator from "../CustomComponent/PlatformActivityIndicator.js";
import { DOCUMENT_TYPE, IMAGE_TYPE } from "../Constants/AppConstants";
import { SendMessageStatus } from "../Constants/SendMessageStatus";
import { CommonStyle } from "../Constants/CommonStyle";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";
import * as DocumentType from "../Constants/DocumentTypeConstant";

export default class Document extends Component {
    constructor(props) {
        super(props);
        // It is use for set progress loader false in case of document from history
        // because there is no progress update in case of history document
        this.state = {
            isImageLoading: this.props.documentType != DOCUMENT_TYPE,
        };
    }

    formatSizeUnits(bytes) {
        if (bytes >= 1048576) { bytes = (bytes / 1048576).toFixed(1) + " MB"; }
        else if (bytes >= 1024) { bytes = (bytes / 1024).toFixed(0) + " KB"; }
        else { bytes = "0 B"; }
        return bytes;
    }

    render() {
        var statusView = <View/>;
        var isRetryEnabled = false;        
        if (this.props.status == SendMessageStatus.Pending) {
            this.props.uploadDocument();
        } else if (this.props.status == SendMessageStatus.Sending) {
            statusView = <View style={CommonStyle.statusView}>
                <Image style={CommonStyle.statusImage} source={require("../images/clock.webp")}/>
                <Text style={CommonStyle.messageTimeSendMessage}>Sending</Text>
            </View>
        } else if (this.props.status == SendMessageStatus.NoInternet || this.props.status == SendMessageStatus.Failure) {
            if (this.props.retryCount <= 3) {
                if (this.props.internetStatus) {
                    this.props.uploadDocument();
                } else {
                    statusView = <View style={CommonStyle.statusView}>
                        <Image style={CommonStyle.statusImage} source={require("../images/clock.webp")}/>
                        <Text style={CommonStyle.messageTimeSendMessage}>Sending</Text>
                    </View>
                }
            } else {
                isRetryEnabled = true;
                statusView = <View style={CommonStyle.statusView}>
                    <Text style={[CommonStyle.messageTimeSendMessage, CommonStyle.messageError]}>Message not delivered. Tap to retry</Text>
                </View>
            }  
        } else if (this.props.status == SendMessageStatus.CannotSend) {
            statusView = <View style={CommonStyle.statusView}>
                <Text style={[CommonStyle.messageTimeSendMessage, CommonStyle.messageError]}>Message not delivered.</Text>
            </View>
        } else { 
            statusView = <View style={CommonStyle.statusView}>
                <Image style={CommonStyle.statusImage} source={require("../images/doubleTick.webp")}></Image>
                <Text style={CommonStyle.messageTimeSendMessage}>{"Sent at ".concat(this.props.timeStamp)}</Text>
            </View>
        }

        var container = null;
        if (this.props.documentType === DocumentType.IMAGE) {
            var loadingView = this.state.isImageLoading ? <PlatformActivityIndicator /> : null;
            var overlayView = (this.state.isImageLoading || isRetryEnabled) ? <View style={CommonStyle.loaderContainer}>
                {loadingView}
            </View> : null;

            container = <View style={style.imageContainer}>
                <Image
                    style={CommonStyle.ImageContainer}
                    source={{ uri: this.props.docUrl }}
                    onLoadEnd={() => this.setState({ isImageLoading: false })}
                />
                {overlayView}
            </View>;
        } else if (this.props.documentType === DocumentType.DOCUMENT) {
            let FileDetail = ""
            if (this.props.fileSize != null) {
                FileDetail = this.formatSizeUnits(this.props.fileSize);
                if (this.props.fileType != null && this.props.fileType != "") {
                    FileDetail = FileDetail.concat(" | ").concat(this.props.fileType.toUpperCase());
                }
            } else {
                if (this.props.fileType != null && this.props.fileType != "") {
                    FileDetail = this.props.fileType.toUpperCase();
                }
            }

            container = <View style={style.documentConatiner}>
                <Image source={require("../images/icon_file.webp")} style={style.fileIcon} />
                <View style={style.fileDetailConatiner}>
                    <Text style={[style.fileName, isRetryEnabled && style.fileNameFailure]} ellipsizeMode="tail" numberOfLines={1}>{this.props.fileName}</Text>
                    <Text style={style.fileType}>{FileDetail}</Text>
                </View>
            </View>;
        }

        return (
            <View style={style.MainContainer}>
                <TouchableOpacity disabled={!(isRetryEnabled || (this.props.documentType === DocumentType.IMAGE && !this.state.isImageLoading))}
                    onPress={() => {
                        if (isRetryEnabled) {
                            this.props.uploadDocument();
                        } else {
                            this.props.onImageClick({ uri: this.props.docUrl });
                        }
                    }}
                >
                    {container}
                    {statusView}
                </TouchableOpacity>
            </View>
        );
    }
}

const style = StyleSheet.create({
    MainContainer: {
        flex: 1,
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing16,
        alignSelf: "flex-end"
    },
    imageContainer: {
        marginRight: spacing.spacing16,
    },
    documentConatiner: {
        backgroundColor: colors.white,
        width: 220,
        padding: spacing.spacing12,
        marginRight: spacing.spacing16,
        borderRadius: 4,
        flexWrap: "wrap",
        alignSelf: "flex-end",
        flexDirection: "row"
    },
    fileDetailConatiner: {
        flex: 1,
        justifyContent: "space-between",
    },
    fileNameFailure: {
        color: colors.salmonRed
    },
    fileIcon: {
        marginRight: spacing.spacing8,
        padding: spacing.spacing4,
        width: 28,
        height: 34
    },
    fileName: {
        fontSize: 12,
        fontFamily: "Lato",
        color: colors.charcoalGrey
    },
    fileType: {
        color: colors.grey,
        fontFamily: "Lato",
        fontSize: 10
    }
});
