import {Alert, NativeModules, PermissionsAndroid, Platform} from 'react-native';
import BottomSheet from 'react-native-bottomsheet';
import ImagePicker from 'react-native-image-picker';
import * as Actions from '../Constants/ActionTypes';
import * as DocumentType from '../Constants/DocumentTypeConstant';

import {DocumentPicker, DocumentPickerUtil} from 'react-native-document-picker';
import {CAMERA_DENY_PERMISSION, STORRGAE_DENY_PERMISSION} from '../Constants/AppConstants';

const MAX_FILE_SIZE = 5242880;
const options = {
    quality: 1.0,
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
        skipBackup: true,
    },
};
const chatBrigeRef = NativeModules.ChatBridge;
export default class DocumentUploadUtils {

    plusIconClick(index, _this, onDocumentSelect) {
        if (Platform.OS === 'ios') {
            BottomSheet.showBottomSheetWithOptions(
                {
                    options: [
                        'Take a photo',
                        'Choose from Documents',
                        'Choose from Gallery',
                        'Cancel',
                    ],
                    dark: false,
                    cancelButtonIndex: 4,
                },
                value => {
                    this.selectPhotoTapped(value, index, _this, onDocumentSelect);
                },
            );
        } else {
            BottomSheet.showBottomSheetWithOptions(
                {
                    options: ['Take a photo', 'Choose from Gallery'],
                    dark: false,
                    cancelButtonIndex: 3,
                },
                value => {
                    this.selectPhotoTapped(value, index, _this, onDocumentSelect);
                },
            );
        }
    }

    // @Params index is stand for bottom sheet slected item index
    // @Params itemIndex is stand for flat list item
    // @params _this is stand for refrence of flat list child component
    // @params onDocumentSelect is function to pass the item index slectcted doc uri,url
    selectPhotoTapped(index, itemIndex, _this, onDocumentSelect) {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,
            },
        };
        if (index === 0) {
            if (Platform.OS === 'android' && Platform.Version >= 22) {
                this.checkCameraPermissionAndOpenCamera(_this, itemIndex, onDocumentSelect);
            } else {
                this.openCamera(_this, itemIndex, onDocumentSelect);
            }
        } else if (index === 1) {
            if (Platform.OS === 'android') {
                if (Platform.Version >= 22) {
                    this.checkStoragePermissionAndAccessDocument(_this, itemIndex, onDocumentSelect);
                } else {
                    this.pickDocumentFromGallery(_this, itemIndex, onDocumentSelect);
                }
            } else {
                this.pickDocumentFromFiles(_this, itemIndex, onDocumentSelect);
            }
        } else if (index === 2) {
            ImagePicker.launchImageLibrary(options, response => {
                if (response.didCancel) {
                } else if (response.error) {
                    if (
                        Platform.OS === 'ios' &&
                        response.error === 'Photo library permissions not granted'
                    ) {
                        chatBrigeRef.imagePickerPermissionDenied('library');
                    }
                } else if (response.customButton) {
                } else if (!(response.fileSize > 0 && response.fileSize <= MAX_FILE_SIZE)) {
                    Alert.alert(
                        '',
                        Actions.DOCUMENT_SIZE_ERROR_MSG,
                        [{text: 'Okay, got it'}],
                        {cancelable: true},
                    );
                } else {
                    onDocumentSelect(itemIndex, response.uri, DocumentType.IMAGE, response.path);
                }
            });
        }
    }

    async checkCameraPermissionAndOpenCamera(_this, itemIndex, onDocumentSelect) {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    this.openCamera(_this, itemIndex, onDocumentSelect);
                } else if (granted === PermissionsAndroid.RESULTS.DENIED) {
                    this.storagePermisionError(_this, () => {
                        this.checkCameraPermissionAndOpenCamera(_this);
                    });
                } else {
                    this.showrationalPermissionDailog(_this, STORRGAE_DENY_PERMISSION);
                }
            } else if (granted === PermissionsAndroid.RESULTS.DENIED) {
                this.cameraPermisionError(_this, () => {
                    this.checkCameraPermissionAndOpenCamera(_this);
                });
            } else {
                this.showrationalPermissionDailog(_this, CAMERA_DENY_PERMISSION);
            }
        } catch (err) {
            console.warn(err);
        }
    }

    async checkStoragePermissionAndAccessDocument(_this, itemIndex, onDocumentSelect) {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this.pickDocumentFromGallery(_this, itemIndex, onDocumentSelect);
            } else if (granted === PermissionsAndroid.RESULTS.DENIED) {
                this.storagePermisionError(_this, () => {
                    this.checkStoragePermissionAndAccessDocument(_this);
                });
            } else {
                this.showrationalPermissionDailog(_this, STORRGAE_DENY_PERMISSION);
            }
        } catch (err) {
            console.warn(err);
        }
    }

    cameraPermisionError(_this, checkPermissionAgain) {
        if (_this.DialogViewRef !== undefined) {
            _this.DialogViewRef.showDailog({
                message: CAMERA_DENY_PERMISSION,
                primaryButton: {
                    text: 'Allow', onClick: () => {
                        checkPermissionAgain();
                    },
                },
                secondaryButton: {
                    text: 'Later', onClick: () => {
                    },
                },
                cancelable: false,
                onClose: () => {
                },
            });
        }
    }

    storagePermisionError(_this, checkPermissionAgain) {
        if (_this.DialogViewRef !== undefined) {
            _this.DialogViewRef.showDailog({
                message: STORRGAE_DENY_PERMISSION,
                primaryButton: {
                    text: 'Allow', onClick: () => {
                        checkPermissionAgain();
                    },
                },
                secondaryButton: {
                    text: 'Later', onClick: () => {
                    },
                },
                cancelable: false,
                onClose: () => {
                },
            });
        }
    }

    showrationalPermissionDailog(_this, alertMessage) {
        if (_this.DialogViewRef !== undefined) {
            _this.DialogViewRef.showDailog({
                title: 'We Request Again',
                message: alertMessage,
                primaryButton: {
                    text: 'Go to settings', onClick: () => {
                        chatBrigeRef.openAndroidAppSettings();
                    },
                },
                secondaryButton: {
                    text: 'Later', onClick: () => {
                    },
                },
                cancelable: false,
                onClose: () => {
                },
            });
        }
    }

    openCamera(_this, itemIndex, onDocumentSelect) {
        ImagePicker.launchCamera(options, response => {
            if (response.didCancel) {
            } else if (response.error) {
                if (
                    Platform.OS === 'ios' &&
                    response.error === 'Camera permissions not granted'
                ) {
                    chatBrigeRef.imagePickerPermissionDenied('camera');
                }
            } else if (response.customButton) {
            } else {
                onDocumentSelect(itemIndex, response.uri, DocumentType.IMAGE, response.path, response.fileSize, response.type);

            }
        });
    }

    // This method is used to pick document from Files in iOS
    pickDocumentFromFiles(_this, itemIndex, onDocumentSelect) {
        DocumentPicker.show(
            {
                filetype: [DocumentPickerUtil.allFiles()],
            },
            (error, res) => {
                if (res != null) {
                    let fileType = res.fileName.substring(
                        res.fileName.lastIndexOf('.') + 1);
                    let type = DocumentType.DOCUMENT;
                    if (
                        ['jpg', 'jpeg', 'png'].includes(fileType)
                    ) {
                        type = DocumentType.IMAGE;
                    }
                    if (!DocumentType.supportedDocumentType.includes(fileType)) {
                        this.showDialogForError(_this, Actions.DOCUMENT_TYPE_ERROR_MSG);
                    } else if (!(res.fileSize > 0 && res.fileSize <= MAX_FILE_SIZE)) {
                        this.showDialogForError(_this, Actions.DOCUMENT_SIZE_ERROR_MSG);
                    } else {
                        onDocumentSelect(itemIndex, res.uri, type, res.path, res.fileSize, res.type);
                    }

                }
            },
        );
    }

    // To pick the document from the gallery
    pickDocumentFromGallery(_this, itemIndex, onDocumentSelect) {
        DocumentPicker.show(
            {
                filetype: [DocumentPickerUtil.allFiles()],
            },
            (error, res) => {
                if (res != null) {
                    var documentType = res.fileName.substring(
                        res.fileName.lastIndexOf('.') + 1,
                    );
                    if (!DocumentType.supportedDocumentType.includes(documentType)) {
                        this.showDialogForError(_this, Actions.DOCUMENT_TYPE_ERROR_MSG);

                    } else if (!(res.fileSize > 0 && res.fileSize <= MAX_FILE_SIZE)) {
                        this.showDialogForError(_this, Actions.DOCUMENT_SIZE_ERROR_MSG);
                    } else {
                        let documentType = DocumentType.DOCUMENT;
                        if (['jpg', 'jpeg', 'png'].includes(res.fileName.split('.')[1].toString().toLowerCase())) {
                            documentType = DocumentType.IMAGE;
                        }
                        onDocumentSelect(itemIndex, res.uri, documentType, res.fileName, res.fileSize, res.type);
                    }
                }
            },
        );
    }

    showDialogForError(context, errorMessage) {
        if (context.DialogViewRef !== undefined) {
            context.DialogViewRef.showDailog({
                message: errorMessage,
                primaryButton: {
                    text: 'Okay, got it', onClick: () => {
                    }
                },
                cancelable: true,
                onClose: () => {
                }
            });
        }
    }
}
