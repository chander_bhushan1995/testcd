import React, { Component } from "react";
import { Dimensions, Image, SafeAreaView, StyleSheet, TouchableOpacity, View } from "react-native";
import ImageZoom from 'react-native-image-pan-zoom';
import PlatformActivityIndicator from "../CustomComponent/PlatformActivityIndicator.js";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";
import DeviceInfo from "react-native-device-info";

export default class ImagePreview extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isImageLoading: true,
        };
    }

    render() {
        var marginTop = DeviceInfo.hasNotch() ? spacing.spacing40 : spacing.spacing16;
        const { navigation } = this.props;
        const imageUri = navigation.getParam('imageUri', null);

        var overlayView = this.state.isImageLoading ? <View style={styles.loaderContainer}>
            <PlatformActivityIndicator />
        </View> : null;

        return (
            // <SafeAreaView style={styles.SafeArea}>
                <View style={styles.backgroundImage}>
                    <ImageZoom cropWidth={Dimensions.get('window').width}
                        cropHeight={Dimensions.get('window').height}
                        imageWidth={Dimensions.get('window').width}
                        imageHeight={Dimensions.get('window').height}>
                        <Image style={styles.Image}
                            source={imageUri}
                            onLoadEnd={() => this.setState({ isImageLoading: false })} />
                    </ImageZoom>
                    {overlayView}
                    <TouchableOpacity
                        style={[styles.crossIconContainer, {marginTop: marginTop}]}
                        onPress={() => {
                            this.props.navigation.goBack();
                        }}>
                        <Image
                            source={require("../images/icon_cross.webp")}
                            style={styles.crossImageContainer}
                        />
                    </TouchableOpacity>
                </View>
            // </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({
    SafeArea: {
        flex: 1,
        backgroundColor: colors.charcoalGrey
    },
    backgroundImage: {
        flex: 1,
        backgroundColor: colors.charcoalGrey,
    },
    Image: {
        width: "100%",
        height: "100%",
        justifyContent: 'flex-end',
        resizeMode: 'contain',
    },
    loaderContainer: {
        backgroundColor: colors.charcoalGrey,
        position: "absolute",
        width: "100%",
        height: "100%",
        alignSelf: "center",
        justifyContent: "center",
        alignItems: "center"
    },
    crossIconContainer: {
        height: 48,
        width: 48,
        position: 'absolute',
        justifyContent: 'flex-start',
    },
    crossImageContainer: {
        marginLeft: spacing.spacing16,
        marginTop: spacing.spacing16,
        height: 20,
        width: 20,
        resizeMode: "contain",
    }
});
