import React, {Component} from 'react';
import {View, Text, TouchableOpacity, TextInput, StyleSheet, Image} from 'react-native';
import colors from '../Constants/colors';
import spacing from '../Constants/Spacing';
import {TextStyle} from '../Constants/CommonStyle';

class InputBox extends Component {
    state = {
        borderColor: colors.grey,
        height: 48,
    };

    constructor(props) {
        super(props);
        this.state.height = props.height ? props.height : 48;
        this.textInputRef = props.textInputRef;
    }

    handleFocus = (text) => {
        this.setState({borderColor: colors.blue});
    };

    handleBlur = (text) => {
        this.setState({borderColor: colors.grey});
    };
    updateSize = (height) => {
        if (this.state.height <= height) {
            this.setState({height: height});
        }
    };

    static getDerivedStateFromProps(props, state) {
        const newState = {};
        if (props.errorMessage === undefined || props.errorMessage === null || props.errorMessage === '') {
            newState.borderColor = colors.grey;
        } else if (props.errorMessage !== null && props.errorMessage !== undefined && props.errorMessage !== '') {
            newState.borderColor = colors.inoutErrorMessage;
        }
        return newState;
    }

    render() {
        return (
            <View style={this.props.containerStyle}>
                <Text
                    style={[styles.inputHeading, {color: this.props.headingTextColor ?? colors.charcoalGrey}]}>{this.props.inputHeading}</Text>
                <View style={[styles.inputContainerStyle, {
                    borderColor: this.state.borderColor,
                    height: this.state.height,
                }]}>
                    <TextInput style={[styles.input, (!this.props.editable) && styles.nonEditableInput]}
                               ref={this.textInputRef}
                               underlineColorAndroid="transparent"
                               placeholder={this.props.placeholder}
                               placeholderTextColor={colors.placeHolderColor}
                               autoCorrect={false}
                               autoCompleteType='off'
                               underlineColorAndroid="transparent"
                               autoCapitalize={(this.props.autoCapitalize ? 'characters' : 'none')}
                               textAlignVertical={this.props.textAlignVertical}
                               numberOfLines={this.props.numberOfLines}
                               autoFocus={this.props.autofocus}
                               keyboardType={this.props.keyboardType}
                               multiline={this.props.multiline}
                               contextMenuHidden={this.props.contextMenuHidden}
                               onChangeText={(inputText) => {
                                   this.props.onChangeText(inputText);
                               }}
                               secureTextEntry={this.props.secureTextEntry}
                               onContentSizeChange={(e) => this.updateSize(e.nativeEvent.contentSize.height + 16)}
                               onFocus={() => {
                                   this.props.onFocus?.();
                                   this.handleFocus();
                                   try {
                                       if (this.props.updateFocusChanges !== null && this.props.updateFocusChanges !== undefined) {
                                           this.props.updateFocusChanges();
                                       }
                                       ;
                                   } catch (e) {
                                   }
                               }}
                               onBlur={() => {
                                   this.props.onBlur?.();
                                   this.handleBlur();
                                   try {
                                       if (this.props.updateBlurChanges !== null && this.props.updateBlurChanges !== undefined) {
                                           this.props.updateBlurChanges();
                                       }
                                       ;
                                   } catch (e) {
                                   }
                               }}
                               value={this.props.value}
                               maxLength={this.props.maxLength}
                               editable={this.props.editable}
                    />
                    <TouchableOpacity onPress={()=>{this.props.onRightImageTapped?.()}}>
                        <Image
                          source={this.props.rightImage}
                          style={styles.imageStyle}/>
                    </TouchableOpacity>
                </View>
                <Text style={[TextStyle.text_12_normal, styles.errorText]}>{this.props.errorMessage}</Text>
            </View>
        );
    }
}

export default InputBox;


const styles = StyleSheet.create({
    inputContainerStyle: {
        marginTop: spacing.spacing12,
        padding: spacing.spacing8,
        borderWidth: 1,
        flexDirection: 'row',
    },
    input: {
        fontSize: spacing.spacing14,
        color: colors.charcoalGrey,
        fontFamily: 'Lato-Regular',
        width: '92%',
    },
    inputHeading: {
        fontSize: spacing.spacing12,
        color: colors.charcoalGrey,
        fontFamily: 'Lato-Regular',
    },
    imageStyle: {
        width: spacing.spacing24,
        height: spacing.spacing24,
        alignSelf: 'center',
    },
    errorText: {
        color: colors.inoutErrorMessage,
    },
    nonEditableInput: {
        borderColor: colors.lightGrey,
        color: colors.grey,
    },
});
