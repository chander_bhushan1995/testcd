import React, {Component} from 'react'
import {View, Text, TouchableOpacity, StyleSheet, Image, TextInput} from 'react-native'
import colors from '../Constants/colors'
import spacing from '../Constants/Spacing';
import {TextStyle} from "../Constants/CommonStyle";

class InputBoxDropDown extends Component {
    state = {
        borderColor: colors.lightGrey,
        height: 28
    }

    constructor(props) {
        super(props);
        this.state.height = props.height ? props.height : 28;
    }

    handleFocus = () => {
        this.setState({borderColor: colors.blue});
    };

    handleBlur = () => {
        this.setState({borderColor: colors.grey});
    };
    updateSize = (height) => {
        if (this.state.height <= height)
            this.setState({height: height});
    };

    static getDerivedStateFromProps(props) {
        const newState = {};
        if (props.errorMessage === undefined || props.errorMessage === null || props.errorMessage === "") {
            newState.borderColor = colors.grey;
        } else if (props.errorMessage !== null && props.errorMessage !== undefined && props.errorMessage !== "") {
            newState.borderColor = colors.inoutErrorMessage;
        }
        return newState;
    }

    render() {
        const rightImage = this.props.rightImage ?? require("../images/down_arrow.webp")
        return (
            <View style={this.props.containerStyle}>
                <Text style={styles.inputHeading}>{this.props.inputHeading}</Text>
                <TouchableOpacity style={[{
                    flexDirection: "row",
                    flex: 1,
                    borderColor: this.state.borderColor
                }, styles.touchableOpacity]}
                                  onPress={() => {
                                      this.props.onPress()
                                  }}
                >
                    {
                        this.props.value
                            ? <Text
                                style={[{flex: 1, height: this.state.height, color: colors.charcoalGrey}, styles.inputText]}>{this.props.value}</Text>
                            : <Text style={[{
                                flex: 1,
                                height: this.state.height,
                                color: colors.placeHolderColor
                            }, styles.inputText]}>{this.props?.placeholder}</Text>
                    }
                    <Image style={{justifyContent: "flex-end", alignSelf: "center"}} source={rightImage}/>
                </TouchableOpacity>

                {this.props.errorMessage ?
                    <Text style={[TextStyle.text_12_normal, styles.errorText]}>{this.props.errorMessage}</Text> : null}
                {this.props.fieldDescription ?
                    <Text style={styles.fieldDescription}>{this.props.fieldDescription}</Text> : null}
            </View>
        )
    }
}

export default InputBoxDropDown


const styles = StyleSheet.create({
    touchableOpacity: {
        borderWidth: 1,
        marginTop: spacing.spacing12,
        padding: spacing.spacing8,
        fontSize: spacing.spacing14,
        color: colors.charcoalGrey,
        fontFamily: "Lato-Regular",
    },
    inputHeading: {
        fontSize: spacing.spacing12,
        color: colors.charcoalGrey,
        fontFamily: "Lato-Regular",
    },
    inputText: {
        flex: 1,
        paddingTop: spacing.spacing8,
    },
    errorText: {
        color: colors.inoutErrorMessage,
    },
    fieldDescription: {
        ...TextStyle.text_12_normal,
        color: colors.color_888F97,
        marginTop: spacing.spacing4
    }
})
