import React, {Component} from "react";
import {Image, Keyboard, StyleSheet, TextInput, TouchableOpacity, View} from "react-native";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";
import {SUGGESTION_VIEW_MARGIN_FROM_BOTTOM} from "../Buyback/buybackActions/Constants";
import * as Actions from "../Constants/ActionTypes";
import {removeSpecialCharacters} from '../commonUtil/AppUtils';

let handleAutoSuggestion;
let _this;
const DEFAULT_INPUT_TEXT_HEIGHT=40;
export default class InputLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            height: DEFAULT_INPUT_TEXT_HEIGHT,
            inputMessage: "",
            diffBetweenAutoSuggestionAndInputView:SUGGESTION_VIEW_MARGIN_FROM_BOTTOM-DEFAULT_INPUT_TEXT_HEIGHT
        };
        _this = this;
    }

    componentDidMount() {
        let delayAfterPauseTypingTime=this.props.delayAfterPauseTypingTime !==null && this.props.delayAfterPauseTypingTime!==undefined?this.props.delayAfterPauseTypingTime:200;
        handleAutoSuggestion = this.debounce(function (text) {
            _this.props.getAutosuggestions(text);
        }, delayAfterPauseTypingTime);
    }

    static getDerivedStateFromProps(props, state) {
		const newState = {...state};
		if (props.shouldClearInput) {
			if(this.textInput !==null && this.textInput!== undefined)
				this.textInput.clear();
			newState.inputMessage = "";
			return newState;
		}
		else {
			return state;
		}
    }

    updateSize = height => {
        if (height < 80) {
            this.setState({
                height
            });
            this.props.updateBottomMarginAutoSuggestionView(this.state.diffBetweenAutoSuggestionAndInputView+height);
        }
    };

    render() {
        const {newValue, height} = this.state;
        let textInputContainer = {
            height,
            flex: 1,
            justifyContent: "space-between",
            marginTop: 16,
            marginBottom: 16,
            minHeight: 24,
        };
        return (
            <View style={styles.MainContainer}>
                <TouchableOpacity
                    onPress={() => {
                        this.props.plusIconClick();
                        // Keyboard.dismiss();
                        this.textInput.clear();
                        this.state.inputMessage = "";
                    }}
                >
                    <Image
                        source={require("../images/icon_plus.webp")}
                        style={styles.iconContainer}
                    />
                </TouchableOpacity>
                <TextInput
                    ref={input => {
                        this.textInput = input;
                    }}
                    underlineColorAndroid="transparent"
                    onChangeText={inputMessage => {
                        this.setState({ inputMessage })
                        this.props.autoSuggestionVisible(inputMessage !== "");
                        if (this.props.autoSuggestionEnabled && inputMessage !== "")
                        {
                            handleAutoSuggestion(removeSpecialCharacters(inputMessage));

                        }

                    }}
                    style={[textInputContainer]}
                    editable={true}
                    height={this.state.height}
                    multiline={true}
                    value={this.state.inputMessage}
                    numberOfLines={3}
                    placeholder={"Type a message"}
                    placeholderTextColor={colors.lightGrey}
                    onContentSizeChange={e => {
                        this.updateSize(e.nativeEvent.contentSize.height);
                    }
                    }
                />
                <TouchableOpacity
                    onPress={() => {
                        if (this.state.inputMessage !== "") {
                            let msg = removeSpecialCharacters(this.state.inputMessage);
                            this.props.sendMessage(msg.trim(), msg.trim(), Actions.SEND_MESSAGE);
                            // Keyboard.dismiss();
                            this.textInput.clear();
                            this.state.inputMessage = "";
                            this.props.autoSuggestionVisible(false);
                        }
                    }}
                >
                    <Image
                        source={require("../images/icon_send.webp")}
                        style={styles.iconContainer}
                    />
                </TouchableOpacity>
            </View>
        );
    }

    debounce(func, wait, immediate) {
        let timeout;
        return function () {
            let context = this, args = arguments;
            let later = function () {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            let callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };
}
const styles = StyleSheet.create({
    MainContainer: {
        flexDirection: "row",
        alignItems: "center",
        width: "100%",
        backgroundColor: colors.white,
        shadowOffset: {height: -4},
        shadowOpacity: 0.12,
        shadowRadius: 2,
        shadowColor: colors.black,
        elevation: 12,
    },
    iconContainer: {
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        height: 24,
        width: 24,
        resizeMode: "contain",
    }
});
