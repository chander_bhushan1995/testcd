import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";

export default class InternetBar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let text = this.props.isConnected ? "You’re back online." : "No internet connection found. Please check and retry.";
        if (this.props.showInternetBar) {
            return (
                <View style={[style.MainContainer, !this.props.isConnected && style.MainContainerDisconnected]}>
                    <Text style={style.Text}>{text}</Text>
                </View>
            );
        } else {
            return null;
        }
    }
}

const style = StyleSheet.create({
    MainContainer: {
        padding: spacing.spacing8,
        backgroundColor: colors.seaGreen,
    },
    MainContainerDisconnected: {
        backgroundColor: colors.salmonRed,
    },
    Text: {
        alignSelf: "center",
        fontSize: 12,
        fontFamily: "Lato",
        fontWeight: "bold",
        color: colors.white
    }
});
