import React, {Component} from "react";
import { StyleSheet, Text, View} from "react-native";
import PlatformActivityIndicator from "../CustomComponent/PlatformActivityIndicator.js";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";

export default class Loader extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.isLoading) {
            return (
                <View style={[style.MainContainer,this.props.loaderStyle]}>
                    <View style={style.ActivityIndicatorContainer}>
                        <PlatformActivityIndicator
                        size={this.props.size}
                        color={this.props.color}
                        />
                    </View>
                    <Text style={style.TextStyle}>{this.props.loadingMessage}</Text>
                </View>
            );
        } else {
            return null;
        }
    }
}

const style = StyleSheet.create({
    MainContainer: {
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
    },
    ActivityIndicatorContainer: {
        justifyContent: "center",
        alignItems: "center",
        height: 60,
    },
    ActivityIndicator: {
        justifyContent: "center",
        alignItems: "center",
    },
    TextStyle: {
        marginTop: spacing.spacing12,
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center",
        color: colors.charcoalGrey
    }
});
