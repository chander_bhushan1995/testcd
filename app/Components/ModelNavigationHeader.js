import React, {Component} from "react";
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";

export default class ModelNavigationHeader extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <TouchableOpacity
                    style={styles.crossIconContainer}
                    onPress={() => {
                        this.props.backTapped();
                    }}>
                    <Image
                        source={require("../images/icon_editFormBack.webp")}
                        style={styles.crossImageContainer}
                    />
                </TouchableOpacity>
                <Text style={styles.headerText}>{this.props.headerText}</Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    mainContainer: {
        flexDirection: "row",
        backgroundColor: colors.white,
        paddingLeft: 6,
        paddingRight: 20,
        paddingTop: 10,
        paddingBottom: 8,
    },
    crossIconContainer: {
        height: 48,
        width: 48,
        padding:14,
        justifyContent: 'flex-start',
    },
    headerText: {
        fontSize: 16,
        fontFamily: "Lato",
        paddingTop:12,
        fontWeight: "500",
        lineHeight: 24,
        color: colors.charcoalGrey,
        marginRight: spacing.spacing4,
    },
    crossImageContainer: {
        height: 20,
        width: 20,
        tintColor: colors.charcoalGrey,
    }
});
