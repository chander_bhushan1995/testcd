import React, {Component} from "react";
import PropTypes from "prop-types";
import {Animated, Modal, PanResponder, TouchableOpacity, View, StyleSheet} from "react-native";

const SUPPORTED_ORIENTATIONS = [
    "portrait",
    "portrait-upside-down",
    "landscape",
    "landscape-left",
    "landscape-right"
];

export default class RBSheet extends Component {
    constructor() {
        super();
        this.state = {
            modalVisible: false,
            animatedHeight: new Animated.Value(0),
            pan: new Animated.ValueXY()
        };

        this.createPanResponder();
    }

    createPanResponder() {
        this.panResponder = PanResponder.create({
            onStartShouldSetPanResponder: () => this.props.closeOnSwipeDown,
            onPanResponderMove: (e, gestureState) => {
                gestureState.dy < 0
                    ? null
                    : Animated.event([null, {dy: this.state.pan.y}])(e, gestureState);
            },
            onPanResponderRelease: (e, gestureState) => {
                if (this.props.height / 4 - gestureState.dy < 0) {
                    this.setModalVisible(false);
                } else {
                    Animated.spring(this.state.pan, {toValue: {x: 0, y: 0}}).start();
                }
            }
        });
    }

    setModalVisible(visible) {
        const {height, duration, onClose} = this.props;
        if (visible) {
            this.setState({modalVisible: visible});
            Animated.timing(this.state.animatedHeight, {
                toValue: height,
                duration: duration
            }).start();
        } else {
            Animated.timing(this.state.animatedHeight, {
                toValue: 0,
                duration: duration
            }).start(() => {
                this.setState({modalVisible: visible});
                this.state.pan.setValue({x: 0, y: 0});
                if (typeof onClose === "function") onClose();
            });
        }
    }

    open() {
        this.setModalVisible(true);
    }

    openWithIndex(index) {
        this.setState({index: index});
        this.setModalVisible(true);
    }

    index() {
        return this.state.index;
    }

    close() {
        this.setModalVisible(false);
    }

    updateHeight(height) {
        var animatedObj = this.state.animatedHeight;
        animatedObj._value = height;
        this.setState({animatedHeight: animatedObj});
        // Animated.timing(this.state.animatedHeight, {
        //     toValue: height,
        //     duration: this.props.duration
        // }).start(()=>{});

    }

    render() {
        const {closeOnPressMask, children, customStyles} = this.props;
        const panStyle = {
            transform: this.state.pan.getTranslateTransform()
        };
        if (this.state.modalVisible) {
            return (
                <Modal
                    transparent={true}
                    animationType="none"
                    visible={this.state.modalVisible}
                    supportedOrientations={SUPPORTED_ORIENTATIONS}
                    onRequestClose={() => {
                        this.setModalVisible(false);
                    }}
                >
                    <View style={[styles.wrapper, customStyles.wrapper]}>
                        <TouchableOpacity
                            style={styles.mask}
                            activeOpacity={1}
                            onPress={() => (closeOnPressMask ? this.close() : {})}
                        />
                        <Animated.View
                            {...this.panResponder.panHandlers}
                            style={[
                                panStyle,
                                styles.container,
                                customStyles.container,
                                {height: this.state.animatedHeight}
                            ]}
                        >
                            {children}
                        </Animated.View>
                    </View>
                </Modal>
            );
        } else {
            return null;
        }
    }
}

RBSheet.propTypes = {
    height: PropTypes.number,
    duration: PropTypes.number,
    closeOnSwipeDown: PropTypes.bool,
    closeOnPressMask: PropTypes.bool,
    customStyles: PropTypes.object,
    onClose: PropTypes.func
};

RBSheet.defaultProps = {
    height: 260,
    duration: 300,
    closeOnSwipeDown: true,
    closeOnPressMask: true,
    customStyles: {}
};

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: "#00000077"
    },
    mask: {
        flex: 1,
        backgroundColor: "transparent"
    },
    container: {
        backgroundColor: "white",
        width: "100%",
        height: 0,
        overflow: "hidden"
    }
});
