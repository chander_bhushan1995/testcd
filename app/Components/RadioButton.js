import React, { Component } from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import spacing from '../Constants/Spacing';
import fontSize from '../Constants/FontSizes';
import colors from "../Constants/colors";
export default class RadioButton extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onClick} activeOpacity={0.8} style={styles.radioButton}>
                <View style={[styles.radioButtonHolder, { height: this.props.data.size, width: this.props.data.size, borderColor: this.props.data.color }]}>
                    {
                        (this.props.data.selected)
                            ?
                            (<View style={[styles.radioIcon, { height: this.props.data.size / 2, width: this.props.data.size / 2, backgroundColor: this.props.data.color }]}></View>)
                            :
                            null
                    }
                </View>
                <Text style={[styles.label, { color: colors.charcoalGrey }]}>{this.props.data.label}</Text>
            </TouchableOpacity>
        );
    }
}
const styles = StyleSheet.create(
    {
        container:
        {
            flex: 1,
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
            paddingHorizontal: spacing.spacing24
        },

        radioButton:
        {
            flexDirection: 'row',
            marginBottom: spacing.spacing10,
            marginRight: spacing.spacing10,
            marginTop: spacing.spacing10,
            alignItems: 'center',
            justifyContent: 'center'
        },

        radioButtonHolder:
        {
            borderRadius: 50,
            borderWidth: 2,
            justifyContent: 'center',
            alignItems: 'center'
        },

        radioIcon:
        {
            borderRadius: 50,
            justifyContent: 'center',
            alignItems: 'center'
        },

        label:
        {
            marginLeft: spacing.spacing10,
            fontSize: fontSize.fontSize14,
            fontFamily: "Lato",
        },

        selectedTextHolder:
        {
            position: 'absolute',
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: colors.grey,
            padding: spacing.spacing16,
            justifyContent: 'center',
            alignItems: 'center'
        },

        selectedText:
        {
            fontSize: 18,
            color: 'white'
        }
    });
