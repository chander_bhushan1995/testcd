import React, {Component} from "react";
import {StyleSheet, Text, View, Image} from "react-native";
import CheckBox from "react-native-check-box";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";
import InputBox from './InputBox'

var selectedQuestionsId;

export default class RatingQuestionnaireItem extends Component {
    constructor(props) {
        super(props);
        selectedQuestionsId = this.props.selectedQuestionsId;
        this.state = {
            isChecked: false,
            fontWeight: "normal",
            ratingFeedback: ""
        };
    }

    render() {
        var item = this.props.questionListObj;
        let textContainer = {
            flex: 1,
            justifyContent: "space-between",
            marginTop: 16,
            marginBottom: 16,
            fontSize: 16,
            fontFamily: "Lato",
            lineHeight: 26,
            fontWeight: this.state.fontWeight
        };
        if (this.props.questionListObj == "add_textinput") {
            return (
                <InputBox
                    containerStyle={styles.textInputContainer}
                    inputHeading={this.props.leaveComment??"Leave a comment(Optional)"}
                    multiline={true}
                    onChangeText={ratingFeedback => this.props.ratingFeedbackComment(ratingFeedback)}
                />
            );
        } else {
            return (
                <View style={styles.MainContainer}>
                    <Text style={[textContainer]}>{item.Question}</Text>
                    <CheckBox
                        style={styles.checkboxContainer}
                        isChecked={this.state.isChecked}
                        onClick={() => {
                            let fontWeight = this.state.isChecked ? "normal" : "bold";

                            if (!this.state.isChecked) {
                                selectedQuestionsId.push(item.id);
                            } else {
                                var index = selectedQuestionsId.indexOf(item.id);
                                if (index !== -1) selectedQuestionsId.splice(index, 1);
                            }
                            if (selectedQuestionsId.length > 0) {
                                this.props.disbaleSubmitButton(false);
                            } else {
                                this.props.disbaleSubmitButton(true);
                            }
                            this.setState({
                                isChecked: !this.state.isChecked,
                                fontWeight: fontWeight
                            });
                        }}
                        checkedImage={<Image source={require("../images/ic_check_box.webp")} style={styles.checkbox}/>}
                        unCheckedImage={<Image source={require("../images/ic_check_box_outline_blank.webp")} style={styles.checkbox}/>}
                        checkedCheckBoxColor={colors.blue}
                        uncheckedCheckBoxColor={colors.grey}
                    />
                </View>
            );
        }
    }
}
const styles = StyleSheet.create({
    MainContainer: {
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        flexDirection: "row",
        alignItems: "center",
        width: "100%"
    },
    checkboxContainer: {
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing12,
        height: 48,
        width: 48,
        padding: spacing.spacing16,
        justifyContent: "flex-end"
    },
    textInputContainer: {
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        marginTop: spacing.spacing32,
        marginBottom: spacing.spacing16
    },
    checkbox: {
        height: 16,
        width: 16
    }
});
