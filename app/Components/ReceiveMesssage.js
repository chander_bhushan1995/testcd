import React, {Component} from "react";
import {StyleSheet, Text, View} from "react-native";
import Hyperlink from 'react-native-hyperlink';
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";

export default class ReceiveMesssage extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={style.MainContainer}>
                <Hyperlink linkDefault={true} linkStyle={{ textDecorationLine: 'underline' }}>
                    <Text style={style.receiveMessage}>{this.props.messageBody}</Text>
                </Hyperlink>
            </View>
        );
    }
}

const style = StyleSheet.create({
    MainContainer: {
    },
    receiveMessage: {
        lineHeight: 20,
        flexDirection: "row",
        flexWrap: "wrap",
        color: colors.charcoalGrey,
        padding: spacing.spacing12,
        borderRadius: 4,
        borderBottomLeftRadius: 0,
        alignSelf: "flex-start",
        backgroundColor: colors.white,
        fontSize: 14,
        fontFamily: "Lato",
        overflow: "hidden"
    }
});
