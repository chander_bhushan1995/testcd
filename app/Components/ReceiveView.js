import React, { useState, useEffect } from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import * as Actions from "../Constants/ActionTypes";
import ReceiveMesssage from "./ReceiveMesssage";
import Typing from "./Typing";
import StarRating from "./StarRating";
import AppReview from "./AppReview";
import AgentSideDocument from "./AgentSideDocumentLayout";
import ChatbotComponent from "../ChatbotComponent/ChatbotComponent";
import spacing from "../Constants/Spacing";
import { createAnimatableComponent } from 'react-native-animatable';
import TypingAnimatedComponent from "../ChatbotComponent/TypingAnimatedComponent";
import { CommonStyle } from "../Constants/CommonStyle";
import { chatAnimationTimes } from "./../Chat/ChatUtils";

const AnimatableImage = createAnimatableComponent(Image);

export default function ReceiveView(props) {
    const { data, action, animationCompletion } = props;
    const { type, value, docUrl, docType, timeStamp, fileName, fileType, fileSize } = data;
    const [logoAnimationCompleted, setLogoAnimationCompleted] = useState(data?.animationCompleted ?? false)
    const [animationCompleted, setAnimationCompleted] = useState(data?.animationCompleted ?? false)

    useEffect(() => {
        if (animationCompleted && data?.animationCompleted !== true) {
            animationCompletion()
        }
    }, [animationCompleted])

    const nonListView = () => {
        switch (type) {
            case Actions.TYPING:
                return <Typing />
            case Actions.RECEIVE_MESSAGE:
                return <ReceiveMesssage messageBody={value} />;
            case Actions.AGENT_SIDE_DOCUMENT:
                return <AgentSideDocument
                    docUrl={docUrl}
                    documentType={docType}
                    fileName={fileName}
                    fileType={fileType}
                    fileSize={fileSize}
                    onImageClick={imageUri => action(Actions.AGENT_SIDE_DOCUMENT, null, { imageUri: imageUri })}
                />;
            case Actions.RATING:
                return <StarRating data={value} ratingSubmit={rating => action(Actions.RATING, null, { rating: rating })} />;
            case Actions.REVIEW:
                return <AppReview data={value} onClick={(takeReview, message) => action(Actions.REVIEW, null, { takeReview: takeReview, message: message })}/>;
            default:
                return null;
        }
    }

    const timeStampView = () => {
        if (animationCompleted && (type == Actions.RECEIVE_MESSAGE || type == Actions.AGENT_SIDE_DOCUMENT || (type == Actions.BOT && !(value.find(data => (data.actionType == "singleSelect" || data.actionType == "dateTimeInput")))))) {
            return <Text style={CommonStyle.messageTimeReceiveMessage}>{timeStamp}</Text>;
        } else {
            return null
        }
    }

    const receiveView = () => {
        if (type == Actions.BOT) {
            return <ChatbotComponent
                animationCompleted={(animationCompleted)}
                show={logoAnimationCompleted}
                animationCompletion={() => setAnimationCompleted(true)}
                botData={value}
                onOpenWebViewAndSendMessage={option => action(Actions.BOT, Actions.BOT_WEBVIEW, option)}
                onSendMessage={messageText => action(Actions.BOT, messageText.params.actionType, { messageText: messageText })}
            />;
        } else {
            return (logoAnimationCompleted ? <TypingAnimatedComponent
                isTyping={type == Actions.TYPING}
                isAlreadyAnimated={animationCompleted}
                animationCompleted={() => setAnimationCompleted(true)}
            >
                {nonListView()}
            </TypingAnimatedComponent> : null);
        }
    }

    return (
        <View style={style.MainContainer}>
            <AnimatableImage
                animation={logoAnimationCompleted ? null : "zoomIn"}
                delay={chatAnimationTimes.logo.delay}
                duration={chatAnimationTimes.logo.duration}
                source={require("../images/icon_agent.webp")}
                style={style.circleImage}
                onAnimationEnd={() => setLogoAnimationCompleted(true)}
            />
            <View style={style.AnimatableView}>
                {receiveView()}
                {timeStampView()}
            </View>
        </View>
    );
}

const style = StyleSheet.create({
    MainContainer: {
        flex: 1,
        flexDirection: "row",
        alignSelf: "auto",
        padding: spacing.spacing16,
    },
    AnimatableView: {
        flex: 1,
        marginLeft: spacing.spacing8,
    },
    circleImage: {
        width: 40,
        height: 40,
    },
});
