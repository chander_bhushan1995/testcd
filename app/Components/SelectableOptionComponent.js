import spacing from "../Constants/Spacing";
import {TextStyle} from "../Constants/CommonStyle";
import colors from "../Constants/colors";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";
import React, {useEffect, useState} from "react";

const SelectableOptionComponent = (props) => {

    const [isSelected, setIsSelected] = useState(false)

    useEffect(() => {
        setIsSelected(props.isSelected)
    })

    const containerStyle = isSelected ? style.selectedContainer : style.unselectedContainer
    const textStyle = isSelected ? style.selectedText : style.unselectedText

    return (

        <TouchableOpacity style={containerStyle}
                          onPress={() => {
                              let updateState = !isSelected;
                              setIsSelected(updateState)
                              props.onSelectionUpdate(updateState)

                          }}>
            <Text style={textStyle}>{props.itemData.name}</Text>
        </TouchableOpacity>
    )
}

const style = StyleSheet.create({
    selectedContainer: {
        paddingHorizontal: spacing.spacing12,
        paddingVertical: spacing.spacing14,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: colors.color_0282F0,
        backgroundColor: colors.color_0282F0,
        marginRight: spacing.spacing12,
        marginVertical: spacing.spacing10,
        justifyContent: 'center'
    },

    unselectedContainer: {
        paddingHorizontal: spacing.spacing12,
        paddingVertical: spacing.spacing14,
        borderRadius: 4,
        borderColor: colors.color_0282F0,
        borderWidth: 1,
        backgroundColor: colors.white,
        marginRight: spacing.spacing12,
        marginVertical: spacing.spacing10,
        justifyContent: 'center',
    },

    selectedText: {
        ...TextStyle.text_14_bold,
        color: colors.white,
    },

    unselectedText: {
        ...TextStyle.text_14_bold,
        color: colors.color_0282F0,
    }
});

export default SelectableOptionComponent
