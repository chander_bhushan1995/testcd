import React, { Component } from "react";
import { StyleSheet, Text, TouchableOpacity, View, Image } from "react-native";
import { SendMessageStatus } from "../Constants/SendMessageStatus";
import Hyperlink from 'react-native-hyperlink';
import { CommonStyle } from "../Constants/CommonStyle";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";
import {chatManager} from '../Navigation';

export default class SendMesssage extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var statusView = <View/>;
        var isRetryEnabled = false;
        if (this.props.status == SendMessageStatus.Pending) {
            this.props.sendMessage();
        } else if (this.props.status == SendMessageStatus.Sending) {
            statusView = <View style={CommonStyle.statusView}>
                <Image style={CommonStyle.statusImage} source={require("../images/clock.webp")}/>
                <Text style={CommonStyle.messageTimeSendMessage}>Sending</Text>
            </View>
        } else if (this.props.status == SendMessageStatus.NoInternet || this.props.status == SendMessageStatus.Failure) {
            if (this.props.retryCount <= 3) {
                if (this.props.internetStatus) {
                    this.props.sendMessage();
                } else {
                    statusView = <View style={CommonStyle.statusView}>
                        <Image style={CommonStyle.statusImage} source={require("../images/clock.webp")}/>
                        <Text style={CommonStyle.messageTimeSendMessage}>Sending</Text>
                    </View>
                }
            } else {
                isRetryEnabled = true;
                statusView = <View style={CommonStyle.statusView}>
                    <Text style={[CommonStyle.messageTimeSendMessage, CommonStyle.messageError]}>Message not delivered. Tap to retry</Text>
                </View>
            }
        } else if (this.props.status == SendMessageStatus.CannotSend) {
            statusView = <View style={CommonStyle.statusView}>
                <Text style={[CommonStyle.messageTimeSendMessage, CommonStyle.messageError]}>Message not delivered.</Text>
            </View>
        } else {
            let sentAtText = (chatManager?.getSentAtText() ?? "Sent at ") + " " + this.props.timeStamp;
            statusView = <View style={CommonStyle.statusView}>
                <Image style={CommonStyle.statusImage} source={require("../images/doubleTick.webp")}></Image>
                <Text style={CommonStyle.messageTimeSendMessage}>{sentAtText}</Text>
            </View>
        }

        return (
            <TouchableOpacity disabled={!isRetryEnabled}
                onPress={() => {
                    if (isRetryEnabled) {
                        this.props.sendMessage();
                    }
                }}
            >
                <Hyperlink linkDefault={true} linkStyle={{ textDecorationLine: 'underline' }}>
                    <Text style={style.sendMessage}>{this.props.messageBody}</Text>
                </Hyperlink>
                {statusView}
            </TouchableOpacity>
        );
    }
}

const style = StyleSheet.create({
    sendMessage: {
        marginTop: spacing.spacing16,
        marginRight: spacing.spacing16,
        marginLeft: 80,
        flexDirection: "row",
        flexWrap: "wrap",
        color: colors.white,
        borderRadius: 4,
        padding: spacing.spacing12,
        lineHeight: 20,
        alignSelf: "flex-end",
        backgroundColor: colors.blue,
        fontSize: 14,
        fontFamily: "Lato",
        overflow: "hidden",
        textAlign: "left"
    },
});
