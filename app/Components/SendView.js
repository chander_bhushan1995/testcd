import React, { useState, useEffect } from "react";
import { createAnimatableComponent } from 'react-native-animatable';
import { View } from "react-native";
import * as Actions from "../Constants/ActionTypes";
import SendMesssage from "./SendMesssage";
import DocumentLayout from "./DocumentLayout";
import ChatbotTagsSendView from '../ChatbotComponent/ChatbotTagsSendView';

const AnimatableView = createAnimatableComponent(View);

export default function SendView(props) {
    const { data, action, internetStatus, animationCompletion } = props;
    const { type, value, status, retryCount, docUrl, docType, timeStamp, fileName, fileType, fileSize, pickedFrom } = data;
    const [animationCompleted, setAnimationCompleted] = useState(data?.animationCompleted ?? false)

    useEffect(() => {
        if (animationCompleted && data?.animationCompleted !== true) {
            animationCompletion()
        }
    }, [animationCompleted])

    const sendView = () => {
        switch (type) {
            case Actions.SEND_MESSAGE:
                return <SendMesssage
                    messageBody={value}
                    timeStamp={timeStamp}
                    status={status}
                    internetStatus={internetStatus}
                    retryCount={retryCount}
                    sendMessage={() => action(Actions.SEND_MESSAGE, null, { data: data.data })}
                />;
            case Actions.DOCUMENT:
                return <DocumentLayout
                    docUrl={docUrl}
                    documentType={docType}
                    timeStamp={timeStamp}
                    status={status}
                    fileName={fileName}
                    fileType={fileType}
                    fileSize={fileSize}
                    internetStatus={internetStatus}
                    retryCount={retryCount}
                    onImageClick={imageUri => action(Actions.DOCUMENT, "onImageClick", { imageUri: imageUri })}
                    uploadDocument={() => action(Actions.DOCUMENT, "uploadDocument", { docUrl: docUrl, pickedFrom: pickedFrom })}
                />;
            case Actions.SEND_MESSAGE_TAGS:
                return <ChatbotTagsSendView
                    messageBody={value}
                    timeStamp={timeStamp}
                    status={status}
                    internetStatus={internetStatus}
                    retryCount={retryCount}
                    sendMessage={() => action(Actions.SEND_MESSAGE, null, { data: data.data })
                    }
                />;
            default:
                return null;
        }
    }

    return (
        <AnimatableView
            animation={animationCompleted ? null : "fadeInUp"}
            delay={350}
            duration={350}
            contentInsetAdjustmentBehavior="automatic"
            onAnimationEnd={() => setAnimationCompleted(true)}
        >
            {sendView()}
        </AnimatableView>
    );
}