import React, { Component } from "react";
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";

var maxRating = 5;

export default class StarRating extends Component {

    constructor() {
        super();
        this.state = {
            defaultRating: maxRating,
            isSubmitButtonDisabled: false
        };
    }

    render() {
        var Star = require("../images/star.webp");
        var Star_With_Border = require("../images/star_unselected.webp");
        let reactNativeRatingBar = [];
        for (var i = 1; i <= maxRating; i++) {
            let value = i;
            reactNativeRatingBar.push(
                <TouchableOpacity activeOpacity={1.0} key={i} onPress={() => this.setState({ defaultRating: value })}>
                    <Image style={styles.StarImage} source={value <= this.state.defaultRating ? Star : Star_With_Border} />
                </TouchableOpacity>
            );
        }
        return (
            <View style={styles.MainContainer}>
                <Text style={styles.Heading}>{this.props?.data?.ratingCardText?.heading1 ?? "How was your chat experience?"}</Text>
                <View style={styles.Rating}>{reactNativeRatingBar}</View>
                <TouchableOpacity
                    disabled={this.state.isSubmitButtonDisabled}
                    style={styles.submitButton}
                    onPress={() => {
                        this.setState({ isSubmitButtonDisabled: true });
                        this.props.ratingSubmit(this.state.defaultRating);
                    }}>
                    <Text style={styles.TextStyle}>{this.props?.data?.ratingCardText?.submitText ?? 'Submit'}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    MainContainer: {
        marginBottom: spacing.spacing16,
        borderRadius: 4,
        overflow: "hidden",
        backgroundColor: colors.white
    },
    StarImage: {
        width: 40,
        height: 40,
        margin: spacing.spacing4,
        resizeMode: "cover"
    },
    TextStyle: {
        color: colors.white,
        textAlign: "center",
        fontWeight: "bold",
        fontFamily: "Lato",
        fontSize: 16
    },
    Heading: {
        margin: spacing.spacing12,
        lineHeight: 20,
        color: colors.charcoalGrey,
        fontFamily: "Lato",
        fontSize: 16
    },
    Rating: {
        alignSelf: "center",
        flexDirection: "row",
        marginBottom: spacing.spacing8
    },
    submitButton: {
        padding: spacing.spacing12,
        margin: spacing.spacing16,
        backgroundColor: colors.blue,
        borderRadius: 4
    }
});
