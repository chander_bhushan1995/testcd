import React, {Component} from "react";
import {Image, StyleSheet, View} from "react-native";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";

export default class Typing extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={style.MainContainer}>
                <Image source={require("../images/typing.gif")} style={style.typingImage} />
            </View>
        );
    }
}

const style = StyleSheet.create({
    MainContainer: {
        borderRadius: 4,
        width: 64,
        height: 40,
        flex: 1,
        backgroundColor: colors.soulitudeGrey,
    },
    typingImage: {
        margin: spacing.spacing4,
        height: 32,
    },
});
