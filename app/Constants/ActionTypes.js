// All the constant define here
export const SHOW_START_CHAT = 'show_start_chat';
export const SEND_MESSAGE = 'send_message';
export const SEND_MESSAGE_TAGS = 'send_message_tags';
export const SEND_MESSAGE_STATUS = 'send_message_status';
export const RECEIVE_MESSAGE = 'receive_message';
export const GET_DATA_FOR_CLOSE_CHAT = 'get_data_for_close_chat';
export const START_CHAT = 'start_chat';
export const TYPING = 'typing';
export const TYPING_PAUSED = 'typing_paused';
export const HISTORY = 'history';
export const DOCUMENT = 'document';
export const INTERNET_ERROR = 'internet_error';
export const TECHNICAL_ERROR = 'technical_error';
export const CLOSED_CHAT_ERROR = 'closed_chat_error';
export const RETRY = 'retry';
export const ANIMATION_DONE = 'ANIMATION_DONE';
export const ADD_SEND_RECEIVE_MESSAGE = 'selected_department';
export const AGENT_SIDE_DOCUMENT = 'agent_side_document';
export const RESET = 'reset';
export const RATING = 'rating';
export const REVIEW = 'review';
export const RATING_SUBMIT_SUCCESS = 'rating_submit_success';
export const RATING_SUBMIT_FAILURE = 'rating_submit_failure';
export const TECHNICAL_ERROR_MESSAGE = "Looks like we're facing issues communicating with our servers. Please try after some time.";
export const CLOSED_CHAT_ERROR_MESSAGE = "The conversation has been closed. If your query was not resolved, you can chat with us again.";
export const CLOSED_CHAT_MESSAGE = "The conversation has been closed. If your query was not resolved, you can chat with us again.";
export const INTERNET_ERROR_MESSAGE = 'No internet';
export const INTERNET_ERROR_MESSAGE_BUYBACK = 'Please check your internet';
export const NETWORK_FAILED = 'Network request failed'
export const REMOVE_VIEW_FROM_LIST = 'removeViewFromList';
export const REMOVE_CHAT_BOT_VIEW_FROM_LIST = 'removeChatBotViewFromList';
export const UPDATE_AUTO_SUGGESTION_ENABLED = 'update_auto_suggestion_enabled';
export const NEAGATIVE_RATING_QUESTIONS = 'neagative_rating_questions';
export const DOCUMENT_TYPE_ERROR_MSG = 'File not supported';
export const DOCUMENT_SIZE_ERROR_MSG = 'File must not be Empty. Maximum file size supported 5Mb';
export const BOT = 'bot';
export const BOT_WEBVIEW = 'bot_webview';
export const TAGS_INPUT = 'tags input';
export const TAGS_INPUT_OTHERS = 'tags input others';
export const DATE_TIME = 'date time';
export const INPUT_ENABLED = 'input enabled';
export const UPDATE_CONNECTION_STATUS = 'update connection status';
export const PARSE_QUESTION_ANSWER_RESPONSE = 'parse question answer response';
export const SUBMIT_PRESSED = 'submit pressed';
export const NAME_EMAIL_SUBMITTED = 'name email submitted';
export const API_FAILED = 'api failed';
export const CHANGE_STATE_OF_CHECKBOX = 'change state of checkbox';
export const LOADING_BUTTON = 'Loading Button';
export const UPDATE_BOTTOM_VIEW = 'update bottom view';
export const GET_TRANSLATIONS = 'get translations';
export const START_CHAT_BUTTON = 'start chat button';

export const AUTO_SUGGESTIONS_API_SUCCESS = 'auto_suggestions_api_success';
export const AUTO_SUGGESTIONS_CLOSE = 'auto_suggestions_close';
export const AUTO_SUGGESTIONS_API_FAILURE = 'auto_suggestions_api_failure';
export const SUGGESTIONS_SELECTION = 'suggestions_selection';
export const SUGGESTIONS_SELECTION_RESET = 'suggestions_selection_reset';
export const SUGGESTIONS_VIEW_MARGIN_BOTTOM_UPDATE = 'suggestions_view_margin_bottom_update';
export const AUTO_SUGGESTION_VISIBLE = 'auto_suggestion_visible';
export const SHOW_DATETIME_PICKER = 'SHOW_DATETIME_PICKER';
export const HIDE_DATETIME_PICKER = 'HIDE_DATETIME_PICKER';
export const RESET_BOTTOM_VIEW = 'reset_bottom_view';

/************** RC Calculator *************/

export const GET_RC_QUESTIONS = "get_rc_questions";
export const GET_NEXT_QUESTION = "get_next_question";
export const VERIFY_ANSWERED_QUESTION = "verify_answered_question";

export const RC_LAUNCHER_LOADER_UPDATE = "rc_launcher_loader_update";
export const RC_QUESTIONS_LOADER_UPDATE = "rc_questions_loader_update";
export const CHECK_RISK_CALCULATOR_START_SUCCESS = "checkRiskCalculatorStartSuccess";
export const CHECK_RISK_CALCULATOR_END_SUCCESS = "checkRiskCalculatorEndSuccess";
export const CHECK_RISK_CALCULATOR_END_FAILURE = "checkRiskCalculatorEndDFailure";

export const REQUEST_SCREEN_COMPONENTS = "REQUEST_SCREEN_COMPONENTS";
export const REQUEST_BottomSheet_SCREEN_DATA = "REQUEST_BottomSheet_SCREEN_DATA";
export const REQUEST_IDFENCE_PLANS_DATA = "REQUEST_IDFENCE_PLANS_DATA";
export const REQUEST_FOR_VERIFY_PIN = "REQUEST_FOR_VERIFY_PIN";
export const ERROR_FOR_VERIFY_PIN = "ERROR_FOR_VERIFY_PIN";
export const REQUEST_TESTIMONIAL_DATA = "REQUEST_TESTIMONIAL_DATA";
export const REQUEST_PRODUCT_INFO_COMPONENTS = "REQUEST_PRODUCT_INFO_COMPONENTS";
export const VALIDATE_FORM_FIELD = "validate_form_field";
export const TOGGLE_CHECKBOX = "toggle_checkbox";

export const SET_MEMBERSHIPS = "SET_MEMBERSHIPS";
export const REQUEST_SOD_SERVICE = "REQUEST_SOD_SERVICE";
export const REQUEST_SOD_APPLIANCES_OFFERS = "REQUEST_SOD_APPLIANCES_OFFERS";
export const REQUEST_SOD_PRODUCT_PAGE = "REQUEST_SOD_PRODUCT_PAGE";
export const REQUEST_SOD_PRODUCT_SERVICE_PAGE = "REQUEST_SOD_PRODUCT_SERVICE_PAGE";
export const REQUEST_SOD_PLANS_DATA = "REQUEST_SOD_PLANS_DATA";
export const REQUEST_SOD_TODAY_TIME_SLOTS = "REQUEST_SOD_TODAY_TIME_SLOTS";
export const REQUEST_SOD_PRODUCT_ISSUES = "REQUEST_SOD_PRODUCT_ISSUES";
export const REQUEST_SOD_LABOUR_COST = "REQUEST_SOD_LABOUR_COST";
export const START_LOADING_DATA = "START_LOADING_DATA";
export const STOP_LOADING_DATA = "STOP_LOADING_DATA";
export const PAINT_SOD_PRODUCT_SERVICE_SCREEN = "PAINT_SOD_PRODUCT_SERVICE_SCREEN";
export const REQUEST_BRANDS = "REQUEST_BRANDS";
export const REQUEST_MORE_SOD_PRODUCTS = "REQUEST_MORE_SOD_PRODUCTS";
export const UPDATE_SOD_PLAN_QUANTITY_PRICE = "UPDATE_SOD_PLAN_QUANTITY_PRICE";
export const FILTER_BRANDS = "FILTER_BRANDS";
export const REQUEST_WHC_APPLIANCES_COUNTS = "REQUEST_WHC_APPLIANCES_COUNTS";
export const SHOW_PLAN_LISTING = "SHOW_PLAN_LISTING";
export const PREPARE_BUYBACK_TAGS = "PREPARE_BUYBACK_TAGS";
export const CHANGE_COVER_AMOUNT = "CHANGE_COVER_AMOUNT";
export const GET_PRICE_RANGE = "GET_PRICE_RANGE";
export const SHOW_OFFER_ALERT = "SHOW_OFFER_ALERT";
export const SET_FIELD_ERROR = "SET_FIELD_ERROR";
export const BUY_PLAN_INDEX_API_DATA = "BUY_PLAN_INDEX_API_DATA";
export const VALIDATE_FORM = "VALIDATE_FORM";
export const FORM_VALIDATED = "FORM_VALIDATED";


export const MEMBERSHIP_TAB_ACTION_TYPES = {
    MEMBERSHIP_DATA_FROM_HOME: "MEMBERSHIP_DATA_FROM_HOME",
    MEMBERSHIP_GET_TEMP_CUSTINFO: "MEMBERSHIP_GET_TEMP_CUSTINFO",
    MEMBERSHIP_GET_DOCS_TO_UPLOAD: "MEMBERSHIP_GET_DOCS_TO_UPLOAD",
    MEMBERSHIP_GET_INTIMATION: "MEMBERSHIP_GET_INTIMATION",
    MEMBERSHIP_GET_SERVICE_REQUEST_DATA: "MEMBERSHIP_GET_SERVICE_REQUEST_DATA",
    MEMBERSHIP_GET_BUYBACK_STATUS_DATA: "MEMBERSHIP_GET_BUYBACK_STATUS_DATA",
    MEMBERSHIP_CLAIM_CHECK_ELIGIBILITY: "MEMBERSHIP_CLAIM_CHECK_ELIGIBILITY",
    MEMBERSHIP_DETAILS: "MEMBERSHIP_DETAILS",
    MEMBERSHIP_UPDATE_OTHER_API_CALL_FLAG:"MEMBERSHIP_UPDATE_OTHER_API_CALL_FLAG",
    MEMBERSHIP_CHECK_SUBSCRIPTION_STATUS: "MEMBERSHIP_CHECK_SUBSCRIPTION_STATUS",
    MEMBERSHIP_RENEWALS_REQUEST_DATA: "MEMBERSHIP_RENEWALS_REQUEST_DATA",
    ONGOING_SR_PREPARE_CARDS: "ONGOING_SR_PREPARE_CARDS",
    MEMBERSHIP_IDFENCE_SI_DATA:"MEMBERSHIP_IDFENCE_SI_DATA",
    MEMBERSHIP_CARDS_WITH_DRAFT_SR_DATA: "MEMBERSHIP_CARDS_WITH_DRAFT_SR_DATA",
    MEMBERSHIP_CARDS_WITH_BUYBACK_DATA: "MEMBERSHIP_CARDS_WITH_BUYBACK_DATA",
    MEMBERSHIP_INITIAL_API_CALLS: "MEMBERSHIP_INITIAL_API_CALLS",
    MEMBERSHIP_UPDATE_VIEW_MODEL: "MEMBERSHIP_UPDATE_VIEW_MODEL"
};



