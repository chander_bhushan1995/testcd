

export const IDFenceErrorApiErrorMsg = {
    CYBER_DATA_ALREADY_ENTERED: "Same data has already been added, Specify another one.",
    DEFAULT: "Some error occurred. Please try again later.",
    CUSTOMER_INACTIVE: "Your membership has got deactivated. To reactivate your benefits please reactivate your plan",
    LUHN_CHECK_FAILED: "Invalid card number",

    CANNOT_ADD_PRIMARY_EMAIL:'This is your Primary Email Address, which is already being monitored'
};
