export const SAVE_QUESTIONS = '/buyback/api/v1/answeredQuestions';
export const EXTRACT_PRICE = '/buyback/api/v1/exactPrice';
export const CREATE_ORDER = '/buyback/api/v1/orders';
export const APPLY_COUPON = '/buyback/applyCouponCode?quoteId=';
export const GET_COUPON_CODES = '/buyback/couponCodes?quoteId=';
export const GET_ORDER_STATUS = '/buyback/api/v1/orders?quoteId=';
export const DOCUMENT_UPLOAD = '/buyback/documents';
export const RATING_SUBMIT = 'buyback/ratings';
export const NEGATIVE_QUESTIONNAIRE = '/buyback/ratings/questions/BB_FEEDBACK_CODES_';
export const PAYMENT_FLOW_DOCUMENTS = "/OASYS/webservice/rest/uploadAssetDocuments";
export const SUBMIT_POST_PAYMENT_DETAILS = "/OASYS/v2/submitPostPaymentDetails";
export const GET_STATE_AND_CITY = "/OASYS/restservice/getStateAndCity";
export const PAYMENT = "buyback/api/v1/quotes/";
export const CONTACT_CREATION = "payment-service/api/v1/contacts/create";
export const GET_ACCOUNT_STATUS = "payment-service/api/v1/contacts/getAccountStatus?fundId=";
export const GET_CATALYST_COMPONENTS = "/ui-component-ws/api/v1/uicomponents"
export const SEND_EMAIL_VERIFICATION_LINK = "/myaccount/api/emailVerification"
export const GET_CUSTOMER_DETAILS = (custID) => {
    return `/myaccount/service/customer/getcustdetail?custId=${custID}`;
}
export const GET_RETAILERS_LIST = (lat, long, quoteId) => { return `buyback/api/v1/businessUnits?latitude=${lat}&longitude=${long}&quoteId=${quoteId}` }
export const GET_STORES_LIST = (lat, long, roleCodes) => { return `/oacommons/partner/businessUnits?isBuyBack=Y&latitude=${lat}&longitude=${long}&roleCodes=${roleCodes}&partnerCode=1094` }
export const UPDATE_BUYBACK_ORDER = "buyback/api/v1/orders/update?action=UPDATE_STATUS"
export const UPDATE_ONGOING_BUYBACK = (quoteId) => {
    return `buyback/api/v1/quotes/${quoteId}/update`
}
export const CHECK_PINCODE_SERVICEABILITY = (lat, long, pincode, quoteId) => {
    return `buyback/api/v1/checkPincodeServiceability?latitude=${lat}&longitude=${long}&pincode=${pincode}&quoteId=${quoteId}`
}

// chat API urls
export const AUTO_SUGGESTIONS = 'jcmoarestapi/v1/suggestions';
export const CHAT_AUTH = '/jcmoarestapi/v1/suggestions/auth';
export const SERVICEPLATFORM_DOCUMENT_UPLOAD = '/serviceplatform/api/draft/documents';
export const SERVICEPLATFORM_DRAFT_DETAIL = '/serviceplatform/api/draft/detail?id=';
export const VISITOR_API = 'apigateway/jcm/api/v2/account?user_type=VISITOR';
export const NEW_START_CHAT_API = 'apigateway/jcm/api/v3/chats';

// risk calculator API urls
// export const RISK_CALCULATOR_QUESTIONS = 'https://idfence-risk-calculator.s3.us-east-1.amazonaws.com/questions.json';
export const IDFENCE_RISK_CALCULATOR_PROD = 'https://lambda.oneassist.in/v1/risk';
export const IDFENCE_RISK_CALCULATOR_NON_PROD = 'https://lambda.1atesting.in/v1/idfence/risk';
export const RISK_CALCULATOR_QUESTIONS = 'static/campaign/Risk-Calculator/json/questions.json';


//HomeScreen API urls
export const GET_BUYBACK = (custId, modelName, ram, storage, deviceIdentifier) => {
    return `buyback/api/v1/customers/buybackStatus?modelName=${modelName}&customerId=${custId}&ram=${ram}&storage=${storage}&deviceIdentifier=${deviceIdentifier}`
}
export const ALLCARDS = (custID) => {
    return `myaccount/api/customer/${custID}/cards?status=A`
}

export const HOME_SCREEN_DATA = 'static/oaapp/home/homeScreenData_v1.json'
export const SPOTLIGHT = 'static/oaapp/spotlite/spotlite_data_v2.json'
export const RECOMMENDAION_SPOTLIGHT = 'static/oaapp/spotlite/recommendation_spotlite_data_v1.json'

export const CARD_OFFERS = 'cardgenie/api/v1/aggregated-offers'
export const GET_SERVICE_REQUEST = (custID, options) => {
    return `serviceplatform/api/servicerequests?customerId=${custID}&options=${options}`
}
export const Get_Pending_SR_Orders = (custID, activity) => {
    return `aggregator/api/customer/${custID}/order?activity=${activity}`
}

//Catalyst2.0 API's URL
export const GET_CATALYST_RECOMMENDED_UI_COMPONENTS_SUB_URL = (userID,isOnlyFavorite) => `pandora-ws/api/v1/recommended-ui-components?user_id=${userID}&fav_only=${isOnlyFavorite}`
export const UPDATE_FAVORITE_RECOMMENDATIONS_URL = (userID) => `pandora-ws/api/v1/recommended-ui-components?user_id=${userID}`

//on boardingQuestions
export const GET_QUESTIONS = (custID) => {
    return `/myaccount/api/customer/${custID}/question-answer?categoryCode=BOARDING&questionCode=ASSET_HA,ASSET_PE,ASSET_F`
}
export const CHECK_SUBSCRIPTION_STATUS = (memUUID) => {
    return `idfence-service/customer/checkSubscriptionStatus?subscriberNo=${memUUID}`
}
export const SUBMIT_ANSWER_OF_QUESTIONS = (custID) => {
    return `/myaccount/api/customer/${custID}/question-answer`
};
export const SUBMIT_NAME_EMAIL = '/myaccount/api/customer/updatecustdetail';
export const CANCEL_MEMBERSHIP_URL = (memID) => {
    return `/myaccount/api/membership/si?memId=${memID}`
}
export const QUESTION_LIST = 'static/oaapp/buyBack/QuestionList.json';
export const PRIVACY_POLICY = '/privacypolicy?mediumCode=app'

export const GET_BUYBACK_REQUEST_FEEDBACK_STATUS_URL = (refPrimaryTrackingNos) => `serviceplatform/api/servicerequests?refPrimaryTrackingNos=${refPrimaryTrackingNos}&serviceRequestTypes=BUY_BACK`
//new buy plan journey
export const WHC_PINCODE_SERVICEABILITY = 'serviceplatform/api/pincodes/';
export const WHC_NOTIFY = 'OASYS/webToLeadCapture?'
export const REPORT_FOR_PINCODE = pinCode => `serviceplatform/api/pincodes/${pinCode}/services/reports`
export const BUY_TAB_SPLIT_URL = 'static/oaapp/buytab'
export const GET_OFFER_DATA = 'categoryoffers'
export const GET_TESTIMONIALS = (category, category_service, product) => {
    let url = 'data-warehouse-ws/api/v1/rating-feedback?'
    if (category) {
        url += "category_code=" + category+"&"
    }
    if (category_service) {
        url += "service_name=" + category_service+"&"
    }
    if (product) {
        url += "prod_name=" + product
    }
    return url
}
export const SUGGEST_PLANS = 'recoengine/plan/suggestplans'
export const GET_SOD_PRODUCTS = 'recoengine/product/v1/productservices'
export const GET_PRODUCT_SPECIFICATIONS = 'recoengine/product/v1/productspecifications'
export const RENEW_MEMBERSHIP = 'OASYS/webservice/rest/api/renewals'
export const GET_TIME_SLOTS = (serviceRequestType, serviceRequestSourceType, serviceRequestDate, pinCode, bpCode, buCode, productCode) => `serviceplatform/api/masterData/availableServiceSlots?serviceRequestType=${serviceRequestType}&serviceRequestSourceType=${serviceRequestSourceType}&serviceRequestDate=${serviceRequestDate}&pincode=${pinCode}&bpCode=${bpCode}&buCode=${buCode}&productCode=${productCode}`
export const SERVICE_TASKS = (productCode, taskType, status) => `serviceplatform/api/servicetasks?referenceCode=${productCode}&taskType=${taskType}&status=${status}`
export const GET_PRICE_RANGES = (serviceType, productCode, brand) => `recoengine/plans/invoiceRange?brand=${brand}&buType=App&productCode=${productCode}&service=${serviceType}`
export const GET_BRANDS = ()=> `OASYS/restservice/wsGetAssetMakes`


export const  MEMBERSHIP_TAB_API_URLS={
    GET_MEMBERSHIPS:'/myaccount/api/customer/memberships?',
    GET_TEMP_CUSTOMER_INFO:(activationCode)=>`OASYS/getTempCustomerInfo?activationCode=${activationCode}`,
    GET_DOCS_TO_UPLOAD:(activationCode)=>`OASYS/getDocsToUpload?activationCode=${activationCode}`,
    GET_INTIMATION:(memIds)=>`serviceplatform/api/v1/intimation?id=${memIds}`,
    GET_SERVICE_REQUESTS:'serviceplatform/api/servicerequests?',
    GET_BUYBACK_STATUS:'buyback/api/v1/customers/buybackStatus?',
    GET_CHECK_SUBSCRIPTION_STATUS:(subscriberNo)=>`idfence-service/customer/checkSubscriptionStatus?subscriberNo=${subscriberNo}`,
    GET_IDFENCE_SI_DATA:(memUUID)=>`myaccount/api/membership/si/data?memUUID=${memUUID}`,
    GET_CLAIM_CHECK_ELIGIBILITY:'myaccount/api/claim/checkEligibility?',
    GET_CUSTOMER_MEMBERSHIPS:'myaccount/api/customer/memberships?',
    POST_RENEWALS_REQUEST:'OASYS/webservice/rest/api/renewals?',

};

export  const  API_HEADERS={
    CONTENT_TYPE_TEXT_HTML_UTF_8:"text/html;charset=utf-8"
}
