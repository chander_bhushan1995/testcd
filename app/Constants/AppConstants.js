import { Dimensions } from "react-native";

export const CONTACT_NUMBER = "18001233330";
export const DOCUMENT_TYPE = "document";
export const IMAGE_TYPE = "image";
export const PICKED_FROM_CAMERA = "picked_from_camera";
export const PICKED_FROM_DOC_PICKER = "picked_from_doc_picker";
export const STORRGAE_DENY_PERMISSION = "OneAssist wants to access your storage for uploading documents";
export const CAMERA_DENY_PERMISSION = "OneAssist wants to access your camera for taking photos of your documents";
export const CAMERA_AND_STORAGE_RATIONAL_DENY_PERMISSION = "OneAssist” wants to access your storage and camera to upload documents";
export const TECHNICAL_ERROR_CODE = "10001";
export const IMEI_PERMISSION = "Track your mobile's performance with OneAssist. Please allow IMEI permission.";
export const IMEI_PERMISSION_RATIONAL = "Once you allow us IMEI permission, it's our job to track your mobile's performance. Please allow access";
export const LOCATION_PERMISSION = "Device Location is off";
export const LOCATION_PERMISSION_RATIONAL = "Turn on your device location for accurate serviceability and better experience";
export const CURRENCY = "INR";
export const IDFencePlanCode = "IDF";
export const FINANCE = "Finance";
export const QUICK_REPAIR = "Quick Repair";

export const QuestionsCategoryCode = {
    onBoarding: "BOARDING",
};
export const PLATFORM_OS =
    {
        android: "android",
        ios: "ios",
    };

export const TabIndex = {
    newHome: 0,
    buy: 1,
    membership: 2,
    account: 3,
};
export const TaskTypes = {
    ISSUE: "ISSUE",
    LABOUR_COST: "LABOURCOST",
};

export const Category = {
    homeAppliances: "HA",
    finance: "F",
    personalElectronics: "PE",
    quickRapairs: "QR",
};

export const ScreenDimension = {
    screenHeight: Dimensions.get("screen").height,
    screenWidth: Dimensions.get("screen").width,
};

export const BuybackComboPlanServices = {
     sellYourPhone: 0,
    bookAService: 1
}

export const PlanServices = {
    idFence: "DARK_WEB_MONITORING",
    wallet: "Card Fraud Insurance",
    extendedWarranty: "Extended Warran",
    whcInspection: "WHC_INSPECTION",
    creditScoreMonitoring: "CREDIT_SCORE", //"DARK_WEB_MONITORING"
    adld: "ADLD",
    assuredBuyback: "Buyback",
};

export const Services = {
    fire: "HA_FR",
    buglary: "HA_BR",
    extendedWarranty: "HA_EW",
    homeExtendedWarranty: "Extended Warran",
    breakdown: "HA_BD",
    accidentalDamage: "HA_AD",
    liquidDamage: "ADLD",
    selfRepair: "Self_Repair",
    peExtendedWarranty: "PE_EW",
    peADLD: "PE_ADLD",
    peTheft: "PE_THEFT",
    pms: "PMS",
    creditScoreMonitoring: "CREDIT_SCORE",
    idFence: "DARK_WEB_MONITORING",
    wallet: "Card Fraud Insurance",
    sop: "SOP", // its actually not a service it just to identify service card of ha on buySubtab(HA subcategorytab).

    getCategoryType: (service) => {
        let category = null;
        switch (service) {
            case Services.fire:
            case Services.buglary:
            case Services.breakdown:
            case Services.accidentalDamage:
            case Services.extendedWarranty:
            case Services.selfRepair:
            case Services.homeExtendedWarranty:
            case Services.pms:
                category = Category.homeAppliances;
                break;
            case Services.peADLD:
            case Services.peExtendedWarranty:
            case Services.peTheft:
                category = Category.personalElectronics;
                break;
            case Services.idFence:
            case Services.creditScoreMonitoring:
            case Services.wallet:
                category = Category.finance;
                break;
            default:
                break;
        }
    },
};

export const Destination = {
    whc: "HA",
    ew: "EW",
    pe: "PE",
    peEW: "PE_EW",
    gadgetserve: "HAGS",
    wallet: "WP01",
    idfance: "IDF",
    sod: "SOD",
    sodQTY: "SODQTY",
    haSOP: "HA_SOP",

    getDestination: (category, service) => {
        if (category === Category.homeAppliances) {
            if (service === PlanServices.extendedWarranty) {
                return Destination.ew;
            } else if (service === Services.sop) {
                return Destination.haSOP;
            }
            return Destination.whc;
        } else if (category === Category.personalElectronics) {
            if (service === PlanServices.extendedWarranty) {
                return Destination.peEW;
            }
            return Destination.pe;
        } else if (category === Category.finance) {
            if (service === PlanServices.wallet) {
                return Destination.wallet;
            } else if (category === PlanServices.idFence) {
                return Destination.idfance;
            }
            return Destination.wallet;
        }

        return null
    },

    // returns array of possible routes for buy journey for specific destination
    getRoutesForDestination: (destination) => {
        switch (destination) {
            case Destination.wallet:
                return ["PlanListingScreen", "PaymentFlow"];
            case Destination.haSOP:
                return ["PinCodeScreen", "PlanListingScreen", "PaymentFlow"];
            case Destination.whc:
                return ["PinCodeScreen", "ApplianceCountSelectionScreen", "PlanListingScreen", "PaymentFlow"];
            case Destination.ew:
                return ["ProductInfoInputScreen", "PlanListingScreen", "PaymentFlow"];
            case Destination.pe:
                return ["ProductInfoInputScreen", "PlanListingScreen", "PaymentFlow"];
            case Destination.peEW:
                return ["ProductInfoInputScreen", "PlanListingScreen", "PaymentFlow"];
            default:
                return [];
        }
    },
};

export const FIREBASE_KEYS = {
    SERVICE_GADGETS: "service_gadgets",
    BUY_TAB_JSON: "buy_tab_json",
    QR_CampaignId: "qr_campaign_id_nonserviceable",
    HA_CampaignId: "ha_campaign_id",
    FIND_PLAN_VARIATIONS: "find_plan_variations",
    MODULE_FLOW_SEQUENCING: "module_flow_sequencing",
    PLAN_TAGLINE: "plan_tagline",
    FIND_A_PLAN: "find_a_plan_config",
    COVER_AMOUNT_INFO: "cover_amount_info",
};

export const FlowSequence = {
    SODFlow: {
        PRODUCT_SCREEN: "PRODUCT_SCREEN",
        PINCODE_SCREEN: "PINCODE_SCREEN",
    },
};

export const LoginForScreen = {
    SOD_PLAN_SALES: "SOD Sales",
    SALES: "Sales",
    ACTIVATE_VOUCHER: "Activate Voucher",
    MEMBERSHIP_TAB: "Membership Tab",
};

export const APIErrorCodes = {
    "INVALID_PROMO_CODE": "6150054",
};

export const NumberVerifyAlerts = {

    buttonText: "Verify mobile number",
    buyPlan: {
        message: "To Buy Plan, Please verify mobile number.",
        detailMsg: "To Buy Plan, Please verify mobile number.",
    },
    buySODPlan: {
        message: "To continue booking, please verify your mobile number.",
    },
    chatWithUs: {
        message: "To chat with customer care, please verify mobile number.",
    },
    activateVoucherLoginAlert: {
        message: "Have a voucher code? To activate, please verify mobile number.",
    },
    paycreditcardbill: {
        message: "To pay credit card bills, please verify mobile number.",
    },
    addCard: {
        message: "To add your cards, please verify mobile number.",
    },
    bookmarkOffer: {
        message: "To bookmark an offer, please verify mobile number.",
    },
    bookmarkLoginAlert: {
        message: "To see your Bookmarks, please verify mobile number.",
    },
    sod: {
        message: "To continue booking, please verify your mobile number.",
    },
    activateVoucher: {
        title: "Have a voucher code? To activate, please verify mobile number.",
    },
};

export const ID_FENCE_BLOG = "ID Fence Blog";
export const THANK_YOU_FOR_BLOG_SUBSCRIPTION = "Thanks! You have been subscribed to our newsletter.";
export const MEMBERSHIP_DETAILS = "MEMBERSHIP DETAILS";
export const ID_FENCE_SHARE_LINK = "https://www.oneassist.in/category/identity-theft-monitoring-protection-idfence/?utm_source=app-plan-detail-page&utm_medium=share&utm_campaign=social";

export const TESTIMONIAL_LIST_TITLE = "Recent Customer Reviews";
export const PINCODE_TITLE = "Enter residential pincode";
export const CHECK_SERVICEABILITY = "Check Service Availability";
export const NOTIFY_ME = "Notify Me";
export const EXPLORE_OTHER_SERVICES = "Explore other services";
export const ENTER_MOBILE = "Enter 10 digit number";
export const FULL_NAME = "Full Name";
export const INVALID_INPUT = "Invalid Input";
export const COMING_SOON = "Coming soon to your location. Get notified when our services open in your city.";
export const NOTIFY_SUCCESS = "Thanks, you will be notified when services open in your city.";
export const PINCODE_DESCRIPTION = "This helps us check service availability in your region";
export const NOT_SERVICING = "Sorry, we are currently not servicing in your residential area";
export const CAN_NOTIFY = "We can notify you when once we are available in your area";
export const SOMETHING_WRONG = "Something went wrong";
export const NEED_SERVICE_EXPERT = "I need a service expert for";
export const NEED_SPECIAL_ATTENTION = "Your request needs special attention";
export const COPIED = "Copied to clipboard";
export const MAX_QUANTITY_SELECTED = (quantity) => {
    return `You can select a maximum  of ${quantity} in  each appliance. Our expert will contact you shortly.`;
};
export const SERVED = count => `${count}+ appliances repaired in your pincode in last three months`;
export const TELL_US_MORE = "Tell us a few details before continuing";
export const PLAN_STARTS_AFTER = "OneAssist Plan starts after manufacturer warranty expires.";
export const AVAILABLE_SLOTS = count => `${count} slots available today`;
export const LABOUR_COST_DESCRIPTION = "Note: Labour cost is charged if appliance needs repair and is to be paid after inspection only.";
export const LABOUR_COST_TITLE = "Labour Cost (To be Paid after inspection only)";
export const PAY_AFTER_SERVICE = "Pay after service  available in few cities";
export const PROCEED_TO_BOOK = "Proceed to book";
export const SELECT_ISSUE = "Select issue you are facing";
export const ALREADY_COVERED = "Looks like, your appliance is already covered in HomeAssist Plan";
export const BOOK_FREE = "You can book a service/repair for free";
export const RAISE_SERVICE_REQ = "Raise a Service Request for Free";
export const CONTINUE_BOOKING = "Continue with Booking";
export const SPECIAL_ATTENTION = "Your request needs special attention";
export const UP_TO_APPLIANCE = count => `You can select a maximum of ${count} in each appliance. Our expert will contact you shortly.`;
export const NEAR_BY_STORES = "Nearby Stores";
export const CONFIRM_VISIT = "Confirm Visit";
export const CALL_RETAILER = "Call Retailer";
export const GET_DIRECTIONS = "Get Directions";
export const CHAT = "Chat";
export const THANKS_USER = user => `Thanks for your interest ${user}!`;
export const RATING_HEADER = "How was your overall experience?";
export const RATING_DESCRIPTION = "Rate us on a scale of 1 to 5";
export const RATING_NOTE = "What do you like the most?";
export const RATING_NOTE_2 = "What could be improved?";
export const MOBILE_EXCHANGE_REQ_PLACED = "Mobile Exchange Request Placed";
export const MOBILE_RESALE_REQ_PLACED = "Resale request successfully placed";
export const MOBILE = "Mobile";
export const EXCHANGE_PRICE = "Exchange price";
export const VIEW_DETAIL = "View Details";
export const VIEW = "View"
export const START_ACTIVATION = "Start Activation"
export const NO_RETAILERS = "Oops! No nearby retailers found, but you can schedule pickup from your home";
export const BUYBACK_CANCEL_CONFIRM = "Are you sure you want to cancel? You may not get this best price again.";
export const RESALE_PRICE = "Resale price is";
export const PRICE_EXP_IN = (days) => {
    return `Price expires in ${days} days`;
};
export const CANCEL_REQUEST = "Cancel Request";
export const SCHEDULE_PICKUP = "Schedule pickup";
export const WANT_TO_SELL = "I want to sell";
export const WANT_TO_EXCHANGE = "I want to exchange";
export const SUBMIT_PICKUP_REQUEST = "Submit pickup request";
export const SCHEDULE_PICKUP_DOORSTEP = "Schedule pickup at your doorstep";
export const NOT_FOR_EXCHANGE = "Not looking to exchange?";
export const PURCHASED_FROM_SAME = "You purchased your last mobile from this store";
export const NOT_SERVING = "We are currently not serving in this area. You can schedule pickup from another location.";
export const LOCATION_OFF = "Device Location is off";
export const ON_DEVICE_LOCATION = "Turn on your device location for accurate serviceability and better experience";
export const TURN_ON_LOCATION = "Turn on location";
export const RATE_APPSTORE = (platform) => {
    return `Sweet! Show us your love by giving 5 star rating on the ${platform}.`;
};
export const GIVE_FEEdBACK = "Sorry to hear that! Please give us your feedback.";
export const THANKS_FOR_FEEDBACK = "Thank you! Your feedback is valuable in helping us improve our services.";
export const RATE_NOW = "Rate now";
export const BACK_TO_HOME = "Back to home"
export const ADD_FEEDBACK = "Add a feedback";
export const NO_PLANS_FOUND = "Plans not found!";

export const HAVE_ACTIVATION_CODE = "Have a membership activation code?";
export const ACTIVATE_NOW = "Activate Now";
export const VERIFY_NUMBER_FOR_MEMBERSHIP = "To see your memberships, please verify mobile number.";
export const BUY_PLAN_FOR_MEMBERSHIP = "You don't have any membership. Buy a plan now.";
export const BUY_PLAN = "Buy Plan";
export const SERVICE_REQUESTS = "Service Request(s)";
export const CLAIM_NOT_ALLOWED = "You can't raise claim on this membership. Press ok to refresh membership.";
export const CARD_FRAUD_FOR_WALLET = "Card Fraud Insurance' for Wallet";
export const WALLET_STRING = "Wallet";
export const HOME_EW = "Home EW";
export const IDFENCE_STRING = "ID Fence";
export const DARK_WEB_MONITORING_STRING = "Dark Web Monitoring";
export const IDFENCE_ASSETS_TEXT = "Dark Web Monitoring for data compromises";
export const IDFENCE_PENDING_MEMBERSHIP_TEXT = "Your upgrade request is in process. Please wait for a few minutes.";
export const EXPECTED_CLOSURE = (startDate,endDate) => `Expected service closure between ${startDate} - ${endDate}`;
export const DOWNLOAD_INVOICE = "Download Invoice"
export const YOUR_APPLIANCES = "Your Appliances"
export const ACTIVATION_NOT_SETUP = "Activation setup not completed. "
export const ACTIVATION_SETUP_COMP = "Activation setup complete. "

export const ButtonTexts = {
    BUY_NOW: "Buy Now",
    UPGRADE_NOW: "Upgrade Now",
    REACTIVATE_NOW: "Reactivate Now",
    RENEW_NOW: "Renew Now",
    MY_CARDS: "My Cards",
    TXT_SELL_YOUR_PHONE: "Sell your Phone",
    TXT_RAISE_CLAIM:'Raise Claim',
    TXT_BOOK_A_SERVICE:'Book a Service'

};

export const IDFenceTexts = {
    MEMBERSHIP_CANCELED: "Your membership is Cancelled.  Buy now to continue monitoring your assets.",
    TRAIL_ACTIVE: (days) => `Your trial ends in ${days} day(s). Please upgrade to continue monitoring your assets.`,
    TRAIL_EXPIRED: (days) => `Your trial membership is expired. Please upgrade within ${days} day(s) to continue monitoring your assets.`,
    TRAIL_MEM_EXPIRED: (days) => `Your trial membership is expired. Please update your payment information from dashboard to continue monitoring your assets within ${days} day(s)`,
    TRAIL_INACTIVE: "Your trial membership is expired. Please upgrade to continue monitoring your assets.",
    TRAIL_CANCELLED_BUY_NOW: "Your membership is Cancelled.  Buy now to continue monitoring your assets.",
    PREMIUM_EXPIRED: (days) => `Your membership is expired. Please update your payment information from dashboard to continue monitoring your assets within ${days} day(s).`,
    PREMIUM_INACTIVE: `Your membership is inactivated. Please reactivate to continue monitoring your assets.`,
};




