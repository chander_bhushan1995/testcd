export const AppStringConstants = {
    PRICE_RANGE_TYPE_1_HEADER: 'How much did your device cost?',
    PRICE_RANGE_TYPE_2_HEADER: 'Select a price range',
    RUPEE_SYMBOL: '\u0024',
    SEARCH: 'Search',
    POPULAR_BRANDS: 'POPULAR BRANDS',
    OTHER_BRANDS: 'OTHER BRANDS',
    WHC_APPLIANCE_COUNT_SELECTION_HEADER: 'How many appliances do you want to protect?',
    ERROR_SELECT_APPLIANCE: 'Please select an appliance first',
    ERROR_SELECT_BRAND: 'Please select a brand first',

    MOST_BOUGHT: 'MOST BOUGHT',
    OTHER_APPLIANCES: 'OTHER APPLIANCES',
    SELECT_APPLIANCE: 'Select Appliance',
    FIND_A_PLAN: "Find a plan",
    FIND_PLAN: "Find plan",
    ERROR_MSG_SELECT_APPLIANCE: "Please select an appliance",
    ERROR_ENTER_INVOICE_AMOUNT: "Please enter invoice amount",
    ERROR_MINIMUM_AMOUNT_SHOULD_BE: "Minimum amount should be Rs. %s",
    ERROR_MAXIMUM_AMOUNT_EXCEEDED: "Maximum amount exceeded, please contact the customer care.",
    TYPE_OF_APPLIANCES: "%1s types of appliances are covered: %2s",
};

export const _APPLIANCES = (count) => `Upto ${count} Appliance`
export const _TYPE_OF_APPLIANCES_ARE_COVERED = (count) => `${count} type of appliances are covered:`

