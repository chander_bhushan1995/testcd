export const SET_INITIAL_PROPS = 'SET_INITIAL_PROPS';
export const UPDATE_INITIAL_PROPS = 'UPDATE_INITIAL_PROPS';

export const INITIAL_PROPS_PARAMS = {
    custType: "custType",
    customer_id: "customer_id",
    mobile_number: "mobile_number",
    user_Name: "user_Name",
    user_Email: "user_Email",
    question_ListData: "question_ListData",
};

/*
This must be save for native CustType enum
 */
export const CustType = {
    nonSubscriber: 'NON_SUBSCRIBER', //user has verified the number but have no membership-----(comming from api)
    subscriber: 'SUBSCRIBER', //user has verified the number and have a membership----(comming from api)
    notVerified: 'NOT_VERIFIED', //if user skip the number verification----(for internal use)
};




