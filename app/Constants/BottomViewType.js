export const BottomViewType = {
    None: 'BottomViewTypeNone',
    Input: 'BottomViewTypeInput',
    ChatbotTagInput: 'BottomViewTypeChatbotTagInput',
    ChatbotInput: 'BottomViewTypeChatbotInput',
    ChatbotForm: 'BottomViewTypeChatbotForm',
    ChatbotUploadForm: 'BottomViewTypeChatbotUploadForm',
    StartChat: 'BottomViewTypeStartChat'
};
