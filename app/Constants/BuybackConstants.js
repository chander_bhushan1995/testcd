// All the constant define here
export const STEP_LOCATION = 'step_location';
export const STEP_MHC = 'step_mhc';
export const STEP_QUESTIONS = 'step_questions';
export const STEP_PICKUP = 'step_pickup';

export const STEP_NOT_STARTED = 'step_not_started';
export const STEP_IN_PROGRESS = 'step_in_progress';
export const STEP_COMPLETED = 'step_completed';

export const buybackOrderStatus = {
    ORDER_STATUS_OG: 'OG',
    ORDER_STATUS_PUS: 'PUS',
    ORDER_STATUS_PUR: 'PUR',
    ORDER_STATUS_OEX: 'OEX',
    ORDER_STATUS_SDP: 'SDP',
    ORDER_STATUS_SPU: 'SPU',
    ORDER_STATUS_FPU: 'FPU',
    ORDER_STATUS_OC: 'OC',
};

export const buybackOrderStatusMessages = {
    AWAITING_CONFIRMATION: 'Awaiting confirmation',
    PICKUP_SCHEDULED: 'Pickup scheduled',
    PICKUP_RESCHEDULED: 'Pickup rescheduled',
    ORDER_EXPIRED: 'Order is expired',
    PAYMENT_SUCCESSFUL: 'Payment successful',
    DEVICE_NOT_PICKED: 'Device is not picked up',
    ORDER_CANCELLED: 'Order is cancelled',
    VOUCHER_GENERATED: 'Voucher Generated',
    VOUCHER_REDEEMED: 'Voucher Redeemed',
};

export const buybackOrderStatusDescription = {
    DESCRIPTION_OG_ABB: 'Our partner agent will call you within 24 hours to schedule mobile pickup.',
    DESCRIPTION_OG_THIRD_PARTY: 'Our partner agent from Instacash will call you within 24 hours to schedule mobile pickup.',
    DESCRIPTION_PUS_ABB: 'Make sure you have hard copy of invoice and you have taken data backup before handing over the mobile.',
    DESCRIPTION_PUS_NON_ABB: 'Make sure you have taken data backup before handing over the mobile.',
    DESCRIPTION_VOUCHER_REDEEMED: (date) => {
        return 'Voucher redeemed successfully on ' + date;
    },
    DESCRIPTION_SDP: (deviceName, price) => {
        return 'You have sold your ' + deviceName + ' for ' + price;
    },
};

export const buybackStatusConstants = {
    BUYBACK_NOT_STARTED: 'BUYBACK_NOT_STARTED',
    SERVICEABILITY_CHECKED: 'SERVICEABILITY_CHECKED',
    QUOTE_CREATED: "QUOTE_CREATED",
    MHC_COMPLETED: 'MHC_COMPLETED',
    QUESTIONS_IN_PROGRESS: 'QUESTIONS_IN_PROGRESS',
    BUYBACK_PICKUP_PENDING: 'BUYBACK_PICKUP_PENDING',
    QUOTE_GENERATED: 'QUOTE_GENERATED',
    ORDER_PLACED: 'ORDER_PLACED',
    VOUCHER_GENERATED: 'VOUCHER_GENERATED',
    MODEL_NOT_FOUND: 'MODEL_NOT_FOUND',
};

export const enterBankDetailsScreenState = {
    STATE_INITIAL: 'STATE_INITIAL',
    STATE_ACCOUNT_STATUS_PROCESSING: 'STATE_ACCOUNT_STATUS_PROCESSING',
    STATE_FINAL_SUBMIT: 'STATE_FINAL_SUBMIT',
};

export const buybackFlowType = {
    CUSTOMER_PICKUP: 'CUSTOMER_PICKUP',
    RETAILER_PICKUP: 'RETAILER_PICKUP',
    THIRD_PARTY: 'THIRD_PARTY',
    BOTH: 'BOTH'
};

export const buybackVoucherStatus = {
    UNUSED: 'UNUSED',
    REDEEMED: 'REDEEMED',
};

export const buybackPartnerType = {
    PARTNER_XIAOMI: 'XIAOMI',
};

export const DL_BUYBACK_TIMELINE = 'buyback/timeline';
export const DL_BUYBACK_SCHEDULE_PICKUP = 'buyback/schedulepickup';
export const DL_BUYBACK_POST_PICKUP = 'buyback/postpickup';
export const DL_BUYBACK_START_MHC = 'buyback/startmhc';

export const EXCELLENT = 'Excellent';
export const GOOD = 'Good';
export const AVERAGE = 'Average';
export const POOR = 'Poor';

export const ABB_DIALOG_TEXT = ' is not covered under Assured Buyback.';
export const ABB_DIALOG_DUE_TO_DEVICE_AGE = ' is not eligible for Assured Buyback due to device age';

export const buybackFlowStrings = {
    buySellSelectionTitle: 'What would you like to do?',
    exchange: 'Exchange',
    xiaomiExchangeTitle: 'Submit at a nearby Mi Service Centre',
    exchangeDescription: 'Exchange your phone at a nearby store and enjoy savings on your new phone',
    exchangeButtonTitle: 'See nearby stores',
    xiaomiExchangeButtonTitle: 'See nearby service centres',
    sell: 'Sell',
    sellDescription: 'Sell your phone and get instant payment right at your home',
    xiaomiDescription: 'You will get a voucher of the resale value after inspection which can be redeemed while purchasing new phone.',
    sellButtonTitle: 'Schedule Pickup',
    specialNoteMessage: 'Please carry valid government id, invoice and original accessories to the store',
    enter_bank_btn_subtext: '₹1 will be deducted from your account to verify bank details',
    verify_bank_account_title: 'Verify Bank Account',
    verify_bank_account_description: 'We will transfer an amount of Rs.1 in your bank account to verify the details. Final amount will be deposited later.',
    confirm_account: 'Confirm account',
    check_service_availability: 'Check service availability',
    verify_account_success: 'We have successfully verified your account details',
    click_photo_msg: 'Click on photo to see the full view',
};

export const BuybackEventLocation = {
    EXCHANGE_REQUEST : "exchange request",
    VISIT_CONFIRMATION: "visit confirmation",
    TIMELINE_CHECK: "timeline check",
    NOT_FOR_EXCHANGE: "Not looking to exchange",
    FULL_SCREEN: "Full Screen",
    BUYBACK: "buyback"
}
