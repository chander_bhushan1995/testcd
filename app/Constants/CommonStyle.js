import {StyleSheet, PixelRatio} from "react-native";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";

export const CommonStyle = StyleSheet.create({
    messageTimeReceiveMessage: {
        color: colors.grey,
        fontSize: 10,
        fontFamily: "Lato",
        alignSelf: "flex-start",
        marginLeft: spacing.spacing8,
        marginTop: spacing.spacing4
    },
    messageTimeSendMessage: {
        color: colors.grey,
        fontSize: 10,
        fontFamily: "Lato"
    },
    separator: {
        height: 1,
        width: "100%",
        backgroundColor: colors.lightGrey2
    },
    sepraterVertical: {
        width: 1,
        backgroundColor: colors.lightGrey2
    },
    statusView: {
        flexDirection: "row",
        alignSelf: "flex-end",
        marginTop: spacing.spacing4,
        marginRight: spacing.spacing24,
        marginBottom: spacing.spacing16
    },
    statusImage: {
        width: 10,
        height: 10,
        marginTop: 1,
        marginRight: 3,
        resizeMode: "contain"
    },
    messageError: {
        color: colors.salmonRed
    },
    ImageContainer: {
        borderRadius: 4,
        width: 220,
        height: 220,
        borderColor: colors.grey,
        borderWidth: 1 / PixelRatio.get()
    },
    loaderContainer: {
        backgroundColor: colors.darkGrey,
        position: "absolute",
        width: 220,
        height: 220,
        borderRadius: 4,
        alignSelf: "center",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: "center",
        alignItems: "center"
    },
    crossIconContainer: {
        marginRight: 16,
        marginTop: 16,
        height: 16,
        width: 16,
        resizeMode: "contain",
        alignSelf: "flex-end",
        justifyContent: "flex-end"
    },
    loaderStyle: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: colors.white
    },
    cardContainerWithShadow: {
        shadowColor: colors.lightGrey,
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowRadius: 4,
        shadowOpacity: 0.72,
        backgroundColor: colors.white,
        margin: spacing.spacing2,
        elevation: 2,
        /*borderRadius: 4*/
    },
    bulletIcon: {
        height: 10,
        width: 10,
        alignSelf: 'flex-end'
    }
});

export const TextStyle = StyleSheet.create({
    text_36_bold: {
        color: colors.charcoalGrey,
        fontSize: 36,
        fontFamily: 'Lato-Bold',
        lineHeight: 44
    },
    text_24_bold: {
        color: colors.charcoalGrey,
        fontSize: 24,
        fontFamily: "Lato-Bold",
        lineHeight: 22
    },
    text_24_semibold: {
        color: colors.charcoalGrey,
        fontSize: 24,
        fontFamily: 'Lato-Semibold',
        lineHeight: 22
    },
    text_22_normal: {
        color: colors.charcoalGrey,
        fontSize: 22,
        fontFamily: 'Lato-Regular',
        lineHeight: 32
    },
    text_20_bold: {
        color: colors.charcoalGrey,
        fontSize: 20,
        fontFamily: "Lato-Bold",
        lineHeight: 22
    },
    text_20_normal: {
        color: colors.charcoalGrey,
        fontSize: 20,
        fontFamily: 'Lato-Regular',
        lineHeight: 22
    },
    text_18_bold: {
        color: colors.charcoalGrey,
        fontSize: 18,
        fontFamily: 'Lato-Bold',
        lineHeight: 26
    },
    text_18_semibold: {
        color: colors.charcoalGrey,
        fontSize: 18,
        //fontFamily: 'Lato-Semibold',
        lineHeight: 26
    },
    text_18_normal: {
        color: colors.charcoalGrey,
        fontSize: 18,
        fontFamily: 'Lato-Regular',
        lineHeight: 26
    },

    text_16_regular: {
        color: colors.charcoalGrey,
        fontSize: 16,
        fontFamily: 'Lato-Regular',
        lineHeight: 24
    },
    text_16_bold: {
        color: colors.charcoalGrey,
        fontSize: 16,
        fontFamily: "Lato-Bold",
        lineHeight: 24
    },
    text_16_semibold: {
        color: colors.charcoalGrey,
        fontSize: 16,
        fontFamily: 'Lato-Semibold',
        lineHeight: 22
    },
    text_16_bold_24: {
        color: colors.charcoalGrey,
        fontSize: 16,
        fontFamily: 'Lato-Bold',
        lineHeight: 24
    },
    text_14_normal_charcoalGrey: {
        color: colors.charcoalGrey,
        fontSize: 14,
        fontFamily: 'Lato-Regular',
        lineHeight: 20
    },
    text_14_normal: {
        color: colors.grey,
        fontSize: 14,
        fontFamily: "Lato-Regular",
        lineHeight: 20
    },
    text_14_bold: {
        color: colors.charcoalGrey,
        fontSize: 14,
        fontFamily: "Lato-Bold",
        lineHeight: 20
    },
    text_14_semibold: {
        color: colors.charcoalGrey,
        fontSize: 14,
        fontFamily: "Lato-Semibold",
        lineHeight: 20
    },
    text_16_normal: {
        color: colors.charcoalGrey,
        fontSize: 16,
        fontFamily: "Lato-Regular",
        lineHeight: 22
    },
    text_12_normal: {
        color: colors.grey,
        fontSize: 12,
        fontFamily: "Lato-Regular",
        lineHeight: 18
    },
    text_12_semibold: {
        color: colors.blue,
        fontSize: 12,
        fontFamily: "Lato-Semibold",
        lineHeight: 18
    },
    text_12_bold: {
        color: colors.grey,
        fontSize: 12,
        fontFamily: "Lato-Bold",
        lineHeight: 18
    },
    text_12_medium: {
        color: colors.charcoalGrey,
        fontSize: 12,
        fontFamily: "Lato-Medium",
        lineHeight: 18
    },
    text_12_regular: {
        color: colors.charcoalGrey,
        fontSize: 11,
        fontFamily: 'Lato-Regular',
        lineHeight: 16
    },
    text_10_bold: {
        color: colors.grey,
        fontSize: 10,
        fontFamily: "Lato-Bold",
        lineHeight: 16
    },
    text_10_regular: {
        color: colors.grey,
        fontSize: 10,
        fontFamily: 'Lato-Regular',
    },
    text_10_normal: {
        color: colors.grey,
        fontSize: 10,
        fontFamily: "Lato-Regular",
        lineHeight: 16
    },
    text_8_normal: {
        color: colors.charcoalGrey,
        fontSize: 8,
        fontFamily: "Lato-Regular",
        lineHeight: 14
    }
});

export const margin = StyleSheet.create({
    leftRight16: {
        marginLeft: 16,
        marginRight: 16
    },
    leftRight12: {
        marginLeft: 12,
        marginRight: 12
    }
});

export const ButtonStyle = StyleSheet.create({
    BlueButton: {
        backgroundColor: colors.blue,
        padding: spacing.spacing12,
        borderRadius: 4
    },
    WhiteButton: {
        backgroundColor: colors.white,
        padding: spacing.spacing12,
        borderRadius: 4
    },
    GreyButton: {
        backgroundColor: colors.lightGrey,
        padding: spacing.spacing12,
        borderRadius: 4
    },
    primaryButtonText: {
        color: colors.white,
        textAlign: "center",
        fontSize: 16,
        fontFamily: "Lato-Medium",
        lineHeight: 22
    },
    TextOnlyButton: {
        color: colors.blue,
        textAlign: "center",
        fontSize: 14,
        fontFamily: "Lato-Medium",
        lineHeight: 22
    }
});


export const ButtonViewStyle = {
    splashBlueButton: {
        buttonStyle: {
            ...ButtonStyle.BlueButton,
            marginHorizontal: spacing.spacing36,
            marginBottom: spacing.spacing12,
            height: spacing.spacing48
        },
        actionTextStyle: {
            ...TextStyle.text_16_bold,
            color: colors.white,
            flex: 1,
            paddingVertical: spacing.spacing0
        }
    },
    splashWhiteButton: {
        buttonStyle: {
            ...ButtonStyle.WhiteButton,
            marginHorizontal: spacing.spacing36,
            marginBottom: spacing.spacing22,
            height: spacing.spacing48
        },
        actionTextStyle: {
            ...TextStyle.text_16_bold,
            color: colors.blue,
            flex: 1,
            paddingVertical: spacing.spacing0
        }
    },
    defaultBlueButton: {
        buttonStyle: {
            ...ButtonStyle.BlueButton,
            marginHorizontal: spacing.spacing16,
            marginBottom: spacing.spacing12,
            height: spacing.spacing48
        },
        actionTextStyle: {
            ...TextStyle.text_16_bold,
            color: colors.white,
            flex: 1,
            paddingVertical: spacing.spacing0
        }
    },
    buttonSelectedStyle: {
        backgroundColor: colors.blue028,
        padding: spacing.spacing12,
        borderRadius: 2,
        alignItems: "center",
        justifyContent: 'center',
        height: spacing.spacing48,
    },
    buttonDisabledStyle: {
        backgroundColor: colors.lightGrey,
        padding: spacing.spacing12,
        borderRadius: 2,
        alignItems: "center",
        justifyContent: 'center',
        height: spacing.spacing48,
    },
    buttonUnselectedStyle: {
        backgroundColor: colors.white,
        padding: spacing.spacing12,
        borderRadius: 2,
        alignItems: "center",
        justifyContent: 'center',
        height: spacing.spacing48,
        borderColor: colors.greye0,
        borderWidth: 1,
    },
};

export const SafeAreaStyle = StyleSheet.create({
    safeArea: {
        flex: spacing.spacing1,
        backgroundColor: colors.white
    }
});
