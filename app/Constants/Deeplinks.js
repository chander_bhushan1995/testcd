export const Deeplink  = {
    myRewards: "myRewards",
    onboardingQuestion: "onboardingQuestion",
    profileQuestion: "profileQuestion",
    myProfile: "myProfile",
    rewardAppShare: "rewardAppShare",
    activateVoucher: "common/activatevoucher",
    accountTab: "common/myaccount",
    onlineOffer: "wallet/onlineoffers",
    myOffers: "wallet/offers",
    offlineOffer: "wallet/offlineoffers",
    buyback: "buyback",
    covidKnowMore: "covidknowmore",
    riskCalculator: "campaign/Risk-Calculator",
    homeSuggestAPlan: "common/purchasehomeassistprotectionplan",
    suggestAplanHome: "home/suggestaplan",
    suggestAplanWallet: "wallet/suggestaplan",
    walletSuggestAPlan: "common/purchasewalletprotectionplan",
    mobileSuggestAPlan: "common/purchasemobileprotectionplan",
    sopSuggestPlan: "common/purchasehomeunlimitedserviceplan",
    suggestAplanMobile: "mobile/suggestaplan",
    submitDetails: "mobile/submitdetails",
    homeAssistSeggestPlan: "common/purchasehomeprotectionplan",
    buy: "buy",
    buyPlanListing: "buy/listing",
    buyRecommendation: "buy/recommendation",
    quickRepairs: "QR",
    salesRecommendationIDFence: "category/identity-theft-monitoring-protection-idfence",
    salesRecommendation: "sales/recommendation",
    mobileUpgrade: "mobile/upgradenow",
    howClaimWorks: "howclaimworks",
    whcSelectAddress: "home/selectaddress",
    serviceDetail: "SR/servicedetail",
    idfence: "idfence"
}
