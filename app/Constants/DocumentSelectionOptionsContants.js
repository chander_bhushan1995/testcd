export const TAKE_A_PHOTO = 'Take a photo';
export const CHOOSE_FROM_GALLERY = 'Choose from Gallery';
export const CHOOSE_FROM_DOCUMENTS = 'Choose from Documents';
export const CANCEL = 'Cancel';