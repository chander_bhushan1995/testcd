export const IMAGE = 'image';
export const DOCUMENT = 'document';
export const supportedDocumentType = [
    "jpg",
    "jpeg",
    "png",
    "gif",
    "doc",
    "pdf",
    "docx",
    "txt",
    "rtf",
    "plain",
    "JPG",
    "JPEG",
    "PNG",
    "GIF",
    "DOC",
    "PDF",
    "DOCX",
    "TXT",
    "RTF",
    "PLAIN"
];

export const imageType = [
    "jpg",
    "jpeg",
    "png"
];