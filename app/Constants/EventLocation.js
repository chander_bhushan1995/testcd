export const DEEPLINK = "Deeplink";
export const CATEGORY_SCREEN = "Category Screen";
export const PRODUCT_SERVICE_SCREEN = "Product Service Screen";
export const PLAN_RECOMMENDATION_SCREEN = "Plan Recommendation Screen";
export const PLAN_LISTING_SCREEN = "Plan Listing Screen";
export const BENEFITS_SCREEN = "Benefits Screen";
export const SELECT_APPLIANCE_COUNT = "Select Appliance Count"
