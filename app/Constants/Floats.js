const floats = {
    float0_1: 0.1,
    float2_7: 2.7,
    float0_5: 0.5,
    float0_7: 0.7,
    float0_8: 0.8,
    float0_72: 0.72,
    float2: 2,
    float4: 4,
    float6: 6
}
export default floats;
