const fontSize = {
    fontSize8: 8,
    fontSize10: 10,
    fontSize12: 12,
    fontSize14: 14,
    fontSize16: 16,
    fontSize18: 18,
    fontSize20: 20,
    fontSize24: 24,
    fontSize34: 34,
    fontSize28: 28,
    fontFamily_Lato: "Lato",
    fontWeight_bold: "bold",
    fontWeight_normal: "normal",
}

export default fontSize;
