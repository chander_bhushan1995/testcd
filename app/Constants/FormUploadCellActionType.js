export const FMIP = 'showFMIPPopout';
export const MI_LOGOUT = 'showMiLogoutPopout';
export const DOWNLOAD_TEMPLATE = 'showSubrogationDownloadTemplate';
export const VIEW_TEMPLATE = 'ViewTemplate';

export const FMIP_TEXT = 'How to turn off Find My iPhone service ?';
export const MI_LOGOUT_TEXT = 'How to logout from mi account ?';
export const DOWNLOAD_TEMPLATE_TEXT = 'Download Template';
