export const TYPE = {
    EXPIRED: "E",
    PENDING: "P",
    ACTIVE: "A",
    CANCELED: "X",
};
export const CATEGORY = {
    HOME_APPLIANCE: "HA",
    PERSONAL_ELECTRONICS: "PE",
    FINANCE: "F",
};
export const RENEWAL_STATUS = {
    PENDING: "P",
    INITIATED: "I",
    REJECTED: "R",
    NOT_APPLICABLE: "NA",
    IN_RENEWAL_WINDOW: "RW",
    UPGRADE: "UD",
};
export const SI_STATUS = {
    ACTIVE: "A",
    DEACTIVATED: "X",
    FAILED: "F",
    NOT_APPLICABLE: "Not applicable",
};
export const CLAIM_ACTION_TYPE = {
    NEW_SR: "NEW_SR",
    RESUME_SR: "RESUME_SR",
};

export const USER_TASK = {
    DOC_UPLOAD: "DOC_UPLOAD",
};

export const IDFENCE_SERVICE_NAME = {
    DARK_WEB_MONITORING: "DARK_WEB_MONITORING",
    CREDIT_SCORE: "CREDIT_SCORE",
};
export const REQUEST_TYPE = {
    REQUEST_ONGOING: "REQUEST_ONGOING",
    REQUEST_MEMBERSHIP_TAB: "REQUEST_MEMBERSHIP_TAB",
    REQUEST_HISTORY: "REQUEST_HISTORY",
};
export const RESTRICT_ASSETS = "RESTRICT_ASSETS";
export const SERVICE_REQUEST_API_DATA = {
    REQUEST_TYPE: {
        REQUEST_ONGOING: "REQUEST_ONGOING",
        REQUEST_MEMBERSHIP_TAB: "REQUEST_MEMBERSHIP_TAB",
        REQUEST_HISTORY: "REQUEST_HISTORY",
    },
    STATUS: {
        REQUEST_ONGOING: "P,OH,IP",
        REQUEST_MEMBERSHIP_TAB: "P,OH,IP,CO,CRES",
        DEFAULT: "X,CO,F,CUNR,CRES,CREJ",
    },
    IS_WALKIN_SRS_REQUIRED: "Y",
    REQUIRES_ADDITIONAL_DETAILS: "Y",
    OPTIONS: "showAssets",
    SERVICE_REQUEST_TYPES: "HA_EW,HA_BD,HA_AD,HA_FR,HA_BR,PE_ADLD,PE_THEFT,PE_EW,PMS",
}

export const SERVICE_REQUEST_STATUS = {
    COMPLETE: "CO",
    ON_HOLD: "OH",
    CLOSED: "X",
    CLOSED_UNRESOLVED: "CUNR",
    CLOSED_REJECTED: "CREJ",
    CLOSED_RESOLVED: "CRES",
    IN_PROGRESS: "IP",
    PENDING: "P"
}
export const ASSURED_BUYBACK_PLAN = "Buyback";
export const WHC_ADDR_TYPE = "INSPECTION";
export const ORDER_PLACED = "ORDER_PLACED";
export const EXTENDED_WARRAN = "Extended Warran";
export const ID_FENCE_DARK_WEB_MONITORING = "DARK_WEB_MONITORING";
export const ID_FENCE_CREDIT_SCORE = "CREDIT_SCORE";
export const PRIORITY = {
    USER_ACTION_PENDING_CLAIMS: 0,
    USER_ACTION_PENDING_INSPECTION: 1,
    USER_ACTION_PENDING_SALES: 2,
    OA_ACTION_PENDING_CLAIMS: 3,
    OA_ACTION_PENDING_INSPECTION: 4,
    OA_ACTION_PENDING_SALES: 7,
    ACTION_RENEW: 8,
    CLAIM_SUCCESSFUL: 9,
    ACTIVE_MEMBERSHIP_PERSONAL_ELECTRONICS: 10,
    ACTIVE_MEMBERSHIP_FINANCE: 11,
    ACTIVE_MEMBERSHIP_HOME_APPLIANCE: 12,
    DEFAULT: 13,
    CANCELLED_MEMBERSHIP: 14,
};
export const CLAIM_SERVICE_TYPE = {
    HA_EW:"HA_EW",
    HA_BD:"HA_BD",
    HA_AD:"HA_AD",
    HA_BR:"HA_BR",
    HA_FR:"HA_FR",
    HA_EW_SR:"HA_EW_SR",
    PE_ADLD:"PE_ADLD",
    PE_THEFT:"PE_THEFT",
    PE_EW:"PE_EW",
    PMS:"PMS",
    SOD:"SOD",
}
export const IDFENCE_SUBSCRIPTION_STATUS = {
    CUSTOMER_DOES_NOT_EXIST: "CUSTOMER_DOES_NOT_EXIST",
    CUSTOMER_IN_PROGRESS: "CUSTOMER_IN_PROGRESS",
    CUSTOMER_UNSUBSCRIBED: "CUSTOMER_UNSUBSCRIBED",
    CUSTOMER_ACTIVE: "CUSTOMER_ACTIVE",
    CUSTOMER_DEACTIVATED: "CUSTOMER_DEACTIVATED",
};

export const CARD_STATUS = {
    NEW: "NEW",
    IN_PROGRESS: "IN_PROGRESS",
    SUCCESS: "SUCCESS",
    ERROR: "ERROR",
    AWAITING_CONFIRMATION: "AWAITING_CONFIRMATION",
    DOCUMENT_UPLOAD_PENDING: "DOCUMENT_UPLOAD_PENDING",
    ON_HOLD: "ON_HOLD",
    DOCUMENT_RE_UPLOAD_PENDING: "DOCUMENT_RE_UPLOAD_PENDING",
    PENDING_SUBMISSION: "PENDING_SUBMISSION",
    COMPLETED: "COMPLETED",
    REJECTED: "REJECTED",
    CANCELLED: "CANCELLED",
    RAISED: "RAISED",
    RE_SCHEDULE_VISIT: "RE_SCHEDULE_VISIT",
    POSTDTLPENDING: "POSTDTLPENDING",
    POSTDTLCOMPLETE: "POSTDTLCOMPLETE",
    REUPLOAD: "REUPLOAD",
    PENDING: "PENDING",
    APPROVED: "APPROVED",
    QUEUED: "QUEUED",
    MEM_CANCELLED: "MEM_CANCELLED",
    MEM_REJECTED: "MEM_REJECTED",
    INSPECTION_PENDING: "INSPECTION_PENDING",
    INSPECTION_SCHEDULED: "INSPECTION_SCHEDULED",
    INSPECTION_CANCELLED: "INSPECTION_CANCELLED",
    INSPECTION_FAILED: "INSPECTION_FAILED",
    ACTIVATED_SUCCESSFULLY: "ACTIVATED_SUCCESSFULLY",
    SR_RATING_PENDING: "SR_RATING_PENDING",
    RENEWAL_IN_PROGRESS: "RENEWAL_IN_PROGRESS",
    RESUME_CLAIM_SERVICE: "RESUME_CLAIM_SERVICE",
    LABLE_INSPECTION_SCHEDULED: "INSPECTION SCHEDULED",
    FD_SDT_FLOW : "SDT",
    FD_MT_FLOW : "MT",
    FRONT_PANEL_KEY : "SCREEN_IMAGE",
    BACK_PANEL_KEY : "REAR_PANEL_IMAGE",
    OTHER_PLATFORM_MEMBERSHIP : "MEM_FOR_OTHER_PLATFORM",
    /**
     * Doc STATUS
     */
    STR_ACCEPTED : "AC",
    STR_PENDING : "P"
}
export const MEMBERSHIP_TAB_CARD_TEXT = {
    TEXT_AWAITING_CONFIRMATION_DESCRIPTION: 'We have received your service request. On confirmation, we will send you an SMS/Email with service details.',
    RATE_US: 'Rate Us',

    ON_HOLD_LOWERCASE: 'On hold',
    TXT_VIEW_DETAILS: 'View Details',
    VERIFY_NOW: 'Verify Now',
    TEXT_UPLOAD: 'Upload',
    PENDING_SUBMISSION_1: 'Pending Submission',

    YOU_HAVE_PENDING_SUBMISSION_TEXT:(listSize)=>`You have ${listSize} pending claim submission, do you want to continue?`,
    RENEWAL_MSG_YOUR_MEM_EXP_RW_IN_GRACE_DAYS:(param1,param2)=>`Your membership was expired on ${param1}. You can renew your membership within ${param2} day(s) of expiry date.`,
    RENEWAL_MSG_YOUR_MEM_EXP_RW:(param1)=>`Your membership is expiring on ${param1}.`,
    RENEWAL_MSG_YOUR_MEM_EXP_X:(param1)=>`Your membership is expiring on ${param1}.`,
    RENEWAL_MSG_YOUR_MEM_EXP_RW_WITH_SAVED_AMOUNT:(param1)=>`Valid Till ${param1}.`,

    TEXT_PLAN_ACTIVATION_DESC:'We have received your payment. Your membership will be activated in 24 hrs.',
    TXT_YOUR_REQUEST_PROGRESS:'Your request is in progress.',
    TXT_SELL_YOUR_PHONE_ELIGIBILITY_DES:'You can sell your phone after a few months. Check details for eligibility of your phone.',
    TEXT_VALID_TILL:(param)=>`Valid till ${param}`,
    ACTIVATE_BY:(param)=>`Activate by ${param}`,
    TEXT_VALID_FOR:"Valid from",
    UPLOAD_COMPLETE_ACTIVATION_DESC:(param)=>`Complete activation process before ${param} to avoid membership cancellation.`,
    UPLOAD_PENDING_DESC:(param)=>`Please upload the mandatory documents before ${param} to avoid cancellation`,
    FD_ACTIVATE_NOW:'Activate now',
    FD_GET_STATED:'Get started',
    FD_UPLOAD_DOCUMENTS:'Upload documents',
    FD_BTN_LABEL_CONTINUE:'Continue',
    FD_MT_FLOW_MEMBERSHIP_CANCELLATION:'Please check and submit the front panel and back panel photos to avoid membership cancellation',
    FD_OPEN_IMEI_SCREEN:'Open IMEI Screen',
    FD_MEM_CARD_OPEN_IMEI_SCREEN:(param1,param2)=>`Click the below button to open IMEI screen on your ${param1} mobile.Upload front & back panel images of ${param1} mobile on the link sent to ${param2} via SMS`,
    REUPLOAD_FRONT_BACK_PANEL_DESC:'Please re-upload Front and Back panel photos to avoid membership cancellation',
    REUPLOAD_FRONT_DESC:(param)=>`Please re-upload photo of Front panel before ${param} to avoid membership cancellation`,
    REUPLOAD_BACK_DESC:(param)=>`Please re-upload photo of Back panel before ${param} to avoid membership cancellation`,
    REUPLOAD_DESC:(param)=>`Please re-upload the documents before ${param} to avoid cancellation`,
    PENDING_DESC:'We are verifying your activation details. This process may take upto 2 working day(s)',
    CANCEL_DESC:'Your membership was canceled.  Refund will be processed within 7 working days. For any assistance, you can chat with us.',
    REJECTED_DESC:'Your membership was rejected. Refund will be processed within 7 working days. For any assistance, you can chat with us.',

    DOCUMENT_APPROVED:'ACTIVATED SUCCESSFULLY',
    SR_RESCEDULE_VISIT:'Re-schedule visit',
    MEMBERSHIP_QUEUED:'AWAITING CONFIRMATION',
    TEXT_RESEND_IMAGE_UPLOAD_LINK:'Resend image upload link',
    AWAITING_CONFIRMATION:'AWAITING CONFIRMATION',
    DOCUMENT_UPLOAD_PENDING: 'Document upload pending',
    DOCUMENT_RE_UPLOAD_PENDING: 'Document re-upload pending',
    RE_SCHEDULE_A_VISIT: 'Re-schedule visit',
    RE_SCHEDULE: 'RE-SCHEDULE',
    SERVICE_IN_PROGRESS: 'Service in progress',
    RENEWAL_IN_PROGRESS:'RENEWAL IN PROGRESS',
    DOCUMENT_VERIFICATION_PENDING:'DOCUMENT VERIFICATION PENDING',
    COMPLETED:'COMPLETED',
    MEMBERSHIP_CANCELLED:'MEMBERSHIP CANCELLED',
    MEMBERSHIP_REJECTED:'MEMBERSHIP REJECTED',
    ON_HOLD:'ON HOLD',
    PENDING_SUBMISSION:'PENDING SUBMISSION',
    CANCELLED:'CANCELLED',
    REJECTED:'REJECTED',
    APPLE_MEMBERSHIP:'Activation in Progress',
    ACTIVATE_NOW_TEXT:'Activate Now',
    TXT_RESUME_NOW:'Resume Now',
    TXT_VIEW_STATUS:'View status',
    APPLE_PLAN_ON_ANDROID_MSG:'This plan is for your Apple Mobile (Apple). Please login from any apple mobile to start activation.',
    ANDROID_PLAN_ON_APPLE_MSG:(param)=>`This plan is for your ${param} Mobile(Android). Please login from any android mobile to start activation flow`,
    TEXT_TRIAL_UPGRADE_DESCRIPTION:'You currently have limited protection on your mobile and wallet. Upgrade now to enjoy all benefits.',
    FINANCE_CARD_BILL_PAYMENT_DESCRIPTION:'Never miss your credit card bill payment. Set monthly reminders and pay bills easily.',
    GADGETSERV_PLAN_UNLIMITED_APPLIANCES_COVERED:'GadgetServ Plan - Unlimited appliances covered',
    GADGETSERV_PLAN_UPTO_X_APPLIANCES_COVERED:(param)=>`GadgetServ Plan - Upto ${param} appliances covered.`,
    HOMEASSIST_PLAN_COVERED_1:(param)=>`HomeAssist Plan - ${param} is covered`,
    HOMEASSIST_PLAN_COVERED_2:(param)=>`HomeAssist Plan - ${param} Appliance is covered`,
    HOMEASSIST_PLAN_COVERED_3:(param)=>`HomeAssist Plan -  ${param} Appliances are covered`,
    AS_INSPECTION_WASNT_SCHEDULED_YOUR_MEMBERSHIP_CANCELLED:"As inspection wasn't scheduled, your membership is cancelled. Refund will be processed within 7 working days. For any assistance, chat with us.",
    TEXT_INSPECTION_REQUEST_UNDER_PROCESS:"Your inspection request is under progress",
    TXT_VIEW_INSPECTION_DETAILS:"View inspection details",
    INSPECTION_FAILED:"INSPECTION FAILED",
    INSPECTION_PENDING:"INSPECTION PENDING",
    SCHEDULE_INSPECTION:"Schedule Inspection",
    INSPECTION_CANCELLED:"INSPECTION CANCELLED",
    INSPECTION_FAILED_DESCRIPTION:"Inspection failed as no appliances were in working condition. Refund will be processed within 7 working days. for any assistance, chat with us.",
    PLEASE_SCHEDULE_INSPECTION_BEFORE:(param)=>`Please schedule an inspection before ${param} to renew your membership`,
    PLEASE_SCHEDULE_INSPECTION_BEFORE_1:(param)=>`Please schedule an inspection before ${param} to activate your membership`,
    TEXT_PREVIOUS_CANCELLED_RESCHEDULE:(param)=>`As your previous inspection was cancelled please reschedule before ${param}`,
    TEXT_INSPECTION_NOT_SCHEDULE_GET_IN_TOUCH:'You have not scheduled the inspection yet. Get in touch at the earliest',
    TEXT_SCHEDULE_INSPECTION_OR_WILL_CANCEL:'Please schedule an inspection immediately or your membership will be cancelled.',
    TEXT_PREVIOUS_CANCELLED_DUE_TO_UNAVAILABILITY:'Your previous inspection was cancelled as you were unavailable',
    RENEW_NOW:'Renew Now',
    TEXT_ACTIVATED_SUCCESSFULLY:'Activated Successfully',
    UPGRADE_NOW:'Upgrade now',
    IDFENCE_TRIAL_PLAN_DESC:'Monitor your Debit/Credit Cards, Bank Accounts, PAN and more 24/7 and get data breach alerts on the go',
    IDFENCE_TRIAL_PLAN_DESC_1:'Monitor your Credit score, Debit/Credit Cards, PAN and more 24/7 and get data breach alerts on the go.',
    GET_GUARANTEED_EXCHANGE_MSG:'Get guranteed best exchange value for your smartphone',
    INCLUDES_PREVENTIVE_MAINTENANCE_SERVICE:'(Includes Preventive Maintenance Service)',
    TXT_SERVICE_REQ_ID:(param)=>`Service Request ID ${param}`,
    TXT_IMMEDIATE_ACTION_NEEDED:'Immediate action needed!',


}


export const WORKFLOW_STAGE_CODE = {
    CO: "CO",
    ID: "ID",
    RA: "RA",
    VE: "VE",
    VR: "VR",
    DU: "DU",
    CS: "CS",
    ICD: "ICD",
    DE: "DE",
    RE: "RE",
    PU: "PU",
};
export const   SERVICE_REQUEST_WORKFLOW_STAGE ={
     DOCUMENT_UPLOAD : "DU",
}
export const  SERVICE_REQUEST_WORKFLOW_STAGE_STATUS ={
    DOCUMENT_UPLOAD_PENDING :"DUP",
    TECHNICIAN_CANCELLED_CUSTOMER_NOT_AVAILABLE :"TCCA",
    TECHNICIAN_CANCELLED_OTHER :"TCO",
    CANCELLED_CUSTOMER_CANCELLED :"CACC",
    CANCELLED_DUE_TO_EXPIRY :"CEXP",
    CANCELLED_CUSTOMER_NOT_AVAILABLE :"CCNA",
    CANCELLED_TECHNICIAN_NOT_AVAILABLE :"CTNA",
    SUCCESS :"SU",
    SUCCESSFULLY :"INCS",
    BER :"BER",
    UNRESOLVED :"UNRESOLVED",
    REJECTED :"RJ",
    CUSTOMER_DENIED_PAYMENT :"CDP",
    VERIFICATION_UNSUCCESSFUL :"VU",
    ACCIDENTAL_DAMAGE :"AD",
}
