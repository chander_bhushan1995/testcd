export const OAImageSource = {
    icon_camera: {source: require("../images/icon_camera.webp"), dimensions: {width: 48, height: 48}},

    fb_logo: {source: require("../images/fb_logo.webp"), dimensions: {width: 84, height: 84}},
    twitter_logo: {source: require("../images/twitter_logo.webp"), dimensions: {width: 84, height: 84}},
    instagram_logo: {source: require("../images/instagram_logo.webp"), dimensions: {width: 84, height: 84}},
    linkedin_logo: {source: require("../images/linkedin_logo.webp"), dimensions: {width: 84, height: 84}},

    pan_logo: {source: require("../images/pan_logo.webp"), dimensions: {width: 64, height: 64}},
    passport_logo: {source: require("../images/passport_logo.webp"), dimensions: {width: 64, height: 64}},

    dl_logo: {
        source: require("../images/dl_logo.webp"),
        dimensions: {width: 64, height: 64}
    },

    mobile: {
        source: require("../images/mobile.webp"),
        dimensions: {width: 84, height: 82}
    },
    bank_logo: {
        source: require("../images/bank_logo.webp"),
        dimensions: {width: 84, height: 62}
    },

    chevron_down: {source: require("../images/chevron_down.webp"), dimensions: {width: 12, height: 7}},
    chevron_up: {
        source: require("../images/chevron_down.webp"),
        dimensions: {width: 12, height: 7, transform: [{rotate: '-180deg'}]}
    },
    right_arrow: require("../images/right_arrow.webp"),
    delete: {source: require("../images/delete.webp"), dimensions: {width: 12, height: 15}},
    MasterCard: {
        source: require("../images/MasterCard.webp"),
        dimensions: {width: 24, height: 18}
    },
    Visa: {source: require("../images/icon_camera.webp"), dimensions: {width: 48, height: 48}},

    icon_fb: {
        source: require("../images/icon_fb.webp"),
        dimensions: {width: 24, height: 24}
    },
    icon_insta: {
        source: require("../images/icon_insta.webp"),
        dimensions: {width: 18, height: 18}
    },
    icon_twitter: {
        source: require("../images/icon_twitter.webp"),
        dimensions: {width: 24, height: 24}
    },
    icon_linkedin: {
        source: require("../images/icon_linkedin.webp"),
        dimensions: {width: 24, height: 24}
    },

    ssl: {
        source: require("../images/ssl.webp"),
        dimensions: {width: 46, height: 17}
    },
    pci: {
        source: require("../images/pci.webp"),
        dimensions: {width: 52, height: 20}
    },

    norton: {
        source: require("../images/norton.webp"),
        dimensions: {width: 41, height: 20}
    },
    checkBox: {
        source: require("../images/ic_check_box.webp"),
        dimensions: {width: 18, height: 18}
    },
    checkBoxBlank: {
        source: require("../images/ic_check_box_outline_blank.webp"),
        dimensions: {width: 18, height: 18}
    },
    chevron_down_right: {
        source: require("../images/chevron_down.webp"),
        dimensions: {width: 12, height: 7, transform: [{rotate: '-90deg'}]}
    },
    not_alert_found: require("../images/not_alert_found.webp"),
    credit_score_inner_circle: require("../images/credit_score_inner_circle.webp"),
    credit_score_view_trend: require("../images/view_trend.webp"),
    pro_tip: require("../images/pro_tip.webp"),
    info_credit_score: require("../images/info_credit_score.webp"),

    info_indicator: require("../images/info_indicator.webp"),
    not_data_found_error: require("../images/not_data_found_error.webp"),
    credit_card_new_badge: require("../images/credit_card_new_badge.webp"),
    mail_credit_score: require("../images/mail_credit_score.webp"),
    call_credit_score: require("../images/call_credit_score.webp"),
    experian_credit_score: require("../images/experian_credit_score.webp"),
    white_cross_img: require("../images/white_cross_img.webp"),
    icon_back_black_img: require("../images/icon_back_black.webp"),
    trend_tooltip: require("../images/trend_tooltip.webp"),
    selected_trend_indicator: require("../images/selected_trend_indicator.webp"),
    non_selected_trend_indicator: require("../images/non_selected_trend_indicator.webp"),
    score_up_indicator: require("../images/score_up.webp"),
    score_down_indicator: require("../images/score_down.webp"),
    star_selected: require("../images/star.webp"),
    star_unselected: require("../images/star_unselected.webp"),
    arrow_down: require("../images/arrow_down.webp"),
    ic_tick_right: require("../images/ic_tick_right.webp"),
    tip_light_buib: require("../images/tip_light_buib.webp"),
    deafult_dark_bg_card: require("../images/deafult_dark_bg_card.png"),
    default_white_bg_card: require("../images/default_white_bg_card.png"),

    tick_left_image:
        {
            source: require("../images/tick_left_image.webp"),
            dimensions: {width: 15, height: 15}
        },
    dismiss_cross_recommendation:
        {
            source: require("../images/dismiss_cross_recommendation.webp"),
            dimensions: {width: 44, height: 44}
        },

    cta_arrow_blue: {
        source: require("../images/cta_arrow_blue.webp"),
        dimensions: {width: 14, height: 7}
    },
    my_favorites_heart: {
        source: require("../images/my_favorites_heart.webp"),
        dimensions: {width: 16, height: 14}
    },
    back_arrow: {source: require("../images/back_arrow.webp"), dimensions: {width: 23, height: 21}},
    icon_back_img: {
        source: require("../images/icon_back.webp"),
        dimensions: {width: 24, height: 24}
    },
    fav_recommendation_selected: {
        source: require("../images/fav_recommendation_selected.webp"),
        dimensions: {width: 24, height: 24}
    },
    fav_recommendation_unselected: {
        source: require("../images/fav_recommendation_unselected.webp"),
        dimensions: {width: 24, height: 24}
    },
    fav_recommendation_loading_selected: {
        source: require("../images/favorite_loading_select.gif"),
        dimensions: {width: 24, height: 24}
    },
    fav_recommendation_loading_unselected: {
        source: require("../images/favorite_loading_unselect.gif"),
        dimensions: {width: 24, height: 24}
    },
    white_arrow: {
        source: require("../images/white_arrow.webp"),
        dimensions: {width: 10, height: 12}
    },
    cta_arrow_black: {
        source: require("../images/cta_arrow_black.webp"),
        dimensions: {width: 20, height: 20}
    },
    cta_arrow_white: {
        source: require("../images/cta_arrow_white.webp"),
        dimensions: {width: 20, height: 20}
    },
    more_menu: {
        source:require("../images/more_menu.webp"),
        dimensions: {width: 24, height: 24}
    },
    green_right: {
        source: require("../images/green_right.webp"),
        dimensions: {width: 20, height: 20}
    },
};
