export const SendMessageStatus = {
    Pending: 'SendMessageStatusPending',
    Sending: 'SendMessageStatusSending',
    Success: 'SendMessageStatusSuccess',
    Failure: 'SendMessageStatusFailure',
    NoInternet: 'SendMessageStatusNoInternet',
    CannotSend: 'SendMessageStatusCannotSend'
}