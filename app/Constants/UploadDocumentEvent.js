export const NOT_STARTED = 'not_started';
export const UPLOADING = 'uploading';
export const COMPLETE = 'complete';

export const THUMBNAIL_IMAGE_FILE = 'ThumbnailImageFile';
export const BYTEARRAYFILE = 'ByteArrayFile';
