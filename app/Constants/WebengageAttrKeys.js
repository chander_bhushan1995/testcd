import { AllAssetKeys } from "../idfence/constants/Constants";
import { getProductWithServicesList } from "../BuyPlan/Helpers/BuyPlanUtils";
//attribute keys
export const LOCATION = "Location";
export const MEMBERSHIP_STATUS = "Membership Status";
export const CATEGORY = "Category";
export const PRODUCT = "Product";
export const ASSET_NAME = "Asset Name";
export const RESULT = "Result";
export const TYPE = "Type";
export const PREMIUM_AMOUNT = "Premium Amount";
export const FINAL_PREMIUM_AMOUNT = "Final Premium Amount";
export const SERVICE = "Service";
export const ADDON_SERVICE = "Addon Service";
export const COVER_AMOUNT = "Cover Amount";
export const DISCOUNT = "Discount";
export const INVOICE_AMOUNT = "Invoice Amount";
export const INVOICE_DATE = "Invoice Date";
export const PLAN = "Plan";
export const PLAN_CODE = "Plan Code";
export const PLAN_NAME = "Plan Name";
export const BRAN_AND_MODEL = "Brand and Model";
export const SUPPORTED = "Supported";
export const STAGE = "Stage";
export const PRICE = "Price";
export const CONTENT_ID = "content_id";
export const CURRENCY = "currency";
export const CONTENT_TYPE = "content_type";
export const PAGE_PLAN_NAME = "pagePlanName";
export const PAGE_PLAN_Code = "pagePlanCode";
export const NO_OF_APPLIANCE = "Number of Appliances";
export const APPLIANCE_NAME = "Appliance Name";
export const SERVICEABLE = "Serviceable";
export const PINCODE = "Pincode";
export const APPLIANCE = "Appliance";
export const COUNT = "Count";
export const SERVICE_TYPE = "ServiceType";
export const POSITION = "Position";
export const SCREEN_DURATION = "ScreenDuration";
export const COST = "Cost";
export const DATE = "Date";
export const TIME = "Time";
export const SR_NUMBER = "SR Number";
export const EMAIL_ID = "Email Id";
export const ABB = "abb";
export const SERVED_BY = "served by";
export const RETAILER_NAME = "retailer name";
export const OFFER_PRICE = "offer price";
export const TAG = "Tag";
export const TAGS = "Tags";
export const EXCHANGE_PRICE = "exchange price";
export const BRAND = "brand";
export const AGE = "Age";
export const RETAILER_COUNT = "retailer count";
export const STAR_RATING = "Star Rating";
export const BLOG_TITLE = "blogTitle";
export const ALREADY_HAS_PLAN = "hasPlan";
//Event keys
export const PROCEED_PAYMENT = "Proceed Payment";
export const ACTIVATION = "Continue Activation";
export const SCHEDULE_INSPECTION = "Schedule Inspection";
export const SOD_DETAILS_SUBMIT = "SOD Details Submit";
export const SOD_SR_DETAIL = "SOD SR Detail";
export const FD_MODE = "Mode";
export const VARIATIONID = "VariationId";

// Apps Flyer specific attributes
export const AF_PRICE = "af_price"
export const AF_CONTENT_ID = "af_content_id"
export const AF_CONTENT_TYPE = "af_content_type"
export const AF_CURRENCY = "af_currency"
export const AF_CONTENT_LIST = "af_content_list"

//ATTR VALUES
export const SERVICE_ON_DEMAND = "Service On Demand";
export const FD_WE_ATTRIBUTE_VALUE_SECONDARY_DEVICE = "Secondary Device";
export const FD_WE_ATTRIBUTE_VALUE_MIRROR = "Mirror";

export const FD_SDT_FLOW = "SDT";
export const FD_MT_FLOW = "MT";
/***
 *  IDFence webengage events
 */
export const WEBENGAGE_IDFENCE_DASHBOARD = {
    EVENT_NAME: {
        ID_TAB: "Tab",
        ID_ALERTS: "ID Alerts",
        ID_CARD_LOAD: "ID Card Load",
        ID_TODO_LOAD: "ID Todo Load",
        ID_DETAILS: "ID Details",
        ID_DASHBOARD_DISMISS_PROMPT: "ID Dashboard Dismiss Prompt",
        ID_DASHBOARD_PROMPT_LOAD: "ID Dashboard Prompt Load",
        ID_WHY_LINK_ACCOUNT: "ID Why Link Account",
        ID_ALERT_FILTER: "ID Alert Filter",
        ID_ALERT_DOWNLOAD_REPORT: "ID Alert Download Report",
        ID_ALERT_RECOMMENDATION: "ID Alert Recommendation",
        ID_ALERT_FILTER_APPLY: "ID Alert Filter Apply",
        ID_ALERT_FILTER_DISMISS: "ID Alert Filter Dismiss",
        ID_ASSET_DELETE: "ID Asset Delete",
        ID_ALERT_VIEW: "ID Alert View",
        ID_GIF_ICON: "ID GIF Icon",
        ID_EXTENER_LINK_RECOMMENDED_ACTION: "ID Extener Link Recommended Action",
        ID_DETAIL_BUCKET: "ID Detail Bucket",
        ID_PAYMENT_DETAILS_UPDATE: "ID Payment Details Update",
        ID_PAYMENT_DETAILS_DOWNLOAD_INVOICE: "ID Payment Details Download Invoice",
        ID_DASHBOARD_BUY_PREMIUM_FOOTER_LOAD: "ID Dashboard Buy Premium Footer Load",
        ID_DASHBOARD_BUY_PREMIUM_FOOTER: "ID Dashboard Buy Premium Footer",
        GO_TO_CREDIT_SCORE_DASHBOARD: "Go to Credit Score Dashboard",
        ID_DASHBOARD_CREDIT_SCORE_CARD_LOAD: "ID Dashboard Credit Score card Load",
        ID_DASHBOARD_CREDIT_SCORE_RETRY_ACTIVATION: "ID Dashboard Credit Score Retry Activation",
        ID_RETRY_ACTIVATION_SUBMIT: "ID Retry Activation Submit ",
        ID_TODO_CARD_DISMISS: "ID Todo Dismiss",
        ID_TODO_CARD_TAP: "ID Todo Card Tap",
        ID_ASSET_OPEN_TO_ADD: "ID Asset Open to Add",
        ID_ASSET_ADDED: "ID Asset Added",
        ID_DASHBOARD_ACCESS: "IDFenceDashboardAccess",
        DASHBOARD_BOTTOM_SHEET_LOAD: "Dashboard_Bottom_Sheet_Load",
        DASHBOARD_BOTTOM_SHEET_CTA_TAB: "Dashboard_Bottom_Sheet_CTA_tap",
        IDFence_Blog_Subscription: "IDFence_Blog_Subscription",
        ID_FENCE_CANCEL_MEMBERSHIP_COMPLETE: "IDFence_Cancel_Membership_Complete",
        ID_FENCE_BLOG_CARD_CLICK: "IDFence_Blog_Card_Tap",
        ID_FENCE_SHARE_CARD_CLICK: "IDFence_Share_Tap",
        ID_FENCE_MEM_START_CANCEL: "IDFence_Cancel_Membership_Start_1",
        ID_FENCE_MEM_CONFIRM_CANCEL: "IDFence_Cancel_Membership_Confirm",


    },
    ATTR_NAME: {
        ASSET_NAME: "Asset Name",
        MOBILE_NUMBER: "Mobile Number",
        MEMBERSHIP_ID: "Membership ID",
        PLAN_CODE: "Plan Code",
        PLAN_NAME: "Plan Name",
        MEMBERSHIP_STATUS: "Membership Status",
        SOURCE: "Source",
        TYPE: "Type",
        BUCKET_NAME: "Bucket Name",
        SI_STATUS: "SI Status",
        STATUS: "Status",
        ALERT_TYPE: "Alert type",
        BP: "BP",
        BU: "BU",
    },
    ATTR_VALUE: {
        SOURCE_ATTR_VALUE: "App",
        OVERVIEW_ATTR_VALUE: "Overview",
        ALERTS_TAB_ATTR_VALUE: "Alerts Tab",
        ASSET_TAB_ATTR_VALUE: "Asset Tab",
        ID_ALERTS_OVERVIEW_NOTIFICATION_TRAY_VALUE: "Overview_Notification_Tray",
        ID_FENCE_DASHBOARD: "IDFence Dashboard",
        MEMBERSHIP_DETAIL_TAB: "Membership Detail Tab",
        DASHBOARD: "Dashboard",
        TYPE_ATTR_ID_DASHBORD_DISMISS_PROMPT_VALUE: {
            VERIFY_EMAIL: "Verify Email",
            SOCIAL_MEDIA: "Social Media",
            Update_Payment_Method: "Update Payment Method",
        },
        ID_ALERT_DOWNLOAD_REPORT_ATTR_VALUE: {
            ALERT_TAB: "Alert Tab",
            ALERT_DETAIL_PAGE: "Alert Detail Page",
        },
        ID_DASHBOARD_CREDIT_SCORE_CARD_LOAD_ATTR_VALUE: {
            ACTIVE: "Active",
            NO_SCORE: "NO Score",
            IN_PROCESSING: "In Processing",
        },
        LOCATION_ATTR_VALUE: {
            OVERVIEW: "Overview",
            ALERTS: "Alerts",
            ASSETS: "Assets",
            DETAILS: "Details",
        },
        ID_DETAIL_BUCKET_ATTR_VALUE: {
            PLAN_DETAILS: "Plan Details",
            PAYMENT_DETAILS: "Payment Details",
            BASIC_DETAILS: "Basic Details",
            PLAN_BENEFITS: "Plan Benefits",
            PAYMENT_HISTORY: "Payment History",
            FAQ: "FAQ",
        },

        ID_ASSET_NAME_ATTR_VALUE: {
            EMAIL: "Email",
            TELEPHONE: "Mobile",
            CARD: "CC/DC",
            BANK: "Bank Accounts",
            NATIONAL_ID: "National id",
            FACEBOOK: "FB",
            LINKEDIN: "LinkedIn",
            INSTAGRAM: "Instagram",
            TWITTER: "Twitter",
            PAN: "PAN",
            PASSPORT: "Passport",
            DL: "DL",
        },

        attrValueForAsset: (asset) => {
            switch (asset) {

                case AllAssetKeys.EMAIL:
                    return WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_ASSET_NAME_ATTR_VALUE.EMAIL;
                case AllAssetKeys.MOBILE:
                    return WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_ASSET_NAME_ATTR_VALUE.TELEPHONE;
                case AllAssetKeys.BANK_ACCOUNT:
                    return WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_ASSET_NAME_ATTR_VALUE.BANK;
                case AllAssetKeys.CREDIT_DEBIT:
                    return WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_ASSET_NAME_ATTR_VALUE.CARD;
                case AllAssetKeys.DRIVING_LICENCE:
                    return WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_ASSET_NAME_ATTR_VALUE.DL;
                case AllAssetKeys.PASSPORT:
                    return WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_ASSET_NAME_ATTR_VALUE.PASSPORT;
                case AllAssetKeys.PAN_CARD:
                    return WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_ASSET_NAME_ATTR_VALUE.PAN;
                case AllAssetKeys.FACEBOOK:
                    return WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_ASSET_NAME_ATTR_VALUE.FACEBOOK;
                case AllAssetKeys.TWITTER:
                    return WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_ASSET_NAME_ATTR_VALUE.TWITTER;
                case AllAssetKeys.INSTAGRAM:
                    return WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_ASSET_NAME_ATTR_VALUE.INSTAGRAM;
                case AllAssetKeys.LINKEDIN:
                    return WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_ASSET_NAME_ATTR_VALUE.LINKEDIN;

                default:
                    return "";
            }
        },
    },
};
