// All the webengage attribute constant define here

export const MOBILE_BUYBACK = "Mobile Buyback";
export const SHARE_ICON = "Share icon";
export const SHARE_BUTTON = "Share Button";
export const BUYBACK_REQUEST_LISTING = "Buyback Request Listing";
export const YES = "Yes";
export const NO = "No";
export const MHC = "MHC";
export const CASH = "Cash";
export const PAYTM = "Paytm";
export const UPI = "UPI";
export const IMPS = "IMPS";
export const SERVICEABLE = "Pincode Serviceability";

export const PURCHASE_TYPE_ATTR_VALUE="App";
export const RESULT_ATTR_VALUE_FAIL = "Failure";
export const RESULT_ATTR_VALUE_SUCCESS = "Success";
export const ATTR_TYPE_VALUE_WALLET = "Wallet";
export const ATTR_TYPE_VALUE_MOBILE = "Mobile";
