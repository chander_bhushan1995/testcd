// All the webengage constant define here
export const PINCODE = "Pincode";
export const RISK_CALCULATOR_GET_STARTED = "Risk Calculator Get Started";
export const RISK_CALCULATOR_SUBMIT = "Risk Calculator Submit";
export const SHARE_RISK_CALCULATOR = "Share Risk Calculator";
export const CHECK_PINCODE_AVAILABILITY = "Check Pincode Availability";
export const MHC_CHECK_NOW = "MHC Check Now";
export const ADDITIONAL_DETAILS = "Additional Details";
export const ADDITIONAL_DETAILS_SUBMIT = "Additional Details Submit";
export const BUYBACK_QUOTE_GEN = "buyback_quote_gen";
export const UPLOAD_INVOICE = "Upload Invoice";
export const SCHEDULE_PICKUP = "Schedule Pickup";
export const BUYBACK_PAYMENT_MODE = "Buyback Payment Mode";
export const BUYBACK_PAYMENT_DETAILS = "Buyback Payment Details";
export const VIEW_NEARBY_STORES = "View Nearby Stores";
export const SUBMIT_PAYMENT_DETAILS = "Submit Payment Details";
export const RATING_POPUP = "Rating Popup";
export const RATING_SUBMITTED = "Rating Submitted";
export const RATE_NOW = "Rate Now";
export const NOT_NOW = "Not Now";
export const CHAT_ON_MEMBERSHIP_DETAIL= "char on membership detail tapped"
export const APPLY_PROMO = "Apply Promo";
export const BUYBACK_COUPON_SCREEN = "Buyback Coupon Screen";
export const BUYBACK_TIMELINE_SCREEN = "Buyback Timeline";
export const AF_PURCHASE = "af_purchase";
export const FIRST_PURCHASE = "first_purchase";
export const FIND_A_PLAN = "Find a plan";
export const PLAN_DETAIL_PRIMARY_CTA = "Plan Detail Page Primary CTA Tap";
export const PLAN_DETAIL_PAGE_LOAD = "plan_detal_page_load";
export const CALL_RETAILER = "Call retailers";
export const GET_DIRECTION = "Get Direction";
export const VISIT_CONFIRMATION = "Visit Confirmation";
export const CANCEL_REQUEST = "Cancel request";
export const CANCEL_REQUEST_CONFIRMATION = "Cancel request confirmation";
export const UPGRADE_IDFENCE = "Upgrade ID Fence"
// For Payment flow

export const MOBILE = "Mobile";
export const WALLET = "Wallet";
export const CONTINUE_ACTIVATION = "Continue Activation"
export const PURCHASE_TYPE = "Purchase type";
export const REGISTER = "Register";
export const ADDRESS = "Address";
export const PLAN_PURCHASED = "Plan Purchased";
export const ACTIVATION_STRIP = "Activation Strip"


// For Catalyst flow

export const CATALYST_RECOMMENDATION_SCREEN = "Catalyst Landing Screen";
export const CATALYST = "Catalyst";
export const CATALYST_LANDING_SCREENVIEW = "Catalyst Landing Screenview";
export const CATALYST_VIEW_PLAN = "Catalyst View Plan";

export const WALKTHROUGH_GET_STARTED = "Walkthrough Get Started";
export const WALKTHROUGHT_LOGIN = "Walkthrough Login";
export const CATALYST_CARD_VIEW = "Catalyst Card View";
export const CATALYST_CARD_CLICK = "Catalyst Card Click";
export const SPOTLIGHT_VIEW = "Spotlight View";
export const SPOTLIGHT_CLICK = "Spotlight Click";
export const CATALYST_FAVOURITE = "Catalyst Favourite";
export const CATALYST_DISMISS = "Catalyst Dismiss";
export const HOME_TAB = "Home Tab";
export const UPDATE_CATALYST_RECO = "Update Catalyst Reco";
export const UPDATE_CATALYST_NO_RECO = "Update Catalyst No Reco";
export const CATALYST_AUTO_REFRESH = "Catalyst Auto Refresh";
export const NO_CATALYST_RECO = "No Catalyst Reco";
export const COMPLETE_PROFILE = "Complete Profile";
export const PROFILE = "Profile";
export const ONBOARDING_SKIP = "Onboarding Skip";
export const REWARD_UNLOCK_POPUP = "Reward Unlock Popup";
export const REWARD_UNLOCKED = "Reward Unlocked";
export const MY_REWARDS = "My Rewards";
export const IDFT_UNLOCKED = "IDFT Unlocked";

export const MHC_QUICK_TEST = "MHC Quick Test";
export const LOGIN_SKIP = "Login Skip";
export const ALL_OFFERS = "All Offers";
export const SHARE_APP = "Share app";
export const RATE_APP = "Rate app";
export const ACTIVATE_VOUCHER = "Activate Voucher";
export const LOGOUT = "Logout";
export const CHAT = "Chat";
export const CHAT_ON_HOME_TAPPED = "chat on home tapped";


//Screen Names
export const WALKTHROUGH = "Walkthrough";
export const MY_REWARDS_SCREEN = "MyRewards";
export const MY_PROFILE = "MyProfile";
export const RECO_SCREENVIEW = "Reco Screenview";
export const CATALYST_FAVOURITE_SCREENVIEW = "Catalyst Favourite Screenview";
export const RECO_SCREEN = "Reco Screen";
export const CATALYST_FAVOURITE_SCREEN = "Catalyst Favourite Screen";
// For chat
export const START_CHAT_V1_REQUEST = "start chat request";
export const START_CHAT_V1_RESPONSE = "start chat response success";
export const START_CHAT_V2_REQUEST = "Start Chat V2 Request";
export const START_CHAT_V2_RESPONSE = "Start Chat V2 Response";
export const CREATE_ACCOUNT_V2_REQUEST = "Create Account V2 Request";
export const CREATE_ACCOUNT_V2_RESPONSE = "Create Account V2 Response";
export const CREATE_ACCOUNT_V2_INIT = "Create Account V2 Init";

export const CHAT_HISTORY_REQUEST = "Chat history request";
export const CHAT_HISTORY_RESPONSE = "Chat history response";
export const CHAT_HISTORY_ERROR = "Chat history error";
export const CHAT_EXIT = "Chat exit";
export const USER_CHAT_EXIT_WITHOUT_FIRST_MSG = "User exited chat without seeing first message";
export const SP_DOC_UPLOAD_FAILURE = "Sp doc upload failure";
export const ENABLE_CHAT_BOT_INPUT = "Enable Chat Bot Input";
// Location
export const PLAN_RECOMMENDATION_SCREEN = "Plan Recommendation Screen";

//For BuyPlan
export const SOD_PRODUCT_SCREEN = "SOD Product Screen";
export const SOD_PROCEED_TO_BOOK = "SOD Proceed to Book";
export const SOD_PRODUCT_SERVICE_SCREEN = "SOD Product Service Screen";
export const AUTO_SCROLL_SERVICES = "Auto-Scroll Services";
export const MOBILE_BUYBACK_INTENT = "Mobile Buyback Intent"
export const WATCH_VIDEO = "Watch Video";
export const SELECT_SERVICE = "Select Service";
export const SELECT_PRODUCT = "Select Product";
export const SELECT_PRODUCT_SERVICE = "Select Product Service";
export const PRODUCT_SERVICE_SCREENVIEW = "Product Service Screenview";
export const VIEW_MORE_REVIEWS_EVENT = "View More Reviews";
export const HOW_CLAIM_WORKS = "See How Claim Works";
export const CLAIM_PROCESS_SCREENVIEW = "Claim Process Screenview";
// export const FIND_A_PLAN = "Find a plan";
export const SEE_ALL_BENEFITS = "See Plan Benefits";
export const SELECT_PLAN = "Select Plan";
export const PINCODE_SERVICEABILITY = "Pincode Serviceability";
export const CONTENT_TYPE = "content_type";
export const CONTENT_LIST = "content_list";
export const WHATS_NOTE_COVERED = "Plan Whats Not Covered";
export const CALCULATE_RISK = "Calculate Risk";
export const SELECT_DEVICE_BRAND = "Select Device Brand";
export const SELECT_INVOICE_RANGE = "Device Invoice Amount Click";
export const INVOICE_AMOUNT_SELECTED = "Device Invoice Amount";
export const NO_PLANS_FOUND_EVENT = "No Plans Found";
export const SEE_BEST_PLANS = "See Best Plans";

export const BUY_SUBTAB = "Buy Subtab"
export const SELECT_NO_OF_APPLIANCE = "Select No. of Appliance";
export const SELECT_APPLIANCE = "Select Appliance";

//appsflyer events
export const AF_CONTENT_VIEW = "af_content_view";
export const AF_LIST_VIEW = "af_list_view";
