import {StyleSheet} from "react-native";
import colors from "../colors";
import fontSizes from "../FontSizes";

StyleSheet.create({
    ButtonFontSize_14_normal: {
        color: colors.color_808080,
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_normal,
        fontSize: fontSizes.fontSize14
    }
});
