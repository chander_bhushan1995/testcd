import {StyleSheet} from "react-native";
import colors from "../colors";
import fontSizes from "../FontSizes";
import dimens from "../Dimens";

export default StyleSheet.create({


    TextFontSize_14_bold: {
        lineHeight: 24,
        color: colors.color_000000,
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_bold,
        fontSize: fontSizes.fontSize14
    },
    TextFontSize_8_normal: {
        lineHeight: 10,
        color: colors.color_808080,
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_normal,
        fontSize: fontSizes.fontSize8
    },
    TextFontSize_8_bold_808080: {
        lineHeight: 10,
        color: colors.color_808080,
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_bold,
        fontSize: fontSizes.fontSize8
    },
    TextFontSize_10_bold_888F97: {
        lineHeight: 18,
        color: colors.color_888F97,
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_normal,
        fontSize: fontSizes.fontSize10
    },
    TextFontSize_18_bold_212121: {
        lineHeight: 26,
        color: colors.color_212121,
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_bold,
        fontSize: fontSizes.fontSize18
    },
    TextFontSize_18_normal_212121: {
        lineHeight: 26,
        color: colors.color_212121,
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_normal,
        fontSize: fontSizes.fontSize18
    },
    TextFontSize_20_bold_212121: {
        lineHeight: 24,
        color: colors.color_212121,
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_bold,
        fontSize: fontSizes.fontSize20
    },
    TextFontSize_12_bold_212121: {
        lineHeight: 20,
        color: colors.color_212121,
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_bold,
        fontSize: fontSizes.fontSize12
    },
    TextFontSize_12_normal_212121: {
        lineHeight: 20,
        color: colors.color_212121,
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_normal,
        fontSize: fontSizes.fontSize12
    },
    TextFontSize_10_normal_0282F0: {
        lineHeight: 16,
        color: colors.color_888F97,
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_normal,
        fontSize: fontSizes.fontSize10
    },
    TextFontSize_16_normal: {
        lineHeight: 18,
        color: colors.color_DE000000,
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_normal,
        fontSize: fontSizes.fontSize16
    },
    TextFontSize_16_bold: {
        lineHeight: 18,
        color: colors.color_DE000000,
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_bold,
        fontSize: fontSizes.fontSize16
    },
    TextFontSize_14_normal: {
        lineHeight: 18,
        color: colors.color_808080,
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_normal,
        fontSize: fontSizes.fontSize14
    },

    TextFontSize_12_normal: {
        lineHeight: 18,
        color: colors.color_8A000000,
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_normal,
        fontSize: fontSizes.fontSize12
    },
    TextFontSize_34_bold: {
        lineHeight: 41,
        color: colors.color_FFFFFF,
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_bold,
        fontSize: fontSizes.fontSize34
    },
    TextFontSize_28_bold: {
        lineHeight: 34,
        color: colors.color_212121,
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_bold,
        fontSize: fontSizes.fontSize28
    },
    TextFontSize_12_bold: {
        lineHeight: dimens.dimen15,
        color: colors.color_212121,
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_bold,
        fontSize: fontSizes.fontSize12
    },
});
