import React from "react";
import { View, Image, StyleSheet, Text, TouchableOpacity } from "react-native";
import Dimens from "../Constants/Dimens";
import { TextStyle, CommonStyle } from "../Constants/CommonStyle";
import spacing from "../Constants/Spacing";
import colors from "../Constants/colors";
import LinkButton from "../CustomComponent/LinkButton";
export default function AccountsCard(props) {

  let { data, style, onClick} = props;
  let { titleStyle, buttonStyle, actionTextStyle} = style;
  let {imageSource, title, linkUrl} = data;

  return (
    <TouchableOpacity style={styles.container}
                      onPress={() => onClick(linkUrl)}
                      activeOpacity={1}>
      <View style={styles.headerViewStyle}>
      <Image
        source={imageSource.source}
        style={imageSource.dimensions}
      />
        <Text style={[styles.titleStyle, titleStyle]}>
          {title}
        </Text>
      </View>

      <LinkButton
          data={data}
          onPress={() => onClick(linkUrl)}
          style={{
            buttonStyle: buttonStyle,
            actionTextStyle: actionTextStyle
          }}
        />
    </TouchableOpacity>
  );
}

const handleClick = index => {
  alert("Button clicked!");
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: spacing.spacing8,
    padding: spacing.spacing12,
    marginTop: spacing.spacing4,
    alignItems: "flex-end"
  },
  imgCross: {
    width: Dimens.dimen64,
    height: Dimens.dimen64
  },
  titleStyle: {
    ...TextStyle.text_14_normal,
    flex: 1,
    marginLeft: spacing.spacing12
  },
  headerViewStyle: {
    flexDirection: 'row'
  }
});
