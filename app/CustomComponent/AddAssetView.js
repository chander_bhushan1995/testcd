import {Dimensions, StyleSheet, Text, View} from 'react-native';
import RBSheet from '../Components/RBSheet';
import React, {useState, useEffect} from 'react';
import spacing from '../Constants/Spacing';
import {TextStyle} from '../Constants/CommonStyle';
import colors from '../Constants/colors';
import InputBox from '../Components/InputBox';
import ButtonWithLoader from '../CommonComponents/ButtonWithLoader';

export function AddAssetPopup(props) {
    let height = props.height;
    let popupMaxHeight = Dimensions.get('window').height * 0.72; //TODO: move in constants
    height = height > popupMaxHeight ? popupMaxHeight : height;
    return (
        <RBSheet
            ref={ref => {
                this.RBSheet = ref;
            }}
            duration={10}
            height={height}
            closeOnSwipeDown={false}
        >
            <IDFenceAdd item={props.item} onPress={props.onPress}/>
        </RBSheet>
    );
}

export const openAddPopup = () => {
    this.RBSheet.open();
};

export const closeAddPopup = () => {
    this.RBSheet.close();
};

function IDFenceAdd(props) {
    let item = props.item;
    const [errorMessage, setErrorMessage] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [text, setText] = useState('');

    const errorPadding = errorMessage === ''? spacing.spacing0: spacing.spacing8;

    return (
        <View style={styles.container}>
            <InputBox
                containerStyle={styles.textInputContainer}
                inputHeading={item.title}
                autoCapitalize={true}
                placeholder={item.placeholder}
                placeholderTextColor={item.placeHolderColor}
                isMultiLines={item.isMultiLines}
                keyboardType={item.keyboardType}
                editable={true}
                errorMessage={errorMessage}
                onFocus={ () => setErrorMessage('') }
                onChangeText={(text) => {
                    setText(text);
                }}
            />
            <Text style={{color:colors.color_CF021B}}>*error message</Text>
            
            <View style={[styles.ActionButtonContainer, {marginTop: errorPadding}]}>
                <ButtonWithLoader
                    isLoading={isLoading}
                    enable={!isLoading}
                    Button={{
                        text: item.btnText,
                        onClick: () => {
                            if (text === '') {
                                setErrorMessage(item.errorMessage);
                                return;
                            }

                            if (!item.validationMethodRef(text, false)) {
                                setErrorMessage(item.validationErrorMessage);
                                return;
                            }
                            // setIsLoading(true);
                            props.onPress(text)
                            closeAddPopup()
                        },
                    }}
                />
            </View>
        </View>
    );

}

const styles = StyleSheet.create({
    container: {marginHorizontal: spacing.spacing20, marginTop: spacing.spacing24, marginBottom: spacing.spacing16},
    title: {
        ...TextStyle.text_12_normal,
        color: colors.charcoalGrey,
    },
    textInputContainer: {
        width: '100%',
    },
    ActionButtonContainer: {
        height: 48,
    },
});

