import React from 'react';
import {Dimensions, ImageBackground, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {TextStyle} from '../Constants/CommonStyle';
import spacing from '../Constants/Spacing';
import colors from '../Constants/colors';
import OALinearGradient from '../CommonComponents/OALinearGradient';
import LinearGradient from 'react-native-linear-gradient';

export default function BlogCard(props) {

    let {data, onClick} = props;
    let {webPageURL} = data;

    return (
        <TouchableOpacity style={styles.container}
                          onPress={() => onClick(data.title, webPageURL)}
                          activeOpacity={1}>
            <ImageBackground source={{uri: data.imageUrl}} style={styles.image}>
                {getGradiendCell(data.title, data.subtitle)}
            </ImageBackground>
        </TouchableOpacity>
    );
}

function getGradiendCell(title, subTitle) {
    return (
        <LinearGradient style={styles.gradientViewStyle} start={{x: 0, y: 0}}
                        end={{x: 0, y: 1}} colors={[colors.transparent, colors.black]}>
            <Text style={styles.titleStyle} numberOfLines={2}>
                {title}
            </Text>
            <Text style={styles.subTitleStyle} numberOfLines={1}>
                {subTitle}
            </Text>
        </LinearGradient>
    );
}

const styles = StyleSheet.create({
    container: {
        borderRadius: spacing.spacing8,
        overflow: 'hidden',
        marginTop: spacing.spacing4,
        width: 280,
        height: 208,
    },
    image: {
        flex: 1,
        resizeMode: 'cover',
        justifyContent: 'center',
        paddingTop: spacing.spacing120,
        backgroundColor: colors.grey,
    },
    backgroundViewStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
    },

    gradientViewStyle: {
        flexDirection: 'row',
        height: 98,
        bottom: 0,
        paddingHorizontal: spacing.spacing12,
        paddingBottom: spacing.spacing12,
        alignItems: 'flex-end',
    },
    titleStyle: {
        ...TextStyle.text_14_bold,
        color: colors.white,
        marginRight: spacing.spacing50,
        flex: 1,
    },
    subTitleStyle: {
        ...TextStyle.text_10_regular,
        color: colors.white,
        alignSelf: 'flex-end',
    },
});
