import React, {useEffect} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {CommonStyle, TextStyle} from '../Constants/CommonStyle';
import TagComponent from '../Catalyst/components/common/TagsComponent';
import colors from '../Constants/colors';
import spacing from '../Constants/Spacing';
import RightArrowButton from '../CommonComponents/RightArrowButton';
import Images from '../images/index.image';

const DetailCard = props => {

    const {data, onClick, containerStyle, AccessoryView} = props;
    const actionText = data?.action?.actionText ?? 'Undefined';
    const tag = data?.tag ?? '';

    return (
        <View style={[styles.container, containerStyle ?? {}]}>
            <TouchableOpacity activeOpacity={1} onPress={onClick}>
                {tag
                    ? <TagComponent tags={[tag]}
                                    style={styles.tagContainerStyle}
                                    singleTagStyle={styles.singleTagStyle}
                                    textStyle={{...TextStyle.text_10_normal, color: colors.black}}/>
                    : null
                }
                <Text numberOfLines={1} style={[styles.title, {marginTop: tag ? 8 : 0}]}>{data?.title ?? ''}</Text>
                <Text style={styles.description}>{data.description ?? ''}</Text>
                {data.note ? <Text style={styles.note}>{data.note ?? ''}</Text> : null}
                <View style={[CommonStyle.separator, {marginTop: 12}]}/>
                <View style={styles.buttonContainer}>
                    {AccessoryView ? <AccessoryView/> : <View />}
                    <RightArrowButton image={Images.rightBlueArrow} text={actionText}
                                      imageStyle={styles.rightArrowStyle}
                                      textStyle={styles.buttonText} style={{marginBottom: 0, alignSelf: 'flex-end'}}
                                      onPress={onClick}
                    />
                </View>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        ...CommonStyle.cardContainerWithShadow,
        marginHorizontal: spacing.spacing16,
        borderRadius: 4,
        paddingVertical: spacing.spacing12,
    },
    tagContainerStyle: {
        marginTop: 0,
        top: 0,
        right: 0,
        marginHorizontal: spacing.spacing12,
    },
    singleTagStyle: {
        backgroundColor: colors.color_yellowTags,
        borderWidth: 0,
        paddingHorizontal: spacing.spacing8,
        paddingVertical: spacing.spacing2,
        borderColor: colors.transparent,
        borderRadius: 4,
        marginLeft: 0,
    },
    title: {
        ...TextStyle.text_14_bold,
        marginHorizontal: spacing.spacing12,
    },
    description: {
        ...TextStyle.text_12_normal,
        marginTop: spacing.spacing4,
        marginHorizontal: spacing.spacing12,
    },
    note: {
        ...TextStyle.text_12_normal,
        marginTop: spacing.spacing12,
        marginHorizontal: spacing.spacing12,
        color: colors.charcoalGrey
    },
    buttonContainer: {
        paddingTop: spacing.spacing14,
        marginHorizontal: spacing.spacing12,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    buttonText: {
        ...TextStyle.text_14_bold,
        color: colors.blue028,
    },
    rightArrowStyle: {
        marginRight: 0, marginLeft: 4,
        width: 8,
        height: 12,
        resizeMode: "stretch"
    },
});

export default DetailCard;
