import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import spacing from '../Constants/Spacing';
import {ButtonStyle, ButtonViewStyle, TextStyle} from '../Constants/CommonStyle';
import colors from '../Constants/colors';
import TagComponent from '../Catalyst/components/common/TagsComponent';
import ButtonWithLoader from '../CommonComponents/ButtonWithLoader';
import * as FirebaseKeys from '../Constants/FirebaseConstants';

const DetailMultiActionComponent = props => {

    const tag = props.tag;
    const title = props.title ?? '';
    const description = props.description ?? '';
    const notes = props.notes;
    const InfoView = props.infoView;
    const TermsAndConditionsView = props.termsAndConditionsView;
    const primaryButton = props.primaryButton;
    const actionFirst = props.actionFirst;
    const actionSecond = props.actionSecond;
    const isPrimaryBtnEnabled = props.isPrimaryBtnEnabled;
    const isLoading = props.isLoading ?? false

    return (
        <View style={props.style ?? styles.container}>

            {tag
                ? <TagComponent tags={[tag]}
                                style={styles.tagContainerStyle}
                                singleTagStyle={styles.singleTagStyle}
                                textStyle={{...TextStyle.text_10_normal, color: colors.black}}/>
                : null
            }

            <View style={[styles.textAndActionContainer, {marginTop: tag ? 8 : 0}]}>
                <View style={styles.textContainer}>
                    <Text numberOfLines={1} style={[styles.title]}>{title ?? ''}</Text>
                    <Text style={styles.description}>{description ?? ''}</Text>
                    {notes ? <Text style={styles.note}>{notes ?? ''}</Text> : null}
                </View>

                {actionFirst || actionSecond
                    ? <View style={styles.actionContainer}>
                        {actionFirst
                            ? <View style={styles.singleActionContainer}>
                                <TouchableOpacity
                                    onPress={actionFirst.action ?? (() => { })}>
                                    {actionFirst.image ?
                                        <Image source={actionFirst.image} style={styles.actionImage}/> : null}
                                    <Text
                                        style={[TextStyle.text_12_bold, {color: colors.color_008DF6}]}
                                    >
                                        {actionFirst.text}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            : null
                        }

                        {actionSecond ? <View style={styles.seperator}/> : null}

                        {actionSecond
                            ? <View style={styles.singleActionContainer}>
                                <TouchableOpacity onPress={actionSecond.action ?? (() => {})}>
                                    {actionSecond.image ?
                                        <Image source={actionSecond.image} style={styles.actionImage}/> : null}
                                    <Text
                                        style={[TextStyle.text_12_bold, {color: colors.color_008DF6}]}
                                    >
                                        {actionSecond.text}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            : null
                        }
                    </View>
                    : null
                }

            </View>

            {InfoView ? <InfoView/> : null}

            {TermsAndConditionsView ? <TermsAndConditionsView/> : null}

            {primaryButton
                ? <View style={styles.primaryButtonContainer}>
                    <ButtonWithLoader
                        isLoading={isLoading}
                        enable={isPrimaryBtnEnabled}
                        Button={{
                            text: primaryButton.text,
                            onClick: primaryButton.action,
                        }}
                    />
                </View>
                : null
            }

        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginTop: spacing.spacing32,
        paddingHorizontal: 16,
        marginBottom: 20,
    },
    tagContainerStyle: {
        marginTop: 0,
        marginHorizontal: spacing.spacing0,
    },
    singleTagStyle: {
        backgroundColor: colors.color_yellowTags,
        borderWidth: 0,
        paddingHorizontal: spacing.spacing8,
        paddingVertical: spacing.spacing2,
        borderColor: colors.transparent,
        borderRadius: 4,
        marginLeft: 0,
    },
    title: {
        ...TextStyle.text_14_bold,
    },
    description: {
        ...TextStyle.text_12_normal,
        marginTop: spacing.spacing4,
    },
    note: {
        ...TextStyle.text_12_normal,
        marginTop: spacing.spacing12,
        color: colors.charcoalGrey,
    },
    textAndActionContainer: {
        flexDirection: 'row',
    },
    actionContainer: {
        justifyContent: 'space-between',
    },
    singleActionContainer: {
        alignItems: 'center',
    },
    actionImage: {
        height: 18,
        width: 18,
        alignSelf: 'center',
        marginBottom: spacing.spacing8
    },
    seperator: {
        width: '100%',
        height: 1,
        marginVertical: spacing.spacing12,
        backgroundColor: colors.color_F6F6F6,
    },
    textContainer: {
        flex: 1,
        marginRight: spacing.spacing24,
    },
    primaryButtonContainer: {
        marginTop: spacing.spacing20
    }
});

export default DetailMultiActionComponent;
