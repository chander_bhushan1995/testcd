import React from "react";
import { View, Image, StyleSheet, Text, Alert } from "react-native";
import colors from "../Constants/colors";
import Dimens from "../Constants/Dimens";
import Spacing from "../Constants/Spacing";
import { TextStyle } from "../Constants/CommonStyle";
import spacing from "../Constants/Spacing";

export default function InfoCard(props) {
    let  logo = null
    if(props.data?.imageSource !== null) logo = <Image
        source={props.data?.imageSource?.source}
        style={[props.data?.imageSource?.dimensions, {marginRight: spacing.spacing8}]}
    />;
  return (
    <View style={[props.style?.containerStyle]}>
      <View style={styles.headerViewStyle}>
          {logo}
        <Text
          style={[
            TextStyle.text_16_regular,
            props.style?.titleStyle
          ]}
        >
          {props.data?.title}
        </Text>
      </View>
      <Text style={[TextStyle.text_12_normal, props.style?.subtitleStyle]}>
        {props.data?.subtitle}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  headerViewStyle: {
    flexDirection: "row",
    alignItems: "center",
    paddingBottom: spacing.spacing8
  }
});
