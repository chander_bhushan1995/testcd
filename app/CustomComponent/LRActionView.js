import { View, StyleSheet } from "react-native";
import React, { useState } from "react";
import LinkButton from "./LinkButton";
export default function LRActionView(props) {
  let {onLeftClick, onRightClick} = props;
  let actioViewStyle = LRButtonViewStyle;
  if(props.theme !== null && props.theme !== undefined && props.theme === colors.white){
    actioViewStyle = LRButtonViewStyleWhiteTheme
  }
  return (
    <View style={styles.lrContainerStyle}>
      <LinkButton
        data={{ action: props.data?.action1 }}
        style={actioViewStyle.leftButton}
        onPress={() => onLeftClick()}
      />
      <LinkButton
        data={{ action: props.data?.action2 }}
        style={actioViewStyle.rightButton}
        onPress={() => onRightClick()}
      />
    </View>
  );
}

import spacing from "../Constants/Spacing";
import colors from "../Constants/colors";
import { ButtonStyle } from "../Constants/CommonStyle";
export const styles = StyleSheet.create({
  lrContainerStyle: {
    flexDirection: "row",
    justifyContent: "space-between"
  }
});
export const LRButtonViewStyle = {
  leftButton: {
    actionTextStyle: {
      paddingHorizontal: spacing.spacing24,
      ...ButtonStyle.TextOnlyButton,
      fontFamily: "Lato-Regular",
      fontWeight:"normal",

      color: colors.white
    }
  },
  rightButton: {
    buttonStyle: {
      backgroundColor: colors.white
    },
    actionTextStyle: {
      paddingHorizontal: spacing.spacing36,
      ...ButtonStyle.TextOnlyButton,
      fontFamily: "Lato-Regular",
      color: colors.blue
    }
  }
};
export const LRButtonViewStyleWhiteTheme = {
  leftButton: {
    actionTextStyle: {
      paddingHorizontal: spacing.spacing24,
      ...ButtonStyle.TextOnlyButton,
      fontFamily: "Lato-Regular",
      fontWeight:"normal",

      color: colors.blue028
    }
  },
  rightButton: {
    buttonStyle: {
      backgroundColor: colors.blue028
    },
    actionTextStyle: {
      paddingHorizontal: spacing.spacing36,
      ...ButtonStyle.TextOnlyButton,
      fontFamily: "Lato-Regular",
      fontWeight:"normal",

      color: colors.white
    }
  }
};
