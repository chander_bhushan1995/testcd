import { Text, View, StyleSheet } from "react-native";
import { TextStyle } from "../Constants/CommonStyle";
import React, { useState } from "react";

export default function LRTextView(props) {
  return (
    <View style={[styles.lrContainerStyle, props.style?.headerStyle]}>
      <Text style={[styles.leftText, props.style?.leftText]}>
        {props.data?.title}
      </Text>
      <Text style={[styles.rightText, props.style?.rightText]}>
        {props.data?.subtitle}
      </Text>
    </View>
  );
}
export const styles = StyleSheet.create({
  lrContainerStyle: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  leftText: {
    ...TextStyle.text_12_normal
  },
  rightText: {
    ...TextStyle.text_12_normal
  }
});
