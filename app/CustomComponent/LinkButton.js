import React, { useState } from "react";
import { StyleSheet, View, Alert, TouchableOpacity, Text } from "react-native";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";

export default function LinkButton(props) {
  const strikeThroughText = () => {
      if(!props.data?.strikeThroughAmount){
        return null
      }
      return <Text
          style={[
            styles.strikeThroughStyle,
            props.style?.strikeThroughTextStyle
          ]}

      >
        {props.data?.strikeThroughAmount}
      </Text>
  };
  return (
    <TouchableOpacity
      style={[styles.buttonStyle, props.style?.buttonStyle]}
      onPress={() => props.onPress()}
      underlayColor={colors.white}
    >
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <Text style={[styles.actionTextStyle, props.style?.actionTextStyle]}>
          {props.data?.action}
        </Text>
        {strikeThroughText()}
      </View>
    </TouchableOpacity>
  );
}
const styles = StyleSheet.create({
  buttonStyle: {
    borderRadius: 2,
    borderColor: colors.white
  },
  actionTextStyle: {
    color: colors.white,
    textAlign: "center",
    paddingHorizontal: spacing.spacing8,
    paddingVertical: spacing.spacing8
  },
  strikeThroughStyle: {
    textDecorationLine: "line-through",
    textDecorationStyle: "solid",
    color: "rgba(0,127,243,0.5)"
  }
});
