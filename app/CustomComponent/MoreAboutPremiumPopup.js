import React, {useState} from "react";
import RBSheet from "../Components/RBSheet";
import {Dimensions, StyleSheet, View, Text} from "react-native";
import {TextStyle} from "../Constants/CommonStyle";
import RightTickWithLabelView from "../CustomComponent/righttickwithlabelview/RightTickWithLabelView.component";
import LinkButton from "./LinkButton";
import spacing from "../Constants/Spacing";
import colors from "../Constants/colors";
import {ButtonStyle} from "../Constants/CommonStyle";
import ButtonWithLoader from '../CommonComponents/ButtonWithLoader';
import paymentFlowStrings from '../PaymentFlow/Constants/Constants';
import * as Validator from '../commonUtil/Validator';
import {getIDFCommonEventAttr} from "../idfence/constants/IDFence.UtilityMethods";
import {WEBENGAGE_CREDIT_SCORE} from "../idfence/constants/CreditScoreWebEngage.events";
import {logWebEnageEvent} from "../commonUtil/WebengageTrackingUtils";
import {LOCATION, WEBENGAGE_IDFENCE_DASHBOARD} from "../Constants/WebengageAttrKeys";

export function MoreAboutPremiumPopup(props) {
    let height = props.height;
    let data = props.data;
    let membershipData = props.membershipData;
    let popupMaxHeight = Dimensions.get("window").height * 0.72; //TODO: move in constants
    if (data == null) return null;
    height = height > popupMaxHeight ? popupMaxHeight : height;
    sendEvent(membershipData);
    return (
        <RBSheet
            ref={ref => {
                this.RBSheet = ref;
            }}
            duration={10}
            height={height}
            closeOnSwipeDown={false}
        >
            <MoreAboutPremium data={data}
                              onPress={() => {
                                  this.RBSheet.close();
                                  props?.onPress()
                              }}
            />
        </RBSheet>
    );
}

function sendEvent(membershipData) {
    let webEngageEvents = getIDFCommonEventAttr(membershipData);
    webEngageEvents[LOCATION] = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_FENCE_DASHBOARD;
    logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.DASHBOARD_BOTTOM_SHEET_LOAD, webEngageEvents);
}
export const openMoreAboutPremiumPopup = id => {
    this.RBSheet?.open();
};

function MoreAboutPremium(props) {
    let {
        title,
        description,
        checkList,
        note,
        offerPrice,
        oldPrice,
        savedAmount,
        updatePaymentOrRenew
    } = props.data;

    return (
        <View style={styles.container}>
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.description}>{description}</Text>
            <CheckList checkList={checkList}/>
            <Text style={styles.note}>{note}</Text>
            <ActionButton props={props}/>
            <Text style={styles.saveText}>{savedAmount}</Text>
        </View>
    );
}

function ActionButton(props) {
    const [isLoading, setIsLoading] = useState(false);

    let actionProps = props.props.data;
    if (actionProps.updatePaymentOrRenew) {
        return <ButtonWithLoader
            isLoading={isLoading}
            enable={!isLoading}
            Button={{
                text: actionProps?.offerPrice,
                onClick: () => {
                    setIsLoading(true);
                    props.props?.onPress()
                }
            }}/>
    }
    return <LinkButton
        data={{
            action: actionProps?.offerPrice,
            strikeThroughAmount: actionProps?.oldPrice
        }}
        style={LinkButtonStyle}
        onPress={props.props?.onPress}
    />
}

function CheckList(props) {
    let CheckListView = props.checkList?.map(item => {
        return (
            <RightTickWithLabelView
                label={item}
                styles={{
                    MainContainer: {justifyContent: "flex-start"},
                    ItemText: {color: colors.gray}
                }}
            />
        );
    });
    return CheckListView;
}

const styles = StyleSheet.create({
    container: {margin: spacing.spacing16},
    title: {
        ...TextStyle.text_20_bold //FIXME: in design text_20_normal
    },
    description: {
        ...TextStyle.text_14_normal,
        color: colors.charcoalGrey,
        paddingVertical: spacing.spacing16
    },
    note: {
        ...TextStyle.text_12_normal,
        paddingVertical: spacing.spacing16,
        color: colors.charcoalGrey
    },
    saveText: {
        ...TextStyle.text_12_normal,
        color: colors.seaGreen,
        paddingVertical: spacing.spacing8,
        textAlign: 'center'
    }
});

const LinkButtonStyle = {
    buttonStyle: {
        backgroundColor: colors.color_0282F0
    },
    actionTextStyle: {
        paddingHorizontal: spacing.spacing36,
        ...ButtonStyle.TextOnlyButton,
        ...TextStyle.text_14_bold,
        color: colors.white
    },
    strikeThroughTextStyle: {
        color: colors.white
    }
};
