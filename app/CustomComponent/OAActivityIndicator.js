import React from "react";
import { SafeAreaView, View, StyleSheet } from "react-native";
import PlatformActivityIndicator from "./PlatformActivityIndicator";
import { CommonStyle } from "../Constants/CommonStyle";
import spacing from "../Constants/Spacing";
import colors from "../Constants/colors";

export default function OAActivityIndicator(props) {
  return (
    <SafeAreaView style={styles.SafeArea}>
      <View style={CommonStyle.loaderStyle}>
        <PlatformActivityIndicator />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  SafeArea: {
    flex: spacing.spacing1,
    backgroundColor: colors.white
  }
});
