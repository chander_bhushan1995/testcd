import {StyleSheet, View} from "react-native";
import React, {useState} from "react";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";
import OAActivityIndicator from "./OAActivityIndicator";

export default class OAActivityIndicatorShowHide extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            isVisible: false,
        }
    }

    show = () => {
        this.setState({isVisible: true})
    }

    hide = () => {
        this.setState({isVisible: false})
    }

    render() {
        return (<View>
            {this.state.isVisible ? (
                <OAActivityIndicator/>
            ) : null}
        </View>);
    }


}
const styles = StyleSheet.create({
    buttonStyle: {
        borderRadius: 2,
        borderColor: colors.white
    },
    actionTextStyle: {
        color: colors.white,
        textAlign: "center",
        paddingHorizontal: spacing.spacing8,
        paddingVertical: spacing.spacing8
    },
    strikeThroughStyle: {
        textDecorationLine: "line-through",
        textDecorationStyle: "solid",
        color: "rgba(0,127,243,0.5)"
    }
});
