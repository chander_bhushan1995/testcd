import React from "react";
import TextList from "../CustomComponent/TextList";
import RBSheet from "../Components/RBSheet";
import { Dimensions } from "react-native";
import {View} from "react-native";

export function RecommendedActionPopup(props) {
  let height = props.height;

  let popupMaxHeight = Dimensions.get("window").height * 0.72; //TODO: move in constants
  height = height > popupMaxHeight ? popupMaxHeight : height;
  return (
    <RBSheet
      ref={ref => {
        this.RBSheet = ref;
      }}
      duration={10}
      height={400}
      closeOnSwipeDown={false}
    >
      <View style={{height:400,width:'100%',flex:1,backgroundColor:'#ffffff'}}/>
    </RBSheet>
  );
}

export const openRecommendedActionPopup = id => {
  this.RBSheet.open();
};
