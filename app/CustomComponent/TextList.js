import React from "react";
import { View, Text, ScrollView, StyleSheet } from "react-native";
// import styles from "./styles";
import colors from "../Constants/colors";
import spacing from "../Constants/Spacing";
import { CommonStyle, TextStyle } from "../Constants/CommonStyle";
export default function TextList(props) {
  // let { descriptionText } = props?.style;
  // let  { textList } = props?.data;
  // let {action1, action2} = props.action

  let textList = textList ?? [
    "1. Monitor your Social Media accounts 24/7",
    "2. Get real time suggestions to minimize the breach effect",
    "3. Keep a track of your government IDs for any probable breach on Dark Web",
    "4. Monitor all your email accounts, phone numbers and even the transactional cards on Dark Web",
    "5. Lorem Ipsum should come here eventually"
  ];

  const getTextList = () => {
    let template = <View />;
    template = textList.map(item => {
      return <Text style={[styles.descriptionText]}>{item}</Text>;
    });
    return template;
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>RECOMMENDED ACTIONS</Text>
      <ScrollView>{getTextList()}</ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "space-around",
    backgroundColor: colors.white,
    padding: spacing.spacing16
  },
  title: {
    ...TextStyle.text_10_bold,
    color: colors.grey
  },
  listContainer: {
    backgroundColor: colors.white
  },
  descriptionText: {
    ...TextStyle.text_14_normal,
    color: colors.charcoalGrey,
    paddingVertical: spacing.spacing8
  }
});
