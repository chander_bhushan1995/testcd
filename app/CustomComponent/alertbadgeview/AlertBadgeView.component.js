import React from "react";
import {
    Text,
    View,
    ImageBackground,
    StyleSheet
} from "react-native";
import alertBadgeViewStyle from './AlertBadgeView.style';
export default function AlertBadgeView({count}) {
    return (
        <ImageBackground
            resizeMode='contain'
            source={require("../../images/drawable_count_circle.webp")}
            style={[alertBadgeViewStyle.ImageBackground, styles.positionStyle]}>
            <View style={alertBadgeViewStyle.TextContainerView}>
                <Text style={alertBadgeViewStyle.CountTextView}>{count}</Text>
            </View>
        </ImageBackground>
    );
}
const styles = StyleSheet.create({
    positionStyle:{
    marginBottom: -36, marginLeft: -32,
}
});
