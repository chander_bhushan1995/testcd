import {StyleSheet} from 'react-native';
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import fontSizes from "../../Constants/FontSizes";

export default StyleSheet.create({
    ImageBackground: {
        height: spacing.spacing32,
        width: null
    },
    TextContainerView: {
        position: 'absolute',
        top: spacing.spacing_neg_5,
        left: spacing.spacing0,
        right: spacing.spacing0,
        bottom: spacing.spacing0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    CountTextView: {
        color: colors.color_FFFFFF,
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_bold,
        fontSize: fontSizes.fontSize12
    }
});

