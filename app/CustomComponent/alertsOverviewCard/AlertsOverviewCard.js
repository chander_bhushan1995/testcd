import React from "react";
import { View, Text, TouchableOpacity, Image, Alert } from "react-native";
import styles from "./styles";
export default function AlertsOverviewCard(props) {
  let { titleText, descriptionText } = props.style;
  if(props.data === null) return null;
  let { title, alertList, imageSource } = props.data;
  // let {action1, action2} = props.action

  alertList = alertList ?? [];

  const getAlertList = () => {
    let template = <View />;
    template = alertList.map((item,index) => {
      return (
        <Text key={index.toString()} style={[styles.descriptionText, descriptionText]}>{item}</Text>
      );
    });
    return template;
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress = {props.onPress}
                        activeOpacity={1}>
      <View style={styles.titleContainer}>
        <Text style={[styles.titleText, titleText]}>{title}</Text>
          <Image source={imageSource} />
      </View>
      <View style={styles.alertContainer}>{getAlertList()}</View>
      </TouchableOpacity>
    </View>
  );
}
