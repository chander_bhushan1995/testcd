import { StyleSheet } from "react-native";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import { CommonStyle, TextStyle, margin } from "../../Constants/CommonStyle";

export default StyleSheet.create({
  container: {
    justifyContent: "space-around",
    backgroundColor: colors.white,
    padding: spacing.spacing16,
    borderRadius: spacing.spacing4,
    ...CommonStyle.cardContainerWithShadow
  },
  titleContainer: {
    flexDirection: "row",
    backgroundColor: colors.white,
    paddingVertical: spacing.spacing8,
    alignItems: "center",
    justifyContent: "space-between"
  },
  imgArrow: {
    width: spacing.spacing16,
    height: spacing.spacing16
  },
  alertContainer: {
    backgroundColor: colors.white
  },
  titleText: {
    ...TextStyle.text_16_bold
  },
  descriptionText: {
    ...TextStyle.text_14_normal
  }
});
