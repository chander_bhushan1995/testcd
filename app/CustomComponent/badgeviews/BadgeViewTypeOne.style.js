import {StyleSheet} from "react-native";
import dimens from "../../Constants/Dimens";
import {CommonStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import floats from "../../Constants/Floats";
import {OATextStyle} from "../../Constants/commonstyles";

export default StyleSheet.create({
    containerStyle: {
        flexDirection: 'row',
        /*shadowColor: colors.lightGrey,*/
        shadowOffset: {
            width: dimens.dimen0,
            height: dimens.dimen1
        },
        shadowRadius: dimens.dimen4,
        shadowOpacity: floats.float0_72,
        margin: spacing.spacing2,
        elevation: dimens.dimen2,
        borderRadius: dimens.dimen13,
        minWidth: dimens.dimen65,
        height: dimens.dimen24,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.color_45B448
    },
    labelStyle:{
        ...OATextStyle.TextFontSize_12_bold,
        color: colors.color_FFFFFF
    },
    imageStyle:{
        width: dimens.dimen14,
        marginLeft: spacing.spacing3
    }
});
