import React from "react";
import {View, Text, Image, TouchableOpacity} from "react-native";
import styles from "./BadgeViewTypeOne.style";
const BadgeViewTypeOne = ({item, onPress}) => (
    <TouchableOpacity activeOpacity={1} onPress={onPress} style={[styles.containerStyle, {backgroundColor: item?.data?.backgroundColor}]}>
        <Text
            style={styles.labelStyle}>{item?.label}</Text>
        <Image style={styles.imageStyle} resizeMode={'contain'} source={item?.data?.image}/>
    </TouchableOpacity>
);
export default BadgeViewTypeOne;
