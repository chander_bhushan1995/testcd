import React from "react";
import { View, Image, StyleSheet, Text, Alert } from "react-native";
import Dimens from "../../Constants/Dimens";
import { TextStyle, CommonStyle } from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import LinkButton from "../../CustomComponent/LinkButton";
import {getUniqueRowID} from "../../idfence/constants/IDFence.UtilityMethods";
export default function BankLevelSafety(props) {

    // let { data, style} = props;
    // let {logos, title} = data;

    const getBanksTemplate = () => {
        let banksTemplate = props?.data?.logos?.map((logo) => {
            return <Image
                key={getUniqueRowID()}
                source={logo.source}
                style={[styles.imgCross, logo.dimensions]}
            />
        });
        return banksTemplate;
    };
    return (
        <View style={styles.container}>
            <Text style={styles.titleStyle}>
                {props?.data?.title}
            </Text>

            <View style={styles.logoContainer}>

                {getBanksTemplate()}
            </View>
        </View>
    );
}

const handleClick = index => {
    alert("Button clicked!");
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: spacing.spacing32,
        alignItems: "center"
    },
    logoContainer: {
        flexDirection: "row",
        marginTop: spacing.spacing8
    },
    titleStyle:{
        ...TextStyle.text_12_normal
    },
    imgCross: {
       marginHorizontal: spacing.spacing8,


    },

});
