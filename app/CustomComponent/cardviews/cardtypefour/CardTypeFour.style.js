import {StyleSheet} from "react-native";
import spacing from "../../../Constants/Spacing";
import {CommonStyle} from "../../../Constants/CommonStyle";
import {OATextStyle} from "../../../Constants/commonstyles";
import colors from "../../../Constants/colors";
import dimens from "../../../Constants/Dimens";

export default StyleSheet.create({
    containerStyle: {
        justifyContent: 'center',
        paddingVertical: spacing.spacing16
    },
    cardViewStyle: {
        flex: spacing.spacing1,
        ...CommonStyle.cardContainerWithShadow,
        borderRadius: spacing.spacing4,
        marginTop: spacing.spacing16
    },
    cardViewItemContainerStyle: {
        paddingTop: spacing.spacing30,
        paddingBottom: spacing.spacing12
    },
    headerStyle: {
        ...OATextStyle.TextFontSize_14_bold,
        color: colors.color_212121
    },
    cellContainerStyle: {
        flex: spacing.spacing1,
        paddingHorizontal: spacing.spacing24,
        paddingTop: spacing.spacing16
    },
    noteTextStyle: {
        ...OATextStyle.TextFontSize_14_normal
    },
    separatorViewContainerStyle: {
        marginTop: spacing.spacing12
    },
    actionContainerStyle: {
        height: dimens.dimen44,
        paddingHorizontal: spacing.spacing16,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    actionTextStyle: {
        ...OATextStyle.TextFontSize_14_bold,
        color: colors.color_0282F0
    }

});
