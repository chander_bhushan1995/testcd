import React from "react";
import {Linking, Text, View} from "react-native";
import styles from "./CardTypeFour.style";
import colors from "../../../Constants/colors";
import {RowTypeFive} from "../../customcells"
import SeparatorView from "../../separatorview";
import LinkButton from "../../LinkButton";
import {DetailsTabStrings} from "../../../idfence/constants/Constants";

const CardTypeFour = ({item, headerPrefix, noteText, index}) => (
    prepareItemView(item, headerPrefix, noteText, index)
);

function prepareItemView(itemList, headerPrefix, noteText, index) {
    return (<View style={styles.containerStyle}>
        <Text style={styles.headerStyle}>{headerPrefix} {index + 1}</Text>
        <View style={styles.cardViewStyle}>
            <View>
                <View style={styles.cellContainerStyle}>
                    {prepareRowView(itemList)}
                    <Text style={styles.noteTextStyle}>
                        {noteText}
                    </Text>
                </View>
                <View style={{height:20}}>

                </View>
                {/*<View style={styles.separatorViewContainerStyle}>
                    <SeparatorView separatorStyle={{backgroundColor: colors.color_F1F3F4}}/>
                </View>
                <View style={styles.actionContainerStyle}>
                    <Text>Call Now :</Text>
                    <LinkButton
                        onPress={() => Linking.openURL(`tel:${DetailsTabStrings.supportNumber}`)}
                        style={{
                            actionTextStyle: styles.actionTextStyle
                        }}
                        data={{
                            action: "1800 124 0987"
                        }}/>
                </View>*/}

            </View>
        </View>
    </View>);
}

function prepareRowView(itemList) {
    return itemList.map((item, index) => {
        return (<RowTypeFive key={index} item={item}/>);
    });
}

export default CardTypeFour;
