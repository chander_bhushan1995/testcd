import {StyleSheet} from "react-native";
import spacing from "../../../Constants/Spacing";
import {CommonStyle} from "../../../Constants/CommonStyle";

export default StyleSheet.create({
    cardViewStyle: {
        flex: spacing.spacing1,
        ...CommonStyle.cardContainerWithShadow,
        borderRadius: spacing.spacing4
    },
    cardViewItemContainerStyle:{
        height:170,
        paddingTop:spacing.spacing5,
        paddingBottom:spacing.spacing16
    }
});
