import React from "react";
import {View, Text, TouchableOpacity} from "react-native";
import styles from "./CardTypeOne.style";
import SeparatorView from "../../separatorview";
import {OATextStyle} from "../../../Constants/commonstyles";
import colors from "../../../Constants/colors";
import BadgeViewTypeOne from "../../badgeviews";
import spacing from "../../../Constants/Spacing";
import dimens from "../../../Constants/Dimens";

const tempData = [
    ["AA", "BB", "CC"],
    ["qwqw", "dwd", "sqq"]
]
const CardTypeOne = ({item, onPress, badgeData, onBadgeViewPress}) => (
    <TouchableOpacity activeOpacity={1} onPress={onPress}>
        <View style={styles.cardViewStyle}>
            <View style={styles.cardViewItemContainerStyle}>
                <View style={{
                    height: dimens.dimen50,
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingLeft: spacing.spacing16,
                    paddingRight: spacing.spacing16
                }}>
                    <Text style={[OATextStyle.TextFontSize_14_bold, {color: colors.color_212121, alignSelf: 'center'}]}>Your
                        Score</Text>
                    {showBadgeView(badgeData, onBadgeViewPress)}
                </View>
                <SeparatorView separatorStyle={{backgroundColor: colors.color_E2E2E2}}/>
                <View style={{flex: 1, paddingLeft: 8, paddingRight: 8, paddingTop: 20}}>
                    {addTableViewData(item)}
                </View>
            </View>
        </View>
    </TouchableOpacity>

);

function addTableViewData(item) {
    return item.map((item, index) => {
        return (<View style={{flex: 1}} key={index}>
            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                {tableColumnData(item, index)}
            </View>
        </View>);
    });

}

function tableColumnData(itemArr, index) {
    let itemStyle = (index === 0 ? OATextStyle.TextFontSize_20_bold_212121 : [OATextStyle.TextFontSize_12_normal, {
        lineHeight: 15,
        color: colors.color_808080
    }]);
    return itemArr.map((item, index) => {
        return (
            <View key={index}
                  style={{flex: 1, paddingLeft: 8, paddingRight: 8, alignSelf: 'stretch', alignItems: 'flex-start'}}>
                <Text style={itemStyle}>{item}</Text>
            </View>);
    });

}


function showBadgeView(badgeData, onBadgeViewPress) {
    if (!badgeData?.isShowBadge) {
        return null;
    }
    return <BadgeViewTypeOne item={badgeData} onPress={onBadgeViewPress}/>
}

export default CardTypeOne;
