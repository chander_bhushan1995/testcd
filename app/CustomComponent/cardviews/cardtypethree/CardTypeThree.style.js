import {StyleSheet} from "react-native";
import spacing from "../../../Constants/Spacing";
import {CommonStyle} from "../../../Constants/CommonStyle";

export default StyleSheet.create({
    cardViewStyle: {
        flex: spacing.spacing1,
        ...CommonStyle.cardContainerWithShadow,
        borderRadius: spacing.spacing4
    },
    flexOneStyle:{
        flex:spacing.spacing1,
    },
    cardViewItemContainerStyle: {
        minHeight: 105,
        justifyContent:'center',
        paddingTop: spacing.spacing5,
        paddingBottom: spacing.spacing8
    },
    tableContainerStyle:{
        flexDirection: 'row',
        paddingHorizontal: spacing.spacing8,
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'center'
    },
    tableColumnStyle:{
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: spacing.spacing8,
        paddingRight: spacing.spacing8,

    }
});
