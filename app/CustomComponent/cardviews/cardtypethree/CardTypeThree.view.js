import React from "react";
import {Text, View} from "react-native";
import styles from "./CardTypeThree.style";
import {OATextStyle} from "../../../Constants/commonstyles";
import {getRandomColor} from "../../../commonUtil/Formatter";

const CardTypeThree = ({item}) => (
    <View style={styles.cardViewStyle}>
        <View style={styles.cardViewItemContainerStyle}>
            <View style={styles.flexOneStyle}>
                <View style={[styles.flexOneStyle, styles.tableContainerStyle]}>
                    {tableViewData(item)}
                </View>
            </View>
        </View>
    </View>
);


function tableViewData(item) {
    return item.map((item, index) => {
        let viewFlex = 1;
        switch (index) {
            case 0:
            case 1:
            case 3:
                viewFlex = 0.7;
                break;
            case 2:
                //viewFlex = 1.2;
                break;
        }

        return (
            <View key={index} style={{flex: viewFlex, alignItems: 'center', justifyContent: 'center'}}>
                {tableColumnData(item, index)}
            </View>)
    });

}

function tableColumnData(item) {
    return (
        <View style={[ styles.tableColumnStyle]}>
            <View>
                <Text style={OATextStyle.TextFontSize_20_bold_212121}>{item.title}</Text>
                <Text style={OATextStyle.TextFontSize_12_normal}>{item.label}</Text>
            </View>

        </View>);


}

export default CardTypeThree;
