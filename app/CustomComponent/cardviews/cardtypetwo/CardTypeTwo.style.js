import {StyleSheet} from "react-native";
import spacing from "../../../Constants/Spacing";
import {CommonStyle} from "../../../Constants/CommonStyle";
import {OATextStyle} from "../../../Constants/commonstyles";
import colors from "../../../Constants/colors";
import dimens from "../../../Constants/Dimens";

export default StyleSheet.create({
    cardViewStyle: {
        flex: spacing.spacing1,
        ...CommonStyle.cardContainerWithShadow,
        borderRadius: spacing.spacing4
    },
    flexOneStyle: {
        flex: spacing.spacing1
    },
    cardViewItemContainerStyle: {
        height: dimens.dimen170,
        paddingTop: spacing.spacing5,
        paddingBottom: spacing.spacing16
    },
    cardHeaderViewStyle: {
        paddingTop: spacing.spacing10,
        paddingBottom: spacing.spacing14,
        justifyContent: 'flex-start',
        paddingHorizontal: spacing.spacing16
    },
    titleStyle: {
        ...OATextStyle.TextFontSize_14_bold,
        color: colors.color_212121
    },
    subTitleStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        color: colors.color_808080,
        marginTop: spacing.spacing9
    },
    tableViewStyle: {
        flex: spacing.spacing1,
        paddingLeft: spacing.spacing8,
        paddingRight: spacing.spacing8,
        paddingTop: spacing.spacing20
    },
    tableColumnStyle: {
        flex: spacing.spacing1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    itemViewStyle: {
        flex: spacing.spacing1,
        paddingLeft: spacing.spacing8,
        paddingRight: spacing.spacing8,
        alignSelf: 'stretch',
        alignItems: 'flex-start'
    }


});
