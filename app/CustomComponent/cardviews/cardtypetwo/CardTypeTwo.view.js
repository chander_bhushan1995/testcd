import React from "react";
import {Text, TouchableOpacity, View} from "react-native";
import styles from "./CardTypeTwo.style";
import {OATextStyle} from "../../../Constants/commonstyles";
import colors from "../../../Constants/colors";
import SeparatorView from "../../separatorview";

const CardTypeTwo = ({item, title, subTitle}) => (
    <TouchableOpacity activeOpacity={1}>
        <View style={styles.cardViewStyle}>
            <View style={styles.cardViewItemContainerStyle}>
                <View style={styles.cardHeaderViewStyle}>
                    <Text style={styles.titleStyle}>
                        {title}
                    </Text>
                    <Text
                        style={styles.subTitleStyle}>
                        {subTitle}
                    </Text>
                </View>
                <SeparatorView separatorStyle={{backgroundColor: colors.color_E2E2E2}}/>
                <View style={styles.tableViewStyle}>
                    {tableViewData(item)}
                </View>
            </View>
        </View>
    </TouchableOpacity>

);

function tableViewData(item) {
    return item.map((item, index) => {
        return (<View key={index} style={styles.flexOneStyle}>
            <View style={styles.tableColumnStyle}>
                {tableColumnData(item, index)}
            </View>
        </View>);
    });

}

function tableColumnData(itemArr, index) {
    let itemStyle = (index === 0 ? OATextStyle.TextFontSize_20_bold_212121 : [OATextStyle.TextFontSize_12_normal, {
        lineHeight: 15,
        color: colors.color_808080
    }]);
    return itemArr.map(item => {
        return (
            <View key={index} style={styles.itemViewStyle}>
                <Text style={itemStyle}>{item}</Text>
            </View>);
    });

}

export default CardTypeTwo;
