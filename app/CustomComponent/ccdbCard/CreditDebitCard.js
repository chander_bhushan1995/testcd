import React, {useState} from "react";
import {View, Switch, Alert, Image, Text, StyleSheet, TouchableOpacity} from 'react-native';
import SeparatorView from "../../CustomComponent/separatorview";
// import styles from "./styles"
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import dimens from "../../Constants/Dimens";
import {getRandomColor} from "../../commonUtil/Formatter";


export default function CreditDebitCard(props) {
    let {onPress} = props
    let {title, logoImageSource, imageSource, linkUrl, linked} = props.item;
    const [switchOnValueHolder, setSwitchOnValueHolder] = useState(0);

    return (
        <View style={styles.mainContainer}>
            <SeparatorView separatorStyle={styles.SeparatorViewStyle}/>
            <View style={styles.rowContainer}>
                <View style={styles.leftContainer}>
                    <View style={[styles.imageContainer, {display: (logoImageSource ? 'flex' : 'none')}]}>
                        <Image source={logoImageSource?.source} style={logoImageSource?.dimensions}/>
                    </View>
                    <Text
                        style={[styles.titleStyle, {paddingLeft: (logoImageSource ? spacing.spacing16 : spacing.spacing0)}]}> {title} </Text>
                </View>
                <TouchableOpacity onPress={onPress}>
                    <View style={styles.imageContainer}>
                        <Image source={imageSource?.source} style={imageSource?.dimensions}/>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    mainContainer: {
        paddingHorizontal: spacing.spacing16,
        backgroundColor: colors.color_FAFAFA
    },
    rowContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginVertical: spacing.spacing12
    },
    leftContainer: {flexDirection: "row", alignItems: "center"},
    logoStyle: {
        width: 12,
        height: 15
    },
    titleStyle: {paddingLeft: spacing.spacing16},
    SeparatorViewStyle: {
        backgroundColor: colors.color_F6F6F6
    }
});
