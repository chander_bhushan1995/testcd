import {Image, StyleSheet, Text, View, TouchableOpacity} from "react-native";
import React from "react";

import colors from "../../Constants/colors";
import {TextStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import SeparatorView from "../separatorview";
import {OATextStyle} from "../../Constants/commonstyles";

export default function MultiColumnCell({onRightClick, ...props}) {
    let twoDArr = props.data
    if (!twoDArr) return null;
    let list = twoDArr.map(arr => {
        let row = arr.map((item, index) => {
            if (index == 0) {
                return (
                    <Text style={[styles.text0Style, props.styles?.TextStyle0]}>
                        {item}
                    </Text>
                );
            }
            if (index == 1) {
                return (
                    <Text style={[styles.text1Style, props.styles?.TextStyle1]}>
                        {item}
                    </Text>
                );
            }
            if (index == 2 && item !== undefined) {
                if (item["label"] !== undefined) {
                    return (
                        <View style={[styles.actionButtonContainerStyle, props.styles?.TextStyle3]}>
                            <TouchableOpacity onPress={e => {
                                e.preventDefault();
                                {
                                    onRightClick(item)
                                }
                                ;
                            }}>
                                <View style={{flexDirection: 'row'}}>
                                    <Image source={item["url"]} resizeMode="contain"  style={styles.ImageRight}/>
                                    <Text style={styles.txtPDFStyle}>{item["label"]}</Text>
                                </View>
                            </TouchableOpacity>

                        </View>

                    );
                } else {
                    return (
                        <View style={{flex: 1, flexDirection: 'row', justifyContent: "flex-end", marginRight: 8}}>
                            <Image resizeMode="contain" source={item["url"]} style={styles.ImageRight}/>
                        </View>
                    );
                }
            }

            return (
                <Text style={[styles.TextStyle, props.styles?.otherTextStyle]}>
                    {item}
                </Text>
            );
        });
        return <View style={styles.rowContainerHeight}>
            <View style={styles.rowStyle}>{row}</View>
            <SeparatorView separatorStyle={styles.SeparatorViewStyle}/>
        </View>;
    });
    return <View style={styles.columnStyle}>{list}</View>;
}

export const styles = StyleSheet.create({
    separator: {
        height: spacing.spacing1,
        backgroundColor: colors.grey
    },
    rowContainerHeight: {
        flex: 1,
        height: 56,
    },
    rowStyle: {
        height: 55,
        paddingLeft: spacing.spacing8,
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        backgroundColor: '#FAFAFA',
    },
    columnStyle: {
        backgroundColor: '#FAFAFA'
    },
    SeparatorViewStyle: {backgroundColor: colors.color_F6F6F6},
    text0Style: {
        ...TextStyle.text_14_normal,
        paddingHorizontal: spacing.spacing8
    },
    text1Style: {
        ...TextStyle.text_14_bold,
        paddingHorizontal: spacing.spacing8,
        color: colors.charcoalGrey
    },
    actionButtonContainerStyle: {
        marginRight: 10
    },
    ImageRight: {
        marginRight: spacing.spacing16,
        height: 16
    },
    txtPDFStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        marginLeft: -8,
        color: colors.color_0282F0
    },

    otherTextStyle: {
        ...TextStyle.text_14_normal,
        paddingHorizontal: spacing.spacing8,
        color: colors.charcoalGrey
    }
});
