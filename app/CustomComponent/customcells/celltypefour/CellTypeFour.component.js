import React from "react";
import {View, Text} from "react-native";
import styles from "./CellTypeFour.style";
import {OATextStyle} from "../../../Constants/commonstyles";
import colors from "../../../Constants/colors";
import SeparatorView from "../../separatorview";
import spacing from "../../../Constants/Spacing";

const CellTypeFour = ({item, customStyle}) => (
    <View style={[styles.containerStyle]}>
        <View style={{flex: 1, flexDirection: 'row', paddingVertical: spacing.spacing16, paddingHorizontal: 16}}>
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
                <Text style={[OATextStyle.TextFontSize_14_normal, {color: colors.color_212121}]}>{item?.title}</Text>
                <Text style={[OATextStyle.TextFontSize_12_normal, {
                    color: colors.color_808080,
                    marginTop: 5
                }]}>{item?.subTitle}</Text>
            </View>
            <View style={{justifyContent: 'center', alignItems: 'flex-end'}}>
                <Text
                    style={[OATextStyle.TextFontSize_14_normal, {color: colors.color_808080}]}>{item?.rightTitle}</Text>
            </View>
        </View>
        <SeparatorView separatorStyle={[{backgroundColor: colors.color_F6F6F6}]}/>
    </View>
);
export default CellTypeFour;
