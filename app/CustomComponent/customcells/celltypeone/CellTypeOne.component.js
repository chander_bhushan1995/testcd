import React from "react";
import {View, Text, Image, TouchableOpacity} from "react-native";
import styles from "./CellTypeOne.style";
import {OATextStyle} from "../../../Constants/commonstyles";
import {OAImageSource} from "../../../Constants/OAImageSource";
import SeparatorView from "../../separatorview";
import spacing from "../../../Constants/Spacing";

const CellTypeOne = ({item, onPress, customStyle}) => (
    <TouchableOpacity onPress={e => {
        if (e !== undefined) {
            e.preventDefault();
        }
        onPress(e, item)
    }}>
        <View style={[styles.containerStyle, customStyle?.containerStyle]}>
            <View style={styles.flexOneStyle}>
                <Text
                    style={[styles.titleStyle, customStyle?.titleStyle]}>{item?.title}</Text>
                <Text style={[OATextStyle.TextFontSize_12_normal, customStyle?.subTitleStyle]}>{item?.subTitle}</Text>
            </View>
            <View style={{
                flexDirection: 'row',
                alignSelf: 'center',
                alignItems: 'center'
            }}>
                <Text style={[styles.rightContainerStyle, customStyle?.rightLabelStyle]}>
                    {item?.rightLabel}
                </Text>
                <Text style={[styles.rightContainerStyle, customStyle?.rightLabel2Style]}>
                    {item?.rightLabel2}
                </Text>
                <Image style={styles.imageStyle} resizeMode={'contain'}
                       source={(item?.imgRight !== undefined ? item?.imgRight : OAImageSource.chevron_down.source)}/>
            </View>

        </View>
        {isHideLineSeparator(item?.isHideSeparator, customStyle?.separatorStyle)}
    </TouchableOpacity>
);

function isHideLineSeparator(isHideSeparator, separatorStyle) {
    if (isHideSeparator) {
        return null;
    }
    return <SeparatorView separatorStyle={[styles.separatorStyle, separatorStyle]}/>;
}

export default CellTypeOne;
