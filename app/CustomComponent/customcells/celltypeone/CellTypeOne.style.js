import {StyleSheet} from "react-native";
import colors from "../../../Constants/colors";
import spacing from "../../../Constants/Spacing";
import {OATextStyle} from "../../../Constants/commonstyles";

export default StyleSheet.create({
    containerStyle: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        backgroundColor: colors.color_FFFFFF,
        paddingVertical: spacing.spacing16
    },
    flexOneStyle: {
        flex: spacing.spacing1
    },
    titleStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        color: colors.color_212121
    },
    rightContainerStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        color: colors.color_D0021B,
        /* marginRight: spacing.spacing12*/
    },
    imageStyle: {
        transform: [{rotate: '-90deg'}],
        marginLeft: spacing.spacing12,
        width: spacing.spacing12,
    },
    separatorStyle: {
        backgroundColor: colors.color_F6F6F6
    }
});
