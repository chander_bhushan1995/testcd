import React from "react";
import {View, Text} from "react-native";
import styles from "./CellTypeThree.style";
import colors from "../../../Constants/colors";
import SeparatorView from "../../separatorview";

const CellTypeThree = ({item, customStyle, rowHeight}) => (
    <View style={[styles.containerStyle, {height: rowHeight}]}>
        <View style={[styles.flexOneStyle, styles.viewContainerStyle]}>
            <View style={[styles.flexOneStyle, styles.topLeftContainerStyle]}>
                <Text style={styles.titleStyle}>{item?.title}</Text>
                <Text style={styles.subTitleStyle}>{item?.subTitle}</Text>
            </View>
            <View style={[styles.flexOneStyle, styles.centerContainerStyle]}>
                <Text
                    style={styles.centerTitleStyle}>{item?.centerTitle}</Text>
            </View>
            <View style={[styles.flexOneStyle, styles.rightContainerStyle]}>
                <Text
                    style={styles.rightTitleStyle}>{item?.rightTitle}</Text>
            </View>
        </View>
        <SeparatorView separatorStyle={[{backgroundColor: colors.color_F6F6F6}]}/>
    </View>
);
export default CellTypeThree;
