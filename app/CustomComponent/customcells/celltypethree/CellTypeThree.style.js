import {StyleSheet} from "react-native";
import colors from "../../../Constants/colors";
import spacing from "../../../Constants/Spacing";
import {OATextStyle} from "../../../Constants/commonstyles";

export default StyleSheet.create({
    containerStyle: {
        alignItems: 'flex-start',
        backgroundColor: colors.color_FFFFFF,
    },
    flexOneStyle: {
        flex: spacing.spacing1
    },
    viewContainerStyle: {
        flexDirection: 'row',
        paddingVertical: spacing.spacing16,
        paddingHorizontal: spacing.spacing16
    },
    topLeftContainerStyle: {
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    centerContainerStyle: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    rightContainerStyle: {
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    titleStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        color: colors.color_212121
    },
    centerTitleStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        color: colors.color_808080
    },
    subTitleStyle: {
        ...OATextStyle.TextFontSize_12_normal,
        color: colors.color_808080
    },
    rightTitleStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        color: colors.color_808080
    }


});
