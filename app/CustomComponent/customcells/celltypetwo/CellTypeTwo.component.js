import styles from "./CellTypeTwo.style";
import {Platform, View, Text, ProgressBarAndroid, ProgressViewIOS} from "react-native";
import React from "react";
import {OATextStyle} from "../../../Constants/commonstyles";
import colors from "../../../Constants/colors";
import ProgressBar from "../../../CommonComponents/ProgressBar";
import {getRandomColor} from "../../../commonUtil/Formatter";

const CellTypeTwo = ({item, onPress, customStyle}) => (
    <View style={[styles.containerStyle, customStyle?.containerStyle]}>
        <View style={{flexDirection: 'row'}}>
            <View style={styles.topItemContainerStyle}>
                <Text style={styles.titleStyle}>{item?.title}</Text>
                <Text
                    style={styles.subTitleStyle}> ({item?.subTitle})</Text>
            </View>
            <Text
                style={styles.topRightTitleStyle}>{item?.topRTitle}</Text>
        </View>
        <View style={styles.progressBarContainerStyle}>
            {getHorizontalProgressBar(item.progressValue)}
        </View>

        <View style={styles.bottomItemContainerStyle}>
            <View style={styles.bottomLeftContainerStyle}>
                <Text style={styles.bottomLeftTitleStyle}>
                    {item.bottomLTitle}
                </Text>
            </View>
            <Text
                style={styles.bottomRightTitleStyle}>{item.bottomRTitle}</Text>
        </View>
    </View>
);

function getHorizontalProgressBar(progressValue) {
    return (<View style={{backgroundColor: '#D8D8D8', flex: 1, borderRadius: 12}}>
        <ProgressBar data={{completed: progressValue}}
                     style={{
                         progressBarStyle: {backgroundColor: colors.blue028},
                         AnimatedViewStyle: {height: 8, borderRadius: 12},
                         backgroundColor: '#D8D8D8'
                     }}

        />
    </View>);/*(Platform.OS === 'android')
        ?
        (<ProgressBarAndroid style={styles.hProgressBarStyle} styleAttr="Horizontal" progress={progressValue}
                             indeterminate={false}/>)
        :
        (<ProgressViewIOS style={styles.hProgressBarStyle} progress={0.7}/>)*/
}

export default CellTypeTwo;
