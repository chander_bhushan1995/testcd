import {StyleSheet} from "react-native";
import colors from "../../../Constants/colors";
import spacing from "../../../Constants/Spacing";
import {OATextStyle} from "../../../Constants/commonstyles";

export default StyleSheet.create({
    containerStyle: {
        backgroundColor: colors.color_FFFFFF,
        paddingVertical: spacing.spacing16,
        paddingHorizontal: spacing.spacing16
    },
    hProgressBarStyle: {
        transform: [{scaleX: 1.0}, {scaleY: 2}],
        height: 8,
        borderRadius:12,
        borderWidth: 2,
    },
    topItemContainerStyle:{
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    titleStyle:{
        ...OATextStyle.TextFontSize_14_bold,
        color: colors.color_212121
    },
    subTitleStyle:{
        ...OATextStyle.TextFontSize_12_normal,
        color: colors.color_808080
    },
    topRightTitleStyle:{
        ...OATextStyle.TextFontSize_14_bold,
        color: colors.color_212121
    },
    progressBarContainerStyle:{
        paddingVertical: spacing.spacing4
    },
    bottomItemContainerStyle:{
        flexDirection: 'row',
        paddingVertical: spacing.spacing4
    },
    bottomLeftContainerStyle:{
        flexDirection: 'row',
        flex: spacing.spacing1
    },
    bottomLeftTitleStyle:{
        ...OATextStyle.TextFontSize_12_normal,
        color: colors.color_808080
    },
    bottomRightTitleStyle:{
        ...OATextStyle.TextFontSize_12_normal,
        color: colors.color_808080
    }

});
