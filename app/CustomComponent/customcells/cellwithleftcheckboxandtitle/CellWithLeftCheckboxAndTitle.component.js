import React, {useState} from "react";
import {
    Text,
    View,
    TouchableOpacity,
    Image,
} from 'react-native';
import CheckBox from 'react-native-check-box';
import styles from "./CellWithLeftCheckboxAndTitle.style"
import SeparatorView from "../../separatorview";
import spacing from "../../../Constants/Spacing";
import {OAImageSource} from '../../../Constants/OAImageSource';

const TAG = "CellWithLeftCheckboxAndTitle";
export default function CellWithLeftCheckboxAndTitle({
                                                         onItemCheckListener,
                                                         item, // Item
                                                         index,  // row index
                                                         checkItemList, // data holder for checked cell data
                                                         ...props // other data
                                                     }
) {

    const [checked, setIsChecked] = useState((checkItemList.some(filterItem => filterItem.item.alertType === item.alertType)));
    return (
        <TouchableOpacity style={styles.mainContainer} onPress={onCellPress}>
            <SeparatorView separatorStyle={styles.separatorViewStyle}/>
            <View style={styles.rowContainer}>
                {/*<CheckBox*/}
                {/*    center*/}
                {/*    title={item.name}*/}
                {/*    style={styles.checkBoxLeft}*/}
                {/*    value={checked}*/}
                {/*    onValueChange={onCellPress}*/}
                {/*/>*/}
                <CheckBox
                    style={styles.checkboxStyle}
                    checkedImage={<Image source= {OAImageSource.checkBox.source}
                                         style={OAImageSource.checkBox.dimensions} />}
                    unCheckedImage={<Image source={OAImageSource.checkBoxBlank.source}
                                           style={OAImageSource.checkBoxBlank.dimensions} />}
                    isChecked={checked}
                    onClick={onCellPress}
                />
                <Text style={[styles.titleStyle, props.titleStyle]}
                      numberOfLines={spacing.spacing1}
                      ellipsizeMode='tail'>{item.title}
                </Text>
            </View>
        </TouchableOpacity>
    );

    /**
     * Call on Row and CheckBox Press
     */
    function onCellPress() {
        setIsChecked(!checked);
        let prevPos = checkItemList.map(function (e) { // find if already present in checked list data store
            return e.index;
        }).indexOf(index);
        if (!checked && prevPos === -1) {
            // key ===== item  for checked item data
            // key ===== index for checked index
            const itemData = {'item': item, 'index': index};
            checkItemList.push(itemData); // add new selected item in checked list data store
        } else if (prevPos !== -1) {
            checkItemList.splice(prevPos, 1); // remove new selected item in checked list data store
        }
        {
            onItemCheckListener()
        }
        ;
    }
}
