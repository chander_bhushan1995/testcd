import {StyleSheet} from "react-native";
import spacing from "../../../Constants/Spacing";
import colors from "../../../Constants/colors";
import dimens from "../../../Constants/Dimens";
import {OATextStyle} from "../../../Constants/commonstyles";

export default StyleSheet.create({

    mainContainer: {
        height: 56,
        flexDirection: "column"
    },
    rowContainer: {
        flexDirection: "row",
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingRight: spacing.spacing16,
        paddingLeft: spacing.spacing16,
    },
    checkBoxLeft: {
        marginLeft: spacing.spacing12,
        width: dimens.dimen24,
        height: dimens.dimen24
    },
    titleStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        color: colors.color_212121,
        marginLeft: spacing.spacing16
    },
    separatorViewStyle: {backgroundColor: colors.color_F6F6F6}
});
