import React, {useState} from "react";

import {
    Text,
    View,
    Image,
    TouchableOpacity
} from "react-native";

import styles from "./CellWithLeftTitleAndRightImage.style"
import {OATextStyle} from "../../../Constants/commonstyles";
import SeparatorView from "../../separatorview";
import colors from "../../../Constants/colors";

export default function CellWithLeftTitleAndRightImage({item, index, onPress, ...props}) {
    const [readColor, setReadColor] = useState((props.cellTitleOnPressColor===null?colors.color_000000:props.cellTitleOnPressColor));
    return (
        <TouchableOpacity style={styles.MainContainer} onPress={(e) => onRowClick(e, item, index)}>
            <SeparatorView separatorStyle={styles.SeparatorViewStyle}/>
            <View style={styles.ColumnContainer}>
                <Image resizeMode={"contain"}  source={item.leftImageSource} style={[styles.ImageLeft]}/>
                <View style={styles.RowContainer}>
                    <Text style={[OATextStyle.TextFontSize_14_bold, {color: readColor}]}
                          numberOfLines={1}
                          ellipsizeMode='tail'>
                        {item.title}
                    </Text>
                    <Text style={styles.SubTitleText}
                          numberOfLines={1}
                          ellipsizeMode='tail'>
                        {item.subTitle}
                    </Text>
                </View>
                <View style={styles.RightImageContainer}>
                    <Image source={item.imageSource} style={styles.ImageRight}/>
                </View>

            </View>
        </TouchableOpacity>
    );

    function onRowClick(e, item, index) {
        e.preventDefault();
        setReadColor(colors.color_000000);
        onPress(e, item, index);
    }
}
