import React from "react";
import {
    Text,
    View,
    Image,
    TouchableOpacity,
    StyleSheet
} from "react-native";
import styles from "./CellWithLeftTitleAndRightImage.style"
import {OATextStyle} from "../../../Constants/commonstyles";
import SeparatorView from "../../separatorview";

export default function CellWithLeftTitleAndRightImageV2({item, index, onPress, isBottomSeperator,bottomSeperatorStyle, ...props}) {

    return (
        <TouchableOpacity style={styles.MainContainer} onPress={()=>{onPress()}}>
           {!isBottomSeperator ? <SeparatorView separatorStyle={styles.SeparatorViewStyle}/> : null}
            <View style={styles.ColumnContainer}>
                <Image source={item.imageSource} style={[styles.ImageLeft,item.leftImageStyle]}/>
                <View style={[styles.RowContainer,{flex:1}]}>
                    <Text style={[OATextStyle.TextFontSize_14_bold, props.titleStyle]}
                          numberOfLines={1}
                          ellipsizeMode='tail'>
                         {item.title}
                    </Text>
                    <Text style={[OATextStyle.TextFontSize_12_normal, styles.SubTitleText, props.subTitleStyle]}
                          numberOfLines={1}
                          ellipsizeMode='tail'>
                        {item.subTitle}
                    </Text>
                </View>
                <View style={style.RightImageContainer}>
                    {item.rightImage ? <Image source={item.rightImage} style={[styles.ImageRight,item.rightImageStyle]}/> : <Image source={item.imageSource} style={styles.ImageRight}/>}
                </View>
            </View>
            {isBottomSeperator ? <SeparatorView separatorStyle={[styles.SeparatorViewStyle,bottomSeperatorStyle]}/> : null}
        </TouchableOpacity>
    );
}


const style = StyleSheet.create({
    RightImageContainer: {
        flexDirection: 'row',
        // flex: 1,
        justifyContent: 'flex-end',
    }
})
