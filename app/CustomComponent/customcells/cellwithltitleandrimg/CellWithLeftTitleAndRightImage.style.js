import {StyleSheet} from "react-native";
import spacing from "../../../Constants/Spacing";
import colors from "../../../Constants/colors";
import dimens from "../../../Constants/Dimens";
import {OATextStyle} from "../../../Constants/commonstyles";

export default StyleSheet.create({

    MainContainer: {
        height: 72,
        flexDirection: "column"
    },
    ColumnContainer: {
        flexDirection: "row"
    },
    RowContainer: {
        paddingRight: spacing.spacing16,
        paddingLeft: spacing.spacing16,
        paddingTop: spacing.spacing12,
        flexDirection: "column"
    },
    SubTitleText: {
        ...OATextStyle.TextFontSize_12_normal,
        marginTop: spacing.spacing6,
    },
    ImageLeft: {
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing16,
        height: dimens.dimen24,
        maxWidth:30
    },
    ImageRight: {
        alignSelf: 'center',
        marginTop: spacing.spacing12,
        marginRight: spacing.spacing16,
        justifyContent: 'flex-end',
        width:12,
        height:7
    },
    RightImageContainer: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'flex-end'
    },
    SeparatorViewStyle: {backgroundColor: colors.color_F6F6F6}
});
