import React from "react";
import {
    Text,
    View,
    Image,
    TouchableOpacity
} from "react-native";
import styles from "./CellWithTitleAndRightAction.style"
import {OATextStyle} from "../../../Constants/commonstyles";
import SeparatorView from "../../separatorview";
import colors from "../../../Constants/colors";

export default function CellWithTitleAndRightAction({item, onRightActionClick, index, ...props}) {
    return (
        <TouchableOpacity >
            <SeparatorView separatorStyle={styles.SeparatorViewStyle}/>
            <View style={styles.MainContainer}>
                <View style={styles.ColumnContainer}>
                    <View style={styles.RowContainer}>
                        <Text style={[OATextStyle.TextFontSize_16_normal, props.titleStyle]}
                              numberOfLines={1}
                              ellipsizeMode='tail'>
                            {index} {item.title}
                        </Text>
                        <Text style={[OATextStyle.TextFontSize_12_normal,
                            styles.SubTitleText, props.subTitleStyle, (item.subtitle == null) ? {display: 'none'} : null]}
                              numberOfLines={1}
                              ellipsizeMode='tail'>
                            {item.subtitle}
                        </Text>
                    </View>
                    <TouchableOpacity onPress={onCellPress}>
                        <View style={[styles.RightImageContainer,{backgroundColor:colors.salmonRed,height:50,width:50}]}>

                        </View>

                    </TouchableOpacity>

                </View>
            </View>

        </TouchableOpacity>
    );
    function onCellPress() {
    }
}
