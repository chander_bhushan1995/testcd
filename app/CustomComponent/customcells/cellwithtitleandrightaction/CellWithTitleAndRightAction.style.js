import {StyleSheet} from "react-native";
import spacing from "../../../Constants/Spacing";
import colors from "../../../Constants/colors";

export default StyleSheet.create({

    MainContainer: {
        height: 56,
        flex: 1,
        justifyContent: 'center',
        alignItems: "center",
        flexDirection: "column"
    },
    ColumnContainer: {
        alignItems: "center",
        alignSelf: "center",
        flexDirection: "row",
    },
    RowContainer: {
        paddingRight: spacing.spacing16,
        paddingLeft: spacing.spacing16,
        flexDirection: "column"
    },
    SubTitleText: {
        marginTop: spacing.spacing6,
    },
    ImageRight: {
        alignSelf: 'center',
        marginTop: spacing.spacing12,
        marginRight: spacing.spacing16,
        justifyContent: 'flex-end',
        width: 24,
        height: 24
    },
    RightImageContainer: {
        flexDirection: 'row',
        flex: 1,
        backgroundColor: colors.salmonRed,
        justifyContent: 'flex-end'
    },
    SeparatorViewStyle: {backgroundColor: colors.color_F6F6F6}
});
