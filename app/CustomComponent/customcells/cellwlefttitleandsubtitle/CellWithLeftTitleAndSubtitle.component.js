import React from "react";
import {
    Text,
    View,
    Image,
    TouchableOpacity
} from "react-native";
import styles from "./CellWithLeftTitleAndSubtitle.style"
import SeparatorView from "../../separatorview";

export default function CellWithLeftTitleAndSubtitle(props) {
    let { item, index, onPress } = props;
    let {titleStyle, subTitleStyle, containerStyle} = props;
    let subTextComponent = null;

    if(item?.subTitle !== null && item?.subTitle !== undefined){
        subTextComponent = <Text style={[
            styles.SubTitleText, subTitleStyle, (item.subTitle === "") ? {display: 'none'} : null]}
                                 ellipsizeMode='tail'>
            {item.subTitle}
        </Text>;
    }

    return (
            <View style={[styles.MainContainer, containerStyle]}>
                <View style={styles.RowContainer}>
                    <View style={styles.ColumnContainer}>
                        <Text style={[styles.titleStyle, titleStyle, (item.title == null) ? {display: 'none'} : null]}
                              numberOfLines={1}
                              ellipsizeMode='tail'>
                            {item.title}
                        </Text>
                        {subTextComponent}
                    </View>
                    <TouchableOpacity onPress={onPress} style={[styles.RightImageContainer,{display:(item?.imageSource===undefined?'none':'flex')}]}>
                            <Image source={item?.imageSource?.source} style={item?.imageSource?.dimensions}/>
                    </TouchableOpacity>
                </View>
                <SeparatorView separatorStyle={styles.SeparatorViewStyle}/>
            </View>);
}
