import {StyleSheet} from "react-native";
import spacing from "../../../Constants/Spacing";
import colors from "../../../Constants/colors";
import {OATextStyle} from '../../../Constants/commonstyles';

export default StyleSheet.create({

    MainContainer: {
        flex: 1,
        alignItems: 'flex-start',
    },
    RowContainer: {
        alignSelf: "flex-start",
        flexDirection: "row",
    },
    ColumnContainer: {

        flex: 1,
        paddingTop: spacing.spacing16,
        paddingLeft: spacing.spacing16
    },
    titleStyle:{
        ...OATextStyle.TextFontSize_16_normal
    },
    SubTitleText: {
        marginTop: spacing.spacing4,
        ...OATextStyle.TextFontSize_12_normal
    },
    ImageRight: {
        alignSelf: 'center',
        justifyContent: 'flex-end',
        width:12,
        height:15
    },
    RightImageContainer: {
        marginTop: spacing.spacing12,
        marginRight: spacing.spacing8,
        padding: spacing.spacing9
    },
    SeparatorViewStyle: {backgroundColor: colors.color_F6F6F6, marginTop: spacing.spacing16}
});
