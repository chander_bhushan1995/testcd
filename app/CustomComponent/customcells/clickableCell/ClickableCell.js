import React from "react";
import {
    Text,
    View,
    Image,
    TouchableOpacity,
    Switch
} from "react-native";
import styles from "./styles"
import SeparatorView from "../../separatorview";
// import Switch from '../../../Components/CustomSwitch';
import colors from '../../../Constants/colors';
import CustomSwitch from '../../../Components/CustomSwitch';

export default function ClickableCell(props) {
    let { item, index, onPress } = props
    let { titleStyle, subTitleStyle, containerStyle } = props
    let { badgeText, badgeStyle, badgeTextStyle } = props
    let { isWithSwitch, isSwitchEnabled, onSwitchValueChange, switchRef } = props
    return (
        <View style={[styles.MainContainer, containerStyle]}>
            <TouchableOpacity onPress={onPress}>
                <View style={styles.RowContainer}>
                    <View style={styles.ColumnContainer}>
                        <Text style={[styles.titleStyle, titleStyle]}
                              numberOfLines={1}
                              ellipsizeMode='tail'>
                            {item.title}
                        </Text>
                        <Text style={[
                            styles.SubTitleText, subTitleStyle, (item.subTitle === "" || item.subTitle === undefined || item.subTitle === null) ? {display: 'none'} : null]}
                              ellipsizeMode='tail'>
                            {item.subTitle}
                        </Text>
                    </View>
                    <View style={badgeStyle}><Text style={badgeTextStyle}>{badgeText}</Text></View>
                    <View style={styles.RightImageContainer}>
                        {isWithSwitch
                        ? <Switch
                                onValueChange = {onSwitchValueChange}
                                value = {isSwitchEnabled}
                                trackColor={{ false: "#767577", true: colors.blue028 }}
                            />
                        : <Image source={item.imageSource.source} style={[styles.ImageRight,item.imageSource.dimensions]}/>
                        }
                    </View>
                </View>
            </TouchableOpacity>
            <SeparatorView separatorStyle={styles.SeparatorViewStyle} />
        </View>);
}
