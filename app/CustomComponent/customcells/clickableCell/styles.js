import {StyleSheet} from "react-native";
import spacing from "../../../Constants/Spacing";
import colors from "../../../Constants/colors";
import {OATextStyle} from '../../../Constants/commonstyles';

export default StyleSheet.create({

    MainContainer: {
        flex: 1,
        alignItems: 'flex-start',
    },
    RowContainer: {
        alignSelf: "flex-start",
        flexDirection: "row",
        paddingTop: spacing.spacing16,
        paddingHorizontal: spacing.spacing16
    },
    ColumnContainer: {
        flex: 1,
        alignSelf: 'center'
    },
    titleStyle:{
        ...OATextStyle.TextFontSize_16_bold
    },
    SubTitleText: {
        marginTop: spacing.spacing4,
        ...OATextStyle.TextFontSize_12_normal
    },
    ImageRight: {
        alignSelf: 'center',
        justifyContent: 'flex-end',
    },
    RightImageContainer: {
        flexDirection: 'row',
        // flex: 1,
        justifyContent: 'flex-end'
    },
    SeparatorViewStyle: {backgroundColor: colors.color_F6F6F6, marginTop: spacing.spacing16}
});
