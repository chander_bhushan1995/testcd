import {StyleSheet} from "react-native";
import colors from "../../../Constants/colors";
import spacing from "../../../Constants/Spacing";

export default StyleSheet.create({
    containerStyle: {
        flexDirection:'row',
        alignItems:'flex-start',
        backgroundColor:colors.color_FFFFFF,
        paddingVertical:spacing.spacing12
    }
});
