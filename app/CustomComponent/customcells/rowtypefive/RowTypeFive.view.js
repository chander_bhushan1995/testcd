import {Text, View} from "react-native";
import styles from "./RowTypeFive.style";
import React from "react";
import {OATextStyle} from "../../../Constants/commonstyles";
import colors from "../../../Constants/colors";
import {getRandomColor} from "../../../commonUtil/Formatter";

const RowTypeFive = ({item}) => (
    <View style={[styles.containerStyle]}>
        <View style={{flex: 1}}>
            <Text style={[OATextStyle.TextFontSize_14_normal, {color: colors.color_212121}]}>{item?.title}</Text>
        </View>
        <View style={{flex: 1, alignItems: 'flex-start'}}>
            <Text style={[OATextStyle.TextFontSize_14_normal, {color: colors.color_212121}]}>{item?.value}</Text>
        </View>
    </View>
);
export default RowTypeFive;

