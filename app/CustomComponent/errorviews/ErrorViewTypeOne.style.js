import {StyleSheet} from "react-native";
import colors from "../../Constants/colors";
import {OATextStyle} from "../../Constants/commonstyles";
import dimens from "../../Constants/Dimens";

export default StyleSheet.create({
    containerStyle: {
        alignItems:'flex-start',
        backgroundColor: colors.color_FFFFFF,
    },
    imageStyle:{
        width: dimens.dimen40
    },
    errorMsgStyle:{
        marginTop:-10,
        ...OATextStyle.TextFontSize_16_normal,
        color:colors.color_212121
    }

});
