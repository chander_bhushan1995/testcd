import {View, Image, Text} from "react-native";
import styles from "./ErrorViewTypeOne.style";
import React from "react";
import {OAImageSource} from "../../Constants/OAImageSource";

const ErrorViewTypeOne = ({item}) => (
    <View style={[styles.containerStyle, item?.style?.containerStyle]}>
        <Image style={[styles.imageStyle, item?.style?.imageStyle]} resizeMode={'contain'}
               source={OAImageSource.not_data_found_error}/>
        <Text style={[styles.errorMsgStyle, item?.style?.messageStyle]}>{item?.message}</Text>
    </View>
);
export default ErrorViewTypeOne;
