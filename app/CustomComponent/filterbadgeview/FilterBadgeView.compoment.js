import React, {useState} from "react";
import {
    View,
    Image, Text, TouchableOpacity
} from "react-native";
import styles from "./FilterBadgeView.style"
import colors from "../../Constants/colors";
import {OATextStyle} from "../../Constants/commonstyles";

export default function FilterBadgeView({status, onFilterBadgePress}) {
    const initialValue =
        {
            borderColor: '#0282F0',
            backgroundColor: colors.color_FFFFFF,
            textColor: "#0282F0",
            showTopRightIndicator: false,
        };
    const selectedValue =
        {
            borderColor: '#0282F0',
            backgroundColor: "#0282F0",
            textColor: colors.color_FFFFFF,
            showTopRightIndicator: true,
        };
    const [uiData, setUIData] = useState(status ? selectedValue : initialValue);

    return (
        <TouchableOpacity onPress={onFilterBadgePress}>
            <View>
                <View style={{
                    height: 32,
                    width: 60,
                    flexDirection: 'row',
                    borderRadius: 5,
                    borderWidth: 1,
                    borderColor: uiData.borderColor,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: uiData.backgroundColor
                }}>
                    <View>
                        {showTopRightIndicator()}
                        <View style={{flexDirection: 'row', marginTop: 3}}>
                            <View style={{marginTop:-2}}>
                                <Image style={{width: 12}} resizeMode={'contain'}
                                       source={(status ? require('../../images/selected_alert_filter.webp') : require('../../images/alert_filter.webp'))}/>
                            </View>

                            <Text
                                style={[OATextStyle.TextFontSize_10_normal_0282F0, {
                                    color: uiData.textColor,
                                    marginLeft: 4
                                }]}>Filter</Text>
                        </View>
                    </View>

                </View>

            </View>
        </TouchableOpacity>

    );

    function showTopRightIndicator() {
        if (uiData.showTopRightIndicator) {
            return <View style={styles.circleStyle}/>;
        }
        return null;
    }
}

