import {StyleSheet, Text} from "react-native";

import colors from "../../Constants/colors";

export default StyleSheet.create({
    mainContainer: {
        height: 96,
        width: 96,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.color_FFFFFF,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#D8D8D8'
    },
    circleStyle: {
        width: 6,
        alignSelf:'flex-end',
        marginRight:-12,
        marginTop:-6,
        height: 6,
        backgroundColor: "#FFAB00",
        borderRadius: 6 / 2
    },
    imageViewStyle: {
        padding: 12,
        width: '100%'
    }
});
