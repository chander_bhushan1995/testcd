import React, {Component} from "react";
import {
    Image,
    Modal,
    TouchableOpacity,
    View,
    Dimensions, TouchableWithoutFeedback
} from "react-native";
import styles from "./FullScreenImageView.style";
import spacing from "../../Constants/Spacing";
import {getRandomColor} from "../../commonUtil/Formatter";
import {OAImageSource} from "../../Constants/OAImageSource";

const mWidth = Dimensions.get('window').width;
const mHeight = Dimensions.get('window').height;
export default class FullScreenImageView extends Component {
    state = {modalVisible: false, imageUrl: null};

    setModalVisible(visible, imageUrl) {
        if (visible) {
            this.setState({modalVisible: visible, imageUrl: imageUrl});
        }
        this.setState({modalVisible: visible});
    }

    render() {

        return (
            <View>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }}>
                    <TouchableOpacity
                        activeOpacity={1}
                        style={styles.touchableOpacityStyle}
                        onPress={() => {
                            this.setModalVisible(false)
                        }}>
                        <TouchableWithoutFeedback style={{flex: spacing.spacing1}}>
                            <View style={{
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'center',
                                marginHorizontal: 16,

                                height: (mHeight)
                            }}>
                                <TouchableOpacity style={{
                                    alignSelf: 'flex-end',
                                    width: 30,
                                    padding:8,
                                    marginRight: 16,
                                }} onPress={() => {
                                    this.setModalVisible(false)
                                }}>
                                    <Image style={{
                                        alignSelf: 'flex-end',
                                        width: 12
                                    }} source={OAImageSource.white_cross_img}
                                           resizeMode={'contain'}/>
                                </TouchableOpacity>
                                <Image
                                    style={{
                                        height: (mHeight - 100),
                                        borderRadius: spacing.spacing4,
                                        marginTop: 10,
                                        width: mWidth
                                    }}
                                    resizeMode={'contain'}
                                    source={{ uri: this.state.imageUrl }} onLoadEnd={() => { }}
                                />
                            </View>
                        </TouchableWithoutFeedback>
                    </TouchableOpacity>
                </Modal>
            </View>
        );
    }
}
