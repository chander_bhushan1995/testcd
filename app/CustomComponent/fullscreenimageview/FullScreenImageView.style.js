import {StyleSheet} from "react-native";
import spacing from "../../Constants/Spacing";
import {OATextStyle} from "../../Constants/commonstyles";
import colors from "../../Constants/colors";

export default StyleSheet.create({
    container: {
        flex: spacing.spacing1,
    },
    touchableOpacityStyle: {
        flex: spacing.spacing1,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: colors.color_07000000
    },
    listContainerStyle: {
        alignSelf: 'flex-start',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        maxHeight: '80%',
        flexDirection: 'column',
        backgroundColor: colors.color_FFFFFF,
        padding: spacing.spacing16

    },
    recommendedActionsTitleStyle: {
        ...OATextStyle.TextFontSize_10_bold_888F97,
        textTransform: 'uppercase'
    },
});
