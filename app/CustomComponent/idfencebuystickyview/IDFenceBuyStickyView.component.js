import React, {useState, useEffect} from 'react';
import {
    NativeEventEmitter,
    NativeModules,
    Platform,
    Text,
    View,
} from 'react-native';
import styles from './IDFenceBuyStickyView.style';
import SeparatorView from '../separatorview';
import {OATextStyle} from '../../Constants/commonstyles';
import ButtonWithLoader from '../../CommonComponents/ButtonWithLoader';
import {PLATFORM_OS} from '../../Constants/AppConstants';
import {showAlertForAPI} from '../../commonUtil/CommonMethods.utility';
import {LOCATION, WEBENGAGE_IDFENCE_DASHBOARD} from '../../Constants/WebengageAttrKeys';
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';
import {getIDFCommonEventAttr} from '../../idfence/constants/IDFence.UtilityMethods';
import {getPlanStatus} from '../../idfence/dashboard/DashboardUtils';
import {PlanStatus} from '../../idfence/constants/Constants';

const nativeBrigeRef = NativeModules.ChatBridge;
let dialogViewRef;
export default function IDFenceBuyStickyView({data, upgradeLabel, from, membershipData, actionText, planPrice, isRenewal, renewalAction}) {
    const [isShowButtonLoader, setButtonLoading] = useState(false);


    function receiveMessageFromRenewalAPI(params) {
        if (params == 'fail') {
            showAlertForAPI(
                dialogViewRef,
                'Alert',
                'Something Went Wrong!!!',
            );
        }
        setButtonLoading(false);
    }

    const eventEmitter = new NativeEventEmitter(nativeBrigeRef);

    useEffect(() => {
        if (Platform.OS === 'ios') {
            eventEmitter.addListener('IDFenceRenewalEvent', params => receiveMessageFromRenewalAPI(params));
            return () => {
                eventEmitter.removeListener('IDFenceRenewalEvent', () => {
                });
            };
        }
    }, []);
    return (
        <View style={styles.actionViewContainerStyle}>
            <View style={styles.actionMainViewStyle}>
                <SeparatorView/>
                <View style={styles.actionMainRowViewStyle}>
                    <View style={styles.actionLeftViewStyle}>
                        <View style={styles.actionLeftViewContainerStyle}>
                            <Text style={[OATextStyle.TextFontSize_12_bold_212121, {lineHeight: 20}]}>{upgradeLabel}
                                &nbsp; @ ₹{isRenewal ? planPrice : data.price} </Text>
                            {!isRenewal ? <Text style={styles.textDecorationStyle}>₹{data.anchorPrice}</Text> : null }
                        </View>
                        {!isRenewal ? <Text
                            style={styles.textSavePerMonth}>Save {data.anchorPrice - data.price} per
                            month</Text> : null}
                    </View>

                    <View style={styles.actionRightViewStyle}>
                        <ButtonWithLoader
                            isLoading={isShowButtonLoader}
                            enable={true}
                            Button={{
                                text: actionText,
                                onClick: () => {
                                    let eventAttr = getIDFCommonEventAttr(membershipData);
                                    eventAttr[LOCATION] = from;
                                    logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_DASHBOARD_BUY_PREMIUM_FOOTER, eventAttr);
                                    setButtonLoading(true);
                                    if (isRenewal){
                                        renewalAction()
                                        return
                                    }
                                    if (Platform.OS === PLATFORM_OS.android) {
                                        openIdFence().then(value => {
                                            setButtonLoading(false);
                                        }).catch(onerror => {
                                            setButtonLoading(false);
                                            showAlertForAPI(
                                                dialogViewRef,
                                                'Alert',
                                                'Something Went Wrong!!!',
                                            );
                                        });

                                    } else {
                                        setButtonLoading(false);
                                        nativeBrigeRef.openIDFencePlanDetails(false, '');
                                    }
                                },
                            }}
                        />
                    </View>
                </View>
            </View>
        </View>
    );

    async function openIdFence() {
        await nativeBrigeRef.openIDFencePlanDetails(false, '');
    }
}

