import {StyleSheet} from "react-native";
import colors from "../../Constants/colors";
import spacing from "../../Constants/Spacing";
import {OATextStyle} from "../../Constants/commonstyles";
import dimens from "../../Constants/Dimens";

export default StyleSheet.create({
    actionViewContainerStyle: {
        flexDirection: 'row',
        height: dimens.dimen75,
        width: dimens.width100,
        backgroundColor: colors.color_FFFFFF,
        borderColor: "transparent",
        shadowColor: colors.color_000000,
        shadowOffset: {
            width: dimens.dimen0,
            height: -5,
        },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 4,
    },
    actionMainViewStyle: {
        width: dimens.width100,
    },
    actionMainRowViewStyle: {
        flexDirection: 'row',
        width: dimens.width100,
    },
    actionLeftViewStyle: {
        flex: spacing.spacing1,
        padding: spacing.spacing12,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    actionLeftViewContainerStyle: {
        flexDirection: 'row'
    },
    actionRightViewStyle: {
        flex: spacing.spacing1,
        padding: spacing.spacing12,
        justifyContent: 'flex-end'
    },
    textDecorationStyle: {
        ...OATextStyle.TextFontSize_12_bold_212121,
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid'
    },
    textSavePerMonth: {
        ...OATextStyle.TextFontSize_12_bold_212121,
        color: colors.color_45B448
    }

});
