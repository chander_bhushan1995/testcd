import React from "react";
import {
    Text,
    View,
    Image,
    ImageBackground
} from "react-native";
import styles from "./IDFencePremiumPlanView.style"
import PoweredByView from "../poweredbyview";
import RightTickWithLabelView from "../righttickwithlabelview"
import LinkButton from "../LinkButton";
import SeparatorView from "../separatorview";

export default function IDFencePremiumPlanView({
                                                   title, //title
                                                   subTitle,  //sub title
                                                   benefitPoints,
                                                   ...props //link button props
                                               }) {
    return (
        <ImageBackground style={styles.MainContainer}
                         source={require('../../images/ic_confetti_nextbestplan.webp')}>
            <SeparatorView/>
            <View style={styles.SubContainer}>
                <PoweredByView/>
                <Text style={styles.SaveUpToText}>{title}</Text>
                <Text style={styles.BuyIDFenceText}>{subTitle}</Text>
                <RightTickWithLabelView label={benefitPoints[0]}/>
                <RightTickWithLabelView label={benefitPoints[1]}/>
                <LinkButton {...props}/>
            </View>
            <SeparatorView/>
        </ImageBackground>
    );
}

