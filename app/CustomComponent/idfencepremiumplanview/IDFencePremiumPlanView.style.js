import {StyleSheet, Text} from "react-native";
import spacing from "../../Constants/Spacing";
import dimens from "../../Constants/Dimens";
import colors from "../../Constants/colors";
import fontSizes from "../../Constants/FontSizes";

export default StyleSheet.create({
    MainContainer: {
        height: 230,
        alignItems: 'center',
    },
    SubContainer: {
        alignItems: 'center',
        paddingTop: spacing.spacing10,
        paddingBottom: spacing.spacing10,
        backgroundColor: 'transparent'
    },
    SaveUpToText: {
        color: colors.color_45B448,
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_bold,
        fontSize: fontSizes.fontSize14,
        marginTop: spacing.spacing8
    },
    BuyIDFenceText: {
        color: colors.color_212121,
        textAlign: 'center',
        fontFamily: fontSizes.fontFamily_Lato,
        fontWeight: fontSizes.fontWeight_bold,
        fontSize: fontSizes.fontSize16,
        marginTop: spacing.spacing12
    }
});
