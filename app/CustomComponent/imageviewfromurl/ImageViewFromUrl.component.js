import React, {useState} from "react";
import {
    View,
    Image,
    TouchableOpacity
} from "react-native";
import styles from "./ImageViewFromUrl.style"
import OAActivityIndicator from "../OAActivityIndicator";
import {getRandomColor} from "../../commonUtil/Formatter";

export default function ImageViewFromUrl({imageUrl, onPress}) {
    const [isLoading, setIsLoading] = useState(true);


    return (
        <TouchableOpacity onPress={(e) => {
            e.stopPropagation();
            onPress(imageUrl);
        }} style={styles.mainContainer}>
            <Image style={[styles.imageViewStyle]}
                   onLoadStart={() => setIsLoading(true)}
                   resizeMode={'contain'}
                   source={{uri: imageUrl}}
                   onLoadEnd={() => setIsLoading(false)}
            ></Image>
            {showLoading(isLoading)}
        </TouchableOpacity>
    );

    function showLoading(isLoading) {
        if (isLoading) {
            return (
                <View style={{width: 16, position: 'absolute', backgroundColor: getRandomColor()}}><OAActivityIndicator/></View>);
        }
        return null;
    }

}

