import {StyleSheet, Text} from "react-native";

import colors from "../../Constants/colors";

export default StyleSheet.create({
    mainContainer: {
        width: 96,
        height:96,
        alignItems: 'center',
        justifyContent:'center',
        backgroundColor: colors.color_FFFFFF,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#D8D8D8'
    },

    imageViewStyle:{
        padding:12,
        width: '100%',
        height: '100%'
    }
});
