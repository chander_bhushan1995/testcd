import React from "react";
import {
    Text,
    View,
    Image
} from "react-native";
import styles from "./PoweredByView.style"

export default function IDFencePremiumPlanView() {
    return (
        <View style={styles.PoweredByMainContainer}>
            <Text style={styles.PoweredByText}>Powered by</Text>
            <Image source={require("../../images/drawable_count_circle.webp")}/>
        </View>
    );
}
