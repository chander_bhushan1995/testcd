import {StyleSheet} from "react-native";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import fontSizes from "../../Constants/FontSizes";

export default StyleSheet.create({
    PoweredByMainContainer: {
        flexDirection: "row",
        alignItems:'center',
        justifyContent: 'center'
    },
    PoweredByText:{
        fontFamily: fontSizes.fontFamily_Lato,
        fontSize: fontSizes.fontSize8,
        color: colors.color_808080,
    }

});
