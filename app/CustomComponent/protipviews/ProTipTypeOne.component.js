import React from "react";
import {View, Image, Text} from "react-native";
import styles from "./ProTipTypeOne.style";
import {OAImageSource} from "../../Constants/OAImageSource";
import {OATextStyle} from "../../Constants/commonstyles";

const ProTipTypeOne = ({item, customStyle}) => (
    <View style={[styles.containerStyle, customStyle?.containerStyle]}>
        <View style={{paddingVertical: 12}}>
            <Image style={{width: 60, height: 20}} resizeMode={'contain'} source={OAImageSource.pro_tip}/>
            <View style={{flex: 1, paddingHorizontal: 12,}}>
                <Text style={[OATextStyle.TextFontSize_12_bold_212121, {paddingVertical: 8}]}>
                    {item.title}
                </Text>
                <Text style={[OATextStyle.TextFontSize_12_normal]}>
                    {item.detail}
                </Text>
            </View>
        </View>
    </View>
);

export default ProTipTypeOne;
