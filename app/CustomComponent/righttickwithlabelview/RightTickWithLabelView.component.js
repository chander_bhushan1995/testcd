import React from "react";
import { Image, Text, View } from "react-native";
import styles from "./RightTickWithLabelView.style";

export default function RightTickWithLabelView(props) {
  let { label, MainContainer, ItemText } = props;
  return (
    <View style={[styles.MainContainer, MainContainer]}>
      <Image
        style={styles.ImgTick}
        source={require("../../images/ic_tick_right.webp")}
      />
      <Text style={[styles.ItemText, ItemText]}>{label}</Text>
    </View>
  );
}
