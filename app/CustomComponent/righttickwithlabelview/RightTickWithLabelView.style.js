import { StyleSheet, Image } from "react-native";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import fontSizes from "../../Constants/FontSizes";

export default StyleSheet.create({
  MainContainer: {
    marginTop: spacing.spacing4,
    marginBottom: spacing.spacing4,
    flexDirection: "row",
    justifyContent: "flex-start"
  },
  ItemText: {
    marginLeft: spacing.spacing8,
    fontFamily: fontSizes.fontFamily_Lato,
    fontSize: fontSizes.fontSize14,
    color: colors.color_212121
  },
  ImgTick: {
    marginTop:spacing.spacing4,
    width: spacing.spacing12,
    height: spacing.spacing12
  }
});
