import React, {useState, forwardRef, useImperativeHandle} from "react";
import {Image, Text, TouchableOpacity, View} from "react-native";
import styles from "./SelectableTextWithIndicatorView.style";
import colors from "../../Constants/colors";

const SelectableTextWithIndicatorView = forwardRef((props, ref) => {
    const initialValue =
        {
            containerBgColor: colors.color_F6F6F6,
            textColor: "#212121",
            showRightIndicator: false,
        };
    const selectedValue =
        {
            containerBgColor: colors.color_FFFFFF,
            textColor: "#0282F0",
            showRightIndicator: true,
        };
    const [selectedUIData, setUIData] = useState({
        isSelected: props.setDefaultSelected,
        data: (props.setDefaultSelected ? selectedValue : initialValue)
    });

    useImperativeHandle(ref, () => {
        return {
            resetRowSelection: resetRowSelection
        };
    });
    const resetRowSelection = (status) => {
        setUIData(prev => ({
            ...prev,
            isSelected: status,
            data: (status ? selectedValue : initialValue),
        }))
    }


    return (
        <TouchableOpacity onPress={(e) => onRowClick(e)}>
            <View style={[styles.mainContainer, {backgroundColor: selectedUIData.data.containerBgColor}]}>
                <Text style={[styles.itemText, {color: selectedUIData.data.textColor}]}>{props.label}</Text>
                {showRightIndicator()}
            </View>
        </TouchableOpacity>
    );

    function showRightIndicator() {
        if (selectedUIData.isSelected) {
            return <View style={styles.indicatorCircleStyle}/>;
        }
        return null;
    }


    function onRowClick(e) {
        e.stopPropagation();
        props.onRowPress(props.index);
    }
});

export default SelectableTextWithIndicatorView;
