import {StyleSheet, Image} from "react-native";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import {OATextStyle} from "../../Constants/commonstyles";

export default StyleSheet.create({
    mainContainer: {
        minHeight: 60,
        backgroundColor: "#22aaee",
        flexDirection: "row",
        paddingLeft:18,
        alignItems:'center',
    },
    itemText: {
        ...OATextStyle.TextFontSize_14_bold,
        color: "#0282F0"
    },
    indicatorCircleStyle: {
        marginLeft:4,
        backgroundColor: colors.color_0282F0,
        borderRadius: spacing.spacing6 / 2,
        width: spacing.spacing6,
        height: spacing.spacing6
    }
});
