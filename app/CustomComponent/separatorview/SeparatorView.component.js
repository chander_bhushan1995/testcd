import React from "react";
import {
    View
} from "react-native";
import styles from "./SeparatorView.style";

export default function SeparatorView({separatorStyle}) {
    return (
        <View style={[styles.MainContainer, separatorStyle]}/>
    );
}
