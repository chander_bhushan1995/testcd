import {StyleSheet} from "react-native";
import colors from "../../Constants/colors";

export default StyleSheet.create({
    MainContainer: {
        height: 1,
        width:'100%',
        backgroundColor: colors.color_E0E0E0
    }
});
