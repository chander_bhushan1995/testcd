import React, { useState } from "react";
import { View, Switch, Alert, Image, Text, StyleSheet } from "react-native";
import SeparatorView from "../../CustomComponent/separatorview";
import styles from "./styles"
export default function SocialCard(props) {
  let {onPress} = props;
  let { title, imageSource, linkUrl, linked } = props.item;
  const [switchOnValueHolder, setSwitchOnValueHolder] = useState(0);

  return (
    <View style={styles.mainContainer}>
      <SeparatorView separatorStyle={styles.SeparatorViewStyle} />
      <View style={styles.rowContainer}>
        <View style={styles.leftContainer}>
          <View style={styles.imageContainer}>
            <Image source={imageSource.source} style={imageSource.dimensions} />
          </View>
          <Text style={styles.titleStyle}> {title} </Text>
        </View>
        <Switch
          onValueChange={value => onPress(value)}
          value={linked}
          style={styles.switchStyle}
        />
      </View>
    </View>
  );
}
