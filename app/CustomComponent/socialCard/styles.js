import { StyleSheet } from "react-native";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import dimens from "../../Constants/Dimens";

export default (styles = StyleSheet.create({
  mainContainer: {
    paddingHorizontal: spacing.spacing16,
    backgroundColor: colors.color_FAFAFA
  },
  rowContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  leftContainer: { flexDirection: "row", alignItems: "center" },
    titleStyle:{paddingLeft: spacing.spacing16},
  switchStyle: {
    marginVertical: spacing.spacing12
  },
  SeparatorViewStyle: {
    backgroundColor: colors.color_F6F6F6
  }
}));
