import React, { useState } from "react";
import { StyleSheet, View, Alert } from "react-native";
import TabBarCell from "./TabBarCell";
import colors from "../../Constants/colors";
import spacing from "../../Constants/Spacing";
export default function TabBar(props) {
  tabs = index => {
    let overviewTab = (
      <TabBarCell
        title={"Overview"}
        index={0}
        isSelected={index === 0}
        tabChanged={this.tabChanged}
      />
    );

    let alertsTab = (
      <TabBarCell
        title={"Alerts"}
        index={1}
        isSelected={index === 1}
        tabChanged={this.tabChanged}
      />
    );
    let assetsTab = (
      <TabBarCell
        title={"Assets"}
        index={2}
        tabChanged={this.tabChanged}
        isSelected={index === 2}
      />
    );

    let detailsTab = (
      <TabBarCell
        title={"Details"}
        index={3}
        isSelected={index === 3}
        tabChanged={this.tabChanged}
      />
    );
    let tabs = [overviewTab, alertsTab, assetsTab, detailsTab];
    return tabs;
  };

  tabChanged = index => {
    props.onPress(index);
  };

  return (
    <View style={styles.container}>
      <View style={styles.tabContainer}>{this.tabs(props.selectedTab)}</View>

      <View style={styles.bottomSeparator}></View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: 'stretch',
    height: 64
  },
  tabContainer: {
    flexDirection: "row",
    justifyContent: "space-around"
  },
  bottomSeparator: {
    height: spacing.spacing2,
    backgroundColor: "#d8d8d8",
    width: "100%"
  }
});
