import React, { useState } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import colors from "../../Constants/colors";
import { TextStyle } from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";

export default function TabBarCell(props) {
  let titleColor = colors.grey;
  let separatorColor = colors.white;
  if (props.isSelected) {
    titleColor = colors.blue028;
    separatorColor = colors.blue028;
  }
  return (
    <View style={styles.cellContainer}>
      <TouchableOpacity
        style={{}}
        onPress={() => {
          if (props.tabChanged !== null && props.tabChanged !== undefined) {
            props.tabChanged(props.index);
          }
        }}
      >
        <Text
          style={[
            TextStyle.text_14_normal,
            {
              color: titleColor,
              // paddingHorizontal: spacing.spacing16,
              paddingBottom: spacing.spacing8
            }
          ]}
        >
          {props.title}
        </Text>
      </TouchableOpacity>
      <View
        style={[styles.bottomSeparator, { backgroundColor: separatorColor }]}
      ></View>
    </View>
  );
}

const styles = StyleSheet.create({
  cellContainer: {
    justifyContent: "center"
    // backgroundColor: "gray"
  },
  bottomSeparator: {
    height: 2,
    marginBottom: 0,
    width: "100%",
    backgroundColor: "#000000"
  }
});
