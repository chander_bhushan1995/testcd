import React from "react";
import {View, Image, Text} from "react-native";
import styles from "./TipsListTypeOne.style";
import {OATextStyle} from "../../../Constants/commonstyles";
import colors from "../../../Constants/colors";

let defaultData = [
    'Pay your pending dues at the earliest',
    'Set up reminders to ensure timely payments',
    'Set up AUTO-BILL pay to avoid payment failure'
];


const TipsListTypeOne = ({item, customStyle}) => (
    <View style={styles.containerStyle}>
        <Text
            style={[OATextStyle.TextFontSize_12_bold_212121, {color: colors.color_888F97, textTransform: 'uppercase'}]}>
            Tips to improve your credit score
        </Text>
        <View style={{paddingVertical: 10}}>
            {getTipsList(item||defaultData)}
        </View>
    </View>
);

function getTipsList(item) {
    return item.map((value, index) => {
        return (<Text
            key={index}
            style={[OATextStyle.TextFontSize_14_normal, {
                color: colors.color_212121,
                paddingVertical: 8
            }]}>{(index + 1) + `. ` + value}</Text>);
    });
}

export default TipsListTypeOne;
