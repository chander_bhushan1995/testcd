import React, {useState} from "react";
import {View} from "react-native";
import ProgressBar from "../../CommonComponents/ProgressBar";
import OALinearGradient from "../../CommonComponents/OALinearGradient";
import InfoCard from "../InfoCard";
import LRActionView, {LRButtonViewStyleWhiteTheme} from '../LRActionView';
import LRTextView from "../LRTextView";
import {
    styles,
    LRTextViewStyle,
    infoCardStyle,
    LRTextViewStyleWhiteTheme,
    infoCardStyleWhiteTheme,
    progressBarWhiteTheme
} from "./styles";
import spacing from "../../Constants/Spacing";
import colors from '../../Constants/colors';
import {CommonStyle} from '../../Constants/CommonStyle';

export default function TodoCard(props) {
    let shadowStyle = null;
    if (props.theme !== null && props.theme !== undefined && props.theme === colors.white) {
        shadowStyle = CommonStyle.cardContainerWithShadow;
    }
    return (
        <OALinearGradient
            degree={props?.data?.degree}
            colors={props?.data?.colors}
            child={todoCard(props)}
            style={[{flex: 1, borderRadius: 4}, null]}
        />
    );
}

const todoCard = props => {
    let {onLeftClick, onRightClick} = props;
    let progressTextStyle = LRTextViewStyle;
    let infoCardTextStyle = infoCardStyle;
    let progressBarTheme = null;
    if (props.theme !== null && props.theme !== undefined && props.theme === colors.white) {
        progressTextStyle = LRTextViewStyleWhiteTheme;
        infoCardTextStyle = infoCardStyleWhiteTheme;
        progressBarTheme = progressBarWhiteTheme;

    }
    return <View style={styles.container}>
        <LRTextView data={props.data?.progressBarData} style={progressTextStyle}/>
        <ProgressBar data={props.data?.progressBarData}
                     theme={props.theme}
                     style={progressBarTheme}

        />
        <InfoCard data={props.data?.infoCardData} style={infoCardTextStyle}/>
        <LRActionView data={props.data?.actionData}
                      onLeftClick={onLeftClick}
                      onRightClick={onRightClick}
                      theme={props.theme}
        />
    </View>
};
