import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import Dimens from "../../Constants/Dimens";
import { TextStyle } from "../../Constants/CommonStyle";
import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    padding: spacing.spacing16,
    borderRadius: spacing.spacing4
  }
});
export const LRTextViewStyle = {
  headerStyle: { marginBottom: spacing.spacing4 },
  leftText: {
    ...TextStyle.text_14_normal,
    color: colors.white
  },
  rightText: {
    ...TextStyle.text_14_normal,
    color: colors.white
  }
};

export const infoCardStyle = {
  titleStyle: { ...TextStyle.text_16_bold, color: colors.white },
  subtitleStyle: {
    ...TextStyle.text_14_normal,
    color: colors.white
  },
  imageStyle: { width: Dimens.dimen16, height: Dimens.dimen16 },
  containerStyle: { marginTop: spacing.spacing20, marginBottom: 16 }
};

export const LRTextViewStyleWhiteTheme = {
  headerStyle: { marginBottom: spacing.spacing4 },
  leftText: {
    color: colors.color_45B448
  },
  rightText: {
  }
};

export const infoCardStyleWhiteTheme = {
  titleStyle: { ...TextStyle.text_16_regular},
  subtitleStyle: {
    ...TextStyle.text_14_normal
  },
  imageStyle: { width: Dimens.dimen16, height: Dimens.dimen16 },
  containerStyle: { marginTop: spacing.spacing20, marginBottom: 16 }
};
export const progressBarWhiteTheme = {
  progressBarStyle: { backgroundColor: colors.color_38B839},
  backgroundColor: colors.greyf2
};
