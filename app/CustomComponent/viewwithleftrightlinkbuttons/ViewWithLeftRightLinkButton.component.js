import React from "react";
import {
    Text,
    View,
    Image, TouchableOpacity
} from "react-native";
import styles from "./ViewWithLeftRightLinkButton.style";
import {OATextStyle} from "../../Constants/commonstyles";
import SeparatorView from "../separatorview";
import colors from "../../Constants/colors";
import dimens from "../../Constants/Dimens";
import LinkButton from "../LinkButton";
import IDFencePremiumPlanView from "../idfencepremiumplanview";
import fontSizes from "../../Constants/FontSizes";

export default function ViewWithLeftRightLinkButton({
                                                        leftActionTitle,
                                                        leftActionType,
                                                        rightActionTitle,
                                                        rightActionType,
                                                        actionCallBack,
                                                        index,
                                                        item
                                                    }) {

    return (
        <View style={styles.MainContainer}>
            <SeparatorView separatorStyle={{backgroundColor: colors.color_F6F6F6}}/>
            <View style={{
                flexDirection: "row",
                minHeight: dimens.dimen50
            }}>
                <View style={{
                    flex: 1,
                    alignItems: 'center',
                    flexDirection: 'column',
                    paddingTop: 6
                }}>
                    <LinkButton
                        style={{
                            actionTextStyle: [OATextStyle.TextFontSize_14_bold, {
                                color: colors.color_0282F0
                            }]
                        }}
                        data={{
                            action: leftActionTitle
                        }}
                        onPress={() => onLeftButtonClick({leftActionType})}/>
                </View>
                <View style={{
                    width: 1,
                    flexDirection: 'column',
                    backgroundColor: colors.color_E0E0E0
                }}/>
                <View style={{
                    flex: 1,
                    alignItems: 'center',
                    flexDirection: 'column',
                    paddingTop: 6
                }}>
                    <LinkButton
                        style={{
                            actionTextStyle: [OATextStyle.TextFontSize_14_bold, {
                                color: colors.color_0282F0
                            }]
                        }}
                        data={{
                            action: rightActionTitle
                        }}
                        onPress={() => onRightButtonClick({rightActionType})}/>
                </View>
            </View>
            <SeparatorView separatorStyle={{backgroundColor: colors.color_F6F6F6}}/>
        </View>
    );

    function onLeftButtonClick(action) {
        actionCallBack({item}.item, {index}.index, {action}.action.leftActionType);
    }

    function onRightButtonClick(action) {
        actionCallBack({item}.item, {index}.index, {action}.action.rightActionType);
    }
}
