import {StyleSheet} from "react-native";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import fontSizes from "../../Constants/FontSizes";
import dimens from "../../Constants/Dimens";

export default StyleSheet.create({
    MainContainer: {
        flexDirection: "column",
        backgroundColor: colors.color_FAFAFA,
    },
});
