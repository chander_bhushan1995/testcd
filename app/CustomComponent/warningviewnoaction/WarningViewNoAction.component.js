import React from "react";
import {
    Text,
    View,
    Image, TouchableOpacity
} from "react-native";
import styles from "./WarningViewNoAction.style";
import {OATextStyle} from "../../Constants/commonstyles";

/**
 *    View for Warning
 *    1. Title at top left
 *    2. Subtitle below 1.
 *    3. Single Action button
 *    4. Cross view at top-right
 * @returns {*}
 * @constructor
 */
export default function WarningViewNoAction({
                                                onCrossPress, // top-right cross image click listener call back
                                                title,  //card top-left  title text
                                                subTitle // card sub title text
                                            }) {
    return (
        <View style={styles.WarningViewNoAction}>
            <View style={styles.TitleContainer}>
                <Text style={[OATextStyle.TextFontSize_14_bold, styles.TextTitle]} numberOfLines={1}
                      ellipsizeMode='tail'>
                    {title}
                </Text>
                <TouchableOpacity
                    onPress={(e) => {
                        e.preventDefault()
                        onCrossPress()
                    }}>
                    <Image style={styles.ImageTopRight} source={require("../../images/cross_warning.webp")}/>
                </TouchableOpacity>

            </View>
            <View style={styles.SubTitleContainer}>
                <Text style={[OATextStyle.TextFontSize_14_normal, styles.TextSubTitle]} numberOfLines={4}
                      ellipsizeMode='tail'>
                    {subTitle}
                </Text>
            </View>
        </View>
    );
}
