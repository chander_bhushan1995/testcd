import React from "react";
import {
    Text,
    View,
    Image,
    TouchableOpacity,
    StyleSheet,
} from "react-native";
import Button from "../../CommonComponents/Button";
import styles from "./WarningViewWithAction.style";
import {OATextStyle} from "../../Constants/commonstyles";

/**
 *    View for Warning
 *    1. Title at top left
 *    2. Subtitle below 1.
 *    3. Single Action button
 *    4. Cross view at top-right
 * @returns {*}
 * @constructor
 */
export default function WarningViewWithAction({
                                                  onMainActionPress, // main action button press callback listener
                                                  onCrossPress, // top-right cross image click listener call back
                                                  label, // main action button label text
                                                  title,  //card top-left  title text
                                                  subTitle // card sub title text
                                              }) {
    return (
        <View style={styles.WarningViewWithActionMain}>
            <View style={styles.TitleContainer}>
                <Text style={[OATextStyle.TextFontSize_14_bold,styles.TextTitle ]} numberOfLines={1}
                      ellipsizeMode='tail'>
                    {title}
                </Text>
                <TouchableOpacity
                    onPress={(e) => {
                        e.preventDefault();
                        onCrossPress()
                    }}>
                    <Image style={styles.ImageTopRight} source={require("../../images/cross_warning.webp")}/>
                </TouchableOpacity>

            </View>

            <View style={styles.SubTitleContainer}>
                <Text style={[OATextStyle.TextFontSize_14_normal,styles.TextSubTitle]} numberOfLines={4}
                      ellipsizeMode='tail'>
                    {subTitle}
                </Text>
                <View style={styles.ActionButtonContainer}>
                    <Button
                        enable={true}
                        Button={{
                            text: label,
                            onClick: (e) => {
                                {
                                    {onMainActionPress()}
                                }
                            }
                        }}
                    />
                </View>
            </View>
        </View>
    );
}
