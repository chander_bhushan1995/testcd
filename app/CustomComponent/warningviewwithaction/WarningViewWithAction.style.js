import {StyleSheet} from "react-native";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import fontSizes from "../../Constants/FontSizes";

export default StyleSheet.create({
    WarningViewWithActionMain: {
        minHeight: spacing.height_142,
        flexDirection: "column",
        backgroundColor: 'rgba(245,166,35,0.2)',
        borderColor: colors.color_F5A623,
        borderWidth: spacing.spacing1,
        marginRight: spacing.spacing1,
        padding: spacing.spacing16,
        marginLeft: spacing.spacing1
    },
    TitleContainer: {
        flexDirection: "row"
    },
    SubTitleContainer: {
        flexDirection: "column"
    },
    TextTitle: {
        width: spacing.per_width_95,
        paddingRight: spacing.spacing16
    },
    TextSubTitle: {
        width: spacing.per_width_95,
        paddingRight: spacing.spacing16,
        paddingTop: spacing.spacing8
    },
    ActionButtonContainer: {
        marginTop: spacing.spacing16,
        alignSelf: 'stretch',
        width: spacing.width_120
    },
    ImageTopRight: {
        width: spacing.spacing16,
        height: spacing.spacing16
    }
});
