import React, { Component } from "react";
import {
  SafeAreaView,
  StyleSheet,
  View,
  Platform,
  Text, BackHandler, Dimensions,
} from 'react-native';
import WebView from 'react-native-webview';
import spacing from "../../Constants/Spacing";
import dimens from "../../Constants/Dimens";
import colors from "../../Constants/colors";
import { TextStyle } from "../../Constants/CommonStyle";
import PlatformActivityIndicator from "../PlatformActivityIndicator";
import { PLATFORM_OS } from "../../Constants/AppConstants";
import { HeaderBackButton } from "react-navigation";
import {ASSET_NAME, LOCATION, WEBENGAGE_IDFENCE_DASHBOARD} from "../../Constants/WebengageAttrKeys";
import {logWebEnageEvent} from "../../commonUtil/WebengageTrackingUtils";

let screenNavigator;
let tabBarRefreshed = false;
let asset;
let location;


export default class OAWebView extends Component {
  static navigationOptions = ({ navigation }) => {
    screenNavigator = navigation;
    let componentTitle = navigation.getParam("title", null);
    if (componentTitle == null || componentTitle == undefined) {
      componentTitle = "Connect";
    }
    return {
      headerTitle: (
        <View
          style={
            Platform.OS === PLATFORM_OS.ios
              ? { alignItems: "center" }
              : { alignItems: "flex-start" }
          }
        >
          <Text style={TextStyle.text_16_bold}>{componentTitle}</Text>
        </View>
      ),
      headerLeft: (
        <HeaderBackButton
          tintColor={colors.black}
          onPress={() => {
            navigation.pop();
          }}
        />
      ),
      headerTitleStyle: { color: colors.white },
      headerTintColor: colors.white
    };
  };
  componentDidMount() {
    tabBarRefreshed = false;
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }
  render() {
    const { navigation } = this.props;
    let url = navigation.getParam("url", "www.google.com");
    asset = navigation.getParam("asset", "");
    location = navigation.getParam("location", "");


    return (
      <SafeAreaView style={{ flex: 1 }}>
        <WebView
          ref={ref => {
            this.webView = ref;
          }}
          style={styles.WebViewStyle}
          source={{ uri: url }}
          thirdPartyCookiesEnabled={true}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          scalesPageToFit={true}
          renderLoading={this.getActivityIndicatorLoadingView}
          startInLoadingState={true}
          onShouldStartLoadWithRequest={navigator =>
            this.onShouldStartLoadWithRequest(navigator)
          }
          onNavigationStateChange={navigator =>
            this.onShouldStartLoadWithRequest(navigator)
          }
          dataDetectorTypes={"none"}
          source={{ uri: url }}
          style={{ padding: spacing.spacing16 }}
        />
      </SafeAreaView>
    );
  }

  getActivityIndicatorLoadingView() {
    return (
      <View style={styles.loaderContainer}>
        <PlatformActivityIndicator />
      </View>
    );
  }

  onShouldStartLoadWithRequest = navigator => {
    if (navigator.url.includes(".in/idfence/?subscriberNo")) {
      this.sendEvent();
      this.webView.stopLoading();
      if (!tabBarRefreshed) this.props.refreshTabBar(screenNavigator);
      tabBarRefreshed = true;
      return false;
    } else {
      return true;
    }
  };

  sendEvent(){
    let attr = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.attrValueForAsset(asset);
    if (attr !== null && attr !== undefined) {
      let eventAttr = new Object();
      eventAttr[LOCATION] = location;
      eventAttr[ASSET_NAME] = attr;
      logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_ASSET_ADDED, eventAttr);
    }
  }

  onBackPress = () => {
    const {navigation} = this.props;
    let enableNativeBack = navigation.getParam('enableNativeBack', false);
    if ((enableNativeBack == null || enableNativeBack == undefined)) {
      nativeBrigeRef.goBack();
    } else {
      screenNavigator.pop();
      return true;
    }
    return false;
  };
}
const styles = StyleSheet.create({
  SafeArea: {
    flex: spacing.spacing1,
    width: dimens.width100,
    height: dimens.height100,
    flexDirection: "column",
    backgroundColor: colors.white
  },
  textStyle: {
    flex: spacing.spacing1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: spacing.spacing20
  },
  imgStyle: {
    width: dimens.dimen16,
    height: dimens.dimen16,
    flex: spacing.spacing1,
    marginTop: spacing.spacing1
  },
  container: {
    flex: spacing.spacing1,
    backgroundColor: colors.white,
    paddingTop: spacing.spacing32
  },
  scrollViewStyle: {
    backgroundColor: colors.white,
    flex: spacing.spacing1,
    width: dimens.width100,
    position: "absolute",
    top: spacing.spacing0,
    bottom: spacing.spacing0,
    left: spacing.spacing0,
    right: spacing.spacing0,
    alignSelf: "stretch",
    height: dimens.height80,
    flexDirection: "column"
  },
  WebViewStyle: {
    justifyContent: "center",
    alignItems: "center",
    flex: spacing.spacing1
  },
  ActivityIndicatorStyle: {
    position: "absolute",
    left: spacing.spacing0,
    right: spacing.spacing0,
    top: spacing.spacing0,
    bottom: spacing.spacing0,
    alignItems: "center",
    justifyContent: "center"
  },
  primaryButtonContainer: {
    margin: spacing.spacing16,
    flex: spacing.spacing1,
    justifyContent: "flex-end",
    alignSelf: "stretch"
  },
  text_12_margin: {
    ...TextStyle.text_12_normal,
    marginTop: spacing.spacing14
  },
  loaderContainer: {
    justifyContent: "center",
    position: "absolute",
    top: Dimensions.get('screen').height/ 2.5,
    left: Dimensions.get('screen').width/ 2.2,
  }
});
