import {refreshTabBar} from '../../idfence/constants/actionCreators';
import {connect} from 'react-redux';
import OAWebView from './OAWebView'

const mapStateToProps = state => ({
    refreshNeeded: state.assetsTabReducer.refreshNeeded,
});

const mapDispatchToProps = dispatch => ({
refreshTabBar: (screenNavigator) =>{
    screenNavigator.pop();
    dispatch(refreshTabBar());
}

});

export default connect(mapStateToProps, mapDispatchToProps)(OAWebView);
