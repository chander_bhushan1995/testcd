import { useContext } from "react";
import {EditProfileContext} from "../index.editprofile";


export default function useCommonComponent() {
   return useContext(EditProfileContext)
}
