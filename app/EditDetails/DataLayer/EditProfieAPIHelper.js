import {HTTP_GET, executeApi, HTTP_POST} from '../../network/ApiManager';
import {APIData} from '../../../index';
import {GET_CUST_DETAILS, GET_PINCODE_ADDRESS, UPDATE_CUSTOMER_DETAIL} from "./EditProfileAPIUrls";

const getDataFromApiGateway = (apiParams) => {
    let params = APIData;
    apiParams.apiProps = {baseUrl: params?.api_gateway_base_url, header: params?.apiHeader};
    return apiParams;
};

const custId = () => {
    return APIData?.customer_id;
};

const getPinLabToken = () => {
    return APIData?.pinlab_token;
};


export const getCustomerDetails = (callback) => {
    let urlSubPart = getPinLabToken() ? "token=" + getPinLabToken() : "custId=" + custId();
    let apiParams = {
        httpType: HTTP_GET,
        url: GET_CUST_DETAILS + urlSubPart,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams));
};

export const getPinCodeAddress = (pinCode, callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: GET_PINCODE_ADDRESS(custId(), pinCode),
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams));
};
export const updateCustomerDetails = (customerDetails, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: UPDATE_CUSTOMER_DETAIL,
        requestBody: customerDetails,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

