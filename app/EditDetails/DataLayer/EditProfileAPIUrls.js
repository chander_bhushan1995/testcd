export const GET_CUST_DETAILS = 'myaccount/service/customer/getcustdetail?';
export const UPDATE_CUSTOMER_DETAIL = 'myaccount/api/customer/updatecustdetail';
export const GET_PINCODE_ADDRESS = (customerId, pinCode) => {
    return `myaccount/api/customer/${customerId}/address?pinCode=${pinCode}&addressCount=1`
};
