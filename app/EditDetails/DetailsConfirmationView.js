import React, { Component } from "react";
import { Modal, View, TouchableWithoutFeedback, StyleSheet, TouchableOpacity, Text, Image } from 'react-native'
import { TextStyle, ButtonStyle, CommonStyle } from "../Constants/CommonStyle"
import colors from "../Constants/colors"
import spacing from "../Constants/Spacing";
import { EditProfileConstants } from "./EditProfileUtils";

function UserInfoView(props) {
    const { name, mobileNumber, email, address, button, onClose, onAssistance } = props

    buttonView = () => {
        const { text, onClick = () => { } } = button
        return (<TouchableOpacity style={styles.primaryButton} activeOpacity={.8} onPress={onClick} >
            <Text style={ButtonStyle.primaryButtonText}>{text}</Text>
        </TouchableOpacity>);
    }

    crossView = () => {
        return (<TouchableOpacity onPress={onClose}>
            <Image source={require("../images/icon_cross.webp")} style={CommonStyle.crossIconContainer} />
        </TouchableOpacity>);
    }

    const footerView = () => {
        return (
            <View style={styles.pineLabFooterViewStyle}>
                <Text style={styles.needAssistanceTextStyle}>{EditProfileConstants.NEED_ASSISTANCE_TEXT}</Text>
                <TouchableOpacity onPress={onAssistance}>
                    <Text style={styles.emailTextStyle}>{EditProfileConstants.NEED_ASSISTANCE_ACTION}</Text>
                </TouchableOpacity>
            </View>
        );
    };

    return (
        <View style={styles.container}>
            {crossView()}
            <Text style={[styles.titleLayout]}>Confirm your details</Text>
            <Text style={styles.descLayout}>Please make sure your details are correct.</Text>
            <View style={styles.detailsBox}>
                <View style={{ flexDirection: "row" }}>
                    <Text style={styles.detailTypeTextBox}>Name:</Text>
                    <Text style={styles.detailDataTextBox}>{name}</Text>
                </View>
                <View style={{ flexDirection: "row", marginTop: spacing.spacing20 }}>
                    <Text style={styles.detailTypeTextBox}>Mobile Number:</Text>
                    <Text style={styles.detailDataTextBox}>{mobileNumber}</Text>
                </View>
                <View style={{ flexDirection: "row", marginTop: spacing.spacing20 }}>
                    <Text style={styles.detailTypeTextBox}>Email:</Text>
                    <Text style={styles.detailDataTextBox}>{email}</Text>
                </View>
                <View style={{ flexDirection: "row", marginTop: spacing.spacing20 }}>
                    <Text style={styles.detailTypeTextBox}>Address:</Text>
                    <Text style={styles.detailDataTextBox}>{address}</Text>
                </View>
            </View>
            {buttonView()}
            {footerView()}
        </View>
    );
}

export default class DetailsConfirmationView extends Component {
    state = {
        visible: false,
        name: null,
        mobileNumber: null,
        email: null,
        address: null,
    }

    constructor(props) {
        super(props);
    }

    showDailog(name, mobileNumber, email, address) {
        this.setState({
            visible: true,
            name: name,
            mobileNumber: mobileNumber,
            email: email,
            address: address,
        })
    }

    closePopup() {
        this.setState({
            visible: false,
            name: null,
            mobileNumber: null,
            email: null,
            address: null
        })
    }

    _renderOutsideTouchable() {
        return (
            <TouchableWithoutFeedback onPress={() => this.closePopup()} style={{ flex: 1, width: '100%' }}>
                <View style={{ flex: 1, width: '100%' }} />
            </TouchableWithoutFeedback>
        )
    }

    renderView() {
        return <View style={styles.container}>
            <UserInfoView style={styles.childContainer}
                name={this.state.name}
                mobileNumber={this.state.mobileNumber}
                email={this.state.email}
                address={this.state.address}
                button={{ text: "Confirm Now", onClick: () => {
                    this.closePopup()
                    this.props.onConfirm() 
                }}}
                onAssistance={()=>{
                    this.closePopup()
                    this.props.onAssistance() 
                }}
                onClose={() => this.closePopup()}
                cancellable={false}
            />
        </View>
    }

    render() {
        return (<Modal
            transparent={true}
            visible={this.state.visible}
            onRequestClose={() => this.closePopup()}>
            <View style={[{
                flex: 1,
                backgroundColor: "#000000AA",
                padding: 24
            }]}>
                {this._renderOutsideTouchable()}
                {this.renderView()}
                {this._renderOutsideTouchable()}
            </View>
        </Modal>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        opacity: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    childContainer: {},
    container: {
        backgroundColor: colors.white,
        width: '100%',
        paddingBottom: spacing.spacing16
    },
    titleLayout: {
        ...TextStyle.text_18_bold,
        textAlign: "left",
        marginHorizontal: spacing.spacing22
    },
    descLayout: {
        ...TextStyle.text_14_normal,
        textAlign: "left",
        marginTop: spacing.spacing6,
        marginHorizontal: spacing.spacing22
    },
    detailsBox: {
        marginTop: spacing.spacing32,
        marginHorizontal: spacing.spacing22,
    },
    detailTypeTextBox: {
        ...TextStyle.text_14_normal,
        width: 120,
    },
    detailDataTextBox: {
        ...TextStyle.text_14_normal_charcoalGrey,
        flex: 1,
    },
    primaryButton: {
        ...ButtonStyle.BlueButton,
        marginTop: spacing.spacing24,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
    },
    needAssistanceTextStyle: {
        ...TextStyle.text_16_semibold,
        color: colors.color_808080,
    },
    emailTextStyle: {
        ...TextStyle.text_16_semibold,
        color: colors.color_008DF6,
        paddingTop: spacing.spacing5,
    },
    pineLabFooterViewStyle: {
        marginTop: spacing.spacing18,
        alignItems: 'center'
    }
});