import React, {useState, useEffect, useRef} from 'react';
import {
    NativeModules,
    Platform,
    SafeAreaView,
    Image,
    TouchableOpacity,
    ScrollView,
    Text,
    View
} from 'react-native';
import useBackHandler from '../CardManagement/CustomHooks/useBackHandler';
import {TextStyle} from "../Constants/CommonStyle";
import colors from "../Constants/colors";
import {HeaderBackButton} from 'react-navigation';
import {style} from './EditDetailsStyles';
import LinkButton from "../CustomComponent/LinkButton";
import InputBox from "../Components/InputBox";
import spacing from "../Constants/Spacing";
import CheckBox from 'react-native-check-box';
import paymentFlowStrings from "../PaymentFlow/Constants/Constants";
import styles from "../idfence/dashboard/screens/detailsTab/DetailsTab.style";
import {
    EditProfileConstants,
    isEmailAddressValid,
    mobileNumberRegex,
    userNameRegex,
    warrantyPeriodPostFix,
} from "./EditProfileUtils";
import ButtonWithLoader from "../CommonComponents/ButtonWithLoader";
import InputBoxRightText from "../Components/InputBoxRightText";
import InputBoxDropDown from "../Components/InputBoxDropDown";
import useCommonComponent from "./CustomeHooks/useCommonComponent";
import * as EditProfieAPI from "./DataLayer/EditProfieAPIHelper";
import {APIData} from '../../index';
import {PLATFORM_OS} from "../Constants/AppConstants";
import {formatDateDD_MMM_YYYY} from "../commonUtil/Formatter";
import DetailsConfirmationView from "./DetailsConfirmationView";
import {logWebEnageEvent} from '../commonUtil/WebengageTrackingUtils';

const bridgeRef = NativeModules.ChatBridge;

export default function EditDetailsScreen(props) {
    const {navigation} = props;
    const isPineLab = APIData?.isPinelabLink ?? false;
    const {Alert, BlockingLoader, editProfileState} = useCommonComponent();
    const {customerDetails, error} = editProfileState;
    const customerDetail = customerDetails?.customers?.[0];
    const customerAddress = customerDetail?.addresses?.filter(address => address.addressType === "PER")?.[0] ?? customerDetail?.addresses?.[0];
    const [userName, setUserName] = useState({userName: '', errorMessage: ''});
    const [userMobileNum, setUserMobileNum] = useState({userMobileNum: '', errorMessage: ''});
    const [userEmailAddress, setUserEmailAddress] = useState({userEmailAddress: '', errorMessage: ''});
    const [userPinCode, setUserPinCode] = useState({
        userPinCode: '',
        city: '',
        state: '',
        errorMessage: ''
    });
    const [userAddress, setUserAddress] = useState({userAddress: '', errorMessage: ''});
    const [custId, setCustId] = useState(null);
    const [checkBoxStatus, setCheckBoxStatus] = useState(false);

    const confirmationBoxRef = useRef(null);
    useBackHandler(() => bridgeRef.goBack());

    useEffect(() => {
        if (customerDetail ?? {}) {
            setDefaultUserDetails();
        }
        if (Object.keys(error).length > 0) {
            if (isPineLab) {
                navigateToSuccessScreen(false);
            } else {
                Alert.showAlert("Error", error?.message, {text: 'Ok'});
            }
        }
    }, [customerDetail, error, customerAddress]);

    const setDefaultUserDetails = () => {
        setUserName({
            ...userName,
            userName: customerDetail?.firstName ?? "" + " " + customerDetail?.lastName ?? "",
            errorMessage: ''
        });
        setUserEmailAddress({...userName, userEmailAddress: customerDetail?.emailId, errorMessage: ''});
        setUserMobileNum({
            ...userMobileNum,
            userMobileNum: customerDetail?.mobileNumber?.toString(),
            errorMessage: ''
        });
        setUserPinCode({
            ...userPinCode,
            userPinCode: customerAddress?.pincode?.toString(),
            city: customerAddress?.cityName ?? "",
            state: customerAddress?.stateName ?? "",
            errorMessage: ''
        });
        setUserAddress({...userAddress, userAddress: customerAddress?.addressLine1, errorMessage: ''})
        webEnageEvent(EditProfileConstants.WEBENGAGE_EVENT_NAME_PROFILE);
        setCustId(customerDetail?.custId)

    };

    const webEnageEvent = (eventName) => {
        let eventData = new Object();
        eventData[EditProfileConstants.WEBENGAGE_ATTR_LOCATION] = EditProfileConstants.WEBENGAGE_EVENT_NAME_PROFILE;
        eventData[EditProfileConstants.WEBENGAGE_ATTR_PLAN_NAME] = customerDetails?.planName ?? "";
        eventData[EditProfileConstants.WEBENGAGE_ATTR_PLAN_CODE] = customerDetails?.planCode ?? "";
        eventData[EditProfileConstants.WEBENGAGE_ATTR_BP_NAME] = APIData?.BP_NAME ?? "";
        eventData[EditProfileConstants.WEBENGAGE_ATTR_BU_NAME] = APIData?.BU_NAME ?? "";
        logWebEnageEvent(eventName, eventData);
    }

    const headerView = () => {
        if (isPineLab) {
            return (
                <View style={style.headerViewEditProfile}>
                    <Text style={TextStyle.text_14_normal_charcoalGrey}>{customerDetails?.planName}</Text>
                    <Text style={style.headerViewSubTitleStyle}>Membership
                        Period: {formatDateDD_MMM_YYYY(customerDetails?.membershipStartDate)} - {formatDateDD_MMM_YYYY(customerDetails?.membershipEndDate)}</Text>
                    {customerDetails?.products?.[0]?.category === 'PE' ? headerViewPE() : headerViewHA_EW()}
                </View>
            )
        } else {
            return null
        }
    };

    const headerViewPE = () => {
        let product = customerDetails?.products?.[0];
        const isPE_MP01 = product?.prodCode;
        return (
            <View style={style.headerViewEditProfile}>
                <Text style={style.headerViewSubTitleStyle}>Sum Insured: ₹ {customerDetails?.sumAssured ?? ""}</Text>
                <Text
                    style={style.headerViewSubTitleStyle}>{isPE_MP01 ? "IMEI" : "SN"}: {customerDetails?.products?.[0]?.serialNo}</Text>
            </View>
        )
    };

    function onViewAllButtonClick() {
        navigation.navigate("ProductDetailSheet", {
            params: customerDetails
        })
    }

    const headerViewHA_EW = () => {
        let product = customerDetails?.products?.[0];
        if (product?.warrantyPeriod) {
            return (
                <View>
                    <Text style={style.headerViewSubTitleStyle}>Sum Insured: ₹{customerDetails?.sumAssured ?? ""}</Text>
                    <Text
                        style={style.headerViewSubTitleStyle}>Manufacturer
                        Warranty: {warrantyPeriodPostFix(customerDetails?.products?.[0]?.warrantyPeriod ?? "")}</Text>
                    <Text
                        style={style.headerViewSubTitleStyle}>SN: {customerDetails?.products?.[0]?.serialNo}</Text>
                </View>
            )
        } else {
            return (
                <View>
                    <Text style={style.headerViewSubTitleStyle}>{customerDetails?.products?.[0]?.name},
                        {customerDetails?.products?.[0]?.brand}, {customerDetails?.products?.[0]?.model}</Text>
                    <Text style={style.headerViewSubTitleStyle}>SN: {customerDetails?.products?.[0]?.serialNo}</Text>
                    <Text
                        style={style.headerViewSubTitleStyle}>{customerDetails?.products?.[0]?.technology} |
                        {" " + customerDetails?.products?.[0]?.size} {customerDetails?.products?.[0]?.sizeUnit ?? ""}</Text>
                    <LinkButton
                        style={{actionTextStyle: style.viewAllBtnStyle}}
                        data={{action: EditProfileConstants.ACTION_TITLE_VIEW_ALL}}
                        onPress={() => onViewAllButtonClick()}/>
                </View>
            )
        }

    };

    const nameInputBox = () => {
        return (
            <View style={style.inputBoxContainer}>
                <InputBox
                    maxLength={EditProfileConstants.INPUTBOX_MAX_MIN_LENGTH.MAXLENGTH_200}
                    inputHeading={EditProfileConstants.INPUT_FIELDS_HEADING.USER_NAME}
                    placeholder={""}
                    isMultiLines={false}
                    value={userName.userName === '' ? ' ' : userName.userName}
                    errorMessage={userName.errorMessage}
                    editable={APIData?.isNameEditable ?? false}
                    onChangeText={(text) => {
                        text = text.replace(/\s{2,}/g, ' ');
                        if (userNameRegex.test(text.trim().toString())) {
                            setUserName({...userName, userName: text, errorMessage: ''});
                        }
                    }}
                />
            </View>
        )
    };

    const mobileNumberInputBox = () => {
        return (
            <View style={style.inputBoxContainer}>
                <InputBox
                    maxLength={EditProfileConstants.INPUTBOX_MAX_MIN_LENGTH.MAXLENGTH_10}
                    containerStyle={{paddingTop: spacing.spacing20}}
                    inputHeading={EditProfileConstants.INPUT_FIELDS_HEADING.MOBILE_NUMBER}
                    placeholder={""}
                    isMultiLines={false}
                    autoCorrect={false}
                    contextMenuHidden={true}
                    keyboardType={"numeric"}
                    value={userMobileNum.userMobileNum}
                    errorMessage={userMobileNum.errorMessage}
                    editable={false}
                    onChangeText={setUserMobileNumber}
                />
            </View>
        )
    };

    function setUserMobileNumber(inputText) {
        if (mobileNumberRegex.test(inputText.trim().toString())) {
            setUserMobileNum({...userMobileNum, userMobileNum: inputText.trim(), errorMessage: ''});
            return
        }
        if (inputText === '') {
            setUserMobileNum({...userMobileNum, userMobileNum: ' ', errorMessage: ''});
        }
    }

    const emailInputBoxAndroid = () => {
        return (
            <View style={style.inputBoxContainer}>
                <InputBoxDropDown
                    containerStyle={{paddingTop: spacing.spacing20}}
                    height={38}
                    keyboardType={"email-address"}
                    inputHeading={EditProfileConstants.INPUT_FIELDS_HEADING.EMAIL}
                    placeholder={""}
                    isMultiLines={false}
                    autoCorrect={false}
                    placeholderTextColor={colors.grey}
                    maxLength={EditProfileConstants.INPUTBOX_MAX_MIN_LENGTH.MAXLENGTH_200}
                    value={userEmailAddress.userEmailAddress}
                    editable={false}
                    errorMessage={""}
                    contextMenuHidden={true}
                    onPress={() => {
                        showEmailIDSelectionAlert().then();
                    }}/>
            </View>
        )
    };
    const emailInputBoxiOS = () => {
        return (
            <View style={style.inputBoxContainer}>
                <InputBox
                    containerStyle={{paddingTop: spacing.spacing20}}
                    height={38}
                    keyboardType={"email-address"}
                    inputHeading={EditProfileConstants.INPUT_FIELDS_HEADING.EMAIL}
                    placeholder={""}
                    isMultiLines={false}
                    autoCorrect={false}
                    placeholderTextColor={colors.grey}
                    maxLength={EditProfileConstants.INPUTBOX_MAX_MIN_LENGTH.MAXLENGTH_200}
                    value={userEmailAddress.userEmailAddress}
                    editable={true}
                    errorMessage={userEmailAddress.errorMessage}
                    contextMenuHidden={true}
                    onChangeText={(text) => {
                        setUserEmailAddress({...userEmailAddress, userEmailAddress: text.trim(), errorMessage: ''});
                    }}/>
            </View>
        )
    };

    async function showEmailIDSelectionAlert() {
        let selectedEmailID = await bridgeRef.selectUserEmailID();
        setUserEmailAddress({...userEmailAddress, userEmailAddress: selectedEmailID, errorMessage: ''});
    }

    const pinCodeInputBox = () => {
        return (
            <View>
                <InputBoxRightText
                    maxLength={EditProfileConstants.INPUTBOX_MAX_MIN_LENGTH.MAXLENGTH_6}
                    rightMessage={userPinCode.city + ", " + userPinCode.state}
                    containerStyle={{paddingTop: spacing.spacing20, flex: 1, position: 'relative'}}
                    inputHeading={EditProfileConstants.INPUT_FIELDS_HEADING.PINCODE}
                    placeholder={""}
                    isMultiLines={false}
                    autoCorrect={false}
                    keyboardType={"numeric"}
                    value={userPinCode.userPinCode === '' ? ' ' : userPinCode.userPinCode}
                    errorMessage={userPinCode.errorMessage}
                    editable={true}
                    onChangeText={(text) => {
                        if (/^\d+$/.test(text.trim().toString())) {
                            setUserPinCode({...userPinCode, userPinCode: text.trim(), errorMessage: ''});
                            if (text.trim()?.length == EditProfileConstants.INPUTBOX_MAX_MIN_LENGTH.MAXLENGTH_6) {
                                fetchCityAndState(text);
                            }
                        }
                        if (text === '') {
                            setUserPinCode({...userPinCode, userPinCode: ' ', errorMessage: ''});
                        }
                    }}
                />
            </View>
        )
    };

    const fetchCityAndState = (pinCode) => {
        BlockingLoader.startLoader("Fetching details...");
        EditProfieAPI.getPinCodeAddress(pinCode, (response, error) => {
            BlockingLoader.stopLoader();
            if (response) {
                setUserPinCode({
                    ...userPinCode,
                    userPinCode: pinCode,
                    errorMessage: '',
                    city: response?.data?.cityName ?? "",
                    state: response?.data?.stateName ?? ""
                });
            }
        });
    };

    const addressInputBox = () => {
        return (
            <View style={style.inputBoxContainer}>
                <InputBox
                    maxLength={EditProfileConstants.INPUTBOX_MAX_MIN_LENGTH.MAXLENGTH_500}
                    containerStyle={[style.addressTxtInputContainer]}
                    inputHeading={EditProfileConstants.INPUT_FIELDS_HEADING.ADDRESS}
                    height={100}
                    textAlignVertical={'top'}
                    numberOfLines={EditProfileConstants.INPUTBOX_NUMBEROFLINES.NUMBEROFLINES_20}
                    placeholder={""}
                    isMultiLines={true}
                    autoCorrect={false}
                    keyboardType={"default"}
                    value={userAddress.userAddress}
                    errorMessage={userAddress.errorMessage}
                    editable={true}
                    onChangeText={(text) => setUserAddress({...userAddress, userAddress: text, errorMessage: ''})}
                />
            </View>
        )
    };

    const tncComponent = () => {
        if (isPineLab) {
            return (
                <View>
                    <View style={style.tncViewStyle}>
                        <CheckBox
                            style={style.checkboxStyle}
                            checkedImage={<Image source={require('../images/ic_check_box.webp')}
                                                 style={style.checkboxStyle}/>}
                            unCheckedImage={<Image source={require('../images/ic_check_box_outline_blank.webp')}
                                                   style={style.checkboxStyle}/>}
                            isChecked={checkBoxStatus}
                            onClick={() => setCheckBoxStatus(!checkBoxStatus)}
                        />
                        <View style={{flexDirection: 'row'}}>
                            <Text style={[TextStyle.text_12_bold]} onPress={() => setCheckBoxStatus(!checkBoxStatus)}> I
                                agree with the </Text>
                            <Text style={[TextStyle.text_12_normal, style.tncTextStyle]} onPress={navigateToTnCScreen}>terms
                                and conditions</Text>
                        </View>
                    </View>
                    <View style={{paddingVertical: spacing.spacing10}}>
                        {EditProfileConstants.PINELAB_TNC_POINTS.map(renderItem)}
                    </View>
                </View>
            );
        } else {
            return null
        }
    };

    const renderItem = (item, index) => {
        return (
            <View index={index} style={style.tncPointRowStyle}>
                <Text style={style.blackCircle}/>
                <Text style={[styles.tncPointStyle]}
                      numberOfLines={EditProfileConstants.INPUTBOX_NUMBEROFLINES.NUMBEROFLINES_3}
                      ellipsizeMode='tail'>
                    {item}
                </Text>
            </View>
        );
    };

    const footerView = () => {
        if (isPineLab) {
            return (
                <View style={style.pineLabFooterViewStyle}>
                    <Text style={style.needAssistanceTextStyle}>{EditProfileConstants.NEED_ASSISTANCE_TEXT}</Text>
                    <TouchableOpacity onPress={() => bridgeRef.openChat()}>
                        <Text style={style.emailTextStyle}>{EditProfileConstants.NEED_ASSISTANCE_ACTION}</Text>
                    </TouchableOpacity>
                </View>
            );
        } else {
            return null;
        }
    };

    const validateInputFields = () => {
        return ((isPineLab ? checkBoxStatus : true) &&
            userName.userName?.trim()?.length > EditProfileConstants.INPUTBOX_MAX_MIN_LENGTH.MINLENGTH_3 &&
            userMobileNum.userMobileNum?.trim()?.length == EditProfileConstants.INPUTBOX_MAX_MIN_LENGTH.MAXLENGTH_10 &&
            userPinCode.userPinCode?.trim()?.length == EditProfileConstants.INPUTBOX_MAX_MIN_LENGTH.MAXLENGTH_6 &&
            userAddress.userAddress?.trim()?.length > EditProfileConstants.INPUTBOX_MAX_MIN_LENGTH.MINLENGTH_10)
    }

    const primaryButtonComponent = () => {
        const title = isPineLab ? EditProfileConstants.ACTION_TITLE_CONTINUE : EditProfileConstants.ACTION_TITLE_SAVE_DETAILS
        return (
            <View style={style.primaryButtonContainer}>
                <ButtonWithLoader
                    isLoading={false}
                    enable={validateInputFields()}
                    Button={{
                        text: title,
                        onClick: () => {
                            if (!isEmailAddressValid(userEmailAddress.userEmailAddress)) {
                                setUserEmailAddress({
                                    ...userEmailAddress,
                                    errorMessage: EditProfileConstants.ENTER_VALID_EMAIL_ADDRESS
                                });
                                return;
                            }
                            if (isPineLab) {
                                confirmationBoxRef.current.showDailog(
                                    userName.userName,
                                    userMobileNum.userMobileNum,
                                    userEmailAddress.userEmailAddress,
                                    userAddress.userAddress + ", " + userPinCode.userPinCode + ", " + userPinCode.city + ", " + userPinCode.state
                                )
                            } else {
                                updateCustomerDetails();
                            }
                        },
                    }}
                />
            </View>
        );
    };

    function updateCustomerDetails() {
        BlockingLoader.startLoader("Updating details...");
        let customerInfoJson = {
            addresses: [{
                addressId: customerAddress?.addressId,
                addressLine1: userAddress.userAddress,
                pincode: userPinCode.userPinCode,
                city: userPinCode.city,
                state: userPinCode.state
            }],
            custId: custId,
            mobileNumber: userMobileNum.userMobileNum,
            emailId: userEmailAddress.userEmailAddress,
        };
        if (APIData?.isNameEditable) {
            customerInfoJson.firstName = userName.userName?.trim();
            customerInfoJson.lastName = '';
        }
        let updateRequest = {
            customers: [customerInfoJson],
            modifier: (Platform.OS === PLATFORM_OS.ios ? "iOS App" : "Android App"),
            isEmailVerificationNeeded: true
        };
        if (customerDetails?.memId && customerDetails?.memId !== 0) {
            updateRequest.memId = customerDetails?.memId;
        }
        EditProfieAPI.updateCustomerDetails(updateRequest, (response, error) => {
            BlockingLoader.stopLoader();
            if (response) {
                bridgeRef.sendCustomerDetailsUpdatedEvent();
                if (customerDetail?.emailId !== userEmailAddress.userEmailAddress) {
                    bridgeRef.setVerifyEmailDismissed(false)
                }
                if (isPineLab) {
                    webEnageEvent(EditProfileConstants.WEBENGAGE_EVENT_NAME_COMPLETE_PROFILE);
                    navigateToSuccessScreen(true);
                } else {
                    webEnageEvent(EditProfileConstants.WEBENGAGE_EVENT_NAME_COMPLETE_PROFILE);
                    Alert.showAlert('Success',
                        EditProfileConstants.MSG_DETAILS_UPDATED_SUCCESSFULLY, {
                            text: 'Ok',
                            onClick: () => bridgeRef.goBack(),
                        });
                }
            } else {
                Alert.showAlert("Error", error?.error?.[0]?.message ?? error?.message, {text: 'Ok'});
            }
        })
    }

    function navigateToTnCScreen() {
        navigation.navigate('TnCWebViewComponent', {
            url: paymentFlowStrings.productTermAndCUrl,
            headerTitle: 'T&C',
        });
    };

    function navigateToSuccessScreen(isSuccess) {
        navigation.navigate('EditProfileSuccessScreen', {
            url: paymentFlowStrings.productTermAndCUrl,
            headerTitle: 'Verify Your Details',
            isSuccess: isSuccess,
            custId: APIData?.customer_id
        });
    };

    return (
        <SafeAreaView style={style.safeAreaViewStyle}>
            <ScrollView style={style.container}>
                <View style={style.container}>
                    {headerView()}
                    <View style={{paddingTop: spacing.spacing12, paddingHorizontal: spacing.spacing16}}>
                        {nameInputBox()}
                        {mobileNumberInputBox()}
                        {Platform.OS === PLATFORM_OS.ios ? emailInputBoxiOS() : emailInputBoxAndroid()}
                        {pinCodeInputBox()}
                        {addressInputBox()}
                        {tncComponent()}
                        {primaryButtonComponent()}
                        {footerView()}
                        <DetailsConfirmationView
                            ref={confirmationBoxRef}
                            onConfirm={updateCustomerDetails}
                            onAssistance={() => bridgeRef.openChat()}
                        />
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}


EditDetailsScreen.navigationOptions = () => {
    return {
        headerStyle: {borderBottomWidth: 0, elevation: 6},
        headerTitle: <View style={Platform.OS === 'ios' ? {alignItems: 'center'} : {alignItems: 'flex-start'}}>
            <Text style={TextStyle.text_16_bold}>{EditProfileConstants.TOOLBAR_TITLE_EDIT_PROFILE}</Text>
        </View>,
        headerLeft: <HeaderBackButton tintColor={colors.black} onPress={() => bridgeRef.goBack()}/>,
        headerTitleStyle: {color: colors.white},
        headerTintColor: colors.white,
        headerRight: <TouchableOpacity style={{marginTop: 0, flexDirection: 'row', marginRight: 16}}
                                       onPress={() => bridgeRef.openChat()}>
            <Image style={styles.chatIconStyle} source={require("../images/icon_chat_blue.webp")}/>
        </TouchableOpacity>
    };
};
