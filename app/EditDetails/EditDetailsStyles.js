import {StyleSheet} from "react-native";
import colors from "../Constants/colors"
import spacing from "../Constants/Spacing";
import fontSize from "../Constants/FontSizes";
import {TextStyle} from "../Constants/CommonStyle";
import {OATextStyle} from "../Constants/commonstyles";

export const style = StyleSheet.create({
    safeAreaViewStyle:{
        flex: 1, backgroundColor: colors.white
    },
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: colors.white
    },
    headerTitle: {
        fontSize: fontSize.fontSize16,
        fontFamily: "Lato",
        color: colors.charcoalGrey
    },
    headerViewEditProfile: {
        backgroundColor: colors.color_F5F5F5,
        padding: spacing.spacing16,
        flex: 1,
        flexDirection: "column",
    },
    inputBoxContainer: {
        backgroundColor: colors.color_FFFFFF,
        flex: 1,
        flexDirection: "column",
    },
    inputBoxTitleStyle: {
        ...TextStyle.text_12_normal,
        padding: spacing.spacing16,
        color: colors.color_808080,
    },
    headerViewSubTitleStyle: {
        ...TextStyle.text_12_normal,
        color: colors.color_757575,
        paddingVertical: spacing.spacing2
    },
    viewAllBtnStyle: {
        ...OATextStyle.text_12_normal,
        color: colors.color_0282F0,
    },

    addressTxtInputContainer: {
        marginTop: spacing.spacing16,
        textAlignVertical: 'top',
    },
    tncViewStyle: {
        flexDirection: 'row',
        marginTop: spacing.spacing16,
        //marginLeft: spacing.spacing16,

    },
    termsContainer: {
        padding: spacing.spacing16,
    },
    checkboxStyle: {
        width: spacing.spacing20,
        height: spacing.spacing20,
        marginRight: spacing.spacing4,
    },
    tncTextStyle: {
        marginLeft: spacing.spacing4,
        marginRight: spacing.spacing4,
        color: colors.blue028,
    },
    blackCircle: {
        width: 3,
        height: 3,
        borderRadius: 3 / 2,
        backgroundColor: colors.color_808080,
        marginRight: 10,
    },
    tncPointStyle: {
        ...OATextStyle.text_14_normal,
        paddingHorizontal: 6,
        color: colors.color_808080,
    },
    tncPointRowStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: spacing.spacing5
    },
    primaryButtonContainer: {
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        marginBottom: spacing.spacing16,
        justifyContent: 'flex-end',
        alignSelf: 'stretch',
    },
    needAssistanceTextStyle: {
        ...OATextStyle.text_16_semibold,
        color: colors.color_808080,
    },
    emailTextStyle: {
        ...OATextStyle.text_16_semibold,
        color: colors.color_008DF6,
        paddingTop: spacing.spacing5,
        paddingBottom: spacing.spacing10
    },
    pineLabFooterViewStyle: {
        flexDirection: 'column',
        alignItems: 'center'
    },
    emailTxtInputContainer: {
        height:48,
    }
});
