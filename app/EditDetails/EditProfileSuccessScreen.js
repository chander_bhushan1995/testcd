import React from 'react';
import {
    NativeModules,
    SafeAreaView,
    Text,
    Image,
    Platform,
    StyleSheet,
    TouchableOpacity,
    View
} from 'react-native';
import spacing from "../Constants/Spacing";
import colors from "../Constants/colors";
import fontSize from "../Constants/FontSizes";
import { TextStyle } from "../Constants/CommonStyle";
import { EditProfileConstants } from "./EditProfileUtils";
import { HeaderBackButton } from 'react-navigation';
import call from 'react-native-phone-call';
import ButtonWithLoader from '../CommonComponents/ButtonWithLoader';
import useBackHandler from '../CardManagement/CustomHooks/useBackHandler';
const chatBrigeRef = NativeModules.ChatBridge;

const bridgeRef = NativeModules.ChatBridge;

export default function EditProfileSuccessScreen(props) {
    const { navigation } = props;
    const isSuccess = navigation.getParam('isSuccess', false);
    const custId = navigation.getParam('custId', false);
    const mobileNumber = "1800 123 3330"

    useBackHandler(() => bridgeRef.goBack())

    return (
        <SafeAreaView style={styles.safeAreaViewStyle}>
            {isSuccess
                ? <View style={styles.container}>
                    <Image source={require('../images/checkboxOutline.webp')} style={styles.iconStyle} />
                    <Text style={styles.titleStyle}>Details Updated</Text>
                    <Text style={styles.subtitleStyle}>Your OneAssist account details have been updated. If you have not requested for this change, kindly connect with our service experts.</Text>
                    <View style={styles.buttonStyle}>
                        <ButtonWithLoader
                            isLoading={false}
                            enable={true}
                            Button={{
                                text: "View Membership",
                                onClick: () => chatBrigeRef.openMemTab()
                            }}
                        />
                    </View>
                </View>
                : <View style={styles.container}>
                    <Image source={require('../images/checkboxOutlineFail.webp')} style={styles.iconStyle} />
                    <Text style={[styles.titleStyle, { color: colors.inoutErrorMessage }]}>Link Expired</Text>
                    <Text style={styles.subtitleStyle}>
                        <Text>Please call us at </Text>
                        <Text style={styles.mobileNumberStyle} onPress={() => call({ number: mobileNumber, prompt: false })}>{mobileNumber}</Text>
                        <Text> for any assistance.</Text>
                    </Text>
                    <View style={styles.buttonStyle}>
                        <ButtonWithLoader
                            isLoading={false}
                            enable={true}
                            Button={{
                                text: "Ok, Got It",
                                onClick: () => bridgeRef.goBack(),
                            }}
                        />
                    </View>
                </View>
            }
        </SafeAreaView>
    );
}

EditProfileSuccessScreen.navigationOptions = () => {
    return {
        headerStyle: { borderBottomWidth: 0, elevation: 6 },
        headerTitle: <View style={Platform.OS === 'ios' ? { alignItems: 'center' } : { alignItems: 'flex-start' }}>
            <Text style={TextStyle.text_16_bold}>{EditProfileConstants.TOOLBAR_TITLE_VERIFY_YOUR_DETAILS}</Text>
        </View>,
        headerLeft: <HeaderBackButton tintColor={colors.black} onPress={() => bridgeRef.goBack()} />,
        headerTitleStyle: { color: colors.white },
        headerTintColor: colors.white,
        headerRight: <TouchableOpacity style={{ marginTop: 0, flexDirection: 'row', marginRight: 16 }}
            onPress={() => bridgeRef.openChat()}>
            <Image style={styles.chatIconStyle} source={require("../images/icon_chat_blue.webp")} />
        </TouchableOpacity>
    };
};

const styles = StyleSheet.create({
    safeAreaViewStyle: {
        flex: 1, backgroundColor: colors.white
    },
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: colors.white
    },
    iconStyle: {
        marginTop: spacing.spacing64,
        width: spacing.spacing86,
        height: spacing.spacing86,
        alignSelf: 'center',
    },
    titleStyle: {
        ...TextStyle.text_18_bold,
        marginTop: spacing.spacing40,
        alignSelf: "center"
    },
    subtitleStyle: {
        ...TextStyle.text_14_normal,
        marginTop: spacing.spacing18,
        marginHorizontal: spacing.spacing20,
        alignSelf: "center",
        textAlign: "center",
    },
    headerTitle: {
        fontSize: fontSize.fontSize16,
        fontFamily: "Lato",
        color: colors.charcoalGrey
    },
    buttonStyle: {
        width: '100%',
        marginTop: spacing.spacing26,
        backgroundColor: colors.white,
        padding: spacing.spacing16,
        alignSelf: 'flex-end',
    },
    mobileNumberStyle: {
        ...TextStyle.text_14_semibold,
        color: colors.blue028,
    },
});
