import {formatDateDD_MMM_YYYY} from "../commonUtil/Formatter";

export const EditProfileConstants = {
    NEED_ASSISTANCE_TEXT: "Need assistance?",
    NEED_ASSISTANCE_ACTION: "Chat With Us",
    PINELAB_TNC_POINTS: ["Claim should be intimated within 48 hrs of incident",
        "Device Purchase Invoice Mandatory for claim processing",
        "Excess charges levied for approved claims",
        "Cash Settlement applicable if repairs cost exceeds 80% of sum insured (Beyond Economic Repair)",
        "In Beyond Economic Repairs settlement depreciation would be applicable"],

    ACTION_TITLE_VIEW_ALL: 'View All',
    ACTION_TITLE_SAVE_DETAILS: 'Save Details',
    ACTION_TITLE_CONTINUE: 'Continue',
    TOOLBAR_TITLE_EDIT_PROFILE: 'Edit profile',
    TOOLBAR_TITLE_VERIFY_YOUR_DETAILS: 'Verify Your Details',
    ENTER_VALID_EMAIL_ADDRESS: 'Enter valid email address',

    FETCHING_USER_DETAILS: "Fetching user details",
    MSG_DETAILS_UPDATED_SUCCESSFULLY: "Details updated successfully!",
    MSG_UNABLE_TO_UPDATE_DETAILS: "Unable to update details.try again!",

    WEBENGAGE_EVENT_NAME_COMPLETE_PROFILE: 'Complete Profile',
    WEBENGAGE_EVENT_NAME_PROFILE: 'Profile',
    WEBENGAGE_ATTR_LOCATION: 'Location',
    WEBENGAGE_ATTR_VALUE_P_DETAILS_UPDATE: 'Personal Details update',

    WEBENGAGE_ATTR_PLAN_NAME: 'Plan name',
    WEBENGAGE_ATTR_PLAN_CODE: 'Plan code',
    WEBENGAGE_ATTR_BU_NAME: 'BU name',
    WEBENGAGE_ATTR_BP_NAME: 'BP name',

    FLOW_TYPE_EKIT: "EKIT",
    FLOW_TYPE_PINLAB: "PINE_LAB",

    INPUT_FIELDS_HEADING: {
        USER_NAME: 'Full Name (Must match Govt ID and Invoice)',
        MOBILE_NUMBER: 'Mobile Number',
        EMAIL: 'Email',
        PINCODE: 'Pincode',
        ADDRESS: 'Address',
        ENTER_EMAIL: 'Enter Email (Optional)',
        ENTER_NAME : 'Enter Name'
    },
    INPUTBOX_MAX_MIN_LENGTH: {
        MAXLENGTH_200: 200,
        MAXLENGTH_10: 10,
        MAXLENGTH_6: 6,
        MAXLENGTH_500: 500,
        MINLENGTH_3: 3,
        MINLENGTH_10: 10,
    },
    INPUTBOX_NUMBEROFLINES: {
        NUMBEROFLINES_20: 20,
        NUMBEROFLINES_3: 3
    },
    ERROR_CODE_ARRAY: [
        "ACC_000039",
        "ACC_000034"
    ]
}
export const isEmailAddressValid = (e) => {
    return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(e.toString());
};

export const mobileNumberRegex = /^[1-9][0-9]*$/;
export const userNameRegex = /^[a-zA-Z ]*$/;

export const warrantyPeriodPostFix = (warrantyPeriod) => {
    let year = warrantyPeriod / 12;
    let month = warrantyPeriod % 12;
    let warrantyPeriodLabel = "";
    if (year > 1) {
        warrantyPeriodLabel = year + "Years ";
    } else if (year == 1) {
        warrantyPeriodLabel = year + "Year ";
    }

    if (month > 1) {
        warrantyPeriodLabel = warrantyPeriodLabel + month + "Months ";
    } else if (month == 1) {
        warrantyPeriodLabel = warrantyPeriodLabel + month + "Month ";
    }

    return warrantyPeriodLabel;
};
