import {createStackNavigator} from 'react-navigation';

import EditDetailScreen from './EditDetailsScreen';
import EditProfileSuccessScreen from './EditProfileSuccessScreen';
import Dialog from '../Components/DialogView';
import TnCWebViewComponent from '../PaymentFlow/Components/TnCWebViewComponent';
import ProductDetailSheet from './ProductDetailSheet/ProductDetailSheet'

const navigator = createStackNavigator({
    EditDetailScreen: {
        screen: EditDetailScreen,
    },
    TnCWebViewComponent: {
        screen: TnCWebViewComponent,
    },
    EditProfileSuccessScreen: {
        screen: EditProfileSuccessScreen,
    }
}, {});

const navigator1 = createStackNavigator({
    Main: {
        screen: navigator,
    },
    ProductDetailSheet: {
        screen: ProductDetailSheet,
    },
    Dialog: {
        screen: Dialog,
    },
}, {
    mode: 'modal',
    headerMode: 'none',
    initialRouteName: 'Main',
});

export default navigator1;
