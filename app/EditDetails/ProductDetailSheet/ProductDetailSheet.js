import React from 'react';
import {
    SafeAreaView,
    Image,
    TouchableOpacity,
    ScrollView,
    StyleSheet,
    Text,
    View
} from 'react-native';
import useBackHandler from '../../CardManagement/CustomHooks/useBackHandler';
import { TextStyle } from "../../Constants/CommonStyle";
import colors from "../../Constants/colors";
import spacing from "../../Constants/Spacing";

import { dateFromTime, formattedDate } from './../../CardManagement/CardUtils';

export default function ProductDetailSheet(props) {
    const { navigation } = props;
    const custDetails = navigation.getParam('params', null);

    useBackHandler(() => navigation.pop())

    const products = () => {
        var memPeriod = ""
        const start = dateFromTime(custDetails?.membershipStartDate);
        const end = dateFromTime(custDetails?.membershipEndDate);
        if (start && end) {
            const formattedStartDate = formattedDate(start, 'dd mmm yyyy');
            const formattedEndDate = formattedDate(end, 'dd mmm yyyy');
            memPeriod = "Membership Period: " + formattedStartDate + " - " + formattedEndDate
        }

        let sumInsured = "Sum Insured: Rs " + (custDetails?.sumAssured ?? "")
        return custDetails?.products?.map((product) => {
            var arrDetails = []
            var tech = [product?.technology, product?.brand, product?.product].filter(value => value?.length > 0).join(", ")
            let serialNo = "SN " + (product?.serialNo ?? "")
            let size = (product.size ?? "") + " " + (product.sizeUnit ?? "")
            switch (product.category) {
                case "PE":
                    arrDetails = [tech + " IMEI: " + (product.serialNo ?? ""), sumInsured, memPeriod]
                    break
                case "HA":
                    let warrantyPeriod = product.warrantyPeriod
                    let productName = product?.name

                    if (productName) {
                        tech = productName + ", " + tech
                    }
                    if (warrantyPeriod) {
                        let wpText = "Manufacturer warranty: " + warrantyPeriod + "Months"
                        arrDetails = [tech, serialNo, sumInsured, wpText, memPeriod]
                    } else {
                        arrDetails = [memPeriod, tech, serialNo, size]
                    }
                    break
                default:
                    print("Handle this case")
                    break
            }
            return arrDetails
        })
    }

    return (
        <SafeAreaView style={styles.safeArea}>
            <TouchableOpacity style={styles.crossTouch} onPress={() => navigation.pop()}>
                <Image style={styles.crossIcon} source={require("../../images/icon_cross.webp")} />
            </TouchableOpacity>
            <Text style={styles.headerTitle}>Your Products</Text>
            <ScrollView>
                {products()?.map((productItem, index) => {
                    return (<View>
                        <View style={styles.cellView} key={index} >
                            {productItem.map((descriptionItem, index) => {
                                return <Text key={index} style={styles.textView}>{descriptionItem}</Text>
                            })}
                        </View>
                        <View style={styles.lineView} />
                    </View>)
                })}
            </ScrollView>
        </SafeAreaView>
    );
}

export const styles = StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: colors.white
    },
    headerTitle: {
        ...TextStyle.text_14_normal,
    },
    crossTouch: {
        width: 48,
        height: 48,
        marginTop: spacing.spacing4,
        padding: spacing.spacing16,
        alignSelf: "flex-start"
    },
    crossIcon: {
        width: 16,
        height: 16,
        tintColor: colors.darkGrey,
        resizeMode: "contain"
    },
    headerTitle: {
        ...TextStyle.text_18_bold,
        marginTop: spacing.spacing24,
        marginHorizontal: spacing.spacing16
    },
    headerSubtitle: {
        ...TextStyle.text_16_normal,
        marginTop: spacing.spacing8,
        marginBottom: spacing.spacing20,
        marginHorizontal: spacing.spacing16
    },
    cellView: {
        padding: spacing.spacing20
    },
    textView: {
        ...TextStyle.text_12_normal,
        marginBottom: spacing.spacing4
    },
    lineView: {
        flexDirection: 'row',
        borderBottomColor: colors.color_E0E0E0,
        borderBottomWidth: 2,
        alignSelf: 'stretch',
        height: 2,
    }
});

