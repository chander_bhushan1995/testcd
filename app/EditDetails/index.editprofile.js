import React, {useRef, useReducer, useEffect} from 'react';
import AppNavigator from './NavigationStack';
import useLoader from '../CardManagement/CustomHooks/useLoader';
import BlockingLoader from '../CommonComponents/BlockingLoader';
import DialogView from '../Components/DialogView';
import Toast, {DURATION} from '../CommonComponents/Toast';
import colors from '../Constants/colors';
import {TextStyle} from '../Constants/CommonStyle';

import * as EditProfieAPI from '../EditDetails/DataLayer/EditProfieAPIHelper';
import * as Actions from '../EditDetails/DataLayer/Actions';
import {EditProfileConstants} from "./EditProfileUtils";
import codePush from "react-native-code-push";
import {setAPIData, updateAPIData} from "../../index";

export const EditProfileContext = React.createContext();

const initialEditProfileState = {
    isInitialLoad: false,
    customerDetails: [],
    error: {},
};

const reducer = (state = initialEditProfileState, action) => {
    switch (action.type) {
        case Actions.ACTION_GET_CUST_DETAILS:
            return {
                ...state,
                customerDetails: action.data ?? [],
                error: action.error ?? {},
                isInitialLoad: action.isInitialLoad ?? false,
            };
        default:
            return state;
    }
};

let Root: () => React$Node = props => {
    setAPIData(props);

    console.ignoredYellowBox = ['Warning:'];
    console.disableYellowBox = true;

    const [isBlockingLoader, blockingLoaderMessage, startBlockingLoader, stopBlockingLoader] = useLoader();

    const toastRef = useRef(null);
    const showToast = (message, timeout = DURATION.LENGTH_SHORT) => toastRef.current.show(message, timeout, true);

    const alertRef = useRef(null);
    const showAlert = (title, alertMessage, primaryButton = {
        text: 'Yes', onClick: () => {
        }
    }, secondaryButton, checkBox) => {
        alertRef.current.showDailog({
            title: title,
            message: alertMessage,
            primaryButton: primaryButton,
            secondaryButton: secondaryButton,
            checkBox: checkBox,
            cancelable: true,
            onClose: () => {
            },
        });
    };

    const [editProfileState, editProfileDispatch] = useReducer(reducer, initialEditProfileState);

    useEffect(() => {
        startBlockingLoader(EditProfileConstants.FETCHING_USER_DETAILS);
        getCustomerDetails(true);
    }, []);

    useEffect(() => {
        updateAPIData(props)
    }, [props])

    const getCustomerDetails = (isInitialLoad) => {
        EditProfieAPI.getCustomerDetails((response, error) => {
            stopBlockingLoader();
            if (response) {
                editProfileDispatch({
                    type: Actions.ACTION_GET_CUST_DETAILS,
                    data: response?.data,
                    isInitialLoad: isInitialLoad,
                });
            } else {
                let errorObj = error?.error[0];
                if (EditProfileConstants.ERROR_CODE_ARRAY.includes(errorObj?.errorCode)) {
                    editProfileDispatch({
                        type: Actions.ACTION_GET_CUST_DETAILS,
                        error: errorObj,
                        isInitialLoad: isInitialLoad,
                    });
                }else {
                    showAlert(errorObj?.errorCode, errorObj?.message, {text: "Ok"})
                }
            }
        });
    };
    return (
        <EditProfileContext.Provider value={{
            Toast: {showToast: showToast},
            Alert: {showAlert: showAlert},
            BlockingLoader: {startLoader: startBlockingLoader, stopLoader: stopBlockingLoader},
            editProfileState: editProfileState,
            editProfileDispatch: editProfileDispatch
        }}>
            <BlockingLoader visible={isBlockingLoader} loadingMessage={blockingLoaderMessage}/>
            <AppNavigator/>
            <Toast ref={toastRef}
                   style={{backgroundColor: colors.color_404040}}
                   position='bottom'
                   textStyle={{...TextStyle.text_14_bold, color: colors.white}}
            />
            <DialogView ref={alertRef}/>
        </EditProfileContext.Provider>
    );
}

export default codePush(Root);