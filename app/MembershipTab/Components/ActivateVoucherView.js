import React from "react";
import { StyleSheet, TouchableOpacity, Text, View } from "react-native";
import spacing from "../../Constants/Spacing";
import { TextStyle } from "../../Constants/CommonStyle";
import colors from "../../Constants/colors";
import { ACTIVATE_NOW, HAVE_ACTIVATION_CODE } from "../../Constants/AppConstants";


const ActivateVoucherView = props => {

    const onActivateVoucher = props.onActivateVoucher ?? (() => {
    });

    return (
        <View style={styles.shadowContainer}>
            <TouchableOpacity activeOpacity={1} style={styles.container} onPress={onActivateVoucher}>
                <Text style={styles.heading}>{HAVE_ACTIVATION_CODE}</Text>
                <Text style={styles.buttonText}>{ACTIVATE_NOW}</Text>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    shadowContainer:{
        paddingVertical: spacing.spacing8,
        paddingHorizontal: spacing.spacing16,
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity:  0.4,
        shadowRadius: 3,
        elevation: 5,
        backgroundColor: colors.white
    },
    container: {
        backgroundColor: colors.color_E5F2FD,
        padding: spacing.spacing8,
        flexDirection: "row",
        justifyContent: "flex-end",
        alignItems: "center",
        borderRadius: 4,
    },
    heading: {
        ...TextStyle.text_12_regular,
        color: colors.charcoalGrey,
        flex: 1,
    },
    buttonText: {
        ...TextStyle.text_14_bold,
        color: colors.blue028,
    },
});

export default ActivateVoucherView;
