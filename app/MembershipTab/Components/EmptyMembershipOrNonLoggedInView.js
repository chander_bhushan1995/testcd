import React from "react";
import { Text, View, StyleSheet } from "react-native";
import ButtonWithLoader from "../../CommonComponents/ButtonWithLoader";
import spacing from "../../Constants/Spacing";
import { TextStyle } from "../../Constants/CommonStyle";


const EmptyMembershipOrNonLoggedInView = props => {

    const message = props?.message;
    const buttonText = props?.buttonText;
    const primaryButtonAction = props?.primaryButtonAction ?? (() => {
    });

    return (
        <View style={[styles.container, props.style?.container]}>
            <Text style={styles.message}>{message}</Text>
            <View style={styles.buttonContainer}>
                <ButtonWithLoader
                    isLoading={false}
                    enable={true}
                    Button={{
                        text: buttonText,
                        onClick:()=>{
                            primaryButtonAction()
                        },
                    }} />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    message: {
        ...TextStyle.text_20_bold,
        lineHeight: 24,
        textAlign: 'center',
        marginHorizontal: spacing.spacing16,
    },
    buttonContainer: {
        width: "100%",
        marginTop: spacing.spacing32,
        paddingHorizontal: spacing.spacing16,
    },
});

export default EmptyMembershipOrNonLoggedInView;