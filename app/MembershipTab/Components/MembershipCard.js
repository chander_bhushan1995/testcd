import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { CommonStyle, TextStyle } from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import MembershipStatusViewContainer from "./MembershipStatusViews/MembershipStatusViewContainer";
import MembershipCardActionContainer from "./MembershipCardActionViews/MembershipCardActionContainer";
import TagComponent from "../../Catalyst/components/common/TagsComponent";
import { handleMembershipBtnActions } from "../Helpers/MembershipButtonActions";


const MembershipCard = props => {

    const data = props?.data;
    const heading = data?.heading ?? "";
    const image = data?.image ? (typeof data?.image === "number" ? data?.image : { uri: data?.image }) : null;
    const subHeading = data?.subHeading; //{text: "",style:{}};
    const description = data?.description; //?? { text: "", tappableText: { text: "", action: "" } };
    const note = data?.note;
    const statusViews = data?.statusViews;
    const actionViews = data?.actionViews;
    const tag = data?.tag;

    return (
        <View style={styles.card}>
            <View style={{ overflow: "hidden", borderRadius: 4 }}>
                <View style={styles.headerContainer}>
                    <View style={{ flex: 1 }}>
                        <Text style={styles.heading}>{heading}</Text>
                        {subHeading ?
                            <Text style={[styles.subHeading, subHeading?.style]}>{subHeading?.text}</Text> : null}
                    </View>

                    {image
                        ? <Image style={styles.planImage} source={image} />
                        : null
                    }
                </View>

                {"" ? <View style={{ backgroundColor: "red", marginVertical: 10, height: 10 }}></View> : null}

                {description || note?.text
                    ? <View style={{ marginHorizontal: spacing.spacing16, marginTop: spacing.spacing8 }}>
                        {description
                            ? (description?.tappableText?.text
                                ? <Text style={styles.description}>{description?.text}
                                    <Text style={styles.description} onPress={() => {
                                        handleMembershipBtnActions(description?.tappableText?.action, {
                                            ...props.extraProps,
                                            membershipResponse: data?.membershipResponse,
                                            data: description?.tappableText?.data,
                                        });
                                    }}>
                                        {description?.tappableText?.text}
                                    </Text>
                                </Text>
                                : <Text style={styles.description}>{description?.text}</Text>)
                            : null
                        }

                        {note?.text
                            ? <Text style={[styles.description, note?.style]}>{note?.text}</Text>
                            : null
                        }
                    </View>
                    : null
                }
                {tag
                    ? <TagComponent tags={[tag?.text]}
                                    style={styles.tagContainerStyle}
                                    singleTagStyle={[styles.singleTagStyle, {
                                        borderColor: tag?.borderColor ?? colors.saffronYellow,
                                        backgroundColor: tag?.bgColor ?? colors.transparent,
                                    }]}
                                    textStyle={{
                                        ...TextStyle.text_10_bold,
                                        color: tag?.textColor ?? colors.saffronYellow,
                                    }} />
                    : null
                }

                {statusViews
                    ? <View style={{ marginBottom: (Array.isArray(actionViews) && actionViews.length > 0) ? spacing.spacing0 : spacing.spacing16 }}>
                        <MembershipStatusViewContainer statusViews={statusViews} extraProps={{
                            ...props.extraProps,
                            membershipResponse: data?.membershipResponse,
                        }} />
                    </View>
                    : null
                }

                {Array.isArray(actionViews) && actionViews.length > 0
                    ? <View style={{ marginTop: spacing.spacing16 }}>
                        <MembershipCardActionContainer cardActionViews={actionViews} extraProps={{
                            ...props.extraProps,
                            membershipResponse: data?.membershipResponse,
                        }} />
                    </View>
                    : null}
            </View>
        </View>
    );
};


const styles = StyleSheet.create({
    card: {
        ...CommonStyle.cardContainerWithShadow,
        marginHorizontal: spacing.spacing16,
    },
    headerContainer: {
        flexDirection: "row",
        marginTop: spacing.spacing16,
        marginHorizontal: spacing.spacing16,
        justifyContent: "space-between",
    },
    heading: {
        ...TextStyle.text_16_bold,
    },
    subHeading: {
        ...TextStyle.text_12_regular,
    },
    description: {
        ...TextStyle.text_12_regular,
        color: colors.grey,
    },
    planImage: {
        height: 44,
        width: 44,
        resizeMode: "contain",
        marginLeft: spacing.spacing24,
    },
    tagContainerStyle: {
        marginHorizontal: spacing.spacing16,
        marginTop: spacing.spacing16,
    },
    singleTagStyle: {
        borderWidth: 1,
        paddingHorizontal: spacing.spacing8,
        paddingVertical: spacing.spacing3,
        borderColor: colors.saffronYellow,
        borderRadius: 2,
        marginLeft: 0,
    },
});

export default MembershipCard;
