import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { CommonStyle, TextStyle } from "../../../Constants/CommonStyle";
import spacing from "../../../Constants/Spacing";
import colors from "../../../Constants/colors";
import { handleMembershipBtnActions } from "../../Helpers/MembershipButtonActions";


const DualActionView = props => {

    const data = props?.data ?? {};
    const leftButton = data?.leftButton;
    const rightButton = data?.rightButton;
    const isLeftButtonEnabled = leftButton?.isEnabled ?? true
    const isRightButtonEnabled = rightButton?.isEnabled ?? true

    return (
        <View style={styles.container}>
            <View style={CommonStyle.separator} />
            <View style={styles.buttonContainer}>
                <TouchableOpacity disabled={!isLeftButtonEnabled} style={{ flex: 1, padding: spacing.spacing16,backgroundColor: isLeftButtonEnabled ? colors.white : colors.lightGrey }} onPress={() => {
                    handleMembershipBtnActions(leftButton?.action, { ...props.extraProps, data: leftButton?.data });
                }}>
                    <Text style={[styles.buttonText,{color: isLeftButtonEnabled ? colors.color_008DF6 : colors.white}]}>{leftButton?.text}</Text>
                </TouchableOpacity>
                <View style={[CommonStyle.sepraterVertical, { height: "100%" }]} />
                <TouchableOpacity disabled={!isRightButtonEnabled} style={{ flex: 1, padding: spacing.spacing16, backgroundColor: isRightButtonEnabled ? colors.white : colors.lightGrey }} onPress={() => {
                    handleMembershipBtnActions(rightButton?.action, { ...props.extraProps, data: rightButton?.data });
                }}>
                    <Text style={[styles.buttonText,{color: isRightButtonEnabled ? colors.color_008DF6 : colors.white}]}>{rightButton?.text}</Text>
                </TouchableOpacity>
            </View>
        </View>
    );

};

const styles = StyleSheet.create({
    container: {
        marginTop: spacing.spacing0,
        // height: 48,
    },
    buttonContainer: {
        flexDirection: "row",
    },
    buttonText: {
        ...TextStyle.text_14_bold,
        color: colors.color_008DF6,
        alignSelf: "center",
    },
});

export default DualActionView;
