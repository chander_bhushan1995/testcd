import { StyleSheet, View } from "react-native";
import React from "react";
import TextWithRightImageActionView from "./TextWithRightImageActionView";
import PrimaryButton from "./PrimaryButton";
import DualActionView from "./DualActionView";


export const MembershipCardActionsType = {
    TEXT_WITH_RIGHT_IMAGE: "TEXT_WITH_RIGHT_IMAGE",
    DUAL_ACTION_BUTTON: "DUAL_ACTION_BUTTON",
    PRIMARY_BUTTON: "PRIMARY_BUTTON",
};

const MembershipCardActionContainer = props => {

    const cardActionViews = props.cardActionViews ?? [];

    const getActionViewFor = (cardAction, index) => {
        switch (cardAction?.type) {
            case MembershipCardActionsType.TEXT_WITH_RIGHT_IMAGE:
                return <TextWithRightImageActionView data={cardAction} extraProps={props.extraProps} />;
            case MembershipCardActionsType.DUAL_ACTION_BUTTON:
                return <DualActionView data={cardAction} extraProps={props.extraProps} />;
            case MembershipCardActionsType.PRIMARY_BUTTON:
                return <PrimaryButton data={cardAction} extraProps={props.extraProps} />;
            default:
                break;
        }
    };

    return (
        <View>
            {(cardActionViews ?? []).map((cardAction, index) => {
                return (
                    <View key={index.toString()}>
                        {index != 0 ? <View style={{ height: 12 }} /> : null}
                        {getActionViewFor(cardAction, index)}
                    </View>
                );
            })}
        </View>
    );
};

const styles = StyleSheet.create({});

export default MembershipCardActionContainer;
