import React from "react";
import spacing from "../../../Constants/Spacing";
import ButtonWithLoader from "../../../CommonComponents/ButtonWithLoader";
import { View, StyleSheet } from "react-native";
import { handleMembershipBtnActions } from "../../Helpers/MembershipButtonActions";

const PrimaryButton = props => {
    const data = props?.data ?? {};
    const button = data?.button;
    return (
        <View style={styles.container}>
            <ButtonWithLoader
                isLoading={false}
                enable={true}
                Button={{
                    text: button?.text,
                    onClick: () => {
                        handleMembershipBtnActions(button?.action, { ...props.extraProps, data: button?.data });
                    },
                }} />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginHorizontal: spacing.spacing16,
        marginBottom: spacing.spacing16,
    },
});

export default PrimaryButton;
