import React from "react";
import { Image, Text, TouchableOpacity, View, StyleSheet } from "react-native";
import { CommonStyle, TextStyle } from "../../../Constants/CommonStyle";
import spacing from "../../../Constants/Spacing";
import colors from "../../../Constants/colors";
import { handleMembershipBtnActions } from "../../Helpers/MembershipButtonActions";


const TextWithRightImageActionView = props => {

    const data = props?.data ?? {};
    const heading = data?.heading;
    const description = data?.description;
    const button = data?.button;
    const style = data?.style;

    return (
        <View style={styles.container}>
            <View style={CommonStyle.separator} />
            <View style={styles.textAndButtonContainer}>
                <View style={{ flex: 1, marginRight: spacing.spacing8 }}>
                    <Text style={[styles.heading, style?.heading]}>{heading}</Text>
                    <Text style={[styles.description, styles?.description]}>{description}</Text>
                </View>
                <TouchableOpacity onPress={() => {
                    handleMembershipBtnActions(button?.action,{...props.extraProps,data: button?.data})
                }}>
                    <Image style={styles.image} source={button?.image} />
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
    },
    textAndButtonContainer: {
        padding: spacing.spacing16,
        flexDirection: "row",
        alignItems: 'center'
    },

    heading: {
        ...TextStyle.text_12_regular,
        color: colors.grey,
    },

    description: {
        ...TextStyle.text_16_regular,
        color: colors.charcoalGrey,
    },

    image: {
        height: 32,
        width: 32,
        resizeMode: "contain",
        alignSelf: "center",
    },
});

export default TextWithRightImageActionView;