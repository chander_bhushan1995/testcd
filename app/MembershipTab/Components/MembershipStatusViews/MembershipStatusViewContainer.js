import { TouchableOpacity, View, StyleSheet } from "react-native";
import React from "react";
import spacing from "../../../Constants/Spacing";
import TextWithButtonStatusView from "./TextWithButtonStatusView";
import TextWithProgressBarView from "./TextWithProgressBarView";
import images from "../../../images/index.image";
import { handleMembershipBtnActions } from "../../Helpers/MembershipButtonActions";

export const MembershipStatusViewType = {
    TEXT_WITH_BUTTON: "TEXT_WITH_BUTTON",
    TEXT_WITH_PROGRESS: "TEXT_WITH_PROGRESS",
};

const MembershipStatusViewContainer = props => {

    const statusViews = props.statusViews ?? {};

    const getStatusViewFor = (statusView, index) => {
        switch (statusView?.type) {
            case MembershipStatusViewType.TEXT_WITH_BUTTON:
                return <TextWithButtonStatusView data={statusView} extraProps={props.extraProps} />;
            case MembershipStatusViewType.TEXT_WITH_PROGRESS:
                return <TextWithProgressBarView data={statusView} extraProps={props.extraProps} />;
            default:
                break;
        }
    };

    return (
        <TouchableOpacity activeOpacity={1} onPress={() => {
            handleMembershipBtnActions(statusViews?.button?.action, {
                ...props.extraProps,
                data: statusViews?.button?.data,
            });
        }} style={[styles.statusViewContainer, { backgroundColor: statusViews?.bgColor }]}>
            {(statusViews?.views ?? []).map((statusView, index) => {
                return (
                    <View key={index.toString()}>
                        {index != 0 ? <View style={{ height: 8 }} /> : null}
                        {getStatusViewFor(statusView, index)}
                    </View>
                );
            })}
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    statusViewContainer: {
        marginHorizontal: spacing.spacing16,
        marginTop: spacing.spacing16,
        paddingVertical: spacing.spacing12,
        borderRadius: 4,
        overflow: "hidden",
    },
});

export default MembershipStatusViewContainer;
