import React from "react";
import { Image, Text, TouchableOpacity, View, StyleSheet } from "react-native";
import { TextStyle } from "../../../Constants/CommonStyle";
import colors from "../../../Constants/colors";
import spacing from "../../../Constants/Spacing";
import Images from "../../../images/index.image";
import { handleMembershipBtnActions } from "../../Helpers/MembershipButtonActions";

const TextWithButtonStatusView = props => {
    const data = props?.data ?? {};
    const heading = data?.heading;
    const image = data?.image ? (typeof data?.image === "number" ? data?.image : { uri: data?.image }) : null;
    const description = data?.description;
    const button = data?.button;
    const style = data?.style;

    const getTextViews = (data) => {
        return data?.map((object,index) => {
            return <Text key={index.toString()} style={[object.style]}>{object.text} </Text>;
        });
    };

    return (
        <View>
            {heading && Array.isArray(heading) && heading.length > 0
                ? <Text style={[styles.header]}>{getTextViews(heading)}</Text>
                : null}

            {image || description || button
                ? <View
                    style={[styles.imageTextButtonContainer, { marginTop: heading ? spacing.spacing8 : spacing.spacing0 }]}>
                    {image ? <Image source={image} style={[styles.image, style?.image]} /> : null}

                    {description ? <Text style={[styles.description, description?.style]}>{description?.text}</Text> : null}

                    {button
                        ? <TouchableOpacity style={styles.button} onPress={() => {
                            handleMembershipBtnActions(button?.action, { ...props.extraProps, data: button?.data });
                        }}>
                            <Text style={styles.buttonText}>{button.text}</Text>
                            {button?.isArrowButton
                                ? <Image source={Images.rightBlueArrow} style={styles.rightArrowStyle} />
                                : null
                            }
                        </TouchableOpacity>
                        : null}
                </View>
                : null}
        </View>
    );
};

const styles = StyleSheet.create({

    header: {
        ...TextStyle.text_12_regular,
        color: colors.charcoalGrey,
        marginHorizontal: spacing.spacing12,
        marginLeft: spacing.spacing12,
    },
    imageTextButtonContainer: {
        marginHorizontal: spacing.spacing12,
        flexDirection: "row",
        justifyContent: "flex-end",
        marginTop: 8,
    },
    description: {
        ...TextStyle.text_12_regular,
        color: colors.charcoalGrey,
        flex: 1,
        alignSelf: 'center'
    },
    image: {
        marginRight: spacing.spacing4,
        marginTop: spacing.spacing4,
        height: 20,
        width: 20,
        resizeMode: "contain",
    },
    button: {
        flexDirection: "row",
        marginLeft: spacing.spacing8,
        alignItems: "center",
    },
    rightArrowStyle: {
        width: 8,
        height: 12,
        marginLeft: spacing.spacing4,
        resizeMode: "contain",
    },
    buttonText: {
        ...TextStyle.text_14_bold,
        color: colors.blue,
        alignSelf: "center",
    },
});

export default TextWithButtonStatusView;
