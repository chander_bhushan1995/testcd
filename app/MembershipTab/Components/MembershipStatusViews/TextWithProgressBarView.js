import React, { useEffect } from "react";
import { View, Text, StyleSheet } from "react-native";
import ProgressBar from "../../../CommonComponents/ProgressBar";
import colors from "../../../Constants/colors";
import { TextStyle } from "../../../Constants/CommonStyle";
import spacing from "../../../Constants/Spacing";

const TextWithProgressBarView = props => {

    const data = props?.data ?? {};
    const heading = data?.heading;
    const progressValue = data?.progressValue;
    const progressColor = data?.progressColor;
    const style = data?.style;

    return (
        <View style={{ paddingHorizontal: spacing.spacing12 }}>
            <Text style={[styles.heading, style?.heading]}>{heading}</Text>
            <ProgressBar data={{completed: progressValue}}
                         style={{
                             containerStyle: {
                                 ...style?.progressBar,
                                 backgroundColor: colors.greye0,
                                 marginTop: spacing.spacing8,
                                 borderRadius: 2,
                                 overflow: 'hidden'
                             },
                             progressBarStyle: { backgroundColor: progressColor },
                         }} />
        </View>
    );
};

const styles = StyleSheet.create({
    heading: {
        ...TextStyle.text_12_regular,
        color: colors.charcoalGrey,
    },
});

export default TextWithProgressBarView;