import React from "react";
import {StyleSheet, Text, View, Image, TouchableOpacity} from "react-native";
import {CommonStyle, TextStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import MembershipStatusViewContainer from "./MembershipStatusViews/MembershipStatusViewContainer";
import MembershipCardActionContainer from "./MembershipCardActionViews/MembershipCardActionContainer";

import {handleMembershipBtnActions} from "../Helpers/MembershipButtonActions";
import Images from "../../images/index.image";


const ServiceRequestCard = props => {

    const data = props?.data;
    const heading = data?.heading; //?? { text: "", tappableText: { text: "", action: "" } };
    const subheading = data?.subHeading;
    const topLeftButton = data?.topLeftButton;
    const bottomRightButton = data?.bottomRightButton;
    const statusViews = data?.statusViews;
    const actionViews = data?.actionViews;

    return (
        <View style={styles.card}>
            {heading || topLeftButton ?
                <View style={[styles.headerContainer]}>
                    <View style={{marginRight: spacing.spacing16, flex: 1}}>
                        <View style={{flex: 1, flexWrap: "wrap", flexDirection: "row"}}>
                            <Text style={styles.heading} numberOfLines={1}>{heading?.text}</Text>
                            {heading?.tappableText
                                ? <Text style={styles.tappableText} onPress={() => {
                                    handleMembershipBtnActions(heading?.tappableText?.action, {
                                        ...props.extraProps,
                                        membershipResponse: data?.membershipResponse,
                                        data: heading?.tappableText?.data,
                                    });
                                }}> {heading?.tappableText?.text}</Text>
                                : null
                            }
                        </View>
                    </View>
                    {topLeftButton
                        ? <TouchableOpacity style={styles.cardTopLeftButton} onPress={() => {
                            handleMembershipBtnActions(topLeftButton?.action, {
                                ...props.extraProps,
                                membershipResponse: data?.membershipResponse,
                                data: topLeftButton?.data,
                            });
                        }}>
                            <Text style={styles.cardTopLeftButtonText}>{topLeftButton?.text}</Text>
                        </TouchableOpacity>
                        : null}
                </View>
                : null}

            {subheading
                ? <Text style={styles.subHeading}>{subheading}</Text>
                : null
            }

            {statusViews
                ? <View style={{marginBottom: spacing.spacing16}}>
                    <MembershipStatusViewContainer statusViews={statusViews} extraProps={{
                        ...props.extraProps,
                        membershipResponse: data?.membershipResponse,
                    }}/>
                </View>
                : null
            }

            {actionViews
                ? <MembershipCardActionContainer cardActionViews={actionViews} extraProps={{
                    ...props.extraProps,
                    membershipResponse: data?.membershipResponse,
                }}/>
                : null}

            {bottomRightButton
                ? <TouchableOpacity style={styles.cardBottomRightButton} onPress={() => {
                    handleMembershipBtnActions(bottomRightButton?.action, {
                        ...props.extraProps,
                        membershipResponse: data?.membershipResponse,
                        data: bottomRightButton?.data,
                    });
                }}>
                    <Text style={styles.cardBottomRightButtonText}>{bottomRightButton?.text}</Text>
                    {bottomRightButton?.isArrowButton
                        ? <Image source={Images.rightBlueArrow} style={styles.rightArrowStyle}/>
                        : null
                    }
                </TouchableOpacity>
                : null
            }
        </View>
    );
};


const styles = StyleSheet.create({
    card: {
        ...CommonStyle.cardContainerWithShadow,
        marginHorizontal: spacing.spacing12,
        borderRadius: 4,
    },
    headerContainer: {
        flexDirection: "row",
        paddingHorizontal: spacing.spacing16,
        paddingTop: spacing.spacing16,
    },
    heading: {
        ...TextStyle.text_16_bold,
        color: colors.charcoalGrey,
    },
    tappableText: {
        ...TextStyle.text_16_bold,
        color: colors.blue028,
    },
    cardTopLeftButton: {},
    cardTopLeftButtonText: {
        ...TextStyle.text_14_bold,
        color: colors.blue028,
    },
    cardBottomRightButton: {
        alignSelf: "flex-end",
        flexDirection: "row",
        alignItems: "center",
        marginRight: spacing.spacing16,
        marginBottom: spacing.spacing16,
    },
    rightArrowStyle: {
        width: 8,
        height: 12,
        marginLeft: spacing.spacing4,
        resizeMode: "stretch",
    },
    cardBottomRightButtonText: {
        ...TextStyle.text_14_bold,
        color: colors.blue028,
    },
    subHeading: {
        ...TextStyle.text_12_regular,
        color: colors.grey,
        marginHorizontal: spacing.spacing16,
        marginTop: spacing.spacing8,
    },
});

export default ServiceRequestCard;
