import images from "../images/index.image";
import colors from "../Constants/colors";
import { CONTACT_NUMBER } from "../Constants/AppConstants";
import { MembershipButtonActions } from "./Helpers/MembershipButtonActions";
import { TextStyle } from "../Constants/CommonStyle";


export const SERVICE_CARDS = [
    {
        "heading": { "text": "ACTIVA Air Conditioner", "tappableText": { "text": "+3 More", "action": "" } },
        "subHeading": "Expected service closure date is 27th Sept",
        "topLeftButton": {
            "text": "Download Invoice",
            "action": "DOWNLOAD_INVOICE",
        },
        "statusViews": {
            "bgColor": "rgba(255,171,0,0.08)",
            "button": {
                "action": "RATE_US",
            },
            "views": [
                {
                    "type": "TEXT_WITH_BUTTON",
                    "heading": [{text: "Your claim was successfully processed on 09-mar. Hope you had a good claim experience."}],
                    "image": "",
                    "description": "",
                    "button": {
                        "isArrowButton": true,
                        "text": "Rate us",
                        "action": "RATE_US",
                        "data": {
                            "dummyData": "dummyValue",
                        },
                    },
                },
            ],
        },
    },
    {
        "heading": {
            "text": "ACTIVA Air Conditioner",
        },
        "topLeftButton": {
            "text": "Download Invoice",
            "action": "DOWNLOAD_INVOICE",
        },
        "statusViews": {
            "bgColor": "rgba(255,171,0,0.08)",
            "button": {
                "action": "RATE_US",
            },
            "views": [
                {
                    "type": "TEXT_WITH_BUTTON",
                    "heading": [{text: "Your claim was successfully processed on 09-mar. Hope you had a good claim experience."}],
                    "description": "",
                },
            ],
        },
        "bottomRightButton": {
            "text": "View Detail",
            "action": "VIEW_DETAIL",
            "isArrowButton": true,
        },
    },
];

export const DUMMY_CARDS =
    [
        {
            "heading": "HomeAssist Plan - Covers upto 7 Appliances",
            "image": "https://uat1cdn.1atesting.in/static/oaapp/iOS/washingMachine@3x.png",
            "subHeading": "HomeAssist Plan - Covers upto 7 Appliances",
            "description": {
                "text": "Washing Machine, Air Conditioner, TV…",
                "tappableText": {
                    "text": " +2 more",
                    "action": "SHOW_APPLIANCE_BOTTOM_SHEET",
                    "data": {
                        "dummyData": "dummyValue",
                    },
                },
            },
            "note": {
                "text": "Valid till 11th Oct, 2018",
            },
            "tag": {
                "text": "AWAITING CONFIRMATION",
                "textColor": colors.saffronYellow,
                "borderColor": colors.saffronYellow,
            },
            "statusViews": {
                "bgColor": "rgba(69,180,72,0.08)",
                "button": {
                    "action": "",
                },
                "views": [
                    {
                        "type": "TEXT_WITH_BUTTON",
                        "heading": [{text: "Awesome! Your claim is successfully completed 👍  Please tell us your overall service experience."}],
                        "image": "",
                        "description": {text: "hellow owrld",style:{...TextStyle.text_14_bold}},
                        "button": {
                            "isArrowButton": true,
                            "text": "Rate us",
                            "action": "SHOW_APPLIANCE_BOTTOM_SHEET",
                            "data": {
                                "dummyData": "dummyValue",
                            },
                        },
                    },
                    {
                        "type": "TEXT_WITH_BUTTON",
                        "heading": [{text: "Awesome! Your claim is successfully completed 👍  Please tell us your overall service experience.",style:{color: colors.grey}}],
                    }
                ],
            },
            "actionViews": [
                {
                    "type": "DUAL_ACTION_BUTTON",
                    "leftButton": {
                        "text": "View Details",
                        "action": "SHOW_APPLIANCE_BOTTOM_SHEET",
                        "data": {
                            "dummyData": "dummyValue",
                        },
                    },
                    "rightButton": {
                        "text": "Raise Claim",
                        "action": "SHOW_APPLIANCE_BOTTOM_SHEET",
                        "data": {
                            "dummyData": "dummyValue",
                        },
                    },
                },
            ],
        },
        {
            "heading": "HomeAssist Plan - Covers upto 7 Appliances",
            "image": "https://uat1cdn.1atesting.in/static/oaapp/iOS/washingMachine@3x.png",
            "subHeading": "",
            "description": {
                "text": "Washing Machine, Air Conditioner, TV…",
                "tappableText": {
                    "text": " +2 more",
                    "action": "SHOW_APPLIANCE_BOTTOM_SHEET",
                    "data": {
                        "dummyData": "dummyValue",
                    },
                },
            },
            "note": {
                "text": "Valid till 11th Oct, 2018",
                "style": {
                    "color": colors.redBF0,
                },
            },
            "statusViews": {
                "bgColor": "rgba(255,171,0,0.08)",
                "button": {
                    "action": "SHOW_APPLIANCE_BOTTOM_SHEET",
                    "data": {
                        "dummyData": "dummyValue",
                    },
                },
                "views": [
                    {
                        "type": "TEXT_WITH_PROGRESS",
                        "heading": "Service Request ID 2210896",
                        "progressValue": "50",
                        "progressColor": "#FFAB00",
                    },
                    {
                        "type": "TEXT_WITH_BUTTON",
                        "description": "Document upload pending",
                        "button": {
                            "isArrowButton": true,
                            "text": "Upload",
                            "action": "",
                        },
                    },
                ],
            },
            "actionViews": [
                {
                    "type": "TEXT_WITH_RIGHT_IMAGE",
                    "heading": "For further help, Call us at",
                    "description": "1800 123 3330",
                    "button": {
                        "image": images.call,
                        "action": MembershipButtonActions.CALL_US,
                        "data": {
                            "number": CONTACT_NUMBER,
                        },
                    },
                },
            ],
        },
        {
            "heading": "HomeAssist Plan - Covers upto 7 Appliances",
            "image": "https://uat1cdn.1atesting.in/static/oaapp/iOS/washingMachine@3x.png",
            "subHeading": "",
            "description": {
                "text": "Washing Machine, Air Conditioner, TV…",
                "tappableText": {
                    "text": " +2 more",
                    "action": "SHOW_APPLIANCE_BOTTOM_SHEET",
                    "data": {
                        "dummyData": "dummyValue",
                    },
                },
            },
            "note": {
                "text": "Valid till 11th Oct, 2018",
                "style": {
                    "color": colors.redBF0,
                },
            },
            "statusViews": {
                "bgColor": "rgba(208,2,27,0.08)",
                "button": {
                    "action": "SHOW_APPLIANCE_BOTTOM_SHEET",
                    "data": {
                        "dummyData": "dummyValue",
                    },
                },
                "views": [
                    {
                        "type": "TEXT_WITH_PROGRESS",
                        "heading": "Service Request ID 2210896",
                        "progressValue": 50,
                        "progressColor": "#D0021B",
                    },
                    {
                        "type": "TEXT_WITH_BUTTON",
                        "description": "Claim Rejected",
                        "button": {
                            "isArrowButton": true,
                            "text": "View status",
                            "action": "SHOW_APPLIANCE_BOTTOM_SHEET",
                            "data": {
                                "dummyData": "dummyValue",
                            },
                        },
                    },
                ],
            },
            "actionViews": [
                {
                    "type": "TEXT_WITH_RIGHT_IMAGE",
                    "heading": "For further help, Call us at",
                    "description": "1800 123 3330",
                    "action": "SHOW_APPLIANCE_BOTTOM_SHEET",
                    "data": {
                        "dummyData": "dummyValue",
                    },
                },
            ],
        },
        {
            "heading": "HomeAssist Plan - Covers upto 7 Appliances",
            "image": "https://uat1cdn.1atesting.in/static/oaapp/iOS/washingMachine@3x.png",
            "subHeading": "",
            "description": {
                "text": "Washing Machine, Air Conditioner, TV…",
                "tappableText": {
                    "text": " +2 more",
                    "action": "SHOW_APPLIANCE_BOTTOM_SHEET",
                    "data": {
                        "dummyData": "dummyValue",
                    },
                },
            },
            "note": {
                "text": "Valid till 11th Oct, 2018",
                "style": {
                    "color": colors.redBF0,
                },
            },
            "statusViews": {
                "bgColor": "rgba(255,171,0,0.08)",
                "button": {
                    "action": "SHOW_APPLIANCE_BOTTOM_SHEET",
                    "data": {
                        "dummyData": "dummyValue",
                    },
                },
                "views": [
                    {
                        "type": "TEXT_WITH_BUTTON",
                        "description": "2 on-going requests",
                        "button": {
                            "isArrowButton": true,
                            "text": "View status",
                            "action": "SHOW_APPLIANCE_BOTTOM_SHEET",
                            "data": {
                                "dummyData": "dummyValue",
                            },
                        },
                    },
                ],
            },
            "actionViews": [
                {
                    "type": "TEXT_WITH_RIGHT_IMAGE",
                    "heading": "For further help, Call us at",
                    "description": "1800 123 3330",
                    "button": {
                        "image": images.call,
                        "action": MembershipButtonActions.CALL_US,
                        "data": {
                            "number": CONTACT_NUMBER,
                        },
                    },
                },
            ],
        },
        {
            "heading": "HomeAssist Plan - Covers upto 7 Appliances",
            "image": "https://uat1cdn.1atesting.in/static/oaapp/iOS/washingMachine@3x.png",
            "subHeading": "",
            "description": {
                "text": "Washing Machine, Air Conditioner, TV…",
                "tappableText": {
                    "text": " +2 more",
                    "action": "SHOW_APPLIANCE_BOTTOM_SHEET",
                    "data": {
                        "dummyData": "dummyValue",
                    },
                },
            },
            "note": {
                "text": "Valid till 11th Oct, 2018",
                "style": {
                    "color": colors.redBF0,
                },
            },
            "statusViews": {
                "bgColor": "rgba(255,171,0,0.08)",
                "button": {
                    "action": "SHOW_APPLIANCE_BOTTOM_SHEET",
                    "data": {
                        "dummyData": "dummyValue",
                    },
                },
                "views": [
                    {
                        "type": "TEXT_WITH_BUTTON",
                        "image": images.yellowInfo,
                        "heading": [{text: "2 on-going requests"}],
                        "description": "Immediate action needed!",
                        "button": {
                            "isArrowButton": true,
                            "text": "View status",
                            "action": "SHOW_APPLIANCE_BOTTOM_SHEET",
                            "data": {
                                "dummyData": "dummyValue",
                            },
                        },
                    },
                ],
            },
            "actionViews": [
                {
                    "type": "TEXT_WITH_RIGHT_IMAGE",
                    "heading": "For further help, Call us at",
                    "description": "1800 123 3330",
                    "button": {
                        "image": images.call,
                        "action": MembershipButtonActions.CALL_US,
                        "data": {
                            "number": CONTACT_NUMBER,
                        },
                    },
                },
            ],
        },
        {
            "heading": "HomeAssist Plan - Covers upto 7 Appliances",
            "image": "https://uat1cdn.1atesting.in/static/oaapp/iOS/washingMachine@3x.png",
            "subHeading": "",
            "description": {
                "text": "Washing Machine, Air Conditioner, TV…",
                "tappableText": {
                    "text": " +2 more",
                    "action": "SHOW_APPLIANCE_BOTTOM_SHEET",
                    "data": {
                        "dummyData": "dummyValue",
                    },
                },
            },
            "note": {
                "text": "Valid till 11th Oct, 2018",
                "style": {
                    "color": colors.redBF0,
                },
            },
            "actionViews": [
                {
                    "type": "PRIMARY_BUTTON",
                    "button": {
                        "text": "Upload Documents",
                        "action": "SHOW_APPLIANCE_BOTTOM_SHEET",
                        "data": {
                            "dummyData": "dummyValue",
                        },
                    },
                },
            ],
        },
    ];

export const MEMBERSHIP_RAW_RESPONSE = [
    {
        "memId": 1005125802,
        "isPending": true,
        "startDate": 1594924200000,
        "salesPrice": 99.0,
        "purchaseDate": 1594924200000,
        "orderId": 11571539,
        "graceDays": 0,
        "partnerCode": 29,
        "customers": [{
            "custId": 6430487,
            "firstName": "Akash",
            "lastName": "M",
            "relationship": "self",
            "mobileNumber": 8800757363,
            "emailId": "akash.mondal@oneassist.in",
            "addresses": [{
                "addressId": 0,
                "addressType": null,
                "addressLine1": null,
                "addressLine2": null,
                "landmark": null,
                "country": null,
                "pincode": 0,
                "mailingAddress": false,
                "stateName": null,
                "stateCode": null,
                "cityName": null,
                "cityCode": null,
                "stdcode": null,
                "mobileNo": null,
            }],
        }],
        "plan": {
            "planName": "IDFence Premium Plan with Credit Score",
            "planCode": 6392,
            "allowedMaxQuantity": 1,
            "products": ["WP01"],
            "services": [{
                "id": 8210,
                "serviceId": 24,
                "serviceName": "DARK_WEB_MONITORING",
                "description": [],
                "claimEligibility": true,
                "insuranceBacked": true,
            }, {
                "id": 8211,
                "serviceId": 28,
                "serviceName": "CREDIT_SCORE",
                "description": [],
                "claimEligibility": true,
                "insuranceBacked": true,
            }],
            "categories": ["F"],
            "trial": "N",
            "maxInsuranceValue": 0.0,
            "minInsuranceValue": 0.0,
            "version": "5",
            "quantity": 0,
        },
        "uploadDocsLastDate": 1596220200000,
        "activationCode": "xdtI%2BaMnIMCEQEh%2FIMH58g%3D%3D",
        "activity": "RW",
        "activityRefId": "5f10a1c38f0828c4b20eb66e",
        "invoiceLimitInDays": 0,
        "glance": false,
    },
    {
        "memId": 1008255283,
        // "siStatus": "A",
        "memUUID": "d29c641e-416b-4c9e-9dd1-a622cf9b830d",
        "loanNo": null,
        "startDate": 1614450600000,
        "endDate": 1616956200000,
        "memCancellationDate": null,
        "coverAmount": 0.0,
        "planName": "IDFence Monthly Gold Plan ",
        "allowedMaxQuantity": 1,
        "salesPrice": 129.0,
        "purchaseDate": 1614450600000,
        "orderId": 13551528,
        "graceDays": 7,
        "orderStatus": null,
        "membershipStatus": "A",
        "enableFileClaim": true,
        "enableChatClaim": true,
        "invoiceNumber": 11965878,
        "products": ["WP01"],
        "assets": [{
            "assetId": 6281709,
            "name": "WalletAssist",
            "prodCode": "WP01",
            "category": "F",
            "createdOn": 1609236999593,
            "custId": 6430487,
            "canRenew": true,
            "status": "A",
            "contentType": "MOBILE",
            "contentNo": "8800757363",
            "lossReportId": "0",
            "contentTypeDesc": "Mobile No",
        }],
        "claims": null,
        "enableClaimTracking": true,
        "showSIOpt": true,
        "showInsuranceCertificate": true,
        "showPaymentReceipt": true,
        "planCode": 7007,
        "partnerCode": 139,
        "partnerBUCode": 2185,
        "partnerName": "ASSIST APP",
        "partnerBUName": "ASSIST IOS",
        "partnerCompanyCode": "APP",
        "isSIEnabled": true,
        "trial": "N",
        "renewalStatus": "NA",
        "renewalActivity": "RW",
        "customers": [{
            "custId": 6430487,
            "firstName": "Akash",
            "lastName": "M",
            "gender": "MALE",
            "dob": 668889000000,
            "salutation": "MR",
            "emailId2": "akash.iitmadras09@gmail.com",
            "custType": "SUBSCRIBER",
            "relationship": "self",
            "mobileNumber": 8800757363,
            "emailId": "akash.mondal@oneassist.in",
            "addresses": [{
                "addressId": 5495869,
                "addressType": "PER",
                "addressLine1": "Fyfuf",
                "addressLine2": "NA",
                "landmark": "gurgaon",
                "country": "INDIA",
                "pincode": 122001,
                "mailingAddress": false,
                "stateName": "Delhi-NCR",
                "stateCode": "DL",
                "cityName": "Gurgaon",
                "cityCode": "GUG",
                "stdcode": null,
                "mobileNo": 8800757363,
            }, {
                "addressId": 8911602,
                "addressType": null,
                "addressLine1": "Nvm ",
                "addressLine2": null,
                "landmark": null,
                "country": null,
                "pincode": 560100,
                "mailingAddress": true,
                "stateName": "Karnataka",
                "stateCode": "KA",
                "cityName": "Bangalore South",
                "cityCode": "BNGS",
                "stdcode": null,
                "mobileNo": null,
            }],
        }],
        "accountNo": 8553498,
        "plan": {
            "planName": "IDFence Monthly Gold Plan ",
            "planCode": 7007,
            "planId": 8537,
            "allowedMaxQuantity": 1,
            "products": ["WP01"],
            "services": [{
                "id": 9762,
                "serviceId": 28,
                "serviceName": "CREDIT_SCORE",
                "partnerCode": 235,
                "description": [],
                "products": [{
                    "code": "WP01",
                    "displayValue": "WalletAssist",
                }],
                "partnerName": "ONEASSIST SERVICE",
                "claimEligibility": true,
                "insuranceBacked": true,
            }, {
                "id": 9763,
                "serviceId": 24,
                "serviceName": "DARK_WEB_MONITORING",
                "partnerCode": 235,
                "description": [],
                "products": [{
                    "code": "WP01",
                    "displayValue": "WalletAssist",
                }],
                "partnerName": "ONEASSIST SERVICE",
                "claimEligibility": true,
                "insuranceBacked": true,
            }],
            "coverAmount": 0.0,
            "price": 129.0,
            "anchorPrice": 199.0,
            "maxInsuranceValue": 0.0,
            "minInsuranceValue": 0.0,
            "paymentFreq": "30",
            "version": "5",
            "quantity": 1,
        },
        "categories": ["F"],
        "isEnterprise": "N",
        "applicationDate": 1614450600000,
        "ekitDocId": null,
        "applicationNo": null,
        "planValidity": "30",
        "anchorPrice": 199.0,
        "orderTransDate": 1614450600000,
        "paymentStatus": "A",
        "planId": 8537,
        "orderCreatedDate": 1614450798887,
        "memCreatedDate": 1609236999294,
        "parentCode": null,
        "orderModifiedDate": 1614450799430,
    },
    {
        "memId": 1005125802,
        "memUUID": "ded4019b-d760-4820-af5f-bfd5c3f561f1",
        "loanNo": null,
        "startDate": 1592332200000,
        "endDate": 1594837800000,
        "memCancellationDate": null,
        "coverAmount": 0.0,
        "planName": "IDFence Premium Plan with Credit Score",
        "allowedMaxQuantity": 1,
        "salesPrice": 2.0,
        "purchaseDate": 1592332200000,
        "orderId": 11340973,
        "graceDays": 7,
        "orderStatus": null,
        "membershipStatus": "E",
        "enableFileClaim": false,
        "enableChatClaim": false,
        "invoiceNumber": 9882089,
        "products": ["WP01"],
        "assets": [{
            "assetId": 4299907,
            "name": "WalletAssist",
            "prodCode": "WP01",
            "category": "F",
            "createdOn": 1564577328438,
            "modifiedOn": 1585913517296,
            "custId": 6430487,
            "canRenew": true,
            "status": "A",
            "contentType": "EMAIL",
            "contentNo": "akash.mondal@oneassist.in",
            "lossReportId": "0",
            "contentTypeDesc": "Email Id"
        }, {
            "assetId": 4299908,
            "name": "WalletAssist",
            "prodCode": "WP01",
            "category": "F",
            "createdOn": 1564577328461,
            "modifiedOn": 1585913517296,
            "custId": 6430487,
            "canRenew": true,
            "status": "A",
            "contentType": "MOBILE",
            "contentNo": "8800757363",
            "lossReportId": "0",
            "contentTypeDesc": "Mobile No"
        }, {
            "assetId": 4691899,
            "name": "WalletAssist",
            "prodCode": "WP01",
            "category": "F",
            "createdOn": 1568328372628,
            "modifiedOn": 1585913517296,
            "custId": 6430487,
            "canRenew": true,
            "status": "A",
            "contentType": "EMAIL",
            "contentNo": "akash.iitmadras09@gmail.com",
            "lossReportId": "0",
            "contentTypeDesc": "Email Id"
        }, {
            "assetId": 5628682,
            "name": "WalletAssist",
            "prodCode": "WP01",
            "category": "F",
            "createdOn": 1586930575069,
            "custId": 6430487,
            "canRenew": true,
            "status": "A",
            "contentType": "PNC",
            "contentNo": "ABCDE1234F",
            "lossReportId": "0",
            "contentTypeDesc": "Pan Card"
        }, {
            "assetId": 5628725,
            "name": "WalletAssist",
            "prodCode": "WP01",
            "category": "F",
            "createdOn": 1586939461704,
            "custId": 6430487,
            "canRenew": true,
            "status": "A",
            "contentType": "CRC",
            "contentNo": "5123XXXXXXXX2346",
            "lossReportId": "0",
            "contentTypeDesc": "Credit Card"
        }, {
            "assetId": 5634314,
            "name": "WalletAssist",
            "prodCode": "WP01",
            "category": "F",
            "createdOn": 1588155989185,
            "custId": 6430487,
            "canRenew": true,
            "status": "A",
            "contentType": "BANK",
            "contentNo": "12233333333333",
            "lossReportId": "0",
            "contentTypeDesc": "Bank Detail"
        }, {
            "assetId": 5634322,
            "name": "WalletAssist",
            "prodCode": "WP01",
            "category": "F",
            "createdOn": 1588154873821,
            "custId": 6430487,
            "canRenew": true,
            "status": "A",
            "contentType": "MOBILE",
            "contentNo": "7982535335",
            "lossReportId": "0",
            "contentTypeDesc": "Mobile No"
        }, {
            "assetId": 5634323,
            "name": "WalletAssist",
            "prodCode": "WP01",
            "category": "F",
            "createdOn": 1588155886153,
            "custId": 6430487,
            "canRenew": true,
            "status": "A",
            "contentType": "DRL",
            "contentNo": "DL12637383838",
            "lossReportId": "0",
            "contentTypeDesc": "Driving License"
        }, {
            "assetId": 5634324,
            "name": "WalletAssist",
            "prodCode": "WP01",
            "category": "F",
            "createdOn": 1588156239438,
            "custId": 6430487,
            "canRenew": true,
            "status": "A",
            "contentType": "FB",
            "contentNo": "NOT_AVAILABLE",
            "lossReportId": "0",
            "contentTypeDesc": "FB"
        }, {
            "assetId": 5770855,
            "name": "WalletAssist",
            "prodCode": "WP01",
            "category": "F",
            "createdOn": 1594207513737,
            "custId": 6430487,
            "canRenew": true,
            "status": "A",
            "contentType": "EMAIL",
            "contentNo": "vikas.dhull@oneassist.in",
            "lossReportId": "0",
            "contentTypeDesc": "Email Id"
        }],
        "claims": null,
        "enableClaimTracking": true,
        "showSIOpt": true,
        "showInsuranceCertificate": true,
        "showPaymentReceipt": true,
        "planCode": 6392,
        "partnerCode": 29,
        "partnerBUCode": 308,
        "partnerName": "PORTAL",
        "partnerBUName": "PORTAL-ORGANIC",
        "partnerCompanyCode": "1A",
        "isSIEnabled": false,
        "trial": "N",
        "renewalStatus": "NA",
        "renewalActivity": "NA",
        "customers": [{
            "custId": 6430487,
            "firstName": "Akash",
            "lastName": "M",
            "gender": "MALE",
            "dob": 668889000000,
            "salutation": "MR",
            "emailId2": "akash.iitmadras09@gmail.com",
            "custType": "SUBSCRIBER",
            "relationship": "self",
            "mobileNumber": 8800757363,
            "emailId": "akash.mondal@oneassist.in",
            "addresses": [{
                "addressId": 5495869,
                "addressType": "PER",
                "addressLine1": "Fyfuf",
                "addressLine2": "NA",
                "landmark": "gurgaon",
                "country": "INDIA",
                "pincode": 122001,
                "mailingAddress": false,
                "stateName": "Delhi-NCR",
                "stateCode": "DL",
                "cityName": "Gurgaon",
                "cityCode": "GUG",
                "stdcode": null,
                "mobileNo": 8800757363
            }, {
                "addressId": 8911602,
                "addressType": null,
                "addressLine1": "Nvm ",
                "addressLine2": null,
                "landmark": null,
                "country": null,
                "pincode": 560100,
                "mailingAddress": true,
                "stateName": "Karnataka",
                "stateCode": "KA",
                "cityName": "Bangalore South",
                "cityCode": "BNGS",
                "stdcode": null,
                "mobileNo": null
            }]
        }],
        "accountNo": 5317494,
        "plan": {
            "planName": "IDFence Premium Plan with Credit Score",
            "planCode": 6392,
            "planId": 6392,
            "allowedMaxQuantity": 1,
            "products": ["WP01"],
            "services": [{
                "id": 6933,
                "serviceId": 24,
                "serviceName": "DARK_WEB_MONITORING",
                "partnerCode": 235,
                "description": [],
                "products": [{
                    "code": "WP01",
                    "displayValue": "WalletAssist"
                }],
                "partnerName": "ONEASSIST SERVICE",
                "claimEligibility": true,
                "insuranceBacked": true
            }, {
                "id": 6935,
                "serviceId": 28,
                "serviceName": "CREDIT_SCORE",
                "partnerCode": 235,
                "description": [],
                "products": [{
                    "code": "WP01",
                    "displayValue": "WalletAssist"
                }],
                "partnerName": "ONEASSIST SERVICE",
                "claimEligibility": true,
                "insuranceBacked": true
            }],
            "coverAmount": 0.0,
            "price": 129.0,
            "anchorPrice": 199.0,
            "maxInsuranceValue": 0.0,
            "minInsuranceValue": 0.0,
            "paymentFreq": "30",
            "version": "1",
            "quantity": 1
        },
        "categories": ["F"],
        "isEnterprise": "N",
        "applicationDate": 1592332200000,
        "ekitDocId": null,
        "applicationNo": null,
        "planValidity": "30",
        "anchorPrice": 199.0,
        "orderTransDate": 1592332200000,
        "paymentStatus": "A",
        "planId": 6392,
        "orderCreatedDate": 1592392601484,
        "memCreatedDate": 1564511400000,
        "parentCode": null,
        "orderModifiedDate": 1592392685036
    },
    {
        "memId": 1008423893,
        "memUUID": "77a1a9d6-82eb-43e2-b996-3382ce0012b1",
        "startDate": 1615833000000,
        "endDate": 1618338600000,
        "memCancellationDate": null,
        "coverAmount": 0,
        "planName": "IDFence Basic Plan",
        "allowedMaxQuantity": 1,
        "salesPrice": 0,
        "purchaseDate": 1615833000000,
        "orderId": 1130191237,
        "graceDays": 7,
        "orderStatus": null,
        "membershipStatus": "A",
        "enableFileClaim": false,
        "invoiceNumber": 11661074,
        "products": [
            "WP01"
        ],
        "assets": [
            {
                "assetId": 6374438,
                "name": "WalletAssist",
                "prodCode": "WP01",
                "category": "F",
                "createdOn": 1615873274489,
                "custId": 12609158,
                "canRenew": true,
                "status": "A",
                "contentType": "MOBILE",
                "contentNo": "8800000000",
                "lossReportId": "0",
                "contentTypeDesc": "Mobile No"
            }
        ],
        "claims": null,
        "enableClaimTracking": false,
        "showSIOpt": false,
        "showInsuranceCertificate": false,
        "showPaymentReceipt": false,
        "planCode": 7780,
        "partnerCode": 139,
        "partnerBUCode": 2185,
        "partnerName": "ASSIST APP",
        "partnerBUName": "ASSIST IOS",
        "partnerCompanyCode": "APP",
        "isSIEnabled": false,
        "trial": "Y",
        "renewalStatus": "NA",
        "renewalActivity": "NA",
        "customers": [
            {
                "custId": 12609158,
                "firstName": "sdfsd",
                "lastName": "",
                "custType": "SUBSCRIBER",
                "relationship": "self",
                "mobileNumber": 8800000000,
                "emailId": "sdfdl@gmail.com"
            }
        ],
        "accountNo": 8722105,
        "plan": {
            "planName": "IDFence Basic Plan",
            "planCode": 7780,
            "planId": 9579,
            "allowedMaxQuantity": 1,
            "products": [
                "WP01"
            ],
            "services": [
                {
                    "id": 10977,
                    "serviceId": 24,
                    "serviceName": "DARK_WEB_MONITORING",
                    "partnerCode": 235,
                    "description": [],
                    "products": [
                        {
                            "code": "WP01",
                            "displayValue": "WalletAssist"
                        }
                    ],
                    "partnerName": "ONEASSIST SERVICE",
                    "claimEligibility": true,
                    "insuranceBacked": true
                }
            ],
            "coverAmount": 0,
            "price": 0,
            "anchorPrice": 0,
            "maxInsuranceValue": 0,
            "minInsuranceValue": 0,
            "paymentFreq": "30",
            "version": "5",
            "quantity": 1
        },
        "categories": [
            "F"
        ],
        "isEnterprise": "N",
        "applicationDate": 1615833000000,
        "ekitDocId": null,
        "applicationNo": null,
        "planValidity": "30",
        "anchorPrice": 0,
        "orderTransDate": 1615833000000,
        "paymentStatus": "A",
        "planId": 9579,
        "orderCreatedDate": 1615873237117,
        "memCreatedDate": 1615873272298,
        "parentCode": null,
        "orderModifiedDate": 1615873257934
    }
];
