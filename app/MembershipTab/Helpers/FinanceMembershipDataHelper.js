import {
    CARD_STATUS,
    CATEGORY,
    MEMBERSHIP_TAB_CARD_TEXT,
    PRIORITY, RENEWAL_STATUS,
    TYPE,
    USER_TASK
} from "../../Constants/MembershipConstants";
import {getSingleCategory} from "./MembershipDataFilters";
import {
    checkIsMemIsIdFence,
    getAssetListText,
    getDescriptionColor,
    getExpiryTextColor, getMembershipRenewalDescription, getStatusTagColor,
    getValidTillText, isABBOnlyMembership, isUnderRenewal
} from "./MembershipTabUtils";
import { getIDFenceCardViewModel} from "./IDFenceHelper";
import images from "../../images/index.image";
import {formattedDate} from "../../commonUtil/DateUtils";
import {ButtonTexts, VIEW_DETAIL} from "../../Constants/AppConstants";
import {MembershipButtonActions} from "./MembershipButtonActions";
import {addFraudDetectionMembershipCard} from "./PEMembershipDataHelper";
import {MembershipCardActionsType} from "../Components/MembershipCardActionViews/MembershipCardActionContainer";
import Colors from "../../Constants/colors";

/**
 *
 * Helper for Finance memberships to prepare data for UI
 */
const TAG = "FinanceMembershipDataHelper";

export const addFinanceUiMembership = (membershipList) => {
    //get All F Memberships
    let finMembershipList = membershipList?.filter((membership) => CATEGORY.FINANCE === getSingleCategory(membership?.categories));
    let haUIModelList = [];
    let actualMembershipList = finMembershipList?.filter((membership) => (TYPE.EXPIRED !== membership?.membershipStatus ||
        checkIsMemIsIdFence(membership)));
    actualMembershipList?.forEach(membership => {
        if (checkIsMemIsIdFence(membership)) {

            let idFenceCardViewModel = getIDFenceCardViewModel(membership);
            if (idFenceCardViewModel) {
                haUIModelList.push(idFenceCardViewModel);
                return;
            }
            if (membership?.trial === 'Y') {
                let trailMembership = processTrialMembership(membership);
                if (trailMembership) {
                    haUIModelList.push(idFenceCardViewModel);
                }
                return;
            }
            return;
        }
        if (membership?.memId && !membership?.task) {
            let renewalMembership=addRenewalMembershipCard(membership);
            if (renewalMembership) {
                haUIModelList.push(renewalMembership);
                return;
            }
            haUIModelList.push(addNormalMembershipCard(membership));
            return;
        } else if (!membership?.memId &&
            membership?.task &&
            membership?.tempCustInfoData &&
            membership?.tempCustInfoData?.enableFDFlow &&
            [USER_TASK.DOC_UPLOAD].includes(membership?.task?.name)) {
            let fdMembership = addFraudDetectionMembershipCard(membership);
            if (fdMembership) {
                haUIModelList.push(fdMembership)
                return;
            }
        } else {
            haUIModelList.push(processPendingFinanceMembership(membership));
        }
    });
    return haUIModelList;
}

function addRenewalMembershipCard(membership) {
    let statusView = {};
    let actionViews = [];
    let image=(CATEGORY.FINANCE === getSingleCategory(membership?.categories)?images.buy_plan_wallet_icon:images.buy_plan_mobile_icon)
    let description = getMembershipRenewalDescription(membership);
    let dualActionButton = {};
    let assetName = getAssetListText(membership);
    let expiryText = getValidTillText(membership);
    let isABBOnlyActionText=(isABBOnlyMembership(membership) ? ButtonTexts.TXT_SELL_YOUR_PHONE : ButtonTexts.TXT_BOOK_A_SERVICE);
    if (isUnderRenewal(membership)) {
        statusView = {
            bgColor: Colors.BG_DESCRIPTION_RED,
            button: {
                action: MembershipButtonActions.RENEW_NOW,
                data: {}
            },
            views: [
                {
                    type: "TEXT_WITH_BUTTON",
                    heading: description,
                    description: null,
                    button: {
                        isArrowButton: true,
                        text: MEMBERSHIP_TAB_CARD_TEXT.RENEW_NOW,
                        action: MembershipButtonActions.RENEW_NOW,
                        data: {},
                    },
                }
            ]
        };
        dualActionButton.type = "DUAL_ACTION_BUTTON";
        dualActionButton.leftButton = {
            text: VIEW_DETAIL,
            action: MembershipButtonActions.VIEW_DETAILS,
            data: {},
        };
        if (CATEGORY.FINANCE === getSingleCategory(membership?.categories)) {
            dualActionButton.rightButton = {
                text: ButtonTexts.MY_CARDS,
                isEnabled: true,
                action: MembershipButtonActions.MY_CARDS,
                data: {},
            };
        } else {
            dualActionButton.rightButton = {
                text: isABBOnlyActionText,
                isEnabled: membership?.enableFileClaim || false,
                action: MembershipButtonActions.RAISE_CLAIM,
                data: {},
            };
        }
        actionViews.push(dualActionButton)
        return {
            heading: membership?.plan?.planName,
            image: image,
            description: {
                text: assetName,
            },
            note: {text: expiryText, style: {color: getExpiryTextColor(membership)}},
            actionViews: actionViews,
            statusViews: statusView,
            membershipResponse: membership,
            priority: PRIORITY.ACTION_RENEW
        }
    }
    if (CATEGORY.FINANCE === getSingleCategory(membership?.categories) &&
        [RENEWAL_STATUS.IN_RENEWAL_WINDOW].includes(membership?.renewalActivity) && membership?.isPendingMembership) {
        let tag = {
            text: MEMBERSHIP_TAB_CARD_TEXT.RENEWAL_IN_PROGRESS,
            textColor: getStatusTagColor(CARD_STATUS.RENEWAL_IN_PROGRESS),
            borderColor: getStatusTagColor(CARD_STATUS.RENEWAL_IN_PROGRESS),
        }
        statusView = {
            bgColor: getDescriptionColor(CARD_STATUS.RENEWAL_IN_PROGRESS),
            button: {action: null},
            views: [
                {
                    type: "TEXT_WITH_BUTTON",
                    heading: null,
                    description: {text:MEMBERSHIP_TAB_CARD_TEXT.TEXT_PLAN_ACTIVATION_DESC},
                    image: images.yellowInfo
                }
            ]
        };
        return {
            heading: membership?.plan?.planName,
            image: image,
            description: {
                text: assetName,
            },
            note: {text: expiryText, style: {color: getExpiryTextColor(membership)}},
            statusViews: statusView,
            tag: tag,
            membershipResponse: membership,
            priority: PRIORITY.OA_ACTION_PENDING_SALES
        }
    }

    return null;
}

function processPendingFinanceMembership(membership) {
    let expiryText = "";
    let assetName = getAssetListText(membership);
    let subHeading = '';
    if (membership?.isABBPlan) {
        subHeading = {
            text: MEMBERSHIP_TAB_CARD_TEXT.GET_GUARANTEED_EXCHANGE_MSG,
            style: {color: Colors.color_888F97, fontSize: 12}
        };
    } else if (membership?.doesPlanSupportsPMS) {
        subHeading = {
            text: MEMBERSHIP_TAB_CARD_TEXT.INCLUDES_PREVENTIVE_MAINTENANCE_SERVICE,
            style: {color: Colors.color_212121, fontSize: 12}
        };
    }
    if (membership?.uploadDocsLastDate) {
        expiryText = MEMBERSHIP_TAB_CARD_TEXT.ACTIVATE_BY(formattedDate(new Date(new Number(membership?.uploadDocsLastDate)), 'dd mmm yyyy'));
    }
    let actionView = [{
        type: MembershipCardActionsType.PRIMARY_BUTTON,
        button: {
            text: MEMBERSHIP_TAB_CARD_TEXT.ACTIVATE_NOW_TEXT,
            action: MembershipButtonActions.ACTIVATE_FINANCE_MEMBERSHIP,
            data: {},
        }
    }]
    return {
        heading: membership?.plan?.planName,
        image: images.buy_plan_wallet_icon,
        subHeading: subHeading,
        description: {
            text: assetName,
        },
        note: {text: expiryText, style: {color: getExpiryTextColor(membership)}},
        actionViews: actionView,
        membershipResponse: membership,
        priority: PRIORITY.USER_ACTION_PENDING_SALES
    }
}

function addNormalMembershipCard(membership) {
    let newTagViewedCounter = 1;
    let heading = membership?.plan?.planName;
    let image = (membership?.categories?.length>1? images.ic_combo_plan:images.buy_plan_wallet_icon);
    let expiryText = getValidTillText(membership);
    let assetName = getAssetListText(membership);
    let actionViews = [];
    let statusView = {};
    let statusTagText = null;
    let description = null;
    let tag = {};
    let subHeading = '';
    let isABBOnlyActionText=(isABBOnlyMembership(membership) ? ButtonTexts.TXT_SELL_YOUR_PHONE : ButtonTexts.TXT_BOOK_A_SERVICE);
    if (membership?.isABBPlan) {
        subHeading = {
            text: MEMBERSHIP_TAB_CARD_TEXT.GET_GUARANTEED_EXCHANGE_MSG,
            style: {color: Colors.color_888F97, fontSize: 12}
        };
    } else if (membership?.doesPlanSupportsPMS) {
        subHeading = {
            text: MEMBERSHIP_TAB_CARD_TEXT.INCLUDES_PREVENTIVE_MAINTENANCE_SERVICE,
            style: {color: Colors.color_212121, fontSize: 12}
        };
    }
    if (newTagViewedCounter < 2) {
        tag.text = CARD_STATUS.NEW
        tag.textColor = Colors.white
        tag.borderColor = Colors.white
        tag.bgColor=getStatusTagColor(CARD_STATUS.NEW)
        statusTagText = CARD_STATUS.NEW;
        description = (newTagViewedCounter < 2 ? MEMBERSHIP_TAB_CARD_TEXT.FINANCE_CARD_BILL_PAYMENT_DESCRIPTION : '');
    }
    if (statusTagText && description) {
        statusView = {
            bgColor: getDescriptionColor(statusTagText),
            views: [
                {
                    type: "TEXT_WITH_BUTTON",
                    heading: null,
                    description: {text: description},
                    image: images.yellowInfo,
                }
            ]
        }
    }
    let dualActionButton = {};
    dualActionButton.type = "DUAL_ACTION_BUTTON";
    dualActionButton.leftButton = {
        text: VIEW_DETAIL,
        action: MembershipButtonActions.VIEW_DETAILS,
        data: {},
    };
    if (membership?.trial==='N' && membership?.categories?.length>1){
        dualActionButton.rightButton = {
            text: isABBOnlyActionText,
            isEnabled: membership?.enableFileClaim || false,
            action: MembershipButtonActions.RAISE_CLAIM,
            data: {},
        };
    }else{
        dualActionButton.rightButton = {
            text: ButtonTexts.MY_CARDS,
            isEnabled: true,
            action: MembershipButtonActions.MY_CARDS,
            data: {},
        };
    }
    actionViews.push(dualActionButton);

    return {
        heading: heading,
        image: image,
        subHeading: subHeading,
        description: {
            text: assetName,
            tappableText: {
                data: {},
            },
        },
        note: {text: expiryText, style: {color: getExpiryTextColor(membership)}},
        statusViews: statusView,
        tag: tag,
        actionViews: actionViews,
        membershipResponse: membership,
        priority: PRIORITY.ACTIVE_MEMBERSHIP_FINANCE
    }
}


function processTrialMembership(membership) {
    let heading = membership?.plan?.planName;
    let expiryText = getValidTillText(membership);
    let assetName = getAssetListText(membership);
    let isIdFenceMembership = checkIsMemIsIdFence(membership);
    let image = images.buy_plan_wallet_icon;
    let statusDesc = MEMBERSHIP_TAB_CARD_TEXT.TEXT_TRIAL_UPGRADE_DESCRIPTION;
    let statusView = {};
    let dualActionButton = {};
    let actionViews = [];
    let subHeading = '';
    if (membership?.isABBPlan) {
        subHeading = {
            text: MEMBERSHIP_TAB_CARD_TEXT.GET_GUARANTEED_EXCHANGE_MSG,
            style: {color: Colors.color_888F97, fontSize: 12}
        };
    } else if (membership?.doesPlanSupportsPMS) {
        subHeading = {
            text: MEMBERSHIP_TAB_CARD_TEXT.INCLUDES_PREVENTIVE_MAINTENANCE_SERVICE,
            style: {color: Colors.color_212121, fontSize: 12}
        };
    }
    if (!isIdFenceMembership) {
        statusView = {
            bgColor: getDescriptionColor(CARD_STATUS.IN_PROGRESS),
            button: {
                action: MembershipButtonActions.UPGRADE_TRIAL,
            },
            views: [
                {
                    type: "TEXT_WITH_BUTTON",
                    heading: [{text: statusDesc}],
                    description: null,
                    button: {
                        isArrowButton: false,
                        text: MEMBERSHIP_TAB_CARD_TEXT.UPGRADE_NOW,
                        action: MembershipButtonActions.UPGRADE_TRIAL,
                        data: {},
                    },
                }
            ]
        }
        dualActionButton.type = "DUAL_ACTION_BUTTON";
        dualActionButton.leftButton = {
            text: VIEW_DETAIL,
            action: MembershipButtonActions.VIEW_DETAILS,
            data: {},
        };
        dualActionButton.rightButton = {
            text: ButtonTexts.MY_CARDS,
            isEnabled: true,
            action: MembershipButtonActions.MY_CARDS,
            data: {},
        };
        actionViews.push(dualActionButton)
    } else {
        return null;
    }
    return {
        heading: heading,
        isSOP: membership.isSOP,
        image: image,
        subHeading: subHeading,
        note: {text: expiryText, style: {color: getExpiryTextColor(membership)}},
        description: {
            text: assetName,
        },
        membershipResponse: membership,
        statusViews: statusView,
        actionViews: actionViews,
        priority: PRIORITY.ACTIVE_MEMBERSHIP_HOME_APPLIANCE,
    }
}
