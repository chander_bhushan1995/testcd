import {
    CARD_STATUS,
    CATEGORY,
    MEMBERSHIP_TAB_CARD_TEXT, PRIORITY, RENEWAL_STATUS,
    TYPE
} from "../../Constants/MembershipConstants";
import {getSingleCategory} from "./MembershipDataFilters";
import {
    getAssetListText,
    getDescriptionColor, getExpiryTextColor, getMembershipRenewalDescription,
    getStatusTagColor, getValidTillText, isABBOnlyMembership,
    isExtendedWarrantyPlan,
    isUnderInspection, isUnderRenewal, isWhcActivatedMembership
} from "./MembershipTabUtils";
import images from "../../images/index.image";
import {APIData} from '../../../index';
import {MembershipCardActionsType} from "../Components/MembershipCardActionViews/MembershipCardActionContainer";
import {MembershipButtonActions} from "./MembershipButtonActions";
import {addDaysInDate, daysDifference, formattedDate} from "../../commonUtil/DateUtils";
import {addSRCard} from "./PEMembershipDataHelper";
import Colors from "../../Constants/colors";
import {ButtonTexts, VIEW_DETAIL} from "../../Constants/AppConstants";

/**
 *
 * Helper for Finance memberships to prepare data for UI
 */
export const addHomeApplianceUiMembership = (membershipList) => {

    // Get all HA memberships
    let haMembershipList = membershipList?.filter((membership) => CATEGORY.HOME_APPLIANCE === getSingleCategory(membership?.categories));
    let haUIModelList = [];
    let actualMembershipList = haMembershipList?.filter((membership) => (TYPE.CANCELED !== membership?.membershipStatus));
    actualMembershipList?.forEach(membership => {

        if (membership?.membershipStatus === TYPE.EXPIRED) {
            if (isUnderRenewal(membership)){
                let normalMembership = addNormalMembershipCard(membership);
                if (normalMembership){
                    haUIModelList.push(normalMembership);
                }
                return;
            }
        }
        // normal memberships
        let normalMembership = addNormalMembershipCard(membership);
        if (normalMembership) {
            haUIModelList.push(normalMembership);
            return;
        }
    });
    return haUIModelList;
}

function addNormalMembershipCard(membership) {
    let heading = getHAMembershipPlanName(membership);
    let image = isExtendedWarrantyPlan(membership) ? images.buy_plan_ew_icon : images.buy_plan_ha_icon;
    let assetName = getAssetListText(membership);
    let expiryText = getValidTillText(membership);
    let subHeading='';

    if (membership?.isABBPlan){
        subHeading={text: MEMBERSHIP_TAB_CARD_TEXT.GET_GUARANTEED_EXCHANGE_MSG,style:{color:Colors.color_888F97,fontSize:12}};
    }else if (membership?.doesPlanSupportsPMS){
        subHeading={text: MEMBERSHIP_TAB_CARD_TEXT.INCLUDES_PREVENTIVE_MAINTENANCE_SERVICE,style:{color:Colors.color_212121,fontSize:12}};
    }
    if (isUnderInspection(membership)) {
        let underInspectionMembership = processPendingMembership(membership);
        if (underInspectionMembership) {
            underInspectionMembership.heading = heading;
            underInspectionMembership.subHeading=subHeading;
            underInspectionMembership.isSOP = membership.isSOP;
            underInspectionMembership.image = image;
            underInspectionMembership.membershipResponse= membership;
            underInspectionMembership.note = {text: expiryText, style: {color: getExpiryTextColor(membership)}};
            underInspectionMembership.description = {
                text: assetName,
            }
            return underInspectionMembership;
        }
    }

   let srMembershipCard = addSRCard(membership,CATEGORY.HOME_APPLIANCE,false);
    if (srMembershipCard) {
        return srMembershipCard;
    }
    let renewMemberships = addRenewMemberships(membership);
    if (renewMemberships) {
        renewMemberships.heading = heading;
        renewMemberships.subHeading=subHeading;
        renewMemberships.isSOP = membership.isSOP;
        renewMemberships.image = image;
        renewMemberships.note = {text: expiryText, style: {color: 'red'}};
        renewMemberships.description = {
            text: assetName,
        }
        renewMemberships.membershipResponse= membership;
        return renewMemberships;
    }
    if (isWhcActivatedMembership(membership) && (daysDifference(membership?.startDate, new Date().getTime())*24) <= 24 * 2) {
        return {
            heading : heading,
            isSOP : membership.isSOP,
            subHeading:subHeading,
            image : image,
            note : {text: expiryText, style: {color: getExpiryTextColor(membership)}},
            description : {
                text: assetName,
            },
            membershipResponse: membership,
            ...addWhcActivatedMembership(membership)
        };
    }
    if (!membership?.memId && !membership?.task) {
        return {
            heading: heading,
            isSOP: membership.isSOP,
            image: image,
            subHeading:subHeading,
            note : {text: expiryText, style: {color: getExpiryTextColor(membership)}},
            description: {
                text: assetName,
            },
            membershipResponse: membership,
            ...processPendingPEMembership(membership),
        }
    }else{
        let actionViews = [];
        let dualActionButton = {};
        dualActionButton.type = "DUAL_ACTION_BUTTON";
        dualActionButton.leftButton = {
            text: VIEW_DETAIL,
            action: MembershipButtonActions.VIEW_DETAILS,
            data: {},
        };
        dualActionButton.rightButton = {
            text: ButtonTexts.TXT_BOOK_A_SERVICE,
            isEnabled: membership?.enableFileClaim || false,
            action: MembershipButtonActions.RAISE_CLAIM,
            data: {},
        };
        actionViews.push(dualActionButton)

        return {
            heading: heading,
            isSOP: membership.isSOP,
            image: image,
            subHeading:subHeading,
            note : {text: expiryText, style: {color: getExpiryTextColor(membership)}},
            description: {
                text: assetName,
            },
            actionViews: actionViews,
            membershipResponse: membership,
            priority: PRIORITY.ACTIVE_MEMBERSHIP_HOME_APPLIANCE,
        }
    }
    return null;
}

function processPendingPEMembership(membership) {
    let expiryText = "";
    if (membership?.uploadDocsLastDate) {
        expiryText = MEMBERSHIP_TAB_CARD_TEXT.ACTIVATE_BY(formattedDate(new Date(new Number(membership?.uploadDocsLastDate)), 'dd mmm yyyy'));
    }
    let actionView = [{
        type: MembershipCardActionsType.PRIMARY_BUTTON,
        button: {
            text: MEMBERSHIP_TAB_CARD_TEXT.ACTIVATE_NOW_TEXT,
            action: MembershipButtonActions.ACTIVATE_HOME_EW_MEMBERSHIP,
            data: {},
        }
    }]
    return {
        note: {text: expiryText, style: {color: getExpiryTextColor(membership)}},
        actionViews: actionView,
        priority: PRIORITY.USER_ACTION_PENDING_SALES
    }
}

function addWhcActivatedMembership(membership) {
    let isABBOnlyActionText=(isABBOnlyMembership(membership) ? ButtonTexts.TXT_SELL_YOUR_PHONE : ButtonTexts.TXT_BOOK_A_SERVICE);
    let tag = {};
    let actionViews = [];
    let dualActionButton = {};
    tag.text = MEMBERSHIP_TAB_CARD_TEXT.TEXT_ACTIVATED_SUCCESSFULLY.toUpperCase()
    tag.textColor = getStatusTagColor(CARD_STATUS.ACTIVATED_SUCCESSFULLY)
    tag.borderColor = getStatusTagColor(CARD_STATUS.ACTIVATED_SUCCESSFULLY)
    dualActionButton.type = "DUAL_ACTION_BUTTON";
    dualActionButton.leftButton = {
        text: VIEW_DETAIL,
        action: MembershipButtonActions.VIEW_DETAILS,
        data: {},
    };
    dualActionButton.rightButton = {
        text: isABBOnlyActionText,
        isEnabled: membership?.enableFileClaim || false,
        action: MembershipButtonActions.RAISE_CLAIM,
        data: {},
    };
    actionViews.push(dualActionButton)
    return {
        tag: tag,
        actionViews: actionViews,
        priority: PRIORITY.ACTIVE_MEMBERSHIP_HOME_APPLIANCE,
    }
}

function addRenewMemberships(membership) {
    let isABBOnlyActionText=(isABBOnlyMembership(membership) ? ButtonTexts.TXT_SELL_YOUR_PHONE : ButtonTexts.TXT_BOOK_A_SERVICE);
    if (isUnderRenewal(membership)) {
        let statusView = {};
        let priority = PRIORITY.ACTION_RENEW;
        let actionViews = [];
        let description = getMembershipRenewalDescription(membership);
        let dualActionButton = {};
        statusView = {
            bgColor: Colors.BG_DESCRIPTION_RED,
            button: {
                action: MembershipButtonActions.RENEW_NOW,
                data: {}
            },
            views: [
                {
                    type: "TEXT_WITH_BUTTON",
                    heading: description,
                    description:null,
                    button: {
                        isArrowButton: true,
                        text: MEMBERSHIP_TAB_CARD_TEXT.RENEW_NOW,
                        action: MembershipButtonActions.RENEW_NOW,
                        data: {},
                    },
                }
            ]
        };
        dualActionButton.type = "DUAL_ACTION_BUTTON";
        dualActionButton.leftButton = {
            text: VIEW_DETAIL,
            action: MembershipButtonActions.VIEW_DETAILS,
            data: {},
        };

        dualActionButton.rightButton = {
            text: isABBOnlyActionText,
            isEnabled: membership?.enableFileClaim || false,
            action: MembershipButtonActions.RAISE_CLAIM,
            data: {},
        };
        actionViews.push(dualActionButton)

        return {
            statusViews: statusView,
            priority: priority,
            actionViews: actionViews,
            membershipResponse: membership,
        }
    }
    return null;
}

function processPendingMembership(membership) {
    let task = membership?.task;
    let membershipStatus = membership?.membershipStatus;
    let tag = {};
    let statusView;
    let priority = PRIORITY.DEFAULT;
    let actionViews = [];
    let memStatus = CARD_STATUS.INSPECTION_PENDING;
    let customerCareNum=APIData?.CUSTOMER_CARE_NUM || "1800 123 3330";
    if (["X"].includes(membershipStatus) || ["CEXP"].includes(task?.status)) {
        //WHC_MEM_AUTO_CANCEL
        memStatus = CARD_STATUS.MEM_CANCELLED;
        tag = {
            text: MEMBERSHIP_TAB_CARD_TEXT.MEMBERSHIP_CANCELLED,
            textColor: getStatusTagColor(CARD_STATUS.MEM_CANCELLED),
            borderColor: getStatusTagColor(CARD_STATUS.MEM_CANCELLED),
        }
        priority = PRIORITY.CANCELLED_MEMBERSHIP;
        statusView = {
            bgColor: getDescriptionColor(CARD_STATUS.MEM_CANCELLED),
            button: {
                action: "",
            },
            views: [
                {
                    type: "TEXT_WITH_BUTTON",
                    heading: null,
                    description: {text:MEMBERSHIP_TAB_CARD_TEXT.AS_INSPECTION_WASNT_SCHEDULED_YOUR_MEMBERSHIP_CANCELLED},

                }
            ]
        }
    } else if (["S", "INCS"].includes(task?.status) ) {
        //WHC_AWAITING_CONFIRMATION
        memStatus = CARD_STATUS.AWAITING_CONFIRMATION;
        tag = {
            text: MEMBERSHIP_TAB_CARD_TEXT.AWAITING_CONFIRMATION,
            textColor: getStatusTagColor(CARD_STATUS.AWAITING_CONFIRMATION),
            borderColor: getStatusTagColor(CARD_STATUS.AWAITING_CONFIRMATION),
        }
        priority = PRIORITY.OA_ACTION_PENDING_INSPECTION;
        statusView = {
            bgColor: getDescriptionColor(CARD_STATUS.AWAITING_CONFIRMATION),
            button: {
                action: "",
            },
            views: [
                {
                    type: "TEXT_WITH_BUTTON",
                    heading: null,
                    description:{text: MEMBERSHIP_TAB_CARD_TEXT.TEXT_INSPECTION_REQUEST_UNDER_PROCESS},
                    image: images.yellowInfo,
                }
            ]
        }
    } else if (["P", "SCNA", "TEA", "INI", "RSD"].includes(task?.status)) {
        //WHC_INSPECTION_SCHEDULED
        memStatus = CARD_STATUS.INSPECTION_SCHEDULED;

        tag = {
            text: CARD_STATUS.LABLE_INSPECTION_SCHEDULED,
            textColor: getStatusTagColor(CARD_STATUS.INSPECTION_SCHEDULED),
            borderColor: getStatusTagColor(CARD_STATUS.INSPECTION_SCHEDULED),
        }
        priority = PRIORITY.OA_ACTION_PENDING_INSPECTION;
        actionViews = [];
        actionViews.push(
            {
                type: MembershipCardActionsType.PRIMARY_BUTTON,
                "button": {
                    "text": MEMBERSHIP_TAB_CARD_TEXT.TXT_VIEW_INSPECTION_DETAILS,
                    action: MembershipButtonActions.VIEW_INSPECTION_DETAILS,
                    data: {},
                },
            })
    } else if (["INF"].includes(task?.status)) {
        //WHC_INSPECTION_FAILED
        tag = {
            text: MEMBERSHIP_TAB_CARD_TEXT.INSPECTION_FAILED,
            textColor: getStatusTagColor(CARD_STATUS.INSPECTION_FAILED),
            borderColor: getStatusTagColor(CARD_STATUS.INSPECTION_FAILED),
        }
        memStatus = CARD_STATUS.INSPECTION_FAILED;
        priority = PRIORITY.USER_ACTION_PENDING_INSPECTION;
        statusView = {
            bgColor: getDescriptionColor(CARD_STATUS.INSPECTION_FAILED),
            button: {
                action: "",
            },
            views: [
                {
                    type: "TEXT_WITH_BUTTON",
                    heading: null,
                    description:{text:  MEMBERSHIP_TAB_CARD_TEXT.INSPECTION_FAILED_DESCRIPTION},
                    image: images.yellowInfo,
                }
            ]
        }
    } else if (["U"].includes(task?.status)) {
        //WHC_INSPECTION_NOT_SCHEDULED
        tag = {
            text: MEMBERSHIP_TAB_CARD_TEXT.INSPECTION_PENDING,
            textColor: getStatusTagColor(CARD_STATUS.INSPECTION_PENDING),
            borderColor: getStatusTagColor(CARD_STATUS.INSPECTION_PENDING),
        }
        memStatus = CARD_STATUS.INSPECTION_PENDING;
        priority = PRIORITY.USER_ACTION_PENDING_INSPECTION;
        let descriptionStatusView = "";
        if ((daysDifference(membership?.purchaseDate, new Date().getTime())*24) <= 24 * 10) {
            let dateAfterAddingDays = addDaysInDate(membership?.purchaseDate, 9)
            let formattedDateString = formattedDate(dateAfterAddingDays.getTime(), 'dd mmm, yyyy')
            if ([RENEWAL_STATUS.IN_RENEWAL_WINDOW, RENEWAL_STATUS.UPGRADE].includes(membership?.activity)) {
                descriptionStatusView = MEMBERSHIP_TAB_CARD_TEXT.PLEASE_SCHEDULE_INSPECTION_BEFORE(formattedDateString);
            } else {
                descriptionStatusView = MEMBERSHIP_TAB_CARD_TEXT.PLEASE_SCHEDULE_INSPECTION_BEFORE_1(formattedDateString);
            }
            statusView = {
                bgColor: getDescriptionColor(CARD_STATUS.INSPECTION_PENDING),
                button: {
                    action: "",
                },
                views: [
                    {
                        type: "TEXT_WITH_BUTTON",
                        heading: null,
                        description: {text:descriptionStatusView},
                        image: images.yellowInfo,
                    }
                ]
            };
            actionViews = [];
            actionViews.push(
                {
                    type: MembershipCardActionsType.PRIMARY_BUTTON,
                    "button": {
                        "text": MEMBERSHIP_TAB_CARD_TEXT.SCHEDULE_INSPECTION,
                        action: MembershipButtonActions.SELECT_ADDRESS_INSPECTION,
                        data: {},
                    },
                })
        } else if ((daysDifference(membership?.startDate, new Date().getTime())*24) > 24 * 10 &&
            (daysDifference(membership?.startDate, new Date().getTime())*24) < 24 * (30 - 7)) {
            statusView = {
                bgColor: getDescriptionColor(CARD_STATUS.INSPECTION_PENDING),
                button: {
                    action: "",
                },
                views: [
                    {
                        type: "TEXT_WITH_BUTTON",
                        heading: null,
                        description: {text:MEMBERSHIP_TAB_CARD_TEXT.TEXT_INSPECTION_NOT_SCHEDULE_GET_IN_TOUCH},
                        image: images.yellowInfo,
                    }
                ]
            };
            actionViews = [];
            actionViews.push(
                {
                    "type": "TEXT_WITH_RIGHT_IMAGE",
                    "heading": "For further help, Call us at",
                    "description": customerCareNum,
                    "button": {
                        "image": images.ic_call_blue,
                        "action": MembershipButtonActions.CALL_US,
                        "data": {
                            "number": customerCareNum,
                        },
                    },
                })
        } else if ((daysDifference(membership?.purchaseDate, new Date().getTime())*24) > 24 * (30 - 7)) {
            statusView = {
                bgColor: getDescriptionColor(CARD_STATUS.INSPECTION_PENDING),
                button: {
                    action: "",
                },
                views: [
                    {
                        type: "TEXT_WITH_BUTTON",
                        heading: null,
                        description: {text:MEMBERSHIP_TAB_CARD_TEXT.TEXT_SCHEDULE_INSPECTION_OR_WILL_CANCEL},
                        image: images.yellowInfo,
                    }
                ]
            };
            actionViews = [];
            actionViews.push(
                {
                    "type": "TEXT_WITH_RIGHT_IMAGE",
                    "heading": "For further help, Call us at",
                    "description": customerCareNum,
                    "button": {
                        "image": images.ic_call_blue,
                        "action": MembershipButtonActions.CALL_US,
                        "data": {
                            "number": customerCareNum,
                        },
                    },
                })
        }
    } else if (["CTNA"].includes(task?.status)) {
        //INSPECTION_CANCELLED
        tag = {
            text: MEMBERSHIP_TAB_CARD_TEXT.INSPECTION_CANCELLED,
            textColor: getStatusTagColor(CARD_STATUS.INSPECTION_CANCELLED),
            borderColor: getStatusTagColor(CARD_STATUS.INSPECTION_CANCELLED),
        }
        memStatus = CARD_STATUS.INSPECTION_CANCELLED;
        priority = PRIORITY.USER_ACTION_PENDING_INSPECTION;
        let descriptionStatusView = "";
        let dateAfterAddingDays = addDaysInDate(membership?.purchaseDate, 9)
        let formattedDateString = formattedDate(dateAfterAddingDays.getTime(), 'dd mmm, yyyy')

        if ((daysDifference(membership?.startDate, new Date().getTime())*24) <= 24 * 10) {
            descriptionStatusView = MEMBERSHIP_TAB_CARD_TEXT.TEXT_PREVIOUS_CANCELLED_RESCHEDULE(formattedDateString);
            statusView = {
                bgColor: getDescriptionColor(CARD_STATUS.INSPECTION_CANCELLED),
                button: {
                    action: "",
                },
                views: [
                    {
                        type: "TEXT_WITH_BUTTON",
                        heading: null,
                        description: {text:descriptionStatusView},
                        image: images.yellowInfo,
                    }
                ]
            };
            actionViews = [];
            actionViews.push(
                {
                    type: MembershipCardActionsType.PRIMARY_BUTTON,
                    "button": {
                        "text": MEMBERSHIP_TAB_CARD_TEXT.RE_SCHEDULE,
                        action: MembershipButtonActions.SCHEDULE_INSPECTION,
                        data: {},
                    },
                })
        } else if ((daysDifference(membership?.startDate, new Date().getTime())*24) > 24 * 10) {
            statusView = {
                bgColor: getDescriptionColor(CARD_STATUS.INSPECTION_CANCELLED),
                button: {
                    action: "",
                },
                views: [
                    {
                        type: "TEXT_WITH_BUTTON",
                        heading: null,
                        description: {text:MEMBERSHIP_TAB_CARD_TEXT.TEXT_PREVIOUS_CANCELLED_DUE_TO_UNAVAILABILITY},
                        image: images.yellowInfo,
                    }
                ]
            };
            actionViews = [];
            actionViews.push(
                {
                    "type": "TEXT_WITH_RIGHT_IMAGE",
                    "heading": "For further help, Call us at",
                    "description": customerCareNum,
                    "button": {
                        "image": images.ic_call_blue,
                        "action": MembershipButtonActions.CALL_US,
                        "data": {
                            "number": customerCareNum,
                        },
                    },
                })
        }
    } else {
        return null;
    }
    return {
        tag: tag,
        statusViews: statusView,
        priority: priority,
        actionViews: actionViews,
        membershipResponse: membership,
        memStatus: memStatus
    }
}

export const getHAMembershipPlanName = (membership) => {
    let UNLIMITED_APPLIANCES_COUNT = parseInt(APIData.UNLIMITED_APPLIANCES_COUNT) || 0;
    let ALLOWED_MAX_QUANTITY = membership?.plan?.allowedMaxQuantity || 0;


    if (membership?.isSOP && ALLOWED_MAX_QUANTITY > 1) {

        if (ALLOWED_MAX_QUANTITY >= UNLIMITED_APPLIANCES_COUNT) {
            return MEMBERSHIP_TAB_CARD_TEXT.GADGETSERV_PLAN_UNLIMITED_APPLIANCES_COVERED;
        }
        return MEMBERSHIP_TAB_CARD_TEXT.GADGETSERV_PLAN_UPTO_X_APPLIANCES_COVERED(ALLOWED_MAX_QUANTITY);
    }
    if (membership?.assets?.length > 0 && !isExtendedWarrantyPlan(membership)) {
        if (membership?.assets?.length == 1) {
            if (membership?.assets?.[0]?.name) {
                return MEMBERSHIP_TAB_CARD_TEXT.HOMEASSIST_PLAN_COVERED_1(membership?.assets?.[0].name);
            }
            return MEMBERSHIP_TAB_CARD_TEXT.HOMEASSIST_PLAN_COVERED_2(membership?.assets?.length);
        } else {
            return MEMBERSHIP_TAB_CARD_TEXT.HOMEASSIST_PLAN_COVERED_3(membership?.assets?.length);
        }
    }
    return membership?.plan?.planName;
}
