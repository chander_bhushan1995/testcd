import {
    addDaysInDate,
    compareDate,
    daysDifference,
    formattedDate,
    getCurretDateMilliseconds,
    minutesDifference,
} from "../../commonUtil/DateUtils";
import { AppForAppFlowStrings } from "../../AppForAll/Constant/AppForAllConstants";
import {
    ButtonTexts,
    IDFENCE_ASSETS_TEXT,
    IDFENCE_PENDING_MEMBERSHIP_TEXT,
    IDFenceTexts,
    VIEW_DETAIL,
} from "../../Constants/AppConstants";
import { MembershipButtonActions } from "./MembershipButtonActions";
import { MembershipCardActionsType } from "../Components/MembershipCardActionViews/MembershipCardActionContainer";
import { MembershipStatus } from "../../commonUtil/MembershipsUtilMethods";
import images from "../../images/index.image";
import colors from "../../Constants/colors";
import { MembershipStatusViewType } from "../Components/MembershipStatusViews/MembershipStatusViewContainer";
import {ID_FENCE_DARK_WEB_MONITORING, MEMBERSHIP_TAB_CARD_TEXT, PRIORITY} from "../../Constants/MembershipConstants";

export const prepareDataForIDFenceDashboard = (idFenceMemRes) => {
    let daysDiff = daysDifference(getCurretDateMilliseconds(), idFenceMemRes?.endDate);

    let orderInfo = {};
    orderInfo.partnerBUCode = AppForAppFlowStrings.partnerBUCode;
    orderInfo.planCode = idFenceMemRes?.plan?.planCode;
    orderInfo.paymentMode = null;
    orderInfo.applicationNo = null;
    orderInfo.partnerCode = AppForAppFlowStrings.partnerCode;
    orderInfo.promoCode = null;

    let customerInfo = null;
    customerInfo = idFenceMemRes.customers[0];

    let renewalDataDict = {};
    renewalDataDict.orderInfo = orderInfo;
    renewalDataDict.customerInfo = [customerInfo];
    renewalDataDict.membershipId = idFenceMemRes.memId;
    renewalDataDict.initiatingSystem = AppForAppFlowStrings.initiatingSystem;


    let membershipDataDict = {};
    membershipDataDict.subscriberNo = idFenceMemRes.memUUID;
    membershipDataDict.graceDays = idFenceMemRes.graceDays;
    membershipDataDict.isTrial = idFenceMemRes.trial === "Y";
    membershipDataDict.memRemainingDays = daysDiff;
    membershipDataDict.memId = idFenceMemRes.memId;
    membershipDataDict.planCode = idFenceMemRes.plan?.planCode;
    membershipDataDict.planName = idFenceMemRes.plan?.planName;
    membershipDataDict.startDate = idFenceMemRes.startDate;
    membershipDataDict.endDate = idFenceMemRes.endDate;
    membershipDataDict.membershipStatus = idFenceMemRes.membershipStatus;
    membershipDataDict.salesPrice = idFenceMemRes.salesPrice;
    membershipDataDict.coverAmount = idFenceMemRes.plan?.coverAmount;
    membershipDataDict.mobileNumber = idFenceMemRes.customers[0].mobileNumber;
    membershipDataDict.partnerCode = idFenceMemRes?.partnerCode;
    membershipDataDict.partnerBUCode = idFenceMemRes?.partnerBUCode;
    membershipDataDict.isSIEnabled = idFenceMemRes?.isSIEnabled;

    let renewalDataStringified = JSON.stringify(renewalDataDict);
    let membershipDataText = JSON.stringify(membershipDataDict);
    return { membershipDataText: membershipDataText, renewalDataStringified: renewalDataStringified };
};


export const getIDFenceCardViewModel = (membership) => {

    let plan = membership?.plan;
    let description = { text: IDFENCE_ASSETS_TEXT };
    let canRenew = false;
    let isTrial = membership?.trial == "Y";
    let isSIEnabled = membership?.isSIEnabled;
    let graceEndDate = addDaysInDate(membership?.endDate,membership?.graceDays).getTime()
    let buttonTitle = ButtonTexts.UPGRADE_NOW;
    let buttonAction = MembershipButtonActions.UPGRADE_IDFENCE;
    let IDFenceText = null;
    let statusViews = null;
    let tag = null;
    let statusImage = null, tagColor: null, statusColor: null;

    let actionViews = [
        {
            "type": MembershipCardActionsType.PRIMARY_BUTTON,
            "button": {
                "text": VIEW_DETAIL,
                "action": MembershipButtonActions.CHECK_FOR_ID_FENCE_DASHBOARD,
                "data": {},
            },
        },
    ];

    if (!membership?.isPendingMembership) { // if membership is not a pending membership
        if (isTrial) {
            canRenew = !isSIEnabled;
            if (membership?.membershipStatus == MembershipStatus.cancelled) {
                IDFenceText = IDFenceTexts.MEMBERSHIP_CANCELED;
                buttonTitle = ButtonTexts.BUY_NOW;
                buttonAction = MembershipButtonActions.BUY_NOW;
                canRenew = false; // here forcefully set to true to show secondary action doesn't matter where it trial with card
            } else if (compareDate(getCurretDateMilliseconds(), membership?.endDate) && !isSIEnabled) {
                let daysDiff = daysDifference(getCurretDateMilliseconds(), membership?.endDate);
                let minutesDiff = minutesDifference(getCurretDateMilliseconds(), membership?.endDate);
                if (minutesDiff - (daysDiff * 24 * 60) > 0) {
                    daysDiff += 1;
                }
                IDFenceText = IDFenceTexts.TRAIL_ACTIVE(daysDiff);
            } else if (compareDate(membership?.endDate, getCurretDateMilliseconds()) && compareDate(getCurretDateMilliseconds(), graceEndDate)) {
                let daysDiff = daysDifference(getCurretDateMilliseconds(), membership?.endDate);
                let minutesDiff = minutesDifference(getCurretDateMilliseconds(), membership?.endDate);
                if (minutesDiff - (daysDiff * 24 * 60) > 0) {
                    daysDiff += 1;
                }
                IDFenceText = !isSIEnabled ? IDFenceTexts.TRAIL_EXPIRED(daysDiff) : null;
                if ((isSIEnabled) && (membership?.siStatus == MembershipStatus.cancelled || membership?.siStatus == MembershipStatus.failed)) {
                    canRenew = false;
                    IDFenceText = IDFenceTexts.TRAIL_MEM_EXPIRED(daysDiff);
                }
            } else if (membership?.membershipStatus == MembershipStatus.inactive) {
                canRenew = true; // here forcefully set to true to show secondary action doesn't matter where it trial with card or without card
                IDFenceText = IDFenceTexts.TRAIL_INACTIVE;
            }
        } else {
            if (membership?.membershipStatus == MembershipStatus.cancelled) {
                canRenew = false;
                buttonTitle = ButtonTexts.BUY_NOW;
                buttonAction = MembershipButtonActions.BUY_NOW;
                IDFenceText = IDFenceTexts.TRAIL_CANCELLED_BUY_NOW;
            } else if (compareDate(membership?.endDate, getCurretDateMilliseconds()) && compareDate(getCurretDateMilliseconds(), graceEndDate)) {
                if ((membership?.siStatus == MembershipStatus.cancelled || membership?.siStatus == MembershipStatus.failed)) {
                    let daysDiff = daysDifference(getCurretDateMilliseconds(), membership?.endDate);
                    let minutesDiff = minutesDifference(getCurretDateMilliseconds(), membership?.endDate);
                    if (minutesDiff - (daysDiff * 24 * 60) > 0) {
                        daysDiff += 1;
                    }
                    IDFenceText = IDFenceTexts.PREMIUM_EXPIRED(daysDiff);
                }
            } else if (compareDate(graceEndDate, getCurretDateMilliseconds())) {
                canRenew = true;
                buttonTitle = ButtonTexts.REACTIVATE_NOW;
                buttonAction = MembershipButtonActions.UPGRADE_IDFENCE;
                IDFenceText = IDFenceTexts.PREMIUM_INACTIVE;
            }
        }

        // here prepare card data
        if (([MembershipStatus.cancelled, MembershipStatus.inactive].includes(membership?.membershipStatus)) || (compareDate(membership?.endDate, getCurretDateMilliseconds()))) {
            if (membership?.membershipStatus == MembershipStatus.active) {
                if (membership?.siStatus == MembershipStatus.active) {
                    statusImage = images.yellowInfo;
                    statusColor = colors.backgroundInProgress;
                    tagColor = colors.saffronYellow;
                } else if (membership?.siStatus == MembershipStatus.failed) {
                    statusImage = images.dlsCanceled;
                    statusColor = colors.backgroundError;
                    tagColor = colors.errorMessage;
                }
            } else {
                statusImage = images.dlsCanceled;
                statusColor = colors.backgroundError;
                tagColor = colors.errorMessage;
            }
        } else {
            statusImage = images.dlsCanceled;
            statusColor = colors.backgroundError;
            tagColor = colors.errorMessage;
        }

        if (canRenew || membership?.membershipStatus == MembershipStatus.cancelled) {
            statusViews = {
                "bgColor": statusColor,
                "button": {
                    "action": buttonAction,
                    "data": {},
                },
                "views": [
                    {
                        "type": MembershipStatusViewType.TEXT_WITH_BUTTON,
                        "heading": [{text: IDFenceText}],
                        "image": null,
                        "button": {
                            "isArrowButton": true,
                            "text": buttonTitle,
                            "action": buttonAction,
                            "data": {},
                        },
                    },
                ],
            };
            if (membership?.membershipStatus == MembershipStatus.cancelled) { // if membership cancelled view detail button would be hidden
                actionViews = null;
            }
        } else if (IDFenceText) {
            statusViews = {
                "bgColor": statusColor,
                "views": [
                    {
                        "type": MembershipStatusViewType.TEXT_WITH_BUTTON,
                        "heading": null,
                        "image": statusImage,
                        "description": { text: IDFenceText },
                    },
                ],
            };
        } else {
            statusViews = null;
        }
    } else {
        tag = {
            "text": "IN PROGRESS",
            "textColor": colors.saffronYellow,
            "borderColor": colors.saffronYellow,
        };

        statusViews = {
            "bgColor": colors.backgroundInProgress,
            "views": [
                {
                    "type": MembershipStatusViewType.TEXT_WITH_BUTTON,
                    "heading": null,
                    "image": images.yellowInfo,
                    "description": {text: IDFENCE_PENDING_MEMBERSHIP_TEXT},
                },
            ],
        };

        actionViews = null;
    }
    let noteText = null, noteColor = canRenew ? colors.errorMessage : colors.grey;
    if (membership?.endDate) {
        noteText = `Valid till ${formattedDate(addDaysInDate(membership?.endDate ?? getCurretDateMilliseconds(), 0), "dS mmm yyyy")}`;
    } else if (membership?.uploadDocsLastDate) {
        noteText = `Activate by ${formattedDate(addDaysInDate(membership?.uploadDocsLastDate ?? getCurretDateMilliseconds(), 0), "dS mmm yyyy")}`;
    }
    return {
        "heading": plan?.planName,
        "image": images.idFence,
        "subHeading": null,
        "description": description,
        "note": { text: noteText, style: { color: noteColor } },
        "tag": tag,
        "statusViews": statusViews,
        "actionViews": actionViews,
        membershipResponse: membership,
        priority:PRIORITY.DEFAULT
    };
};

export const getAssetsTextForIDFence = (isTrial,membership) => {
    if (isTrial){
        return MEMBERSHIP_TAB_CARD_TEXT.IDFENCE_TRIAL_PLAN_DESC;
    }
    if(membership?.plan?.services.some(service=>(ID_FENCE_DARK_WEB_MONITORING===service?.serviceName))){
        return MEMBERSHIP_TAB_CARD_TEXT.IDFENCE_TRIAL_PLAN_DESC_1
    }
    return MEMBERSHIP_TAB_CARD_TEXT.IDFENCE_TRIAL_PLAN_DESC_1
}
