import {getFirebaseData, openDialerWith} from "../../commonUtil/AppUtils";
import {NativeModules, Platform} from "react-native";
import {
    isIDFenceMembership,
    MembershipStatus,
} from "../../commonUtil/MembershipsUtilMethods";
import {APIData} from '../../../index';
import {buybackStatusConstants} from "../../Constants/BuybackConstants";
import {logWebEnageEvent} from "../../commonUtil/WebengageTrackingUtils";
import * as WebEngageEventsName from "../../Constants/WebengageEvents";
import * as WebEngageKeys from "../../Constants/WebengageAttrKeys";
import {WEBENGAGE_IDFENCE_DASHBOARD} from "../../Constants/WebengageAttrKeys";
import {
    BuybackComboPlanServices,
    CARD_FRAUD_FOR_WALLET,
    Category,
    CLAIM_NOT_ALLOWED, DARK_WEB_MONITORING_STRING, HOME_EW, IDFENCE_STRING,
    PlanServices, PLATFORM_OS,
    WALLET_STRING, YOUR_APPLIANCES,
} from "../../Constants/AppConstants";
import {AppForAppFlowStrings} from "../../AppForAll/Constant/AppForAllConstants";
import {RENEW_MESSAGE_HA} from "../../Constants/FirebaseConstants";
import {CATEGORY, IDFENCE_SUBSCRIPTION_STATUS} from "../../Constants/MembershipConstants";
import {
    isABBMembership,
    isABBOnlyMembership,
    isHAEWMembership,
    isHAMembership,
    isPEMembership, isWalletMembership, isWHCMembership,
} from "./MembershipTabUtils";
import {prepareDataForIDFenceDashboard} from "./IDFenceHelper";
import {CommonStyle} from "../../Constants/CommonStyle";
import images from "../../images/index.image";
import {Deeplink} from "../../Constants/Deeplinks";
import {getServiceFromMembership} from "./PEMembershipDataHelper";

const nativeBridgeRef = NativeModules.ChatBridge;

export const MembershipButtonActions = {
    VIEW_DETAILS: "VIEW_DETAILS",
    MY_CARDS: "MY_CARDS", // card management activity
    ACTIVATE_FINANCE_MEMBERSHIP: "ACTIVATE_FINANCE_MEMBERSHIP", // activation for wallet membership

    SCHEDULE_INSPECTION: "SCHEDULE_INSPECTION", // HA Schedule inspection
    SELECT_ADDRESS_INSPECTION: "SELECT_ADDRESS_INSPECTION", // select address
    BUY_NOW: "BUY_NOW", // IDFence plan detail
    RAISE_CLAIM: "RAISE_CLAIM",
    RENEW_NOW: "RENEW_NOW", // tested for ha wallet is remaining
    CHECK_FOR_ID_FENCE_DASHBOARD: "CHECK_FOR_ID_FENCE_DASHBOARD", // open IDFence Dashboard
    ACTIVATE_HOME_EW_MEMBERSHIP: "ACTIVATE_HOME_EW_MEMBERSHIP",
    RESUME_CLAIM: "RESUME_CLAIM",
    VIEW_INSPECTION_DETAILS: "VIEW_INSPECTION_DETAILS",
    UPGRADE_TRIAL_IDFENCE: "UPGRADE_TRIAL_IDFENCE",
    UPGRADE_IDFENCE: "UPGRADE_IDFENCE",
    SR_UPLOAD_DOCUMENT: "SR_UPLOAD_DOCUMENT", // upload document
    SR_VIEW_TIMELINE: "SR_VIEW_TIMELINE", // service timeline
    ACTIVATE_PE_MEMBERSHIP: "ACTIVATE_PE_MEMBERSHIP", // PE ,
    SALES_UPLOAD_DOCUMENTS: "SALES_UPLOAD_DOCUMENTS",
    SR_MULTIPLE_STATUS: "SR_MULTIPLE_STATUS", // multiple services screen
    CALL_US: "CALL_US",
    SR_UPLOAD_PENDENCIES: "SR_UPLOAD_PENDENCIES", // reupload
    FD_OPEN_SEND_SMS_SCREEN: "FD_OPEN_SEND_SMS_SCREEN", // open send sms screen
    PENDING_SUBMISSION: "PENDING_SUBMISSION", //
    RATE_US: "RATE_US",   //

    SR_RESCHEDULE_VISIT: "SR_RESCHEDULE_VISIT",
    DOWNLOAD_INVOICE: "DOWNLOAD_INVOICE",
    SHOW_ASSETS_BOTTOMSHEET: "SHOW_ASSETS_BOTTOMSHEET",
    OPEN_BUYBACK_TIME_LINE: "OPEN_BUYBACK_TIME_LINE",
    // RENEWAL_API_CALL_ERROR_RESPONSE: "RENEWAL_API_CALL_ERROR_RESPONSE", // remove
    // UPGRADE_TRIAL: "UPGRADE_TRIAL", // now PE TRIAL NOT THERE remove this action
    // // SALES_UPLOAD_DOCUMENTS: "SALES_UPLOAD_DOCUMENTS",  // no need it could be handled by ACTIVATE_PE_MEMBERSHIP and check stage of mem in native
    // OPEN_CHAT_FOR_RAISE_CLAIM: "OPEN_CHAT_FOR_RAISE_CLAIM",  // LEAVE it for now as discussed with imran
    // OPEN_CHAT_FOR_RESUME_CLAIM: "OPEN_CHAT_FOR_RESUME_CLAIM", // LEAVE it for now as discussed with imran
    // SHOW_CHECK_ELIGIBILITY_ERROR: "SHOW_CHECK_ELIGIBILITY_ERROR", // not needed
    // SHOW_DIALOG_TO_REFRESH_MEMBERSHIP: "SHOW_DIALOG_TO_REFRESH_MEMBERSHIP", // not needed
    // SHOW_SELECT_SERVICE_BOTTOMSHEET: "SHOW_SELECT_SERVICE_BOTTOMSHEET", // not needed handled in raise claim
    // OPEN_ASSURED_BUYBACK: "OPEN_ASSURED_BUYBACK", // not needed handled in raise claim
    // CHECK_FOR_MEMBERSHIP_DEEEPLINK: "CHECK_FOR_MEMBERSHIP_DEEEPLINK", // leave it for now
    // SET_REFRESHING: "SET_REFRESHING", // remove not need
    // RENEWAL_PROCEED_TO_PAY: "RENEWAL_PROCEED_TO_PAY", // LEAVE it for now as discussed with imran


    //Below are not button actions but these are used to handle an action after calculation or api response
    START_HA_RAISE_CLAIM_PROCESS: "START_HA_RAISE_CLAIM_PROCESS", // it's sub action to start ha claim process using bridge
    HANDLE_PENDING_SUBMISSION_RESPONSE: "HANDLE_PENDING_SUBMISSION_RESPONSE", // it's sub action to start ha claim process using bridge
    DEEPLINK_SERVICE_DETAIL: "DEEPLINK_SERVICE_DETAIL",
    DEEPLINK_WHC_SELECT_ADDRESS: "DEEPLINK_WHC_SELECT_ADDRESS",

};


const routeToBuyback = (buybackStatusList, membership, allMemberships, isSuccessScreen) => {
    let currentData = (buybackStatusList ?? []).filter((currentStatus) => {
        currentStatus?.deviceInfo?.deviceIdentifier == APIData.deviceIdentifier;
    }).first();

    if (currentData && (buybackStatusList ?? []).length == 1) {
        let isSupported = currrentData?.buyBackStatus != buybackStatusConstants.MODEL_NOT_FOUND;
        let buyBackStatus = currrentData.buyBackStatus ?? "";
        let eventData = {};
        eventData[WebEngageKeys.LOCATION] = "Membership Detail Screen";
        eventData[WebEngageKeys.BRAN_AND_MODEL] = Platform.OS === PLATFORM_OS.ios ? "Apple " + APIData.modelName : APIData.brand + " " + APIData.modelName;
        eventData[WebEngageKeys.SUPPORTED] = isSupported ? "YES" : "NO";
        eventData[WebEngageKeys.STAGE] = buyBackStatus;
        logWebEnageEvent(WebEngageEventsName.MOBILE_BUYBACK_INTENT, eventData);
    }

    if (buybackStatusList) {
        let initialPropsData = {
            buyback_status_list: JSON.stringify(buybackStatusList),
            membership_id: membership?.memId,
        };
        if (isSuccessScreen) {
            initialPropsData["openSuccessTimeline"] = true;
        } else {
            initialPropsData["membershipList"] = JSON.stringify(allMemberships);
        }
        nativeBridgeRef.routeToBuyback(initialPropsData);
    }
};

const handleChatClaimRaise = (isClaimResumed: boolean, params) => {
    let membership = {...(params?.membershipResponse ?? {})};
    if (params.checkMembershipClaimFileStatus) {
        params.checkMembershipClaimFileStatus(membership?.memId, (isEnabled) => {
            if (isEnabled) {
                let initialPropsData = {
                    "membership_id": membership?.memId?.toString() ?? "",
                    "actionType": isClaimResumed ? "RESUME_SR" : "NEW_SR",
                };
                if (isClaimResumed) {
                    initialPropsData["draftSrId"] = (membership?.intimationArr ?? []).first()?.id ?? "";
                }
                nativeBridgeRef.openChatWithInitialPrams(initialPropsData);
            } else {
                if (params?.showAlert) {
                    params?.showAlert(null, CLAIM_NOT_ALLOWED, {
                        text: AppForAppFlowStrings.ok, onClick: () => {
                            if (params?.handleMembershipRefresh) {
                                params?.handleMembershipRefresh();
                            }
                        },
                    }, null, null);
                }
            }
        });
    }
};


const handleRaiseClaim = (params, isClaimResumed) => {
    let membership = { ...(params?.membershipResponse ?? {}) };
    let buybackResponseList = params?.buybackResponse?.data?.buybackStatus;
    let allMemberships = params?.allMemberships;
    if (membership) {
        if (membership?.categories?.includes(CATEGORY.PERSONAL_ELECTRONICS)) { // check for pe membership
            if (membership?.isABBPlan) { // if its PE mobile combo plan
                if (isABBOnlyMembership(membership)) {
                    routeToBuyback(buybackResponseList, membership, allMemberships, false);
                } else {
                    nativeBridgeRef.showBuybackServicePicker((receivedIndex) => {
                        let selectedIndex = parseInt(receivedIndex);
                        if (BuybackComboPlanServices.bookAService == selectedIndex) {
                            handleChatClaimRaise(isClaimResumed, params);
                        } else if (BuybackComboPlanServices.sellYourPhone == selectedIndex) {
                            routeToBuyback(buybackResponseList, membership, allMemberships, false);
                        }
                    });
                }
            } else { // not ABB membership raise claim from chat
                handleChatClaimRaise(isClaimResumed, params);
            }
        } else { // if its not pe than most probably it's HA start native raise claim process for HA
            if (Array.isArray(membership?.assets) && membership?.assets?.length > 0) {
                if (params?.checkAvailabilityForPendingSubmission) {
                    params?.checkAvailabilityForPendingSubmission(membership?.memId, (response) => {
                        membership.checkAvailabilityResponse = response;
                        nativeBridgeRef.handleMembershipTabActions(MembershipButtonActions.START_HA_RAISE_CLAIM_PROCESS, {
                            membershipResponse: membership,
                            isResumeClaim: isClaimResumed
                        });
                    });
                }
            } else {
                nativeBridgeRef.handleMembershipTabActions(MembershipButtonActions.START_HA_RAISE_CLAIM_PROCESS, { ...params?.data, membershipResponse: membership, isResumeClaim: isClaimResumed });
            }
        }
    }
};

const routeToIDFence = () => {
    let buyPlanJourneyObj = {
        category: Category.finance,
        service: PlanServices.idFence,
        screenTitle: "IDFence",
        serviceName: "Dark Web Monitering",
    };
    nativeBridgeRef.openBuyPlanJourney({
        "routeName": "ServiceDetailScreen",
        "data": {buyPlanJourney: buyPlanJourneyObj},
    });
};

const handleIDFence = (params) => {
    if (params?.membershipResponse?.membershipStatus != MembershipStatus.cancelled) {
        let eventData = {};
        eventData[WebEngageKeys.LOCATION] = "Membership Tab";
        eventData[WebEngageKeys.MEMBERSHIP_STATUS] = params?.membershipResponse?.membershipStatus;
        eventData[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.BP] = params?.membershipResponse?.partnerCode;
        eventData[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.BU] = params?.membershipResponse?.partnerBUCode;
        eventData[WebEngageKeys.PLAN_CODE] = params?.membershipResponse?.plan?.planCode;
        eventData[WebEngageKeys.PLAN_NAME] = params?.membershipResponse?.plan?.planName;
        logWebEnageEvent(WebEngageEventsName.UPGRADE_IDFENCE, eventData);
    }
    if (params?.membershipResponse.trial == "Y") {
        routeToIDFence();
    } else {
        if (params?.membershipResponse?.membershipStatus == MembershipStatus.cancelled) {
            routeToIDFence();
        } else {
            if (params?.renewalAPICall) {
                params?.renewalAPICall(params?.membershipResponse, true, (response) => {
                    nativeBridgeRef.openIDFencePlanDetails(true, response?.data?.payNowLink);
                });
            }
        }
    }

};

const handleRenewNow = (params) => { // renewal for HA
    if (params?.membershipResponse) {
        if (isHAMembership(params?.membershipResponse)) {
            if (isHAEWMembership(params?.membershipResponse)) {
                getFirebaseData(RENEW_MESSAGE_HA, (message) => {
                    params?.showAlert(null, message, {
                        text: AppForAppFlowStrings.ok, onClick: () => {
                        },
                    }, null, null);
                });
            } else if (isWHCMembership(params?.membershipResponse)) {
                let buyPlanJourneyObj = {
                    category: Category.homeAppliances,
                    service: PlanServices.whcInspection,
                    screenTitle: "",
                    serviceName: PlanServices.whcInspection,
                };
                let customer = {...params?.membershipResponse?.customers?.first()};
                customer.previousCustId = customer?.custId;
                let pinCode = customer?.addresses?.filter((address) => {
                    address?.addressType == "INSPECTION";
                }).first()?.pincode ?? customer?.addresses?.first()?.pincode;
                let renewalData = {
                    customerInfo: customer,
                    memID: params?.membershipResponse?.memId,
                    prevPlanCode: params?.membershipResponse?.plan?.planCode,
                    prevCoverAmount: params?.membershipResponse?.coverAmount,
                    pinCode: pinCode,
                };
                buyPlanJourneyObj.renewalData = renewalData;
                nativeBridgeRef.openBuyPlanJourney({
                    "routeName": "PinCodeScreen",
                    "data": {buyPlanJourney: buyPlanJourneyObj},
                });
            }
        } else if (isWalletMembership(params?.membershipResponse)) { // renewal for wallet
            if (!isIDFenceMembership(params?.membershipResponse)) {
                if (params?.renewalAPICall) {
                    params?.renewalAPICall(params?.membershipResponse, true, (response) => {

                        let initialProps = {
                            "data": response?.data?.payNowLink,
                            "service_Type": "wallet",
                            "hasEW": false,
                            "isRenewal": true,
                        };
                        nativeBridgeRef.openPaymentFlow(initialProps);
                    });
                }
            }
        }
    }
};

const handleFinancePlanActivation = (params) => {
    let membership = params?.membershipResponse;
    if (membership) {
        if (membership?.isPendingMembership) {
            let eventData = {};
            eventData[WebEngageKeys.TYPE] = WALLET_STRING;
            eventData[WebEngageKeys.SERVICE] = CARD_FRAUD_FOR_WALLET;
            logWebEnageEvent(WebEngageEventsName.CONTINUE_ACTIVATION);
            if (membership.activationCode) {
                let initialProps = {
                    "service_Type": "wallet",
                    "postboarding_screen_act_code": membership.activationCode,
                };
                nativeBridgeRef.openPaymentFlow(initialProps);
                return;
            }
        }
    }
};

const openIDFenceDashboard = (params) => {
    let membership = params?.membershipResponse;
    if (params?.checkMembershipSubscriptionStatus) { //call api for checking idfence subscription
        params?.checkMembershipSubscriptionStatus(membership?.memUUID, (statusResponse) => {
            if (statusResponse?.data && [IDFENCE_SUBSCRIPTION_STATUS.CUSTOMER_UNSUBSCRIBED, IDFENCE_SUBSCRIPTION_STATUS.CUSTOMER_ACTIVE, IDFENCE_SUBSCRIPTION_STATUS.CUSTOMER_DEACTIVATED].includes(statusResponse?.data)) {
                const {membershipDataText, renewalDataStringified} = prepareDataForIDFenceDashboard(membership);
                if (Platform.OS === PLATFORM_OS.android) {
                    let requireData = {
                        membership: membership,
                        renewalDataStringified: renewalDataStringified,
                    };
                    nativeBridgeRef.routeToOAIDFenceVCFromMembershipTab(requireData);
                } else {
                    nativeBridgeRef.routeToOAIDFenceVC(membershipDataText, renewalDataStringified);
                }
            } else {
                let message = AppForAppFlowStrings.idFenceDashboardNotSetup;
                if (response?.data == IDFENCE_SUBSCRIPTION_STATUS.CUSTOMER_DOES_NOT_EXIST) {
                    message = AppForAppFlowStrings.idFenceDashboardCustomerDoesntExist;
                    params?.showAlert("", message, {text: AppForAppFlowStrings.close});
                    return;
                } else if (response?.data == IDFENCE_SUBSCRIPTION_STATUS.CUSTOMER_IN_PROGRESS) {
                    message = AppForAppFlowStrings.idFenceDashboardSetupInProgress;
                    params?.showAlert("", message, {text: AppForAppFlowStrings.close});
                    return;
                }
                params?.showAlert("", message, {text: AppForAppFlowStrings.close});
            }
        });
    }
};

const activateHAEW = (params) => {
    let membership = params?.membershipResponse;
    let eventData = {};
    eventData[WebEngageKeys.TYPE] = HOME_EW;
    eventData[WebEngageKeys.SERVICE] = HOME_EW;
    logWebEnageEvent(WebEngageEventsName.CONTINUE_ACTIVATION);

    if (membership.activationCode) {
        let initialProps = {
            "service_Type": "HS",
            "postboarding_screen_act_code": membership.activationCode,
        };
        nativeBridgeRef.openPaymentFlow(initialProps);
        return;
    }
};

const handlePendingSubmission = params => {
    let service = getServiceFromMembership(params?.membershipResponse, params?.data?.serviceRequestId);
    if (service) {
        if (params?.checkAvailabilityForPendingSubmission) {
            params?.checkAvailabilityForPendingSubmission(service?.referenceNo, (response) => {
                let membership = {...(params?.membershipResponse ?? {})};
                membership.checkAvailabilityResponse = response;
                if (params?.data?.serviceRequestId) {
                    membership.srResponseData = [service];
                }
                handleMembershipBtnActions(MembershipButtonActions.HANDLE_PENDING_SUBMISSION_RESPONSE, {
                    membershipResponse: membership,
                    data: {...params?.data},
                });
            });
        }
    }
};

export const handleMembershipBtnActions = (buttonAction, params) => {
    switch (buttonAction) {
        case MembershipButtonActions.CALL_US:
            openDialerWith(params?.data?.number);
            break;
        case MembershipButtonActions.OPEN_BUYBACK_TIME_LINE:
            let membership1 = { ...(params?.membershipResponse ?? {}) };
            let buybackResponseList = params?.buybackResponse?.data?.buybackStatus;
            let allMemberships = params?.allMemberships;
            routeToBuyback(buybackResponseList, membership1, allMemberships, true);
            break;
        case MembershipButtonActions.RAISE_CLAIM:
            handleRaiseClaim(params, false);
            break;
        case MembershipButtonActions.UPGRADE_IDFENCE:
        case MembershipButtonActions.UPGRADE_TRIAL_IDFENCE:
            handleIDFence(params);
            break;
        case MembershipButtonActions.BUY_NOW:
            routeToIDFence();
            break;
        case MembershipButtonActions.SR_MULTIPLE_STATUS:
            nativeBridgeRef.openBuyPlanJourney({
                "routeName": "OnGoingServicesScreen",
                "data": {selectedMembership: params?.membershipResponse},
            });
            break;
        case MembershipButtonActions.ACTIVATE_FINANCE_MEMBERSHIP:
            handleFinancePlanActivation(params);
            break;
        case MembershipButtonActions.RENEW_NOW:
            handleRenewNow(params);
            break;
        case MembershipButtonActions.CHECK_FOR_ID_FENCE_DASHBOARD:
            openIDFenceDashboard(params);
            break;
        case MembershipButtonActions.ACTIVATE_HOME_EW_MEMBERSHIP:
            activateHAEW(params);
            break;
        case MembershipButtonActions.PENDING_SUBMISSION:
            handlePendingSubmission(params);
            break;
        case MembershipButtonActions.RESUME_CLAIM:
            handleRaiseClaim(params, true);
            break;
        case MembershipButtonActions.SHOW_ASSETS_BOTTOMSHEET:
            let bottomSheetData = {
                bottomSheetTitle: YOUR_APPLIANCES,
                listOfText: params?.data,
                image: images.smallDot,
                imageStyle: CommonStyle.bulletIcon,
            };
            if (params?.navigation) {
                params?.navigation?.navigate("BottomSheetScreen", {
                    bottomSheetData: bottomSheetData,
                    bottomSheetType: buttonAction,
                });
            } else {
                nativeBridgeRef.openBuyPlanJourney({
                    "routeName": "BottomSheetScreen",
                    "isPresent": true,
                    "data": {
                        bottomSheetData: bottomSheetData,
                        bottomSheetType: buttonAction,
                    },
                });
            }
            break;
        case MembershipButtonActions.RATE_US:
        case MembershipButtonActions.VIEW_DETAILS:
        case MembershipButtonActions.SR_RESCHEDULE_VISIT:
        case MembershipButtonActions.VIEW_INSPECTION_DETAILS:
        case MembershipButtonActions.FD_OPEN_SEND_SMS_SCREEN:
        case MembershipButtonActions.ACTIVATE_PE_MEMBERSHIP:
        case MembershipButtonActions.SALES_UPLOAD_DOCUMENTS:
        case MembershipButtonActions.SCHEDULE_INSPECTION:
        case MembershipButtonActions.SELECT_ADDRESS_INSPECTION:
        case MembershipButtonActions.HANDLE_PENDING_SUBMISSION_RESPONSE:
        case MembershipButtonActions.SR_VIEW_TIMELINE:
        case MembershipButtonActions.SR_UPLOAD_DOCUMENT:
        case MembershipButtonActions.SR_UPLOAD_PENDENCIES:
        case MembershipButtonActions.DOWNLOAD_INVOICE:
        case MembershipButtonActions.MY_CARDS:
        case MembershipButtonActions.DEEPLINK_SERVICE_DETAIL:
        case MembershipButtonActions.DEEPLINK_WHC_SELECT_ADDRESS:
            // alert(params?.data?.serviceRequestId)
            // just to send actual selected service with membership
            let membership = {...(params?.membershipResponse ?? {})};

            // get buyback api response and send with data
            let buybackResponseListOther = params?.buybackResponse?.data ?? {};

            if (params?.data?.serviceRequestId) {
                let service = getServiceFromMembership(params?.membershipResponse, params?.data?.serviceRequestId);
                membership.srResponseData = [service];
            }
            nativeBridgeRef.handleMembershipTabActions(buttonAction, {
                ...params?.data,
                buyBackStatusData: buybackResponseListOther,
                membershipResponse: membership,
            });
            break;
        default:
            break;
    }
};

export const handleMembershipDeeplink = (deeplinkURL, extraProps) => {
    let parse = require("url-parse");
    let url = parse(deeplinkURL, true);
    if (deeplinkURL) {
        switch (true) {
            case deeplinkURL.includes(Deeplink.whcSelectAddress):
                if (extraProps?.getTempCustomer) {
                    let activationCode = deeplinkURL.substring(deeplinkURL.lastIndexOf("/") + 1);
                    extraProps?.getTempCustomer(activationCode, response => {
                        nativeBridgeRef.handleMembershipTabActions(MembershipButtonActions.DEEPLINK_WHC_SELECT_ADDRESS, {
                            deeplinkParams: {
                                ...url?.query, activationCode: activationCode,
                            },
                            tempCustInfoData: response,
                        });
                    });
                }
                break;
            case deeplinkURL.includes(Deeplink.serviceDetail):
                if (url?.query?.SRid) {
                    if (url?.query?.SRNumber && url?.query?.SRType) {
                        nativeBridgeRef.handleMembershipTabActions(MembershipButtonActions.DEEPLINK_SERVICE_DETAIL, {deeplinkParams: url?.query});
                    } else {
                        extraProps?.serviceRequestFor({serviceRequestId: url?.query?.SRid}, response => {
                            let serviceRequest = response?.data?.first;
                            let srType = serviceRequest?.workflowData?.visit?.isSelfService == "Y" ? "Self_Repair" : serviceRequest?.serviceRequestType;
                            nativeBridgeRef.handleMembershipTabActions(MembershipButtonActions.DEEPLINK_SERVICE_DETAIL, {
                                deeplinkParams: {
                                    ...url?.query,
                                    SRNumber: serviceRequest?.refPrimaryTrackingNo,
                                    SRType: srType,
                                },
                            });
                        });
                    }
                }
                break;
            case deeplinkURL.includes(Deeplink.idfence):
                if (url?.query?.subscriberNo && Array.isArray(extraProps?.allMemberships)) {
                    let filteredMembership = extraProps?.allMemberships?.filter(membership => membership?.memUUID == url?.query?.subscriberNo).first();
                    if (filteredMembership) {
                        openIDFenceDashboard({...extraProps, membershipResponse: filteredMembership});
                    }
                }
                break;
        }
    }
};
