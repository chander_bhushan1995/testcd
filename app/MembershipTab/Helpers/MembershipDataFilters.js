import {ASSURED_BUYBACK_PLAN, CATEGORY, PRIORITY, TYPE, USER_TASK} from "../../Constants/MembershipConstants";
import {checkIsMemIsIdFence} from "./MembershipTabUtils";

/**
 * Filter membership for Draft and Service request api's
 * @param membershipList list for memberships(N+P)
 * @returns  list of membership id's
 */
export const filterSrMembershipIds = (membershipList) => {
    return membershipList?.filter((membership) =>
        (TYPE.ACTIVE === membership?.membershipStatus) &&
        (!(CATEGORY.FINANCE === getSingleCategory(membership?.categories)) &&
            isNonTrialComboPlan(membership)) ||
        !isEnableFileClaim(membership)
    ).map(({memId}) => (memId));
}

/**
 * Filter all IDFence memberships from provided list of Memberships
 * @param membershipList
 * @returns List of IDfence memberships
 */
export const filterIDFenceAllMembershipList = (membershipList) => {
    return  membershipList?.filter(membership => checkIsMemIsIdFence(membership));
}

/**
 * Filter all Pending memberships for FD flow
 * @param membershipList
 * @returns List of pending mobile memberships for FD Flow
 */
export const filterPEMobilePendingMembershipsActCode = (membershipList) => {
    // get activationCode of all membership if task is DOC_UPLOAD
    return membershipList?.filter((membership) => {

            if (CATEGORY.PERSONAL_ELECTRONICS === getSingleCategory(membership?.categories) ||
                CATEGORY.FINANCE === getSingleCategory(membership?.categories)) {
                if (!membership.memId) {
                    let membershipTask = membership?.task;

                    if (membershipTask) {
                        return (USER_TASK.DOC_UPLOAD === membershipTask?.name)
                    }
                }
            }
            return false;
        }
    ).map(({activationCode}) => (activationCode));

}

/**
 * Check if Claim File enabled or not for given membership
 * @param membership
 * @returns {boolean|boolean} true if enable else false
 */
export const isEnableFileClaim = (membership) => {
    return ((membership?.claims || []).length == 0 && !membership?.enableFileClaim);
}
/**
 * Get single category from provided category list
 * @param categories
 * @returns {string|*} a single category from [PE,F,HA]
 */
export const getSingleCategory = (categories) => {
    if (categories) {
        if (categories.length === 1){
            return categories[0];
        }
        if (categories.includes(CATEGORY.FINANCE)) {
            // for finance
            return CATEGORY.FINANCE;
        }
    }
    return "";
}
/**
 * Check if membership if non trial combo plan
 * @param membership
 * @returns {boolean|boolean}
 */
export const isNonTrialComboPlan = (membership) => {
    return (membership?.categories?.length > 1 && (membership?.trial === 'N'))
}

/**
 * Check is provided membership if ABB or not
 * @param membership
 * @returns {boolean} if service name is  `Buyback` then true else false
 */
export const isABBPlan = (membership) => {
    return membership?.assets?.some(asset=>(asset?.services?.some(service => service?.serviceName === ASSURED_BUYBACK_PLAN)))
}

/**
 * Set Priority of memberships
 * @param membership
 */
 export const setMembershipsPriority = (membership) => {
     if (membership?.memId && !membership?.task) {
         switch (getSingleCategory(membership?.categories)) {
             case CATEGORY.PERSONAL_ELECTRONICS:
                 membership['priority']=PRIORITY.ACTIVE_MEMBERSHIP_PERSONAL_ELECTRONICS
                 break;
             case CATEGORY.FINANCE:
                 membership['priority']=PRIORITY.ACTIVE_MEMBERSHIP_FINANCE
                 break;
             case CATEGORY.HOME_APPLIANCE:
                 membership['priority']=PRIORITY.ACTIVE_MEMBERSHIP_HOME_APPLIANCE
                 break;
         }
     }else{
         membership['priority']=PRIORITY.USER_ACTION_PENDING_SALES;
     }
}


