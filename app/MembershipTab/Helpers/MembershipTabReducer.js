import {Platform} from "react-native";
import * as ActionTypes from "../../Constants/ActionTypes";
import {
    filterSrMembershipIds,
    filterPEMobilePendingMembershipsActCode, filterIDFenceAllMembershipList, getSingleCategory,
} from "./MembershipDataFilters";

import {
    addTempCustInfoAndDocsToUploadData,
    prepareCardsForONGoingSRs, prepareMembershipsForUI,
    removeOtherMemberships,
    setRequestDetailsForMembership,
} from "./MembershipTabUtils";
import { INITIAL_API_RESPONSE_TAGS } from "../../network/APIHelper";
import {PLATFORM_OS} from "../../Constants/AppConstants";

export const initialState = {
    membershipData: {},
    allMembershipList: [],
    customerData: {},
    membershipIdsForIntimationApi: [],
    peMobileMembershipActCode: [],
    srListVisibleCards: [],
    idfenceMembershipsUUIDs: [],
    buybackReqData: null, // will remove
    isLoadingData: true,
    membershipViewModel: [],
    isCallInitApi: false,
    fdFlowSmsStatusData: []
};

function removeDuplicateMemberships(membershipList) {
    const uniqueMembershipsObjSet = new Set();
    let allUniqueItems = membershipList?.reduce((arrItems, item) => {
        if ((item.memId && !uniqueMembershipsObjSet.has(item.memId)) || (item.activationCode && !uniqueMembershipsObjSet.has(item.activationCode))) {
            if (item.memId) {
                uniqueMembershipsObjSet.add(item.memId, item)
            } else {
                uniqueMembershipsObjSet.add(item.activationCode, item)
            }
            arrItems.push(item);
        }
        return arrItems;
    }, []);
    return allUniqueItems;
}

export const MembershipTabReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.MEMBERSHIP_TAB_ACTION_TYPES.MEMBERSHIP_DATA_FROM_HOME:
            let membershipTabData = action?.data;
            let membershipData = JSON.parse(membershipTabData?.membershipResponse ?? "{}");
            let fdFlowSmsStatusData = membershipTabData?.fd_flow_sms_status_data ?? [];
            let allPendingMembershipList = membershipData?.data?.pendingMemberships ?? [];
            allPendingMembershipList?.forEach(pendingMembership => {
                    pendingMembership["isPendingMembership"] = true;
                }
            );
            let allMembershipList = [...(membershipData?.data?.memberships ?? []), ...allPendingMembershipList];
            setRequestDetailsForMembership(allMembershipList);
            allMembershipList?.sort((object1, object2) => object1.priority - object2.priority);
            allMembershipList = removeOtherMemberships(allMembershipList);
            let srMembershipIds = filterSrMembershipIds(allMembershipList);
            let peMobileMembershipActCode = filterPEMobilePendingMembershipsActCode(allMembershipList);
            //Idfence
            let allIDFenceAllMembershipList = filterIDFenceAllMembershipList(allMembershipList);
            const allIDFenceAllMembershipUUIDList = allIDFenceAllMembershipList?.map(membership => (membership.memUUID));
            return {
                ...state,
                membershipData: membershipData?.data,
                allMembershipList: allMembershipList,
                customerData: membershipData?.data?.customers?.[0],
                idfenceMembershipsUUIDs: allIDFenceAllMembershipUUIDList,
                isLoadingData: !(peMobileMembershipActCode?.length > 1 || srMembershipIds?.length > 1),
                membershipIdsForIntimationApi: srMembershipIds,
                fdFlowSmsStatusData: fdFlowSmsStatusData,
                peMobileMembershipActCode: peMobileMembershipActCode,
                isCallInitApi: true,
            };
        case ActionTypes.MEMBERSHIP_TAB_ACTION_TYPES.MEMBERSHIP_CARDS_WITH_DRAFT_SR_DATA:
            allMembershipList = injectDraftSrResponse(action?.data, [...state.allMembershipList]);
            return {
                ...state,
                allMembershipList: allMembershipList,
                isLoadingData: false,
                membershipViewModel: prepareMembershipsForUI(allMembershipList, state.fdFlowSmsStatusData),
            };
        case ActionTypes.MEMBERSHIP_TAB_ACTION_TYPES.MEMBERSHIP_CARDS_WITH_BUYBACK_DATA:
            allMembershipList = injectBuyBackResponse(action?.data, state.allMembershipList);
            return {
                ...state,
                buybackReqData: action?.data,
                isLoadingData: false,
                membershipViewModel: prepareMembershipsForUI(allMembershipList, state.fdFlowSmsStatusData),
            };
        case ActionTypes.MEMBERSHIP_TAB_ACTION_TYPES.MEMBERSHIP_GET_BUYBACK_STATUS_DATA:
            return {
                ...state,
                membershipData: injectBuyBackResponse(action?.data, state.allMembershipList),
                buybackReqData: action?.data,
                isLoadingData: false,
            };

        case ActionTypes.MEMBERSHIP_TAB_ACTION_TYPES.ONGOING_SR_PREPARE_CARDS:
            return {
                ...state,
                srListVisibleCards: action?.data?.cards,
            };
        case ActionTypes.MEMBERSHIP_TAB_ACTION_TYPES.MEMBERSHIP_UPDATE_VIEW_MODEL:
            return {
                ...state,
                isCallInitApi: false,
                membershipViewModel: prepareMembershipsForUI(action?.data, state.fdFlowSmsStatusData),
            }
        case ActionTypes.MEMBERSHIP_TAB_ACTION_TYPES.MEMBERSHIP_INITIAL_API_CALLS:
            let allApiData = action?.data;
            let buybackResponse = {}
            allMembershipList = state.allMembershipList;
            if (allApiData?.size > 0) {
                allApiData.forEach(function (value, key) {
                    switch (key) {
                        case INITIAL_API_RESPONSE_TAGS.SR_DRAFT_DATA:
                            allMembershipList = injectDraftSrResponse(value, state.allMembershipList);
                            break;
                        case INITIAL_API_RESPONSE_TAGS.SERVICE_REQUEST_DATA:
                            allMembershipList = injectServiceRequestResponse(value, state.allMembershipList);
                            break;
                        case INITIAL_API_RESPONSE_TAGS.BUY_BACK_DATA:
                            allMembershipList = injectBuyBackResponse(value, state.allMembershipList);
                            buybackResponse = value
                            break;
                        case INITIAL_API_RESPONSE_TAGS.TEMP_CUST_INFO_LIST:
                            allMembershipList = injectTempCustInfoAndGetDocToUploadDataResponse(value, state.allMembershipList, "tempCustInfoData");
                            break;
                        case INITIAL_API_RESPONSE_TAGS.TEMP_DOC_TO_UPLOAD_LIST:
                            allMembershipList = injectTempCustInfoAndGetDocToUploadDataResponse(value, state.allMembershipList, "docsToUploadData");
                            break;
                        case INITIAL_API_RESPONSE_TAGS.IDFENCE_SI_DATA_LIST:
                            allMembershipList = injectIdFenceSIResponse(value, state.allMembershipList);
                            break;
                    }
                });
            }
            return {
                ...state,
                allMembershipList: allMembershipList,
                isCallInitApi: false,
                buybackReqData: buybackResponse,
                membershipViewModel: prepareMembershipsForUI(allMembershipList, state.fdFlowSmsStatusData),
            };
        default:
            return state;
    }
};

function injectIdFenceSIResponse(response, allMembershipList) {
    response?.map(siData => {
        let itemIndex = allMembershipList?.findIndex(membership => membership?.memUUID === siData?.uuid);
        if (itemIndex != -1) {
            let idFenceMembership = allMembershipList?.[itemIndex];
            idFenceMembership["siData"] = siData?.response;
            allMembershipList?.splice(itemIndex, 1, idFenceMembership);
        }
    });
    return allMembershipList;
}

function injectTempCustInfoAndGetDocToUploadDataResponse(response, allMembershipList, dataKey) {
    response?.map(item => {
        let membershipIndex = allMembershipList?.findIndex((element) => {
            if (dataKey === "tempCustInfoData") {
                return element?.activityRefId === item?.customerDetails?.[0]?.activityRefId;
            } else {
                return element?.activityRefId === item?.activityReferenceId;
            }
        });

        if (membershipIndex != -1) {
            let itemAtIndex = allMembershipList?.[membershipIndex];
            itemAtIndex[dataKey] = item;
            allMembershipList?.splice(membershipIndex, 1, itemAtIndex);
        }
    });
    return allMembershipList;
}

function injectBuyBackResponse(response, allMembershipList) {
    response?.data?.buybackStatus?.map(item => {
        let itemIndex = allMembershipList?.findIndex(membership => (membership?.memId && membership?.memId == item?.membershipId));
        if (itemIndex > -1) {
            let membershipItem = allMembershipList?.[itemIndex];
            membershipItem["buybackStatusModel"] = item;
            allMembershipList?.splice(itemIndex, 1, membershipItem);
        }
    });
    return allMembershipList;
}

function injectDraftSrResponse(response, allMembershipList) {
    response?.data?.map(item => {
        let itemIndex = allMembershipList?.findIndex(membership => membership?.memId == item?.referenceNo);
        if (itemIndex > -1) {
            let membershipItem = {...allMembershipList?.[itemIndex]};
            membershipItem["intimationArr"] = item?.intimation;
            allMembershipList?.splice(itemIndex, 1, membershipItem);
        }
    });
    return allMembershipList;
}

const injectServiceRequestResponse = (response, allMembershipList) => {
    allMembershipList = allMembershipList.map(membership => {
        membership.srResponseData = [];
        return membership;
    });
    response?.data?.map(item => {
        let itemIndex = allMembershipList?.findIndex(membership => {
            return membership?.memId == item?.referenceNo;
        });
        if (itemIndex > -1) {
            let membershipItem = allMembershipList?.[itemIndex];
            let srResponseDataArr = membershipItem["srResponseData"];
            if (srResponseDataArr) {
                srResponseDataArr.push(item);
                membershipItem["srResponseData"] = srResponseDataArr;
            } else {
                membershipItem["srResponseData"] = [item];
            }
            allMembershipList?.splice(itemIndex, 1, membershipItem);
        }
    });
    return allMembershipList;
};
export default MembershipTabReducer;
