import {
    CARD_STATUS as Status,
    CARD_STATUS,
    CATEGORY,
    CLAIM_SERVICE_TYPE,
    EXTENDED_WARRAN,
    IDFENCE_SERVICE_NAME,
    MEMBERSHIP_TAB_CARD_TEXT,
    RENEWAL_STATUS, RESTRICT_ASSETS,
    SERVICE_REQUEST_API_DATA,
    TYPE,
    WHC_ADDR_TYPE,
    WORKFLOW_STAGE_CODE,
} from "../../Constants/MembershipConstants";
import { getInitiatingSystem } from "../../commonUtil/CommonMethods.utility";
import { MembershipButtonActions } from "./MembershipButtonActions";
import * as FirebaseKeys from "../../Constants/FirebaseConstants";
import {
    DOWNLOAD_INVOICE, EXPECTED_CLOSURE, PlanServices,
} from "../../Constants/AppConstants";
import { compareDate, DATE_MONTH_FORMAT, daysDifference, formattedDate } from "../../commonUtil/DateUtils";
import { getFirebaseData } from "../../commonUtil/AppUtils";
import { NativeModules } from "react-native";
import { getSingleCategory, isABBPlan, setMembershipsPriority } from "./MembershipDataFilters";
import Colors from "../../Constants/colors";

import { addPersonalElectronicsUiMembership, addSRCard, setPendingSrCard } from "./PEMembershipDataHelper";
import { addFinanceUiMembership } from "./FinanceMembershipDataHelper";
import { addHomeApplianceUiMembership } from "./HomeApplianceMembershipDataHelper";

import images from "../../images/index.image";

export const getServiceRequestParams = (requestType, parameters) => {
    let params = new Object();
    if (requestType === SERVICE_REQUEST_API_DATA.REQUEST_TYPE.REQUEST_ONGOING) {
        params.status = SERVICE_REQUEST_API_DATA.STATUS.REQUEST_ONGOING;
    } else if (requestType === SERVICE_REQUEST_API_DATA.REQUEST_TYPE.REQUEST_MEMBERSHIP_TAB) {
        params.status = SERVICE_REQUEST_API_DATA.STATUS.REQUEST_MEMBERSHIP_TAB;
    } else {
        params.status = SERVICE_REQUEST_API_DATA.STATUS.DEFAULT;
    }
    return { ...params, ...commonServiceRequestParams(parameters) };
};

const commonServiceRequestParams = (parameters) => {
    let params = new Object();
    params.referenceNumbers = parameters?.membershipIDs;
    params.serviceRequestId = parameters?.serviceRequestId;
    params.isWalkInSRsRequired = SERVICE_REQUEST_API_DATA.IS_WALKIN_SRS_REQUIRED;
    params.requiresAdditionalDetails = SERVICE_REQUEST_API_DATA.REQUIRES_ADDITIONAL_DETAILS;
    params.options = SERVICE_REQUEST_API_DATA.OPTIONS;
    params.serviceRequestTypes = SERVICE_REQUEST_API_DATA.SERVICE_REQUEST_TYPES;
    return params;
};

export const checkIsMemIsIdFence = (membership) => {
    return membership?.plan?.services?.some(service => (service.serviceName === IDFENCE_SERVICE_NAME.DARK_WEB_MONITORING && membership?.memId));
};

export const addTempCustInfoAndDocsToUploadData = (allMemberships, data, dataKey) => {
    data?.map(item => {
        let membershipIndex = allMemberships?.findIndex((element) => {
            if (dataKey === "tempCustInfoData") {
                return element?.activityRefId === item?.customerDetails?.[0]?.activityRefId;
            } else {
                return element?.activityRefId === item?.activityReferenceId;
            }
        });

        if (membershipIndex != -1) {
            let itemAtIndex = allMemberships?.[membershipIndex];
            itemAtIndex[dataKey] = item;
            allMemberships?.splice(membershipIndex, 1, itemAtIndex);
        }
    });
    return allMemberships;
};

export const setRequestDetailsForMembership = (membershipsList) => {
    membershipsList?.forEach(membership => {

        membership["isPendingMembership"] = membership?.isPendingMembership ? true : false;
        membership["isABBPlan"] = isABBPlan(membership);
        membership["doesPlanSupportsPMS"] = doesPlanSupportsPMS(membership);
        membership["isSOP"] = evaluateIsSOP(membership);

        if (!membership?.endDate) {
            let currentDate = new Date();
            // add next year in current date
            currentDate.setFullYear(currentDate.getFullYear() + 1);
            membership["endDate"] = currentDate.getTime().toString();
        }
        if (membership?.isPendingMembership) {
            membership["categories"] = membership?.plan?.categories;
        }

        // set setPlanRestrictedPMS
        membership["isPlanRestrictedPMS"] = setPlanRestrictedPMS(membership);

        setMembershipsPriority(membership);
    });

    return membershipsList;
};
export const removeOtherMemberships = (membershipsList) => {
    return membershipsList?.filter(membership => !(
        (TYPE.CANCELED === membership?.membershipStatus && !checkIsMemIsIdFence(membership)) ||
        (TYPE.EXPIRED === membership?.membershipStatus && !checkIsMemIsIdFence(membership)
            && CATEGORY.FINANCE === getSingleCategory(membership?.categories))));
};

export const getCheckEligibilityReqParams = (memId, assetId) => {
    let params = new Object();
    params.assetId = assetId;
    params.memId = memId;
    return params;
};

export const getParamsForMembershipDetails = (mobile, membershipId) => {
    let params = new Object();
    params.mobileNo = mobile;
    params.membershipType = "A";
    params.assetRequired = "true";
    params.assetServicesRequired = "true";
    params.checkClaimEligibility = "true";
    params.claims = "true";
    params.memId = membershipId;
    return params;
};


export const getNoNHARenewalReqParams = (membership, customerData) => {
    let renewalApiRequest = {};
    if (membership && customerData) {
        let customerInfo = getCustomerInfoForApiRequest(customerData);
        renewalApiRequest.customerInfo = [customerInfo];

        // Payment info
        let renewalPaymentInfo = {};
        renewalPaymentInfo.paymentMode = null;

        //Order Info
        let renewalOrderInfo = {};
        renewalOrderInfo.partnerBUCode = "2184";
        renewalOrderInfo.partnerCode = "139";
        renewalOrderInfo.planCode = membership?.plan?.planCode?.toString();
        renewalApiRequest.orderInfo = renewalOrderInfo;

        renewalApiRequest.initiatingSystem = getInitiatingSystem();
        renewalApiRequest.membershipId = membership?.memId?.toString();
    }
    return renewalApiRequest;
};


export const getHARenewalReqParams = (membership, customerData) => {
    let renewalApiRequest = new Object();
    if (membership && customerData) {
        // Customer info
        let renewalCustomerInfo = getCustomerInfoForApiRequest(customerData);
        // Address
        let customerAddressList = customerData?.addresses;
        let inspectionAddress = customerAddressList?.find(address => address?.addressType === WHC_ADDR_TYPE);
        inspectionAddress = inspectionAddress || customerAddressList?.[0];
        let renewalAddressInfo = {};
        renewalAddressInfo.addrType = WHC_ADDR_TYPE;
        renewalAddressInfo.cityCode = inspectionAddress?.cityCode;
        renewalAddressInfo.stateCode = inspectionAddress?.stateCode;
        renewalAddressInfo.pinCode = inspectionAddress?.pincode;
        // Payment info
        let renewalPaymentInfo = {};
        renewalPaymentInfo.paymentMode = null;

        //Order Info
        let renewalOrderInfo = {};
        renewalOrderInfo.planCode = membership?.plan?.planCode?.toString();
        renewalOrderInfo.partnerBUCode = "2184";
        renewalOrderInfo.partnerCode = "139";
        renewalApiRequest.customerInfo = [renewalCustomerInfo];
        renewalApiRequest.addressInfo = [renewalAddressInfo];
        renewalApiRequest.paymentInfo = renewalPaymentInfo;
        renewalApiRequest.orderInfo = renewalOrderInfo;
        renewalApiRequest.initiatingSystem = getInitiatingSystem();
        renewalApiRequest.membershipId = membership?.memId?.toString();
    }
    return renewalApiRequest;
};

function getCustomerInfoForApiRequest(customerData) {
    let customerInfo = new Object();
    let { firstName, lastName, emailId, mobileNumber, relationship, custId } = customerData;
    customerInfo.firstName = firstName;
    customerInfo.lastName = lastName;
    customerInfo.emailId = emailId;
    customerInfo.mobileNumber = mobileNumber;
    customerInfo.relationship = relationship;
    customerInfo.previousCustId = custId;
    return customerInfo;
}

export const isWalletMembership = (membership) => {
    return CATEGORY.FINANCE === getSingleCategory(membership?.categories);
};

export const isABBMembership = (membership) => {
    if (Array.isArray(membership?.assets)) {
        for (let i = 0; i < membership?.assets.length; i++) {
            if (Array.isArray(membership?.assets[i].services)) {
                for (let j = 0; j < membership?.assets[i].services.length; j++) {
                    if ((membership?.assets[i].services[j].serviceName ?? "").toUpperCase() == PlanServices.assuredBuyback.toUpperCase()) {
                        return true;
                    }
                }
            }
        }
    }
    return false;
};
export const isABBOnlyMembership = (membership) => {
    let isSingleServicePresent = false;
    if (Array.isArray(membership?.assets)) {
        for (let i = 0; i < membership?.assets.length; i++) {
            if (Array.isArray(membership?.assets[i].services)) {
                for (let j = 0; j < membership?.assets[i].services.length; j++) {
                    isSingleServicePresent=true;
                    if ((membership?.assets[i].services[j].serviceName ?? "").toUpperCase() !== PlanServices.assuredBuyback.toUpperCase()) {
                        return false;
                    }
                }
            }
        }
    }
    return isSingleServicePresent;
};

export const isHAMembership = (membership) => {
    return CATEGORY.HOME_APPLIANCE === getSingleCategory(membership?.categories);
};

export const isPEMembership = (membership) => {
    return CATEGORY.PERSONAL_ELECTRONICS === getSingleCategory(membership?.categories);
};

export const isHAEWMembership = (membership) => {
    let result = false;
    if (membership?.task) {
        if (membership?.task?.name == "INSPECTION") {
            return result;
        }
        if (Array.isArray(membership?.assets)) {
            membership?.assets?.every((asset) => {
                if (Array.isArray(asset?.services)) {
                    let result = asset?.services?.map((service) => {
                        service?.serviceName;
                    }).includes(PlanServices.extendedWarranty);
                    return result;
                }
            });
        }
        return result;
    }
};

export const isWHCMembership = (membership) => {
    let result = false;
    if (membership?.task) {
        if (membership?.task?.name == "INSPECTION") {
            return true;
        }
    }

    if (Array.isArray(membership?.assets)) {
        membership?.assets?.every((asset) => {
            if (Array.isArray(asset?.services)) {
                result = asset?.services?.map((service) => {
                    service?.serviceName;
                }).includes(PlanServices.extendedWarranty) == false;
                return result;
            }
        });
    } else {
        return true;
    }
    return result;
};


export const getWorkFlowStageDescription = (data) => {
    switch (data?.workflowStage) {
        case WORKFLOW_STAGE_CODE.CO:
            return data?.workflowData?.completed?.description || "";
        case WORKFLOW_STAGE_CODE.ID:
            return data?.workflowData?.insuranceDecision?.description || "";
        case WORKFLOW_STAGE_CODE.RA:
            return data?.workflowData?.repairAssessment?.description || "";
        case WORKFLOW_STAGE_CODE.VE:
            return data?.workflowData?.visit?.description || "";
        case WORKFLOW_STAGE_CODE.VR:
            return data?.workflowData?.verification?.description || "";
        case WORKFLOW_STAGE_CODE.DU:
            return data?.workflowData?.documentUpload?.description || "";
        case WORKFLOW_STAGE_CODE.CS:
            return data?.workflowData?.claimSettlement?.description || "";
        case WORKFLOW_STAGE_CODE.ICD:
            return data?.workflowData?.icDoc?.description || "";
        case WORKFLOW_STAGE_CODE.DE:
            return data?.workflowData?.delivery?.description || "";
        case WORKFLOW_STAGE_CODE.RE:
            return data?.workflowData?.repair?.description || "";
        case WORKFLOW_STAGE_CODE.PU:
            return data?.workflowData?.pickup?.description || "";
        default:
            return "";
    }
};

export const prepareCardsForONGoingSRs = (membership, services, callback) => {
    let cards = [];
    let subCategories = [];
    getFirebaseData(FirebaseKeys.HA_TOP_SUBCATEGORIES, (data) => {
        if (data) {
            subCategories.push(...data?.subCategories);
            getFirebaseData(FirebaseKeys.HA_OTHER_SUBCATEGORIES, (data) => {
                if (data) {
                    subCategories.push(...data?.subCategories);
                    getFirebaseData(FirebaseKeys.PE_DEVICE_LIST, (data) => {
                        if (data) {
                            subCategories.push(...data?.subCategories);
                            let srsViews = addSRCard(membership, getSingleCategory(membership?.categories), true);
                            if (Array.isArray(services) && services?.length > 0) {
                                services?.forEach((onGoingService) => {
                                    let cardViewModel = {};
                                    let assets = (onGoingService?.assets ?? []).map((asset) => {
                                        return `${asset?.make ?? ""} ${(subCategories.filter(subCategory => subCategory.subCategoryCode == asset?.productCode).first() ?? {})?.subCategoryName ?? ''}`;
                                    });
                                    let assetNameHeading = assets.length <= 1 ? assets.first() : `${assets.first()}, ...`;
                                    let moreAssetsNames = assets.length > 1 ? {
                                        text: ` +${assets.length - 1} more`,
                                        data: assets,
                                        action: MembershipButtonActions.SHOW_ASSETS_BOTTOMSHEET,
                                    } : null;

                                    if (onGoingService?.thirdPartyProperties?.eddToDate && onGoingService?.thirdPartyProperties?.eddFromDate) {
                                        let endDate = formattedDate(new Date(onGoingService?.thirdPartyProperties?.eddToDate), DATE_MONTH_FORMAT);
                                        let startDate = formattedDate(new Date(onGoingService?.thirdPartyProperties?.eddFromDate), DATE_MONTH_FORMAT);
                                        if (endDate && startDate) {
                                            cardViewModel.subheading = EXPECTED_CLOSURE(startDate, endDate);
                                        }
                                    }

                                    if (onGoingService?.showDownloadJobsheet) {
                                        cardViewModel.topLeftButton = {
                                            text: DOWNLOAD_INVOICE,
                                            action: MembershipButtonActions.DOWNLOAD_INVOICE,
                                            data: {
                                                serviceRequestId: onGoingService?.serviceRequestId,
                                            },
                                        };
                                    }
                                    if (srsViews?.some(obj => obj.serviceRequestId == onGoingService.serviceRequestId)) {
                                        cardViewModel.statusViews = srsViews?.filter(obj => obj.serviceRequestId == onGoingService.serviceRequestId).first()?.statusViews;
                                        cardViewModel.heading = { text: assetNameHeading, tappableText: moreAssetsNames };
                                        cardViewModel.bottomRightButton = null;
                                        cardViewModel.actionViews = null;
                                        cardViewModel.membershipResponse = membership;
                                        cards.push(cardViewModel);
                                    }
                                });
                            }
                            if (Array.isArray(membership?.intimationArr), membership?.intimationArr?.length > 0) {
                                let cardViewModel = {};
                                cardViewModel.statusViews = setPendingSrCard(Status.RESUME_CLAIM_SERVICE, membership)?.statusViews;
                                cardViewModel.membershipResponse = membership;
                                cards.push(cardViewModel);
                            }
                            callback(cards);
                        }
                    }); // PE CATEGORIES
                }
            }); // HA OTHER CATEGORIES
        }
    });// HA CATEGORIES
};

export const getMembershipRenewalDescription = (membership) => {
    let memEndDate = new Date(new Number(membership?.endDate));
    let currentDate = new Date();
    let daysDiff = daysDifference(currentDate.getTime(), memEndDate.getTime());
    let formattedEndDate = formattedDate(memEndDate, "dS mmmm, yyyy");
    let totalClaimAmountForYear = getTotalClaimAmountForYear(membership);
    let graceDays = membership?.graceDays || 0;
    if (totalClaimAmountForYear > 0) {
        let part1 = "You have saved";
        let part2 = "Rs " + totalClaimAmountForYear;
        let part3 = "last year on repairs. Renew now & stay protected 👍";
        return [{ "text": part1 }, { "text": part2, style: { color: Colors.color_2E8B57 } }, { "text": part3 }];
    }
    if (daysDiff < 0) {
        if (graceDays > 0) {
            return [{ "text": MEMBERSHIP_TAB_CARD_TEXT.RENEWAL_MSG_YOUR_MEM_EXP_RW_IN_GRACE_DAYS(formattedEndDate, graceDays) }];
        } else {
            return [{ "text": MEMBERSHIP_TAB_CARD_TEXT.RENEWAL_MSG_YOUR_MEM_EXP_RW(formattedEndDate) }];
        }
    } else {
        return [{ "text": MEMBERSHIP_TAB_CARD_TEXT.RENEWAL_MSG_YOUR_MEM_EXP_RW(formattedEndDate) }];
    }
};

export const getTotalClaimAmountForYear = (membership) => {
    let totalPrevClaimAmount = 0;
    membership?.claims?.forEach(claim => {
        let claimCreatedOn = claim?.createdOn;
        // replaceAll not required. causing issue `Unresolved function or method replaceAll()`
        //claimCreatedOn = claimCreatedOn?.replaceAll("-", "/");
        let claimCreatedOnTimeStamp = Date.parse(claimCreatedOn).toFixed(0);
        if (compareDate(membership?.startDate, claimCreatedOnTimeStamp)) {
            if (claim?.workflowData && claim?.workflowData?.repairAssessment) {
                let costToCompany = claim?.workflowData?.repairAssessment?.costToCompany || "0";
                totalPrevClaimAmount = totalPrevClaimAmount + new Number(costToCompany);
            }
        }
    });
    return totalPrevClaimAmount;
};

export const getExpiryTextColor = (membership) => {
    if (RENEWAL_STATUS.IN_RENEWAL_WINDOW === membership?.renewalActivity) {
        if ([RENEWAL_STATUS, RENEWAL_STATUS.PENDING, RENEWAL_STATUS.NOT_APPLICABLE, RENEWAL_STATUS.REJECTED].includes(membership?.renewalStatus)) {
            return "red";
        }
    }
    return Colors.grey;
};

export const getAssetListText = (membership) => {
    let strAssetText = "";
    if (!membership?.categories) {
        return strAssetText;
    }

    if (membership?.categories?.includes(CATEGORY.FINANCE)) {
        if (membership?.categories.length == 1) {
            strAssetText = "Wallet";
        } else {
            strAssetText = "Mobile & Wallet";
        }
    } else if (membership?.categories?.includes(CATEGORY.PERSONAL_ELECTRONICS)) {
        if (membership?.assets) {
            strAssetText = membership?.assets[0].brand + " " + (membership?.assets[0]?.model || "");
        } else {
            strAssetText = "Mobile";
        }
    } else if (membership?.categories?.includes(CATEGORY.HOME_APPLIANCE)) {
        if (membership?.assets?.length > 2) {
            strAssetText = membership?.assets?.[0].name + ", " + membership?.assets?.[1].name + ",... +" + (membership?.assets?.length - 2) + " more";
        } else {
            if (membership?.assets?.length > 1) {
                strAssetText = membership?.assets?.[0].name + ", " + membership?.assets?.[1].name;
            } else {
                strAssetText = membership?.assets?.[0].name;
            }

        }
    }
    return strAssetText;
};

export const getDescriptionColor = (status) => {
    if ([CARD_STATUS.POSTDTLPENDING,
        CARD_STATUS.POSTDTLCOMPLETE,
        CARD_STATUS.REUPLOAD,
        CARD_STATUS.PENDING,
        CARD_STATUS.QUEUED,
        CARD_STATUS.NEW,
        CARD_STATUS.IN_PROGRESS,
        CARD_STATUS.DOCUMENT_RE_UPLOAD_PENDING,
        CARD_STATUS.DOCUMENT_UPLOAD_PENDING,
        CARD_STATUS.ON_HOLD,
        CARD_STATUS.RAISED,
        CARD_STATUS.RE_SCHEDULE_VISIT,
        CARD_STATUS.INSPECTION_PENDING,
        CARD_STATUS.RENEWAL_IN_PROGRESS,
        CARD_STATUS.RESUME_CLAIM_SERVICE, TYPE.PENDING].includes(status)) {
        return Colors.BG_DESCRIPTION_YELLOW_FEF9EC;
    }
    if ([CARD_STATUS.SUCCESS,
        CARD_STATUS.APPROVED,
        CARD_STATUS.COMPLETED,
        CARD_STATUS.INSPECTION_SCHEDULED,
        CARD_STATUS.ACTIVATED_SUCCESSFULLY,
        CARD_STATUS.SR_RATING_PENDING,
        CARD_STATUS.AWAITING_CONFIRMATION, TYPE.ACTIVE].includes(status)) {
        return Colors.BG_DESCRIPTION_GREEN_EDF5FE;
    }
    if ([CARD_STATUS.MEM_CANCELLED,
        CARD_STATUS.MEM_REJECTED,
        CARD_STATUS.CANCELLED,
        CARD_STATUS.REJECTED,
        CARD_STATUS.INSPECTION_CANCELLED,
        CARD_STATUS.INSPECTION_FAILED,
        CARD_STATUS.PENDING_SUBMISSION, TYPE.EXPIRED, TYPE.CANCELED].includes(status)) {
        return Colors.BG_DESCRIPTION_RED_FBE6E7;
    }
    return Colors.BG_DESCRIPTION_YELLOW_FEF9EC;
};

export const getStatusTagColor = (status) => {
    if ([CARD_STATUS.NEW,
        CARD_STATUS.POSTDTLPENDING,
        CARD_STATUS.POSTDTLCOMPLETE,
        CARD_STATUS.REUPLOAD,
        CARD_STATUS.PENDING,
        CARD_STATUS.QUEUED,
        CARD_STATUS.IN_PROGRESS,
        CARD_STATUS.DOCUMENT_RE_UPLOAD_PENDING,
        CARD_STATUS.DOCUMENT_UPLOAD_PENDING,
        CARD_STATUS.ON_HOLD,
        CARD_STATUS.RAISED,
        CARD_STATUS.RE_SCHEDULE_VISIT,
        CARD_STATUS.INSPECTION_PENDING,
        CARD_STATUS.RENEWAL_IN_PROGRESS,
        CARD_STATUS.RESUME_CLAIM_SERVICE].includes(status)) {
        return Colors.STATUS_BG_YELLOW;
    }
    if ([CARD_STATUS.SUCCESS,
        CARD_STATUS.APPROVED,
        CARD_STATUS.COMPLETED,
        CARD_STATUS.INSPECTION_SCHEDULED,
        CARD_STATUS.ACTIVATED_SUCCESSFULLY,
        CARD_STATUS.SR_RATING_PENDING,
        CARD_STATUS.AWAITING_CONFIRMATION].includes(status)) {
        return Colors.STATUS_BG_GREEN;
    }
    if ([CARD_STATUS.MEM_CANCELLED,
        CARD_STATUS.MEM_REJECTED,
        CARD_STATUS.CANCELLED,
        CARD_STATUS.REJECTED,
        CARD_STATUS.INSPECTION_CANCELLED,
        CARD_STATUS.INSPECTION_FAILED,
        CARD_STATUS.PENDING_SUBMISSION].includes(status)) {
        return Colors.STATUS_BG_RED;
    }
    return Colors.STATUS_BG_YELLOW;
};

export const getPEProductImage = (membership) => {
    let productCode=membership?.products?.[0] || null;
    if (productCode){
        if (productCode==='LAP'){
            return images.buy_plan_laptop_icon;
        }else if (productCode==='WR'){
            return images.buy_plan_wearable_icon;
        }
    }
    return images.buy_plan_mobile_icon;
}

export const getStatusText = (status) => {
    switch (status) {
        case CARD_STATUS.DOCUMENT_UPLOAD_PENDING:
        case CARD_STATUS.POSTDTLPENDING:
        case CARD_STATUS.POSTDTLCOMPLETE:
            return MEMBERSHIP_TAB_CARD_TEXT.DOCUMENT_UPLOAD_PENDING;

        case CARD_STATUS.REUPLOAD:
            return MEMBERSHIP_TAB_CARD_TEXT.DOCUMENT_RE_UPLOAD_PENDING;
        case CARD_STATUS.PENDING:
            return MEMBERSHIP_TAB_CARD_TEXT.DOCUMENT_VERIFICATION_PENDING;
        case CARD_STATUS.APPROVED:
            return MEMBERSHIP_TAB_CARD_TEXT.DOCUMENT_APPROVED;
        case CARD_STATUS.COMPLETED:
        case CARD_STATUS.QUEUED:
            return MEMBERSHIP_TAB_CARD_TEXT.COMPLETED;
        case CARD_STATUS.AWAITING_CONFIRMATION:
            return MEMBERSHIP_TAB_CARD_TEXT.AWAITING_CONFIRMATION;
        case CARD_STATUS.MEM_CANCELLED:
            return MEMBERSHIP_TAB_CARD_TEXT.MEM_CANCELLED;
        case CARD_STATUS.MEM_REJECTED:
            return MEMBERSHIP_TAB_CARD_TEXT.MEM_REJECTED;
        case CARD_STATUS.ON_HOLD:
            return MEMBERSHIP_TAB_CARD_TEXT.ON_HOLD;
        case CARD_STATUS.PENDING_SUBMISSION:
            return MEMBERSHIP_TAB_CARD_TEXT.PENDING_SUBMISSION;
        case CARD_STATUS.CANCELLED:
            return MEMBERSHIP_TAB_CARD_TEXT.CANCELLED;
        case CARD_STATUS.REJECTED:
            return MEMBERSHIP_TAB_CARD_TEXT.REJECTED;
        case CARD_STATUS.RE_SCHEDULE_VISIT:
            return MEMBERSHIP_TAB_CARD_TEXT.RE_SCHEDULE_VISIT;
        case CARD_STATUS.OTHER_PLATFORM_MEMBERSHIP:
            return MEMBERSHIP_TAB_CARD_TEXT.APPLE_MEMBERSHIP;

    }
    return null;
};


export const prepareMembershipsForUI = (membershipList, fdFlowSmsStatusData) => {
    let peUIModelList = [];
    // get all PE memberships
    peUIModelList = [...addPersonalElectronicsUiMembership(membershipList, fdFlowSmsStatusData),
        ...addFinanceUiMembership(membershipList),
        ...addHomeApplianceUiMembership(membershipList)];
    peUIModelList?.sort((object1, object2) => object1?.priority - object2?.priority);
    return peUIModelList;
};

export const getValidTillText = (membership) => {
    if (membership?.endDate) {
        let memEndDate = new Date(new Number(membership?.endDate));
        let memFormattedEndDate = formattedDate(memEndDate, "dd mmm yyyy");
        return MEMBERSHIP_TAB_CARD_TEXT.TEXT_VALID_TILL(memFormattedEndDate);
    }
    return "";
};

export const isExtendedWarrantyPlan = (membership) => {
    return membership?.assets?.some(asset => (asset?.services?.some(service => (service?.serviceName === EXTENDED_WARRAN))));
};

export const isWhcActivatedMembership = (membership) => {
    let isWHCMembership = false;
    membership?.assets?.forEach(asset => {
        asset?.services?.forEach(service => {
            if (service?.serviceName === EXTENDED_WARRAN) {
                return false;
            }
            isWHCMembership = true;
        });
    });
    return isWHCMembership;
};

export const isUnderInspection = (membership) => {
    return membership?.task?.name === WHC_ADDR_TYPE;
};
export const isUnderRenewal = (membership) => {
    if ([RENEWAL_STATUS.IN_RENEWAL_WINDOW].includes(membership?.renewalActivity)) {
        return [RENEWAL_STATUS.NOT_APPLICABLE, RENEWAL_STATUS.REJECTED].includes(membership?.renewalStatus);
    }
    return false;
};

export const doesPlanSupportsPMS = (membership) => {

    return membership?.plan?.services.some((service) => (service?.description?.some(description => (description?.claimType === CLAIM_SERVICE_TYPE.PMS))));
};
export const evaluateIsSOP = (membership) => {
    return membership?.plan?.services?.some((service) => ((service?.description?.some(description => (description?.claimType === CLAIM_SERVICE_TYPE.HA_BD))) && !service?.insuranceBacked));
};

export const setPlanRestrictedPMS = (membership) => {
    return membership?.plan?.services?.some((service) => (
        service?.description?.some(description => (description?.attributes?.some(attribute => (description?.claimType === CLAIM_SERVICE_TYPE.PMS &&
            attribute?.attributeName === RESTRICT_ASSETS && attribute?.attributeValue === "Y"))))));
};


