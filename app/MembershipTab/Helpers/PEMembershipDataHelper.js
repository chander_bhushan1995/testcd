import {
    Platform,
    NativeModules,
} from "react-native";
import {
    ASSURED_BUYBACK_PLAN,
    CARD_STATUS as Status,
    CARD_STATUS,
    CATEGORY,
    MEMBERSHIP_TAB_CARD_TEXT,
    ORDER_PLACED,
    PRIORITY,
    SERVICE_REQUEST_STATUS,
    SERVICE_REQUEST_WORKFLOW_STAGE,
    SERVICE_REQUEST_WORKFLOW_STAGE_STATUS,
    TYPE,
    USER_TASK,
} from "../../Constants/MembershipConstants";
import { getSingleCategory } from "./MembershipDataFilters";
import {APIData} from '../../../index';
import { MembershipButtonActions } from "./MembershipButtonActions";

import {
    getAssetListText,
    getDescriptionColor,
    getExpiryTextColor, getPEProductImage,
    getStatusTagColor,
    getStatusText,
    getValidTillText,
    getWorkFlowStageDescription,
    isABBOnlyMembership,
    isExtendedWarrantyPlan,
} from "./MembershipTabUtils";
import { formattedDate } from "../../commonUtil/DateUtils";
import { ButtonTexts, PLATFORM_OS, VIEW_DETAIL } from "../../Constants/AppConstants";
import { MembershipCardActionsType } from "../Components/MembershipCardActionViews/MembershipCardActionContainer";
import Colors, { colors } from "../../Constants/colors";
import images from "../../images/index.image";
import { TextStyle } from "../../Constants/CommonStyle";
import { getHAMembershipPlanName } from "./HomeApplianceMembershipDataHelper";

/**
 *
 * Helper for Personal Electronics memberships to prepare data for UI
 */
const nativeBridgeRef = NativeModules.ChatBridge;
export const addPersonalElectronicsUiMembership = (membershipList, fdFlowSmsStatusData) => {
    //get All PE Memberships
    let peMembershipList = membershipList?.filter((membership) => CATEGORY.PERSONAL_ELECTRONICS === getSingleCategory(membership?.categories));
    // Get non Expired and Cancelled memberships
    let actualMembershipList = peMembershipList?.filter((membership) => (TYPE.EXPIRED !== membership?.membershipStatus && TYPE.CANCELED != membership?.membershipStatus));
    let peUIModelList = [];
    actualMembershipList?.forEach(membership => {
        if (membership?.memId && !membership?.task) {
            let abbCard = prepareABBCard(membership);
            if (abbCard) {
                peUIModelList.push(abbCard);
                return;
            }
            let addSRMembershipCard = addSRCard(membership, CATEGORY.PERSONAL_ELECTRONICS, false);

            if (addSRMembershipCard) {
                peUIModelList.push(addSRMembershipCard);
                return;
            }
            peUIModelList.push(addNormalMembershipCard(membership));
            return;
        } else if (!membership?.memId &&
            membership?.task &&
            membership?.tempCustInfoData &&
            membership?.tempCustInfoData?.enableFDFlow &&
            [USER_TASK.DOC_UPLOAD].includes(membership?.task?.name)) {
            let fdMembership = addFraudDetectionMembershipCard(membership, fdFlowSmsStatusData);
            if (fdMembership) {
                peUIModelList.push(fdMembership);
                return;
            }

        } else {
            peUIModelList.push(processPendingPEMembership(membership));
        }
    });
    return peUIModelList;
};

export const getServiceFromMembership = (membership, serviceRequestId) => {
    return (membership?.srResponseData ?? []).filter(serviceObj => serviceObj?.serviceRequestId == serviceRequestId).first();
};


function processPendingPEMembership(membership) {
    let expiryText = "";
    let assetName = getAssetListText(membership);
    let subHeading = "";

    if (membership?.isABBPlan) {
        subHeading = {
            text: MEMBERSHIP_TAB_CARD_TEXT.GET_GUARANTEED_EXCHANGE_MSG,
            style: { color: Colors.color_888F97, fontSize: 12 },
        };
    } else if (membership?.doesPlanSupportsPMS) {
        subHeading = {
            text: MEMBERSHIP_TAB_CARD_TEXT.INCLUDES_PREVENTIVE_MAINTENANCE_SERVICE,
            style: { color: Colors.color_212121, fontSize: 12 },
        };
    }
    if (membership?.uploadDocsLastDate) {
        expiryText = MEMBERSHIP_TAB_CARD_TEXT.ACTIVATE_BY(formattedDate(new Date(new Number(membership?.uploadDocsLastDate)), "dd mmm yyyy"));
    }
    let actionView = [{
        type: MembershipCardActionsType.PRIMARY_BUTTON,
        button: {
            text: MEMBERSHIP_TAB_CARD_TEXT.ACTIVATE_NOW_TEXT,
            action: MembershipButtonActions.ACTIVATE_PE_MEMBERSHIP,
            data: {
                membershipResponse: membership,
            },
        },
    }];
    return {
        heading: membership?.plan?.planName,
        image: getPEProductImage(membership),
        subHeading: subHeading,
        description: {
            text: assetName,
        },
        note: { text: expiryText, style: { color: getExpiryTextColor(membership) } },
        actionViews: actionView,
        membershipResponse: membership,
        priority: PRIORITY.USER_ACTION_PENDING_SALES,
    };
}


export const addFraudDetectionMembershipCard = (membership, fdFlowSmsStatusData) => {
    let heading = membership?.plan?.planName;
    let image = getPEProductImage(membership);
    let expiryText = getValidTillText(membership);
    let tag = {};
    let assetName = "";

    let description = null;
    let btnText = null;
    let tagStatus = null;
    let strLastDateRaiseAClaim = null;
    let uiCardItem = null;
    let smsSendStatus = false;
    if (membership?.uploadDocsLastDate) {
        strLastDateRaiseAClaim = formattedDate(new Date(new Number(membership?.uploadDocsLastDate)), "dS mmmm");
    }

    if (membership?.tempCustInfoData?.task) {
        let userTask = membership?.tempCustInfoData?.task?.[0];
        let customerDetails = membership?.tempCustInfoData?.customerDetails?.[0];
        if (fdFlowSmsStatusData) {
            if (Platform.OS === PLATFORM_OS.ios) {
                smsSendStatus = fdFlowSmsStatusData?.includes(customerDetails?.orderId?.toString())
            } else {
                smsSendStatus = fdFlowSmsStatusData?.includes(customerDetails?.activityRefId);
            }
        }
        tagStatus = userTask?.status;
        switch (userTask?.status) {
            case CARD_STATUS.POSTDTLPENDING:
                if ([CARD_STATUS.FD_MT_FLOW].includes(userTask?.fdTestType)) {
                    description = MEMBERSHIP_TAB_CARD_TEXT.UPLOAD_COMPLETE_ACTIVATION_DESC(strLastDateRaiseAClaim);
                    btnText = MEMBERSHIP_TAB_CARD_TEXT.FD_ACTIVATE_NOW;
                } else {
                    description = MEMBERSHIP_TAB_CARD_TEXT.UPLOAD_PENDING_DESC(strLastDateRaiseAClaim);
                    btnText = MEMBERSHIP_TAB_CARD_TEXT.FD_GET_STATED;
                }
                break;
            case CARD_STATUS.POSTDTLCOMPLETE:
                description = MEMBERSHIP_TAB_CARD_TEXT.UPLOAD_PENDING_DESC(strLastDateRaiseAClaim);
                btnText = MEMBERSHIP_TAB_CARD_TEXT.FD_UPLOAD_DOCUMENTS;
                if ([CARD_STATUS.FD_MT_FLOW].includes(userTask?.fdTestType)) {
                    description = MEMBERSHIP_TAB_CARD_TEXT.FD_MT_FLOW_MEMBERSHIP_CANCELLATION;
                    btnText = MEMBERSHIP_TAB_CARD_TEXT.FD_BTN_LABEL_CONTINUE;
                } else {
                    if (smsSendStatus) {
                        description = MEMBERSHIP_TAB_CARD_TEXT.FD_MEM_CARD_OPEN_IMEI_SCREEN(
                            customerDetails?.deviceMake,
                            customerDetails?.verificationNo,
                        );
                        btnText = MEMBERSHIP_TAB_CARD_TEXT.FD_OPEN_IMEI_SCREEN;
                    } else {
                        btnText = MEMBERSHIP_TAB_CARD_TEXT.FD_UPLOAD_DOCUMENTS;
                    }
                }
                break;
            case CARD_STATUS.REUPLOAD:
                let docsRequireToUpload = fdDocRequireToUpload(membership?.docsToUploadData);
                if (docsRequireToUpload?.isFrontPanelRequire && docsRequireToUpload?.isBackPanelRequire) {
                    description = MEMBERSHIP_TAB_CARD_TEXT.REUPLOAD_FRONT_BACK_PANEL_DESC;
                } else if (docsRequireToUpload?.isFrontPanelRequire) {
                    description = MEMBERSHIP_TAB_CARD_TEXT.REUPLOAD_FRONT_DESC(strLastDateRaiseAClaim);
                } else if (docsRequireToUpload?.isBackPanelRequire) {
                    description = MEMBERSHIP_TAB_CARD_TEXT.REUPLOAD_BACK_DESC(strLastDateRaiseAClaim);
                } else {
                    description = MEMBERSHIP_TAB_CARD_TEXT.REUPLOAD_DESC(strLastDateRaiseAClaim);
                }
                if ([CARD_STATUS.FD_MT_FLOW].includes(userTask?.fdTestType)) {
                    btnText = MEMBERSHIP_TAB_CARD_TEXT.FD_BTN_LABEL_CONTINUE;
                } else {
                    btnText = MEMBERSHIP_TAB_CARD_TEXT.FD_UPLOAD_DOCUMENTS;
                }
                break;
            case CARD_STATUS.PENDING:
                description = MEMBERSHIP_TAB_CARD_TEXT.PENDING_DESC;
                break;
            case CARD_STATUS.CANCELLED:
                description = MEMBERSHIP_TAB_CARD_TEXT.CANCEL_DESC;
                break;
            case CARD_STATUS.REJECTED:
                description = MEMBERSHIP_TAB_CARD_TEXT.REJECTED_DESC;
                break;
            case CARD_STATUS.APPROVED:
                description = MEMBERSHIP_TAB_CARD_TEXT.DOCUMENT_APPROVED;
                break;
            case CARD_STATUS.QUEUED:
                description = MEMBERSHIP_TAB_CARD_TEXT.MEMBERSHIP_QUEUED;
                break;
        }
        if (customerDetails?.deviceMake) {
            assetName = customerDetails?.deviceMake + " ";
        }
        if (customerDetails?.deviceModel) {
            assetName = assetName + customerDetails?.deviceModel;
        }


        if (userTask?.status === CARD_STATUS.REUPLOAD) {
            if (Platform.OS === PLATFORM_OS.android) {
                nativeBridgeRef.removewFDReUploadKey(customerDetails?.activityRefId);
            }
        }
        tag.text = getStatusText(userTask?.status).toUpperCase();
        tag.textColor = getStatusTagColor(userTask?.status);
        tag.borderColor = getStatusTagColor(userTask?.status);

        if (Platform.OS === PLATFORM_OS.android && customerDetails?.deviceMake?.toLowerCase() === "apple") {
            description = MEMBERSHIP_TAB_CARD_TEXT.APPLE_PLAN_ON_ANDROID_MSG;
            tag.text = getStatusText(userTask?.status).toUpperCase();
            tag.textColor = Colors.STATUS_BG_YELLOW;
            tag.borderColor = Colors.STATUS_BG_YELLOW;

            let statusView = {
                bgColor: Colors.BG_DESCRIPTION_YELLOW_FEF9EC,
                button: {
                    action: "TEXT_WITH_BUTTON",
                },
                views: [
                    {
                        type: "TEXT_WITH_BUTTON",
                        heading: null,
                        description: { text: description },
                        image: images.yellowInfo,
                    },
                ],

            };
            uiCardItem = {
                heading: heading,
                image: image,
                description: {
                    text: assetName,
                    tappableText: {
                        data: {},
                    },
                },
                note: { text: expiryText, style: { color: getExpiryTextColor(membership) } },
                tag: tag,
                statusViews: statusView,
                priority: PRIORITY.ACTIVE_MEMBERSHIP_PERSONAL_ELECTRONICS,
            };
        } else if (Platform.OS === PLATFORM_OS.ios && customerDetails?.deviceMake?.toLowerCase() !== "apple") {
            description = MEMBERSHIP_TAB_CARD_TEXT.ANDROID_PLAN_ON_APPLE_MSG(customerDetails?.deviceMake);
            tag.text = getStatusText(userTask?.status).toUpperCase();
            tag.textColor = Colors.STATUS_BG_YELLOW;
            tag.borderColor = Colors.STATUS_BG_YELLOW;
            let statusView = {
                bgColor: Colors.BG_DESCRIPTION_YELLOW_FEF9EC,
                button: {
                    action: "TEXT_WITH_BUTTON",
                },
                views: [
                    {
                        type: "TEXT_WITH_BUTTON",
                        heading: null,
                        description: { text: description },
                        image: images.yellowInfo,
                    },
                ],

            };
            uiCardItem = {
                heading: heading,
                image: image,
                description: {
                    text: assetName,
                    tappableText: {
                        data: {},
                    },
                },
                note: { text: expiryText, style: { color: getExpiryTextColor(membership) } },
                tag: tag,
                statusViews: statusView,
                membershipResponse: membership,
                priority: PRIORITY.ACTIVE_MEMBERSHIP_PERSONAL_ELECTRONICS,
            };
        } else {
            if ([CARD_STATUS.POSTDTLPENDING, CARD_STATUS.POSTDTLCOMPLETE, CARD_STATUS.REUPLOAD].includes(userTask?.status)) {
                let actionView = [{
                    type: MembershipCardActionsType.PRIMARY_BUTTON,
                    button: {
                        text: btnText,
                        action: MembershipButtonActions.SALES_UPLOAD_DOCUMENTS,
                        data: {},
                    },
                }];
                let statusView = {};
                if (![CARD_STATUS.FD_MT_FLOW].includes(userTask?.fdTestType) &&
                    [CARD_STATUS.POSTDTLCOMPLETE, CARD_STATUS.REUPLOAD].includes(userTask?.status) && smsSendStatus) {
                    statusView = {
                        bgColor: getDescriptionColor(tagStatus),
                        button: {
                            action: MembershipButtonActions.FD_OPEN_SEND_SMS_SCREEN,
                            data: {},
                        },
                        views: [
                            {
                                type: "TEXT_WITH_BUTTON",
                                description: { text: description },
                                image: images.yellowInfo,
                            },
                            {
                                type: "TEXT_WITH_BUTTON",
                                button: {
                                    isArrowButton: false,
                                    action: MembershipButtonActions.FD_OPEN_SEND_SMS_SCREEN,
                                    text: MEMBERSHIP_TAB_CARD_TEXT.TEXT_RESEND_IMAGE_UPLOAD_LINK,
                                    data: {},
                                },
                            },
                        ],
                    };
                } else {
                    statusView = {
                        bgColor: getDescriptionColor(tagStatus),
                        views: [
                            {
                                type: "TEXT_WITH_BUTTON",
                                heading: null,
                                description: { text: description },
                                image: images.yellowInfo,
                            },
                        ],
                    };
                }
                uiCardItem = {
                    heading: heading,
                    image: image,
                    description: {
                        text: assetName,
                        tappableText: {
                            data: {},
                        },
                    },
                    note: { text: expiryText, style: { color: getExpiryTextColor(membership) } },
                    tag: tag,
                    membershipResponse: membership,
                    statusViews: statusView,
                    actionViews: actionView,
                    priority: PRIORITY.USER_ACTION_PENDING_SALES,
                };
            } else {
                let statusView = {
                    bgColor: getDescriptionColor(tagStatus),
                    views: [
                        {
                            type: "TEXT_WITH_BUTTON",
                            heading: null,
                            description: { text: description },
                            image: images.yellowInfo,
                        },
                    ],

                };
                uiCardItem = {
                    heading: heading,
                    image: image,
                    description: {
                        text: assetName,
                        tappableText: {
                            data: {},
                        },
                    },
                    note: { text: expiryText, style: { color: getExpiryTextColor(membership) } },
                    tag: tag,
                    statusViews: statusView,
                    membershipResponse: membership,
                    priority: PRIORITY.USER_ACTION_PENDING_SALES,
                };
            }
        }
    }
    return uiCardItem;
};

function addNormalMembershipCard(membership) {
    let heading = membership?.plan?.planName;
    let image = getPEProductImage(membership);
    let expiryText = getValidTillText(membership);
    let actionViews = [];
    let dualActionButton = {};
    let isABBOnlyActionText = (isABBOnlyMembership(membership) ? ButtonTexts.TXT_SELL_YOUR_PHONE : ButtonTexts.TXT_BOOK_A_SERVICE);
    dualActionButton.type = "DUAL_ACTION_BUTTON";
    dualActionButton.leftButton = {
        text: VIEW_DETAIL,
        action: MembershipButtonActions.VIEW_DETAILS,
        data: {
            membershipResponse: membership,
        },
    };
    let subHeading = "";
    if (membership?.isABBPlan) {
        subHeading = {
            text: MEMBERSHIP_TAB_CARD_TEXT.GET_GUARANTEED_EXCHANGE_MSG,
            style: { color: Colors.color_888F97, fontSize: 12 },
        };
    } else if (membership?.doesPlanSupportsPMS) {
        subHeading = {
            text: MEMBERSHIP_TAB_CARD_TEXT.INCLUDES_PREVENTIVE_MAINTENANCE_SERVICE,
            style: { color: Colors.color_212121, fontSize: 12 },
        };
    }
    if (isABBOnlyMembership(membership)) {
        dualActionButton.rightButton = {
            text: ButtonTexts.TXT_SELL_YOUR_PHONE,
            isEnabled: membership?.enableFileClaim || false,
            action: MembershipButtonActions.RAISE_CLAIM,
            data: {},
        };
    } else {
        dualActionButton.rightButton = {
            text: isABBOnlyActionText,
            isEnabled: membership?.enableFileClaim || false,
            action: MembershipButtonActions.RAISE_CLAIM,
            data: {},
        };
    }
    actionViews.push(dualActionButton);
    return {
        heading: heading,
        image: image,
        subHeading: subHeading,
        description: {
            text: getAssetListText(membership),
            tappableText: {
                data: {
                    membershipResponse: membership,
                },
            },
        },
        note: { text: expiryText, style: { color: getExpiryTextColor(membership) } },
        actionViews: actionViews,
        membershipResponse: membership,
        priority: PRIORITY.ACTIVE_MEMBERSHIP_PERSONAL_ELECTRONICS,
    };

}


export const addSRCard = (membership, categoryType, isReturnStatusViewsOnly) => {
    let heading = categoryType === CATEGORY.PERSONAL_ELECTRONICS ? membership?.plan?.planName : getHAMembershipPlanName(membership);
    let peProductImg=getPEProductImage(membership);

    let image = (categoryType === CATEGORY.PERSONAL_ELECTRONICS ? peProductImg : isExtendedWarrantyPlan(membership) ? images.buy_plan_ew_icon : images.buy_plan_ha_icon);
    let subHeading = "";
    let noteExpiryText = { text: getValidTillText(membership), style: { color: getExpiryTextColor(membership) } };
    let isABBOnlyActionText = (isABBOnlyMembership(membership) ? ButtonTexts.TXT_SELL_YOUR_PHONE : ButtonTexts.TXT_BOOK_A_SERVICE);

    if (membership?.isABBPlan) {
        subHeading = {
            text: MEMBERSHIP_TAB_CARD_TEXT.GET_GUARANTEED_EXCHANGE_MSG,
            style: { color: Colors.color_888F97, fontSize: 12 },
        };
    } else if (membership?.doesPlanSupportsPMS) {
        subHeading = {
            text: MEMBERSHIP_TAB_CARD_TEXT.INCLUDES_PREVENTIVE_MAINTENANCE_SERVICE,
            style: { color: Colors.color_212121, fontSize: 12 },
        };
    }

    let serviceRequestsList = membership?.srResponseData;
    if (serviceRequestsList) {
        // get Rating serviceRequests
        let completedSrCount = getAllRatingCompletedSrList(serviceRequestsList)?.length || 0;
        let statusViewsList = [];
        let totalCompletedCount=0;
        serviceRequestsList?.forEach(serviceRequest => {
            // check for rating
            // if status is CO or CRES
            if ((([SERVICE_REQUEST_STATUS.COMPLETE, SERVICE_REQUEST_STATUS.CLOSED_RESOLVED].includes(serviceRequest?.status)))) {
                if ([SERVICE_REQUEST_WORKFLOW_STAGE_STATUS.SUCCESS, SERVICE_REQUEST_WORKFLOW_STAGE_STATUS.SUCCESSFULLY].includes(serviceRequest?.workflowStageStatus) &&
                    !serviceRequest?.serviceRequestFeedback?.feedbackRating) {
                    let statusView = getMembershipSRCardViews(CARD_STATUS.SR_RATING_PENDING, membership, serviceRequest);
                    if (statusView) {
                        statusViewsList.push(statusView);
                        totalCompletedCount=totalCompletedCount+1;
                    }
                }
                return;
            }
            //check for awaiting confirmation
            else if ((!serviceRequest?.serviceRequestId || !serviceRequest?.refPrimaryTrackingNo || !serviceRequest?.workflowStageStatus)) {
                let statusView = getMembershipSRCardViews(CARD_STATUS.AWAITING_CONFIRMATION, membership, serviceRequest);
                if (statusView) {
                    statusViewsList.push(statusView);
                }
            }
            // check for technician visit re schedule
            else if ([SERVICE_REQUEST_STATUS.ON_HOLD].includes(serviceRequest?.status)) {
                if ([SERVICE_REQUEST_WORKFLOW_STAGE_STATUS.TECHNICIAN_CANCELLED_OTHER,
                    SERVICE_REQUEST_WORKFLOW_STAGE_STATUS.TECHNICIAN_CANCELLED_CUSTOMER_NOT_AVAILABLE].includes(serviceRequest?.workflowStageStatus)) {
                    let statusView = getMembershipSRCardViews(CARD_STATUS.RE_SCHEDULE_VISIT, membership, serviceRequest);
                    if (statusView) {
                        statusViewsList.push(statusView);
                    }
                } else {
                    let statusView = getMembershipSRCardViews(CARD_STATUS.ON_HOLD, membership, serviceRequest);
                    if (statusView) {
                        statusViewsList.push(statusView);
                    }
                }
            }// check for pending submission
            else if ([SERVICE_REQUEST_WORKFLOW_STAGE.DOCUMENT_UPLOAD].includes(serviceRequest?.workflowStage) &&
                SERVICE_REQUEST_WORKFLOW_STAGE_STATUS.VERIFICATION_UNSUCCESSFUL !== serviceRequest?.workflowStageStatus &&
                serviceRequest?.initiatingSystem == 3 &&
                serviceRequest?.modifiedBy !== APIData?.customer_id) {
                let statusView = getMembershipSRCardViews(CARD_STATUS.PENDING_SUBMISSION, membership, serviceRequest);
                if (statusView) {
                    statusViewsList.push(statusView);
                }
            }// check for upload document and pendencies
            else if ([SERVICE_REQUEST_WORKFLOW_STAGE.DOCUMENT_UPLOAD].includes(serviceRequest?.workflowStage)) {

                if ([SERVICE_REQUEST_WORKFLOW_STAGE_STATUS.DOCUMENT_UPLOAD_PENDING].includes(serviceRequest?.workflowStageStatus)) {
                    let statusView = getMembershipSRCardViews(CARD_STATUS.DOCUMENT_UPLOAD_PENDING, membership, serviceRequest);
                    if (statusView) {
                        statusViewsList.push(statusView);
                    }
                } else if ([SERVICE_REQUEST_WORKFLOW_STAGE_STATUS.VERIFICATION_UNSUCCESSFUL].includes(serviceRequest?.workflowStageStatus)) {
                    let statusView = getMembershipSRCardViews(CARD_STATUS.DOCUMENT_RE_UPLOAD_PENDING, membership, serviceRequest);
                    if (statusView) {
                        statusViewsList.push(statusView);
                    }
                }
            } else {
                let statusView = getMembershipSRCardViews(CARD_STATUS.RAISED, membership, serviceRequest);
                if (statusView) {
                    statusViewsList.push(statusView);
                }
            }
        });

        if (isReturnStatusViewsOnly) {
            return statusViewsList;
        }

        if (statusViewsList?.length > 0) {
            let dualActionButton = {};
            let priority = PRIORITY.OA_ACTION_PENDING_CLAIMS;
            let cardItem = {
                heading: heading,
                image: image,
                subHeading: subHeading,
                description: {
                    text: getAssetListText(membership),
                    tappableText: {
                        data: {
                            membershipResponse: membership,
                        },
                    },
                },
                note: noteExpiryText,
                membershipResponse: membership,
            };
            let statusViewsData;
            dualActionButton.type = "DUAL_ACTION_BUTTON";
            dualActionButton.leftButton = {
                text: VIEW_DETAIL,
                action: MembershipButtonActions.VIEW_DETAILS,
                data: {},
            };
            dualActionButton.rightButton = {
                text: isABBOnlyActionText,
                isEnabled: membership?.enableFileClaim || false,
                action: MembershipButtonActions.RAISE_CLAIM,
                data: {},
            };

            if (statusViewsList?.length == 1) {
                statusViewsData = statusViewsList[0];
            } else {
                let isActionRequired = statusViewsList?.some(item => item?.isActionRequired);
                statusViewsData = getMembershipMultipleSrCard(statusViewsList, membership, totalCompletedCount, serviceRequestsList,membership?.intimationArr,isActionRequired);
            }
            if (CATEGORY.HOME_APPLIANCE === getSingleCategory(membership?.categories) &&
                membership?.intimationArr?.length > 0){
                statusViewsData = getMembershipMultipleSrCard(statusViewsList, membership, totalCompletedCount, serviceRequestsList,membership?.intimationArr,true);
            }
            if (statusViewsData?.isActionRequired) {
                if (statusViewsData.STATUS === CARD_STATUS.SR_RATING_PENDING) {
                    priority = PRIORITY.CLAIM_SUCCESSFUL;
                } else {
                    priority = PRIORITY.USER_ACTION_PENDING_CLAIMS;
                }
            }
            cardItem.priority = priority;
            cardItem.statusViews = statusViewsData?.statusViews;
            cardItem.actionViews = [dualActionButton];
            cardItem.membershipResponse = membership;
            return {
                ...cardItem,
            };
        }

    }
    if (membership?.intimationArr?.length > 0) {
        return setPendingSrCard(Status.RESUME_CLAIM_SERVICE, membership);
    }
    return null;
};

export const getAllRatingCompletedSrList = (serviceRequestsList) => {
    let ratingSrList = serviceRequestsList?.filter((serviceRequest) => {
        return (([SERVICE_REQUEST_STATUS.COMPLETE, SERVICE_REQUEST_STATUS.CLOSED_RESOLVED].includes(serviceRequest?.status) &&
            [SERVICE_REQUEST_WORKFLOW_STAGE_STATUS.SUCCESS, SERVICE_REQUEST_WORKFLOW_STAGE_STATUS.SUCCESSFULLY].includes(serviceRequest?.workflowStageStatus) &&
            serviceRequest?.serviceRequestFeedback?.feedbackRating));
    });
    return ratingSrList;
};

export const getMembershipMultipleSrCard = (statusViewsList, membership, completedSrCount, serviceRequestsList,isActionRequired) => {
    let descriptionStyle = {
        ...TextStyle.text_16_bold,
        color: Colors.color_212121,
    };
    let headingStyle = {
        ...TextStyle.text_12_regular,
        color: Colors.color_212121,
    };
    let totalIntimationCount=membership?.intimationArr?.length || 0;
    let headingText = getOnGoingSrCountText(completedSrCount, statusViewsList.length+totalIntimationCount);
    let statusView = {};
    if (isActionRequired) {
        statusView = {
            bgColor: Colors.BG_DESCRIPTION_YELLOW_FEF9EC,
            button: {
                action: MembershipButtonActions.SR_MULTIPLE_STATUS,
                data: {},
            },
            views: [
                {
                    type: "TEXT_WITH_BUTTON",
                    heading: [{ text: headingText, style: headingStyle }],
                    description: {
                        text: MEMBERSHIP_TAB_CARD_TEXT.TXT_IMMEDIATE_ACTION_NEEDED,
                        style: descriptionStyle,
                    },
                    image: images.yellowInfo,
                    button: {
                        isArrowButton: true,
                        text: MEMBERSHIP_TAB_CARD_TEXT.TXT_VIEW_STATUS,
                        action: MembershipButtonActions.SR_MULTIPLE_STATUS,
                        data: {},
                    },
                },
            ],
        };
    } else {
        statusView = {
            bgColor: Colors.BG_DESCRIPTION_GREEN,
            button: {
                action: MembershipButtonActions.SR_MULTIPLE_STATUS,
                data: {},
            },
            views: [
                {
                    type: "TEXT_WITH_BUTTON",
                    heading: null,
                    description: { text: headingText, style: descriptionStyle },
                    button: {
                        isArrowButton: true,
                        text: MEMBERSHIP_TAB_CARD_TEXT.TXT_VIEW_STATUS,
                        action: MembershipButtonActions.SR_MULTIPLE_STATUS,
                        data: {},
                    },
                },
            ],
        };
    }
    return {
        statusViews: statusView,
        isActionRequired: isActionRequired,
        membershipResponse: membership,
    };
};

function getOnGoingSrCountText(totalCompletedSr, totalSrCount) {
    let onGoingRequestsCount = totalSrCount - totalCompletedSr;
    let onGoingSrText = [];
    //console.info("totalCompletedSr :: "+totalCompletedSr +" ==== totalSrCount :: "+totalSrCount)
    if (onGoingRequestsCount > 0) {
        onGoingSrText.push(onGoingRequestsCount + " on-going");
    }
    if (totalCompletedSr > 0) {
        if (onGoingRequestsCount > 0) {
            onGoingSrText.push(" & " + totalCompletedSr + " completed request(s)");
        } else {
            onGoingSrText.push(totalCompletedSr + " completed request(s)");
        }
    } else {
        onGoingSrText.push(" " + " request(s)");
    }

    return onGoingSrText.join("");

}

export const getMembershipSRCardViews = (STATUS, membership, serviceRequestData) => {
    let statusView = {};
    let isActionRequired = false;
    let descriptionStyle = {
        ...TextStyle.text_14_bold,
        color: Colors.color_212121,
    };
    let headingStyle = {
        ...TextStyle.text_12_regular,
        color: Colors.color_212121,
    };
    let headingDescStyle = {
        ...TextStyle.text_12_regular,
        color: Colors.color_888F97,
    };
    let pendencyDescStyle = {
        ...TextStyle.text_14_normal,
        color: Colors.color_212121,
    };


    switch (STATUS) {
        case CARD_STATUS.AWAITING_CONFIRMATION:
            statusView = {
                bgColor: getDescriptionColor(CARD_STATUS.AWAITING_CONFIRMATION),
                button: {
                    action: "",
                },
                views: [
                    {
                        type: "TEXT_WITH_BUTTON",
                        heading: null,
                        description: {
                            text: MEMBERSHIP_TAB_CARD_TEXT.TEXT_AWAITING_CONFIRMATION_DESCRIPTION,
                            style: descriptionStyle,
                        },

                    },
                ],
            };
            break;
        case CARD_STATUS.SR_RATING_PENDING:
            isActionRequired = true;
            statusView = {
                bgColor: getDescriptionColor(CARD_STATUS.SR_RATING_PENDING),
                button: {
                    action: MembershipButtonActions.RATE_US,
                    data: {
                        serviceRequestId: serviceRequestData?.serviceRequestId,
                    },
                },
                views: [
                    {
                        type: "TEXT_WITH_BUTTON",
                        "heading": [{
                            text: getWorkFlowStageDescription(serviceRequestData),
                            style: pendencyDescStyle,
                        }],
                    },
                    {
                        type: "TEXT_WITH_BUTTON",
                        heading: null,
                        button: {
                            isArrowButton: true,
                            text: MEMBERSHIP_TAB_CARD_TEXT.RATE_US,
                            action: MembershipButtonActions.RATE_US,
                            data: {
                                serviceRequestId: serviceRequestData?.serviceRequestId,
                            },
                        },
                    },
                ],
            };
            break;
        case CARD_STATUS.RE_SCHEDULE_VISIT:
            isActionRequired = true;
            statusView = {
                bgColor: getDescriptionColor(CARD_STATUS.RE_SCHEDULE_VISIT),
                button: {
                    action: MembershipButtonActions.SR_RESCHEDULE_VISIT,
                    data: {
                        serviceRequestId: serviceRequestData?.serviceRequestId,
                    },
                },
                views: [
                    {
                        type: "TEXT_WITH_BUTTON",
                        heading: [{
                            text: MEMBERSHIP_TAB_CARD_TEXT.TXT_SERVICE_REQ_ID(serviceRequestData?.refPrimaryTrackingNo),
                            style: headingStyle,
                        }],
                        description: { text: MEMBERSHIP_TAB_CARD_TEXT.SR_RESCEDULE_VISIT, style: descriptionStyle },
                        button: {
                            isArrowButton: true,
                            text: MEMBERSHIP_TAB_CARD_TEXT.RE_SCHEDULE,
                            action: MembershipButtonActions.SR_RESCHEDULE_VISIT,
                            data: {
                                serviceRequestId: serviceRequestData?.serviceRequestId,
                            },
                        },
                    },
                    {
                        type: "TEXT_WITH_BUTTON",
                        "heading": [{ text: getWorkFlowStageDescription(serviceRequestData), style: headingDescStyle }],
                    },
                ],
            };
            break;
        case CARD_STATUS.ON_HOLD:
            isActionRequired = false;
            statusView = {
                bgColor: getDescriptionColor(CARD_STATUS.ON_HOLD),
                button: {
                    action: MembershipButtonActions.SR_VIEW_TIMELINE,
                    data: {
                        serviceRequestId: serviceRequestData?.serviceRequestId,
                    },
                },
                views: [
                    {
                        type: "TEXT_WITH_BUTTON",
                        heading: [{
                            text: MEMBERSHIP_TAB_CARD_TEXT.TXT_SERVICE_REQ_ID(serviceRequestData?.refPrimaryTrackingNo),
                            style: headingStyle,
                        }],
                        description: { text: MEMBERSHIP_TAB_CARD_TEXT.ON_HOLD_LOWERCASE, style: descriptionStyle },
                        button: {
                            isArrowButton: true,
                            text: MEMBERSHIP_TAB_CARD_TEXT.TXT_VIEW_DETAILS,
                            action: MembershipButtonActions.SR_VIEW_TIMELINE,
                            data: {
                                serviceRequestId: serviceRequestData?.serviceRequestId,
                            },
                        },
                    },
                    {
                        type: "TEXT_WITH_BUTTON",
                        "heading": [{ text: getWorkFlowStageDescription(serviceRequestData), style: headingDescStyle }],
                    },
                ],
            };
            break;
        case CARD_STATUS.PENDING_SUBMISSION:
            isActionRequired = true;
            statusView = {
                bgColor: getDescriptionColor(CARD_STATUS.PENDING_SUBMISSION),
                button: {
                    action: MembershipButtonActions.PENDING_SUBMISSION,
                    data: {
                        serviceRequestId: serviceRequestData?.serviceRequestId,
                    },
                },
                views: [
                    {
                        type: "TEXT_WITH_BUTTON",
                        heading: [{ text: getWorkFlowStageDescription(serviceRequestData), style: pendencyDescStyle }],
                    },
                    {
                        type: "TEXT_WITH_BUTTON",
                        description: null,
                        button: {
                            isArrowButton: true,
                            text: MEMBERSHIP_TAB_CARD_TEXT.VERIFY_NOW,
                            action: MembershipButtonActions.PENDING_SUBMISSION,
                            data: {
                                serviceRequestId: serviceRequestData?.serviceRequestId,
                            },
                        },
                    },
                ],
            };
            break;
        case CARD_STATUS.DOCUMENT_UPLOAD_PENDING:
            isActionRequired = true;
            statusView = {
                bgColor: getDescriptionColor(CARD_STATUS.DOCUMENT_UPLOAD_PENDING),
                button: {
                    action: MembershipButtonActions.SR_UPLOAD_DOCUMENT,
                    data: {
                        serviceRequestId: serviceRequestData?.serviceRequestId,
                    },
                },
                views: [
                    {
                        type: "TEXT_WITH_BUTTON",
                        heading: [{
                            text: MEMBERSHIP_TAB_CARD_TEXT.TXT_SERVICE_REQ_ID(serviceRequestData?.refPrimaryTrackingNo),
                            style: headingStyle,
                        }],
                        description: {
                            text: MEMBERSHIP_TAB_CARD_TEXT.DOCUMENT_UPLOAD_PENDING,
                            style: descriptionStyle,
                        },
                        button: {
                            isArrowButton: true,
                            text: MEMBERSHIP_TAB_CARD_TEXT.TEXT_UPLOAD,
                            action: MembershipButtonActions.SR_UPLOAD_DOCUMENT,
                            data: {
                                serviceRequestId: serviceRequestData?.serviceRequestId,
                            },
                        },
                    },
                    {
                        type: "TEXT_WITH_BUTTON",
                        heading: [{ text: getWorkFlowStageDescription(serviceRequestData), style: headingDescStyle }],
                    },
                ],
            };
            break;
        case CARD_STATUS.DOCUMENT_RE_UPLOAD_PENDING:
            isActionRequired = true;
            statusView = {
                bgColor: getDescriptionColor(CARD_STATUS.DOCUMENT_RE_UPLOAD_PENDING),
                button: {
                    action: MembershipButtonActions.SR_UPLOAD_PENDENCIES,
                    data: {
                        serviceRequestId: serviceRequestData?.serviceRequestId,
                    },
                },
                views: [
                    {
                        type: "TEXT_WITH_BUTTON",
                        heading: [{
                            text: MEMBERSHIP_TAB_CARD_TEXT.TXT_SERVICE_REQ_ID(serviceRequestData?.refPrimaryTrackingNo),
                            style: headingStyle,
                        }],
                        description: {
                            text: MEMBERSHIP_TAB_CARD_TEXT.DOCUMENT_RE_UPLOAD_PENDING,
                            style: descriptionStyle,
                        },
                        button: {
                            isArrowButton: true,
                            text: MEMBERSHIP_TAB_CARD_TEXT.TEXT_UPLOAD,
                            action: MembershipButtonActions.SR_UPLOAD_PENDENCIES,
                            data: {
                                serviceRequestId: serviceRequestData?.serviceRequestId,
                            },
                        },
                    },
                    {
                        type: "TEXT_WITH_BUTTON",
                        heading: [{ text: getWorkFlowStageDescription(serviceRequestData), style: headingDescStyle }],
                    },
                ],
            };
            break;
        case CARD_STATUS.RAISED:
            isActionRequired = false;
            statusView = {
                bgColor: Colors.BG_DESCRIPTION_GREEN,
                button: {
                    action: MembershipButtonActions.SR_VIEW_TIMELINE,
                    data: {
                        serviceRequestId: serviceRequestData?.serviceRequestId,
                    },
                },
                views: [
                    {
                        type: "TEXT_WITH_BUTTON",
                        heading: [{
                            text: MEMBERSHIP_TAB_CARD_TEXT.TXT_SERVICE_REQ_ID(serviceRequestData?.refPrimaryTrackingNo),
                            style: headingStyle,
                        }],
                        description: { text: MEMBERSHIP_TAB_CARD_TEXT.SERVICE_IN_PROGRESS, style: descriptionStyle },
                        button: {
                            isArrowButton: true,
                            text: MEMBERSHIP_TAB_CARD_TEXT.TXT_VIEW_DETAILS,
                            action: MembershipButtonActions.SR_VIEW_TIMELINE,
                            data: {
                                serviceRequestId: serviceRequestData?.serviceRequestId,
                            },
                        },
                    },
                    {
                        type: "TEXT_WITH_BUTTON",
                        heading: [{ text: getWorkFlowStageDescription(serviceRequestData), style: headingDescStyle }],
                    },
                ],
            };
            break;
    }

    return {
        serviceRequestId: serviceRequestData?.serviceRequestId,
        statusViews: statusView,
        STATUS: STATUS,
        isActionRequired: isActionRequired,
    };
};


export const setPendingSrCard = (STATUS, membership) => {
    let categoryType=getSingleCategory(membership?.categories);
    let heading = categoryType === CATEGORY.PERSONAL_ELECTRONICS ? membership?.plan?.planName : getHAMembershipPlanName(membership);
    let peProductImg=getPEProductImage(membership);
    let image = (categoryType === CATEGORY.PERSONAL_ELECTRONICS ? peProductImg : isExtendedWarrantyPlan(membership) ? images.buy_plan_ew_icon : images.buy_plan_ha_icon);
    let expiryText = getValidTillText(membership);
    let actionViews = [];
    let intimationArr = membership?.intimationArr;
    let subHeading = "";
    let isABBOnlyActionText = (isABBOnlyMembership(membership) ? ButtonTexts.TXT_SELL_YOUR_PHONE : ButtonTexts.TXT_BOOK_A_SERVICE);


    if (membership?.isABBPlan) {
        subHeading = {
            text: MEMBERSHIP_TAB_CARD_TEXT.GET_GUARANTEED_EXCHANGE_MSG,
            style: { color: Colors.color_888F97, fontSize: 12 },
        };
    } else if (membership?.doesPlanSupportsPMS) {
        subHeading = {
            text: MEMBERSHIP_TAB_CARD_TEXT.INCLUDES_PREVENTIVE_MAINTENANCE_SERVICE,
            style: { color: Colors.color_212121, fontSize: 12 },
        };
    }

    if (intimationArr?.length) {
        let stageDescription = MEMBERSHIP_TAB_CARD_TEXT.YOU_HAVE_PENDING_SUBMISSION_TEXT(intimationArr.length);

        let statusViewActionText = (intimationArr.length > 1 ? MEMBERSHIP_TAB_CARD_TEXT.TXT_VIEW_STATUS :
            MEMBERSHIP_TAB_CARD_TEXT.TXT_RESUME_NOW);

        let statusViewAction = (intimationArr.length > 1 ? MembershipButtonActions.SR_MULTIPLE_STATUS :
            MembershipButtonActions.RESUME_CLAIM);

        let statusView = {
            bgColor: getDescriptionColor(STATUS),
            button: {
                action: MembershipButtonActions.RESUME_CLAIM,
                data: {},
            },
            views: [
                {
                    type: "TEXT_WITH_BUTTON",
                    heading: [{ text: stageDescription }],
                    description: null,
                    button: {
                        isArrowButton: true,
                        text: statusViewActionText,
                        action: statusViewAction,
                        data: {},
                    },
                },
            ],
        };
        let noteExpiryText = { text: expiryText, style: { color: getExpiryTextColor(membership) } };

        let dualActionButton = {};
        dualActionButton.type = "DUAL_ACTION_BUTTON";
        dualActionButton.leftButton = {
            text: VIEW_DETAIL,
            action: MembershipButtonActions.VIEW_DETAILS,
            data: {
                membershipResponse: membership,
            },
        };
        dualActionButton.rightButton = {
            text: isABBOnlyActionText,
            isEnabled: membership?.enableFileClaim || false,
            action: MembershipButtonActions.RAISE_CLAIM,
            data: {
                membershipResponse: membership,
            },
        };
        actionViews.push(dualActionButton);

        return {
            heading: heading,
            image: image,
            subHeading: subHeading,
            description: {
                text: getAssetListText(membership),
                tappableText: {
                    data: {},
                },
            },
            note: noteExpiryText,
            statusViews: statusView,
            actionViews: actionViews,
            priority: PRIORITY.USER_ACTION_PENDING_CLAIMS,
            membershipResponse: membership,
        };
    }
    return null;
};
export const prepareABBCard = (membership) => {
    let subHeading = "";
    let noteExpiryText = { text: getValidTillText(membership), style: { color: getExpiryTextColor(membership) } };
    let isABBOnlyActionText = (isABBOnlyMembership(membership) ? ButtonTexts.TXT_SELL_YOUR_PHONE : ButtonTexts.TXT_BOOK_A_SERVICE);
    if (membership?.isABBPlan) {
        subHeading = {
            text: MEMBERSHIP_TAB_CARD_TEXT.GET_GUARANTEED_EXCHANGE_MSG,
            style: { color: Colors.color_888F97, fontSize: 12 },
        };
    } else if (membership?.doesPlanSupportsPMS) {
        subHeading = {
            text: MEMBERSHIP_TAB_CARD_TEXT.INCLUDES_PREVENTIVE_MAINTENANCE_SERVICE,
            style: { color: Colors.color_212121, fontSize: 12 },
        };
    }

    if (membership?.assets != null && membership?.assets?.length > 0 && !membership?.enableFileClaim) {
        let abbCardView = null;
        let serviceList = membership?.assets?.[0]?.services;
        let serviceFilterList = serviceList?.filter((service) => service?.serviceName === ASSURED_BUYBACK_PLAN);
        if (serviceFilterList?.length > 0) {
            abbCardView = getAbbCardForOrderPlaced(serviceFilterList,membership);
            if (!abbCardView) {
                abbCardView = getAbbCardForIfServiceTenureActive(serviceFilterList);
            }
        }
        if (abbCardView) {
            let dualActionButton = {};
            dualActionButton.type = "DUAL_ACTION_BUTTON";
            dualActionButton.leftButton = {
                text: VIEW_DETAIL,
                action: MembershipButtonActions.VIEW_DETAILS,
                data: {},
            };
            dualActionButton.rightButton = {
                text: isABBOnlyActionText,
                isEnabled: membership?.enableFileClaim || false,
                action: MembershipButtonActions.RAISE_CLAIM,
                data: {},
            };
            return {
                heading: membership?.plan?.planName,
                image: getPEProductImage(membership),
                subHeading: subHeading,
                description: {
                    text: getAssetListText(membership),
                    tappableText: {
                        data: {},
                    },
                },
                membershipResponse: membership,
                note: noteExpiryText,
                actionViews: [dualActionButton],
                ...abbCardView,
            };
        }
    }
    return null;
};

function getAbbCardForOrderPlaced(serviceFilterList, membership) {
    if (membership?.buybackStatusModel?.buyBackStatus === ORDER_PLACED) {
        let statusView = {
            bgColor: getDescriptionColor(CARD_STATUS.IN_PROGRESS),
            button: {
                action: MembershipButtonActions.OPEN_BUYBACK_TIME_LINE,
                data: {},
            },
            views: [
                {
                    type: "TEXT_WITH_BUTTON",
                    heading: null,
                    description: { text: MEMBERSHIP_TAB_CARD_TEXT.TXT_YOUR_REQUEST_PROGRESS },
                    image: images.yellowInfo,
                    action: MembershipButtonActions.OPEN_BUYBACK_TIME_LINE,
                    data: {},
                },
                {
                    type: "TEXT_WITH_BUTTON",
                    heading: null,
                    button: {
                        isArrowButton: true,
                        text: MEMBERSHIP_TAB_CARD_TEXT.TXT_VIEW_DETAILS,
                        action: MembershipButtonActions.OPEN_BUYBACK_TIME_LINE,
                        data: {},
                    },
                },
            ],
        };
        let tag = {
            text: CARD_STATUS.IN_PROGRESS,
            textColor: getStatusTagColor(CARD_STATUS.IN_PROGRESS),
            borderColor: getStatusTagColor(CARD_STATUS.IN_PROGRESS),
        };

        return {
            statusViews: statusView,
            tag: tag,
            priority: PRIORITY.ACTIVE_MEMBERSHIP_PERSONAL_ELECTRONICS,
        };
    }
    return;
}

function getAbbCardForIfServiceTenureActive(serviceFilterList) {
    if (!serviceFilterList?.some(service => service?.isServiceTenureActive)) {
        let statusView = {
            bgColor: Colors.BG_DESCRIPTION_YELLOW_FEF9EC,
            button: {
                action: "",
            },
            views: [
                {
                    type: "TEXT_WITH_BUTTON",
                    heading: null,
                    description: { text: MEMBERSHIP_TAB_CARD_TEXT.TXT_SELL_YOUR_PHONE_ELIGIBILITY_DES },
                    image: images.yellowInfo,
                },
            ],
        };

        let tag = {
            text: CARD_STATUS.NEW,
            textColor: Colors.white,
            borderColor: Colors.white,
            bgColor: getStatusTagColor(CARD_STATUS.NEW),
        };
        return {
            statusViews: statusView,
            tag: tag,
            priority: PRIORITY.ACTIVE_MEMBERSHIP_PERSONAL_ELECTRONICS,
        };
    }
    return;
}


function fdDocRequireToUpload(docsToUploadData) {
    if (docsToUploadData?.deviceDetails) {
        let verificationDocuments = docsToUploadData?.deviceDetails?.[0]?.verificationDocuments;
        let frontPanel = verificationDocuments?.filter((verificationDocument) => CARD_STATUS.FRONT_PANEL_KEY === verificationDocument?.name);
        let backPanel = verificationDocuments?.filter((verificationDocument) => CARD_STATUS.BACK_PANEL_KEY === verificationDocument?.name);
        return {
            isFrontPanelRequire: ![CARD_STATUS.STR_ACCEPTED, CARD_STATUS.STR_PENDING].includes(frontPanel?.status),
            isBackPanelRequire: ![CARD_STATUS.STR_ACCEPTED, CARD_STATUS.STR_PENDING].includes(backPanel?.status),
        };
    }

    return {};
}
