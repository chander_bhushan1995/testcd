import {MembershipCardActionsType} from "./Components/MembershipCardActionViews/MembershipCardActionContainer";
import {VIEW_DETAIL} from "../Constants/AppConstants";
import {MembershipButtonActions} from "./Helpers/MembershipButtonActions";

export const getMembershipCardTemplate = () => {
    return {
        category: '',
        CardType: '',
        priority: 0,
        status: '',
        description: '',
        planImageId: '',
        expiryText: '',
        assetsText: '',
        planName: '',
        expiryTextColor: '',
        membershipData: {},
        SRData:{}
    }
};

export const getSRDataTemplate = () => {
    return {
        completedSrCount: 0,
        srUiModels: [],
        priority: 0,
        isActionRequired:false
    }
};

export const getSRUIModelTemplate = () => {
    return {
        totalStageIndices: 0,
        currentStageIndex: 0,
        serviceRequestId: '',
        currentStage:'',
        actionButton:{text:'',stateEnabled:true,action:null},
        isActionRequired:false,
        stageDescription:'',
        displayServiceId:'',
        intimationId:'',
        rightButton:{text:'',stateEnabled:true}
    }
};
