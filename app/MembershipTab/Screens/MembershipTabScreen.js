import React, { useContext, useRef, useEffect, useCallback, useReducer, useState } from "react";
import {
    SafeAreaView,
    StyleSheet,
    FlatList,
    NativeModules,
    Platform,
    NativeEventEmitter, RefreshControl, View,
} from "react-native";
import MembershipCard from "../Components/MembershipCard";
import spacing from "../../Constants/Spacing";
import SeparatorView from "../../CustomComponent/separatorview";
import colors from "../../Constants/colors";
import ActivateVoucherView from "../Components/ActivateVoucherView";
import {
    BUY_PLAN,
    BUY_PLAN_FOR_MEMBERSHIP,
    LoginForScreen,
    NumberVerifyAlerts, TabIndex,
    VERIFY_NUMBER_FOR_MEMBERSHIP, CLAIM_NOT_ALLOWED, SOMETHING_WRONG, PLATFORM_OS,
} from "../../Constants/AppConstants";
import { AppForAppFlowStrings } from "../../AppForAll/Constant/AppForAllConstants";
import EmptyMembershipOrNonLoggedInView from "../Components/EmptyMembershipOrNonLoggedInView";
import { RootContext } from "../../TabBar/index.tabbar";
import {APIData} from '../../../index';
import Loader from "../../Components/Loader";
import * as ActionTypes from "../../Constants/ActionTypes";
import {
    checkMembershipEligibility, checkSubscriptionStatus,
    getBuybackStatus,
    getDocsToUpload, getMemberShipDetail, getMembershipServiceRequests,
    getSRDraftData,
    getTempCustomerInfo, initialMembershipApiCalls, membershipRenewalRequest,
} from "../../network/APIHelper";
import MembershipTabReducer, { initialState } from "../Helpers/MembershipTabReducer";
import {
    getCheckEligibilityReqParams,
    getHARenewalReqParams, getNoNHARenewalReqParams,
    getParamsForMembershipDetails, prepareMembershipsForUI,
} from "../Helpers/MembershipTabUtils";
import {
    handleMembershipDeeplink,
} from "../Helpers/MembershipButtonActions";
import { trackAppScreen } from "../../commonUtil/WebengageTrackingUtils";

const EVENT_MEMBERSHIP_DATA = "getMembershipDataFromHome";
const EVENT_REFRESH_MEM_DRAFT_SR_STATUS = "RefreshMemDraftSRStatus";
const EVENT_REFRESH_MEMBERSHIPS = "RefreshMembershipsOnTab";
const EVENT_REFRESH_BUYBACK_STATUS = "RefreshMemBuybackStatus";

const nativeBridgeRef = NativeModules.ChatBridge;
const eventEmitter = new NativeEventEmitter(nativeBridgeRef);

const MembershipTabScreen = props => {
    const {
        customer_id,
        mobile_number,
        modelName,
        RAM,
        ROM,
        deviceIdentifier,
    } = APIData ?? {};
    const { Alert, deeplinkPath, setDeeplink } = useContext(RootContext);
    const [reducerState, dispatch] = useReducer(MembershipTabReducer, initialState);
    const [isUserLoggedIn, setIsUserLoggedIn] = useState(!!Number(customer_id ?? 0));
    const [isLoading, setIsLoading] = useState(false);
    const [displayMemList, setDisplayMemList] = useState([]);

    const reducerStateRef = useRef(reducerState);

    useEffect(() => {
        reducerStateRef.current = reducerState;
        if (reducerStateRef.current.allMembershipList.length && deeplinkPath?.length) {
            handleMembershipDeeplink(deeplinkPath, getExtraProps());
            setDeeplink(null)
        }
    }, [reducerState]);

    useEffect(() => {
        eventEmitter.addListener(EVENT_MEMBERSHIP_DATA, params => {
            setIsLoading(true);
            dispatch({
                type: ActionTypes.MEMBERSHIP_TAB_ACTION_TYPES.MEMBERSHIP_DATA_FROM_HOME,
                data: params,
                isInitiallyMemSet: true,
            });
        });

        /*refresh SR status of all memberships*/
        eventEmitter.addListener(EVENT_REFRESH_MEM_DRAFT_SR_STATUS, params => {
            if (reducerStateRef?.current?.membershipIdsForIntimationApi?.length) {
                setIsLoading(true);
                getSRDraftData(reducerStateRef?.current?.membershipIdsForIntimationApi, (response, error) => {
                    setIsLoading(false);
                    dispatch({
                        type: ActionTypes.MEMBERSHIP_TAB_ACTION_TYPES.MEMBERSHIP_CARDS_WITH_DRAFT_SR_DATA,
                        data: response,
                    });
                });
            }
        });

        /*refresh buyback status of all memberships*/
        eventEmitter.addListener(EVENT_REFRESH_BUYBACK_STATUS, params => {
            setIsLoading(true);
            getBuyBackReqData(() => {
                setIsLoading(false);
                dispatch({
                    type: ActionTypes.MEMBERSHIP_TAB_ACTION_TYPES.MEMBERSHIP_CARDS_WITH_BUYBACK_DATA,
                    data: response,
                });
            });
        });

        /* it will listen to refresh memberships tab from native code */
        eventEmitter.addListener(EVENT_REFRESH_MEMBERSHIPS, params => {
            handleRefreshControl();
        });

        return () => {
            if (Platform.OS === "ios") {
                eventEmitter.removeListener(EVENT_MEMBERSHIP_DATA, () => {
                });
                eventEmitter.removeListener(EVENT_REFRESH_MEM_DRAFT_SR_STATUS, () => {
                });
                eventEmitter.removeListener(EVENT_REFRESH_BUYBACK_STATUS, () => {
                });
                eventEmitter.removeListener(EVENT_REFRESH_MEMBERSHIPS, () => {
                });
            }
        };
    }, []);

    useEffect(() => {
        if (reducerStateRef.current.allMembershipList.length && deeplinkPath?.length) {
            handleMembershipDeeplink(deeplinkPath, getExtraProps());
            setDeeplink(null)
        }
    }, [deeplinkPath]);

    useEffect(() => {
        if (reducerState?.isCallInitApi && isUserLoggedIn) {
            setIsLoading(true);
            let buyBackApiRequestParams = null;
            if (reducerState?.allMembershipList?.length > 0) {
                buyBackApiRequestParams = {
                    modelName: modelName || "",
                    customerId: customer_id,
                    ram: RAM || "",
                    storage: ROM || "",
                    deviceIdentifier: deviceIdentifier || "",
                };
            }
            initialMembershipApiCalls(reducerState.membershipIdsForIntimationApi,
                buyBackApiRequestParams,
                reducerState?.peMobileMembershipActCode, reducerState?.idfenceMembershipsUUIDs, (response, error) => {
                    dispatch({
                        type: ActionTypes.MEMBERSHIP_TAB_ACTION_TYPES.MEMBERSHIP_INITIAL_API_CALLS,
                        data: response,
                    });
                });
        } else {
            setDisplayMemList(reducerState?.membershipViewModel);
            setIsLoading(reducerState?.isCallInitApi);
        }

    }, [reducerState?.isCallInitApi]);

    function getBuyBackReqData(callback = null) {
        let buyBackApiRequestParams = {
            modelName: modelName || "",
            customerId: customer_id,
            ram: RAM || "",
            storage: ROM || "",
            deviceIdentifier: deviceIdentifier || "",
        };
        getBuybackStatus(buyBackApiRequestParams, (response, error) => {
            if (callback) {
                callback();
            }
            dispatch({
                type: ActionTypes.MEMBERSHIP_TAB_ACTION_TYPES.MEMBERSHIP_GET_BUYBACK_STATUS_DATA,
                data: response,
            });
        });
    }

    useEffect(() => {
        setIsUserLoggedIn(!!Number(customer_id ?? 0));
            if (!!Number(customer_id ?? 0)) {
                handleRefreshControl();
            }

    }, [customer_id]);

    useEffect(() => {
        if (Array.isArray(reducerState.membershipViewModel), reducerState.membershipViewModel.length > 0) {
            setDisplayMemList(reducerState.membershipViewModel);
            nativeBridgeRef.refreshOnGoingSRScreen({ memberships: reducerState?.allMembershipList });
        }
    }, [reducerState.membershipViewModel]);

    const showAPIError = (error) => {
        showAlert("Alert", error?.message ?? SOMETHING_WRONG, {
            text: "Ok", onClick: () => {
            },
        }, null, null);
    };

    // this method would check claim file status of a membership
    const checkMembershipClaimFileStatus = (membershipId, callback) => {
        setIsLoading(true);
        getMemberShipDetail(getParamsForMembershipDetails(mobile_number, membershipId), (response, error) => {
            setIsLoading(false);
            if (error) {
                showAPIError(error);
                return;
            }
            let filteredMembership = (response?.data?.memberships ?? []).filter((responseMembership) => responseMembership?.memId === membershipId).first();
            callback(filteredMembership?.enableFileClaim);
        });
    };

    const checkMembershipSubscriptionStatus = (membershipUUID, callback) => {
        setIsLoading(true);
        checkSubscriptionStatus(membershipUUID, (response, error) => {
            setIsLoading(false);
            if (error) {
                showAPIError(error);
                return;
            }
            callback(response);
        });
    };

    const checkAvailabilityForPendingSubmission = (membershipID, callback) => {
        setIsLoading(true);
        checkMembershipEligibility(getCheckEligibilityReqParams(membershipID, ""), (response, error) => {
            setIsLoading(false);
            if (error) {
                showAPIError(error);
                return;
            }
            callback(response);
        });
    };

    const getTempCustomer = (activationCode, callback) => {
        setIsLoading(true);
        getTempCustomerInfo([activationCode], (arrayOfResponses, error) => {
            setIsLoading(false);
            if (error) {
                showAPIError(error);
            } else if (Array.isArray(arrayOfResponses) && arrayOfResponses?.length > 0) {
                callback(arrayOfResponses.first());
            }
        });
    };

    const serviceRequestFor = (params, callback) => {
        setIsLoading(true);
        getMembershipServiceRequests(params, (response, error) => {
            setIsLoading(false);
            if (error) {
                showAPIError(error);
            } else {
                callback(response);
            }
        });
    };

    const renewalAPICall = (membership, isNoNHAMembership, callback) => {
        setIsLoading(true);
        let requestParams = isNoNHAMembership ? getNoNHARenewalReqParams(membership, reducerState?.customerData) : getHARenewalReqParams(membership, reducerState?.customerData);
        membershipRenewalRequest(requestParams, (response, error) => {
            setIsLoading(false);
            if (error) {
                showAPIError(error);
                return;
            }
            callback(response);
        });
    };

    const showAlert = (title, alertMessage, primaryButton, secondaryButton, checkBox) => {
        Alert.showAlert(title, alertMessage, primaryButton, secondaryButton, checkBox);
    };
    const onActivateVoucher = () => {
        nativeBridgeRef.showPopupForVerifyNumber(NumberVerifyAlerts.activateVoucher.title, "", LoginForScreen.MEMBERSHIP_TAB, () => {
            nativeBridgeRef.goToActivateVoucher(AppForAppFlowStrings.GOTO_ACTIVATE_VOUCHER_FROM);
        });
    };

    const navigateToLoginScreen = () => {
        if (Platform.OS === PLATFORM_OS.ios) {
            nativeBridgeRef.openLoginWithCallback(status => {
            });
        } else {
            nativeBridgeRef.openLoginWithCallbackForMemTab(status => {
                if (status) {
                    nativeBridgeRef.refreshTabBarMemberships();
                }
            });
        }

    };

    const navigateToBuyTab = () => {
        nativeBridgeRef.openTabBarWithIndex(TabIndex.buy);
    };

    const getExtraProps = () => {
        return {
            handleMembershipRefresh: handleRefreshControl,
            checkMembershipClaimFileStatus: checkMembershipClaimFileStatus,
            checkMembershipSubscriptionStatus: checkMembershipSubscriptionStatus,
            checkAvailabilityForPendingSubmission: checkAvailabilityForPendingSubmission,
            serviceRequestFor: serviceRequestFor,
            renewalAPICall: renewalAPICall,
            showAlert: showAlert,
            allMemberships: reducerStateRef.current.allMembershipList,
            buybackResponse: reducerState?.buybackReqData,
            getTempCustomer: getTempCustomer,
        };
    };

    const _renderComponent = ({ item,index }) => {
        return <MembershipCard key={index.toString()}
            data={item}
            extraProps={getExtraProps()}
        />;
    };

    const handleRefreshControl = () => {
        setIsLoading(true);
        nativeBridgeRef.refreshTabBarMemberships();
    };
    return (
        <SafeAreaView style={styles.screen}>

            <ActivateVoucherView onActivateVoucher={onActivateVoucher} />

            <View style={{ flex: 1, marginTop: spacing.spacing8 }}>

                {displayMemList.length
                    ? <FlatList
                        bounces={Platform.OS === "ios"}
                        style={{ flex: 1, marginBottom: (Platform.OS === PLATFORM_OS.android ? spacing.spacing10 : 0) }}
                        refreshControl={<RefreshControl refreshing={false} onRefresh={handleRefreshControl} />}
                        contentContainerStyle={{ paddingVertical: spacing.spacing16 }}
                        // data={[getIDFenceCardViewModel(MEMBERSHIP_RAW_RESPONSE[2])]}
                        data={displayMemList}
                        renderItem={_renderComponent}
                        showsVerticalScrollIndicator={false}
                        bounces={true}
                        keyExtractor={(item, index) => {
                            return index.toString();
                        }}
                        ItemSeparatorComponent={() => <SeparatorView separatorStyle={styles.cardSeparator} />}
                        scrollEventThrottle={16}
                    />
                    : (!isLoading
                        ? <EmptyMembershipOrNonLoggedInView
                            message={isUserLoggedIn ? BUY_PLAN_FOR_MEMBERSHIP : VERIFY_NUMBER_FOR_MEMBERSHIP}
                            buttonText={isUserLoggedIn ? BUY_PLAN : NumberVerifyAlerts.buttonText}
                            primaryButtonAction={isUserLoggedIn ? navigateToBuyTab : navigateToLoginScreen}
                        />
                        : null)}


                {(isLoading)
                    ? <Loader isLoading={isLoading} loaderStyle={{
                        backgroundColor: reducerState?.isCallInitApi ? colors.color_F3F2F7 : colors.transparent,
                        position: "absolute",
                        top: 0,
                        width: "100%",
                    }} />
                    : null}

            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: colors.color_F3F2F7,
    },
    cardSeparator: {
        height: 10,
        marginVertical: 4,
        backgroundColor: colors.transparent,
    },
});

export default MembershipTabScreen;
