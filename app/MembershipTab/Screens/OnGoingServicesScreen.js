import React, { useContext, useEffect, useReducer, useRef, useState } from "react";
import {
    SafeAreaView,
    Text,
    StyleSheet,
    View,
    Platform,
    TouchableOpacity,
    Image,
    NativeModules, FlatList, NativeEventEmitter, RefreshControl,
} from "react-native";
import { SERVICE_REQUESTS, SOMETHING_WRONG } from "../../Constants/AppConstants";
import BackButton from "../../CommonComponents/NavigationBackButton";
import { onBackPress } from "../../BuyPlan/Helpers/BuyPlanUtils";
import { TextStyle } from "../../Constants/CommonStyle";
import colors from "../../Constants/colors";
import images from "../../images/index.image";
import * as WebEngageEventsName from "../../Constants/WebengageEvents";
import * as WebEngageKeys from "../../Constants/WebengageAttrKeys";
import { logWebEnageEvent, trackAppScreen } from "../../commonUtil/WebengageTrackingUtils";
import spacing from "../../Constants/Spacing";
import { DUMMY_CARDS, SERVICE_CARDS } from "../DummyData";
import SeparatorView from "../../CustomComponent/separatorview";
import ServiceRequestCard from "../Components/ServiceRequestCard";
import MembershipTabReducer, { initialState } from "../Helpers/MembershipTabReducer";
import * as ActionTypes from "../../Constants/ActionTypes";
import { checkMembershipEligibility } from "../../network/APIHelper";
import { getCheckEligibilityReqParams, prepareCardsForONGoingSRs } from "../Helpers/MembershipTabUtils";
import { CommonContext } from "../../CommonComponents/Index/index.common";
import { handleMembershipBtnActions, MembershipButtonActions } from "../Helpers/MembershipButtonActions";
import Loader from "../../Components/Loader";
import { parseJSON } from "../../commonUtil/AppUtils";
import { useBackButton } from "../../BuyPlan/Helpers/CustomHooks";

const nativeBridgeRef = NativeModules.ChatBridge;
const eventEmitter = new NativeEventEmitter(nativeBridgeRef);

const EVENT_REFRESH_ONGOING_SR = "RefreshOnGoingSRScreen";

const OnGoingServicesScreen = props => {

    const selectedMembership = props.navigation.getParam("selectedMembership") ?? {};
    const membershipServices = selectedMembership?.srResponseData ?? [];
    const { Alert } = useContext(CommonContext);
    const [reducerState, dispatch] = useReducer(MembershipTabReducer, initialState);
    const [isLoading, setIsLoading] = useState(false);

    useBackButton(() => {
        onBackPress(props.navigation);
    });

    useEffect(() => {
        prepareCardsForONGoingSRs(selectedMembership, membershipServices, (cards) => {
            dispatch({
                type: ActionTypes.MEMBERSHIP_TAB_ACTION_TYPES.ONGOING_SR_PREPARE_CARDS,
                data: { cards: cards },
            });
        });

        //when user pull to refresh on current screen and got membership response from membership tab screen
        eventEmitter.addListener(EVENT_REFRESH_ONGOING_SR, async allMembershipsJSON => {
            setIsLoading(false);
            let allMemberships = parseJSON(allMembershipsJSON?.memberships);
            if (Array.isArray(allMemberships)) {
                let membership = allMemberships?.filter(membership => membership.memId == selectedMembership?.memId).first();
                prepareCardsForONGoingSRs(membership, membership?.srResponseData, (cards) => {
                    dispatch({
                        type: ActionTypes.MEMBERSHIP_TAB_ACTION_TYPES.ONGOING_SR_PREPARE_CARDS,
                        data: { cards: cards },
                    });
                });
            }
        });

        return () => {
            if (Platform.OS === "ios") {
                eventEmitter.removeListener(EVENT_REFRESH_ONGOING_SR, () => {
                });
            }
        };
    }, []);

    const showAlert = (title, alertMessage, primaryButton, secondaryButton, checkBox) => {
        Alert.showAlert(title, alertMessage, primaryButton, secondaryButton, checkBox);
    };

    const showAPIError = (error) => {
        showAlert("Alert", error?.message ?? SOMETHING_WRONG, {
            text: "Ok", onClick: () => {
            },
        }, null, null);
    };

    const checkAvailabilityForPendingSubmission = (membershipID, callback) => {
        setIsLoading(true);
        checkMembershipEligibility(getCheckEligibilityReqParams(membershipID, null), (response, error) => {
            setIsLoading(false);
            if (error) {
                showAPIError(error);
                return;
            }
            callback(response);
        });
    };

    const _renderComponent = ({ item }) => {
        return <ServiceRequestCard data={item} extraProps={{
            checkAvailabilityForPendingSubmission: checkAvailabilityForPendingSubmission,
            navigation: props?.navigation,
            showAlert: showAlert,
        }} />;
    };

    const handleRefreshControl = () => {
        setIsLoading(true);
        nativeBridgeRef.refreshTabBarMemberships();
    };

    return (
        <SafeAreaView style={{ flex: 1 }}>
            {(reducerState?.srListVisibleCards ?? []).length > 0
                ? <FlatList
                    bounces={Platform.OS === "ios"}
                    refreshControl={<RefreshControl refreshing={false} onRefresh={handleRefreshControl} />}
                    contentContainerStyle={{ paddingVertical: spacing.spacing16 }}
                    data={(reducerState?.srListVisibleCards ?? [])}
                    renderItem={_renderComponent}
                    showsVerticalScrollIndicator={false}
                    bounces={true}
                    keyExtractor={(item, index) => {
                        return index.toString();
                    }}
                    ItemSeparatorComponent={() => <SeparatorView separatorStyle={styles.cardSeparator} />}
                    scrollEventThrottle={16}
                />
                : null
            }

            {(isLoading)
                ? <Loader isLoading={isLoading} loaderStyle={{
                    backgroundColor: "transparent",
                    position: "absolute",
                    top: 0,
                    width: "100%",
                }} />
                : null}

        </SafeAreaView>
    );
};

OnGoingServicesScreen.navigationOptions = (navigationData) => {
    return {
        headerStyle: { borderBottomWidth: 0, elevation: 6 },
        headerTitle: <View style={Platform.OS === "ios" ? { alignItems: "center" } : { alignItems: "flex-start" }}>
            <Text style={TextStyle.text_16_bold}>{SERVICE_REQUESTS}</Text>
        </View>,
        headerLeft: <BackButton onPress={() => {
            onBackPress(navigationData.navigation);
        }} />,
        headerTitleStyle: { color: colors.white },
        headerTintColor: colors.white,
        headerRight: <TouchableOpacity style={{ marginTop: 0, flexDirection: "row", marginRight: 16 }}
                                       onPress={() => {
                                           let eventData = {};
                                           eventData[WebEngageKeys.LOCATION] = "Membership Detail Screen";
                                           logWebEnageEvent(WebEngageEventsName.CHAT, eventData);
                                           logWebEnageEvent(WebEngageEventsName.CHAT_ON_MEMBERSHIP_DETAIL);
                                           trackAppScreen(WebEngageEventsName.CHAT);
                                           nativeBridgeRef.openChat();
                                       }}>
            <Image style={styles.chatIconStyle} source={images.chatIconBlue} />
        </TouchableOpacity>,
    };
};

const styles = StyleSheet.create({
    chatIconStyle: {
        width: 24,
        height: 24,
        marginTop: 4,
        resizeMode: "contain",
    },
    cardSeparator: {
        height: 10,
        marginVertical: 4,
        backgroundColor: colors.transparent,
    },

});

export default OnGoingServicesScreen;
