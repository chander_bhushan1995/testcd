import {applyMiddleware, createStore} from 'redux';

/*
*
 * @param reducer A function that returns the next state tree, given the
 *   current state tree and the action to handle.
 *
 * @param [middleware] The initial state. You may optionally specify it to
 *   hydrate the state from the server in universal apps, or to restore a
 *   previously serialized user session. If you use `combineReducers` to
 *   produce the root reducer function, this must be an object with the same
 *   shape as `combineReducers` keys.
 *
 * @returns A Redux store that lets you read the state, dispatch actions and
 *   subscribe to changes.
* */
export const configStore = (appReducer, middleware) => {
    return createStore(appReducer, applyMiddleware(middleware));
};
