import React from 'react';
import {IndexSetup} from './IndexSetup';
import AppNavigator from '../idfence/navigation/NaviagtionStack';
import dashboardReducer from "../idfence/dashboard/DashboardReducer";
import assetsTabReducer from '../idfence/dashboard/screens/assetsTab/AssetsTabReducer';
import overviewTabReducer from "../idfence/dashboard/screens/overviewTab/OverviewTabReducer"
import alertsTabReducer from "../idfence/dashboard/screens/alertTab/AlertsTab.Reducer"
import detailsTabReducer from "../idfence/dashboard/screens/detailsTab/DetailsTab.Reducer";
import activateCreditScoreComponentReducer from "../idfence/dashboard/screens/overviewTab/activateCreditScore/ActivateCreditScoreComponent.Reducer";
import creditScoreDashboardReducer from "../idfence/creditscore/creditscoredashboard/CreditScoreDashboard.Reducer";
import csPaymentHistoryReducer from "../idfence/creditscore/creditscoredashboard/paymenthistory/CSPaymentHistory.Reducer";
import csCreditUtilisationReducer from "../idfence/creditscore/creditscoredashboard/creditutilisation/CSCreditUtilisation.Reducer";
import csOldestOpenAccountReducer from "../idfence/creditscore/creditscoredashboard/oldestopenaccount/CSOldestOpenAccount.Reducer";
import csCardsAndAccountsReducer from "../idfence/creditscore/creditscoredashboard/cardsandaccounts/CSCardsAndAccounts.Reducer";
import csCreditEnquiriesReducer from "../idfence/creditscore/creditscoredashboard/creditenquiries/CreditEnquiries.Reducer";
import csMonitorAssetsReducer from "../idfence/creditscore/creditscoredashboard/monitorassets/MonitorAssets.Reducer";
import codePush from "react-native-code-push";
import {setAPIData, updateAPIData} from "../../index";

const appReducer = {
    dashboardReducer,
    overviewTabReducer,
    detailsTabReducer,
    alertsTabReducer,
    assetsTabReducer,
    activateCreditScoreComponentReducer,
    creditScoreDashboardReducer,
    csPaymentHistoryReducer,
    csCreditUtilisationReducer,
    csOldestOpenAccountReducer,
    csCardsAndAccountsReducer,
    csCreditEnquiriesReducer,
    csMonitorAssetsReducer
};

class Root extends React.Component {
    indexSetup: IndexSetup;

    constructor(props) {
        super(props);
        setAPIData(props);
        const setupParams = {
            componentName: 'DashboardComponent',
            appReducer: appReducer,
            appNavigator: AppNavigator,
        };
        this.indexSetup = new IndexSetup(setupParams);
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props !== nextProps) {
            updateAPIData(nextProps);
        }

        return true
    }

    render() {

        const initialProsParams = {isInitialPropsRequired: true, initialPropsData: this.props};
        return this.indexSetup.getRootViewRenderItem(initialProsParams);
    }

}
export default codePush(Root);