import {configStore} from "./ConfigStore";
import middleware from "./RootMiddleware";
import oaCombineReducer from "./RootCombineReducers";
import {NavigationReducer} from "./NavigationReducer";
import {SET_INITIAL_PROPS} from "./NavigationConstant";
import {Platform} from "react-native";
import {initialProps} from "./InitialPropsReducer";

import {Provider} from "react-redux";
import React from "react";

/*
* @IndexSetup is class which create the setup the Navigation & provide function to create the root component
* */
export class IndexSetup {
    componentName;
    appReducer;
    navigationReducer;

    /*
     *   @IndexSetup is class which create the setup the Navigation & provide function to create the root component
     *   @Params componentName: To load the component which will visible at screen.
     *           appReducer: List of all reducer which will act in the flow.
     *           AppNavigator: The navigation stack
     * */
    constructor(setUpParams) {
        this.componentName = setUpParams?.componentName;
        this.appReducer = setUpParams?.appReducer;
        this.navigationReducer = new NavigationReducer(setUpParams?.componentName, setUpParams?.appNavigator);
    }

    /*
    *  @middleware function is use for inject the component and its state into redux navigation
    * @Param: componentName : which you want to inject.Here we are inject our very first component.
    * */
    middleware = () => middleware(this.componentName);
    /*
   *  @param reducers An object whose values correspond to different reducer
 *   functions that need to be combined into one. One handy way to obtain it
 *   is to use ES6 `import * as reducers` syntax. The reducers may never
 *   return undefined for any action. Instead, they should return their
 *   initial state if the state passed to them was undefined, and the current
 *   state for any unrecognized action.
 *
 * @returns A reducer function that invokes every reducer inside the passed
 *   object, and builds a state object with the same shape.
   * */
    combineReducer = () => {
        const commonReducer = {nav: this.navigationReducer.navReducer, appData: initialProps,};
        return oaCombineReducer(this.appReducer, commonReducer)
    };
    /*
    *
    * @configStore is function which is responsible to create store
    * @Params: reducers list : which is used to bind the data from particular reducer
    *
    * @return a store object.
    * */
    store = () => configStore(this.combineReducer(), this.middleware());

    /*
    *   AppWithNavigationState is responsible to provide ui container with state and data for root component
    * */
    AppWithNavigationState = () => this.navigationReducer.AppWithNavigationState();

    /*
     *  Here we get the set initial props there is mandate to set the data from initial props after store setup
     * */
    setInitialPropsIntoStore = (initialPropsData,store) => store.dispatch({
        type: SET_INITIAL_PROPS,
        params: initialPropsData
    });

    /*
    *  @getRootViewRender is function which return a container for root view
    *
    * */

    getRootViewRenderItem=(params)=>{
        /*
       *  Here we configure the store and @return store object
       * */
        const store = this.store();
        /*
        *  Here we get the set initial props there is mandate to set the data from initial props after store setup
        * */
        if(params?.isInitialPropsRequired)
        this.setInitialPropsIntoStore(params?.initialPropsData,store);
        /*
       *  Here we get view which we pass to setup
       * */
        const AppWithNavigationState = this.AppWithNavigationState();
        return (
            <Provider store={store}>
                <AppWithNavigationState/>
            </Provider>
        );
    }

}
