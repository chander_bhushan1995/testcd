import {SET_INITIAL_PROPS} from "./NavigationConstant";

export const initialProps = (state = {}, action) => {
    switch (action.type) {
        case SET_INITIAL_PROPS :
            return {...state,params:action.params};
    }
    return state;
};
