
import {NavigationActions} from "react-navigation";
import {reduxifyNavigator} from "react-navigation-redux-helpers";
import {connect} from "react-redux";
export class NavigationReducer {
    componentName;
    initialStateNavigation;
    AppNavigator;
    constructor(componentName,AppNavigator)
    {
        this.componentName=componentName;
        this.AppNavigator=AppNavigator;
        this.initialStateNavigation={
            ...AppNavigator.router.getStateForAction(
                AppNavigator.router.getActionForPathAndParams(componentName))
        };
    }

     navReducer = (state = this.initialStateNavigation, action) => {


        if (action.type === NavigationActions.SET_PARAMS) {
            try {
                const lastRoute = state.routes[0].routes.find(
                    route => route.routeName === action.params.routeName
                );
                action.key = lastRoute.key;
            } catch (e) { }
        }
        const nextState = this.AppNavigator.router.getStateForAction(action, state);
        // Simply return the original `state` if `nextState` is null or undefined.
        return nextState || state;
    };
     App = ()=>reduxifyNavigator(this.AppNavigator, this.componentName);
     mapStateToProps = state => ({
        state: state.nav
    });
    AppWithNavigationState = ()=>connect(this.mapStateToProps)(this.App());
}

