import {combineReducers} from "redux";

const oaCombineReducer = (reducers, commonReducer) => {
    return combineReducers({...reducers, ...commonReducer});
};

export default oaCombineReducer;
