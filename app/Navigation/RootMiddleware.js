import {createReactNavigationReduxMiddleware} from "react-navigation-redux-helpers";

const middleware = (componentName) => createReactNavigationReduxMiddleware(
    componentName,
    state => state.nav
);
export default middleware;
