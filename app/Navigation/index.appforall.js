import React from "react";
import {connect, Provider} from "react-redux";
import {createReactNavigationReduxMiddleware, reduxifyNavigator} from 'react-navigation-redux-helpers'
import AppNavigator from "./navigationStackAppforAll";
import {NavigationActions} from "react-navigation";
import {YellowBox} from 'react-native';
import {applyMiddleware, combineReducers, createStore} from 'redux';
import codePush from "react-native-code-push";
import {setAPIData, updateAPIData} from "../../index";


const initialState = AppNavigator.router.getStateForAction(AppNavigator.router.getActionForPathAndParams('Splash'));
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

const navReducer = (state = initialState, action) => {
    if (action.type === NavigationActions.SET_PARAMS) {
        try {
            const lastRoute = state.routes[0].routes.find(route => route.routeName === action.params.routeName);
            action.key = lastRoute.key;
        } catch (e) {
        }
    }
    const nextState = AppNavigator.router.getStateForAction(action, state);
    // Simply return the original `state` if `nextState` is null or undefined.
    return nextState || state;
};

const combineReducer = combineReducers({ nav: navReducer });

// Note: createReactNavigationReduxMiddleware must be run before reduxifyNavigator
const middleware = createReactNavigationReduxMiddleware("SplashScreen", state => state.nav,);
const App = reduxifyNavigator(AppNavigator, "SplashScreen");
const mapStateToProps = (state) => ({state: state.nav});
const AppWithNavigationState = connect(mapStateToProps)(App);

export const store = createStore(combineReducer, applyMiddleware(middleware));

class Root extends React.Component {
    constructor(props) {
        super(props);
        setAPIData(props);
        initialState.routes[0].routeName = "Splash"

        console.ignoredYellowBox = ['Warning:'];
        console.disableYellowBox = true;
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props !== nextProps) {
            updateAPIData(nextProps);
        }

        return true
    }

    render() {
        return (
            <Provider store={store}>
                <AppWithNavigationState />
            </Provider>
        );
    }
}

export default codePush(Root);
