import React from 'react';
import {connect, Provider} from 'react-redux';
import {createReactNavigationReduxMiddleware, reduxifyNavigator} from 'react-navigation-redux-helpers';
import PincodeReducer from '../Buyback/buybackReducers/PincodeReducers';
import QuestionAndAnswersReducer from '../Buyback/buybackReducers/QuestionAndAnswersReducer';
import EnterBankDetailsReducer from '../Buyback/buybackReducers/EnterBankDetailsReducer';
import FinalPriceSuccessReducer from '../Buyback/buybackReducers/FinalPriceSuccessReducer';
import AppNavigator from './navigationStackBuyback';
import {NavigationActions} from 'react-navigation';
import {NativeModules, View, YellowBox} from 'react-native';
import {applyMiddleware, combineReducers, createStore} from 'redux';
import BuybackRequestSuccessTimelineReducer from '../Buyback/buybackReducers/BuybackRequestSuccessTimelineReducer';
import BuybackMultipleServiceRequestReducer from '../Buyback/buybackReducers/BuybackMultipleServiceRequestReducer';
import MobilePickupScheduledSuccessReducer from '../Buyback/buybackReducers/MobilePickupScheduledSuccessReducer';
import BuySellSelectionReducer from '../Buyback/buybackReducers/BuySellSelectionReducer';
import {Platform} from 'react-native';
import BuybackManager from '../Buyback/buybackActions/BuybackManager';
import * as BuybackApiManager from '../Buyback/buybackActions/BuybackApiHelper';
import Loader from '../Components/Loader';
import DialogView from '../Components/DialogView';
import {decideFlow} from '../Buyback/BuyBackUtils/BuybackNavigationDecider';
import {ABB_DIALOG_TEXT} from '../Constants/BuybackConstants';
import codePush from "react-native-code-push";
import * as Store from '../commonUtil/Store'
import { parseJSON } from "../commonUtil/AppUtils";
import {setAPIData, updateAPIData} from "../../index";

export const BuybackContext = React.createContext();

const nativeBridgeRef = NativeModules.ChatBridge;
const initialState = AppNavigator.router.getStateForAction(AppNavigator.router.getActionForPathAndParams('BuybackComponent'));
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
const navReducer = (state = initialState, action) => {

    if (action.type === NavigationActions.SET_PARAMS) {
        try {
            const lastRoute = state.routes[0].routes.find(route => route.routeName === action.params.routeName);
            action.key = lastRoute.key;
        } catch (e) {
        }
    }
    const nextState = AppNavigator.router.getStateForAction(action, state);
    // Simply return the original `state` if `nextState` is null or undefined.
    return nextState || state;
};
const appReducer = combineReducers({
    nav: navReducer,
    PincodeReducer,
    QuestionAndAnswersReducer,
    EnterBankDetailsReducer,
    FinalPriceSuccessReducer,
    BuybackRequestSuccessTimelineReducer,
    BuybackMultipleServiceRequestReducer,
    MobilePickupScheduledSuccessReducer,
    BuySellSelectionReducer,
});

// Note: createReactNavigationReduxMiddleware must be run before reduxifyNavigator
const middleware = createReactNavigationReduxMiddleware(
    'BuybackComponent',
    state => state.nav,
);

const App = reduxifyNavigator(AppNavigator, 'BuybackComponent');
const mapStateToProps = (state) => ({
    state: state.nav,
});

const AppWithNavigationState = connect(mapStateToProps)(App);

const store = createStore(
    appReducer,
    applyMiddleware(middleware),
);
export let buybackManagerObj = BuybackManager.getInstance();

class Root extends React.Component {
    constructor(props) {
        super(props);
        setAPIData(props);
        let buyBackStatusData;
        let deeplink = '';
        //TODO need to alter this if/else(can use writable map)
        if (Platform.OS === 'ios') {
            buyBackStatusData = props.buyback_status_list;
        } else {
            buyBackStatusData = parseJSON(props.buyback_status_list).buyback_status_list1;
        }
        deeplink = props.deeplink_url_string;
        if (buyBackStatusData) {
            buybackManagerObj.setBuybackStatusData(parseJSON(buyBackStatusData));
        }
        if (deeplink != null && deeplink.length > 0) {
            Store.setDeeplinkUrl(deeplink)
        }

        console.ignoredYellowBox = ['Warning:'];
        console.disableYellowBox = true;

        this.state = {
            reloadState: false,
            buybackdata: null,
            isLoading: false,
            abbDialogText: ABB_DIALOG_TEXT,
            isLoadNavigation: false,
        };
        this.updateState = this.updateState.bind(this);
        this.onFlowDecideCompletion = this.onFlowDecideCompletion.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props !== nextProps) {
            updateAPIData(nextProps);
        }

        return true
    }

    componentDidMount() {
        nativeBridgeRef.getBBQuestionsRemoteVersion(version => {
            let questionVersion = String(version);
            Store.getBuybackQuestionsVersion((error, value) => {
                if (version !== value) {
                    this.getQuestionsDataFromRemote();
                } else {
                    Store.getBuybackQuestionsData((error, value) => {
                        if (value !== null) {
                            buybackManagerObj.setQuestionsList(parseJSON(value));
							decideFlow(this.DialogViewRef, this.onFlowDecideCompletion);
                        } else {
                            this.getQuestionsDataFromRemote();
                        }
                    });
                }
                Store.setBuybackQuestionsVersion(version)
            });
        });
    }

    getQuestionsDataFromRemote() {
        this.setState({isLoading: true});
        BuybackApiManager.getQuestionsListFromRemote((response, error) => {
            if (response !== null) {
                buybackManagerObj.setQuestionsList(response);
                Store.setBuybackQuestionsData(JSON.stringify(response));
                this.setState({isLoading: false});
                decideFlow(this.DialogViewRef, this.onFlowDecideCompletion);
            } else {
                this.setState({isLoading: false});
                this.DialogViewRef.showDailog({
                    title: 'ALERT',
                    message: 'alertMessage',
                    primaryButton: {
                        text: 'Retry', onClick: () => {
                            this.getQuestionsDataFromRemote();
                        },
                    },
                    secondaryButton: {
                        text: 'Cancel', onClick: () => {
                        },
                    },
                    cancelable: false,
                    onClose: () => {
                    },
                });
            }
        });
    }

    updateState(objectToUpdate) {
        this.setState(objectToUpdate);
    }

    onFlowDecideCompletion(routeAndParams) {
        initialState.routes[0].routeName = routeAndParams.routeName;
        initialState.routes[0].params = {params: routeAndParams.params};
        this.setState({isLoadNavigation: true});
    }

    showAlert = (title, alertMessage, primaryButton = {
        text: 'Yes', onClick: () => {
        },
    }, secondaryButton, checkBox) => {
        this.DialogViewRef.showDailog({
            title: title,
            message: alertMessage,
            primaryButton: primaryButton,
            secondaryButton: secondaryButton,
            checkBox: checkBox,
            cancelable: true,
            onClose: () => {
            },
        });
    };

    render() {
        return (
            <Provider store={store}>
                <BuybackContext.Provider value={{
                    Alert: {showAlert: this.showAlert},
                }}>
                    {this.state.isLoadNavigation
                        ? <AppWithNavigationState/>
                        : <View>
                            <Loader isLoading={this.state.isLoading}/>
                        </View>
                    }
                    <DialogView ref={ref => (this.DialogViewRef = ref)}/>
                </BuybackContext.Provider>
            </Provider>
        );
    }
}

export default codePush(Root);
