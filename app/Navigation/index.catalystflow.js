import React from "react";
import {connect, Provider} from "react-redux";
import {createReactNavigationReduxMiddleware, reduxifyNavigator} from 'react-navigation-redux-helpers'
import CatalystReducer from '../Catalyst/reducers/CatalystReducer'
import AppNavigator from "../Catalyst/Navigation/NavigationStack";
import {NavigationActions} from "react-navigation";
import {YellowBox} from 'react-native';
import {applyMiddleware, combineReducers, createStore} from 'redux';
import codePush from "react-native-code-push";
import { parseJSON } from "../commonUtil/AppUtils";
import {setAPIData, updateAPIData} from "../../index";

// const AppNavigator = createStackNavigator(NavigationStack);
const initialState = AppNavigator.router.getStateForAction(AppNavigator.router.getActionForPathAndParams('CatalystScreen'));

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
console.ignoredYellowBox = ['Warning:'];
console.disableYellowBox = true;

const navReducer = (state = initialState, action) => {

    if (action.type === NavigationActions.SET_PARAMS) {
        try {
            const lastRoute = state.routes[0].routes.find(route => route.routeName === action.params.routeName);
            action.key = lastRoute.key;
        } catch (e) {
        }
    }
    const nextState = AppNavigator.router.getStateForAction(action, state);
    // Simply return the original `state` if `nextState` is null or undefined.
    return nextState || state;
};
const appReducer = combineReducers({
    nav: navReducer,
    CatalystReducer,
});

// Note: createReactNavigationReduxMiddleware must be run before reduxifyNavigator
const middleware = createReactNavigationReduxMiddleware(
    "CatalystScreen",
    state => state.nav,
);

const App = reduxifyNavigator(AppNavigator, "CatalystScreen");
const mapStateToProps = (state) => ({
    state: state.nav,
});

const AppWithNavigationState = connect(mapStateToProps)(App);

const store = createStore(
    appReducer,
    applyMiddleware(middleware),
);

class Root extends React.Component {
    constructor(props){
        super(props);
        setAPIData(props)
        initialState.initialProps = props;
        initialState.routes[0].params.catalystQueryParams = parseJSON(props.catalystQueryParams);
        initialState.routes[0].params.catalystDeeplinkURL = props.catalystDeeplinkURL
        initialState.routes[0].routeName = "CatalystScreen";
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props !== nextProps) {
            updateAPIData(nextProps);
        }

        return true
    }
    
    render() {
        return (
            <Provider store={store}>
                <AppWithNavigationState/>
            </Provider>
        );
    }
}
export default codePush(Root);
