import React from 'react';
import ChatReducer from '../Reducers/ChatReducer';
import {IndexSetup} from './IndexSetup';
import AppNavigator from './navigationStack';
import ChatManager from '../Chat/ChatManager';
import EventEmitter from 'events';
import codePush from "react-native-code-push";
import {setAPIData, updateAPIData} from "../../index";

const appReducer = {
    ChatReducer,
};

export const localEventEmitter = new EventEmitter();
export const chatManager = ChatManager.getInstance();

class Root extends React.Component {
    indexSetup: IndexSetup;

    constructor(props) {
        super(props);
        setAPIData(props);
        const setupParams = {
            componentName: 'Main',
            appReducer: appReducer,
            appNavigator: AppNavigator,
        };
        this.indexSetup = new IndexSetup(setupParams);
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props !== nextProps) {
            updateAPIData(nextProps);            
        }

        return true
    }

    render() {
        return this.indexSetup.getRootViewRenderItem(null);
    }

}
export default codePush(Root);
