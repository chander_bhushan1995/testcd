import React from 'react';
import {connect, Provider} from 'react-redux';
import {
    createReactNavigationReduxMiddleware,
    reduxifyNavigator,
} from 'react-navigation-redux-helpers';
import {YellowBox} from 'react-native';
import {applyMiddleware, combineReducers, createStore} from 'redux';
import {NavigationActions} from 'react-navigation';
import AppNavigator from '../PaymentFlow/Navigation/NavigationStack';
import paymentSuccessStepOneReducer from '../PaymentFlow/Reducers/PaymentSuccessStepOneReducer';
import basicDetailsReducer from '../PaymentFlow/Reducers/BasicDetailsReducer';
import createCustomerReducer from '../PaymentFlow/Reducers/CreateCustomerReducer';
import paymentSuccessReducer from '../PaymentFlow/Reducers/PaymentSuccessReducer';
import membershipActivationReducer from '../PaymentFlow/Reducers/MembershipActivationReducer';
import applianceDetailsReducer from '../PaymentFlow/Reducers/ApplianceDetailsReducer';
import * as FirebaseKeys from '../Constants/FirebaseConstants';
import codePush from "react-native-code-push";
import * as Store from '../commonUtil/Store'
import {APIData, setAPIData, updateAPIData} from "../../index";
import EventEmitter from "events";
export const localEventEmitter = new EventEmitter();

const initialState = {
    ...AppNavigator.router.getStateForAction(
        AppNavigator.router.getActionForPathAndParams('CreateCustomerComponent')),
};

YellowBox.ignoreWarnings([
    'Warning: isMounted(...) is deprecated',
    'Module RCTImageLoader',
]);

console.ignoredYellowBox = ['Warning:'];
console.disableYellowBox = true;

const navReducer = (state = initialState, action) => {
    if (action.type == 'Navigation/COMPLETE_TRANSITION') {
        _storeData(state);
    }

    if (action.type === NavigationActions.SET_PARAMS) {
        try {
            const lastRoute = state.routes[0].routes.find(
                route => route.routeName === action.params.routeName,
            );
            action.key = lastRoute.key;
        } catch (e) {
        }
    }
    const nextState = AppNavigator.router.getStateForAction(action, state);
    // Simply return the original `state` if `nextState` is null or undefined.
    return nextState || state;
};
const appReducer = combineReducers({
    nav: navReducer,
    paymentSuccessStepOneReducer,
    createCustomerReducer,
    paymentSuccessReducer,
    basicDetailsReducer,
    membershipActivationReducer,
    applianceDetailsReducer,
});
const middleware = createReactNavigationReduxMiddleware(
    'CreateCustomerComponent',
    state => state.nav,
);
const App = reduxifyNavigator(AppNavigator, 'CreateCustomerComponent');
const mapStateToProps = state => ({
    state: state.nav,
});

const AppWithNavigationState = connect(mapStateToProps)(App);

const store = createStore(appReducer, applyMiddleware(middleware));

async function _storeData(state) {
    await Store.setValueForKeyInAsyn('canExit', state.routes.length.toString());
}

class PaymentFlow extends React.Component {
    constructor(props) {
        super(props);
        setAPIData(props)
        initialState.initialProps = props;
        let routeName = 'CreateCustomerComponent';
        initialState.routes[0].params.isFromMemTab = false;
        if (props.postboarding_screen_act_code !== null && props.postboarding_screen_act_code !== undefined) {
            initialState.routes[0].params.isFromMemTab = true;
            if (props.service_Type === 'HS') {
                routeName = 'MembershipActivationComponent';
            } else {
                routeName = 'BasicDetailsComponent';
            }
        } else if (props.isRenewal === true) {
            initialState.routes[0].params.isFromMemTab = true;
            initialState.routes[0].params.initialProps = props;
            routeName = 'PaymentWebViewComponent';
        } else if (props.isPendingPayment) {
            routeName = 'ServicePaymentPendingComponent';
            initialState.routes[0].params.initialProps = props;
        } else if (props.service_Type === FirebaseKeys.SOD) {
            if (props?.isRescheduleSlot) {
                routeName = 'ScheduleVisitComponent';
                initialState.routes[0].params.initialProps = props;
                initialState.routes[0].params.isPaymentFlow = !props?.isRescheduleSlot;
                initialState.routes[0].params.userDetails = {
                    firstName: props?.user_Name,
                    emailId: props?.user_Email,
                    mobileNumber: props?.mobile_number,
                };
            }
        }
      //  TODO: ENABLE THIS AFTER EDIT functionality
        if(routeName==='CreateCustomerComponent' && props.service_Type!=='IDFence' && props.service_Type!==FirebaseKeys.SOD && !props.hasEW)
        {
            if(props?.mobile_number && props?.user_Email && props?.user_Name)
            {
                initialState.routes[0].params.initialProps = props;
                initialState.routes[0].params.shouldCallOnBoardApi = true;
                initialState.routes[0].params.isSkipCustomerDetail = true;
                initialState.routes[0].params.shouldEditCustomerDetails = true;
                routeName = 'PaymentWebViewComponent';
            }
        }
        initialState.routes[0].routeName = routeName;
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props !== nextProps) {
            updateAPIData(nextProps);
        }

        return true
    }

    render() {
        return (
            <Provider store={store}>
                <AppWithNavigationState/>
            </Provider>
        );
    }
}
export default codePush(PaymentFlow);
