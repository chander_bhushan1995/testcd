import React from 'react';
import {connect, Provider} from 'react-redux';
import {YellowBox} from 'react-native';
import {applyMiddleware, combineReducers, createStore} from 'redux';
import QuestionsGenericComponent from '../AppForAll/MyAccount/Actions/QuestionsGenericAction';
import AppNavigator from './navigationStackUserRegistration';
import {NavigationActions} from 'react-navigation';
import {createReactNavigationReduxMiddleware, reduxifyNavigator} from 'react-navigation-redux-helpers';
import * as Actions from '../Constants/ActionTypes';
import QuestionAnswerResponseParser from '../AppForAll/MyAccount/Components/QuestionAnswerResponseParser';
import codePush from "react-native-code-push";
import {setAPIData, updateAPIData} from "../../index";


const INITIAL_PROPS = 'initial_props';
const initialState = {
    ...AppNavigator.router.getStateForAction(
        AppNavigator.router.getActionForPathAndParams('QuestionsGenericScreen')),
};

YellowBox.ignoreWarnings([
    'Warning: isMounted(...) is deprecated',
    'Module RCTImageLoader',
]);

console.ignoredYellowBox = ['Warning:'];
console.disableYellowBox = true;

const appInitialState = {
    index: 0,
    isDataChanged: false,
    currentScreenNo: 0,
    totalScreens: 0,
    questionAnswer: [],
    flowType: "CompleteProfile",
    isLoading: false,
    type: "",
    isPopulated: false,
    dataSourceQuestionsScreen: [], // It will be array of arrays
    currentOptionList: [],
    currentBrand: "",
    currentModel: "",
    currentName: "",
    currentEmail: "",
    errorMessage: "",
};

const navReducer = (state = initialState, action) => {

    if (action.type === NavigationActions.SET_PARAMS) {
        try {
            const lastRoute = state.routes[0].routes.find(route => route.routeName === action.params.routeName);
            action.key = lastRoute.key;
        } catch (e) {
        }
    }
    const nextState = AppNavigator.router.getStateForAction(action, state);
    // Simply return the original `state` if `nextState` is null or undefined.
    return nextState || state;
};

// This is a reducer
const appData = (state = appInitialState, action) => {
    switch (action.type) {
        case INITIAL_PROPS:
            return {
                ...state,
                params: action.initialProps,
                isDataChanged: false,
                currentScreenNo: action.initialProps.currentScreenNo,
                index: 1,
                totalScreens: action.initialProps.totalScreens,
                flowType: action.initialProps.flowType,
                questionAnswer: action.initialProps.questionAnswer,
                isLoading: false,
                type: "",
                isPopulated: false,
                dataSourceQuestionsScreen: [], // It will be array of arrays
                currentOptionList: [],
                currentBrand: action.initialProps.currentBrand,
                currentModel: action.initialProps.currentModel,
                currentName: "",
                currentEmail: "",
                errorMessage: "",
            };
        case Actions.SUBMIT_PRESSED:
            return {...state, index: action.index + 1, currentScreenNo: action.currentScreenNo + 1, isLoading: action.isLoading, type: "", isDataChanged: true};
        case Actions.LOADING_BUTTON:
            return {...state, isLoading: action.isLoading, type: ""};
        case Actions.API_FAILED:
            return {...state, isLoading: action.isLoading, type: action.type, errorMessage: action.error?.[0].message ?? 'Something went wrong!!!'};
        case Actions.NAME_EMAIL_SUBMITTED:
            return {...state, isLoading: action.isLoading, type: action.type, isDataChanged: true};
        case Actions.PARSE_QUESTION_ANSWER_RESPONSE:
            let questionAnswerResponseParser = new QuestionAnswerResponseParser();

            let parsedDataSourceFromResponse = questionAnswerResponseParser.questionAnswerDataSourceFromResponse(
                action.questionAnswerResponse, action.currentBrand, action.currentModel);
            return {
                ...state,
                dataSourceQuestionsScreen: parsedDataSourceFromResponse,
                isPopulated: true,
            };
        case Actions.CHANGE_STATE_OF_CHECKBOX:
            let questionAnswer = action.dataSourceQuestionsScreen[action.index - 1];
            let item = questionAnswer.sectionData[action.column].data[action.row];
            item.isSelected = !item.isSelected;

            const index = action.currentOptionList.indexOf(item.optionId);

            if (index > -1) {
                action.currentOptionList.splice(index, 1);
            } else {
                action.currentOptionList.push(item.optionId);
            }
            return {
                ...state,
                dataSourceQuestionsScreen: [...action.dataSourceQuestionsScreen],
                currentOptionList: action.currentOptionList,
                type: action.type
            };
        default:
            return {...state};
    }
};

const appReducer = combineReducers({
    nav: navReducer,
    appData,
});

// Note: createReactNavigationReduxMiddleware must be run before reduxifyNavigator
const middleware = createReactNavigationReduxMiddleware(
    'QuestionsGenericScreen',
    state => state.nav,
);

const App = reduxifyNavigator(AppNavigator, 'QuestionsGenericScreen');
const mapStateToProps = (state) => ({
    state: state.nav,
});

const AppWithNavigationState = connect(mapStateToProps)(App);

export const store = createStore(
    appReducer,
    applyMiddleware(middleware),
);

class Root extends React.Component {
    constructor(props) {
        super(props);
        setAPIData(props);
        store.dispatch({type: INITIAL_PROPS, initialProps: props});
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props !== nextProps) {
            updateAPIData(nextProps);
        }

        return true
    }

    render() {
        return (
            <Provider store={store}>
                <AppWithNavigationState/>
            </Provider>
        );
    }
}

export default codePush(Root);
