import { createStackNavigator } from "react-navigation";
import RatingQuestionnaire from "../Components/RatingQuestionnaire";
import ImagePreview from "../Components/ImagePreview";
import ChatAction from '../Actions/ChatAction';
import ChatbotForm from '../ChatbotComponent/ChatbotForm';
import ChatbotUploadDocumentForm from '../ChatbotComponent/ChatbotUploadDocumentForm';
import EditChatbotFormSingleSelect from "../ChatbotComponent/EditChatbotFormSingleSelect";
import EditChatbotFormText from "../ChatbotComponent/EditChatbotFormText";
import Dialog from "../Components/DialogView";
import colors from "../Constants/colors";
import FMIPTutorialComponent from '../ChatbotComponent/FMIPTutorialComponent';
import MILogoutTutorialComponent from '../ChatbotComponent/MILogoutTutorialComponent';
import DocPreviewComponent from '../ChatbotComponent/DocPreviewComponent';
import EditChatbotFormMultiSelect from '../ChatbotComponent/EditChatbotFormMultiSelect';
import ChatWebViewComponent from "../Components/ChatWebViewComponent";
var backgroundColor = colors.white;
var titleColor = colors.black;
var tintColor = colors.blue;

const navigator = createStackNavigator({
    screen1: {
        screen: ChatAction,
        navigationOptions: {
            title: "Chat",
            headerTitleStyle: {
                color: titleColor,
            },
            headerStyle: {
                backgroundColor: backgroundColor
            },
            headerTintColor: tintColor
        }
    },
    ChatWebViewComponent: {
        screen: ChatWebViewComponent,
        navigationOptions: {
            headerTitleStyle: {
                color: titleColor,
            },
            headerStyle: {
                backgroundColor: backgroundColor
            },
            headerTintColor: tintColor
        }
    },
    initialRouteName:'screen1',
}, {
    /* Same configuration as before */
});
const navigator1 = createStackNavigator(
    {
        Main: {
            screen: navigator,
        },
        RatingQuestionnaireModal: {
            screen: RatingQuestionnaire,
            navigationOptions: {
                header: null,
                gesturesEnabled: false,
            }
        },
        ImagePreview: {
            screen: ImagePreview,
        },
        ChatbotForm: {
            screen: ChatbotForm,
            navigationOptions: {
                headerTitleStyle: {
                    color: titleColor,
                },
                headerStyle: {
                    backgroundColor: backgroundColor
                },
                headerTintColor: tintColor
            }
        },
        ChatbotUploadDocumentForm: {
            screen: ChatbotUploadDocumentForm,
            navigationOptions: {
                headerTitleStyle: {
                    color: titleColor,
                },
                headerStyle: {
                    backgroundColor: backgroundColor
                },
                headerTintColor: tintColor
            }
        },
        EditChatbotFormSingleSelect: {
            screen: EditChatbotFormSingleSelect,
            navigationOptions: {
                headerTitleStyle: {
                    color: titleColor,
                },
                headerStyle: {
                    backgroundColor: backgroundColor
                },
                headerTintColor: tintColor
            }
        },
        EditChatbotFormMultiSelect: {
            screen: EditChatbotFormMultiSelect,
            navigationOptions: {
                headerTitleStyle: {
                    color: titleColor,
                },
                headerStyle: {
                    backgroundColor: backgroundColor
                },
                headerTintColor: tintColor
            }
        },
        EditChatbotFormText: {
            screen: EditChatbotFormText,
            navigationOptions: {
                headerTitleStyle: {
                    color: titleColor,
                },
                headerStyle: {
                    backgroundColor: backgroundColor
                },
                headerTintColor: tintColor
            }
        },
        Dialog: {
            screen: Dialog,
        },
        FMIPTutorialComponent: {
            screen: FMIPTutorialComponent,
            navigationOptions: {
                header: null,
                gesturesEnabled: false,
            }
        },
        MILogoutTutorialComponent: {
            screen: MILogoutTutorialComponent,
            navigationOptions: {
                header: null,
                gesturesEnabled: false,
            }
        },
        DocPreviewComponent: {
            screen: DocPreviewComponent,
            navigationOptions: {
                header: null,
                gesturesEnabled: false,
            }
        },
    },
    {
        mode: 'modal',
        headerMode: 'none',
        // cardStyle: {
        //     backgroundColor: 'transparent',
        //     opacity: 0.7,
        // },
        // transitionConfig: () => ({
        //     containerStyle: {
        //         backgroundColor: 'transparent',
        //     },
        // })
    }
);

export default navigator1;
