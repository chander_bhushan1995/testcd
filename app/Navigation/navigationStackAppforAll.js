import {createStackNavigator} from "react-navigation";
import SplashScreen from  '../AppForAll/Components/SplashScreen'
import WalkThrough from  '../AppForAll/Components/WalkThroughtScreen'
import WelcomeScreen from '../AppForAll/Components/WelcomeScreen';
import TnCWebViewComponent from "../PaymentFlow/Components/TnCWebViewComponent";

const AppForAllNavigator = createStackNavigator(
    {
        Splash: {
            screen: SplashScreen,
            navigationOptions: {
                header: null
            }
        },
        WalkThrough: {
            screen: WalkThrough,
            navigationOptions: {
                header: null,
                gesturesEnabled: false
            }
        },
        WelcomeScreen: {
            screen: WelcomeScreen,
            navigationOptions: {
                header: null
            }
        },
        TnCWebViewComponent: {
            screen: TnCWebViewComponent,
            navigationOptions: {
                title: "Terms & Conditions",
                gesturesEnabled: false
            }
        }
    })


export default AppForAllNavigator;
