import {createStackNavigator} from 'react-navigation';
import {Platform} from 'react-native';
import colors from '../Constants/colors';
import QuestionActions from '../Buyback/buybackActions/QuestionAndAnswersActions';
import EnterBankDetailsActions from '../Buyback/buybackActions/EnterBankDetailsActions';
import buybackAction from '../Buyback/buybackActions/PincodeActions';
import FinalPriceSuccessActions from '../Buyback/buybackActions/FinalPriceSuccessActions';
// import BuybackNavigationDeciderActions from "../Buyback/buybackActions/BuybackNavigationDeciderActions";
import BuybackRequestSuccessTimelineActions from '../Buyback/buybackActions/BuybackRequestSuccessTimelineActions';
import BuybackMultipleServiceRequestActions from '../Buyback/buybackActions/BuybackMultipleServiceRequestActions';
import MobilePickupScheduledSuccessActions from '../Buyback/buybackActions/MobilePickupScheduledSuccessActions';
import EnterPaytmMobileNumberComponent from '../Buyback/BuybackComponent/EnterPaytmMobileNumberComponent';
import RatingQuestionnaire from '../Buyback/BuybackComponent/RatingQuestionnaire1';
import TermsAndConditionsComponent from '../Buyback/BuybackComponent/TermsAndConditionsComponent';
import EnterUPIComponent from '../Buyback/BuybackComponent/EnterUPIComponent';
import BuybackDeclarationComponent from '../Buyback/BuybackComponent/BuybackDeclarationComponent';
import NearByRetailers from '../Buyback/BuybackComponent/NearByRetailers';
import RetailerDetailScreen from '../Buyback/BuybackComponent/RetailerDetailScreen';
import RetailersEmptyScreen from '../Buyback/BuybackComponent/RetailersEmptyScreen';
import FlowRatings from '../CommonComponents/FlowRating/FlowRatings';
import EnterPincodeQuestion from '../Buyback/BuybackComponent/EnterPincodeQuestion';
import BuySellSelectionActions from '../Buyback/buybackActions/BuySellSelectionActions';
import QuestionHintScreen from '../Buyback/BuybackComponent/QuestionHintScreen';

var backgroundColor;
var titleColor;
var tintColor;
if (Platform.OS === 'ios') {
    titleColor = colors.black;
    tintColor = colors.blue;
    backgroundColor = colors.white;
} else {
    backgroundColor = colors.blue;
    titleColor = colors.white;
    tintColor = colors.white;
}

const navigator = createStackNavigator(
    {
        screen1: {
            screen: buybackAction,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
    },
    {
        headerMode: 'none',
    },
);

const successNavigator = createStackNavigator(
    {
        screen1: {
            screen: FinalPriceSuccessActions,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
    },
    {
        headerMode: 'none',
        mode: 'modal',
    },
);

const hintNavigator = createStackNavigator(
    {
        screen1: {
            screen: QuestionHintScreen,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
    },
    {
        headerMode: 'none',
        mode: 'modal',
    },
);

const schedulrpickupnavigator = createStackNavigator({
    screenQuestion: {
        screen: QuestionActions,
        navigationOptions: {
            gesturesEnabled: false,
            title: 'Mobile Condition',
            headerTitleStyle: {
                color: colors.charcoalGrey,
            },
            headerStyle: {
                backgroundColor: colors.white,
            },
            headerTintColor: colors.blue,
        },
    },
});

const MultipleRequestNavigator = createStackNavigator({
        BuybackMultipleServiceRequest: {
            screen: BuybackMultipleServiceRequestActions,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
    },
    {
        headerMode: 'screen',
    });

export default navigator1 = createStackNavigator(
    {
        BuybackComponent: {
            screen: buybackAction,
            navigationOptions: {
                gesturesEnabled: false,
                header: null
            },
        },
        BuybackMultipleServiceRequest: {
            screen: BuybackMultipleServiceRequestActions,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        BuybackQuestions: {
            screen: QuestionActions,
            navigationOptions: {
                gesturesEnabled: false,
                title: 'Mobile Condition',
                headerTitleStyle: {
                    color: colors.charcoalGrey,
                },
                headerStyle: {
                    backgroundColor: colors.white,
                },
                headerTintColor: colors.blue,
            },
        },
        SchedulePickup: {
            screen: schedulrpickupnavigator,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        EnterBankDetails: {
            screen: EnterBankDetailsActions,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        MobilePickupScheduledSuccess: {
            screen: MobilePickupScheduledSuccessActions,
            navigationOptions: {
                gesturesEnabled: false,
                header: null
            },
        },
        BuybackRequestSuccessTimeline: {
            screen: BuybackRequestSuccessTimelineActions,
            navigationOptions: {
                gesturesEnabled: false,
                header: null
            },
        },
        SuccessScreen: {
            screen: successNavigator,
            navigationOptions: {
                gesturesEnabled: false,
                header: null
            },
        },
        HintScreen: {
            screen: hintNavigator,
            navigationOptions: {
                gesturesEnabled: false,
                header: null
            },
        },
        RatingQuestionnaireModal1: {
            screen: RatingQuestionnaire,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        EnterPaytmNumber: {
            screen: EnterPaytmMobileNumberComponent,
            navigationOptions: {
                gesturesEnabled: false,
                header: null
            },
        },
        EnterPincodeNumber: {
            screen: EnterPincodeQuestion,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        EnterUPI: {
            screen: EnterUPIComponent,
            navigationOptions: {
                gesturesEnabled: false,
                header: null
            },
        },
        TermsAndConditions: {
            screen: TermsAndConditionsComponent,
            navigationOptions: {
                gesturesEnabled: false,
                title: 'Terms and Conditions',
                headerTitleStyle: {
                    color: colors.charcoalGrey,
                },
                headerStyle: {
                    backgroundColor: colors.white,
                },
                headerTintColor: colors.blue,
            },
        },
        BuybackDeclaration: {
            screen: BuybackDeclarationComponent,
            navigationOptions: {
                gesturesEnabled: false,
                title: 'Declaration',
                headerTitleStyle: {
                    color: colors.charcoalGrey,
                },
                headerStyle: {
                    backgroundColor: colors.white,
                },
                headerTintColor: colors.blue,
            },
        },
        BuySellSelection: {
            screen: BuySellSelectionActions,
            navigationOptions: {
                gesturesEnabled: false,
                title: '',
                headerTitleStyle: {
                    color: colors.charcoalGrey,
                },
                headerStyle: {
                    backgroundColor: colors.white,
                },
                headerTintColor: colors.blue,
            },
        },
        NearByRetailers: {
            screen: NearByRetailers,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        RetailerDetailScreen: {
            screen: RetailerDetailScreen,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        RetailersEmptyScreen: {
            screen: RetailersEmptyScreen,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        FlowRatings: {
            screen: FlowRatings,
            navigationOptions: {
                gesturesEnabled: false,
                header: null
            },
        }
    },
    {
        headerMode: 'screen',
        // cardStyle: {
        //     backgroundColor: 'transparent',
        //     opacity: 0.7,
        // },
        // transitionConfig: () => ({
        //     containerStyle: {
        //         backgroundColor: 'transparent',
        //     },
        // })
    },
);
