import {createStackNavigator} from "react-navigation";
import RCQuestionnaireScreen from "../riskcalculator/action/RCQuestionsAction";
import colors from '../Constants/colors';
import RCLauncherScreen from "../riskcalculator/action/RCLauncherAction";
import RCResultScreen from "../riskcalculator/components/RCResultComponent"

var backgroundColor = colors.white;
var titleColor = colors.black;
var tintColor = colors.blue;

const rcNavigator = createStackNavigator(
    {
        RiskCalculatorFlow: {
            screen: RCLauncherScreen,
            navigationOptions: {
                mode: 'modal',
                header: null
            }
        },
        RCQuestionsScreen: {
            screen: RCQuestionnaireScreen,
            navigationOptions: {
                title: "Risk Calculator",
                headerTitleStyle: {
                    color: titleColor,
                },
                headerStyle: {
                    backgroundColor: backgroundColor
                },
                headerTintColor: tintColor,
            }
        },
        RCResultScreen: {
            screen: RCResultScreen,
            navigationOptions: {
                title: "Risk Calculator",
                headerTitleStyle: {
                    color: titleColor,
                },
                headerStyle: {
                    backgroundColor: backgroundColor
                },
                headerTintColor: tintColor,
            }
        }
    });


export default rcNavigator;