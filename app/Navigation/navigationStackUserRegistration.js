import {createStackNavigator} from 'react-navigation';
import QuestionsGenericScreen from '../AppForAll/MyAccount/Actions/QuestionsGenericAction';
import SelectBankScreen from '../AppForAll/MyAccount/Components/SelectBankScreen';
import colors from '../Constants/colors';


const UserRegistrationNavigator = createStackNavigator(
    {
        QuestionsGenericScreen: {
            screen: QuestionsGenericScreen,
        },
        SearchBank: {
            screen: SelectBankScreen,
        },
    },{
        transitionConfig: () => ({ screenInterpolator: () => null }),
    });

export default UserRegistrationNavigator;
