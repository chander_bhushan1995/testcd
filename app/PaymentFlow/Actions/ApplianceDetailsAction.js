import { connect } from "react-redux";
import { NativeModules } from "react-native";
import * as Actions from "./ActionTypes";
import ApplianceDetailsComponent from "../Components/ApplianceDetailsComponent"
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

const nativeBrigeRef = NativeModules.ChatBridge;
const mapStateToProps = state => ({
  displayedComponents: state.applianceDetailsReducer.displayedComponents,
  arrDocsForUpload: state.applianceDetailsReducer.arrDocsForUpload,
  assetInfo: state.applianceDetailsReducer.assetInfo,
  initialProps: state.nav.initialProps,
  formFieldsJson: state.createCustomerReducer.formFieldsJson,
  formFieldsInputValid: state.applianceDetailsReducer.formFieldsInputValid,
  type: state.applianceDetailsReducer.type,
  tempCustInfoDataFromPayment: state.paymentSuccessStepOneReducer.tempCustInfoData,
  tempCustInfoDataRefreshed: state.membershipActivationReducer.tempCustInfoData,
});

const mapDispatchToProps = dispatch => ({
  getHA_EW_ApplianceDetailsFormFields: (initialProps, formFieldsJson) => {
    if (formFieldsJson !== null) {
      dispatch({ type: Actions.getFormFields_hs_ew_ad, initialProps: initialProps, formFieldsJson: formFieldsJson.pre_post_onboarding_form_fields })
      return
    }
    //get preboarding form fields from firebase
    nativeBrigeRef.getPrePostOnboardingFormFields(json => {
      let jsonObject = parseJSON(json);
      let prepostJson = deepCopy(jsonObject.pre_post_onboarding_form_fields);
      dispatch({ type: Actions.getFormFields_hs_ew_ad, initialProps: initialProps, formFieldsJson: prepostJson })
    });
  },
  validateApplianceDetailsFormInput: (text, index) => {
    dispatch({ type: Actions.validateApplianceDetailsFormInput, text: text, index: index })
  },
  validateApplianceDetalsFormFields: () => {
    dispatch({ type: Actions.validateApplianceDetalsFormFields });
  },
  resetType: () => {
    dispatch({ type: Actions.resetViewType });
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplianceDetailsComponent);
