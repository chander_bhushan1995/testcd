import * as Actions from "./ActionTypes";
import { connect } from "react-redux";
import BasicDetailsComponent from "../Components/BasicDetails/BasicDetailsComponent";
import * as PaymentApi from '../DataLayer/PaymentAPIHelper';
import { NativeModules } from "react-native";
import paymentFlowStrings from "../Constants/Constants";
import { trackAppScreen } from "../../commonUtil/WebengageTrackingUtils"
import { MOBILE_ADDRESS, WALLET_ADDRESS } from "../../Constants/WebEngageScreenConstant";
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

import * as Store from '../../commonUtil/Store'

const nativeBrigeRef = NativeModules.ChatBridge;
const mapStateToProps = state => ({
  getDocLoaderShowing: state.basicDetailsReducer.getDocLoaderShowing,
  arrDocsForUpload: state.basicDetailsReducer.arrDocsForUpload,
  radioItems: state.basicDetailsReducer.radioItems,
  displayedComponents: state.basicDetailsReducer.displayedComponents,
  isLoading: state.basicDetailsReducer.isLoading,
  formFieldsValid: state.basicDetailsReducer.formFieldsValid,
  type: state.basicDetailsReducer.type,
  pincodeDetails: state.basicDetailsReducer.pincodeDetails,
  addressTmp: state.basicDetailsReducer.addressTmp,
  actCode: state.paymentSuccessStepOneReducer.actCode,
  planCategory: state.paymentSuccessStepOneReducer.planCategory,
  customerDetails: state.paymentSuccessStepOneReducer.tempCustInfoData,
  refreshedTempCustomerDetails: state.basicDetailsReducer.refreshedTempCustomerDetails,
  initialProps: state.nav.initialProps,
  assetInfo: state.applianceDetailsReducer.assetInfo,
  productCode: state.basicDetailsReducer.productCode,
});
const mapDispatchToProps = dispatch => ({
  getTempCustInfo: activationCode => {

    PaymentApi.getTempCustInfo(activationCode, (response, error) => {
        if (response) {
          dispatch({ type: Actions.fetchUserInfoSuccess, data: response });
        } else {
          dispatch({ type: Actions.fetchUserInfoFail, data: error });
        }
    });
  },

  getPostBoardingFormFields: (firebaseKey) => {
    //get preboarding form fields from firebase
    nativeBrigeRef.getPrePostOnboardingFormFields(json => {
      let jsonObject = parseJSON(json);
      let prepostJson = deepCopy(jsonObject.pre_post_onboarding_form_fields);
      Store.getCustomerBasicDetails((error, value) => {
        dispatch({ type: Actions.getPostBoardingFormFields, prepostJson: prepostJson, firebaseKey: firebaseKey, pincodeDetails: value });
      })
    });
  },
  trackAppScreen: (service_Type) => {
    //get service type
    if (service_Type === paymentFlowStrings.paymentFlowServiceTypes.PE) {
      trackAppScreen(MOBILE_ADDRESS);
    } else if (service_Type === paymentFlowStrings.paymentFlowServiceTypes.WALLET) {
      trackAppScreen(WALLET_ADDRESS);
    }
    //logWebEnageEvent(ADDRESS, eventAttr);
  },
  getDocsToUpload: activationCode => {
    PaymentApi.getDocsToUpload(activationCode, (response, error) => {
        if (response) {
          dispatch({ type: Actions.fetchDocsToUploadSuccess, data: response });
        } else {
          dispatch({ type: Actions.fetchDocsToUploadFail, data: error });
        }
    });
  },
  validateInput: (text, index) => {
    dispatch({ type: Actions.validateInput, text: text, index: index })
  },
  checkPincode: (pincode) => {
    dispatch({ type: Actions.checkingPincode })
      // get address details
    PaymentApi.checkPincode(pincode, (response, error) => {
        if (response) {
          let status = response["status"];
          if (status === 'success') {
            let addressTmp = response["addressTmp"];
            dispatch({ type: Actions.checkPincodeSuccess, data: addressTmp })

          } else {
            dispatch({ type: Actions.checkPincodeFailure, data: null })
            // this.setState({ isPrimaryBtnEnabled: true });
          }
        } else {
          dispatch({ type: Actions.checkPincodeFailure, text: error.responseMessage, index: 0 })
          // this.setState({ isPrimaryBtnEnabled: true });
        }
      });
  },
  submitPostDetails: (postPaymentDetails) => {
    PaymentApi.submitPostDetails(postPaymentDetails, (response, error) => {
      if (response) {
          dispatch({ type: Actions.submitPostDetailsSuccess, data: null })
      } else {
          dispatch({ type: Actions.submitPostDetailsFailure, data: null })
          // this.setState({ isPrimaryBtnEnabled: true });
          // this.refs.toast.show(paymentFlowStrings.paymentFlowErrorMessages.submitDetailsErrorMsg);
        }
      });
  },
})
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BasicDetailsComponent);
