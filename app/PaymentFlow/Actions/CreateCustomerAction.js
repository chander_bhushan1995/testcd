import { NativeModules } from "react-native";
import { connect } from "react-redux";
import CreateCustomerComponent from "../Components/CreateCustomerComponent";
import * as Actions from "../Actions/ActionTypes";
import * as PaymentApi from "../DataLayer/PaymentAPIHelper";
import { parseJSON, deepCopy } from "../../commonUtil/AppUtils";
import { APIErrorCodes } from "../../Constants/AppConstants";

const nativeBrigeRef = NativeModules.ChatBridge;
const mapStateToProps = state => ({
    isLoading: state.createCustomerReducer.isLoading,
    userCreated: state.createCustomerReducer.userCreated,
    paynowlink: state.createCustomerReducer.paynowlink,
    isApiError: state.createCustomerReducer.isApiError,
    errorApiMessage: state.createCustomerReducer.errorApiMessage,
    errorApiType: state.createCustomerReducer.errorApiType,
    displayedComponents: state.createCustomerReducer.displayedComponents,
    flowServiceType: state.createCustomerReducer.flowServiceType,
    productTermAndCUrl: state.createCustomerReducer.productTermAndCUrl,
    formFieldsValid: state.createCustomerReducer.formFieldsValid,
    checkboxChecked: state.createCustomerReducer.checkboxChecked,
    agreedFDTerms: state.createCustomerReducer.agreedFDTerms,
    primaryButtonEnabled: state.createCustomerReducer.primaryButtonEnabled,
    isApiCalled: state.createCustomerReducer.isApiCalled,
    type: state.createCustomerReducer.type,
    userDetailsString: state.createCustomerReducer.userDetailsString,
    activationCode: state.nav.initialProps.postboarding_screen_act_code,
    warranties: state.createCustomerReducer.warranties,
    warrantyRequired: state.createCustomerReducer.warrantyRequired,
    formFieldsJson: state.createCustomerReducer.formFieldsJson,
    initialProps: state.nav.initialProps,
    customerInfo: state.createCustomerReducer.customerInfo,
    customerOrderInfo: state.createCustomerReducer.customerOrderInfo,
    idfenceSortTerm: state.createCustomerReducer.idfenceSortTerm,
    idfenceTerms: state.createCustomerReducer.idfenceTerms,
    viewMoreTitle: state.createCustomerReducer.viewMoreTitle,
    trialHeaderData: state.createCustomerReducer.trialHeaderData,
    callValidateEmail: state.createCustomerReducer.callValidateEmail,
    routeList: state.nav.routes,
});
const mapDispatchToProps = dispatch => ({
    createTrialCustomer: (inputDetails, initialProps) => {
        let initialDetails = initialProps.data;
        if (initialDetails && inputDetails) {
            dispatch({
                type: Actions.createCustomerInProgress,
            });
            PaymentApi.boardTrialPlan(initialDetails, inputDetails, (response, error) => {
                if (response) {
                    dispatch({
                        type: Actions.createCustomerSuccessTrial,
                        data: response,
                    });
                } else {
                    dispatch({
                        type: Actions.createCustomerFailure,
                        data: error,
                    });
                }
            });
        }
    },
    resetAction:()=>
    {
        dispatch({
            type: Actions.createCustomerActionReset,
        });
    },
    createCustomer: (inputDetails, initialProps) => {
        let initialDetails = initialProps.data;
        if (initialDetails && inputDetails) {
            dispatch({
                type: Actions.createCustomerInProgress,
            });
            PaymentApi.onboardCustomer(initialDetails, inputDetails, function callback(response, error) {
                if (response) {
                    dispatch({
                        type: Actions.createCustomerSuccess,
                        data: response,
                    });
                } else {
                    if (error && error.errors && error.errors.length > 0) {
                        let errorCode = error?.errors[0]?.errorCode;
                        if (errorCode === APIErrorCodes.INVALID_PROMO_CODE) {
                            if (initialDetails && typeof initialDetails === "string") {
                                let initialDetailsJSON = parseJSON(initialDetails);
                                if (initialDetailsJSON?.orderInfo) {
                                    initialDetailsJSON.orderInfo["promoName"] = null;
                                    initialDetails = JSON.stringify(initialDetailsJSON);
                                }
                            }
                            PaymentApi.onboardCustomer(initialDetails, inputDetails, callback);
                            return;
                        }
                    }
                    dispatch({
                        type: Actions.createCustomerFailure,
                        data: error,
                    });
                }
            });
        }
    },
    validateEmail: (inputDetails, initialProps) => {
        dispatch({
            type: Actions.createCustomerInProgress,
        });
        PaymentApi.validateEmail(inputDetails.emailId, (response, error) => {
            if (response) {
                dispatch({
                    type: Actions.validateEmailSuccess,
                    data: response,
                });
            } else {
                dispatch({
                    type: Actions.validateEmailFailure,
                    data: error,
                });
            }
        });
    },
    getformFields: (initialProps, formFieldsJson) => {
        dispatch({
            type: Actions.getFormFields,
            initialProps: initialProps,
            formFieldsJson: formFieldsJson,
        });
    },
    validatePreboardingFormFields: () => {
        dispatch({
            type: Actions.validatePreboardingFormFields,
        });
    },
    toggleCheckbox: () => {
        dispatch({
            type: Actions.toggleCheckbox,
        });
    },
    toggleFDTermsCheckbox: () => {
        dispatch({
            type: Actions.toggleFDTermsCheckbox,
        });
    },
    validatePreBoardFormInput: (text, index) => {
        dispatch({
            type: Actions.validatePreBoardFormInput,
            text: text,
            index: index,
        });
    },
    getWarrantyPeriod: () => {
        dispatch({
            type: Actions.getWarrantyInProgress,
        });
        PaymentApi.getWarrantyPeriod((response, error) => {
            if (response) {
                dispatch({
                    type: Actions.getWarrantySuccess,
                    data: response,
                });
            } else {
                dispatch({
                    type: Actions.getWarrantyFailure,
                    data: error,
                });
            }
        });
    },
    getFormFieldsJson: () => {
        nativeBrigeRef.getPrePostOnboardingFormFields(json => {
            let jsonObject = parseJSON(json);
            dispatch({
                type: Actions.getFormFieldsJson,
                data: jsonObject,
            });
        });
    },
    getTermsAndConditionJson: initialProps => {
        nativeBrigeRef.getProductTerms(json => {
            let jsonObject = parseJSON(json);
            dispatch({
                type: Actions.getTermsAndConditionJson,
                initialProps: initialProps,
                data: jsonObject,
            });
        });
    },
});
export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(CreateCustomerComponent);
