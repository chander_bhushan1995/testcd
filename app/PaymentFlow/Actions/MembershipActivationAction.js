import { connect } from "react-redux";
import * as Actions from "./ActionTypes";
import MembershipActivationComponent from "../Components/MembershipActivationComponent"
import * as PaymentApi from '../DataLayer/PaymentAPIHelper';
import {
  NativeModules,
} from "react-native";
const nativeBrigeRef = NativeModules.ChatBridge;

const mapStateToProps = state => ({
    stageCount: state.membershipActivationReducer.stageCount,
    tempCustInfoData:  state.membershipActivationReducer.tempCustInfoData,
    customerDetails: state.paymentSuccessStepOneReducer.tempCustInfoData,
    initialProps: state.nav.initialProps,
    applianceName: state.membershipActivationReducer.applianceName,
    actCode: state.paymentSuccessStepOneReducer.actCode,
  });

const mapDispatchToProps = dispatch => ({
    getTempCustInfo: activationCode => {
        PaymentApi.getTempCustInfo(activationCode, (response, error) => {
            if (response) {
              dispatch({ type: Actions.fetchTempCustInfoSuccess, data: response });
            } else {
              dispatch({ type: Actions.fetchTempCustInfoFail, data: error });
            }
        });
      },
      getApplianceName: productCode => {
        nativeBrigeRef.getNameForSubCategory(productCode, subCategoryName => {
          dispatch({ type: Actions.getApplianceName, applianceName: subCategoryName });

        })
      }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MembershipActivationComponent);

