import { gerRelationshipNumber } from "../Actions/ActionTypes";
import { connect } from "react-redux";
import paymentSuccessComponent from "../Components/WAPaymentSuccessComponent";
import * as Store from '../../commonUtil/Store'
const mapStateToProps = state => ({
	relno: state.paymentSuccessReducer.relno,
});

const mapDispatchToProps = dispatch => ({
	getRelNo: () => {
		Store.getValueForKey("relno", (error, value) => {
			dispatch({ type: gerRelationshipNumber, relno: value });
		});
	},
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(paymentSuccessComponent);

