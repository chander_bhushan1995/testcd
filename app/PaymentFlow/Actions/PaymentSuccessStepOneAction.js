import { fetchUserInfoSuccess, fetchUserInfoFail } from "./ActionTypes";
import { connect } from "react-redux";
import PaymentSuccessStepOneComponent from "../Components/PaymentSuccessStepOneComponent";
import * as PaymentApi from '../DataLayer/PaymentAPIHelper';
import { NativeModules } from "react-native";
const chatBridgeRef = NativeModules.ChatBridge;
const mapStateToProps = state => ({
  isLoading: state.paymentSuccessStepOneReducer.isLoading,
  productCode: state.paymentSuccessStepOneReducer.productCode,
  planCategory: state.paymentSuccessStepOneReducer.planCategory,
  tempCustInfoData: state.paymentSuccessStepOneReducer.tempCustInfoData,
  actCode: state.paymentSuccessStepOneReducer.actCode,
  routeList: state.nav.routes,
  isTaskInspection: state.paymentSuccessStepOneReducer.isTaskInspection,
  isPlanServiceEW: state.paymentSuccessStepOneReducer.isPlanServiceEW,
  screenData: state.paymentSuccessStepOneReducer.screenData,
  type: state.paymentSuccessStepOneReducer.type,
  initialProps: state.nav.initialProps,

});
const mapDispatchToProps = dispatch => ({
  getTempCustInfo: activationCode => {
    if (activationCode === null || activationCode === undefined) {
      dispatch({ type: fetchUserInfoFail, data: "activationCode not found" });
      return;
    }
    PaymentApi.getTempCustInfo(activationCode, (response, error) => {
        if (response) {
          dispatch({ type: fetchUserInfoSuccess, activationCode: activationCode, data: response });
        } else {
          dispatch({ type: fetchUserInfoFail, data: error });
        }
    });
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PaymentSuccessStepOneComponent);
