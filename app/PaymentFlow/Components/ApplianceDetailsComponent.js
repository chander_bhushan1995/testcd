import React, { Component } from "react";
import {
  Platform,
  NativeModules,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
  BackHandler,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator,
} from "react-native";
import ButtonWithLoader from "../../CommonComponents/ButtonWithLoader.js";
import spacing from "../../Constants/Spacing";
import Toast from "react-native-easy-toast";
import paymentFlowStrings from "../Constants/Constants";
import colors from "../../Constants/colors";
import { PLATFORM_OS } from "../../Constants/AppConstants";
import { TextStyle } from "../../Constants/CommonStyle";
import { HeaderBackButton } from "react-navigation";
import InputBox from "../../Components/InputBox";
import * as Actions from "../Actions/ActionTypes";
import * as FirebaseKeys from "../../Constants/FirebaseConstants"
import fontSizes from "../../Constants/FontSizes";
import DocumentUploadUtils from "../../Components/DocumentUploadUtils";
import * as PaymentApi from '../DataLayer/PaymentAPIHelper';
import DialogView from "../../Components/DialogView";

const nativeBrigeRef = NativeModules.ChatBridge;
let tempCustInfoData = null;

export default class ApplianceDetailsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      docFileInfo: { type: "image" },
      arrDocsForUpload: [],
      isAnyImageUploading: false,
      isDocUploaded: false,
    }
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: (
        <View
          style={
            Platform.OS === PLATFORM_OS.ios
              ? { alignItems: "center" }
              : { alignItems: "flex-start" }
          }
        >
          <Text style={TextStyle.text_16_bold}>
            {
              paymentFlowStrings.paymentFlowComponentTitles
                .applianceDetailsTitle
            }
          </Text>
        </View>
      ),
      headerLeft: (
        <HeaderBackButton
          tintColor={colors.black}
          onPress={() => {
            navigation.pop();
          }}
        />
      ),
      headerTitleStyle: { color: colors.white },
      headerTintColor: colors.white,
      headerRight:<TouchableOpacity style={{marginTop: 0, flexDirection: 'row', marginRight: 16}}
                        onPress={() => {
                            nativeBrigeRef.openChat();
                        }}>
          <Image style={styles.chatIconStyle} source={require("../../images/icon_chat_blue.webp")}/>
      </TouchableOpacity>,
    };
  };
  componentDidMount() {
    this.props.getHA_EW_ApplianceDetailsFormFields(this.props.initialProps, this.props.formFieldsJson);
    BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    tempCustInfoData = this.props.navigation.getParam("tempCustInfoData", null);

  }
  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
  }
  render() {
    switch (this.props.type) {
      case Actions.validateApplianceDetalsFormFields:
        if (this.props.formFieldsInputValid && this.state.isDocUploaded) {
          this.navigateToMembershipActivationScreen();
        }
      default: break;
    }

    return (
      <SafeAreaView style={styles.SafeArea}>
        <ScrollView style={styles.scrollViewStyle}>
          <View style={styles.rootContainerStyle}>
            <View style={styles.nameAndDesctiptionContainerStyle}>
              <Text style={styles.stepTextStyle}>STEP 1 of 2</Text>
              <Text style={styles.nameAndDesctiptionTextStyle}>
                Ensure to provide correct details inline with your appliance
                invoice to avoid any hiccups.
              </Text>
              {this.getFormFieldsTemplate()}
              {this.getDocsToUploadComponent()}
            </View>
          </View>
        </ScrollView>
        <Toast ref="toast" position="center" />
        <DialogView ref={ref => (this.DialogViewRef = ref)}/>
        {this.getPrimaryButtonComponent()}

      </SafeAreaView>
    );
  }

  /// components
  getFormFieldsTemplate = () => {
    let template = <View />;
    if (this.props.displayedComponents !== undefined) {
      template = this.props.displayedComponents.map(item => {
        return (
          <View style={styles.addressContainer} key={item.index}>
            <InputBox
              containerStyle={styles.addressTxtInputContainer}
              key={item.index}
              isMultiLines={item.isMultiLines}
              value={item.inputValue}
              height={item.height}
              keyboardType={item.keyboardType}
              inputHeading={item.title}
              placeholder={item.placeholder}
              placeholderTextColor={colors.grey}
              maxLength={item.maxLength}
              errorMessage={item.errorMessage}
              onChangeText={text => {
                item.errorMessage = "";
                this.props.validateApplianceDetailsFormInput(text, item.index);
              }}
            />
          </View>
        );
      });
    }
    return template;
  };

  getPrimaryButtonComponent = () => {
    return (
      <View style={styles.primaryButtonContainer}>
        <ButtonWithLoader
          isLoading={false} //{!this.state.primaryButtonEnabled}
          enable={true} //{this.state.primaryButtonEnabled}
          Button={{
            text:
              paymentFlowStrings.paymentFlowActionButtonTitle.proceedToNextStep,
            onClick: () => {
              this.props.validateApplianceDetalsFormFields();
              if (!this.state.isDocUploaded) {
                this.refs.toast.show('Please upload invoice.');
                return;
              }

            }
          }}
        />
      </View>
    );
  };

  getDocsToUploadComponent = () => {
    let template = <View />;
    template = this.props.arrDocsForUpload.map((item) => {
      return (
        <View style={styles.imgContainer} key={item["index"]} >
          <View style={{
            justifyContent: 'flex-start',
            alignSelf: 'flex-start',
            width: "80%"

          }}>
            <Text style={{
              lineHeight: 20,
              fontSize: fontSizes.fontSize16,
              fontFamily: "Lato",
              color: '#212121',
            }}>{item["displayName"]}</Text>
            <View style={{
              justifyContent: 'flex-start',
              alignSelf: 'flex-start',
              width: "100%",
              marginTop: 12,
              flexDirection: 'row',
              display: ((item["guideMessage"] === null) ? 'none' : 'flex')
            }}>
              <View style={styles.circleShapeView} />
              <Text style={{
                justifyContent: 'flex-start',
                alignSelf: 'flex-start',
                width: "95%",
                marginTop: -5,
                marginLeft: 8,
                fontSize: fontSizes.fontSize12,
                fontFamily: "Lato",
                color: '#757575',


              }}>{(item["guideMessage"] === null) ? "" : item["guideMessage"][0]}</Text>
            </View>
          </View>
          <View style={{
            width: "20%",
            justifyContent: 'center',
          }}>
            <TouchableOpacity onPress={() => this.onImageUploadClick(item)}>
              <View style={{
                width: 56,
                height: 56,
                justifyContent: 'center',
                alignSelf: 'flex-end',
                alignItems: 'center',
                borderRadius: 4,
                borderWidth: 1,
                borderColor: '#E0E0E0',
                backgroundColor: '#FAFAFA'
              }}>
                <ImageBackground style={{
                  width: 52, height: 52, alignItems: 'center',
                  justifyContent: 'center',
                  alignSelf: "center",
                  display: ((item["localImgUrl"] === "") ? 'none' : 'flex')
                }}
                  source={{ uri: item["localImgUrl"] }} >
                  <ActivityIndicator animating ={this.state.isAnyImageUploading} style={{
                    display: ((item["isShowLoader"]) ? 'flex' : 'none')
                  }} size="small" color="#0000ff" />
                </ImageBackground>
                <Image key={item["index"]}
                  style={{ width: 14, height: 12, marginTop: -50, display: ((item["localImgUrl"] === undefined) ? 'flex' : 'none') }}
                  source={require('../../images/fd_ic_camera_service_claim.webp')} />
              </View>
            </TouchableOpacity>
          </View>

        </View>
      );
    });
    return template;
  };

  onImageUploadClick = (item) => {

    this.props.resetType();
    if (!this.state.isAnyImageUploading) {
      new DocumentUploadUtils().plusIconClick(
        item["index"],
        this,
        (index, docUri, docType, fileName, docSize, fileType) => {
          const arrUploadDocs = this.props.arrDocsForUpload;
          let selectedImage = arrUploadDocs[index];
          let docDetail, docName, docFormat, fName;

          if (Platform.OS === "ios") {
            docDetail = docUri.substring(docUri.lastIndexOf("/") + 1);
            docName = docDetail;
            docFormat = docDetail.substring(docDetail.lastIndexOf(".") + 1);
            fName = docDetail.substring(0, docDetail.lastIndexOf("."));
          } else {
            docDetail = fileName.substring(docUri.lastIndexOf("/") + 1);
            docName = fileName;
            fName = fileName.substring(0, fileName.lastIndexOf("."));
            docFormat = fileName.substring(fileName.lastIndexOf(".") + 1);
          }
          try {
            let customerDetails = tempCustInfoData["customerDetails"][0]
            let activityReferenceId = customerDetails["activityRefId"];
            if (docUri !== null) {
              selectedImage.localImgUrl = docUri;
              selectedImage.docName = docName;
              selectedImage.fileName = fName;
              selectedImage.docFormat = docFormat;
              selectedImage.activityReferenceId = activityReferenceId;
              selectedImage.isShowLoader = true;
              arrUploadDocs[index] = selectedImage;
              this.setState({ isAnyImageUploading: true });
              PaymentApi.uploadAssetDocument(selectedImage, fileType, (response, error) => {
                  if (response && response.status === "success") {
                      selectedImage.data = response.data;
                      selectedImage.isShowLoader = false;
                      arrUploadDocs[index] = selectedImage;
                      this.refs.toast.show('Doc uploaded successfully!');
                      this.setState({
                        isAnyImageUploading: false,
                        isDocUploaded: true,
                      });
                    } else {
                      this.refs.toast.show('Unable to upload file. type again!');
                      arrUploadDocs[index].localImgUrl = undefined;
                      this.setState({
                        isAnyImageUploading: false,
                        isDocUploaded: false,
                      });
                    }
              });
            }
          } catch (e) {
          }

        });
    }
  };


  onBackPress = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  navigateToMembershipActivationScreen = () => {
    this.props.navigation.navigate('BasicDetailsComponent', {
      stageCount: 2,
      firebaseKey: FirebaseKeys.HS_EW_BD
    });
  }
}

/// styles
const styles = StyleSheet.create({
  SafeArea: {
    flex: spacing.spacing1,
    backgroundColor: colors.white
  },
  rootContainerStyle: {
    flex: spacing.spacing1,
    backgroundColor: colors.white,
    paddingHorizontal: spacing.spacing16
  },
  profileIconContainerStyle: {
    flex: spacing.spacing1,
    backgroundColor: colors.white,
    alignItems: "center"
  },
  stepCounterContainerStyle: {},
  activationStageContainerStyle: {
    flexDirection: "row"
  },
  stepCounterStyle: {
    width: "100%",
    height: undefined,
    aspectRatio: 1
  },
  nameAndDesctiptionContainerStyle: {
    flex: spacing.spacing1,
    backgroundColor: colors.white,
    paddingTop: spacing.spacing0,
  },
  stepTextStyle: {
    ...TextStyle.text_14_normal,
    marginTop: spacing.spacing8
  },
  nameAndDesctiptionTextStyle: {
    ...TextStyle.text_14_bold,
    marginTop: spacing.spacing8
  },
  desctiptionTextStyle: {
    ...TextStyle.text_12_normal,
    marginTop: spacing.spacing8
  },
  primaryButtonContainer: {
    marginTop: spacing.spacing16,
    marginLeft: spacing.spacing16,
    marginRight: spacing.spacing16,
    marginBottom: spacing.spacing16,
    justifyContent: "flex-end",
    alignSelf: "stretch"
  },
  addressTxtInputContainer: {
    flex: 1,
    marginTop: spacing.spacing16
  },
  scrollViewStyle: {
    flex: spacing.spacing1,
    paddingBottom: spacing.spacing32,
    backgroundColor: colors.white
  },
  profileIcon: {
    alignItems: "center"
  },
  imgContainer: {
    flex: 1,
    marginTop: 20,
    alignSelf: 'stretch',
    flexDirection: "row",
  },
  circleShapeView: {
    alignSelf: 'flex-start',
    width: 8,
    height: 8,
    borderRadius: 8 / 2,
    backgroundColor: '#b3b3b3'
  },
  addressContainer: {
    backgroundColor: colors.white,
    flexDirection: "row",
  },
  chatIconStyle: {
      width: 24,
      height: 24,
      marginTop: 4,
      resizeMode: "contain"
  },
  chatTextStyle: {
      marginLeft: spacing.spacing6,
      textAlign: 'center',
      color: colors.white
  }
});
