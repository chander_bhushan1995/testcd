import { StyleSheet } from 'react-native';
import spacing from "../../../Constants/Spacing";
import dimens from '../../../Constants/Dimens';
import colors from '../../../Constants/colors';
import fontSizes from "../../../Constants/FontSizes";
import {TextStyle} from '../../../Constants/CommonStyle';
export default StyleSheet.create({
    SafeArea: {
        flex: spacing.spacing1,
        backgroundColor: colors.white
    },
    container: {
        backgroundColor: colors.white,
        flex: spacing.spacing1,
    },
    scrollViewcontainer: {
        flex: spacing.spacing1,
        flexDirection: 'column',
        backgroundColor: colors.white,
        paddingBottom: spacing.spacing32,
        alignItems: 'center',
    },
    bttonContainer: {
        height: dimens.height20,
        width: dimens.width100,
        justifyContent: 'flex-end',
        flexDirection: 'column',
        backgroundColor: colors.white,
        alignItems: 'center',
    },
    circleShapeView: {
        alignSelf: 'flex-start',
        width: spacing.spacing8,
        height: spacing.spacing8,
        borderRadius: spacing.spacing8 / 2,
        backgroundColor: colors.color_b3b3b3
    },
    textView: {
        width: dimens.width100,
        textAlignVertical: 'center',
        padding: spacing.spacing10,
        color: colors.black
    },
    scrollViewStyle: {
        flex: spacing.spacing1,
        backgroundColor: colors.white,
    },
    topHeaderContainer: {
        backgroundColor: colors.white,
        marginTop: spacing.spacing16,
        marginRight: spacing.spacing20,
        marginLeft: spacing.spacing16,
    },
    genderHeaderContainer: {
        flexDirection: "row",
        marginTop: spacing.spacing24,
        marginRight: spacing.spacing20,
        marginLeft: spacing.spacing16,
    },
    templateContainer: {
        backgroundColor: colors.white,
        marginTop: spacing.spacing20,
    },
    addressContainer: {
        backgroundColor: colors.white,
        flexDirection: "row",
    },
    imgContainer: {
        flex: spacing.spacing1,
        marginTop: spacing.spacing20,
        marginRight: spacing.spacing20,
        marginLeft: spacing.spacing20,
        alignSelf: 'stretch',
        flexDirection: "row",
    },
    radioButtonContainer: {
        flexDirection: "row",
        marginLeft: spacing.spacing16,
        marginTop: spacing.spacing12,
        marginRight: spacing.spacing20,
    },
    stepTextStyle: {
        ...TextStyle.text_14_normal,
        marginTop: spacing.spacing8
    },
    txtTopHeaderTitle: {
        flex: spacing.spacing1,
        lineHeight: spacing.spacing20,
        fontWeight: "bold",
        fontSize: fontSizes.fontSize16,
        fontFamily: "Lato",
        color: colors.charcoalGrey,
        paddingTop: spacing.spacing8
    },
    primaryButton: {
        marginLeft: spacing.spacing20,
        marginRight: spacing.spacing20,
        marginBottom: spacing.spacing20,
        justifyContent: 'flex-end',
        alignSelf: 'stretch',
    },
    textInputContainer: {
        width: dimens.width90,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        marginBottom: spacing.spacing16
    },
    addressTxtInputContainer: {
        width: dimens.width90,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        marginBottom: spacing.spacing16
    },
    chatIconStyle: {
        width: 24,
        height: 24,
        marginTop: 4,
        resizeMode: "contain"
    },
    chatTextStyle: {
        marginLeft: spacing.spacing6,
        textAlign: 'center',
        color: colors.white
    }
});
