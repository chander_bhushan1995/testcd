import React from 'react';
import { BackHandler, NativeModules, Platform, SafeAreaView, Image, TouchableOpacity, ScrollView, Text, View } from 'react-native';
import Toast from "react-native-easy-toast";
import { HeaderBackButton } from "react-navigation";
import ButtonWithLoader from '../../../CommonComponents/ButtonWithLoader.js';
import RadioButton from '../../../Components/RadioButton';
import InputBox from '../../../Components/InputBox';
import colors from "../../../Constants/colors";
import { TextStyle, CommonStyle } from "../../../Constants/CommonStyle";
import PlatformActivityIndicator from "../../../CustomComponent/PlatformActivityIndicator";
import * as Actions from "../../Actions/ActionTypes"
import paymentFlowStrings from "../../Constants/Constants";
import { PLATFORM_OS } from "../../../Constants/AppConstants";
const nativeBrigeRef = NativeModules.ChatBridge;
import * as Validator from '../../../commonUtil/Validator';
import styles from './BasicDetails.component.style';
import * as FirebaseKeys from "../../../Constants/FirebaseConstants"
import * as webengageAttrKeys from '../../../Constants/WebengageAttrKeys';
import {
    logWebEnageEvent,
} from '../../../commonUtil/WebengageTrackingUtils';
import {SERVICEABLE } from '../../../Constants/WebengageAttributes';
import { parseJSON, deepCopy} from "../../../commonUtil/AppUtils";

let productCode = null;
let pinCode = ""
export default class BasicDetailsComponent extends React.Component {
    constructor(props) {
        super(props);
        this.index = 0;
        this.state = {
            selectedItem: 'Male',
        }
    }
    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: <View style={Platform.OS === PLATFORM_OS.ios ? { alignItems: "center" } : { alignItems: "flex-start" }}>
                <Text style={TextStyle.text_16_bold}>{paymentFlowStrings.paymentFlowComponentTitles.basicDetailsComponentTitle}</Text>
            </View>,
            headerLeft: <HeaderBackButton tintColor={colors.black}
                onPress={() => {
                    if (navigation.state.params.isFromMemTab) {
                        nativeBrigeRef.goBack();
                    } else {
                        navigation.pop();
                    }
                }} />,
            headerTitleStyle: { color: colors.white },
            headerTintColor: colors.white,
            headerRight:<TouchableOpacity style={{marginTop: 0, flexDirection: 'row', marginRight: 16}}
                              onPress={() => {
                                  nativeBrigeRef.openChat();
                              }}>
                <Image style={styles.chatIconStyle} source={require("../../../images/icon_chat_blue.webp")}/>
            </TouchableOpacity>,
        };
    };
    componentDidMount() {
        const { navigation } = this.props;
        if (this.props.customerDetails === null) {
            let activationCode = this.props.initialProps.postboarding_screen_act_code
            if (activationCode === undefined || activationCode === null) {
                activationCode = this.props.actCode;
            }
            if (activationCode !== null && activationCode !== undefined) {
                this.props.getTempCustInfo(activationCode)
            }
        }
        this.props.trackAppScreen(this.props.initialProps.service_Type);

        let service_TypeEW = navigation.getParam("firebaseKey", null); //TODO:- fix this
         productCode = navigation.getParam("productCode", null);

        if (this.props.initialProps.service_Type === "HS") {
            if (service_TypeEW !== null) {
                this.props.getPostBoardingFormFields(service_TypeEW);
            } else {
                this.props.getPostBoardingFormFields(FirebaseKeys.HomeAppliance);
            }
        } else {//PE, Wallet
            this.props.getPostBoardingFormFields(this.props.initialProps.service_Type);
        }
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        this.setState({ index: 0 });
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    changeActiveRadioButton(index) {
        this.props.radioItems.map((item) => {
            item.selected = false;
            item.color = colors.charcoalGrey;
        });
        this.props.radioItems[index].selected = true;
        this.props.radioItems[index].color = colors.blue;
        this.setState({ selectedItem: this.props.radioItems[index].label });
    }

    render() {


        if(productCode === null && (this.props.productCode === null || this.props.productCode === undefined) ){
            return (
                <View style={CommonStyle.loaderStyle}>
                  <PlatformActivityIndicator />
                </View>
              );
        }
        switch (this.props.type) {
            case Actions.submitPostDetailsFailure:
                this.refs.toast.show(paymentFlowStrings.paymentFlowErrorMessages.submitDetailsErrorMsg);
                break;
            case Actions.checkPincodeSuccess:
                this.sendPincodeServiceabilityEvent("YES");
                this.submitPostPaymentDetails();
                break;
            case Actions.checkPincodeFailure:
                this.sendPincodeServiceabilityEvent("NO");
                break;
            case Actions.submitPostDetailsSuccess:
                this.props.navigation.navigate('WAPaymentSuccessComponent', {
                    tempCustInfoData: this.props.customerDetails,
                });
                break;
            case Actions.fetchUserInfoSuccess:
                productCode = this.props.productCode

            break;
            default: break;
        }
        return (
            <SafeAreaView style={styles.SafeArea}>
                <ScrollView style={styles.scrollViewStyle}>
                    <View style={styles.container}>
                        <View style={styles.topHeaderContainer}>
                            {this.stepCount()}
                            <Text style={styles.txtTopHeaderTitle}>Please give us some basic details</Text>
                        </View>
                        <View style={styles.genderHeaderContainer}>
                            <Text style={styles.txtTopHeaderTitle}>Gender</Text>
                        </View>
                        <View style={styles.radioButtonContainer}>
                            {
                                this.props.radioItems.map((item, key) => (
                                    <RadioButton key={key} data={item} onClick={this.changeActiveRadioButton.bind(this, key)} />
                                ))
                            }
                        </View>
                    </View>
                    <View style={styles.templateContainer}>
                        {this.getFormFieldsTemplate()}
                    </View>
                </ScrollView>
                <View style={styles.primaryButton}>
                    <ButtonWithLoader
                        isLoading={this.props.isLoading}
                        enable={!this.props.isLoading}
                        Button={{
                            text: paymentFlowStrings.paymentFlowActionButtonTitle.activateNowButton,
                            onClick: () => {
                                let validationStatus = Validator.validateFormFields(this.props.displayedComponents);
                                if (validationStatus.isValid) {
                                    let pinCodeItem = this.getPincodeField()
                                    pinCode = pinCodeItem.inputValue
                                    this.props.checkPincode(pinCode)

                                }
                            }
                        }
                        } />
                </View>
                <Toast ref="toast" position="center" />
            </SafeAreaView>
        );
    }

    /// components
    stepCount = () => {
        let stageCount = this.props.navigation?.getParam("stageCount", null);
        if (stageCount === null || stageCount === undefined) return null;
        return  <Text style={styles.stepTextStyle}>STEP {stageCount} of {stageCount}</Text>

    };
    getFormFieldsTemplate = () => {
        let template = null;
        if (this.props.displayedComponents !== undefined) {
            template = this.props.displayedComponents.map((item) => {
                let title = item.title;
                let keyboardType = item.keyboardType;
                if(item.paramName === "deviceId" && productCode === "MP01"){
                    title = "IMEI";
                    keyboardType = "number-pad"
                }
                return (
                    <View style={styles.addressContainer} key={item.index} >
                        <InputBox containerStyle={styles.addressTxtInputContainer}
                            key={item.index}
                            multiline={item.isMultiLines}
                            height={item.height}
                            value={item.inputValue}
                            keyboardType={keyboardType}
                            inputHeading={title}
                            textAlignVertical={item.isMultiLines ? "top" : 'center'}
                            placeholder={item.placeHolder}
                            placeholderTextColor={colors.grey}
                            maxLength={item.maxLength}
                            errorMessage={item.errorMessage}
                            onChangeText={(text) => {
                                item.errorMessage = '';
                                this.props.validateInput(text, item.index);
                            }} />
                    </View>
                );
            });
        }
        return template;
    }

    submitPostPaymentDetails = () => {
        let customerDetailsInfo = {};
        let deviceId = "", model = "";
        let addressTmp = this.props.addressTmp;
        let customerDetailJSON = this.props.customerDetails;
        if (customerDetailJSON === null || customerDetailJSON === undefined){
            customerDetailJSON = this.props.refreshedTempCustomerDetails
        }
        if(customerDetailJSON === null || customerDetailJSON === undefined){
            return
        }
        let obj = customerDetailJSON["customerDetails"][0];
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                let val = obj[key];
                customerDetailsInfo[key] = val;
            }
        }
        this.props.displayedComponents.map((item) => {
            if (item.inputValue != '') {
                if (item.paramName === "deviceId") {
                    deviceId = item.inputValue;
                } else if (item.paramName === "model") {
                    model = item.inputValue;
                } else {
                    customerDetailsInfo[item.paramName] = item.inputValue;
                }
            }
        });

        customerDetailsInfo["gender"] = this.state.selectedItem;
        customerDetailsInfo["city"] = addressTmp["city"];
        customerDetailsInfo["cityName"] = addressTmp["cityName"];
        customerDetailsInfo["stateCode"] = addressTmp["stateCode"];
        customerDetailsInfo["stateName"] = addressTmp["stateName"];
        let assetInfo = [
            {
                "productCode": "" + this.props.productCode,
                "deviceId": deviceId,
                "model": model
            }
        ];

        if (this.props.assetInfo === null) {
            customerDetailsInfo["assetInfo"] = assetInfo;
        } else {
            customerDetailsInfo["assetInfo"] = [this.props.assetInfo];
        }

        let activationCode = this.props.initialProps.postboarding_screen_act_code
        if (activationCode === undefined || activationCode === null) {
            activationCode = this.props.actCode;
        }
        let postPaymentDetails = {
            "postPaymentDetails": {
                "activationCode": decodeURIComponent(activationCode),
                "customerDetails": [
                    customerDetailsInfo
                ],
            }
        };
        this.props.submitPostDetails(postPaymentDetails);
    }

    //send WebEngage Event
    sendPincodeServiceabilityEvent = (serviceable) => {
   // ---------- Web Engage -----------
        let eventDict = this.props.initialProps.eventDict;
        if(eventDict === null || eventDict === undefined){return}
        let eventJson =  parseJSON(this.props.initialProps.eventDict)
        let eventAttr = new Object();
        if(eventJson.type !== null && eventJson.type !== undefined){
            eventAttr[webengageAttrKeys.TYPE] = eventJson.type;
        }
        eventAttr[webengageAttrKeys.SERVICEABLE] = serviceable;
        eventAttr[webengageAttrKeys.PINCODE] = pinCode;
        logWebEnageEvent(SERVICEABLE, eventAttr);
    }

    getPincodeField = () => {
        let pincodeField = null;
        this.props.displayedComponents.map((item) => {
            if (item.paramName === "pinCode") {
                pincodeField = item;
            }
        });
        return pincodeField;
    }
    isFromMemTab = () => {
        return  this.props.initialProps.postboarding_screen_act_code !== undefined &&  this.props.initialProps.postboarding_screen_act_code !== null;
    }
    onBackPress = () => {
        if (this.isFromMemTab()) {
            nativeBrigeRef.goBack();
        } else {
            this.props.navigation.goBack(null);
            return true;
        }
    };
}
