import React, {Component} from "react";
import {
    BackHandler,
    Image,
    NativeModules,
    Platform,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from "react-native";

import CheckBox from "react-native-check-box";
import {HeaderBackButton} from "react-navigation";
import ButtonWithLoader from "../../CommonComponents/ButtonWithLoader";
import DialogView from "../../Components/DialogView";
import InputBox from "../../Components/InputBox";
import InputBoxDropDown from "../../Components/InputBoxDropDown";
import colors from "../../Constants/colors";
import {CommonStyle, TextStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import paymentFlowStrings from "../Constants/Constants";
import * as Actions from "../Actions/ActionTypes";
import PlatformActivityIndicator from "../../CustomComponent/PlatformActivityIndicator";
import {PLAN_STARTS_AFTER, PLATFORM_OS, TELL_US_MORE} from "../../Constants/AppConstants";
import BottomSheet from "react-native-bottomsheet";
import RBSheet from "../../Components/RBSheet";
import * as WebengageKeys from "../../Constants/WebengageAttrKeys";

import {
    logWebEnageEvent,
} from "../../commonUtil/WebengageTrackingUtils";
import * as FirebaseKeys from "../../Constants/FirebaseConstants";
import {APPLIANCE} from "../../Constants/WebengageAttrKeys";
import {COUNT} from "../../Constants/WebengageAttrKeys";
import {SERVICE_TYPE} from "../../Constants/WebengageAttrKeys";
import {COST} from "../../Constants/WebengageAttrKeys";
import {parseJSON, deepCopy, getActivityName} from "../../commonUtil/AppUtils";
import images from "../../images/index.image";
import LeftImageTextComponent from "../../BuyPlan/Components/LeftImageTextComponent";
import {localEventEmitter} from "../../Navigation/index.paymentflow";

const nativeBrigeRef = NativeModules.ChatBridge;
const secureDetailIcons = require('../../images/ic_lock.webp');
let _this;
let isEditFromPayment = false;

export default class CreateCustomerComponent extends Component {


    static navigationOptions = ({navigation}) => {
        const {params} = navigation.state;
        if (params?.isEditFromPayment) {
            return {
                headerTitle: <View
                    style={Platform.OS === PLATFORM_OS.ios ? {alignItems: "center"} : {alignItems: "flex-start"}}>
                    <Text
                        style={{
                            ...TextStyle.text_16_bold,
                            marginLeft: 24
                        }}>{paymentFlowStrings.paymentFlowComponentTitles.createCustomerComponentTitle}</Text>
                </View>,
                headerLeft:null,
                headerTitleStyle: {color: colors.white},
                headerTintColor: colors.white,
                headerRight: <TouchableOpacity style={{marginTop: 0, flexDirection: "row", marginRight: 16}}
                                               onPress={() => {
                                                   params.onBackPress()
                                               }}>
                    <Image style={styles.chatIconStyle} source={require("../../images/ic_blackClose.webp")}/>
                </TouchableOpacity>,
            };
        } else {
            return {
                headerTitle: <View
                    style={Platform.OS === PLATFORM_OS.ios ? {alignItems: "center"} : {alignItems: "flex-start"}}>
                    <Text
                        style={TextStyle.text_16_bold}>{paymentFlowStrings.paymentFlowComponentTitles.createCustomerComponentTitle}</Text>
                </View>,
                headerLeft: <HeaderBackButton tintColor={colors.black}
                                              onPress={() => {
                                                  params.onBackPress()

                                              }}/>,
                headerTitleStyle: {color: colors.white},
                headerTintColor: colors.white,
                headerRight: <TouchableOpacity style={{marginTop: 0, flexDirection: "row", marginRight: 16}}
                                               onPress={() => {
                                                   nativeBrigeRef.openChat();
                                               }}>
                    <Image style={styles.chatIconStyle} source={require("../../images/icon_chat_blue.webp")}/>
                </TouchableOpacity>,
            };
        }

    };

    constructor(props) {
        super(props);
        _this = this;
        this.state = {reRender: false}
    }

    componentDidMount() {
        localEventEmitter.addListener(paymentFlowStrings.events.eventEditFromPayment, params => {
            if (params?.isEditFromPayment) {
                isEditFromPayment = params?.isEditFromPayment
                _this.props.navigation.setParams({isEditFromPayment: params?.isEditFromPayment})
                _this.setState({reRender: !_this.state.reRender})
            }
        });
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        this.props.navigation.setParams({onBackPress: this.onBackPress});
        this.props.getFormFieldsJson();
        this.props.getTermsAndConditionJson(this.props.initialProps);
        if (this.props.initialProps.hasEW) {
            this.props.getWarrantyPeriod();
        }
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    onBackPress = () => {
        if (this.props.routeList && this.props.routeList.length > 1) {
            this.props.navigation.pop();
        } else if (isEditFromPayment) {
            this.navigateToPayment();
            this.props.navigation.setParams({isEditFromPayment: false})
            isEditFromPayment = false;

        } else
            nativeBrigeRef.goBack()
        return true
    };

    render() {
        switch (this.props.type) {
            case Actions.createCustomerInProgress:
                this.showActivityIndicator();
                break;
            case Actions.createCustomerFailure:
            case Actions.validateEmailFailure:
                this.showAlert(this.props.errorApiMessage);
                break;
            case Actions.validateEmailSuccess:
                this.navigateToScheduleVisitScreen();
                this.props.resetAction();
                break;
            case Actions.createCustomerSuccess:
                this.navigateToPayment();
                this.props.resetAction();
                break;
            case Actions.createCustomerSuccessTrial:
                this.navigateToPaymentSuccess();
                break;
            case Actions.validatePreboardingFormFields:
                this.onboardCustomer();
                break;
            case Actions.getFormFieldsJson:
                let initialProps = {...this.props.initialProps};
                if (this.props.userDetailsString) {
                    let firstName = this.props.userDetailsString?.firstName;
                    let emailId = this.props.userDetailsString?.emailId;
                    let mobileNumber = this.props.userDetailsString?.mobileNumber;
                    let initialPropsData = initialProps.data;
                    let data = parseJSON(initialPropsData);
                    if (data !== null && data !== undefined && data.hasOwnProperty('customerInfo')) {
                        let customerInfoTemp = data.customerInfo;
                        if (firstName) {
                            customerInfoTemp[0].firstName = firstName
                        }
                        if (emailId) {
                            customerInfoTemp[0].emailId = emailId
                        }
                        if (mobileNumber) {
                            customerInfoTemp[0].mobileNumber = mobileNumber
                        }

                        initialPropsData = data;
                        initialProps.data = JSON.stringify(initialPropsData)
                    }
                }
                this.props.getformFields(initialProps, this.props.formFieldsJson);
                break;
            default:
                break;
        }
        let warranties = [];
        if ((this.props.warranties !== null && this.props.warranties !== undefined)) {
            warranties = this.props.warranties;
        }
        return (
            <SafeAreaView style={styles.SafeArea}>
                <ScrollView style={styles.scrollViewStyle}>
                    <View style={{backgroundColor: colors.color_F3F2F7, paddingBottom: spacing.spacing12}}>
                        <LeftImageTextComponent data={{
                            image: secureDetailIcons,
                            isLocalImage: true,
                            paragraph: {
                                "fontSize": 12,
                                "value": paymentFlowStrings.secureDetailsText,
                                "isHTML": false
                            }
                        }}/>
                    </View>
                    {this.getFreeRewardHeader()}
                    {this.getWarrantyActionsheetComponent(warranties)}
                    {this.getIDFenceTermsActionsheetComponent(this.props.idfenceTerms)}
                    <View style={styles.container}>
                        {(this.props.initialProps.totalSteps && this.props.initialProps.currentStep) ?
                            <Text style={styles.stepText}>
                                {`STEP ${this.props.initialProps.currentStep} of ${this.props.initialProps.totalSteps}`}
                            </Text>
                            : null}
                        <Text style={styles.commonHeading}>{TELL_US_MORE}</Text>
                        {this.getFormFieldsTemplate()}
                        {/*{this.getCheckboxComponent()}*/}
                        {this.getAgreementCheckboxComponent()}
                    </View>
                </ScrollView>
                {this.getPrimaryButtonComponent()}
                <DialogView ref={ref => (this.DialogViewRef = ref)}/>
            </SafeAreaView>
        );
    }

    /// components
    getFormFieldsTemplate = () => {
        let template = <View/>;
        if (this.props.displayedComponents !== null && this.props.displayedComponents !== undefined) {
            template = this.props.displayedComponents.map((item) => {
                if (item.paramName === "warrantyPeriod") {
                    if (!this.props.warrantyRequired) {
                        return null;
                    }
                    return (
                        <View style={styles.addressContainer} key={item.index}>
                            <InputBoxDropDown containerStyle={styles.addressTxtInputContainer}
                                              key={item.index}
                                              isMultiLines={item.isMultiLines}
                                              value={item.inputValue}
                                              height={28}
                                              keyboardType={item.keyboardType}
                                              inputHeading={item.title}
                                              placeholder={item.placeholder}
                                              placeholderTextColor={colors.grey}
                                              maxLength={item.maxLength}
                                              errorMessage={item.errorMessage}
                                              onPress={() => {
                                                  this.EW_RBSheet.openWithIndex(item.index);
                                                  item.errorMessage = "";
                                              }}
                                              onChangeText={(text) => {
                                                  item.errorMessage = "";
                                                  this.props.validatePreBoardFormInput(text, item.index);
                                              }}
                                              fieldDescription={PLAN_STARTS_AFTER}
                            />
                        </View>
                    );
                }
                return (
                    <View style={styles.addressContainer} key={item.index}>
                        <InputBox containerStyle={styles.addressTxtInputContainer}
                                  key={item.index}
                                  isMultiLines={item.isMultiLines}
                                  value={item.inputValue}
                                  height={item.height}
                                  keyboardType={item.keyboardType}
                                  inputHeading={item.title}
                                  placeholder={item.placeholder}
                                  placeholderTextColor={colors.grey}
                                  maxLength={item.maxLength}
                                  errorMessage={item.errorMessage}
                                  editable={item.editable}
                                  onChangeText={(text) => {
                                      item.errorMessage = "";
                                      this.props.validatePreBoardFormInput(text, item.index);
                                  }}/>
                    </View>
                );
            });

        }
        return template;
    };

    // TODO:- REMOVE CODE IN FURTURE
    // getCheckboxComponent = () => {
    //     return (
    //         <View style={styles.tncViewStyle}>
    //             <CheckBox
    //                 style={styles.checkboxStyle}
    //                 checkedImage={<Image source={require('../../images/ic_check_box.webp')}
    //                                      style={styles.checkboxStyle}/>}
    //                 unCheckedImage={<Image source={require('../../images/ic_check_box_outline_blank.webp')}
    //                                        style={styles.checkboxStyle}/>}
    //                 isChecked={this.props.checkboxChecked}
    //                 onClick={() => {
    //                     this.props.toggleCheckbox();
    //                 }
    //                 }
    //             />
    //             <Text>
    //                 <Text style={[TextStyle.text_12_bold]} onPress={() => {
    //                     this.props.toggleCheckbox();
    //                 }}>{paymentFlowStrings.prePaymentFormStrings.checkAge} </Text>
    //                 <Text style={[TextStyle.text_12_normal, styles.tncTextStyle]} onPress={() => {
    //                     this.navigateToTnCScreen();
    //                 }}>{paymentFlowStrings.prePaymentFormStrings.tnc}</Text>
    //             </Text>
    //         </View>
    //     );
    // };


    getAgreementCheckboxComponent = () => {
        if (!this.hasCreditScore() && !this.props.initialProps.isTrialPlan) {
            return null;
        }
        return (
            <View style={styles.agreeTermsViewStyle}>
                <CheckBox
                    style={styles.checkboxStyle}
                    checkedImage={<Image source={require("../../images/ic_check_box.webp")}
                                         style={styles.checkboxStyle}/>}
                    unCheckedImage={<Image source={require("../../images/ic_check_box_outline_blank.webp")}
                                           style={styles.checkboxStyle}/>}
                    isChecked={this.props.agreedFDTerms}
                    onClick={() => {
                        this.props.toggleFDTermsCheckbox();
                    }
                    }
                />
                <Text>
                    <Text style={[TextStyle.text_12_bold]} onPress={() => {
                        this.props.toggleFDTermsCheckbox();
                    }}>{this.props.idfenceSortTerm} </Text>
                    <Text style={[TextStyle.text_12_normal, styles.tncTextStyle]} onPress={() => {
                        this.openIDFenceTerms(this.props.idfenceTerms);
                    }}>{this.props.viewMoreTitle}</Text>
                </Text>
            </View>
        );
    };

    getWarrantyActionsheetComponent(warranties) {
        if (!this.props.initialProps.hasEW) {
            return null;
        }
        return (
            <RBSheet
                ref={ref => {
                    this.EW_RBSheet = ref;
                }}
                customStyles={{
                    container: {
                        flex: 1,
                        flexDirection: "column",
                        borderTopLeftRadius: 12,
                        borderTopRightRadius: 12,
                    },
                }}
                duration={10}
                height={spacing.spacing56 * warranties.length + spacing.spacing36}
                closeOnSwipeDown={true}
            >
                <View style={styles.warrantyContainer}>
                    <TouchableOpacity style={styles.crossBtnContainer} onPress={() => {
                        this.EW_RBSheet.close();
                    }}>
                        <Image
                            style={{height: 10, width: 10}}
                            source={images.ic_blackClose}/>
                    </TouchableOpacity>
                    <Text style={[TextStyle.text_18_bold, {
                        color: colors.charcoalGrey,
                        marginHorizontal: spacing.spacing20, marginBottom: spacing.spacing32,
                    }]}>{"Select your manufacturer warranty duration"}</Text>
                    {warranties.map(warranty => (
                        <View style={{height: spacing.spacing56}}>
                            <View style={CommonStyle.separator}></View>
                            <Text
                                style={([TextStyle.text_16_bold], styles.warrantyTextStyle)}
                                onPress={() => {
                                    this.props.validatePreBoardFormInput(
                                        warranty.paramCode,
                                        this.EW_RBSheet.index(),
                                    );
                                    this.EW_RBSheet.close();
                                }}
                            >
                                {warranty.paramName}
                            </Text>
                            <View style={CommonStyle.separator}></View>
                        </View>
                    ))}
                </View>
            </RBSheet>
        );
    }

    getIDFenceTermsActionsheetComponent(terms) {
        if (!terms) {
            return null;
        }
        return (
            <RBSheet
                ref={ref => {
                    this.RBSheet = ref;
                }}
                duration={10}
                height={450}
                closeOnSwipeDown={true}
            >
                <ScrollView style={styles.scrollViewStyle}>
                    <View style={styles.termsContainer}>
                        <TouchableOpacity onPress={() => {
                            this.RBSheet.close();
                        }}>
                            <Image source={require("../../images/icon_cross.webp")} style={styles.crossIconStyle}/>
                        </TouchableOpacity>
                        {terms.map((term, index) => (
                            <View key={index.toString()} style={styles.termsStyle}>
                                <Text>{"\u2022"}</Text>
                                <Text style={([TextStyle.text_16_bold], styles.termsTextStyle)}
                                      onPress={() => {
                                      }}>
                                    {term}
                                </Text>
                            </View>
                        ))}
                    </View>
                </ScrollView>
            </RBSheet>
        );
    }

    getPrimaryButtonComponent = () => {
        let buttonTitle = paymentFlowStrings.prePaymentFormStrings.action;
        if (isEditFromPayment) {
            buttonTitle = paymentFlowStrings.prePaymentFormStrings.saveDetail;
        }
        if (this.props.initialProps.isTrialPlan) {
            buttonTitle = paymentFlowStrings.prePaymentFormStrings.continue;
        } else if (this.props.initialProps.service_Type === FirebaseKeys.SOD) {
            buttonTitle = paymentFlowStrings.prePaymentFormStrings.ProceedToScheduleVisit;
        }
        return (
            <>
                <View style={styles.ageTandCView}>
                    <Text
                        style={[TextStyle.text_12_normal]}>{paymentFlowStrings.prePaymentFormStrings.age18TandC}
                        <Text style={[TextStyle.text_12_normal, styles.tncTextStyle]} onPress={() => {
                            this.navigateToTnCScreen();
                        }}>
                            {paymentFlowStrings.prePaymentFormStrings.tnc + "."}
                        </Text>
                    </Text>
                </View>
                <View style={styles.primaryButtonContainer}>
                    <ButtonWithLoader
                        isLoading={!this.props.primaryButtonEnabled}
                        enable={(!this.hasCreditScore() && !this.props.initialProps.isTrialPlan) ? this.props.primaryButtonEnabled : this.props.agreedFDTerms}
                        Button={{
                            text: buttonTitle,
                            onClick: () => {
                                if (this.props.initialProps.service_Type === FirebaseKeys.SOD) {
                                    this.sendSODWebengageEvent();
                                } else {
                                    this.sendRegisterEvent();
                                }
                                this.props.validatePreboardingFormFields();
                            },
                        }}
                    />
                </View>
            </>
        );
    };
    getFreeRewardHeader = () => {
        if (this.props.initialProps.isFreeReward) {
            return (
                <View style={styles.freeHeaderContainerStyle}>
                    <Text style={styles.freeHeaderTitleStyle}>{this.props.trialHeaderData.title}
                    </Text>
                    <Text style={styles.freeHeaderDescriptionStyle}>{this.props.trialHeaderData.description}</Text>
                </View>
            );
        }
        return null;
    };

    //methods
    dropDownClicked(index) {
        if (this.props.warranties === undefined) {
            return;
        }
        let wp = [], i;
        for (i = 0; i < this.props.warranties.length; i++) {
            let pcode = this.props.warranties[i].paramCode;
            wp.push(pcode);
        }
        BottomSheet.showBottomSheetWithOptions(
            {
                options: wp,
                dark: false,
                cancelButtonIndex: wp.length,
            },
            value => {
                this.props.validatePreBoardFormInput(wp[value], index);

            },
        );
    };

    //send Event
    sendRegisterEvent = () => {
        // ---------- Web Engage -----------
        let eventJson = this.getEventDictJson();
        if (eventJson === null || eventJson === undefined) {
            return;
        }
        let eventAttr = new Object();

        if (eventJson.TYPE !== null && eventJson.TYPE !== undefined) {
            eventAttr[WebengageKeys.LOCATION] = eventJson.TYPE;
        }
        if (eventJson.COVER_AMOUNT !== null && eventJson.COVER_AMOUNT !== undefined && eventJson.COVER_AMOUNT !== 0) {
            try {
                eventAttr[WebengageKeys.COVER_AMOUNT] = parseInt(eventJson.COVER_AMOUNT);
            } catch (e) {
                eventAttr[WebengageKeys.COVER_AMOUNT] = eventJson.COVER_AMOUNT;
            }
        }
        if (eventJson.PREMIUM_AMOUNT !== null && eventJson.PREMIUM_AMOUNT !== undefined && eventJson.PREMIUM_AMOUNT !== 0) {
            try {
                eventAttr[WebengageKeys.PREMIUM_AMOUNT] = parseInt(eventJson.PREMIUM_AMOUNT);
            } catch (e) {
                eventAttr[WebengageKeys.PREMIUM_AMOUNT] = eventJson.PREMIUM_AMOUNT;
            }
        }
        if (eventJson.SERVICE !== null && eventJson.SERVICE !== undefined) {
            eventAttr[WebengageKeys.SERVICE] = eventJson.SERVICE;
        }
        if (eventJson.ADDON_SERVICE !== null && eventJson.ADDON_SERVICE !== undefined) {
            eventAttr[WebengageKeys.ADDON_SERVICE] = eventJson.ADDON_SERVICE;
        }
        if (eventJson.PLAN !== null && eventJson.PLAN !== undefined) {
            eventAttr[WebengageKeys.PLAN] = eventJson.PLAN;
        }
        if (eventJson.PLAN_CODE !== null && eventJson.PLAN_CODE !== undefined) {
            eventAttr[WebengageKeys.PLAN_CODE] = "" + eventJson.PLAN_CODE;
        }
        eventAttr[WebengageKeys.DISCOUNT] = eventJson.DISCOUNT;
        eventAttr[WebengageKeys.PRODUCT] = eventJson.PRODUCT;
        eventAttr[WebengageKeys.NO_OF_APPLIANCE] = eventJson.NO_OF_APPLIANCES;

        if (eventJson.FINAL_PREMIUM_AMOUNT !== null && eventJson.FINAL_PREMIUM_AMOUNT !== undefined && eventJson.FINAL_PREMIUM_AMOUNT !== 0) {
            try {
                eventAttr[WebengageKeys.FINAL_PREMIUM_AMOUNT] = parseInt(eventJson.FINAL_PREMIUM_AMOUNT);
            } catch (e) {
                eventAttr[WebengageKeys.FINAL_PREMIUM_AMOUNT] = eventJson.FINAL_PREMIUM_AMOUNT;
            }
        }
        logWebEnageEvent(WebengageKeys.PROCEED_PAYMENT, eventAttr);
    };

    //send SOD Event
    sendSODWebengageEvent = () => {
        let eventJson = this.getEventDictJson();
        if (eventJson === null || eventJson === undefined) {
            return;
        }
        let eventAttr = new Object();
        eventAttr[APPLIANCE] = eventJson?.Appliance;
        eventAttr[COUNT] = eventJson?.Count;
        eventAttr[SERVICE_TYPE] = eventJson?.ServiceType;
        eventAttr[COST] = eventJson?.Cost;
        logWebEnageEvent(WebengageKeys.SOD_DETAILS_SUBMIT, eventAttr);
        nativeBrigeRef.sendSODDetailsSubmitEvent(eventJson?.Appliance, eventJson?.Count, eventJson?.ServiceType, eventJson?.Cost);
    };

    onboardCustomer = () => {
        if (this.props.formFieldsValid) {
            if (this.hasCreditScore() && !this.props.agreedFDTerms) {
                this.showAlert(paymentFlowStrings.prePaymentFormStrings.alertForIDFencePlanAgreement);
                return;
            }
            if (this.props.checkboxChecked) {
                let data = this.props.initialProps.data;
                let dataJson = parseJSON(data);
                if (this.props.initialProps.service_Type === FirebaseKeys.SOD) {
                    if (this.props.callValidateEmail) {
                        this.props.validateEmail(this.props.userDetailsString, this.props.initialProps);
                    } else {
                        this.navigateToScheduleVisitScreen();
                    }
                } else if (this.props.initialProps.isTrialPlan) {
                    if ((dataJson?.paymentInfo?.paymentOptions ?? []).length > 0) { // if idfence plan is trail plan and payment options are not empty then make payment
                        this.props.createCustomer(this.props.userDetailsString, this.props.initialProps);
                        return;
                    }
                    this.props.createTrialCustomer(this.props.userDetailsString, this.props.initialProps);
                } else {
                    this.props.createCustomer(this.props.userDetailsString, this.props.initialProps);
                }
            } else {
                this.showAlert(paymentFlowStrings.prePaymentFormStrings.alert);
            }
        }
        this.props.resetAction();
    };

    hasCreditScore = () => {
        return this.props.initialProps.hasCreditScore === true;
    };
    /// Navigation


    navigateToTnCScreen = () => {
        this.props.navigation.navigate("TnCWebViewComponent", {
            productTermAndCUrl: this.props.productTermAndCUrl,
            otherParam: "T&C",
        });
    };

    openIDFenceTerms(terms) {
        this.RBSheet.open();
    }

    navigateToScheduleVisitScreen = () => {
        this.props.navigation.navigate("ScheduleVisitComponent", {
            initialProps: this.props.initialProps,
            isPaymentFlow: true,
            userDetails: this.props.userDetailsString,
            fromCreateCustomerScreen: true,
        });
    };

    navigateToPayment = () => {
        let eventJson = this.getEventDictJson();
        let data = this.props.initialProps.data;
        let dataJson = parseJSON(data);
        let subCategory = eventJson?.TYPE;
        if (dataJson?.customerInfo[0]?.assetInfo && dataJson?.customerInfo[0]?.assetInfo[0]) {
            subCategory = dataJson?.customerInfo[0]?.assetInfo[0]?.productCode;
        }
        this.props.navigation.navigate("PaymentWebViewComponent", {
            paymentLink: this.props.paynowlink,
            otherParam: "Payment Review",
            customerOrderInfo: this.props.customerOrderInfo,
            isHA_SOP: this.props.initialProps?.isHA_SOP,
            initialProps: this.props.initialProps,
            isSkipCustomerDetail: false,
            shouldCallOnBoardApi: false,
            shouldReloadWebView: true,
            shouldEditCustomerDetails: true,
            eventInfo: {
                activity: getActivityName(this.props.initialProps?.isRenewal),
                netAmount: eventJson?.PREMIUM_AMOUNT,
                product: eventJson?.TYPE,
                category: this.props.initialProps?.service_Type,
                price: eventJson?.PREMIUM_AMOUNT,
                planCode: dataJson?.orderInfo?.planCode,
                serviceType: eventJson?.SERVICE,
                subCategory: subCategory,
                planName: eventJson?.PLAN,
                productService: eventJson?.ProductService,
                coverAmount: eventJson.COVER_AMOUNT,
                finalPremiumAmount: eventJson.FINAL_PREMIUM_AMOUNT,
                discount: eventJson.DISCOUNT,
                noOfApplications: eventJson.NO_OF_APPLIANCES,
                addOnService: eventJson.ADDON_SERVICE,
                plan: eventJson.PLAN,
            },
        });
        localEventEmitter.emit(paymentFlowStrings.events.eventWebviewReloadAndBack, {
            isSkipCustomerDetail: false,
            shouldReloadWebView: true
        })
        isEditFromPayment = false;
        this.props.navigation.setParams({isEditFromPayment: false})
    };

    getEventDictJson() {
        let eventDict = this.props.initialProps.eventDict;
        if (eventDict === null || eventDict === undefined) {
            return null;
        }
        let eventJson = parseJSON(eventDict);
        return eventJson;
    }

    navigateToPaymentSuccess() {
        this.props.navigation.navigate("WAPaymentSuccessComponent", {
            tempCustInfoData: this.props.customerInfo,
            title: paymentFlowStrings.activationSuccessTitle.activatedTrial,
        });
    }

    showAlert(alertMessage) {
        if (this.DialogViewRef !== null && this.DialogViewRef !== undefined) {
            this.DialogViewRef.showDailog({
                title: "Alert!",
                message: alertMessage,
                primaryButton: {
                    text: "OK", onClick: () => {
                    },
                },
                cancelable: true,
                onClose: () => {
                },
            });
        }
    }

    showActivityIndicator() {
        return (
            <SafeAreaView style={styles.SafeArea}>
                <View style={CommonStyle.loaderStyle}>
                    <PlatformActivityIndicator/>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    SafeArea: {
        flex: spacing.spacing1,
        backgroundColor: colors.white,
    },
    container: {
        flex: spacing.spacing1,
        backgroundColor: colors.white,
        paddingTop: spacing.spacing32,
    },
    formFieldscontainer: {
        flexDirection: "column",
        flex: spacing.spacing1,
    },
    ageTandCView: {
        marginHorizontal: spacing.spacing16,
    },
    primaryButtonContainer: {
        margin: spacing.spacing16,
        justifyContent: "flex-end",
        alignSelf: "stretch",
    },
    addressTxtInputContainer: {
        width: "90%",
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        marginBottom: spacing.spacing16,
    },
    tncViewStyle: {
        flexDirection: "row",
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
    },
    termsContainer: {
        padding: spacing.spacing16,
    },
    agreeTermsViewStyle: {
        flexDirection: "row",
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing32,
        marginTop: spacing.spacing16,
    },
    termsStyle: {
        flexDirection: "row",
        paddingTop: spacing.spacing16,
    },
    crossIconStyle: {
        height: spacing.spacing16,
        width: spacing.spacing16,
        resizeMode: "contain",
        alignSelf: "flex-end",
        justifyContent: "flex-end",
    },
    scrollViewStyle: {
        flex: spacing.spacing1,
        paddingBottom: spacing.spacing32,
        backgroundColor: colors.white,
    },
    checkboxStyle: {
        width: spacing.spacing20,
        height: spacing.spacing20,
        marginRight: spacing.spacing4,
    },
    tncTextStyle: {
        marginLeft: spacing.spacing4,
        marginRight: spacing.spacing4,
        color: colors.blue028,
    },
    addressContainer: {
        backgroundColor: colors.white,
        flexDirection: "row",
    },
    warrantyListStyle: {
        padding: spacing.spacing8,
        margin: spacing.spacing8,
        borderColor: colors.grey,
        borderWidth: spacing.spacing1,
    },
    warrantyTextStyle: {
        paddingHorizontal: spacing.spacing20,
        paddingVertical: spacing.spacing16,
        flex: 1,
    },
    termsTextStyle: {
        flex: 1,
        paddingLeft: 5,
    },
    warrantyContainer: {
        marginTop: spacing.spacing8,
    },
    loaderContainer: {
        justifyContent: "center",
        flex: spacing.spacing1,
    },
    chatIconStyle: {
        width: 24,
        height: 24,
        marginTop: 4,
        resizeMode: "contain",
    },
    chatTextStyle: {
        marginLeft: spacing.spacing6,
        textAlign: "center",
        color: colors.white,
    },
    freeHeaderContainerStyle: {
        backgroundColor: colors.color_0282F007,
        padding: spacing.spacing16,

    },
    freeHeaderTitleStyle: {
        marginBottom: spacing.spacing4,
        ...TextStyle.text_14_bold,
    },
    freeHeaderDescriptionStyle: {
        ...TextStyle.text_12_normal,
        color: colors.grey,
    },
    crossBtnContainer: {
        margin: spacing.spacing12,
        backgroundColor: colors.greyF1F2F3,
        height: 24,
        width: 24,
        borderRadius: 12,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "flex-end",
    },
    commonHeading: {
        ...TextStyle.text_16_bold,
        color: colors.charcoalGrey,
        marginBottom: spacing.spacing24,
        marginHorizontal: spacing.spacing16,
    },
    stepText: {
        ...TextStyle.text_10_bold,
        color: colors.color_848F99,
        marginHorizontal: spacing.spacing16,
    },
});
