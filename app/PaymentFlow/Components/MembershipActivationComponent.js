import React, { Component } from "react";
import {
  Platform,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  NativeModules,
  TouchableOpacity,
  Image,
  BackHandler
} from "react-native";
import ButtonWithLoader from "../../CommonComponents/ButtonWithLoader.js";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import paymentFlowStrings from "../Constants/Constants";
import * as FirebaseKeys from "../../Constants/FirebaseConstants"
import PlatformActivityIndicator from '../../CustomComponent/PlatformActivityIndicator';
import { CommonStyle, TextStyle } from '../../Constants/CommonStyle';
import { PLATFORM_OS } from "../../Constants/AppConstants";
import { HeaderBackButton } from "react-navigation";
const nativeBrigeRef = NativeModules.ChatBridge;

export default class MembershipActivationComponent extends Component {
  constructor(props) {
    super(props);
  }
  /// navigation options
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: (
        <View
          style={
            Platform.OS === PLATFORM_OS.ios
              ? { alignItems: "center" }
              : { alignItems: "flex-start" }
          }
        >
          <Text style={TextStyle.text_16_bold}>
            {
              paymentFlowStrings.paymentFlowComponentTitles
                .membershipActivationTitle
            }
          </Text>
        </View>
      ),
      headerLeft: (
        <HeaderBackButton
          tintColor={colors.black}
          onPress={() => {
            if (navigation.state.params.isFromMemTab) {
              nativeBrigeRef.goBack();
            } else {
              navigation.goBack(null);
            }

          }}
        />
      ),
      headerTitleStyle: { color: colors.white },
      headerTintColor: colors.white,
      headerRight:<TouchableOpacity style={{marginTop: 0, flexDirection: 'row', marginRight: 16}}
                        onPress={() => {
                            nativeBrigeRef.openChat();
                        }}>
          <Image style={styles.chatIconStyle} source={require("../../images/icon_chat_blue.webp")}/>
      </TouchableOpacity>,
    };
  };
  componentDidMount() {
    if (this.props.customerDetails === null) {
      let activationCode = this.props.initialProps.postboarding_screen_act_code
      if (activationCode === undefined || activationCode === null) {
          activationCode = this.props.actCode;
      }
      if (activationCode !== null && activationCode !== undefined) {
          this.props.getTempCustInfo(activationCode)
      }
  }

    BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
  }
  /// render
  render() {
    let tempCustInfoData = this.props.customerDetails; //from previos screen
    if (tempCustInfoData === null || tempCustInfoData === undefined) {
      tempCustInfoData = this.props.tempCustInfoData;  //refreshed data
    }
    if (tempCustInfoData === null || tempCustInfoData === undefined) {
      return (
        <View style={CommonStyle.loaderStyle}>
          <PlatformActivityIndicator />
        </View>
      );
    }
    let customerInfo = tempCustInfoData["customerDetails"][0];
    if (this.props.applianceName === null || this.props.applianceName === undefined || this.props.applianceName === ""){
      this.props.getApplianceName(customerInfo["productCode"][0])
      return (
        <View style={CommonStyle.loaderStyle}>
          <PlatformActivityIndicator />
        </View>
      );
    }
    let primaryButton = (
      <View style={styles.primaryButtonContainer}>
        <ButtonWithLoader
          isLoading={false} //{!this.state.primaryButtonEnabled}
          enable={true} //{this.state.primaryButtonEnabled}
          Button={{
            text: paymentFlowStrings.paymentFlowActionButtonTitle.letsbegin,
            onClick: () => {
              this.navigateToApplianceDetails(tempCustInfoData);
            }
          }}
        />
      </View>
    );
    return (
      <SafeAreaView style={styles.SafeArea}>
        <ScrollView style={styles.scrollViewStyle}>
          <View style={styles.rootContainerStyle}>
            <View style={styles.profileIconContainerStyle}>
              <Image
                source={require("../../../app/images/online_support.webp")}
              />
            </View>

            <View style={styles.nameAndDesctiptionContainerStyle}>
              <Text style={styles.nameAndDesctiptionTextStyle}>
                Hi {customerInfo["firstName"]}!
              </Text>
              <Text style={styles.desctiptionTextStyle}>
                Follow these steps to activate{" "}
                <Text style={styles.nameAndDesctiptionTextStyle}>
                  {customerInfo["planName"]}
                </Text>{" "}
                for your{" "}
                <Text style={styles.nameAndDesctiptionTextStyle}>
                  {this.props.applianceName}.
                </Text>
              </Text>
            </View>

            <View style={styles.activationStageContainerStyle}>
              <View style={styles.stepCounterContainerStyle}>
                {this.getStageCounterComponent(1)}
              </View>
              <View style={styles.nameAndDesctiptionContainerStyle}>
                <Text style={styles.nameAndDesctiptionTextStyle}>
                  Enter Appliance details
                </Text>
                <Text style={styles.desctiptionTextStyle}>
                  Brand, Model, Serial No. and Warranty Period.
                </Text>
              </View>
            </View>

            <View style={styles.activationStageContainerStyle}>
              <View style={styles.stepCounterContainerStyle}>
                {this.getStageCounterComponent(2)}
              </View>
              <View style={styles.nameAndDesctiptionContainerStyle}>
                <Text style={styles.nameAndDesctiptionTextStyle}>
                  Enter Basic Details
                </Text>
                <Text style={styles.desctiptionTextStyle}>
                  Pincode, Address and Upload Invoice.
                </Text>
              </View>
            </View>
          </View>
        </ScrollView>
        {primaryButton}
      </SafeAreaView>
    );
  }

  getStageCounterComponent = (count) => {
    return (
      <Text style={[TextStyle.text_14_bold, styles.stageNumberStyleInProgress]}>
        {count}
      </Text>
    )
  }

  ///Navigation
  navigateToApplianceDetails = (tempCustInfoData) => {
    this.props.navigation.navigate("ApplianceDetailsComponent", {
      otherParam: "Appliance Details",
      tempCustInfoData: tempCustInfoData,
    });
  };
  navigateToBasicDetails = () => {
    this.props.navigation.navigate('BasicDetailsComponent', {
      service_Type_ew: FirebaseKeys.HS_EW_BD,
    });
  };

  onBackPress = () => {
    if (this.isFromMemTab()) {
      nativeBrigeRef.goBack();
    } else {
      this.props.navigation.goBack(null);
      return true;
    }
  };
  isFromMemTab = () => {
    return this.props.initialProps.postboarding_screen_act_code !== undefined && this.props.initialProps.postboarding_screen_act_code !== null;
  }
}

/// styles
const styles = StyleSheet.create({
  SafeArea: {
    flex: spacing.spacing1,
    backgroundColor: colors.white
  },
  rootContainerStyle: {
    flex: spacing.spacing1,
    backgroundColor: colors.white,
    paddingTop: spacing.spacing20
  },
  profileIconContainerStyle: {
    flex: spacing.spacing1,
    backgroundColor: colors.white,
    alignItems: "center"
  },
  stepCounterContainerStyle: {},
  activationStageContainerStyle: {
    flexDirection: "row",
    paddingTop: spacing.spacing16,
    paddingLeft: spacing.spacing16
  },
  stepCounterStyle: {
    width: "100%",
    height: undefined,
    aspectRatio: 1
  },
  stepImageStyle: {
    width: spacing.spacing24,
    height: spacing.spacing24
  },
  nameAndDesctiptionContainerStyle: {
    flex: spacing.spacing1,
    backgroundColor: colors.white,
    paddingTop: spacing.spacing0,
    paddingLeft: spacing.spacing16
  },
  nameAndDesctiptionTextStyle: {
    ...TextStyle.text_14_bold,
    marginTop: spacing.spacing0
  },
  desctiptionTextStyle: {
    ...TextStyle.text_12_normal,
    marginTop: spacing.spacing8
  },
  primaryButtonContainer: {
    marginTop: spacing.spacing16,
    marginLeft: spacing.spacing16,
    marginRight: spacing.spacing16,
    marginBottom: spacing.spacing16,
    justifyContent: "flex-end",
    alignSelf: "stretch"
  },

  scrollViewStyle: {
    flex: spacing.spacing1,
    paddingBottom: spacing.spacing32,
    backgroundColor: colors.white
  },
  profileIcon: {
    alignItems: "center"
  },
  stageNumberStyleInProgress: {
    width: spacing.spacing24,
    height: spacing.spacing24,
    borderRadius: 24 / 2,
    backgroundColor: colors.blue,
    color: colors.white,
    textAlign: "center",
    overflow: "hidden"
  },
  stageNumberStylePending: {
    width: spacing.spacing24,
    height: spacing.spacing24,
    borderRadius: 24 / 2,
    backgroundColor: colors.white,
    borderWidth: 1,
    borderColor: colors.lightGrey,
    color: colors.lightGrey,
    textAlign: "center",
    overflow: "hidden"
  },
  chatIconStyle: {
      width: 24,
      height: 24,
      marginTop: 4,
      resizeMode: "contain"
  },
  chatTextStyle: {
      marginLeft: spacing.spacing6,
      textAlign: 'center',
      color: colors.white
  }
});
