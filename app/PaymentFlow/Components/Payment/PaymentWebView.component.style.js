import { StyleSheet } from 'react-native';
import spacing from "../../../Constants/Spacing";
import dimens from '../../../Constants/Dimens';
import colors from '../../../Constants/colors';
import { TextStyle } from "../../../Constants/CommonStyle";
export default StyleSheet.create({
    SafeArea: {
        flex: spacing.spacing1,
        width: dimens.width100,
        height: dimens.height100,
        flexDirection: 'column',
        backgroundColor: colors.white
    },
    textStyle: {
        flex: spacing.spacing1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: spacing.spacing20
    },
    imgStyle: {
        width: dimens.dimen16,
        height: dimens.dimen16,
        flex: spacing.spacing1,
        marginTop: spacing.spacing1
    },
    container: {
        flex: spacing.spacing1,
        backgroundColor: colors.white,
        paddingTop: spacing.spacing32
    },
    scrollViewStyle: {
        backgroundColor: colors.white,
        flex: spacing.spacing1,
        width: dimens.width100,
        position: 'absolute',
        top: spacing.spacing0,
        bottom: spacing.spacing0,
        left: spacing.spacing0,
        right: spacing.spacing0,
        alignSelf: 'stretch',
        height: dimens.height80,
        flexDirection: 'column'
    },
    WebViewStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: spacing.spacing1
    },
    ActivityIndicatorStyle: {
        position: 'absolute',
        left: spacing.spacing0,
        right: spacing.spacing0,
        top: spacing.spacing0,
        bottom: spacing.spacing0,
        alignItems: 'center',
        justifyContent: 'center'

    },
    primaryButtonContainer: {
        margin: spacing.spacing16,
        flex: spacing.spacing1,
        justifyContent: 'flex-end',
        alignSelf: 'stretch'
    },
    text_12_margin: {
        ...TextStyle.text_12_normal,
        marginTop: spacing.spacing14,
    },
    loaderContainer: {
        justifyContent: 'center',
        flex: spacing.spacing1
    }
});
