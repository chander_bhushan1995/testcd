import React from "react";
import {
    BackHandler,
    Image,
    Platform,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    View,
    NativeModules,
} from "react-native";
import WebView from "react-native-webview";
import ButtonWithLoader from "../../../CommonComponents/ButtonWithLoader";
import colors from "../../../Constants/colors";
import {TextStyle} from "../../../Constants/CommonStyle";
import Loader from "../../../Components/Loader";
import {HeaderBackButton} from "react-navigation";
import paymentFlowStrings from "../../Constants/Constants";
import {getActivityName, parseJSON, parseQueryParamsFromUrl} from "../../../commonUtil/AppUtils";
import {APIErrorCodes, CURRENCY, ID_FENCE_BLOG, PLATFORM_OS} from "../../../Constants/AppConstants";
import styles from "./PaymentWebView.component.style";
import {logAppsFlyerEvent, logWebEnageEvent} from "../../../commonUtil/WebengageTrackingUtils";
import {Events} from "../../../CardManagement/Analytics";
import {AF_PURCHASE} from "../../../Constants/WebengageEvents";
import * as Store from "../../../commonUtil/Store";
import * as PaymentApi from "../../DataLayer/PaymentAPIHelper";
import * as Actions from "../../Actions/ActionTypes";
import PaymentFlow, {localEventEmitter} from "../../../Navigation/index.paymentflow";
import * as FirebaseKeys from "../../../Constants/FirebaseConstants";
import * as WebengageKeys from "../../../Constants/WebengageAttrKeys";
import spacing from "../../../Constants/Spacing";

const relnoQueryParamKey = "relno";
let navigateToNextScreen = true;
const nativeBrigeRef = NativeModules.ChatBridge;
let screenNavigator;
let _this;
let isSkipCustomerDetail = false;
export default class PaymentWebViewComponent extends React.Component {
    static navigationOptions = ({navigation}) => {
        screenNavigator = navigation;
        return {
            headerTitle: <View
                style={Platform.OS === PLATFORM_OS.ios ? {alignItems: "center"} : {alignItems: "flex-start"}}>
                <Text
                    style={TextStyle.text_16_bold}>{paymentFlowStrings.paymentFlowComponentTitles.paymentWebViewComponentTitle}</Text>
            </View>,
            headerLeft: <HeaderBackButton tintColor={colors.black}
                                          onPress={() => {
                                              let initialProps = navigation.getParam("initialProps", null);
                                              if (initialProps !== null && initialProps !== undefined && initialProps.isRenewal === true) {
                                                  nativeBrigeRef.goBack();
                                              } else if (isSkipCustomerDetail) {
                                                  nativeBrigeRef.goBack();
                                              } else {
                                                  navigation.pop();
                                              }
                                          }}/>,
            headerTitleStyle: {color: colors.white},
            headerTintColor: colors.white,
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            isPaymentFailed: false,// to show retry view when payment is failed
            isRenewal: false,
            onBoardCustomerApiCalling: false,
            webViewKey: 1,
        };
        _this = this;
    }

    componentDidMount() {
        localEventEmitter.addListener(paymentFlowStrings.events.eventWebviewReloadAndBack, params => {
            if (params?.shouldReloadWebView) {
                if (this.webView)
                    this.webView.reload()
            }
        });
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        let initialProps = this.props.navigation.getParam("initialProps", null);
        isSkipCustomerDetail = this.props.navigation.getParam("isSkipCustomerDetail", false);


        if (this.props.navigation.getParam("shouldCallOnBoardApi", false)) {

            this.setState({onBoardCustomerApiCalling: true});
            let inputDetails = {
                firstName: initialProps?.user_Name,
                emailId: initialProps?.user_Email,
                mobileNumber: initialProps?.mobile_number
            }
            let initialDetails = initialProps.data;
            if (initialDetails && inputDetails) {
                PaymentApi.onboardCustomer(initialDetails, inputDetails, function callback(response, error) {
                    if (response) {
                        let eventJson = null
                        let eventDict = _this.props.navigation.getParam("initialProps", null).eventDict;
                        if (eventDict) {
                            eventJson = parseJSON(eventDict);
                        }
                        let data = initialProps?.data;
                        let dataJson = parseJSON(data);
                        let subCategory = eventJson?.TYPE;
                        if (dataJson?.customerInfo[0]?.assetInfo && dataJson?.customerInfo[0]?.assetInfo[0]) {
                            subCategory = dataJson?.customerInfo[0]?.assetInfo[0]?.productCode;
                        }
                        let createCustomerResponse = response;
                        let customerOrderInfo = {
                            customerDetails: [createCustomerResponse.customerOrderInfo],
                        };
                        let payNowLink = createCustomerResponse['payNowLink'];

                        _this.props.navigation.setParams({
                            paymentLink: payNowLink,
                            otherParam: "Payment Review",
                            customerOrderInfo: customerOrderInfo?.customerOrderInfo,
                            isHA_SOP: initialProps?.isHA_SOP,
                            eventInfo: {
                                activity: getActivityName(initialProps?.isRenewal),
                                netAmount: eventJson?.PREMIUM_AMOUNT,
                                coverAmount: eventJson?.COVER_AMOUNT,
                                finalPremiumAmount: eventJson?.FINAL_PREMIUM_AMOUNT,
                                discount: eventJson?.DISCOUNT,
                                noOfApplications: eventJson?.NO_OF_APPLIANCES,
                                addOnService: eventJson?.ADDON_SERVICE,
                                plan: eventJson?.PLAN,
                                product: eventJson?.TYPE,
                                category: initialProps?.service_Type,
                                price: eventJson?.PREMIUM_AMOUNT,
                                planCode: dataJson?.orderInfo?.planCode,
                                serviceType: eventJson?.SERVICE,
                                subCategory: subCategory,
                                planName: eventJson?.PLAN,
                                productService: eventJson?.ProductService,
                            },
                        });
                        let params = _this.props.navigation.getParam();
                        _this.setState({onBoardCustomerApiCalling: false});

                    } else {
                        if (error && error.errors && error.errors.length > 0) {
                            let errorCode = error?.errors[0]?.errorCode;
                            if (errorCode === APIErrorCodes.INVALID_PROMO_CODE) {
                                if (initialDetails && typeof initialDetails === "string") {
                                    let initialDetailsJSON = parseJSON(initialDetails);
                                    if (initialDetailsJSON?.orderInfo) {
                                        initialDetailsJSON.orderInfo["promoName"] = null;
                                        initialDetails = JSON.stringify(initialDetailsJSON);
                                    }
                                }
                                PaymentApi.onboardCustomer(initialDetails, inputDetails, callback);
                                return;
                            }
                        }
                        dispatch({
                            type: Actions.createCustomerFailure,
                            data: error,
                        });
                    }
                });
            }
        }
        if (initialProps !== null && initialProps !== undefined && initialProps.isRenewal === true) {
            this.setState({isRenewal: true});
        }
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    getActivityIndicatorLoadingView() {
        return (
            <Loader isLoading={true} loadingMessage={paymentFlowStrings.paymentLoaderMessage}
                    loaderStyle={{backgroundColor: colors.white, padding: spacing.spacing20}}/>
        );
    }

    onBackPress = () => {
        debugger;
        let initialProps = this.props.navigation.getParam("initialProps", null);
        if (initialProps !== null && initialProps !== undefined && initialProps.isRenewal === true) {
            nativeBrigeRef.goBack();
        } else if (isSkipCustomerDetail) {
            nativeBrigeRef.goBack();
        } else
            _this.props.navigation.pop();
        return true;
    };

    render() {
        if (this.state.onBoardCustomerApiCalling) {
            return this.getActivityIndicatorLoadingView()
        }
        (Platform.OS == PLATFORM_OS.android) ? nativeBrigeRef.refreshForLead(true) : null;
        if (this.state.isPaymentFailed) {
            return this.paymentRetryScreen();
        } else {
            return this.paymentWebView();
        }
    }

    paymentRetryScreen = () => {
        return (
            <SafeAreaView style={styles.SafeArea}>
                <ScrollView style={styles.scrollViewStyle}>
                    <View style={styles.textStyle}>
                        <Text style={TextStyle.text_12_normal}>Unfortunately the transaction is failed.</Text>
                        <Text style={styles.text_12_margin}>Most common reasons for failure are following:</Text>
                        <Image source={require("../../../images/failure_arrow.webp")} style={styles.imgStyle}/>
                        <Image source={require("../../../images/failure_card.webp")} style={styles.imgStyle}/>
                        <Text style={styles.text_12_margin}>Card number or expiry date is wrong.</Text>
                        <Image source={require("../../../images/failure_arrow.webp")} style={styles.imgStyle}/>
                        <Image source={require("../../../images/failure_arrow.webp")} style={styles.imgStyle}/>
                        <Image source={require("../../../images/failure_bank.webp")} style={styles.imgStyle}/>
                        <Text style={styles.text_12_margin}>The bank has rejected the transaction.</Text>
                        <Image source={require("../../../images/failure_arrow.webp")} style={styles.imgStyle}/>
                        <Image source={require("../../../images/failure_arrow.webp")} style={styles.imgStyle}/>
                        <Image source={require("../../../images/failure_network.webp")} style={styles.imgStyle}/>
                    </View>
                </ScrollView>
                <View style={styles.primaryButtonContainer}>
                    <ButtonWithLoader
                        isLoading={false}
                        enable={true}
                        Button={{
                            text: paymentFlowStrings.paymentFlowActionButtonTitle.retryPaymentButton,
                            onClick: () => {
                                this.setState({isPaymentFailed: false});
                            },
                        }}
                    />
                </View>
            </SafeAreaView>
        );
    };
    paymentWebView = () => {
        const {navigation} = this.props;
        let initialProps = navigation.getParam("initialProps", null);
        let paymentUrl = navigation.getParam("paymentLink", null);
        const shouldEditCustomerDetails = navigation.getParam("shouldEditCustomerDetails", null);
        let url = null;
        if (initialProps !== null && initialProps !== undefined && initialProps.isRenewal === true) {
            url = {uri: initialProps.data + paymentFlowStrings.paymentUrlPrefixMediumCode + paymentFlowStrings.paymentUrlPrefixShowCOD};
        } else if (paymentUrl !== null && paymentUrl !== undefined) {
            let isEditDetail = "";
            if (shouldEditCustomerDetails) {
                isEditDetail = "&isEditVisible=true";
            }
            url = {uri: paymentUrl + paymentFlowStrings.paymentUrlPrefixMediumCode + paymentFlowStrings.paymentUrlPrefixShowCOD + isEditDetail};
        }

        return (
            <WebView
                ref={(ref) => {
                    this.webView = ref;
                }}
                key={this.state.webViewKey}
                style={styles.WebViewStyle}
                source={url}
                thirdPartyCookiesEnabled={true}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                scalesPageToFit={true}
                renderLoading={this.getActivityIndicatorLoadingView}
                startInLoadingState={true}
                onMessage={this.onMessage}
                onShouldStartLoadWithRequest={(navigator) => this.onShouldStartLoadWithRequest(navigator, this.state.isRenewal)}
                onNavigationStateChange={(navigator) => this.onShouldStartLoadWithRequest(navigator, this.state.isRenewal)}
                dataDetectorTypes={"none"}
            />
        );
    };

    onMessage(event) {
        const {data} = event.nativeEvent;
        const params = JSON.parse(data)
        if (params && params.type === "Edit_Customer") {
            _this.props.navigation.navigate("CreateCustomerComponent",{isEditFromPayment:true});
            localEventEmitter.emit(paymentFlowStrings.events.eventEditFromPayment, {isEditFromPayment: true})
            let eventAttr = new Object();
            let eventJson = _this.props.navigation.getParam("eventInfo", null);
            eventAttr[WebengageKeys.COVER_AMOUNT] = parseInt(eventJson.coverAmount ?? 0);
            eventAttr[WebengageKeys.PREMIUM_AMOUNT] = parseInt(eventJson.netAmount ?? 0);
            eventAttr[WebengageKeys.SERVICE] = eventJson.serviceType ?? "";
            eventAttr[WebengageKeys.ADDON_SERVICE] = eventJson.addOnService ?? "";
            eventAttr[WebengageKeys.PLAN] = eventJson.plan ?? "";
            eventAttr[WebengageKeys.PLAN_CODE] = "" + eventJson.planCode ?? "";
            eventAttr[WebengageKeys.DISCOUNT] = eventJson.discount ?? "";
            eventAttr[WebengageKeys.PRODUCT] = eventJson.product ?? "";
            eventAttr[WebengageKeys.NO_OF_APPLIANCE] = eventJson.noOfApplications ?? "";
            eventAttr[WebengageKeys.FINAL_PREMIUM_AMOUNT] = parseInt(eventJson.finalPremiumAmount ?? 0);
            eventAttr[WebengageKeys.LOCATION] = "Checkout Webview"
            logWebEnageEvent("Edit Profile", eventAttr)
        } else if (params && params.type === "tnc") {
            nativeBrigeRef.getProductTerms(json => {
                let data = parseJSON(json);
                let initialProps = _this.props.navigation.getParam("initialProps", null)
                let Url = _this.getTncUrl(data, initialProps) + "?mediumCode=app";
                _this.props.navigation.navigate('OAWebView', {
                    deeplinkUrl: Url,
                    title: 'Terms and Conditions',
                    loadingMessage: "Loading..."
                });
            });

        }

    }

    onShouldStartLoadWithRequest = (navigator, isRenewal) => {
        let isHA_SOP = this.props.navigation.getParam("isHA_SOP", null);
        if (navigator.url.includes(paymentFlowStrings.paymentWebViewStrings.SUCCESS_URL_WITH_REL_NO) ||
            navigator.url.includes(paymentFlowStrings.paymentWebViewStrings.SUCCESS_URL_WITH_SR_NO) ||
            navigator.url.includes(paymentFlowStrings.paymentWebViewStrings.SUCCESS_URL) ||
            navigator.url.includes(paymentFlowStrings.paymentWebViewStrings.wsCreateCustomerpaymentsuccess) ||
            navigator.url.includes(paymentFlowStrings.paymentWebViewStrings.wsUpgradepaymentsuccess) ||
            navigator.url.includes(paymentFlowStrings.paymentWebViewStrings.wsRenewalpaymentsuccess)) {
            this.webView.stopLoading();
            var parsed_qs = parseQueryParamsFromUrl(navigator.url);
            for (var key in parsed_qs) {
                if (key.includes(relnoQueryParamKey)) {
                    var value = parsed_qs[key];
                    Store.setValueForKey(relnoQueryParamKey, value);
                    break;
                }
            }
            if (navigateToNextScreen) {
                const {navigation} = this.props;
                let eventInfo = navigation.getParam("eventInfo", null);
                nativeBrigeRef.sendPaymentSuccessEvent(JSON.stringify(eventInfo));
                nativeBrigeRef.paymentSuccessful();
                logAppsFlyerEvent(AF_PURCHASE, {
                    af_revenue: eventInfo?.netAmount,
                    af_currency: CURRENCY,
                    af_content_id: eventInfo?.planCode,
                    af_content_type: eventInfo?.productService,
                    af_price: eventInfo?.price,
                });
                navigateToNextScreen = false;

                if ((parsed_qs?.planType?.toUpperCase() === paymentFlowStrings.paymentFlowServiceTypes.IDFENCE.toUpperCase()) || isHA_SOP) {
                    this.navigateToPaymentSuccess(parsed_qs);
                } else if (navigator.url.includes(paymentFlowStrings.paymentWebViewStrings.SUCCESS_URL_WITH_SR_NO)) {
                    this.navigateToServicePaymentSuccess(parsed_qs);
                } else {
                    this.navigateToPaymentSuccessStepOne(parsed_qs, isRenewal);
                }
            }
            return false;
        } else if (navigator.url.includes(paymentFlowStrings.paymentWebViewStrings.wsIDFencePaymentUpdateSuccess)) {
            screenNavigator.pop();
            return true;
        } else if (navigator.url.includes(paymentFlowStrings.paymentWebViewStrings.FAILURE_URL) ||
            navigator.url.includes(paymentFlowStrings.paymentWebViewStrings.FAILURE_MSG_URL) ||
            navigator.url.includes(paymentFlowStrings.paymentWebViewStrings.wsPaymentFailure) ||
            navigator.url.includes(paymentFlowStrings.paymentWebViewStrings.wsRenewalpaymentPaymentfailure) ||
            navigator.url.includes(paymentFlowStrings.paymentWebViewStrings.wsUpgradepaymentPaymentfailure)) {
            this.setState({isPaymentFailed: true});
            return false;
        } else {
            return true;
        }
    };
    // Navigation
    navigateToPaymentSuccessStepOne = (params, isRenewal) => {
        if (params !== null && params !== undefined) {
            this.props.navigation.navigate("PaymentSuccessStepOneComponent", {
                activationCode: encodeURIComponent(params.activationCode),
                isRenewal: isRenewal,
                relno: params.relno,
            });
        }
    };

    navigateToServicePaymentSuccess(params) {
        let customerInfo = {};
        const {navigation} = this.props;
        let customerOrderInfo = navigation.getParam("customerOrderInfo", null);
        if (customerOrderInfo !== null && customerOrderInfo !== undefined && customerOrderInfo.hasOwnProperty("customerDetails") && customerOrderInfo.customerDetails.length !== 0) {
            params.price = customerOrderInfo.customerDetails[0].price;
            params.planName = customerOrderInfo.customerDetails[0].planName;
        }

        if (params !== null && params !== undefined) {
            customerInfo["mobileNo"] = params.mobile;
            customerInfo["email"] = params.email;
            customerInfo["custId"] = params.relno;
        }
        let title = paymentFlowStrings.activationSuccessTitle.activated;
        if (this.state.isRenewal && params?.planType?.toUpperCase() === paymentFlowStrings.paymentFlowServiceTypes.IDFENCE.toUpperCase()) {
            title = paymentFlowStrings.activationSuccessTitle.upgraded;
        }
        this.props.navigation.navigate("ServicePaymentSuccessComponent", {
            tempCustInfoData: {"customerDetails": [customerInfo]},
            title: title,
            orderInfo: navigation.getParam("orderInfo"),
            userDetails: navigation.getParam("userDetails"),
        });
    }

    navigateToPaymentSuccess(params) {
        let customerInfo = {};
        const {navigation} = this.props;
        let customerOrderInfo = navigation.getParam(
            "customerOrderInfo",
            null,
        );
        if (customerOrderInfo !== null && customerOrderInfo !== undefined && customerOrderInfo.hasOwnProperty("customerDetails") && customerOrderInfo.customerDetails.length !== 0) {
            params.price = customerOrderInfo.customerDetails[0].price;
            params.planName = customerOrderInfo.customerDetails[0].planName;
        }

        if (params) {
            customerInfo = {
                price: params?.price,
                planName: params?.planName,
                mobileNo: params?.mobile,
                emailId: params?.email,
                custId: params?.relno?.toString()
            }
        }
        let title = paymentFlowStrings.activationSuccessTitle.activated;
        if (this.state.isRenewal && params?.planType?.toUpperCase() === paymentFlowStrings.paymentFlowServiceTypes.IDFENCE.toUpperCase()) {
            title = paymentFlowStrings.activationSuccessTitle.upgraded;
        }
        this.props.navigation.navigate("WAPaymentSuccessComponent", {
            tempCustInfoData: {"customerDetails": [customerInfo]},
            title: title,
        });
    }

    getTncUrl(data, initialProps) {
        let termsAndConditionString = JSON.stringify(data);
        let termsAndConditionJSON = parseJSON(termsAndConditionString);
        let productTerms = termsAndConditionJSON['product_terms'];
        productTerms = parseJSON(productTerms);
        let baseUrl = productTerms['baseUrl'];
        let subPart = productTerms['subPart'];
        let userDataObj = initialProps.data;
        let userDataJSON = parseJSON(userDataObj);
        let customerInfoObject;
        let productCode = '';
        if (userDataJSON !== null && userDataJSON !== undefined) {
            customerInfoObject = userDataJSON['customerInfo'];
            if (customerInfoObject !== undefined && customerInfoObject[0]?.hasOwnProperty('assetInfo')) {
                let assetInfoObject = customerInfoObject[0]['assetInfo'];
                productCode = assetInfoObject[0]['productCode'];
            }
        }

        let termsAndCUrl = paymentFlowStrings.productTermAndCUrl;
        let productUrlJSON = '';
        switch (initialProps.service_Type) {
            case FirebaseKeys.HomeAppliance:
                productUrlJSON = productTerms[paymentFlowStrings.paymentFlowServiceTypes.HA];
                if (initialProps.hasEW) {
                    termsAndCUrl = baseUrl + subPart + productUrlJSON[paymentFlowStrings.productUrlKeyForTAndC.KEY_EW];
                } else if (initialProps?.isHA_SOP) {
                    termsAndCUrl = baseUrl + subPart + productUrlJSON[paymentFlowStrings.productUrlKeyForTAndC.KEY_SOP];
                } else {
                    termsAndCUrl = baseUrl + subPart + productUrlJSON[paymentFlowStrings.productUrlKeyForTAndC.KEY_ADLD];
                }
                break;
            case FirebaseKeys.PE:
                if (productTerms.hasOwnProperty(productCode)) {
                    productUrlJSON = productTerms[productCode];
                } else {
                    productUrlJSON = productTerms[paymentFlowStrings.paymentFlowServiceProductCodes.PE_MP01];
                }
                if (initialProps.hasEW) {
                    termsAndCUrl = baseUrl + subPart + productUrlJSON[paymentFlowStrings.productUrlKeyForTAndC.KEY_EW];
                } else {
                    termsAndCUrl = baseUrl + subPart + productUrlJSON[paymentFlowStrings.productUrlKeyForTAndC.KEY_ADLD];
                }
                break;
            case FirebaseKeys.wallet:
                productUrlJSON = productTerms[productCode];
                termsAndCUrl = baseUrl + subPart + productUrlJSON[paymentFlowStrings.productUrlKeyForTAndC.KEY_WALLET];
                break;
            case FirebaseKeys.SOD:
                productUrlJSON = productTerms[paymentFlowStrings.paymentFlowServiceProductCodes.SOD];
                termsAndCUrl = baseUrl + subPart + productUrlJSON[paymentFlowStrings.paymentFlowServiceProductCodes.SOD];
                break;
            case FirebaseKeys.IDFence:
                productUrlJSON = productTerms[paymentFlowStrings.paymentFlowServiceProductCodes.IDFence];
                termsAndCUrl = baseUrl + subPart + productUrlJSON[paymentFlowStrings.paymentFlowServiceProductCodes.IDFence];
                break;
            default:
                break;
        }
        return termsAndCUrl;
    }
}
