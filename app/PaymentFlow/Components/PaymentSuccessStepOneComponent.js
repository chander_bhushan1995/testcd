import React from "react";
import {
    BackHandler,
    Image,
    NativeModules,
    Platform,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from "react-native";
import ButtonWithLoader from "../../CommonComponents/ButtonWithLoader.js";
import { CommonStyle } from "../../Constants/CommonStyle";
import dimens from "../../Constants/Dimens";
import fontSizes from "../../Constants/FontSizes";
import spacing from "../../Constants/Spacing";
import PlatformActivityIndicator from "../../CustomComponent/PlatformActivityIndicator";
import colors from "../../Constants/colors";
import paymentFlowStrings from "../Constants/Constants";
import { PlanServices, PLATFORM_OS } from "../../Constants/AppConstants";
import * as WebengageKyes from "../../Constants/WebengageAttrKeys";
import { logWebEnageEvent } from "../../commonUtil/WebengageTrackingUtils";
import { parseJSON, deepCopy } from "../../commonUtil/AppUtils";

const chatBrigeRef = NativeModules.ChatBridge;
let customerInfo = null;
let noNeedToRender = false;
let relno = "";
let isFromMemTab = false;
let isShowSuccessScreen = true;
let isRenewal = false;
let activationCode = null;

export default class PaymentSuccessStepOneComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        const { navigation } = this.props;
        activationCode = navigation.getParam("activationCode", null);
        isRenewal = navigation.getParam("isRenewal", false);
        relno = navigation.getParam("relno", null);
        isFromMemTab = navigation.getParam("isFromMemTab", false);
        if (this.props.tempCustInfoData === null || this.props.tempCustInfoData === undefined) {
            this.props.getTempCustInfo(activationCode);
        }
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
        isFromMemTab = false;
        isShowSuccessScreen = false;
    }

    static navigationOptions = {
        title: paymentFlowStrings.paymentFlowComponentTitles.paymentSuccessStepOneComponentTitle,
    };

    render() {
        if (this.props.isLoading || !this.props.tempCustInfoData) {
            return (
                <View style={CommonStyle.loaderStyle}>
                    <PlatformActivityIndicator />
                </View>
            );
        } else {
            // TODO: KAG, check this logic
            var customerDetails = this.props.tempCustInfoData;
            customerInfo = customerDetails["customerDetails"][0];
        }

        var customerDetails = this.props.screenData;
        if (!relno && customerDetails && customerDetails?.hasOwnProperty("custId")) {
            relno = customerInfo?.custId?.toString();
        }

        if (isFromMemTab) {
            isFromMemTab = false;
            noNeedToRender = true;
            if (this.props.initialProps.service_Type === "HS") {
                this.navigateToMembershipActivation(true);
            } else {
                this.navigateToBasicDetails(true);
            }
            return (<View />);
        }
        if (isShowSuccessScreen && this.props.planCategory === paymentFlowStrings.paymentFlowPlanCatCodes.PLAN_CATEGORY_CODE_HA) {
            isShowSuccessScreen = false;
            if (!this.props.isTaskInspection && !this.props.isPlanServiceEW) {
                this.navigateToWAPaymentSuccessComponent();
                return (<View />);
            }
        }

        if (isShowSuccessScreen &&
            this.props.planCategory === paymentFlowStrings.paymentFlowPlanCatCodes.PLAN_CATEGORY_CODE_F &&
            isRenewal) {
            isShowSuccessScreen = false;
            this.navigateToWAPaymentSuccessComponent();
            return (<View />);
        }
        // let mobileDocUploadedByOthersMsg = this.mobileDocUploadedByOthersMsg();
        //if HA_EW : navigate to MembershipActivationComponent
        if (noNeedToRender) return (<View />);
        return (
            <SafeAreaView style={styles.SafeArea}>
                <View style={styles.container}>
                    <View style={styles.topCrossContainer}>
                        <TouchableOpacity activeOpacity={spacing.activeOpacity_05} onPress={this.onBackPress}>
                            <Image source={require("../../images/icon_cross.webp")} style={styles.imgCross} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.paymentSuccessContainer}>
                        <Image source={require("../../images/green_right.webp")} style={styles.imgPaymentSuccess} />
                        <Text style={styles.txtRowTitle}>{customerDetails.stepOneTitle}</Text>
                        <Text style={styles.txtPurchaseAmount}>{customerDetails.amount}</Text>
                    </View>
                    <View style={styles.paymentSuccessDataContainer}>
                        <View style={styles.verticalLineView} />
                        <Text style={styles.txtRowSubTitle}>{customerDetails.stepOneSubTitle}</Text>
                    </View>
                    <View style={[styles.cardContainer]}>
                        <View style={styles.paymentSuccessSTwo}>
                            <Image source={require("../../images/ongoing_timeline_icon.webp")}
                                   style={styles.imgPaymentSuccess} />
                            <Text style={styles.txtBlackTitle}>{customerDetails.stepTwoTitle}</Text>
                        </View>
                        <Text style={styles.txtSubTitleSecondStep}>
                            {customerDetails.stepTwoSubTitle}
                        </Text>
                        {/*{mobileDocUploadedByOthersMsg}*/}
                        {customerDetails.stepTwoNote
                            ? <View style={styles.userHintMsgStep}>
                                <Image source={require("../../images/success_user_hint_msg.webp")}
                                       style={styles.imgPaymentSuccess} />
                                <Text
                                    style={styles.txtUserHintMsg}>{customerDetails.stepTwoNote}</Text>
                            </View>
                            : null
                        }
                        <View style={styles.primaryButton}>
                            <ButtonWithLoader
                                isLoading={this.props.isLoading}
                                enable={!this.props.isLoading}
                                Button={{
                                    text: customerDetails.btnLabel,
                                    onClick: () => {
                                        this.sendActivationEvent();
                                        switch (this.props.planCategory) {
                                            case paymentFlowStrings.paymentFlowPlanCatCodes.PLAN_CATEGORY_CODE_HA:
                                                if (isRenewal) {
                                                    this.navigateToMembershipTab();
                                                } else if (this.props.isTaskInspection && !this.props.isPlanServiceEW) {
                                                    this.navigateToScheduleInspectionFlow();
                                                } else {
                                                    this.navigateToMembershipActivation(false);
                                                }
                                                break;
                                            case paymentFlowStrings.paymentFlowPlanCatCodes.PLAN_CATEGORY_CODE_PE:
                                                switch (this.props.productCode) {
                                                    case paymentFlowStrings.paymentFlowServiceProductCodes.PE_MP01:
                                                        //if mobile EW
                                                        if (this.props.initialProps.hasEW && !(this.props.tempCustInfoData?.enableFDFlow && (this.props.tempCustInfoData?.customerDetails?.first().planServices ?? []).includes(PlanServices.adld))) {
                                                            this.navigateToBasicDetails(false);
                                                        } else {
                                                            this.startFDFlow();
                                                        }
                                                        break;
                                                    case paymentFlowStrings.paymentFlowServiceProductCodes.PE_LAP:
                                                    case paymentFlowStrings.paymentFlowServiceProductCodes.PE_WR:
                                                    case paymentFlowStrings.paymentFlowServiceProductCodes.PE_TBL:
                                                        this.navigateToBasicDetails(false);
                                                        break;
                                                    default:
                                                }
                                                break;
                                            case paymentFlowStrings.paymentFlowPlanCatCodes.PLAN_CATEGORY_CODE_F:
                                                this.navigateToBasicDetails(false);
                                                break;
                                            default:
                                                break;
                                        }
                                    },
                                }} />
                        </View>
                    </View>
                </View>
            </SafeAreaView>
        );
    }

    onBackPress = () => {
        this.navigateToMembershipTab();
    };

    exitToNative = () => {
        chatBrigeRef.goBack();
    };
    navigateToBasicDetails = (isFromMemTab) => {
        this.props.navigation.navigate("BasicDetailsComponent", {
            isLoading: this.props.isLoading,
            productCode: this.props.productCode,
            planCategory: this.props.planCategory,
            activationCode: activationCode,
            relno: relno,
            isFromMemTab: isFromMemTab,
            tempCustInfoData: this.props.tempCustInfoData,
        });
    };

    navigateToWAPaymentSuccessComponent = () => {
        this.props.navigation.navigate("WAPaymentSuccessComponent", {
            tempCustInfoData: this.props.tempCustInfoData["customerDetails"][0],
            title: isRenewal ? paymentFlowStrings.activationSuccessTitle.renewed : paymentFlowStrings.activationSuccessTitle.activated,
        });
    };

    navigateToMembershipActivation = (isFromMemTab) => {
        this.props.navigation.navigate("MembershipActivationComponent", {
            stageCount: 1,
            productCode: this.props.productCode,
            planCategory: this.props.planCategory,
            actCode: this.props.actCode,
            relno: relno,
            isFromMemTab: isFromMemTab,
            tempCustInfoData: this.props.tempCustInfoData,
        });
    };

    navigateToMembershipTab = () => {
        chatBrigeRef.openMemTab();
    };
    startFDFlow = () => {
        let isPlanForDiffPlatform = (Platform.OS === PLATFORM_OS.android && customerInfo["deviceMake"] === "Apple") || (Platform.OS === PLATFORM_OS.ios && customerInfo["deviceMake"] !== "Apple");
        if (isPlanForDiffPlatform) {
            this.onBackPress();
        } else {
            chatBrigeRef.startFDFlow(activationCode, this.props.tempCustInfoData);
        }
    };

    navigateToScheduleInspectionFlow = () => {
        chatBrigeRef.openScheduleInspectionFlow(activationCode, customerInfo["pinCode"]);
    };

    //send WebEngage Event
    sendActivationEvent = () => {
        // ---------- Web Engage -----------
        let eventDict = this.props.initialProps.eventDict;
        if (eventDict === null || eventDict === undefined) {
            return;
        }
        let eventJson = parseJSON(this.props.initialProps.eventDict);
        let eventAttr = new Object();

        if (eventJson.TYPE !== null && eventJson.TYPE !== undefined) {
            eventAttr[WebengageKyes.TYPE] = eventJson.TYPE;
        }
        if (eventJson.SERVICE !== null && eventJson.SERVICE !== undefined) {
            eventAttr[WebengageKyes.SERVICE] = eventJson.SERVICE;
        }
        let eventName = WebengageKyes.ACTIVATION;
        if (this.props.isTaskInspection) {
            eventName = WebengageKyes.SCHEDULE_INSPECTION;
        }
        if (this.props.productCode === paymentFlowStrings.paymentFlowServiceProductCodes.PE_MP01) {
            let customerDetails = this.props.tempCustInfoData;
            if (customerDetails !== null && customerDetails !== undefined) {
                let tasks = customerDetails?.task?.first();
                let fdTestType = tasks?.fdTestType;
                let strFDFlowMode = WebengageKyes.FD_WE_ATTRIBUTE_VALUE_SECONDARY_DEVICE;
                if (WebengageKyes.FD_MT_FLOW === fdTestType) {
                    strFDFlowMode = WebengageKyes.FD_WE_ATTRIBUTE_VALUE_MIRROR;
                }
                eventAttr[WebengageKyes.FD_MODE] = "" + strFDFlowMode;
            }

        }
        logWebEnageEvent(eventName, eventAttr);
    };

}
const styles = StyleSheet.create({
    SafeArea: {
        flex: spacing.spacing1,
        backgroundColor: colors.white,
    },
    scrollViewStyle: {
        backgroundColor: colors.white,
        flex: spacing.spacing1,
    },
    container: {
        backgroundColor: colors.white,
        flex: spacing.spacing1,
        alignSelf: "auto",
    },
    topCrossContainer: {
        marginTop: spacing.spacing12,
        alignSelf: "flex-end",
        marginRight: spacing.spacing20,
        flexDirection: "row",
    },
    paymentSuccessContainer: {
        flexDirection: "row",
        marginTop: spacing.spacing56,
        marginRight: spacing.spacing20,
        marginLeft: spacing.spacing20,
    },
    imgCross: {
        width: dimens.dimen16,
        height: dimens.dimen16,
    },
    imgPaymentSuccess: {
        resizeMode: "contain",
        width: dimens.dimen20,
        height: dimens.dimen20,
    },
    txtRowTitle: {
        flex: spacing.spacing1,
        fontWeight: "bold",
        fontSize: fontSizes.fontSize16,
        marginLeft: spacing.spacing16,
        fontFamily: "Lato",
        color: colors.charcoalGrey,
    },
    txtPurchaseAmount: {
        fontWeight: "bold",
        fontSize: fontSizes.fontSize16,
        fontFamily: "Lato",
        textAlign: "right",
        color: colors.charcoalGrey,
    },
    paymentSuccessDataContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginRight: spacing.spacing20,
        marginLeft: spacing.spacing32,
        paddingBottom: spacing.spacing56,
    },
    verticalLineView: {
        backgroundColor: colors.color_E0E0E0,
        marginTop: spacing.spacing8,
        marginBottom: -48,
        width: spacing.spacing1,
    },
    txtRowSubTitle: {
        flex: spacing.spacing1,
        lineHeight: spacing.spacing20,
        marginTop: spacing.spacing8,
        fontSize: fontSizes.fontSize14,
        marginLeft: spacing.spacing22,
        fontFamily: "Lato",
        color: colors.grey,
    },
    cardContainer: {
        // height: spacing.cardContainerPaymentSuccessStepOneHeight,

        padding: spacing.spacing20,
        marginTop: -20,
        shadowOffset: {
            //width: spacing.spacing0,
            height: spacing.spacing4,
        },
        shadowOpacity: spacing.cardContainerPaymentSuccessStepOneShadowOpacity,
        shadowRadius: spacing.spacing6,
        shadowColor: colors.black,
        elevation: spacing.spacing4,
        borderColor: "transparent",
        backgroundColor: colors.white,
    },
    paymentSuccessSTwo: {
        flexDirection: "row",
        marginRight: spacing.spacing20,
    },
    primaryButton: {
        marginLeft: spacing.spacing36,
        marginRight: spacing.spacing24,
        marginBottom: spacing.spacing14,
        marginTop: spacing.spacing24,
    },
    txtSubTitleSecondStep: {
        fontSize: fontSizes.fontSize14,
        fontFamily: "Lato",
        textAlign: "left",
        marginLeft: spacing.spacing36,
        marginTop: spacing.spacing8,
        lineHeight: spacing.spacing20,
        color: colors.grey,
    },
    txtBlackTitle: {
        fontWeight: "bold",
        fontSize: fontSizes.fontSize16,
        marginLeft: spacing.spacing16,
        fontFamily: "Lato",
        color: colors.charcoalGrey,
    },
    userHintMsgStep: {
        flexDirection: "row",
        marginLeft: spacing.spacing36,
        marginRight: spacing.spacing36,
        marginBottom: spacing.spacing_neg_5,
        marginTop: spacing.spacing22,
        lineHeight: spacing.spacing20,
        color: colors.grey,
    },
    txtUserHintMsg: {
        fontWeight: "normal",
        fontSize: fontSizes.fontSize12,
        marginLeft: spacing.spacing5,
        fontFamily: "Lato",
        color: colors.color_888F97,
    },
});
