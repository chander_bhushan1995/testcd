import React, {useEffect, useState} from 'react';
import {
    Alert,
    Image,
    NativeModules,
    Platform,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from 'react-native';
import colors from '../../Constants/colors';
import useBackHandler from '../../CardManagement/CustomHooks/useBackHandler';
import {SafeAreaStyle, TextStyle} from '../../Constants/CommonStyle';
import ButtonWithLoader from '../../CommonComponents/ButtonWithLoader';
import spacing from '../../Constants/Spacing';
import * as PaymentApi from '../DataLayer/PaymentAPIHelper';
import SlotTimeComponent from './SlotTimeComponent';
import SlotsListComponent from './SlotsListComponent';
import {formattedDate, getTodayDate, getTomorrowDate} from '../../commonUtil/DateUtils';
import DateTimePicker from 'react-native-modal-datetime-picker';
import BlockingLoader from '../../CommonComponents/BlockingLoader';
import {parseJSON, deepCopy, getActivityName, getSlotsTimeText} from '../../commonUtil/AppUtils';
import * as Actions from '../../Constants/ActionTypes';
import {SERVICE_ON_DEMAND} from '../../Constants/WebengageAttrKeys';
import {APIData} from '../../../index';
import paymentFlowStrings from '../Constants/Constants';

const bridgeRef = NativeModules.ChatBridge;
let sod_proceed_to_payment_subtitle = 'Pay after service available in few cities';
export default function ScheduleVisitComponent(props) {
    const {navigation} = props;
    const initialProps = navigation.getParam('initialProps');
    const data = parseJSON(initialProps?.data);
    const [slotsData, setSlotsData] = useState([]);
    const [selectedDate, setSelectedDate] = useState(getTodayDate);
    const [todayDisable, setTodayDisable] = useState(false);
    const [tomorrowDisable, setTomorrowDisable] = useState(false);
    const [showCalendar, setShowCalendar] = useState(false);
    const [showLoader, setShowLoader] = useState(false);
    const [address, setAddress] = useState('');
    const [addressId, setAddressId] = useState('');
    const [addressTitle, setAddressTitle] = useState(paymentFlowStrings.addressTitlePrefilled);
    const [calendarEnable, setCalendarEnable] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [addressErrorMsg, setAddressErrorMsg] = useState("");
    const [slotError, setSlotErrorMsg] = useState("");
    const isPaymentFlow = navigation.getParam('isPaymentFlow', true);
    const [dateForSlots, setDateForSlots] = useState(getTodayDate);
    const [noSlotsAvailable, setNoSlotsAvailable] = useState("");
    const [isFistTime, setFirstTime] = useState(true);

    let price = '';

    if (data?.customerInfo != null && data?.customerInfo[0] !== null && data?.customerInfo[0] !== undefined && data?.customerInfo[0]?.assetInfo[0] !== null && data?.customerInfo[0]?.assetInfo[0] !== undefined) {
        price = (data?.customerInfo[0]?.assetInfo[0]?.invoiceValue).toString();
    }
    const priceValue = price && price?.length > 0 ? ' (\u20B9' + price + ')' : '';
    let pincode;
    if (isPaymentFlow) {
        pincode = data?.addressInfo[0]?.pinCode;
    } else {
        pincode = data?.thirdPartyParams?.serviceRequestAddressDetails?.pincode;
    }

    let isSubmitEnabled = (slotsData.length > 0) ;

    useEffect(() => {
        getSlots();
        setShowLoader(true);
    }, [dateForSlots, isFistTime]);

    useEffect(() => {
        getAddress();
        updateNavigationHeader();
    }, []);

    useBackHandler(() => backScreen());

    function backScreen() {
        if (navigation.getParam('fromCreateCustomerScreen', false)) {
            navigation.pop();
            return true
        } else {
            bridgeRef.goBack();
        }
    }

    const updateNavigationHeader = () => {
        navigation.setParams({
            headerTitle: 'Schedule visit',
            onBackPress: () => backScreen(),
        });
    };

    const validateFields = () => {
        let isValid = true;
        if (!address) {
            setAddressErrorMsg(paymentFlowStrings.pleaseEnterTheAddress);
            isValid = false;
        } else {
            setAddressErrorMsg("");
        }
        if (slotsData.filter(data => data?.isSelected)?.length === 0) {
            setSlotErrorMsg(paymentFlowStrings.pleaseSelectASlot);
            isValid = false;
        } else {
            setSlotErrorMsg("");
        }
        return isValid;
    }

    const getSlots = () => {
            let productCode = "";
            if (data?.thirdPartyParams?.assets && data?.thirdPartyParams?.assets.length > 0) {
                productCode = data?.thirdPartyParams?.assets[0].productCode;
            }
            let bpCode = data?.partnerCode ?? APIData.partnerCode;
            let buCode = data?.partnerBUCode ?? APIData.partnerBUCode;
            PaymentApi.getSlots(data?.thirdPartyParams?.serviceRequestType, data?.thirdPartyParams?.serviceRequestSourceType,
                selectedDate, pincode, bpCode, buCode, productCode, (response, error) => {
                    if (response) {
                        setShowLoader(false);
                        if (dateForSlots !== response?.data?.serviceRequestDate) {
                            if (!calendarEnable && isFistTime) {
                                if ((dateForSlots === getTodayDate())) {
                                    setTodayDisable(true);
                                }
                                if (getTomorrowDate() !== response?.data?.serviceRequestDate) {
                                    setCalendarEnable(true)
                                }
                                setSelectedDate(response?.data?.serviceRequestDate);
                                setSlotsData(getSlotData(response?.data?.serviceSlots));
                            } else {
                                setSlotsData([]);
                            }
                        } else {
                            setSlotsData(getSlotData(response?.data?.serviceSlots));
                        }
                    } else {
                        setShowLoader(false);
                    }
                }
            );

            bridgeRef.getSODProceedToPaymentSubTitle(jsonData => {
                let jsonSubtitle = parseJSON(jsonData);
                sod_proceed_to_payment_subtitle = jsonSubtitle.sod_proceed_to_payment_subtitle;
            });
        }
    ;

    function getSlotData(serviceSlots) {
        for (let i = 0; i < serviceSlots?.length; i++) {
            let slot = serviceSlots[i];
            slot = {...slot, isSelected: false, slotText: getSlotsTimeText(slot?.startTime, slot?.endTime)};
            serviceSlots[i] = slot;
        }
        return serviceSlots;
    }

    const getAddress = () => {
        PaymentApi.getAddress(pincode, (response, error) => {
            if (response) {
                let combinedAddress;
                if (response?.data?.customerAddress != null && response?.data?.customerAddress[0] != null && response?.data?.customerAddress[0] !== undefined) {
                    let customerAddress = response?.data?.customerAddress[0];
                    combinedAddress = getAddressValue(customerAddress?.addressLine1) + getAddressValue(customerAddress?.addressLine2) + getAddressValue(customerAddress?.landmark);
                    if (combinedAddress?.length === 0) {
                        setAddressTitle(paymentFlowStrings.addressTitleNotPrefilled);
                    }
                    setAddress(combinedAddress);
                    setAddressId(customerAddress?.addressId);
                } else {
                    setAddressTitle(paymentFlowStrings.addressTitleNotPrefilled);
                }
            } else {

            }
        });
    };

//send SOD Event
    const sendSODWebengageEvent = (slot) => {
        let eventDict = APIData?.eventDict;
        bridgeRef.sendScheduleVisitEvent(SERVICE_ON_DEMAND, eventDict?.Appliance, eventDict?.Count, eventDict?.ServiceType, eventDict?.Cost, selectedDate, slot?.startTime + ", " + slot?.endTime);
    }

    const callCreateServiceOrderApi = () => {
        setIsLoading(true);
        let slot = slotsData.filter(data => data?.isSelected);
        sendSODWebengageEvent(slot[0]);
        PaymentApi.createServiceOrder(navigation.getParam('userDetails'), selectedDate, address, slot[0], addressId, (response, error) => {
            setIsLoading(false);
            if (response) {
                let eventDict = APIData?.eventDict;
                navigation.navigate('PaymentWebViewComponent', {
                    paymentLink: response?.data?.paymentLink,
                    otherParam: 'Payment Review',
                    initialProps:initialProps,
                    orderInfo: {
                        price: price,
                        orderId: response?.data?.orderId,
                        selectedDate: selectedDate,
                        selectedSlot: slot[0],
                        isPaymentFlow: isPaymentFlow,
                        serviceRequestType: data?.thirdPartyParams?.serviceRequestType,
                    },
                    userDetails: navigation.getParam('userDetails'),
                    eventInfo: {
                        activity: getActivityName(initialProps?.isRenewal),
                        netAmount: eventDict?.Cost,
                        product: initialProps?.service_Type,
                        category: initialProps?.service_Type,
                        price: eventDict?.Cost,
                        planCode: data?.orderInfo?.planInfo[0]?.planCode,
                        serviceType: data?.thirdPartyParams?.serviceRequestType,
                        subCategory: eventDict?.Appliance,
                        planName: eventDict?.Appliance + " " + eventDict?.ServiceType,
                        productService: eventDict?.ProductService
                    },
                });
            } else {
                let errorMsg = error?.error[0]?.message;
                errorMsg = errorMsg ? errorMsg : Actions.TECHNICAL_ERROR_MESSAGE,
                    Alert.alert(
                        '',
                        errorMsg,
                        [{text: 'Ok'}],
                        {cancelable: true},
                    );
            }
        });
    };

    const callScheduleSlotApi = () => {
        setIsLoading(true);
        let slot = slotsData.filter(data => data?.isSelected);
        PaymentApi.createServiceOrderForSlots(navigation.getParam('userDetails'), selectedDate, slot[0], (response, error) => {
            setIsLoading(false);
            if (response) {
                navigation.navigate('ServicePaymentSuccessComponent', {
                    orderInfo: {
                        orderId: response?.data?.orderId,
                        selectedDate: selectedDate,
                        selectedSlot: slot[0],
                        isPaymentFlow: isPaymentFlow,
                    },
                    userDetails: navigation.getParam('userDetails'),
                });
            } else {
                let errorMsg = error?.error[0]?.message;
                errorMsg = errorMsg ? errorMsg : Actions.TECHNICAL_ERROR_MESSAGE,
                    Alert.alert(
                        '',
                        errorMsg,
                        [{text: 'Ok'}],
                        {cancelable: true},
                    );
            }
        });
    };

    function getAddressValue(value) {
        return value !== null && value !== undefined ? value + ' ' : '';
    }

    function onDateSelected(date) {
        setFirstTime(false);
        setSelectedDate(date);
        setDateForSlots(formattedDate(date, 'dd-mmm-yyyy'));
    }

    function onCalendarSelected() {
        setShowCalendar(true);
    }

    function onDateConfirm(date) {
        setFirstTime(false);
        setSelectedDate(formattedDate(date, 'dd-mmm-yyyy'));
        setDateForSlots(formattedDate(date, 'dd-mmm-yyyy'));
        setCalendarEnable(true);
        setShowCalendar(false);
    }

    function onDateCancel() {
        setShowCalendar(false);
    }

    const getAddressView = () => {
        return isPaymentFlow ?
            <View style={{marginTop: spacing.spacing32}}>
                <Text style={[TextStyle.text_16_bold]}>{addressTitle}</Text>
                <View style={styles.addressInputBoxContainerStyle}>
                    <TextInput
                        style={{textAlign: 'left', textAlignVertical: 'top'}}
                        multiline={true}
                        height={100}
                        maxLength={1000}
                        editable={true}
                        value={address}
                        autoFocus={!address ? true : false}
                        onChangeText={inputMessage => {
                            setAddress(inputMessage);
                        }}/>
                </View>
                {addressErrorMsg ?
                    <Text style={{...TextStyle.text_14_normal, color: colors.salmonRed}}>{addressErrorMsg}</Text>
                    : null}
            </View>
            : null;
    };

    const getTopMessageView = () => {
        return !isPaymentFlow ?
            <View style={styles.topMessageViewStyle}><Text style={[TextStyle.text_14_bold]}>Payment Successful for AC
                Service Request</Text>
                <Text style={[TextStyle.text_12_medium, {marginTop: spacing.spacing4}]}>Unfortunately, the slot you
                    choose is currently full. Please select a different slot for technician visit.</Text></View> : null;
    };

    function onSlotSelected(index) {
        let slots = [...slotsData];
        for (let i = 0; i < slots?.length; i++) {
            let slot = slots[i];
            slot = {...slot, isSelected: i === index};
            slots[i] = slot;
        }
        setSlotsData(slots);
    }

    return (
        <SafeAreaView style={SafeAreaStyle.safeArea}>
            <BlockingLoader visible={showLoader} loadingMessage={'Loading'}/>
            <ScrollView style={styles.container}>
                {getTopMessageView()}
                <View style={{marginLeft: spacing.spacing32, marginRight: spacing.spacing32}}>
                    {getAddressView()}
                    <Text style={[TextStyle.text_16_bold, {marginTop: spacing.spacing16}]}>When would you like the
                        technician to visit?</Text>
                    <SlotTimeComponent
                        onDateSelected={onDateSelected}
                        calendarEnable={calendarEnable}
                        onCalendarSelected={onCalendarSelected}
                        todayDisable={todayDisable}
                        selectedDate={selectedDate}/>
                    {
                        (slotsData.length > 0) ?
                            <View>
                                <Text style={[TextStyle.text_16_bold, {marginTop: spacing.spacing32}]}>and around what time?</Text>
                                <SlotsListComponent
                                    data={slotsData}
                                    onSlotSelected={onSlotSelected}/>
                            </View>
                            : <Text style={{...TextStyle.text_14_bold, marginTop: spacing.spacing16}}>{paymentFlowStrings.noSlotsAvailable}</Text>
                    }

                    {slotError ?
                        <Text style={{...TextStyle.text_14_normal, color: colors.salmonRed}}>{slotError}</Text>
                        : null}
                </View>
            </ScrollView>
            <View style={styles.primaryButton}>
                <ButtonWithLoader
                    isLoading={isLoading}
                    enable={isSubmitEnabled}
                    Button={{
                        text: isPaymentFlow ? 'Proceed to payment' + priceValue : 'Done',
                        onClick: () => {
                            if (validateFields()) {
                                if (isPaymentFlow) {
                                    callCreateServiceOrderApi();
                                } else {
                                    callScheduleSlotApi();
                                }
                            }
                        },
                    }
                    }/>
                <Text style={[TextStyle.text_12_normal, {
                    marginTop: spacing.spacing12,
                    marginBottom: spacing.spacing_n_12,
                    alignSelf: 'center'
                }]}>{sod_proceed_to_payment_subtitle}</Text>
            </View>
            <DateTimePicker
                minimumDate={new Date()}
                isVisible={showCalendar}
                mode="date"
                onConfirm={onDateConfirm}
                onCancel={onDateCancel}
            />
        </SafeAreaView>
    );
}

ScheduleVisitComponent.navigationOptions = ({navigation}) => {
    const {
        headerTitle = null,
        onBackPress,
    } = navigation.state.params;
    return {
        headerTitle: (
            <View style={Platform.OS === 'ios' ? {alignItems: 'center'} : {alignItems: 'flex-start'}}>
                <Text style={TextStyle.text_18_bold}>{headerTitle}</Text>
            </View>
        ),
        headerLeft: (
            <TouchableOpacity onPress={
                () => {
                    onBackPress()
                }
            }>
                <Image
                    source={require('../../images/ic_back.webp')}
                    style={{
                        height: 24,
                        width: 30,
                        marginLeft: 12,
                        marginRight: 12,
                        marginVertical: 12,
                        resizeMode: 'contain',
                    }}
                />
            </TouchableOpacity>
        ),
    };
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    topMessageViewStyle: {
        width: '100%',
        backgroundColor: colors.color_FFF1D8,
        paddingLeft: spacing.spacing12,
        paddingRight: spacing.spacing24,
        paddingTop: spacing.spacing16,
        paddingBottom: spacing.spacing16,
    },
    addressInputBoxContainerStyle: {
        padding: spacing.spacing12,
        borderWidth: spacing.spacing1,
        borderColor: colors.color_E0E0E0,
        marginTop: spacing.spacing16,
        borderRadius: spacing.spacing2,
    },
    primaryButton: {
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        marginBottom: spacing.spacing24,
        marginTop: spacing.spacing24,
        justifyContent: 'flex-end',
        alignSelf: 'stretch',
    },
});
