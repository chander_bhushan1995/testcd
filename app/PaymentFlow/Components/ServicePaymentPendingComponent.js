import React from 'react';
import {
    StyleSheet,
    View,
    SafeAreaView,
    ScrollView, Image, Text, TouchableOpacity, NativeModules,
} from 'react-native';
import colors from '../../Constants/colors';
import useBackHandler from '../../CardManagement/CustomHooks/useBackHandler';
import {CommonStyle, SafeAreaStyle, TextStyle} from '../../Constants/CommonStyle';
import ButtonWithLoader from '../../CommonComponents/ButtonWithLoader';
import spacing from '../../Constants/Spacing';
import dimens from '../../Constants/Dimens';
import * as Validator from '../../commonUtil/Validator';
import * as Formatter from '../../commonUtil/Formatter';
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

const nativeBrigeRef = NativeModules.ChatBridge;

export default function ServicePaymentPendingComponent(props) {
    const {navigation} = props;
    const initialProps = navigation.getParam("initialProps")?.data;
    const data = parseJSON(initialProps);

    useBackHandler(() => navigation.pop());

    const getAmountView = () => {
        let amountLabel = 'Amount Paid';
        let amount = data?.salesPrice;
        amount = amount?.toString();
        amount = Validator.isValidString(amount) ? Formatter.formatCoverAmount(amount) : 0;
        return amount !== null && amount !== undefined ? <View style={styles.amountCoverView}>
            <Text style={[TextStyle.text_14_normal, styles.amountCoverLeftViewStyle]}>{amountLabel}</Text>
            <Text style={[TextStyle.text_14_normal, {color: colors.color_404040}]}>{'₹ ' + amount}</Text>
        </View> : null;
    };

    return (
        <SafeAreaView style={SafeAreaStyle.safeArea}>
            <ScrollView style={{flex: 1}}>
                <View style={styles.container}>
                    <Image source={require('../../images/yellow_indicator.webp')} style={styles.imgSuccessScreen}/>
                    <Text style={[TextStyle.text_18_bold, styles.txtTitle]}>Payment is under process</Text>
                    {getAmountView()}
                    <View style={[CommonStyle.separator, {marginTop: spacing.spacing16}]}/>
                    <Text
                        style={[TextStyle.text_12_normal, {color: colors.color_808080, marginTop: spacing.spacing16}]}>We
                        are currently processing your payment. We will notify you once the payment is confirmed.</Text>
                    <View style={styles.querySectionStyle}>
                        <Text style={[TextStyle.text_14_bold, {color: colors.color_808080, marginRight: spacing.spacing8}]}>For any queries, feel free to contact us</Text>
                        <TouchableOpacity onPress={() => nativeBrigeRef.openChat()}>
                            <Image source={require('../../images/ic_chat.webp')}
                                   style={{width: dimens.dimen32, height: dimens.dimen32}}/>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
            <View style={styles.primaryButton}>
                <ButtonWithLoader
                    isLoading={false}
                    enable={true}
                    Button={{
                        text: 'Close',
                        onClick: () => {
                            nativeBrigeRef.goBack();
                        },
                    }
                    }/>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        alignItems: 'center',
        paddingLeft: spacing.spacing16,
        paddingRight: spacing.spacing16,
    },
    imgSuccessScreen: {
        marginTop: spacing.spacing80,
        width: dimens.dimen48,
        height: dimens.dimen48,
    },
    txtTitle: {
        marginTop: spacing.spacing24,
        color: colors.saffronYellow,
        marginBottom: spacing.spacing32,
    },
    amountCoverView: {
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    amountCoverLeftViewStyle: {
        color: colors.color_404040,
        flex: spacing.spacing1,
    },
    querySectionStyle: {
        width: '100%',
        flexDirection: 'row',
        marginTop: spacing.spacing24,
        borderRadius: dimens.dimen2,
        borderWidth: dimens.dimen1,
        borderColor: colors.charcoalGrey,
        padding: spacing.spacing16,
        justifyContent: 'center',
        alignItems: 'center',
    },
    primaryButton: {
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        marginBottom: spacing.spacing24,
        marginTop: spacing.spacing24,
        justifyContent: 'flex-end',
        alignSelf: 'stretch',
    },
});
