import React, {useEffect, useState} from 'react';
import {
    StyleSheet,
    View,
    SafeAreaView,
    ScrollView, TouchableOpacity, Image, NativeModules, Text,
} from 'react-native';
import colors from '../../Constants/colors';
import useBackHandler from '../../CardManagement/CustomHooks/useBackHandler';
import {CommonStyle, SafeAreaStyle, TextStyle} from '../../Constants/CommonStyle';
import ButtonWithLoader from '../../CommonComponents/ButtonWithLoader';
import spacing from '../../Constants/Spacing';
import dimens from '../../Constants/Dimens';
import * as Validator from '../../commonUtil/Validator';
import * as Formatter from '../../commonUtil/Formatter';
import { parseJSON, deepCopy, get12HourTime} from '../../commonUtil/AppUtils';
import {getDateText} from '../../commonUtil/DateUtils';
import paymentFlowStrings from '../Constants/Constants';
import { TabIndex } from "../../Constants/AppConstants";

const bridgeRef = NativeModules.ChatBridge;

export default function ServicePaymentSuccessComponent(props) {
    const {navigation} = props;
    const orderInfo = navigation.getParam("orderInfo", null);
    const userDetails = navigation.getParam("userDetails", null);
    const isPaymentSuccess = orderInfo?.isPaymentFlow;
    const [specialNotesList, setSpecialNotesList] = useState([]);

    useBackHandler(() => {
        bridgeRef.refreshMemberships()
        bridgeRef.openTabBarWithIndex(TabIndex.newHome);
    });

    useEffect(() => {
        bridgeRef.getOneAssistPromiseData(data => {
            let parsedData = parseJSON(data);
            let promiseData = parseJSON(parsedData['oneassist_promise']);
            setSpecialNotesList(promiseData.promises);
        });
    }, []);

    const getAmountView = () => {
        let amountLabel = 'Amount Paid';
        let amount = orderInfo?.price;
        amount = amount?.toString();
        amount = Validator.isValidString(amount) ? Formatter.formatCoverAmount(amount) : 0;
        return isPaymentSuccess && amount !== null && amount !== undefined ? <View style={styles.amountCoverView}>
            <Text style={[TextStyle.text_14_normal, styles.amountCoverLeftViewStyle]}>{amountLabel}</Text>
            <Text style={[TextStyle.text_14_normal, {color: colors.color_404040}]}>{'₹ ' + amount}</Text>
        </View> : null;
    };

    const getServiceRequestSection = () => {
        return isPaymentSuccess ? <View style={styles.serviceRequestCoverView}>
            <Text style={[TextStyle.text_14_normal, styles.amountCoverLeftViewStyle]}>Payment Reference ID</Text>
            <Text style={[TextStyle.text_14_normal, {color: colors.color_404040}]}>{orderInfo?.orderId}</Text>
        </View> : null;
    };

    const getSubInfoSection = () => {
        return isPaymentSuccess ? <View>
            <View style={[CommonStyle.separator, {marginTop: spacing.spacing8}]}/>
            <Text style={[TextStyle.text_12_regular, styles.subInfoStyle]}>{"All the details sent to your mobile number "  + userDetails.mobileNumber + ", " + userDetails.emailId}</Text>
            <View style={[CommonStyle.separator, {marginBottom: spacing.spacing16}]}/>
        </View> : null;
    };

    const paymentTitle = () => {
        return isPaymentSuccess?"Request Placed":"Great! Technician visit scheduled.";
    }

    const getRepairTextView = () => {
        return orderInfo?.serviceRequestType === "HA_BD" ?
            <View style={{backgroundColor: colors.color_FFF1D8, padding: spacing.spacing8, marginTop: spacing.spacing16}}>
                <Text style={TextStyle.text_12_regular}>Note: Final Price will be decided after inspection. Spare Parts will cost extra.</Text></View> : null;
    };

    return (
        <SafeAreaView style={SafeAreaStyle.safeArea}>
            <View style={styles.topCrossContainer}>
                <TouchableOpacity activeOpacity={0.5} onPress={() => {
                    bridgeRef.refreshMemberships()
                    bridgeRef.openTabBarWithIndex(TabIndex.newHome);
                }}>
                    <Image source={require('../../images/icon_cross.webp')} style={styles.imgCross}/>
                </TouchableOpacity>
            </View>
            <ScrollView style={styles.scrollViewStyle}>
                <View style={styles.container}>
                    <Image source={isPaymentSuccess?require('../../images/ic_activated_success.webp'):require('../../images/green_right.webp')} style={styles.imgSuccessScreen}/>
                    <Text style={[TextStyle.text_18_bold, styles.txtTitle]}>{paymentTitle()}</Text>
                    {getServiceRequestSection()}
                    {getSubInfoSection()}
                    <Text style={[TextStyle.text_14_bold, styles.serviceInfoStyle]}>{"Service has been scheduled for "  + getDateText(orderInfo?.selectedDate) + " between " + get12HourTime(orderInfo?.selectedSlot?.startTime)
                     + " to " + get12HourTime(orderInfo?.selectedSlot?.endTime)}</Text>
                    <Text style={[TextStyle.text_12_regular, styles.technicianInfoStyle]}>{paymentFlowStrings.technicianText}</Text>
                    {getRepairTextView()}
                </View>
                <View style={styles.specialNotesStyle}>
                    <View style={{flexDirection: 'row', marginBottom: spacing.spacing4}}>
                        <Image source={require('../../images/ic_special_notes.webp')}
                               style={styles.specialNotesIconStyle}/>
                        <Text style={[TextStyle.text_12_bold, {color: colors.color_6475DF, alignSelf: 'center'}]}>ONEASSIST
                            PROMISE</Text>
                    </View>
                    {specialNotesList.map((point, key) =>
                        <View key={key}>
                            <Text
                                style={[TextStyle.text_14_normal_charcoalGrey, {marginTop: spacing.spacing12}]}>{'. '} {point}</Text>
                        </View>,
                    )}
                </View>
            </ScrollView>
            <View style={styles.primaryButton}>
                <ButtonWithLoader
                    isLoading={false}
                    enable={true}
                    Button={{
                        text: 'Explore Other Services',
                        onClick: () => {
                            bridgeRef.refreshMemberships()
                            bridgeRef.openTabBarWithIndex(TabIndex.newHome);
                        },
                    }
                    }/>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    topCrossContainer: {
        marginTop: spacing.spacing16,
        alignSelf: 'flex-end',
        marginRight: spacing.spacing16,
        flexDirection: 'row',
    },
    imgCross: {
        width: dimens.dimen16,
        height: dimens.dimen16,
    },
    scrollViewStyle: {
        flex: 1,
    },
    container: {
        backgroundColor: colors.white,
        paddingLeft: spacing.spacing16,
        paddingRight: spacing.spacing16,
    },
    imgSuccessScreen: {
        marginTop: spacing.spacing48,
        width: spacing.spacing64,
        height: spacing.spacing64,
        alignSelf: 'center',
    },
    txtTitle: {
        marginTop: spacing.spacing36,
        color: colors.seaGreen,
        marginBottom: spacing.spacing40,
        alignSelf: 'center',
    },
    amountCoverView: {
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    amountCoverLeftViewStyle: {
        color: colors.color_404040,
        flex: spacing.spacing1,
    },
    serviceRequestCoverView: {
        marginTop: spacing.spacing12,
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    subInfoStyle: {
        marginTop: spacing.spacing12,
        marginBottom: spacing.spacing16,
        color: colors.color_808080,
    },
    serviceInfoStyle: {
        color: colors.color_404040,
    },
    technicianInfoStyle: {
        marginTop: spacing.spacing8,
        color: colors.color_888F97,
    },
    specialNotesStyle: {
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing12,
        backgroundColor: colors.color_F3F4FD,
        borderRadius: 4,
        padding: spacing.spacing16,
    },
    specialNotesIconStyle: {
        width: dimens.dimen32,
        height: dimens.dimen32,
        marginRight: spacing.spacing8,
    },
    primaryButton: {
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        marginBottom: spacing.spacing24,
        marginTop: spacing.spacing24,
        justifyContent: 'flex-end',
        alignSelf: 'stretch',
    },
});
