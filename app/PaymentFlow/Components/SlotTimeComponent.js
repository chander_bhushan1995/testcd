import React, {useEffect, useState} from 'react';
import {
    Image,
    StyleSheet, Text, TouchableOpacity,
    View,
} from 'react-native';
import spacing from '../../Constants/Spacing';
import BlueButtonComponent from '../../CommonComponents/BlueButtonComponent';
import {getTodayDate, getTomorrowDate} from '../../commonUtil/DateUtils';
import colors from '../../Constants/colors';
import {TextStyle} from '../../Constants/CommonStyle';

export default function SlotTimeComponent(props) {

    const [isTodayEnable, setTodayEnable] = useState(true);
    const [isTomorrowEnable, setTomorrowEnable] = useState(false);
    const [isCalendarClicked, setCalendarClicked] = useState(props.calendarEnable);

    useEffect(()=>{
        setCalendarClicked(props.calendarEnable)
    },[props.calendarEnable])



    function onTodayClick() {
        setTodayEnable(true);
        setTomorrowEnable(false);
        props.onDateSelected(getTodayDate());
    }

    function onTomorrowClick() {
        setTodayEnable(false);
        setTomorrowEnable(true);
        props.onDateSelected(getTomorrowDate());
    }

    function onCalendarClick() {
        // setCalendarClicked(true);
        props.onCalendarSelected();
    }

    const getView = () => {
        return isCalendarClicked ?
            getCalendarEnableView() : getTodayTomorrowView();
    };

    const getTodayTomorrowView = () => {
        return <View style={styles.containerStyle}>
            <BlueButtonComponent
                isSelected={isTodayEnable}
                disable={props.todayDisable}
                text={'Today'}
                onButtonClick={onTodayClick}
                style={styles.buttonStyle}>
            </BlueButtonComponent>
            <BlueButtonComponent
                isSelected={isTomorrowEnable || props.todayDisable}
                text={'Tomorrow'}
                onButtonClick={onTomorrowClick}
                style={styles.buttonStyle}>
            </BlueButtonComponent>
            <TouchableOpacity
                style={styles.calendarIconBoxStyle}
                onPress={onCalendarClick}>
                <Image source={require('../../images/calendar_icon.webp')} style={styles.calendarIconStyle}/>
            </TouchableOpacity>
        </View>;
    };

    const getCalendarEnableView = () => {
        return <View style={styles.containerStyle}>
            <TouchableOpacity
                style={styles.calendarIconBoxStyle}
                onPress={onCalendarClick}>
                <Image source={require('../../images/calendar_icon.webp')} style={styles.calendarIconStyle}/>
            </TouchableOpacity>
            <Text style={[TextStyle.text_12_medium, {
                marginLeft: spacing.spacing12,
                marginRight: spacing.spacing12,
            }]}>{props.selectedDate}</Text>
            <TouchableOpacity
                onPress={onCalendarClick}>
                <Image source={require('../../images/icon_edit.webp')} style={styles.editIconStyle}/>
            </TouchableOpacity>
        </View>;
    };

    return (
        getView()
    );
}

const styles = StyleSheet.create({
    buttonStyle: {
        flex: 1,
        marginRight: spacing.spacing12,
    },
    containerStyle: {
        flex: 1,
        flexDirection: 'row',
        marginTop: spacing.spacing16,
        alignItems: 'center',
    },
    calendarIconBoxStyle: {
        width: 48,
        height: 48,
        borderColor: colors.greye0,
        borderWidth: 1,
        justifyContent: 'center',
    },
    calendarIconStyle: {
        width: 28,
        height: 28,
        alignSelf: 'center',
    },
    editIconStyle: {
        width: 28,
        height: 28,
        alignSelf: 'center',
    },
});
