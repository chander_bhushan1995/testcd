import React, {useState} from 'react';
import {
    StyleSheet,
    View,
} from 'react-native';
import spacing from '../../Constants/Spacing';
import BlueButtonComponent from '../../CommonComponents/BlueButtonComponent';

// let data = [{
//     'startTime': '09:00',
//     'endTime': '11:00',
// }, {
//     'startTime': '11:00',
//     'endTime': '13:00',
// }, {
//     'startTime': '13:00',
//     'endTime': '15:00',
// }, {
//     'startTime': '15:00',
//     'endTime': '17:00',
// }, {
//     'startTime': '17:00',
//     'endTime': '19:00',
// }, {
//     'startTime': '19:00',
//     'endTime': '21:00',
// }];

export default function SlotsListComponent(props) {
    let data = props.data;
    let answerView = [];
    let totalItems = data?.length;
    for (let index = 0; index < totalItems / 2; index++) {
        let firstItemIndex = 2 * index;
        let view1 = getSlotItem(firstItemIndex, true);

        let view2 = <View style={{flex:1, marginLeft: spacing.spacing24}}/>;
        if ((2 * index + 1) < totalItems) {
            let secondItemIndex = 2 * index + 1;
            view2 = getSlotItem(secondItemIndex, false);
        }
        answerView.push(
            <View style={{flexDirection: 'row'}}>
                {view1}
                {view2}
            </View>);
    }

    function onItemClick(index){
        props.onSlotSelected(index);
    }

    function getSlotItem(index, rightMargin){
        return <BlueButtonComponent
            isSelected={data[index].isSelected}
            text={props.data[index].slotText}
            index={index}
            onButtonClick={onItemClick}
            style={[styles.buttonStyle, rightMargin? {marginRight: spacing.spacing16}:{}]}>
        </BlueButtonComponent>
    }

    return (
        <View style={{marginTop: spacing.spacing16}}>
            {answerView}
        </View>
    );
}

const styles = StyleSheet.create({
    buttonStyle: {
        flex: 1,
        marginBottom: spacing.spacing16
    },
});
