import React from 'react';
import {Platform, StyleSheet, Text, View, SafeAreaView, BackHandler, NativeModules} from 'react-native';
import WebView from 'react-native-webview';
import {HeaderBackButton} from "react-navigation";
import colors from '../../Constants/colors';
import spacing from "../../Constants/Spacing";
import {TextStyle} from "../../Constants/CommonStyle";
import paymentFlowStrings from "../Constants/Constants";
import {PLATFORM_OS} from "../../Constants/AppConstants";

const nativeBrigeRef = NativeModules.ChatBridge;
let tncUrl = paymentFlowStrings.productTermAndCUrl;

let navigator;
export default class TnCWebViewComponent extends React.Component {
    static navigationOptions = ({navigation}) => {
        navigator = navigation;
        let componentTitle = navigation.getParam('otherParam', null)
        if (componentTitle == null || componentTitle == undefined) {
            componentTitle = paymentFlowStrings.paymentFlowComponentTitles.tnCWebViewComponentTitle;
        }
        return {
            headerTitle: <View
                style={Platform.OS === PLATFORM_OS.ios ? {alignItems: "center"} : {alignItems: "flex-start"}}>
                <Text style={TextStyle.text_16_bold}>
                    {componentTitle}

                </Text>
            </View>,
            headerLeft: <HeaderBackButton tintColor={colors.black}
                                          onPress={() => {
                                              navigation.pop();
                                          }}/>,
            headerTitleStyle: {color: colors.white},
            headerTintColor: colors.white
        };
    };

    constructor(props) {
        super(props);
        const {navigation} = this.props;
        let productTermAndCUrl = navigation.getParam('productTermAndCUrl', null)
        if (productTermAndCUrl !== null && productTermAndCUrl !== undefined) {
            tncUrl = productTermAndCUrl;
        }
        tncUrl = tncUrl + "?" + paymentFlowStrings.paymentUrlPrefixMediumCode;
    }

    componentDidMount() {
        if (Platform.OS == PLATFORM_OS.android) {
            nativeBrigeRef.enableNativeBack(false)
        }
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }

    componentWillUnmount() {
        if(Platform.OS == PLATFORM_OS.android) {
            nativeBrigeRef.enableNativeBack(true)
        }
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    onBackPress = () => {
        const {navigation} = this.props;
        let enableNativeBack = navigation.getParam('enableNativeBack', false);
        if ((enableNativeBack == null || enableNativeBack == undefined)) {
            nativeBrigeRef.goBack();
        } else {
            navigator.pop();
            return true;
        }
        return false;
    };

    render() {
        return (
            <SafeAreaView style={styles.SafeArea}>
                <WebView
                    style={styles.WebViewStyle}
                    thirdPartyCookiesEnabled={true}
                    source={{uri: tncUrl}}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    scalesPageToFit={true}
                    startInLoadingState={true}/>
            </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    SafeArea: {
        flex: spacing.spacing1,
        backgroundColor: colors.white,
    },
    WebViewStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: spacing.spacing1,
    }
});
