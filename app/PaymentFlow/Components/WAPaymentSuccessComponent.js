import React from 'react';
import { BackHandler, Image, NativeModules, Platform, StyleSheet, Text, TouchableOpacity, View, SafeAreaView } from 'react-native';
import ButtonWithLoader from '../../CommonComponents/ButtonWithLoader.js';
import dimens from '../../Constants/Dimens';
import fontSizes from "../../Constants/FontSizes";
import * as Validator from "../../commonUtil/Validator";
import * as Formatter from "../../commonUtil/Formatter";
import spacing from "../../Constants/Spacing";
import colors from '../../Constants/colors.js';
import paymentFlowStrings from "../Constants/Constants";
import { PLATFORM_OS } from "../../Constants/AppConstants";
const chatBrigeRef = NativeModules.ChatBridge;

let custId;
export default class WAPaymentSuccessComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    render() {
        const { navigation } = this.props;
        let customerInfo = navigation.getParam('tempCustInfoData', null);
        if(customerInfo === null) return null;
        if(customerInfo.hasOwnProperty("customerDetails") && customerInfo["customerDetails"].length !== 0)
            customerInfo = customerInfo["customerDetails"][0];

        let title = navigation.getParam("title", paymentFlowStrings.activationSuccessTitle.activated)
        custId = customerInfo["custId"]?.toString();

        let amountLabel = "Amount Paid"
        let amount = 0
        if(customerInfo.hasOwnProperty("coverAmount")){
            amountLabel = "Cover Amount"
            amount = customerInfo["coverAmount"]
        }else if(customerInfo.hasOwnProperty("price")){
            amount = customerInfo["price"]
        }
        if(amount !== null && amount !== undefined && amount != ""){
          amount = amount.toString();
          amount = Validator.isValidString(amount) ? Formatter.formatCoverAmount(amount) : 0;
        }
        let amountSection = this.getAmountSection(amount, amountLabel);
        let relationshipIdSection = this.getRelationshipIdSestion(custId);
        return (
            <SafeAreaView style={styles.SafeArea}>
                <View style={styles.container}>
                    <View style={styles.topCrossContainer}>
                        <TouchableOpacity activeOpacity={0.5} onPress={() => this.onBackPress()}>
                            <Image source={require("../../images/icon_cross.webp")} style={styles.imgCross} />
                        </TouchableOpacity>
                    </View>
                    <Image source={require("../../images/ic_activated_success.webp")} style={styles.imgSuccessScreen} />
                    <Text style={styles.txtTitle}>{title}</Text>
                    <Text style={styles.txtSubTitle}>{customerInfo["planName"]}</Text>
                    {amountSection}
                    {relationshipIdSection}
                    <View style={styles.horizontalLineView} />
                    <View style={styles.subInfoView}>
                        <Text style={styles.txtSubInfoLeft}>All the details sent to your mobile number {customerInfo["mobileNo"]}, {customerInfo["emailId"]}</Text>
                    </View>
                    <View style={styles.primaryButton}>
                        <ButtonWithLoader
                            isLoading={false}
                            enable={true}
                            Button={{
                                text: paymentFlowStrings.paymentFlowActionButtonTitle.viewMembershipButton,
                                onClick: () => this.onBackPress()
                            }} />
                    </View>
                </View>
            </SafeAreaView>
        );
    }

    onBackPress = () => {
        chatBrigeRef.openMemTab();
    }

    getAmountSection = (amount, amountLabel) => {
        if (amount !== null && amount !== undefined ) {
          return (
            <View style={styles.containerCoverView}>
              <Text style={styles.txtCoverLabelLeft}>{amountLabel}</Text>
              <Text style={styles.txtCoverLabelRight}>{"₹ " + amount}</Text>
            </View>
          );
        }
        return null
    };
    getRelationshipIdSestion = (custId) => {
        if (custId === null && custId === undefined) return null;
        return (
          <View style={styles.containerRelationshipIDView}>
            <Text style={styles.txtCoverLabelLeft}>Relationship ID</Text>
            <Text style={styles.txtCoverLabelRight}>{custId}</Text>
          </View>
        );
    }
}
const styles = StyleSheet.create({
    SafeArea: {
        flex: spacing.spacing1,
        backgroundColor: colors.white,
    },
    container: {
        backgroundColor: colors.white,
        flex: spacing.spacing1,
        alignItems: 'center',
    },
    topCrossContainer: {
        marginTop: spacing.spacing12,
        alignSelf: 'flex-end',
        marginRight: spacing.spacing20,
        flexDirection: 'row',
    },
    imgCross: {
        width: dimens.dimen16,
        height: dimens.dimen16,
    },
    imgSuccessScreen: {
        marginTop: spacing.spacing150,
        width: spacing.spacing64,
        height: spacing.spacing64,
    },
    txtTitle: {
        fontWeight: "bold",
        fontSize: fontSizes.fontSize18,
        marginTop: spacing.spacing36,
        fontFamily: "Lato",
        color: colors.seaGreen,
    },
    txtSubTitle: {
        fontWeight: "normal",
        fontSize: fontSizes.fontSize14,
        marginTop: spacing.spacing12,
        fontFamily: "Lato",
        color: colors.charcoalGrey,
    },
    containerCoverView: {
        marginTop: spacing.spacing40,
        marginLeft: spacing.spacing20,
        marginRight: spacing.spacing20,
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    containerRelationshipIDView: {
        backgroundColor: colors.white,
        marginTop: spacing.spacing12,
        marginLeft: spacing.spacing20,
        marginRight: spacing.spacing20,
        flexDirection: 'row',
    },
    txtCoverLabelLeft: {
        fontWeight: "normal",
        fontSize: fontSizes.fontSize14,
        justifyContent: 'flex-end',
        fontFamily: "Lato",
        color: colors.color_404040,
        flex: spacing.spacing1,
    },
    txtCoverLabelRight: {
        fontWeight: "normal",
        fontSize: fontSizes.fontSize14,
        justifyContent: 'flex-end',
        fontFamily: "Lato",
        color: colors.color_404040,
    },
    horizontalLineView: {
        marginLeft: spacing.spacing20,
        marginRight: spacing.spacing20,
        flexDirection: 'row',
        borderBottomColor: colors.color_979797,
        borderBottomWidth: StyleSheet.hairlineWidth,
        alignSelf: 'stretch',
        marginTop: spacing.spacing14,
        height: spacing.spacing1,
    },
    subInfoView: {
        marginLeft: spacing.spacing20,
        marginRight: spacing.spacing20,
        flexDirection: 'row',
        alignSelf: 'stretch',
        marginTop: spacing.spacing20,
    },
    txtSubInfoLeft: {
        fontWeight: "normal",
        fontSize: fontSizes.fontSize12,
        justifyContent: 'flex-end',
        fontFamily: "Lato",
        color: colors.color_808080,
        flex: spacing.spacing1,
    },
    primaryButton: {
        marginLeft: spacing.spacing20,
        marginRight: spacing.spacing20,
        marginBottom: spacing.spacing20,
        flex: spacing.spacing1,
        justifyContent: 'flex-end',
        alignSelf: 'stretch',
    }
});
