const paymentFlowStrings = {
    paymentStepOneTitle: "Payment Successful",
    paymentStepOneAmount: "₹ %s1",
    paymentStepOneSubTitlePE: "%s1. Details have been sent to your mobile number %s2 & Email ID %s3",
    paymentStepOneSubTitleF: "For %s1 with cover upto %s2. Details have been sent to your mobile number %s3 & Email ID %s4",
    paymentStepOneSubTitleHS: "%s1 for %s2 Appliances. Details have been sent to your mobile number %s3 & Email ID %s4",
    paymentStepOneSubTitleEW: "%s1. Details have been sent to your mobile number %s2 & Email ID %s3",
    paymentStepTwoTitlePE: "Activate your membership",
    paymentStepTwoTitleF: "Activate your membership",
    paymentStepTwoTitle: "Activate your %s1",
    paymentStepTwoTitleHS: "Schedule inspection",
    paymentStepTwoTitleEW: "Activate your membership",
    paymentStepTwoSubTitlePE: "Complete the activation process before %s1 to avoid membership cancellation.",
    paymentStepTwoSubTitleF: "Complete the activation process before %s1 to avoid membership cancellation.",
    paymentStepTwoSubTitleHS: "Schedule appliance inspection to unlock your plan benefits and risk cover of ₹ %s1",
    paymentStepTwoSubTitleEW: "Complete the activation process before %s1 to avoid membership cancellation.",
    paymentStepTwoSubTitleOSMismatch: "Please download OneAssist app on your %s1 phone and activate your membership before %s2 to avoid cancellation",
    paymentStepTwoNoteHS: "82% customers schedule inspection within 15 mins",
    paymentStepTwoNoteEW: "82% customers activate membership within 10 mins",
    paymentStepTwoNotePE: "82% customers activate membership within 10 mins",
    paymentStepTwoNoteF: "More than 80% customers activate membership within 10 mins",
    paymentActionLabelPE: "Activate Now",
    paymentActionLabelBasicDetials: "Continue",
    paymentActionLabelF: "Activate Now",
    paymentActionLabelHS: "Book a time slot",
    paymentActionLabelEW: "Activate Now",
    paymentUrlPrefixMediumCode: "&mediumCode=app",
    paymentUrlPrefixShowCOD: "&showCOD=true",
    productTermAndCUrl: "https://oneassist.in/ProductTerms/",
    addressTitlePrefilled: "Verify Address (Technician will visit here)",
    addressTitleNotPrefilled: "Add Address (Technician will visit here)",
    technicianText: "Technician details will be sent to you via SMS on your registered number",
    pleaseEnterTheAddress: "Please enter the address.",
    pleaseSelectASlot: "Please select a slot",
    noSlotsAvailable: "Time slots are not available for the selected date. Please choose another date",
    secureDetailsText: "Your data is encrypted. 100% safe and secure.",
    paymentLoaderMessage: "It'll only take a few seconds. Please do not go back or close the app.",
    prePaymentFormStrings: {
        title: "Enter your details",
        action: "Proceed to Payment",
        saveDetail: "Save Details",
        alert: "confirm if you are above 18.",
        alertForIDFencePlanAgreement: "Please agree to terms.",
        checkAge: "I am 18 years above and agree to all",
        tnc: "Terms and Conditions",
        continue: "Continue",
        ProceedToScheduleVisit: "Proceed to schedule visit",
        viewMore: "View more",
        age18TandC: "By continuing, you verify that you are at least 18 years old and agree to these ",
    },
    paymentSubUrls: {
        ONBOARD_CUSTOMER_SUB_URL: "/OASYS/webservice/rest/customer/onboardCustomer?",
        ONBOARD_CUSTOMER_TRIAL_PLAN_URL: "/OASYS/webservice/rest/customer/boardTrialPlan",
        GET_DOCS_TO_UPLOAD_SUB_URL: "/OASYS/getDocsToUpload?activationCode=",
        GET_TEMP_CUST_INFO_SUB_URL: "/OASYS/getTempCustomerInfo?activationCode=",
        GET_WARRANTY_URL: "/OASYS/restservice/wsgetMasterData",
        VALIDATE_EMAIL: "myaccount/api/customer/dedupe",
        CREATE_SERVICE_ORDER: "aggregator/api/customer/order",
        GET_SCHEDULE_VISIT_SLOTS: (serviceRequestType, serviceRequestSourceType, date, pincode, bpCode, buCode, productCode) => {
            return `serviceplatform/api/masterData/availableServiceSlots?serviceRequestType=${serviceRequestType}&serviceRequestDate=${date}&pincode=${pincode}&serviceRequestSourceType=${serviceRequestSourceType}&bpCode=${bpCode}&buCode=${buCode}&productCode=${productCode}`
        },
        GET_SCHEDULE_VISIT_ADDRESS: (customerId, pincode) => {
            return `/myaccount/api/customer/${customerId}/address?addressType=MAILING&pinCode=${pincode}&pincodeServiceability=false`
        },
    },
    paymentFlowComponentTitles: {
        createCustomerComponentTitle: "Enter your details",
        paymentFailComponentTitle: "Payment Status",
        basicDetailsComponentTitle: "Basic Details",
        paymentSuccessStepOneComponentTitle: "Payment Status",
        paymentWebViewComponentTitle: "Review Payment",
        tnCWebViewComponentTitle: "T&C",
        membershipActivationTitle: "Membership Activation",
        applianceDetailsTitle: "Appliance details",

    },
    paymentFlowErrorMessages: {
        submitDetailsErrorMsg: "Unable to submit details. please type again!",
        validPincodeErrorMsg: "Provide valid pincode",
    },
    paymentWebViewStrings: {
        SUCCESS_URL: "paymentSuccess",
        SUCCESS_URL_WITH_REL_NO: "paymentSuccess/?relno=",
        SUCCESS_URL_WITH_SR_NO: "paymentSuccess/?srNo=",
        wsCreateCustomerpaymentsuccess: "wsCreateCustomerpaymentsuccess",
        wsUpgradepaymentsuccess: "wsUpgradepaymentsuccess",
        wsRenewalpaymentsuccess: "renew-success/?relno=",
        wsIDFencePaymentUpdateSuccess: "idfence/membership?subscriberNo=",
        FAILURE_URL: "paymentFailure",
        FAILURE_MSG_URL: "message/?msg=",
        wsPaymentFailure: "wsPaymentFailure",
        wsRenewalpaymentPaymentfailure: "wsRenewalpaymentPaymentfailure",
        wsUpgradepaymentPaymentfailure: "wsUpgradepaymentPaymentfailure",

    },
    paymentFlowServiceTypes: {
        PE: "PE",
        WALLET: "wallet",
        HA: "HS",
        IDFENCE: "IDFence",
    },
    paymentFlowServiceProductCodes: {
        PE_MP01: "MP01",
        PE_LAP: "LAP",
        PE_WR: "WR",
        PE_WP01: "WP01",
        PE_TBL: "TBL",
        HS: "HS",
        WM: "WM",
        SOD: "SOD",
        IDFence: "IDFence"
    },
    productUrlKeyForTAndC: {
        KEY_EW: "EW",
        KEY_ADLD: "ADLD",
        KEY_WALLET: "WP01",
        KEY_SOP: "SOP"
    },
    paymentFlowPlanCatCodes: {
        PLAN_CATEGORY_CODE_HA: "HA",
        PLAN_CATEGORY_CODE_PE: "PE",
        PLAN_CATEGORY_CODE_F: "F",
    },
    paymentFlowActionButtonTitle: {
        retryPaymentButton: "Retry Payment",
        activateNowButton: "Activate Now",
        viewMembershipButton: "View Membership",
        letsbegin: "Let’s begin",
        proceedToNextStep: "Proceed to next step"
    },
    activationSuccessTitle: {
        activated: "Plan successfully activated",
        activatedTrial: "Trial plan successfully activated",
        renewed: "Plan successfully renewed",
        upgraded: "Plan successfully Upgraded",
        successUserHintMsg: "Most of the users activate their membership within 10 mins"
    },
    events: {
        eventWebviewReloadAndBack: "eventWebviewReloadAndBack",
        eventEditFromPayment: "eventEditFromPayment",
    }
}
export default paymentFlowStrings;
