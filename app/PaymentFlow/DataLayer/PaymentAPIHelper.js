import { HTTP_GET, HTTP_POST, executeApi, executeDocUploadRequest } from '../../network/ApiManager';
import {APIData} from '../../../index';
import paymentFlowStrings from '../Constants/Constants';
import { GET_STATE_AND_CITY, SUBMIT_POST_PAYMENT_DETAILS, PAYMENT_FLOW_DOCUMENTS } from '../../Constants/ApiUrls';
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

const getDataFromApiGateway = (apiParams) => {
    let params = APIData;
    apiParams.apiProps = {baseUrl: params?.api_gateway_base_url, header: params?.apiHeader};
    return apiParams;
};

const getUploadDataFromApiGateway = (apiParams) => {
    let params = APIData;
    apiParams.apiProps = { baseUrl: params?.api_gateway_base_url, header: { ...params?.apiHeader, 'Content-Type': 'multipart/form-data' } };
    return apiParams;
};

export const custId = () => {
    return APIData?.customer_id;
};

export const getTempCustInfo = (activationCode, callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: paymentFlowStrings.paymentSubUrls.GET_TEMP_CUST_INFO_SUB_URL + activationCode,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getDocsToUpload = (activationCode, callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: paymentFlowStrings.paymentSubUrls.GET_DOCS_TO_UPLOAD_SUB_URL + activationCode,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const checkPincode = (pincode, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: GET_STATE_AND_CITY,
        requestBody: {
            addressTmp: { pinCode: pincode }
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const submitPostDetails = (postPaymentDetails, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: SUBMIT_POST_PAYMENT_DETAILS,
        requestBody: postPaymentDetails,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const boardTrialPlan = (initialDetails, inputDetails, callback) => {
    let data = parseJSON(initialDetails);
    let customerList = data.customerInfo[0];
    Object.keys(inputDetails).forEach(function (key) {
        customerList[key] = inputDetails[key];
    });
    var customerInfo = [];
    customerInfo.push(customerList);
    data.customerList = customerInfo;
    if (data.orderInfo) {
        data.order = data.orderInfo;
    }
    data.initSystem = 7;
    let apiParams = {
        httpType: HTTP_POST,
        url: paymentFlowStrings.paymentSubUrls.ONBOARD_CUSTOMER_TRIAL_PLAN_URL,
        requestBody: data,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const onboardCustomer = (initialDetails, inputDetails, callback) => {
    let data = parseJSON(initialDetails);
    let customerList = data.customerInfo[0];
    let assetInfo = customerList?.assetInfo?.[0];
    if (inputDetails.warrantyPeriod && assetInfo) {
        assetInfo.warrantyPeriod = inputDetails.warrantyPeriod;
        customerList.assetInfo = [assetInfo];
    }
    Object.keys(inputDetails).forEach(function (key) {
        if (key !== "warrantyPeriod") {
            customerList[key] = inputDetails[key];
        }
    });
    let customerInfo = [];
    customerInfo.push(customerList);
    data.customerInfo = customerInfo;

    let apiParams = {
        httpType: HTTP_POST,
        url: paymentFlowStrings.paymentSubUrls.ONBOARD_CUSTOMER_SUB_URL,
        requestBody: data,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
}

export const getWarrantyPeriod = (callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: paymentFlowStrings.paymentSubUrls.GET_WARRANTY_URL,
        requestBody: {
            subParam: "HT",
            masterName: "Warranty_Period"
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const uploadAssetDocument = (selectedImage, fileType, callback) => {
    const data = new FormData();
    data.append("uploadedFile", { uri: selectedImage.localImgUrl, type: fileType, name: selectedImage.fileName });
    data.append("displayName", selectedImage.displayName);
    data.append("fileName", selectedImage.docName);
    data.append("docMandatory", selectedImage.docMandatory);
    data.append("activityReferenceId", selectedImage.activityReferenceId);
    data.append("docFileContentType", selectedImage.docFormat);
    data.append("name", selectedImage.name);

    let apiParams = {
        httpType: HTTP_POST,
        url: PAYMENT_FLOW_DOCUMENTS,
        requestBody: data,
        callback: callback,
    };

    executeDocUploadRequest(getUploadDataFromApiGateway(apiParams));
}

export const getSlots = (serviceRequestType, serviceRequestSourceType, date, pincode,  bpCode, buCode, productode, callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: paymentFlowStrings.paymentSubUrls.GET_SCHEDULE_VISIT_SLOTS(serviceRequestType, serviceRequestSourceType, date, pincode, bpCode, buCode, productode),
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getAddress = (pincode, callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: paymentFlowStrings.paymentSubUrls.GET_SCHEDULE_VISIT_ADDRESS(custId(), pincode),
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const validateEmail = (emailId, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: paymentFlowStrings.paymentSubUrls.VALIDATE_EMAIL,
        requestBody: {
            emailId: emailId,
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const createServiceOrder = (userDetails, date, address, slot, addressId, callback) => {
    let requestData = deepCopy(APIData?.data);
    let custInfoFirstItem = requestData?.customerInfo[0];
    let thirdPartyParam = requestData?.thirdPartyParams;
    thirdPartyParam['scheduleSlotStartDateTime'] = date + ' ' + slot?.startTime + ':00';
    thirdPartyParam['scheduleSlotEndDateTime'] = date + ' ' + slot?.endTime + ':00';
    thirdPartyParam['customerId'] = custInfoFirstItem?.custId;
    thirdPartyParam['createdBy'] = custInfoFirstItem?.custId;
    let serviceRequestAddressDetails = {
        addressLine1: address,
        pincode: requestData?.addressInfo[0]?.pinCode,
        addresseeFullName: userDetails.firstName,
        email: userDetails.emailId,
        mobileNo: userDetails.mobileNumber,
    };
    thirdPartyParam['serviceRequestAddressDetails'] = serviceRequestAddressDetails;
    custInfoFirstItem['addressInfo'] = {addrLine1: address, pincode: requestData?.addressInfo[0]?.pinCode, addressId: addressId};
    custInfoFirstItem['name'] = userDetails?.firstName;
    custInfoFirstItem['emailId'] = userDetails?.emailId;
    delete custInfoFirstItem['assetInfo'];
    delete custInfoFirstItem['relationship'];
    delete custInfoFirstItem['firstName'];
    requestData['customerInfo'] = custInfoFirstItem;
    delete requestData['addressInfo'];
    delete requestData['custId'];
    delete requestData['customerOrderInfo'];
    delete requestData['initiatingSystem'];
    delete requestData['orderInfo']?.['planCode'];
    delete requestData['paymentInfo']?.['viaLink'];

    let apiParams = {
        httpType: HTTP_POST,
        url: paymentFlowStrings.paymentSubUrls.CREATE_SERVICE_ORDER,
        requestBody: requestData,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const createServiceOrderForSlots = (userDetails, date, slot, callback) => {
    let requestData = deepCopy(APIData?.data);
    let thirdPartyParam = requestData?.thirdPartyParams;
    requestData['orderInfo'] = {orderId: requestData?.orderId};
    thirdPartyParam['scheduleSlotStartDateTime'] = date + ' ' + slot.startTime + ':00';
    thirdPartyParam['scheduleSlotEndDateTime'] = date + ' ' + slot.endTime + ':00';
    delete requestData['paymentStatus'];
    delete requestData['orderId'];
    delete thirdPartyParam['referenceOrderNo'];

    let apiParams = {
        httpType: HTTP_POST,
        url: paymentFlowStrings.paymentSubUrls.CREATE_SERVICE_ORDER,
        requestBody: requestData,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};


