import { createStackNavigator } from "react-navigation";
import PaymentSuccessStepOneAction from "../Actions/PaymentSuccessStepOneAction";
import basicDetailsAction from "../Actions/BasicDetailsAction";
import MembershipActivationComponent from "../Components/MembershipActivationComponent";
import ApplianceDetailsAction from "../Actions/ApplianceDetailsAction";
import BasicDetailsAction from "../Actions/BasicDetailsAction"
import createCustomerAction from "../Actions/CreateCustomerAction";
import WAPaymentSuccessComponent from "../Actions/PaymentSuccessAction";
import PaymentWebViewComponent from "../Components/Payment/PaymentWebViewComponent";
import TnCWebViewComponent from "../Components/TnCWebViewComponent";
import colors from "../../Constants/colors";
import MembershipActivationAction from "../Actions/MembershipActivationAction";
import ScheduleVisitComponent from '../Components/ScheduleVisitComponent';
import ServicePaymentSuccessComponent from '../Components/ServicePaymentSuccessComponent';
import ServicePaymentPendingComponent from '../Components/ServicePaymentPendingComponent';
import OAWebView from "../../CustomComponent/webview/OAWebViewAction";
import ChatWebViewComponent from "../../Components/ChatWebViewComponent";
const navigator = createStackNavigator({
  CreateCustomerComponent: {
    screen: createCustomerAction,
    navigationOptions: {
      gesturesEnabled: false,
      title: "Enter your details",
      headerTitleStyle: {
        color: colors.charcoalGrey,
      },
      headerStyle: {
        backgroundColor: colors.white
      },
      headerTintColor: colors.blue
    }
  },
  TnCWebViewComponent: {
    screen: TnCWebViewComponent,
    navigationOptions: {
      title: "Terms & Conditions",
      gesturesEnabled: false,
    }
  },
  PaymentSuccessStepOneComponent: {
    screen: PaymentSuccessStepOneAction,
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    }
  },
  PaymentWebViewComponent: {
    screen: PaymentWebViewComponent,
    navigationOptions: {
      title: "Payment Review",
      gesturesEnabled: false,
    }
  },
  WAPaymentSuccessComponent: {
    screen: WAPaymentSuccessComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    }
  },
  BasicDetailsComponent: {
    screen: BasicDetailsAction,
    navigationOptions: {
      gesturesEnabled: false,
    }
  },
  OAWebView: {
    screen: ChatWebViewComponent,
    navigationOptions: {
      gesturesEnabled: false
    }
  },
  MembershipActivationComponent: {
    screen: MembershipActivationAction,
    navigationOptions: {
      gesturesEnabled: false,
    }
  },
  ApplianceDetailsComponent: {
    screen: ApplianceDetailsAction,
    navigationOptions: {
      gesturesEnabled: false,
    }
  },
  ScheduleVisitComponent: {
    screen: ScheduleVisitComponent
  },
  ServicePaymentSuccessComponent: {
    screen: ServicePaymentSuccessComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    }
  },
  ServicePaymentPendingComponent: {
    screen: ServicePaymentPendingComponent,
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    }
  },
}, {
  mode: 'modal',
});

export default navigator;
