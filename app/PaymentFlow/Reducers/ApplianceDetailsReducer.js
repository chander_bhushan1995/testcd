import * as ActionTypes from "../Actions/ActionTypes";
import { isValidString, validateFormFields } from "../../commonUtil/Validator";
import * as FirebaseKeys from "../../Constants/FirebaseConstants"
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

const initialState = {
  displayedComponents: [],
  arrDocsForUpload: [],
  type: null,
  assetInfo: null,
  formFieldsInputValid: false
};

const applianceDetailsReducer = (state = initialState, action) => {

  switch (action.type) {
    case ActionTypes.getFormFields_hs_ew_ad:
      let formFieldsJson = action.formFieldsJson;
      let prepostJson = parseJSON(formFieldsJson);
      let postJson = prepostJson['postboarding'];
      let response = postJson[FirebaseKeys.HS_EW_AD];
      let arrDocsForUpload = postJson[FirebaseKeys.HS_EW_IMAGES];
      let displayedComponentsNew = [];
      for (let i = 0; i < response.length; i++) {
        let component = response[i];
        let displayedComponent = {
          index: i,
          paramName: component.paramName,
          title: component.label,
          placeholder: component.placeHolder,
          maxLength: component.maxLength,
          keyboardType: component.keyboardType,
          inputValidation: component.inputValidation,
          inputValue: "",
          errorMessage: component.errorMessage,
          isMultiLines: component.isMultiLines,
          height: component.height
        };
        displayedComponentsNew.push(displayedComponent);
      }
      return {
        ...state,
        type: ActionTypes.getFormFields,
        displayedComponents: displayedComponentsNew,
        arrDocsForUpload: arrDocsForUpload
      };
    case ActionTypes.validateApplianceDetalsFormFields:
      let validationStatus = validateFormFields(state.displayedComponents);

      return {
        ...state,
        type: ActionTypes.validateApplianceDetalsFormFields,
        assetInfo: validationStatus.userDetailsString,
        displayedComponents: validationStatus.validatedComponents,
        formFieldsInputValid: validationStatus.isValid
      }
    case ActionTypes.validateApplianceDetailsFormInput:

      let dispComponents = state.displayedComponents;
      let dispComponent = dispComponents[action.index];
      dispComponent.inputValue = action.text;
      dispComponents[action.index] = dispComponent;
      return {
        ...state,
        type: ActionTypes.validateApplianceDetailsFormInput,
        displayedComponents: deepCopy(dispComponents),
      };
    case ActionTypes.resetViewType:

      return { ...state,type:ActionTypes.resetViewType };


    default:
      return { ...state };
  }
};
export default applianceDetailsReducer;
