import * as ActionTypes from "../Actions/ActionTypes";
import fontSizes from "../../Constants/FontSizes";
import colors from '../../Constants/colors';
import * as Validator from "../../commonUtil/Validator";
import * as FirebaseKeys from "../../Constants/FirebaseConstants"
import * as FormFieldProvider from "../../commonUtil/FormFieldProvider";
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

const initialState = {
    isError: false,
    errorType: null,
    errorMessage: "",
    getDocLoaderShowing: true,
    arrDocsForUpload: [],
    radioItems:
        [
            {
                label: 'Male',
                size: fontSizes.fontSize14,
                color: colors.blue,
                selected: true,
            },
            {
                label: 'Female',
                color: colors.charcoalGrey,
                size: fontSizes.fontSize14,
                selected: false,
            },
            {
                label: 'Others',
                size: fontSizes.fontSize14,
                color: colors.charcoalGrey,
                selected: false
            }
        ],
    isLoading: false,
    formFieldsValid: false,
    type: null,
    pincodeDetails: {},
    // selectedItem: 'Male',
    displayedComponents: [],
    addressTmp: null,
    refreshedTempCustomerDetails: null,
    relno: null,
    productCode: null,
};

const basicDetailsReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.getPostBoardingFormFields:
            let prepostJson = action.prepostJson;
            let displayedComponentsNew = [];
            let pincodeDetails = parseJSON(action.pincodeDetails)

            switch (action.firebaseKey) {
                case FirebaseKeys.wallet:
                    displayedComponentsNew = FormFieldProvider.getPostboardingFFWallet(prepostJson, pincodeDetails)
                    // displayedComponentsNew = FormFieldProvider.getPostboardingFFPE_LT(prepostJson, pincodeDetails)
                    break;
                case FirebaseKeys.PE:
                    displayedComponentsNew = FormFieldProvider.getPostboardingFFPE(prepostJson, pincodeDetails);
                    break;
                case FirebaseKeys.HomeAppliance:
                    displayedComponentsNew = FormFieldProvider.getPostboardingFFHS_HA(prepostJson, pincodeDetails)
                    break;
                case FirebaseKeys.PE_Laptop:
                    displayedComponentsNew = FormFieldProvider.getPostboardingFFPE_LT(prepostJson, pincodeDetails)
                    break;
                default: //wallet
                    displayedComponentsNew = FormFieldProvider.getPostboardingFFWallet(prepostJson, pincodeDetails)
            }

            return {
                ...state,
                type: ActionTypes.getFormFields,
                pincodeDetails: pincodeDetails,
                displayedComponents: displayedComponentsNew
            }
        case ActionTypes.validateInput:
            let displayedComponents = state.displayedComponents;
            let displayedComponent = displayedComponents[action.index];
            displayedComponent.inputValue = action.text;
            displayedComponents[action.index] = displayedComponent;
            return {
                ...state,
                displayedComponents: deepCopy(displayedComponents),
            }
        case ActionTypes.checkingPincode:
            return {
                ...state,
                isLoading: true,
            }

        case ActionTypes.checkPincodeFailure:
            let components = state.displayedComponents;
            let component = components[action.index];
            component.errorMessage = action.text;
            components[action.index] = component;
            return {
                ...state,
                isLoading: false,
                displayedComponents: deepCopy(components),
            }
        case ActionTypes.checkPincodeSuccess:

            let addressTmp = action.data;
            return {
                ...state,
                type: ActionTypes.checkPincodeSuccess,
                isLoading: true,
                addressTmp: addressTmp
            }

        case ActionTypes.submitPostDetailsSuccess:
            return {
                ...state,
                type: ActionTypes.submitPostDetailsSuccess,
                isLoading: false,
            }
        case ActionTypes.submitPostDetailsFailure:
            return {
                ...state,
                type: ActionTypes.submitPostDetailsFailure,
                isLoading: false,
            }

        case ActionTypes.fetchDocsToUploadSuccess:
            initialState.arrDocsForUpload.length = 0;
            let customerDetailsArr = action.data;
            let deviceDetails = customerDetailsArr["deviceDetails"][0];
            if (deviceDetails.length > 0) {
                let activityReferenceId = customerDetailsArr["activityReferenceId"];
                let verificationDocuments = deviceDetails["verificationDocuments"];
                let count = 0;
                for (let index in verificationDocuments) {
                    let displayName = verificationDocuments[index]["displayName"];
                    let docMandatory = verificationDocuments[index]["docMandatory"];
                    let name = verificationDocuments[index]["name"];
                    let panelMessage = verificationDocuments[index]["panelMessage"];
                    let guideMessage = verificationDocuments[index]["guideMessage"];
                    let value = verificationDocuments[index]["value"];
                    let item = {
                        index: count,
                        displayName: displayName,
                        docMandatory: docMandatory,
                        name: name,
                        panelMessage: panelMessage,
                        guideMessage: guideMessage,
                        isShowLoader: false,
                        localImgUrl: "",
                        docName: "",
                        docFormat: "",
                        value: value,
                        activityReferenceId: activityReferenceId,
                    };
                    count++;
                    initialState.arrDocsForUpload.push(item);
                }
                return {
                    ...state,
                    getDocLoaderShowing: false,
                    arrDocsForUpload: initialState.arrDocsForUpload,
                }
            }
        case ActionTypes.fetchDocsToUploadFail:
            return {
                ...state,
                getDocLoaderShowing: false,
            };
        case ActionTypes.fetchUserInfoSuccess:
            let tempCustomerDetails = deepCopy(action.data);
            let customerDetails = tempCustomerDetails["customerDetails"][0]
            let productCode = customerDetails["productCode"][0];
            return {
                ...state,
                refreshedTempCustomerDetails: action.data,
                productCode: productCode,
                type: ActionTypes.fetchUserInfoSuccess,
            }
        case ActionTypes.fetchUserInfoFail:
            return { ...state };
        default:
            return {
                ...state,
                getDocLoaderShowing: false,
            };
    }
};
export default basicDetailsReducer;
