import * as ActionTypes from '../Actions/ActionTypes';
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

const initialState = {
    type: '',
    isLoading: true,
    userCreated: false,
    isApiError: false,
    errorApiMessage: '',
    errorApiType: null,
    paynowlink: '',
    displayedComponents: [],
    flowServiceType: null,
    productTermAndCUrl: '',
    formFieldsValid: false,
    checkboxChecked: true,
    agreedFDTerms: true,
    primaryButtonEnabled: true,
    isApiCalled: false,
    toggleCheckbox: false,
    userDetailsString: {},
    warranties: [],
    warrantyRequired: false,
    formFieldsJson: null,
    customerInfo: {},
    customerOrderInfo: {},
    idfenceSortTerm: '',
    viewMoreTitle: '',
    idfenceTerms: [],
    trialHeaderData: {
        title: 'Free Reward: ID Fence 30-Days Free Trial Plan',
        description: 'We need these details to activate your reward.',
    },
    callValidateEmail: true,
};
import {
    validateFormFields,
} from '../../commonUtil/Validator';
import * as FormFieldProvider from '../../commonUtil/FormFieldProvider';
import * as FirebaseKeys from '../../Constants/FirebaseConstants';
import paymentFlowStrings from '../Constants/Constants';

const createCustomerReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.getTermsAndConditionJson:
            let termsAndConditionString = JSON.stringify(action.data);
            let termsAndConditionJSON = parseJSON(termsAndConditionString);
            let productTerms = termsAndConditionJSON['product_terms'];
            productTerms = parseJSON(productTerms);
            let baseUrl = productTerms['baseUrl'];
            let subPart = productTerms['subPart'];
            let userDataObj = action.initialProps.data;
            let userDataJSON = parseJSON(userDataObj);
            let customerInfoObject;
            let productCode = '';
            if (userDataJSON !== null && userDataJSON !== undefined) {
                customerInfoObject = userDataJSON['customerInfo'];
                if (customerInfoObject !== undefined && customerInfoObject[0]?.hasOwnProperty('assetInfo')) {
                    let assetInfoObject = customerInfoObject[0]['assetInfo'];
                    productCode = assetInfoObject[0]['productCode'];
                }
            }

            let termsAndCUrl = paymentFlowStrings.productTermAndCUrl;
            let productUrlJSON = '';
            switch (state.flowServiceType) {
                case FirebaseKeys.HomeAppliance:
                    productUrlJSON = productTerms[paymentFlowStrings.paymentFlowServiceTypes.HA];
                    if (action.initialProps.hasEW) {
                        termsAndCUrl = baseUrl + subPart + productUrlJSON[paymentFlowStrings.productUrlKeyForTAndC.KEY_EW];
                    } else if (action.initialProps?.isHA_SOP) {
                        termsAndCUrl = baseUrl + subPart + productUrlJSON[paymentFlowStrings.productUrlKeyForTAndC.KEY_SOP];
                    } else {
                        termsAndCUrl = baseUrl + subPart + productUrlJSON[paymentFlowStrings.productUrlKeyForTAndC.KEY_ADLD];
                    }
                    break;
                case FirebaseKeys.PE:
                    if (productTerms.hasOwnProperty(productCode)) {
                        productUrlJSON = productTerms[productCode];
                    } else {
                        productUrlJSON = productTerms[paymentFlowStrings.paymentFlowServiceProductCodes.PE_MP01];
                    }
                    if (action.initialProps.hasEW) {
                        termsAndCUrl = baseUrl + subPart + productUrlJSON[paymentFlowStrings.productUrlKeyForTAndC.KEY_EW];
                    } else {
                        termsAndCUrl = baseUrl + subPart + productUrlJSON[paymentFlowStrings.productUrlKeyForTAndC.KEY_ADLD];
                    }
                    break;
                case FirebaseKeys.wallet:
                    productUrlJSON = productTerms[productCode];
                    termsAndCUrl = baseUrl + subPart + productUrlJSON[paymentFlowStrings.productUrlKeyForTAndC.KEY_WALLET];
                    break;
                case FirebaseKeys.SOD:
                    productUrlJSON = productTerms[paymentFlowStrings.paymentFlowServiceProductCodes.SOD];
                    termsAndCUrl = baseUrl + subPart + productUrlJSON[paymentFlowStrings.paymentFlowServiceProductCodes.SOD];
                    break;
                case FirebaseKeys.IDFence:
                    productUrlJSON = productTerms[paymentFlowStrings.paymentFlowServiceProductCodes.IDFence];
                    termsAndCUrl = baseUrl + subPart + productUrlJSON[paymentFlowStrings.paymentFlowServiceProductCodes.IDFence];
                    break;
                default:
                    break;
            }
            return {
                ...state,
                productTermAndCUrl: termsAndCUrl,
                type: ActionTypes.getTermsAndConditionJson,
            };
        case ActionTypes.getFormFields:
            let formFieldsJson = action.formFieldsJson;
            let prepostJson = parseJSON(formFieldsJson.pre_post_onboarding_form_fields);
            let preJson = prepostJson['preboarding'];
            let service_Type = action.initialProps.service_Type;
            let displayedComponentsNew = [];
            //plan ha extended warranty
            let hasEW = action.initialProps.hasEW || action.initialProps.hasEW === '1';
            //user's initial details
            let preBoadingDetails = action.initialProps.data;
            let data = parseJSON(preBoadingDetails);
            let filterKey = null;
            switch (service_Type) {
                case FirebaseKeys.HomeAppliance:
                    filterKey = FirebaseKeys.HomeAppliance;
                    break;
                case FirebaseKeys.PE:
                    filterKey = FirebaseKeys.PE;
                    break;
                case FirebaseKeys.wallet:
                    filterKey = FirebaseKeys.wallet;
                    break;
                case FirebaseKeys.SOD:
                    filterKey = FirebaseKeys.SOD;
                    break;
                case FirebaseKeys.IDFence:
                    filterKey = FirebaseKeys.IDFence;
                    break;
                default: //wallet
                    filterKey = FirebaseKeys.wallet;
            }
            if (action.initialProps.isTrialPlan) {
                filterKey = FirebaseKeys.Trial;
            }
            let response = preJson[filterKey];
            let idfenceTerms = preJson[FirebaseKeys.IDFenceTerms];
            let trialHeaderData = preJson[FirebaseKeys.trialHeaderData];
            if (trialHeaderData === null || trialHeaderData === undefined) {
                trialHeaderData = state.trialHeaderData;
            }
            if (!hasEW && (response !== null && response !== undefined)) {
                //filter Extended warranty field
                response = response.filter((value) => {
                    return value.paramName !== 'warrantyPeriod';
                });
            }
            let callValidateEmail = true;
            if (data !== null && data !== undefined && data.hasOwnProperty('customerInfo')) {
                let customerInfoTemp = data.customerInfo;
                let custInfoFirstItem = customerInfoTemp[0];
                if(action.initialProps.service_Type === FirebaseKeys.SOD){
                    for (let i = 0; i < response.length; i++){
                        if(response[i]?.paramName === 'emailId' && custInfoFirstItem?.emailId?.length > 0){
                            let emailData = response[i];
                            emailData.editable = false;
                            response[i] = emailData;
                        }
                    }
                    displayedComponentsNew = FormFieldProvider.getComponents(response, custInfoFirstItem);
                }else{
                    displayedComponentsNew = FormFieldProvider.getComponents(response, custInfoFirstItem);
                }
                callValidateEmail = !(custInfoFirstItem?.emailId?.length > 0);
            }
            let moreTerms = idfenceTerms?.viewMoreTitle ?? 'View more';
            return {
                ...state,
                type: ActionTypes.getFormFields,
                flowServiceType: service_Type,
                warrantyRequired: hasEW,
                displayedComponents: displayedComponentsNew,
                isLoading: hasEW,
                idfenceSortTerm: idfenceTerms.shortTerm,
                viewMoreTitle: moreTerms,
                idfenceTerms: idfenceTerms.terms,
                trialHeaderData: trialHeaderData,
                callValidateEmail: callValidateEmail,
            };
        case ActionTypes.validatePreboardingFormFields:
            let validationStatus = validateFormFields(state.displayedComponents);
            return {
                ...state,
                type: ActionTypes.validatePreboardingFormFields,
                userDetailsString: validationStatus.userDetailsString,
                displayedComponents: validationStatus.validatedComponents,
                formFieldsValid: validationStatus.isValid,
            };
        case ActionTypes.toggleCheckbox:
            return {
                ...state,
                type: ActionTypes.toggleCheckbox,
                checkboxChecked: !state.checkboxChecked,
            };
        case ActionTypes.toggleFDTermsCheckbox:
            return {
                ...state,
                type: ActionTypes.toggleCheckbox,
                agreedFDTerms: !state.agreedFDTerms,
            };
        case ActionTypes.createCustomerSuccess:
            let createCustomerResponse = action.data;
            let customerOrderInfo = {
                customerDetails: [createCustomerResponse.customerOrderInfo],
            };
            let paynowLink = createCustomerResponse['payNowLink'];
            return {
                ...state,
                type: ActionTypes.createCustomerSuccess,
                paynowlink: paynowLink,
                userCreated: true,
                primaryButtonEnabled: true,
                customerOrderInfo: customerOrderInfo,
            };
            case ActionTypes.createCustomerActionReset:
            return {
                ...state,
                type: ActionTypes.createCustomerActionReset,
            };
        case ActionTypes.createCustomerSuccessTrial:
            let trialData = action.data;
            let customerInfo = {};
            if (trialData !== null && trialData !== undefined) {

                customerInfo['price'] = 0;
                customerInfo['planName'] = '';
                customerInfo['mobileNo'] = trialData.mobileNo;
                customerInfo['emailId'] = trialData.emailId;
                customerInfo['custId'] = '';

                let membershipInfo = trialData['membershipInfo'];
                let otherInfo = trialData['custmerInfo'];

                if (membershipInfo !== null && membershipInfo !== undefined) {
                    customerInfo.planName = membershipInfo.planName;
                    customerInfo.custId = membershipInfo.primaryCustomerId;
                }
                if (otherInfo !== null && otherInfo !== undefined) {
                    customerInfo.mobileNo = otherInfo.contactNo;
                    customerInfo.emailId = otherInfo.emailId;
                }
            }
            let customerDetails = {'customerDetails': [customerInfo]};
            return {
                ...state,
                type: ActionTypes.createCustomerSuccessTrial,
                customerInfo: customerDetails,
                userCreated: true,
                primaryButtonEnabled: true,
            };
        case ActionTypes.createCustomerInProgress:
            return {
                ...state,
                type: ActionTypes.createCustomerInProgress,
                primaryButtonEnabled: false,
            };
        case ActionTypes.createCustomerFailure:
            let errorResponse = action.data;
            let errorsArr = errorResponse['errors'];
            let firstErrorsJson = errorsArr[0];
            let errorMsg = firstErrorsJson['message'];
            return {
                ...state,
                type: ActionTypes.createCustomerFailure,
                isLoading: false,
                errorApiMessage: errorMsg,
                isApiError: true,
                primaryButtonEnabled: true,
            };
        case ActionTypes.validateEmailSuccess:
            return {
                ...state,
                type: ActionTypes.validateEmailSuccess,
                primaryButtonEnabled: true,
            };
        case ActionTypes.validateEmailFailure:
            let errorMessage = action.data?.error[0]?.message;
            return {
                ...state,
                type: ActionTypes.createCustomerFailure,
                isLoading: false,
                errorApiMessage: errorMessage,
                isApiError: true,
                primaryButtonEnabled: true,
            };
        case ActionTypes.getWarrantySuccess:
            let getWarrantyResponse = action.data;
            let warranties = getWarrantyResponse['wSmasterInfoDTOlist'];
            return {
                ...state,
                type: ActionTypes.getWarrantySuccess,
                isLoading: false,
                warranties: warranties,
            };
        case ActionTypes.getWarrantyInProgress:
            return {
                ...state,
                type: ActionTypes.getWarrantyInProgress,
                isLoading: true,
            };
        case ActionTypes.getWarrantyFailure:
            let errResponse = action.data;
            let errArr = errResponse['errors'];
            let firstErrJson = errArr[0];
            let errMsg = firstErrJson['message'];
            return {
                ...state,
                type: ActionTypes.getWarrantyFailure,
                isLoading: false,
                errorApiMessage: errMsg,
            };
        case ActionTypes.validatePreBoardFormInput:
            let displayedComponents = state.displayedComponents;
            let displayedComponent = displayedComponents[action.index];
            displayedComponent.inputValue = action.text;
            displayedComponents[action.index] = displayedComponent;
            return {
                ...state,
                type: ActionTypes.validatePreBoardFormInput,
                displayedComponents: deepCopy(displayedComponents),
            };
        case ActionTypes.getFormFieldsJson:
            return {
                ...state,
                type: ActionTypes.getFormFieldsJson,
                formFieldsJson: action.data,
            };
    }
    return state;
};
export default createCustomerReducer;
