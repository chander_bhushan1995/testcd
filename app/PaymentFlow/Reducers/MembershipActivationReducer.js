import * as ActionTypes from "../Actions/ActionTypes";

const initialState = {
    stageCount: 1,
    tempCustInfoData: null,
    applianceName: "",
}

const membershipActivationReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.fetchTempCustInfoSuccess:
            return {
                ...state,
                tempCustInfoData: action.data,
            }
        case ActionTypes.fetchTempCustInfoFail:
            return { ...state };
        case ActionTypes.getApplianceName:
            return {
                ...state,
                applianceName: action.applianceName,
            }
        default: return { ...state }
    }
}
export default membershipActivationReducer;
