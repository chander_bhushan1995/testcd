import { gerRelationshipNumber } from "../Actions/ActionTypes";
const initialState = {
    relno: "",
};

const paymentSuccessReducer = (state = initialState, action) => {
    switch (action.type) {
        case gerRelationshipNumber:
            return {
                ...state,
                relno: action.relno,

            }
        default:
            return state;
    }
};

export default paymentSuccessReducer;
