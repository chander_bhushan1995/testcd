import { fetchUserInfoSuccess, fetchUserInfoFail } from "../Actions/ActionTypes";
import paymentFlowStrings from "../Constants/Constants";
import * as Validator from "../../commonUtil/Validator";
import * as Formatter from "../../commonUtil/Formatter";
import { PLATFORM_OS } from "../../Constants/AppConstants";
import { Platform } from "react-native";
import * as Store from '../../commonUtil/Store';
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

const initialState = {
  isLoading: true,
  tempCustInfoData: null,
  productCode: null,
  planCategory: null,
  actCode: null,
  isPlanServiceEW: false,
  isTaskInspection: false,
  type: null,
  screenData: {
    amount: 0,
    stepOneTitle: null,
    stepOneSubTitle: null,
    stepTwoTitle: null,
    stepTwoSubTitle: null,
    btnLabel: null,
  },
};

const paymentSuccessStepOneReducer = (state = initialState, action) => {
  switch (action.type) {
    case fetchUserInfoSuccess:
      let customerDetailsArr = deepCopy(action.data);
      let customerDetails = customerDetailsArr["customerDetails"][0]
      let planServicesArr = customerDetails["planServices"];
      let maxQtyAllowed = customerDetails["maxQtyAllowed"];
      let deviceMake=customerDetails["deviceMake"];
      initialState.actCode = action.activationCode;
      initialState.planCategory = customerDetails["categories"][0];
      let isInspectionService = planServicesArr.includes('WHC_INSPECTION') ;

      // check plan service for EW
      for (var index in planServicesArr) {
        let productCodeIns = planServicesArr[index];
        if (productCodeIns === 'Extended Warran') {
          initialState.isPlanServiceEW = true;
          break;
        }
      }
      let productC = customerDetails["productCode"][0];
      let numOfProduct = planServicesArr.length;
      if (isInspectionService) {
        numOfProduct = numOfProduct - 1;
      }

      if(customerDetailsArr.hasOwnProperty('task')){
        let userTaskObj = customerDetailsArr["task"][0];
          if (typeof userTaskObj !== "undefined") {
            let userTaskName = userTaskObj["name"];
            if (userTaskName == 'INSPECTION') {
              initialState.isTaskInspection = true;
              productC = "HS";
            }
          }
        }

      initialState.productCode = productC;

      /// save response in async storage: will be used when user land directly to the final step of the payment
      let basicDetails = {
        addrLine1: customerDetails["addrLine1"],
        pinCode: customerDetails["pinCode"]
      }
      Store.setCustomerBasicDetails(JSON.stringify(basicDetails))

      let productCodeIsMP01 = productC.toUpperCase() === paymentFlowStrings.paymentFlowServiceProductCodes.PE_MP01;
      let productCodeIsLAP = productC.toUpperCase() === paymentFlowStrings.paymentFlowServiceProductCodes.PE_LAP;
      let productCodeIsWR = productC.toUpperCase() === paymentFlowStrings.paymentFlowServiceProductCodes.PE_WR;
      let productCodeIsF = productC.toUpperCase() === paymentFlowStrings.paymentFlowServiceProductCodes.PE_WP01;
      initialState.screenData.amount = Formatter.parameterizedString(paymentFlowStrings.paymentStepOneAmount, customerDetails["amount"]);
      initialState.screenData.stepOneTitle = paymentFlowStrings.paymentStepOneTitle;
      if (initialState.planCategory == "PE") {
        //productCodeIsMP01
        // if (productCodeIsMP01) {
        initialState.screenData.stepTwoNote = paymentFlowStrings.paymentStepTwoNotePE
          initialState.screenData.stepTwoTitle = paymentFlowStrings.paymentStepTwoTitlePE;
        // } else {
        //   initialState.screenData.stepTwoTitle = Formatter.parameterizedString(paymentFlowStrings.paymentStepTwoTitle, customerDetails["planName"]);
        // }
        initialState.screenData.stepOneSubTitle = Formatter.parameterizedString(paymentFlowStrings.paymentStepOneSubTitlePE,
          customerDetails["planName"],
          customerDetails["mobileNo"],
          customerDetails["emailId"]
        );
        let isPlanForDiffPlatform=((Platform.OS === PLATFORM_OS.android && deviceMake ==='Apple') ||
           (Platform.OS === PLATFORM_OS.ios && deviceMake !=='Apple'));
        if (productCodeIsMP01) {
          if(isPlanForDiffPlatform){
            initialState.screenData.btnLabel = paymentFlowStrings.paymentActionLabelBasicDetials;
          }else{
            initialState.screenData.btnLabel = paymentFlowStrings.paymentActionLabelPE;
          }
        } else if (productCodeIsLAP || productCodeIsWR) {
          initialState.screenData.btnLabel = paymentFlowStrings.paymentActionLabelPE;
        } else {
          initialState.screenData.btnLabel = paymentFlowStrings.paymentActionLabelPE;
        }

        if(isPlanForDiffPlatform && productCodeIsMP01 && !initialState.isPlanServiceEW){
          initialState.screenData.stepTwoSubTitle = Formatter.parameterizedString(paymentFlowStrings.paymentStepTwoSubTitleOSMismatch,
            deviceMake,
            Formatter.formatMemEndDate(customerDetails["uploadDocsLastDate"]));
        }else{
          initialState.screenData.stepTwoSubTitle = Formatter.parameterizedString(paymentFlowStrings.paymentStepTwoSubTitlePE,
            Formatter.formatMemEndDate(customerDetails["uploadDocsLastDate"]));
        }
      } else if (productCodeIsF) {
        initialState.screenData.stepTwoTitle = paymentFlowStrings.paymentStepTwoTitleF;
        initialState.screenData.stepTwoNote = paymentFlowStrings.paymentStepTwoNoteF
        let planCoverAmount = customerDetails["coverAmount"];
        planCoverAmount = Validator.isValidString(planCoverAmount) ? Formatter.formatCoverAmount(planCoverAmount) : 0;
        initialState.screenData.stepOneSubTitle = Formatter.parameterizedString(paymentFlowStrings.paymentStepOneSubTitleF,
          customerDetails["planName"],
          planCoverAmount,
          customerDetails["mobileNo"],
          customerDetails["emailId"]
        );
        initialState.screenData.btnLabel = paymentFlowStrings.paymentActionLabelF;
        initialState.screenData.stepTwoSubTitle = Formatter.parameterizedString(paymentFlowStrings.paymentStepTwoSubTitleF,
          Formatter.formatMemEndDate(customerDetails["uploadDocsLastDate"]));
      } else {
        if (initialState.isPlanServiceEW) {
          initialState.screenData.stepTwoNote = paymentFlowStrings.paymentStepTwoNoteEW
          initialState.screenData.stepTwoTitle = paymentFlowStrings.paymentStepTwoTitleEW;
          initialState.screenData.stepOneSubTitle = Formatter.parameterizedString(paymentFlowStrings.paymentStepOneSubTitleEW,
            customerDetails["planName"],
            customerDetails["mobileNo"],
            customerDetails["emailId"]
          );
          initialState.screenData.btnLabel = paymentFlowStrings.paymentActionLabelEW;
          initialState.screenData.stepTwoSubTitle = Formatter.parameterizedString(paymentFlowStrings.paymentStepTwoSubTitleEW,
            Formatter.formatMemEndDate(customerDetails["uploadDocsLastDate"]));
        } else {
          initialState.screenData.stepTwoNote = paymentFlowStrings.paymentStepTwoNoteHS
          initialState.screenData.stepTwoTitle = paymentFlowStrings.paymentStepTwoTitleHS;
          initialState.screenData.stepOneSubTitle = Formatter.parameterizedString(paymentFlowStrings.paymentStepOneSubTitleHS,
            customerDetails["planName"],
            maxQtyAllowed,
            customerDetails["mobileNo"],
            customerDetails["emailId"]
          );
          if (initialState.isTaskInspection) {
            initialState.screenData.btnLabel = paymentFlowStrings.paymentActionLabelHS;
          } else {
            initialState.screenData.btnLabel = paymentFlowStrings.paymentFlowActionButtonTitle.viewMembershipButton;
          }
          initialState.screenData.stepTwoSubTitle = Formatter.parameterizedString(paymentFlowStrings.paymentStepTwoSubTitleHS,
            Formatter.formatCoverAmount(customerDetails["coverAmount"]));
        }
      }
      initialState.type = action.type;
      return {
        ...state,
        isLoading: false,
        productCode: initialState.productCode,
        isTaskInspection: initialState.isTaskInspection,
        planCategory: initialState.planCategory,
        screenData: initialState.screenData,
        isPlanServiceEW: initialState.isPlanServiceEW,
        type: initialState.type,
        actCode: action.activationCode,
        tempCustInfoData: deepCopy(action.data)
      };
    case fetchUserInfoFail:
      return {
        ...state,
        isLoading: false,
        type: fetchUserInfoFail,
      };
    default:
      return { ...state };
  }
};

export default paymentSuccessStepOneReducer;
