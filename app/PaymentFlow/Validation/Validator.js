isNumbersOnly = (e) => {
  return /^\d+$/.test(e.toString());
};
isAlphaNumeric = (e) => {
  return /^[a-z0-9]+$/i.test(e.toString());
};
isTextAndSpaceOnly = (e) => {
  return /^[a-zA-Z\s]+$/.test(e.toString());
};
isAlphaNumericAndSpaceOnly = (e) => {
  return /^[a-zA-Z0-9 ]*$/.test(e.toString());
};
isEmailAddressValid = (e) => {
  return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(e.toString());
};
isValidMobile = (e) => {
  return /^[0][1-9]\d{9}$|^[1-9]\d{9}$/.test(e.toString());
}
isValidPincode = (e) => {
  return /^\d{6}$/.test(e.toString());
}
isValidString = (data) => {
  if (typeof data == 'number') {
    data = data.toString()
  }
  return (typeof (data) === 'string' && data !== null && data !== undefined && data.trim().length !== 0);
}
formatCoverAmount = (coverAmount) => {
  let numInlac = Math.round(coverAmount / 10000.0) / 10;
  return numInlac + (numInlac > 1 ? " lacs" : " lac");
}
validateFormFields1 = (displayedComponentList) => {
  // let userDetailsString = {};
  let validationResult = {
    isValid: false,
    validatedComponents: [],
    userDetailsString: {}
  };

  let isValid = true;
  let formFieldsTemplate = displayedComponentList.map((item) => {
    if (item.inputValue != '') {
      switch (item.paramName) {
        case 'mobileNumber':
          item.isValid = isValidMobile(item.inputValue);
          break;
        case 'emailId':
          item.isValid = isEmailAddressValid(item.inputValue);
          break;
        case 'firstName':
          item.isValid = isTextAndSpaceOnly(item.inputValue);
          break;
        case 'pincode':
          item.isValid = isValidPincode(item.inputValue);
          break;
        default:
          item.isValid = true;
          break;
      }
      if (item.isValid) {
        validationResult.userDetailsString[item.paramName] = item.inputValue;
      } else {
        item.errorMessage = 'Invalid ' + item.title;
      }
    } else {
      item.errorMessage = 'Please enter your ' + item.title;
      item.isValid = false;
    }
    isValid = isValid && item.isValid;
    return item;
  });
  validationResult.validatedComponents = formFieldsTemplate
  validationResult.isValid = isValid
  return validationResult
}
