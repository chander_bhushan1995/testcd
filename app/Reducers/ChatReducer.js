import * as Actions from "../Constants/ActionTypes";
import { BottomViewType } from "../Constants/BottomViewType";
import { SendMessageStatus } from "../Constants/SendMessageStatus";
import { SUGGESTION_VIEW_MARGIN_FROM_BOTTOM } from "../Buyback/buybackActions/Constants";
import ChatbotText from "../ChatbotComponent/ChatbotText";
import { chatManager } from "../Navigation/index";
import { formattedMessageDate } from "../Chat/ChatUtils";

const defaultBottomViewType = () => chatManager.isInputEnabled() ? BottomViewType.Input : BottomViewType.None;

const initialState = {
    animatedIndex: -1,
    dataItem: [],
    bottomViewType: BottomViewType.None,
    isLoading: true,
    isInternetConnected: true,
    showInternetBar: false,
    errorMesage: "",
    BottomSheetCloseChatData: [],
    bottomViewData: null,
    autoSuggestionData: [],
    inputTextForAutoSuggestions: "",
    autoSuggestionVisible: false,
    autoSuggestionEnabled: false,
    selectedSuggestionData: null,
    suggestionViewMarginFromBottom: SUGGESTION_VIEW_MARGIN_FROM_BOTTOM,
    shouldClearInput: false,
    isDateTimePickerVisible: false,
    dateTimePickerType: "datetime",
    selectedTagData: {},
    otherTagData: {},
    translatedData: [],
};

const ChatReducer = (state = initialState, action) => {
    try {
        var dataItemArray = state.dataItem;
        if (action.type !== Actions.INPUT_ENABLED && action.type !== Actions.START_CHAT && action.type !== Actions.ANIMATION_DONE) {
            dataItemArray.map((item, index) => {
                if (item.type === Actions.TYPING) {
                    state.dataItem.splice(index, 1);
                }
            });
        }
        var messageTime = formattedMessageDate(new Date())
        // It is create the data for component to render based on item type
        // Here we can access the data which pass from the action along with the data of current state
        // here we are appending the data from action in current state data of component and update the state of compoent
        switch (action.type) {
            case Actions.ANIMATION_DONE:
                if (action.index < state.dataItem.length) {
                    state.dataItem[action.index].animationCompleted = true;
                }
                return Object.assign({}, state, {
                    dataItem: [...state.dataItem],
                    animatedIndex: Math.max(state.animatedIndex, action.index),
                    type: Actions.ANIMATION_DONE,
                });
            case Actions.RESET:
                return Object.assign({}, state, {
                    dataItem: [],
                    type: Actions.RESET,
                    animatedIndex: -1,
                    bottomViewType: action.isLoading ? BottomViewType.None : defaultBottomViewType(),
                    isLoading: action.isLoading
                });
            case Actions.SHOW_START_CHAT:
                return Object.assign({}, state, {
                    type: Actions.SHOW_START_CHAT,
                    bottomViewType: BottomViewType.StartChat,
                });
            case Actions.REMOVE_VIEW_FROM_LIST:
                state.dataItem.splice(action.index, 1);
                return Object.assign({}, state, {
                    dataItem: [...state.dataItem],
                    type: Actions.REMOVE_VIEW_FROM_LIST,
                });
            case Actions.REMOVE_CHAT_BOT_VIEW_FROM_LIST:
                let filterData = state.dataItem[action.index].value.filter(data => (data.actionType === "text" || data.actionType === "card"));
                if (filterData.length === 0) {
                    const displayObj = {
                        ComponantName: ChatbotText,
                        componentData: { text: action.displayMessage },
                        actionType: "text"
                    };
                    filterData.push(displayObj);
                }
                state.dataItem[action.index].value = filterData;
                return Object.assign({}, state, {
                    dataItem: [...state.dataItem],
                    type: Actions.BOT,
                    bottomViewType: defaultBottomViewType()
                });
            case Actions.TYPING_PAUSED:
                return Object.assign({}, state, {
                    dataItem: [...state.dataItem],
                    type: Actions.TYPING_PAUSED,
                    isLoading: false
                    // bottomViewType: defaultBottomViewType()
                });
            case Actions.TECHNICAL_ERROR:
                return Object.assign({}, state, {
                    dataItem: [...state.dataItem],
                    isLoading: action.isLoading,
                    errorMesage: action.message
                });
            case Actions.SEND_MESSAGE:
                let index1 = state.dataItem.length - 1;
                if (state.dataItem[index1].type === Actions.BOT) {
                    let filterData = state.dataItem[index1].value.filter(data => (data.actionType === "text" || data.actionType === "card"));
                    state.dataItem[index1].value = filterData;
                }
                return Object.assign({}, state, {
                    dataItem: [
                        ...state.dataItem,
                        {
                            value: action.displayData,
                            data: action.sendData,
                            status: SendMessageStatus.Pending,
                            type: action.componentType,
                            timeStamp: messageTime,
                            retryCount: 0
                        }
                    ],
                    type: Actions.SEND_MESSAGE,
                    selectedSuggestionData: null,
                    autoSuggestionData: [],
                    autoSuggestionVisible: false,
                });
            case Actions.SEND_MESSAGE_STATUS:
                state.dataItem[action.index].status = action.status;
                state.dataItem[action.index].timeStamp = messageTime;
                if (action.status === SendMessageStatus.Sending) {
                    let retryCount = state.dataItem[action.index].retryCount;
                    if (retryCount === null || retryCount === undefined) {
                        state.dataItem[action.index].retryCount = 0;
                    } else {
                        state.dataItem[action.index].retryCount = retryCount + 1;
                    }
                }
                return Object.assign({}, state, {
                    dataItem: [...state.dataItem],
                    bottomViewType: defaultBottomViewType(),
                    type: Actions.SEND_MESSAGE_STATUS,
                    selectedTagData: { selectedTags: [] },
                    otherTagData: { isOthersOptionSelected: false }
                });
            case Actions.UPDATE_BOTTOM_VIEW:
                return Object.assign({}, state, {
                    dataItem: [...state.dataItem],
                    type: Actions.UPDATE_BOTTOM_VIEW,
                    bottomViewType: null,
                });
            case Actions.HISTORY:
                var historyData = action.message;
                var list = historyData.list;
                var finalList = list.concat(...state.dataItem)
                return Object.assign({}, state, {
                    dataItem: finalList,
                    animatedIndex: finalList.length,
                    bottomViewType: historyData.bottomViewType,
                    bottomViewData: historyData.bottomViewData,
                    isLoading: false,
                    type: Actions.HISTORY,
                    autoSuggestionEnabled: historyData.autoSuggestionEnabled,
                });
            case Actions.RECEIVE_MESSAGE:
                return Object.assign({}, state, {
                    dataItem: [
                        ...state.dataItem,
                        {
                            value: action.message,
                            type: Actions.RECEIVE_MESSAGE,
                            timeStamp: messageTime,
                            animationCompleted: false
                        }
                    ],
                    type: Actions.RECEIVE_MESSAGE,
                    bottomViewType: defaultBottomViewType(),
                    isLoading: false
                });
            case Actions.RATING:
                return Object.assign({}, state, {
                    dataItem: [...state.dataItem, { type: Actions.RATING, value: action.value, animationCompleted: false }],
                    type: Actions.RATING,
                    bottomViewType: BottomViewType.None
                });
            case Actions.REVIEW:
                return Object.assign({}, state, {
                    dataItem: [...state.dataItem, { type: Actions.REVIEW, value: action.value, animationCompleted: false }],
                    type: Actions.REVIEW,
                    //bottomViewType: BottomViewType.None
                });
            case Actions.TYPING:
                return Object.assign({}, state, {
                    dataItem: [...state.dataItem, { type: Actions.TYPING, animationCompleted: false }],
                    type: Actions.TYPING,
                    isLoading: false
                    // bottomViewType: defaultBottomViewType(),
                });
            case Actions.START_CHAT:
                return Object.assign({}, state, {
                    dataItem: [...state.dataItem],
                    type: Actions.START_CHAT,
                    bottomViewType: defaultBottomViewType(),
                    autoSuggestionEnabled: action.autoSuggestionEnabled,
                    isLoading: action.isLoading,
                });
            case Actions.GET_DATA_FOR_CLOSE_CHAT:
                return Object.assign({}, state, {
                    dataItem: [...state.dataItem],
                    type: Actions.GET_DATA_FOR_CLOSE_CHAT,
                    BottomSheetCloseChatData: [{
                        name: "Close Chat",
                        isDisabled: chatManager._depId === null
                    }],
                });
            case Actions.ADD_SEND_RECEIVE_MESSAGE:
                return Object.assign({}, state, {
                    animatedIndex: state.animatedIndex + (action.animationCompleted ? 1 : 0),
                    dataItem: [...state.dataItem, {
                        value: action.message,
                        type: action.isSent ? Actions.SEND_MESSAGE : Actions.RECEIVE_MESSAGE,
                        timeStamp: messageTime,
                        animationCompleted: action.animationCompleted
                    }],
                    type: Actions.ADD_SEND_RECEIVE_MESSAGE
                });
            case Actions.DOCUMENT:
                return Object.assign({}, state, {
                    dataItem: [
                        ...state.dataItem,
                        {
                            type: Actions.DOCUMENT,
                            docUrl: action.data.docUrl,
                            docType: action.data.type,
                            fileType: action.data.fileType,
                            fileName: action.data.fileName,
                            fileSize: action.data.fileSize,
                            pickedFrom: action.data.pickedFrom,
                            status: SendMessageStatus.Pending,
                            timeStamp: messageTime,
                            retryCount: 0
                        }
                    ],
                    bottomViewType: defaultBottomViewType(),
                    type: Actions.DOCUMENT
                });
            case Actions.AGENT_SIDE_DOCUMENT:
                return Object.assign({}, state, {
                    dataItem: [
                        ...state.dataItem,
                        {
                            type: Actions.AGENT_SIDE_DOCUMENT,
                            docUrl: action.docUrl,
                            docType: action.docType,
                            fileType: action.fileType,
                            fileName: action.fileName,
                            fileSize: action.fileSize,
                            timeStamp: messageTime,
                            animationCompleted: false
                        }
                    ],
                    bottomViewType: defaultBottomViewType(),
                    type: Actions.AGENT_SIDE_DOCUMENT,
                    isLoading: false
                });
            case Actions.NEAGATIVE_RATING_QUESTIONS:
                return Object.assign({}, state, {
                    ratingData: action.response,
                    bottomViewType: BottomViewType.StartChat,
                    type: Actions.NEAGATIVE_RATING_QUESTIONS
                });
            case Actions.BOT:
                //Message id needs to replace with id
                let bottomViewData = null;
                let bottomViewType = BottomViewType.None;
                if (action.data.bottomView != null) {
                    let actionType = action.data.bottomView.actionType;
                    bottomViewData = action.data.bottomView.componentData;
                    if (actionType === "input" || actionType === "dateTimeInput") {
                        bottomViewType = BottomViewType.ChatbotInput
                    } else if (actionType === "formData") {
                        bottomViewType = BottomViewType.ChatbotForm
                    } else if (actionType === "uploadDoc") {
                        bottomViewType = BottomViewType.ChatbotUploadForm
                    } else if (actionType === "tagInput") {
                        bottomViewType = BottomViewType.ChatbotTagInput
                    }
                }
                let data = action.data.list.length !== 0 ? [...state.dataItem, {
                    value: action.data.list,
                    type: Actions.BOT,
                    timeStamp: messageTime,
                    animationCompleted: false
                }] : [...state.dataItem];
                return Object.assign({}, state, {
                    dataItem: data,
                    type: Actions.SEND_MESSAGE,
                    bottomViewType: bottomViewType,
                    bottomViewData: bottomViewData,
                    isLoading: false
                });
            case Actions.TAGS_INPUT:
                return {
                    ...state,
                    selectedTagData: action.selectedTagData,
                    bottomViewType: BottomViewType.ChatbotTagInput
                };
            case Actions.TAGS_INPUT_OTHERS:
                return { ...state, otherTagData: action.otherTagData, bottomViewType: BottomViewType.ChatbotTagInput };
            case Actions.RATING_SUBMIT_SUCCESS:
                chatManager.resetChatId();
                return Object.assign({}, state, {
                    dataItem: [...state.dataItem],
                    bottomViewType: BottomViewType.None,
                    isLoading: false,
                    type: Actions.RATING_SUBMIT_SUCCESS
                });
            case Actions.INPUT_ENABLED:
                if (state.bottomViewType == BottomViewType.None) {
                    return Object.assign({}, state, {
                        bottomViewType: defaultBottomViewType()
                    });
                } else {
                    return Object.assign({}, state, {});
                }
            case Actions.UPDATE_CONNECTION_STATUS:
                // TODO: if internet is connected again then retry all the pending msgs.
                return Object.assign({}, state, {
                    type: Actions.UPDATE_CONNECTION_STATUS,
                    isInternetConnected: action.isConnected,
                    showInternetBar: action.showInternetBar,
                });
            case Actions.AUTO_SUGGESTIONS_API_SUCCESS:
                return Object.assign({}, state, {
                    dataItem: [...state.dataItem],
                    type: Actions.AUTO_SUGGESTIONS_API_SUCCESS,
                    autoSuggestionData: action.autoSuggestionData,
                    inputTextForAutoSuggestions: action.inputText,
                });
            case Actions.AUTO_SUGGESTIONS_CLOSE:
                return Object.assign({}, state, {
                    dataItem: [...state.dataItem],
                    type: Actions.AUTO_SUGGESTIONS_CLOSE,
                    autoSuggestionData: [],
                    autoSuggestionVisible: false,
                    shouldClearInput: false
                });
            case Actions.SUGGESTIONS_SELECTION:
                return Object.assign({}, state, {
                    dataItem: [...state.dataItem],
                    type: Actions.AUTO_SUGGESTIONS_CLOSE,
                    selectedSuggestionData: action.selectedSuggestionObj,
                    autoSuggestionData: [],
                    autoSuggestionVisible: false,
                    shouldClearInput: true
                });
            case Actions.SUGGESTIONS_SELECTION_RESET:
                return Object.assign({}, state, {
                    dataItem: [...state.dataItem],
                    type: Actions.SUGGESTIONS_SELECTION_RESET,
                    selectedSuggestionData: null,
                    shouldClearInput: false,
                    autoSuggestionVisible: false,
                });
            case Actions.AUTO_SUGGESTION_VISIBLE:
                return Object.assign({}, state, {
                    dataItem: [...state.dataItem],
                    type: Actions.AUTO_SUGGESTION_VISIBLE,
                    autoSuggestionVisible: action.visible,
                });
            case Actions.SUGGESTIONS_VIEW_MARGIN_BOTTOM_UPDATE:
                return Object.assign({}, state, {
                    dataItem: [
                        ...state.dataItem,
                    ],
                    type: Actions.SUGGESTIONS_VIEW_MARGIN_BOTTOM_UPDATE,
                    suggestionViewMarginFromBottom: action.newMargin,
                });
            // case Actions.AUTO_SUGGESTIONS_API_FAILURE:
            //     return Object.assign({}, state, {
            //         dataItem: [
            //             ...state.dataItem,
            //             { value: action.message, type: Actions.RECEIVE_MESSAGE, timeStamp: messageTime, animationCompleted: false }
            //         ],
            //         type: Actions.RECEIVE_MESSAGE,
            //         bottomViewType: defaultBottomViewType()
            //     });
            case Actions.SHOW_DATETIME_PICKER:
                return Object.assign({}, state, {
                    dataItem: [...state.dataItem],
                    type: Actions.SHOW_DATETIME_PICKER,
                    dateTimePickerType: action.dateTimePickerType ?? "datetime",
                    isDateTimePickerVisible: true,
                });
            case Actions.HIDE_DATETIME_PICKER:
                return Object.assign({}, state, {
                    dataItem: [...state.dataItem],
                    type: Actions.HIDE_DATETIME_PICKER,
                    dateTimePickerType: "datetime",
                    isDateTimePickerVisible: false,
                });
            case Actions.UPDATE_AUTO_SUGGESTION_ENABLED:
                return {
                    ...state,
                    type: Actions.UPDATE_AUTO_SUGGESTION_ENABLED,
                    autoSuggestionEnabled: action.isAutoSuggestionEnabled,
                };
            case Actions.GET_TRANSLATIONS:
                return {
                    ...state,
                    translatedData: action.translatedData,
                };

            case Actions.START_CHAT_BUTTON:
                return {
                    ...state,
                    bottomViewType: BottomViewType.StartChat,
                };
                case Actions.RESET_BOTTOM_VIEW:
                return {
                    ...state,
                    bottomViewType: BottomViewType.None,
                };
            default:
                return Object.assign({}, state, { type: null });
        }
    } catch (e) {
        return Object.assign({}, state, {});
    }
};

export default ChatReducer;
