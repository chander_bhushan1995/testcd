import {combineReducers, createStore} from 'redux';

import ChatReducer from './ChatReducer'
import NavigationReducer from "./navigationReducer";

const AppReducers = combineReducers({
    ChatReducer,
    NavigationReducer
});

const rootReducer = (state, action) => {
    return AppReducers(state, action);
};

let store = createStore(rootReducer);

export default store;
