import {
    Text,
    View,
    StyleSheet,
    Dimensions,
    Image,
    TouchableOpacity,
    NativeEventEmitter,
    NativeModules, Platform
} from "react-native";
import React, { useEffect, useState } from "react";
import spacing from "../../Constants/Spacing";
import colors from "../../Constants/colors";
import images from "../../images/index.image";
import { TextStyle } from "../../Constants/CommonStyle";
import ProgressBar from "../../CommonComponents/ProgressBar";
import {
    ACTIVATION_NOT_SETUP,
    ACTIVATION_SETUP_COMP, PLATFORM_OS,
    START_ACTIVATION,
    VIEW,
    VIEW_DETAIL,
} from "../../Constants/AppConstants";
import { logWebEnageEvent } from "../../commonUtil/WebengageTrackingUtils";
import { ACTIVATION_STRIP } from "../../Constants/WebengageEvents";
import DeviceInfo from "react-native-device-info";

const EVENT_ON_REMAINING_TIME_CHANGE = "OnDownloadRemainingTimeChange";
const EVENT_ON_ERROR = "OnDownloadError";

const bridge = NativeModules.ChatBridge;
const eventEmitter = new NativeEventEmitter(bridge);

const DownloadingProgressView = props => {

    const [currentState, setCurrentState] = useState({
        progress: 0,
        error: null,
        remainingTime: null,
        isCompleted: false,
    });

    useEffect(() => {
        const onTimeChangeSubscription = eventEmitter.addListener(EVENT_ON_REMAINING_TIME_CHANGE, handleTimeChange);
        const onErrorSubscription = eventEmitter.addListener(EVENT_ON_ERROR, handleOnError);

        return () => {
            onTimeChangeSubscription.remove();
            onErrorSubscription.remove();
        };
    }, []);

    const handleTimeChange = (params) => {
        setCurrentState({ ...currentState, progress: params.progress ?? 0, remainingTime: params.remainingTime, isCompleted: params.isCompleted});
    };

    const handleOnError = (params) => {
        console.log(params)
        setCurrentState({ ...currentState, error: params.error });
    };

    const handleButtonAction = () => {
        let eventData = {"Mode": props.testType};
        if (!currentState.isCompleted && currentState.error) { // show error screen
            eventData["Action"]  = "View Error"
            logWebEnageEvent(ACTIVATION_STRIP, eventData);
            bridge.showDownloadingErrorScreen(currentState.error)
        } else if (currentState.isCompleted && !currentState.error) { // start activation process
            eventData["Action"]  = "Start"
            logWebEnageEvent(ACTIVATION_STRIP, eventData);
            bridge.performPostDownload(props.activationCode)
            if (props.setDownloadProgressVisible) {
                props.setDownloadProgressVisible({ visible: false, activationCode: null });
            }
        } else { // show downloading progress screen
            if (props.setDownloadProgressVisible) {
                props.setDownloadProgressVisible({ visible: false, activationCode: null });
            }
            eventData["Action"] = "View Details"
            logWebEnageEvent(ACTIVATION_STRIP, eventData);
            bridge.showDownloadingProgressScreen()
        }
    };

    const getDescriptionWithBtnText = () => {
        let description = "";
        let buttonText = "";
        if (!currentState.isCompleted && currentState.error) {
            description = ACTIVATION_NOT_SETUP;
            buttonText = VIEW_DETAIL;
        } else if (currentState.isCompleted && !currentState.error) {
            description = ACTIVATION_SETUP_COMP;
            buttonText = START_ACTIVATION;
        } else {
            description = "Activation Setup: " + (currentState.remainingTime ? currentState.remainingTime : '');
            buttonText = VIEW;
        }
        return { description: description, buttonText: buttonText };
    };
    return (
        <View style={styles.container}>

            {(!currentState.error && !currentState.isCompleted)
                ? <ProgressBar data={{ completed: currentState.progress * 100 }}
                               style={{
                                   containerStyle: styles.progressContainerStyle,
                                   progressBarStyle: { backgroundColor: colors.blue028 },
                               }} />
                : null}

            <View style={{ flexDirection: "row", alignItems: "center" }}>

                {(currentState.error || currentState.isCompleted)
                    ?
                    <Image source={currentState.error ? images.yellowInfo : images.green_right} style={styles.image} />
                    : null}

                <Text style={styles.description}>{getDescriptionWithBtnText()?.description}</Text>
                <TouchableOpacity style={styles.buttonContainer} onPress={handleButtonAction}>
                    <Text style={styles.buttonText}>{getDescriptionWithBtnText()?.buttonText}</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        position: "absolute",
        backgroundColor: colors.white,
        width: Dimensions.get("screen").width - 20,
        bottom:56 + (Platform.OS === PLATFORM_OS.android || !DeviceInfo.hasNotch() ? spacing.spacing8 : spacing.spacing28) + 8, // tabbar height + tabbar margin bottom + current view container bottom
        marginLeft: spacing.spacing10,
        borderWidth: 1,
        borderColor: colors.color_97CFFF,
        borderRadius: 4,
        padding: 10,
        flex: 1,
        shadowColor: colors.black,
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowRadius: 4,
        shadowOpacity: 0.13,
        elevation: 4,
    },
    image: {
        height: 20,
        width: 20,
        resizeMode: "contain",
        marginRight: spacing.spacing10,
    },
    description: {
        ...TextStyle.text_12_regular,
        flex: 1,
        marginRight: spacing.spacing10,
    },
    buttonContainer: {},
    buttonText: {
        ...TextStyle.text_12_bold,
        color: colors.blue028,
        padding: 4
    },
    progressContainerStyle: {
        backgroundColor: "rgba(2,130,240,0.07)",
        marginBottom: spacing.spacing10,
        borderRadius: 2,
        overflow: "hidden",
    },
});
export default DownloadingProgressView;
