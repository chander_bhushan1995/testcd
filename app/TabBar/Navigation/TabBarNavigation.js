import {createBottomTabNavigator, createStackNavigator} from 'react-navigation';
import HomeScreen from '../../../app/AppForAll/HomeScreen/Screens/HomeScreen';
import BuyPlanComponent from "../../BuyPlan/Screens/BuyPlanComponent";
import MembershipTabScreen from "../../MembershipTab/Screens/MembershipTabScreen";
import MyAccountScreen from "../../AppForAll/MyAccount/Screens/MyAccountScreen";
import React from 'react';
import {Image, Platform, View, TouchableOpacity, Text, NativeModules} from 'react-native';
import images from '../../images/index.image';
import colors from '../../Constants/colors';
import spacing from '../../Constants/Spacing';
import {PLATFORM_OS} from '../../Constants/AppConstants';
import {localEventEmitter} from '../index.tabbar';
import DeviceInfo from 'react-native-device-info';
import {TextStyle} from '../../Constants/CommonStyle';
import dimens from '../../Constants/Dimens';
import * as Store from '../../commonUtil/Store'
import {tabbarManagerObj} from '../index.tabbar';
import {getTabNameFromIndex, getIndexFromTabName} from '../../commonUtil/AppUtils';
import { AppForAppFlowStrings } from '../../AppForAll/Constant/AppForAllConstants';
import {
    CHAT,
    CHAT_ON_HOME_TAPPED,
} from '../../Constants/WebengageEvents';
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';
import { APIData } from '../../..';

const bridgeRef = NativeModules.ChatBridge;
const HeaderRight = (props) => {
    const count = tabbarManagerObj.getNotificationCount() > 9 ? '9+': tabbarManagerObj.getNotificationCount();

    const openChat = () => {
        logWebEnageEvent(CHAT, { location: AppForAppFlowStrings.homeScreen });
        logWebEnageEvent(CHAT_ON_HOME_TAPPED, new Object());
        const isLoggedIn = Number(APIData?.customer_id);
        if (isLoggedIn) {
            bridgeRef.openChat()
        } else {
            bridgeRef.showPopupForVerifyNumber(AppForAppFlowStrings.chatLoginAlert.title, "", AppForAppFlowStrings.chat, status => {
                bridgeRef.openChat()
            })
        }
    }

    return (<View style={{flexDirection: 'row', alignItems: 'center'}}>
        <TouchableOpacity onPress={() => bridgeRef.goToInboxScreen()}
                          style={{flexDirection: 'row'}}>
            <Image source={images.ic_notification}
                   style={{width: dimens.dimen16, height: dimens.dimen16}}/>
            <View style={{marginTop: spacing.spacing_neg_8}}>
                {count ? <View style={{
                    backgroundColor: colors.saffronYellow,marginLeft: spacing.spacing_neg_8,
                    justifyContent: 'flex-end', paddingHorizontal: spacing.spacing4, borderRadius: spacing.spacing5,
                }}>
                    <Text style={[TextStyle.text_10_bold, {color: colors.white}]}>{count}</Text>
                </View> : null}
            </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={openChat}>
            <Image source={images.chatSmall}
                   style={{width: dimens.dimen16, height: dimens.dimen16, marginRight: spacing.spacing24, marginLeft: spacing.spacing20}}/>
        </TouchableOpacity>
    </View>);
};

let TabNavigation = null;

export const switchTab = (tabIndex, tabParams) => {
    let tabName = getTabNameFromIndex(tabIndex);
    if (TabNavigation) {
        TabNavigation.navigate(tabName, {params: tabParams});
    }
};

export const updateTabParams = (params) => {
    if (TabNavigation) {
        TabNavigation.setParams(params)
    }
};

const HomeTabStack = createStackNavigator({
    Home: {
        screen: props => <HomeScreen {...props} />,
        navigationOptions: ({navigation}) => {
            const {text = null, showShadow = true} = navigation.state?.params ?? {};
            return {
                headerStyle: [{borderBottomWidth: 0, elevation: 0, backgroundColor: colors.white}, showShadow && {shadowOpacity: 1, marginBottom: spacing.spacing1}],
                headerTitle: <View style={{alignItems: 'flex-start', marginLeft: spacing.spacing16, marginRight: spacing.spacing20}}>
                    <Text style={TextStyle.text_16_bold} numberOfLines={1}>{text ?? 'Home'}</Text>
                </View>,
                headerLeft: null,
                headerRight: <HeaderRight/>,
                headerTitleStyle: {color: colors.black},
                headerTintColor: colors.black,

            };
        },
    },
});

const BuyTabStack = createStackNavigator({
    Buy: {
        screen: BuyPlanComponent,
        navigationOptions: ({navigation}) => {
            return {
                headerStyle: {borderBottomWidth: 0, elevation: 0, backgroundColor: colors.white},
                headerTitle: <View style={{alignItems: 'flex-start', marginLeft: spacing.spacing16}}>
                    <Text style={TextStyle.text_16_bold}>{'Buy'}</Text>
                </View>,
                headerLeft: null,
                headerRight: <HeaderRight/>,
                headerTitleStyle: {color: colors.black},
                headerTintColor: colors.black,
            };
        },
    },
});

const MembershipTabStack = createStackNavigator({
    Membership: {
        screen: MembershipTabScreen,
        navigationOptions: ({navigation}) => {
            return {
                headerStyle: {borderBottomWidth: 0, elevation: 0, backgroundColor: colors.white},
                headerTitle: <View style={{alignItems: 'flex-start', marginLeft: spacing.spacing16}}>
                    <Text style={TextStyle.text_16_bold}>{'Membership'}</Text>
                </View>,
                headerLeft: null,
                headerRight: <HeaderRight />,
                headerTitleStyle: {color: colors.black},
                headerTintColor: colors.black,
            };
        },
    },
});

const MyAccountTabStack = createStackNavigator({
    MyAccount: {
        screen: MyAccountScreen,
        navigationOptions: ({navigation}) => {
            return {
                headerStyle: {borderBottomWidth: 0, elevation: 1, backgroundColor: colors.white, shadowOpacity: 1},
                headerTitle: <View style={{alignItems: 'flex-start', marginLeft: spacing.spacing16}}>
                    <Text style={TextStyle.text_16_bold}>{'Account'}</Text>
                </View>,
                headerLeft: null,
                headerRight: <HeaderRight />,
                headerTitleStyle: {color: colors.black},
                headerTintColor: colors.black,
            };
        },
    },
});

export default createBottomTabNavigator(
    {
        Home: HomeTabStack,
        Buy: BuyTabStack,
        Membership: MembershipTabStack,
        Account: MyAccountTabStack,
    },
    {
        navigationOptions: ({navigation}) => {
            TabNavigation = navigation;
            const {showAccountRedDot = false} = navigation.state?.params ?? {};
            return({
                tabBarIcon: ({focused, tintColor}) => {
                    const {routeName} = navigation.state;
                    let iconName;
                    let showRedIcon = false;
                    if (routeName === 'Home') {
                        iconName = focused ? images.homeSmallBlue : images.homeSmall;
                    } else if (routeName === 'Buy') {
                        showRedIcon = tabbarManagerObj.getShowBuyTabRedDot();
                        iconName = focused ? images.buySmallBlue : images.buySmall;
                    } else if (routeName === 'Membership') {
                        iconName = focused ? images.memSmallBlue : images.memSmall;
                    } else if (routeName === 'Account') {
                        showRedIcon = tabbarManagerObj.getShowAccountTabRedDot() || showAccountRedDot;
                        iconName = focused ? images.accountSmallBlue : images.accountSmall;
                    }
                    const redIcon = showRedIcon ? <View style={{
                            width: dimens.dimen8,
                            height: dimens.dimen8,
                            borderRadius: dimens.dimen4,
                            backgroundColor: colors.redBD0, marginLeft: spacing.spacing_n_12
                        }} />
                        : null;
                    return <View style={{flexDirection:'row'}}>
                        <Image style={{width: 25, height: 25}} source={iconName}/>
                        {redIcon}
                    </View>;
                },
                tabBarOnPress: ({navigation, defaultHandler}) => {
                    const {routeName} = navigation.state;
                    bridgeRef.tabChanged(getIndexFromTabName(routeName));
                    if (routeName === 'Buy') {
                        Store.setBuyTabRedDot('true');
                        tabbarManagerObj.setShowBuyTabRedDot(false);
                    }
                    localEventEmitter.emit('TabChanged', routeName);
                    defaultHandler();
                },
            })},
        lazy: true,
        optimizationsEnabled: true,
        tabBarOptions: {
            activeTintColor: colors.color_008DF6,
            inactiveTintColor: colors.color_888F97,
            safeAreaInset: { top: 'never', bottom: 'never' },
            style: {
                height: spacing.spacing56,
                paddingVertical: spacing.spacing4,
                marginTop: spacing.spacing2,
                marginBottom: Platform.OS === PLATFORM_OS.android || !DeviceInfo.hasNotch() ? spacing.spacing8 : spacing.spacing28,
                marginHorizontal: spacing.spacing8,
                elevation: 1,
                shadowOpacity: 0.5,
                shadowOffset: {
                    width: 1,
                    height: 1,
                },
                shadowColor: colors.black,
                backgroundColor: colors.white,
                borderRadius: spacing.spacing8,
                borderColor: colors.grey,
                borderWidth: 0.2,
            },
            labelStyle: {
                fontSize: 12,
                fontFamily: 'Lato-Bold',
                lineHeight: 18,
            },
        },
    },
);
