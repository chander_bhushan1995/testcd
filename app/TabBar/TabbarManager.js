export default class TabbarManager {

    static myInstance = null;
    _showBuyTabRedDot = false;
    _showAccountTabRedDot = false;
    _notificationCount = 0;
    /**
     * @returns {TabbarManager}
     */
    static getInstance() {
        if (TabbarManager.myInstance === null) {
            TabbarManager.myInstance = new TabbarManager();
        }
        return this.myInstance;
    }

    getShowBuyTabRedDot() {
        return this._showBuyTabRedDot;
    }

    setShowBuyTabRedDot(showBuyTabRedDot) {
        this._showBuyTabRedDot = showBuyTabRedDot;
    }

    getShowAccountTabRedDot() {
        return this._showAccountTabRedDot;
    }

    setShowAccountTabRedDot(showAccountTabRedDot) {
        this._showAccountTabRedDot = showAccountTabRedDot;
    }

    getNotificationCount() {
        return this._notificationCount;
    }

    setNotificationCount(notificationCount) {
        this._notificationCount = notificationCount;
    }

}
