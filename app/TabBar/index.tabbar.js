
import React, { useEffect, useState, useRef } from "react";
import useLoader from "../CardManagement/CustomHooks/useLoader";

import { NativeModules, NativeEventEmitter,Platform } from "react-native";
import TabBarNavigation, {switchTab, updateTabParams} from './Navigation/TabBarNavigation';

import {APIData, setAPIData, updateAPIData} from "../../index";
import { getMembershipArrayForLoggedInUserWith } from "../network/APIHelper";
import Loader from "../Components/Loader";
import Toast, {DURATION} from "../CommonComponents/Toast";
import BlockingLoader from "../CommonComponents/BlockingLoader";
import DialogView from "../Components/DialogView";
import codePush from "react-native-code-push";
import colors from "../Constants/colors";
import { TextStyle } from "../Constants/CommonStyle";
import EventEmitter from 'events';
import TabbarManager from './TabbarManager';
import * as Store from '../commonUtil/Store';
import DownloadingProgressView from "./Components/DownloadingProgressView";

export const RootContext = React.createContext();

const bridge = NativeModules.ChatBridge;
const eventEmitter = new NativeEventEmitter(bridge);
export const localEventEmitter = new EventEmitter();
const EVENT_REFRESH_MEMBERSHIPS = "RefreshMemberships";
const EVENT_REFRESH_ACCOUNT_RED_DOT = "RefreshAccountRedDot";
const EVENT_CHANGE_TAB_SELECTED = "ChangeTabSelected";
const EVENT_CAPTURE_DEEPLINK = "DeeplinksData";
const UPDATE_NOTIFICATION_COUNT = "UpdateNotificationCount";
const CHANGE_DOWNLOADING_STRIP_VISIBILITY = "DownloadingStripVisibility"

export let tabbarManagerObj = TabbarManager.getInstance();

const Root: () => React$Node = props => {
    setAPIData(props)
    console.ignoredYellowBox = ['Warning:'];
    console.disableYellowBox = true;

    const [isLoading, setIsLoading] = useState(false);
    const [deeplinkPath, setDeeplinkPath] = useState(APIData?.deeplinkPath);
    const [memberships, setMemberships] = useState(null);
    const [isDownloadProgressVisible, setDownloadProgressVisible] = useState({ visible: false, activationCode: null, testType: null})
    const [isBlockingLoader, blockingLoaderMessage, startBlockingLoader, stopBlockingLoader] = useLoader();
    const toastRef = useRef(null);
    const alertRef = useRef(null);

    const [isMembershipLoading, setIsMembershipLoading] = useState(false);

    const setDeeplink = (deeplinkPath) => {
        setDeeplinkPath(deeplinkPath);
        updateAPIData({
            deeplinkPath: deeplinkPath
        });
    }

    useEffect(() => {
        updateAPIData(props)
    }, [props])

    useEffect(() => {
        setInitialValues()
        initializeViews()
        /* it will listen to refresh memberships tab from native code */
        eventEmitter.addListener(EVENT_REFRESH_MEMBERSHIPS, params => refreshMemberships());
        localEventEmitter.addListener(EVENT_REFRESH_ACCOUNT_RED_DOT, params => {
            tabbarManagerObj.setShowAccountTabRedDot(params)
            updateTabParams({ showAccountRedDot: params });
        });
        eventEmitter.addListener(EVENT_CHANGE_TAB_SELECTED, params => {
            switchTab(params?.tabIndex);
        });
        eventEmitter.addListener(EVENT_CAPTURE_DEEPLINK, params => {
            setDeeplink(params);
        });
        eventEmitter.addListener(UPDATE_NOTIFICATION_COUNT, params => {
            // TODO: KAG, check!! Will this work??
            tabbarManagerObj.setNotificationCount(params?.notiCount);
        });
        const downloadingStripVisibilitySubs = eventEmitter.addListener(CHANGE_DOWNLOADING_STRIP_VISIBILITY, params => {
            setDownloadProgressVisible({visible: params.isVisible, activationCode: params.activationCode, testType: params.testType})
        })
        return () => {
            eventEmitter.removeListener(EVENT_REFRESH_MEMBERSHIPS, () => { });
            localEventEmitter.removeListener(EVENT_REFRESH_ACCOUNT_RED_DOT, () => { });
            eventEmitter.removeListener(EVENT_CHANGE_TAB_SELECTED, () => { });
            eventEmitter.removeListener(EVENT_CAPTURE_DEEPLINK, () => { });
            eventEmitter.removeListener(UPDATE_NOTIFICATION_COUNT, () => { });
            downloadingStripVisibilitySubs.remove()
        };
    }, [])

    const initializeViews = () => {
        if (APIData.customer_id) {
            initializePlans()
        } else {
            initializeTabBar(null)
        }
    }

    const setInitialValues = () => {
        //set initial values in tabbar manager
        Store.getBuyTabRedDot((error, value) => {
            if (!value) {
                tabbarManagerObj.setShowBuyTabRedDot(true);
            }
        });
    }

    const initializePlans = () => {
        setIsLoading(true);
        getMembershipArrayForLoggedInUserWith((response, error) => {
            if (error) {
                showAlert("Error", "Unable to fetch Data", { text: "Retry", onClick: initializePlans }, null, null, false);
            } else {
                setIsLoading(false);
                receivedPlans(response)
            }
        })
    }

    const receivedPlans = (response) => {
        setMemberships(response)
        initializeTabBar(response)
    }

    const initializeTabBar = (reponse) => {
        // Below platform check duue to prod-3378. when we are sending the whole response int value auto convert to double in android side
        bridge.loadInitialTabBar(Platform.OS === 'ios'?reponse:JSON.stringify(reponse))
    }

    const refreshMemberships = () => {
        if (!APIData.customer_id && isMembershipLoading) { return }

        setIsMembershipLoading(true);
        getMembershipArrayForLoggedInUserWith((response, error) => {
            // Below platform check duue to prod-3378. when we are sending the whole response int value auto convert to double in android side
            bridge.updateMembershipDataOnNative(Platform.OS === 'ios'?response:JSON.stringify(response));
            setIsMembershipLoading(false);
        })
    }

    const showToast = (message, timeout = DURATION.LENGTH_SHORT) => toastRef.current.show(message, timeout, true);

    const showAlert = (title, alertMessage, primaryButton = {
        text: "Yes", onClick: () => {
        },
    }, secondaryButton, checkBox, isCrossEnabled = true) => {
        alertRef.current.showDailog({
            title: title,
            message: alertMessage,
            primaryButton: primaryButton,
            secondaryButton: secondaryButton,
            checkBox: checkBox,
            cancelable: true,
            isCrossEnabled: isCrossEnabled,
            onClose: () => {
            },
        });
    };

    const showAlertWithImage = (image, title, alertMessage, primaryButton = {
        text: "Ok", onClick: () => {
        },
    }, secondaryButton, checkBox) => {
        alertRef.current.showDailog({
            title: title,
            message: alertMessage,
            primaryButton: primaryButton,
            secondaryButton: secondaryButton,
            checkBox: checkBox,
            cancelable: false,
            imageUrl: image,
            isCrossEnabled: true,
            onClose: () => {
            },
        });
    };

    return (
        <RootContext.Provider value={{
            InitialProps: props,
            deeplinkPath: deeplinkPath,
            setDeeplink: setDeeplink,
            Toast: {showToast: showToast},
            Alert: {showAlert: showAlert, showAlertWithImage: showAlertWithImage, ref: alertRef},
            BlockingLoader: {startLoader: startBlockingLoader, stopLoader: stopBlockingLoader},
        }}>
            <BlockingLoader visible={isBlockingLoader} loadingMessage={blockingLoaderMessage}/>
            <Loader isLoading={isLoading} />
            <TabBarNavigation />
            <Toast ref={toastRef}
                style={{ backgroundColor: colors.color_404040 }}
                position="bottom"
                textStyle={{ ...TextStyle.text_14_bold, color: colors.white }}
            />
            <DialogView ref={alertRef} />

            {isDownloadProgressVisible?.visible
                ? <DownloadingProgressView setDownloadProgressVisible = {setDownloadProgressVisible}
                    activationCode={isDownloadProgressVisible.activationCode} testType={isDownloadProgressVisible.testType}
                />
                : null
            }
        </RootContext.Provider>
    );
};
export default codePush(Root);
