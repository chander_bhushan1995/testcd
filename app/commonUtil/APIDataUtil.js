import * as Store from "./Store";
import { compareDate, daysDifference, getCurretDateMilliseconds } from "./DateUtils";
import { parseJSON } from "./AppUtils";

let fileSystem = require("react-native-fs");
export const cacheData = (apiParams, callback) => {
    //check for cached data in files
    let path = fileSystem.DocumentDirectoryPath + `/${(apiParams?.url ?? "").replace(/\//g, "_")}`;
    let validity = apiParams?.cacheValidity ?? 10;
    //TODO:- VALIDITY OF CACHE DATA & FORCE UPDATE
    readCacheDataCallback(path, (data) => {
        if (data) { // if data found pass to screen and just write data when response provided by api
            apiParams?.callback(parseJSON(data), null);
            // callback((response, error) => { // no need to call api
            //     writeCacheDataCallback(path, response, error);
            // });
        } else { // if data not found then pass data to screen and write in file
            callback((response, error) => {
                apiParams?.callback(response, error);
                writeCacheDataCallback(path, validity, response, error);
            });
        }
    });
};

const readCacheDataCallback = (path, callback) => {
    let data = null;
    fileSystem.exists(path).then((exist) => {
        if (exist) {
            fileSystem.readFile(path, "utf8").then((stringifiedData) => {
                data = stringifiedData;
                callback(data);
                console.info(`Cache Data Found at ${path}`);
            }).catch((error) => {
                console.warn(` error while reading file at ${path} \n error: ${error.message}`);
                callback(data);
            });
        } else {
            console.info(` File doesn't exist at ${path}`);
            callback(data);
        }
    }).catch(error => {
        callback(data);
        console.warn(`Error while checking file existence of ${path}`, error);
    });
};

const writeCacheDataCallback = (path, validity, response, error) => {
    if (response) {
        let responseJSONString = JSON.stringify(response);
        saveAPICacheData(path, validity);
        fileSystem.writeFile(path, responseJSONString, "utf8")
            .then((success) => {
                console.info(` file written successfully ${path}`);
            })
            .catch((err) => {
                console.warn(` error while writing file at ${path}`);
            });
    }
};
// [
// {
// name: "",
// createdDate: ""
// }
//
// ]
const saveAPICacheData = (path, validity) => {
    Store.getCacheData((error, stringifiedData) => {
        let parsedData = stringifiedData ? parseJSON(stringifiedData) : [];
        if (!(parsedData.filter(obj => obj.name === path).first())) {
            parsedData.push({
                name: path,
                createdDate: getCurretDateMilliseconds(),
                validity: validity,
            });
            Store.setCacheData(JSON.stringify(parsedData));
        }
    });
};

export const removeCacheData = () => {
    Store.getCacheData((error, stringifiedData) => {
        if (stringifiedData) {
            let parsedData = parseJSON(stringifiedData) ?? [];
            let removableAPIs = [];
            let nonRemovableAPIs = [];

            parsedData.forEach((obj, index, array) => {
                let difference = daysDifference(obj.createdDate, getCurretDateMilliseconds());
                if (difference >= obj.validity) {
                    removableAPIs.push(obj);
                } else {
                    nonRemovableAPIs.push(obj);
                }
            }) ?? [];

            //update nonRemovable api data
            Store.setCacheData(JSON.stringify(nonRemovableAPIs))

            removableAPIs.map((obj) => {
                fileSystem.readDir(fileSystem.DocumentDirectoryPath).then((directoryItems) => {
                    directoryItems.map(item => {
                        if (item?.isFile && obj?.name == item?.path) {
                            fileSystem.unlink(obj?.name).then(() => {
                                console.warn(`Cache File Deleted At: ${obj?.name}`);
                            }).catch((err) => {
                                console.warn(`Unable to delete cache file At: ${obj?.name} `, err);
                            });
                        }
                    });
                }).catch((err) => {
                    console.warn(`Unable to read directory error: ${err}`);
                });
            });
        }
    });
};


