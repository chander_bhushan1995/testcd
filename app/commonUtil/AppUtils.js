
import { AsyncStorage, Linking, NativeModules, Platform } from "react-native";
import React from 'react';
import {WALK_THROUGH_ACTION} from '../Constants/AsyncStorageConstant';
import {FIREBASE_KEYS, PLATFORM_OS} from '../Constants/AppConstants';
import * as Store from './Store';

/**
 *  Local method for parsing params from web url.
 * @param {*} query  : url
 */

const chatBrigeRef = NativeModules.ChatBridge;

const parseJSON = (json) => {
    try {
        return JSON.parse(json);
    } catch (error) {
        return null;
    }
}

const deepCopy = (data) => {
    return parseJSON(JSON.stringify(data))
}

const parseQueryParamsFromUrl = (url) => {
    var query_string = {};

    var parts1 = url.split('?')[1];
    if (parts1 !== null && parts1 !== undefined) {
        var vars = parts1.split('&');

        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            var key = decodeURIComponent(pair[0]);
            var value = decodeURIComponent(pair[1]);
            if (typeof query_string[key] === 'undefined') {
                query_string[key] = decodeURIComponent(value);
            } else if (typeof query_string[key] === 'string') {
                var arr = [query_string[key], decodeURIComponent(value)];
                query_string[key] = arr;
            } else {
                query_string[key].push(decodeURIComponent(value));
            }
        }
    }
    return query_string;
};

/**
 *  Get Reward on-off status .
 */
function getrewardOnStatus(callback) {
    chatBrigeRef.getRewardFeatureOnStatus(info => {
        let infoObj = parseJSON(info);
        let rewardOn = infoObj.reward_feature_on
        callback(Boolean(rewardOn) == true)
    });
}

/**
 *  Get SOD Appliances Data Mapping
 */

function getSODAppliancesData(callback){
    chatBrigeRef.sodAppliancesMapping(data =>{
        let dataObj = parseJSON(data)
        let sodApplianceMapping = parseJSON(dataObj.sodAppliancesData);
        callback(sodApplianceMapping)
    })
}

function getCDNDataVersioning(callback) {
    chatBrigeRef.getCDNDataVersioning(data => {
        let jsonData = parseJSON(data);
        callback(jsonData);
    });
}

function getFirebaseData(key, callback) {
    chatBrigeRef.getDataFromFirebase(key, data => {
        if (!data) {
            callback(null)
            return
        }
        switch (key) {
            case FIREBASE_KEYS.QR_CampaignId:
            case FIREBASE_KEYS.HA_CampaignId:
                callback(data);
                break;
            default:
                callback(JSON.parse(data));
        }
    });
}

function getPlansCategoryData(callback){
    chatBrigeRef.getPlansCategoryData(data =>{
        let dataObj = parseJSON(data)
        let plansCategoryData = parseJSON(dataObj.plansCategoryData);
        callback(plansCategoryData)
    })
}

/**
 *  Get Customer detail .
 */
function getCustomerDetail(chatBrigeRef, callback) {
    chatBrigeRef.getUserInfo(userInfo => {
        let userInfoObj = parseJSON(userInfo);
        const customerId = userInfoObj.customerId
        if (customerId != null && customerId != undefined && customerId != "0") { //user has verified his mobile number
            callback.onSuccess(userInfoObj)
        } else {
            callback.onFailure();

        }
    });
}

/**
 *  Get Customer press Skip(number not verified) status .
 */
function isWalkThroughShow(callback) {
    const chatBridgeRef = NativeModules.ChatBridge;
    if (Platform.OS === PLATFORM_OS.android) {
        chatBridgeRef.isWalkThroughVisited((status) => {
            if (Boolean(status) == true) {
                callback.onTrue();
            } else {
                callback.onFalse();
            }
        });
    } else {
        Store.getWalkthroughAction((error, status) => {
            if (Boolean(status) == true) {
                callback.onTrue();
            } else {
                callback.onFalse();
            }
        });
    }
}

/**
 * remove special characters from string
 */
const removeSpecialCharacters = (msg) => {
    return msg?.replace(/[&\\#+()$~%'";*=`^!?<>{}]/g, '');
};

const getSlotsTimeText = (startTime, endTime) => {
    return get12HourTime(startTime) + ' - ' + get12HourTime(endTime);
};

const get12HourTime = (time) => {
    const timeArr = time.split(':');
    if (timeArr[0] <= '11') {
        return timeArr[0] + ' AM';
    } else if (timeArr[0] === '12') {
        return timeArr[0] + ' PM';
    } else {
        return timeArr[0] - 12 + ' PM';
    }
};

const getServiceName = (serviceType) => {
    switch (serviceType) {
        case 'HA_BD':
            return 'Repair';
        case 'PMS':
            return 'Service';
        default:
            return 'Service';
    }
};

const getActivityName = (isRenewal) => {
    if (isRenewal) {
        return 'Renewal';
    }
    return 'Sales';
};

const openDialerWith = (number) => {
    let phoneNumber = ''
    if (Platform.OS === PLATFORM_OS.android) {
        phoneNumber = `tel:${number ?? 0}`;
    } else {
        phoneNumber = `telprompt:${number ?? 0}`;
    }
    Linking.canOpenURL(phoneNumber)
        .then((supported) => {
            if (supported) {
                return Linking.openURL(phoneNumber);
            }
        })
        .catch((err) => console.warn('An error occurred', err));
}

const getTabNameFromIndex = (tabIndex) => {
    if (tabIndex === 0) {
        return 'Home';
    } else if (tabIndex === 1) {
        return 'Buy';
    } else if (tabIndex === 2) {
        return 'Membership';
    } else if (tabIndex === 3) {
        return 'Account';
    } else {
        return 'Home';
    }
};


const getIndexFromTabName = (tabName) => {
    if (tabName === 'Home') {
        return 0;
    } else if (tabName === 'Buy') {
        return 1;
    } else if (tabName === 'Membership') {
        return 2;
    } else if (tabName === 'Account') {
        return 3;
    } else {
        return 0;
    }
};

export {
    parseJSON,
    deepCopy,
    parseQueryParamsFromUrl,
    removeSpecialCharacters,
    getrewardOnStatus,
    isWalkThroughShow,
    getSlotsTimeText,
    getSODAppliancesData,
    get12HourTime,
    getServiceName,
    getActivityName,
    getCDNDataVersioning,
    getPlansCategoryData,
    getFirebaseData,
    openDialerWith,
    getTabNameFromIndex,
    getIndexFromTabName
};
