import {PLATFORM_OS} from "../Constants/AppConstants";
import {Platform} from "react-native";
export const isScrollViewCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 80;
    return layoutMeasurement.height + contentOffset.y >=
        contentSize.height - paddingToBottom;
};

export const showAlertForAPI = (dialogViewRef, title, alertMessage) => {
    if (dialogViewRef !== null && dialogViewRef !== undefined) {
        dialogViewRef.showDailog({
            title: title,
            message: alertMessage,
            primaryButton: {
                text: 'Ok',
            },
            cancelable: true,
        });
    }
};


// arr1 and arr2 are arrays of any length; equalityFunc is a function which
// can compare two items and return true if they're equal and false otherwise
export function arrayUnion(arr1, arr2, equalityFunc) {
    let union = arr1.concat(arr2);

    for (let i = 0; i < union.length; i++) {
        for (var j = i + 1; j < union.length; j++) {
            if (equalityFunc(union[i], union[j])) {
                union.splice(j, 1);
                j--;
            }
        }
    }

    return union;
}

 function randomString(length, chars) {
    var mask = '';
    if (chars.indexOf('a') > -1) {
        mask += 'abcdefghijklmnopqrstuvwxyz';
    }
    if (chars.indexOf('A') > -1) {
        mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }
    if (chars.indexOf('#') > -1) {
        mask += '0123456789';
    }
    if (chars.indexOf('!') > -1) {
        mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
    }
    var result = '';
    for (var i = length; i > 0; --i) {
        result += mask[Math.floor(Math.random() * mask.length)];
    }
    return result;
}

export function generateRandomString(){
    return randomString(8,'#aA')
}

export function setUnion(setA,setB){
    let _union = new Set(setA)
    for (let elem of setB) {
        _union.add(elem)
    }
    return _union
}

export function getInitiatingSystem() {
   return  (Platform.OS == PLATFORM_OS.ios ? "21" : "20")
}

// function areGamesEqual(g1, g2) {
//     return g1.title === g2.title;
// }

// Function call example
// arrayUnion(arr1, arr2, areGamesEqual);
