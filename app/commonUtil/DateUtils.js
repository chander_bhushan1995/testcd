
export const DATE_MONTH_FORMAT = "dd MMM"

export const formattedDate = (date, format) => {
    let dateFormat = require('dateformat');
    return dateFormat(date, format);
};

export const getTodayDate = () => {
    let dateFormat = require('dateformat');
    return dateFormat(new Date(), 'dd-mmm-yyyy');
};

export const getTomorrowDate = () => {
    const today = new Date();
    const tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1);
    let dateFormat = require('dateformat');
    return dateFormat(tomorrow, 'dd-mmm-yyyy');
};


export const addDaysInDate = (milliSeconds, noOfDays) => {
    let date = new Date(parseInt(milliSeconds));
    date.setDate(date.getDate() + parseInt(noOfDays));
    return date;
};

//a method to compare dates where the arguments are in milliseconds and it returns true if first argumented date
//comes before second argumented date
export const compareDate = (first, second) => {
    var date1 = new Date(parseInt(first));
    var date2 = new Date(parseInt(second));
    return date1 < date2;
};
// get current current date in millis
export const getCurretDateMilliseconds = () => {
    let date = new Date();
    return date.getTime();
};
// no of days difference between two argumented dates
export const daysDifference = (first, second) => {
    let date1 = new Date(parseInt(first));
    let date2 = new Date(parseInt(second));
    let Difference_In_Time = date2.getTime() - date1.getTime();
    let Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
    return Math.ceil(Difference_In_Days);
};

export const minutesDifference = (pastDate,futureDate) => {
    let date1 = new Date(parseInt(futureDate));
    let date2 = new Date(parseInt(pastDate));

    let seconds = Math.floor((date1 - (date2))/1000);
    let minutes = Math.floor(seconds/60);

    return minutes
}

export const minutesSecondDifference = (pastDate,futureData) => {
    let date1 = new Date(parseInt(futureData));
    let date2 = new Date(parseInt(pastDate));

    let seconds = Math.floor((date1 - (date2))/1000);
    let minutes = Math.floor(seconds/60);
    seconds = (seconds < 10 ? '0' : '')+seconds
    minutes = (minutes < 10 ? '0' : '')+minutes
    return `${minutes}:${seconds}`
}

export const getDateText = (date) => {
    if (getTodayDate() === date) {
        return 'today';
    } else if (getTomorrowDate() === date) {
        return 'tomorrow';
    } else {
        return date;
    }
};

export const getDateFromString = (dateString, formatIdentifier) => { //Date in format dd/mm/yyyy or dd-mm-yyyy
    const date = new Date();
    let dateParts = dateString?.split(formatIdentifier);
    dateParts = dateParts.filter(function (el) { return (el != null && el.length>0) }); //remove empty, null
    if(dateParts && dateParts.length === 3) {
        date.setDate(parseInt(dateParts[0]));
        date.setMonth(parseInt(dateParts[1]) - 1);
        date.setFullYear(parseInt(dateParts[2]));
    }
    return date;
};


