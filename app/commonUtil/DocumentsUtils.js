import {PermissionsAndroid, Platform, NativeModules, Alert} from 'react-native';
import BottomSheet from 'react-native-bottomsheet';
import ImagePicker from 'react-native-image-picker';
import * as Actions from '../Constants/ActionTypes';
import * as DocumentType from '../Constants/DocumentTypeConstant';
import {DocumentPicker, DocumentPickerUtil} from 'react-native-document-picker';
import {CAMERA_DENY_PERMISSION, STORRGAE_DENY_PERMISSION} from '../Constants/AppConstants';
import {
    CANCEL,
    CHOOSE_FROM_DOCUMENTS,
    CHOOSE_FROM_GALLERY,
    TAKE_A_PHOTO,
} from '../Constants/DocumentSelectionOptionsContants';

const MAX_FILE_SIZE = 5242880;

const options = {
    quality: 1.0,
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
        skipBackup: true,
    },
};
const ANDROID_DOCUMENT_SELECTION_OPTIONS = [TAKE_A_PHOTO, CHOOSE_FROM_GALLERY];
const IOS_DOCUMENT_SELECTION_OPTIONS = [TAKE_A_PHOTO, CHOOSE_FROM_DOCUMENTS, CHOOSE_FROM_GALLERY, CANCEL];
const chatBrigeRef = NativeModules.ChatBridge;
export default class DocumentsUtil {

    onDocumentSelection(params) {
        this.openBottomSheet(params);
    }

    openBottomSheet(params) {
        const menuItemArray = Platform.OS === 'ios' ? IOS_DOCUMENT_SELECTION_OPTIONS : ANDROID_DOCUMENT_SELECTION_OPTIONS;
        BottomSheet.showBottomSheetWithOptions(
            {
                options: menuItemArray,
                dark: false,
                cancelButtonIndex: menuItemArray.length + 1,
            },
            selectedOptionIndex => {
                params.selectedIndex = selectedOptionIndex;
                this.selectPhotoTapped(params);
            },
        );
    }

// @Params index is stand for bottom sheet slected item index
    // @Params itemIndex is stand for flat list item
    // @params _this is stand for refrence of flat list child component
    // @params onDocumentSelect is function to pass the item index slectcted doc uri,url
    selectPhotoTapped(params) {
        switch (params.selectedIndex) {
            case 0:
                if (Platform.OS === 'android' && Platform.Version >= 22) {
                    this.checkCameraPermissionAndOpenCamera(params).done();
                } else {
                    this.openCamera(params);
                }
                break;
            case 1:
                if (Platform.OS === 'android') {
                    if (Platform.Version >= 22) {
                        this.checkStoragePermissionAndAccessDocument(params).done();
                    } else {
                        this.pickDocumentFromGallery(params);
                    }
                } else {
                    this.pickDocumentFromFiles(params);
                }
                break;
            case 2:
                ImagePicker.launchImageLibrary(options, response => {
                    if (response.didCancel) {
                    } else if (response.error) {
                        if (
                            Platform.OS === 'ios' &&
                            response.error === 'Photo library permissions not granted'
                        ) {
                            chatBrigeRef.imagePickerPermissionDenied('library');
                        }
                    } else if (response.customButton) {
                    } else {
                        const {fileName, docType} = DocumentsUtil.documentData(response);
                        params.onSuccess({
                            type: DocumentType.IMAGE,
                            fileType: docType,
                            fileName: fileName,
                            fileSize: response.fileSize,
                            docUrl: response.uri,
                            docType: response.type,
                        });
                    }
                });
                break;
        }
    }

    // This is responsible to check permission & open camera
    async checkCameraPermissionAndOpenCamera(params) {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    this.openCamera(params);
                } else if (granted === PermissionsAndroid.RESULTS.DENIED) {
                    this.storagePermissionError(params.context(), () => {
                        this.checkCameraPermissionAndOpenCamera(params);
                    });
                } else {
                    this.showRationalPermissionDialog(params.context(), STORRGAE_DENY_PERMISSION);
                }
            } else if (granted === PermissionsAndroid.RESULTS.DENIED) {
                this.cameraPermisionError(params.context(), () => {
                    this.checkCameraPermissionAndOpenCamera(params);
                });
            } else {
                this.showRationalPermissionDialog(params.context(), CAMERA_DENY_PERMISSION);
            }
        } catch (err) {
            console.warn(err);
        }
    }

    async checkStoragePermissionAndAccessDocument(params) {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this.pickDocumentFromGallery(params);
            } else if (granted === PermissionsAndroid.RESULTS.DENIED) {
                this.storagePermissionError(params.context(), () => {
                    this.checkStoragePermissionAndAccessDocument(params);
                });
            } else {
                this.showRationalPermissionDialog(STORRGAE_DENY_PERMISSION);
            }
        } catch (err) {
            console.warn(err);
        }
    }

    cameraPermisionError(context, checkPermissionAgain) {
        if (context.DialogViewRef !== undefined) {
            context.DialogViewRef.showDailog({
                message: CAMERA_DENY_PERMISSION,
                primaryButton: {
                    text: 'Allow', onClick: () => {
                        checkPermissionAgain();
                    },
                },
                secondaryButton: {
                    text: 'Later', onClick: () => {
                    },
                },
                cancelable: false,
                onClose: () => {
                },
            });
        }
    }

    storagePermissionError(_this, checkPermissionAgain) {
        if (_this.DialogViewRef !== undefined) {
            _this.DialogViewRef.showDailog({
                message: STORRGAE_DENY_PERMISSION,
                primaryButton: {
                    text: 'Allow', onClick: () => {
                        checkPermissionAgain();
                    },
                },
                secondaryButton: {
                    text: 'Later', onClick: () => {
                    },
                },
                cancelable: false,
                onClose: () => {
                },
            });
        }
    }

    showRationalPermissionDialog(context, alertMessage) {
        if (context.DialogViewRef !== undefined) {
            context.DialogViewRef.showDailog({
                title: 'We Request Again',
                message: alertMessage,
                primaryButton: {
                    text: 'Go to settings', onClick: () => {
                        chatBrigeRef.openAndroidAppSettings();
                    },
                },
                secondaryButton: {
                    text: 'Later', onClick: () => {
                    },
                },
                cancelable: false,
                onClose: () => {
                },
            });
        }
    }

    openCamera(params) {
        ImagePicker.launchCamera(options, response => {
            if (response.didCancel) {
            } else if (response.error) {
                if (
                    Platform.OS === 'ios' &&
                    response.error === 'Camera permissions not granted'
                ) {
                    chatBrigeRef.imagePickerPermissionDenied('camera');
                }
            } else if (response.customButton) {
            } else {
                const {fileName, docType} = DocumentsUtil.documentData(response);
                params.onSuccess({
                    type: DocumentType.IMAGE,
                    fileType: docType,
                    fileName: fileName,
                    fileSize: response.fileSize,
                    docUrl: response.uri,
                    docType: response.type,
                });
            }
        });
    }

    // This method is used to pick document from Files in iOS
    pickDocumentFromFiles(params) {
        DocumentPicker.show(
            {
                filetype: [DocumentPickerUtil.allFiles()],
            },
            (error, response) => {
                if (response !== null) {
                    const {fileName, docType, type} = DocumentsUtil.documentData(response);
                    params.onSuccess({
                        type: type,
                        fileType: docType,
                        fileName: fileName,
                        fileSize: response.fileSize,
                        docUrl: response.uri,
                        docType: response.type,
                    });
                }
            },
        );
    }

    // To pick the document from the gallery
    pickDocumentFromGallery(params) {
        DocumentPicker.show(
            {
                filetype: [DocumentPickerUtil.allFiles()],
            },
            (error, response) => {
                if (response !== null) {
                    const {fileName, isSupported, isValidSize, docType, type} = DocumentsUtil.documentData(response);
                    if (!isSupported) {
                        DocumentsUtil.showDialogForError(params.context(), Actions.DOCUMENT_TYPE_ERROR_MSG);
                    } else if (!isValidSize) {
                        DocumentsUtil.showDialogForError(params.context(), Actions.DOCUMENT_SIZE_ERROR_MSG);
                    } else {
                        params.onSuccess({
                            type: type,
                            fileType: docType,
                            fileName: fileName,
                            fileSize: response.fileSize,
                            docUrl: response.uri,
                            docType: response.type,
                        });
                    }
                }
            },
        );
    }

    static documentData(response) {
        const fileName = response?.fileName ?? response?.uri?.split("/")?.pop();
        const docType = fileName.substring(fileName.lastIndexOf('.') + 1);
        const type = DocumentType.imageType.includes(docType.toLowerCase()) ? DocumentType.IMAGE : DocumentType.DOCUMENT;

        return {
            fileName: fileName,
            isSupported: DocumentType.supportedDocumentType.includes(docType.toLowerCase()),
            isValidSize: (response.fileSize > 0 && response.fileSize <= MAX_FILE_SIZE),
            docType: docType,
            type: type
        }
    }

    static showDialogForError(context, errorMessage) {
        if (context.DialogViewRef !== undefined) {
            context.DialogViewRef.showDailog({
                message: errorMessage,
                primaryButton: {
                    text: "Okay, got it", onClick: () => {
                    }
                },
                cancelable: true,
                onClose: () => {
                }
            });
        } else {
            Alert.alert(errorMessage);
        }
    }
}
