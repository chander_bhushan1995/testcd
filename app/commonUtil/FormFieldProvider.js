import * as FirebaseKeys from "../Constants/FirebaseConstants"
import { parseJSON, deepCopy} from "../commonUtil/AppUtils";

export const getComponents = (response, initialDetails) => {
    let components = [];
    if (response === null || response === undefined) {
        return components
    }
    for (let i = 0; i < response.length; i++) {
        let component = response[i];
        let prefilledValue = ""
        if (initialDetails !== null && initialDetails !== undefined){
            let value = initialDetails[component.paramName]
            if (value !== null && value !== undefined && value !== 0) {
                prefilledValue = value.toString()
            }
        }
        let editable = (prefilledValue !== "")? component.editable : true
        let displayedComponent = {
            index: i,
            paramName: component.paramName,
            title: component.label,
            placeholder: component.placeHolder,
            maxLength: component.maxLength,
            keyboardType: component.keyboardType,
            inputValidation: component.inputValidation,
            inputValue: prefilledValue,
            errorMessage: component.errorMesssage,
            isMultiLines: component.isMultiLines,
            height: component.height,
            editable: editable,
            isOptional:component.isOptional
        };
        components.push(displayedComponent);
    }
    return components
}

///postboarding

//wallet
export const getPostboardingFFWallet = (prepostJson, initialDetails) => {

    let jsonObject = parseJSON(prepostJson);
    let preBoardingDetails = jsonObject['postboarding'];
    let response = preBoardingDetails[FirebaseKeys.wallet];
    return getComponents(response, initialDetails)
}

//PE
export const getPostboardingFFPE = (prepostJson, initialDetails) => {

    let jsonObject = parseJSON(prepostJson);
    let preBoardingDetails = jsonObject['postboarding'];
    let response = preBoardingDetails[FirebaseKeys.PE];
    return getComponents(response, initialDetails)
}
//PE:laptop
export const getPostboardingFFPE_LT = (prepostJson, initialDetails) => {

    let jsonObject = parseJSON(prepostJson);
    let preBoardingDetails = jsonObject['postboarding'];
    let response = preBoardingDetails[FirebaseKeys.PE_Laptop];
    return getComponents(response, initialDetails)
}
//PE:smart watch
export const getPostboardingFFPE_SW = (prepostJson, initialDetails) => {

    let jsonObject = parseJSON(prepostJson);
    let preBoardingDetails = jsonObject['postboarding'];
    let response = preBoardingDetails[FirebaseKeys.PE_Smartwatch];
    return getComponents(response, initialDetails)
}
//PE: tablet
export const getPostboardingFFPE_TL = (prepostJson, initialDetails) => {

    let jsonObject = parseJSON(prepostJson);
    let preBoardingDetails = jsonObject['postboarding'];
    let response = preBoardingDetails[FirebaseKeys.PE_Tablet];
    return getComponents(response, initialDetails)
}

//HA:HomeAssist
export const getPostboardingFFHS_HA = (prepostJson, initialDetails) => {

    let jsonObject = parseJSON(prepostJson);
    let preBoardingDetails = jsonObject['postboarding'];
    let response = preBoardingDetails[FirebaseKeys.HomeAppliance];
    return getComponents(response, initialDetails)
}
//HA: EW_applianceDetails
export const getPostboardingFFHS_EW_AD = (prepostJson, initialDetails) => {

    let jsonObject = parseJSON(prepostJson);
    let preBoardingDetails = jsonObject['postboarding'];
    let response = preBoardingDetails[FirebaseKeys.HS_EW_AD];

    return getComponents(response, initialDetails)
}
//HA: EW_BasicDEtails
export const getPostboardingFFHS_EW_BD = (prepostJson, initialDetails) => {

    let jsonObject = parseJSON(prepostJson);
    let preBoardingDetails = jsonObject['postboarding'];
    let response = preBoardingDetails[FirebaseKeys.HS_EW_BD];
    return getComponents(response, initialDetails)
}
