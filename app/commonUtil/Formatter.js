export const formatCoverAmount = coverAmount => {
    if (coverAmount < 100000) {
        return coverAmount;
    }
    let numInlac = Math.round(coverAmount / 10000.0) / 10;
    return numInlac + (numInlac > 1 ? " lacs" : " lac");
}

export const coverAmountFormat = argCoverAmount => {
    let x = parseFloat(argCoverAmount)
    if (x >= 10000 && x < 100000) {
        return String(roundedNumberWithScale(x, 1000)) + " Thousand"
    } else if (x >= 100000 && x < 10000000) {
        return String(roundedNumberWithScale(x, 100000)) + " Lakh"
    } else if (x >= 10000000) {
        return String(roundedNumberWithScale(x, 10000000)) + " Crore"
    } else {
        return numberWithComma(argCoverAmount)
    }
}

const roundedNumberWithScale = (argAmount, argScale) => {
    let amount = parseFloat(argAmount)
    let scale = parseFloat(argScale)
    if (amount % scale == 0) {
        return (amount / scale).toFixed(0)
    } else if ((amount / scale < 10) && ((amount * 10) % scale == 0)) {
        return (parseFloat(amount) / parseFloat(scale)).toFixed(2)
    } else {
        return (parseFloat(amount) / parseFloat(scale)).toFixed(1)
    }
}

export const formatMemEndDate = memActivationEndDate => {
    let dateString = memActivationEndDate;
    let dateParts = dateString.split("/");
    let month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][dateParts[1] - 1];
    return dateParts[0] + nth(dateParts[0]) + " " + month;
};

export const formatCreditEnquiresDate = enquiresDate => {
    let dateString = enquiresDate;
    let dateParts = dateString.split("/");
    let month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][dateParts[1] - 1];
    return dateParts[0] + nth(dateParts[0]) + " " + month.substr(0, 3) + " " + dateParts[2];
};


export const formatCreditScoreLastUpdatedOn = lastUpdatedOnDate => {
    let dateString = lastUpdatedOnDate;
    let dateParts = dateString.split("/");
    let month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][dateParts[1] - 1];
    return dateParts[0] + nth(dateParts[0]) + " " + month.substr(0, 3);
};


export const formatDateIn_DD_MMM_yyyy = date => { // d/m/yyyy
    let dateParts = date.split("/");
    let month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][dateParts[1] - 1];
    return dateParts[0] + " " + month + " " + dateParts[2];
};

export const formatDateIn_MMM_DD_yyyy = date => { // INPUT:: d/m/yyyy
    // OUTPUT:: MMM DD, yyyy
    let dateParts = date.toString().split("/");
    let month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][dateParts[1] - 1];
    return month.substr(0, 3) + " " + dateParts[0] + ", " + dateParts[2];
};

export const formatDateIn_MMMyyyy = date => { // INPUT:: dd-mm-yyyy hh:mm:ss
    // OUTPUT:: MMM DD, yyyy
    if (!date) return "";
    let dateParts = date.toString().split(" ")[0]?.split("-");

    let month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][dateParts[1] - 1];
    return month + ", " + dateParts[2];
};

export const formatDateIn_DDMMyyyy = (day, month, year) => {
    // OUTPUT:: DD/MM/yyyy
    month = month.length > 1 ? month : '0' + month;
    day = day.length > 1 ? day : '0' + day;
    return day + "/" + month + "/" + year;
};

export const formatDateIn_DDMMyyy = (dateObj) => { //Input Date object
    let date = dateObj.getDate()
    let month = dateObj.getMonth() + 1
    let year = dateObj.getFullYear()
    return date + "/" + month + "/" + year;
};

export const formatDateTime = (dateObj) => { //Input Date object
    if (!dateObj) return null;
    let date = dateObj.getDate();
    let month = dateObj.getMonth() + 1;
    let year = dateObj.getFullYear();
    let hour = dateObj.getHours();
    let minutes = dateObj.getMinutes();
    let seconds = dateObj.getDate();
    return date + "/" + month + "/" + year + " " + hour + ":" + minutes + ":" + seconds;
};

export const formatDateIn_DD_MMM_yyy = (date) => { //Input Date object
    let monthStr = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return date.getDate() + "-" + monthStr[date.getMonth()].substr(0, 3) + "-" + date.getFullYear();
};

export const formatDateDD_MM_YYYY = dateLong => {
    let billingDate = new Date(dateLong);
    let month = String(billingDate.getMonth() + 1);
    let day = String(billingDate.getDate());
    let year = String(billingDate.getFullYear());
    return (day + "-" + month + "-" + year);
}

export const formatDateDD_MMM_YYYY = dateLong => {
    if (dateLong !== undefined) {
        let billingDate = new Date(dateLong);
        let month = String(billingDate.getMonth() + 1);
        let day = String(billingDate.getDate());
        let year = String(billingDate.getFullYear());
        let monthStr = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][month - 1];
        return (day + " " + monthStr.substr(0, 3) + " " + year);
    }
    return "";
}

export const formateDateWD_DDMMM = dateString => {
    let dateObj = new Date(dateString);
    let monthArray = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    return (days[dateObj.getDay()] + ", " + dateObj.getDate() + " " + monthArray[dateObj.getMonth()])
}

export const formatAMPM = (dateString) => {
    let date = new Date(dateString)
    let hours = date.getHours();
    let minutes = date.getMinutes();
    let ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    let strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

export const nth = d => {
    if (d > 3 && d < 21) return 'th';
    switch (d % 10) {
        case 1:
            return "st";
        case 2:
            return "nd";
        case 3:
            return "rd";
        default:
            return "th";
    }
};

export const parameterizedString = (...args) => {
    const str = args[0];
    const params = args.filter((arg, index) => index !== 0);
    if (!str) return "";
    return str.replace(/%s[0-9]+/g, matchedStr => {
        const letiableIndex = matchedStr.replace("%s", "") - 1;
        return params[letiableIndex];
    });
};

export const getRandomColor = () => {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
};

export const numberWithComma = (nStr) => {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

export const formatDateIn_DDMMMyyyy = (inputDate) => {
    // OUTPUT:: DD/MM/yyyy
    let monthArray = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let date = inputDate
    if (typeof date === "string") {
        inputDate = new Date(inputDate)
    }
    let formattedDate = inputDate.getDay() + "/" + monthArray[inputDate.getMonth()].substr(0, 3) + "/" + inputDate.getFullYear()
    return formattedDate
};
