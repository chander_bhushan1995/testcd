import {LOCATION_PERMISSION, LOCATION_PERMISSION_RATIONAL} from '../Constants/AppConstants';
import {NativeModules, PermissionsAndroid} from 'react-native';
import images from '../images/index.image';

const nativeBridgeRef = NativeModules.ChatBridge;

export function PermissionError(dialogViewRef, checkPermissionAgain) {
    dialogViewRef.showDailog({
        imageUrl: images.area_not_servicable,
        title: LOCATION_PERMISSION,
        message: LOCATION_PERMISSION_RATIONAL,
        primaryButton: {
            text: 'Turn on location', onClick: () => {
                checkPermissionAgain();
            },
        },
        secondaryButton: {
            text: 'Later', onClick: () => {
                checkPermissionAgain();
            },
        },
        cancelable: false,
        onClose: () => {
            nativeBridgeRef.goBack();
        },
    });
}

export async function checkLocationPermission(dialogViewRef, onPermissionGrant) {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            onPermissionGrant();
        } else if (granted === PermissionsAndroid.RESULTS.DENIED) {
            PermissionError(dialogViewRef, () => {
                checkLocationPermission(dialogViewRef, onPermissionGrant);
            });
        } else {
            showRationalPermissionDialog(dialogViewRef, LOCATION_PERMISSION_RATIONAL);
        }
    } catch (err) {
        console.warn(err);
    }
}


export function showRationalPermissionDialog(dialogViewRef, alertMessage) {
    dialogViewRef.showDailog({
        title: 'We Request Again',
        message: alertMessage,
        primaryButton: {
            text: 'Go to settings', onClick: () => {
                nativeBridgeRef.openAndroidAppSettings();
            },
        },
        secondaryButton: {
            text: 'Later', onClick: () => {
            },
        },
        cancelable: false,
        onClose: () => {
            nativeBridgeRef.goBack();
        },
    });
}
