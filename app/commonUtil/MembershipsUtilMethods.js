import { PlanServices } from "../Constants/AppConstants";

export const MembershipStatus = {
    cancelled: "X",
    active: "A",
    inactive: "E",
    failed: "F",
};

export const getIDFenceMemberships = (memberships = []) => {
    let idFenceMems = memberships?.filter((membership) => isIDFenceMembership(membership)) ?? [];
    return idFenceMems;
};

export const isIDFenceMembership = (membership) => {
    return membership?.plan?.services?.filter((service) => {
        return service.serviceName === PlanServices.idFence;
    }).length > 0;
};
