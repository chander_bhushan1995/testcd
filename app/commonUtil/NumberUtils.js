export default class NumberUtils {
    static getFormattedPrice(price) {
        if (price === null || price === undefined || price === "") {
            return "";
        } else {
            let commaNumber = require('comma-number');
            let format = commaNumber.bindWith(',', '.');
            let index = price.toString().indexOf('.');
            if (index > -1) {
                return format(price.substring(0, index));
            } else {
                return format(price);
            }
        }
    }
}