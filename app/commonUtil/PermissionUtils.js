import {IMEI_PERMISSION, IMEI_PERMISSION_RATIONAL} from '../Constants/AppConstants';
import {NativeModules, PermissionsAndroid} from 'react-native';

const nativeBridgeRef = NativeModules.ChatBridge;

export function PermisionError(dialogViewRef, checkPermissionAgain) {
    dialogViewRef.showDailog({
        message: IMEI_PERMISSION,
        primaryButton: {
            text: 'Allow', onClick: () => {
                checkPermissionAgain();
            },
        },
        secondaryButton: {
            text: 'Later', onClick: () => {
                checkPermissionAgain();
            },
        },
        cancelable: false,
        onClose: () => {
            nativeBridgeRef.goBack();
        },
    });
}

export async function checkIMEIPermission(dialogViewRef, onPermissionGrant) {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            onPermissionGrant();

        } else if (granted === PermissionsAndroid.RESULTS.DENIED) {
            PermisionError(dialogViewRef, () => {
                checkIMEIPermission(onPermissionGrant);
            });
        } else {
            showRationalPermissionDailog(dialogViewRef, IMEI_PERMISSION_RATIONAL);
        }
    } catch (err) {
        console.warn(err);
    }
}


export function showRationalPermissionDailog(dialogViewRef, alertMessage) {
    dialogViewRef.showDailog({
        title: 'We Request Again',
        message: alertMessage,
        primaryButton: {
            text: 'Go to settings', onClick: () => {
                nativeBridgeRef.openAndroidAppSettings();
            },
        },
        secondaryButton: {
            text: 'Later', onClick: () => {
            },
        },
        cancelable: false,
        onClose: () => {
            nativeBridgeRef.goBack();
        },
    });
}