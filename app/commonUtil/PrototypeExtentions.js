Array.prototype.first = function() {
  if (this.length > 0) {
    return this[0];
  }
  return null;
};

Array.prototype.last = function() {
  if (this.length > 0) {
    return this[this.length - 1];
  }
  return null;
};

Set.prototype.subSet = function(otherSet)
{
  // if size of this set is greater
  // than otherSet then it can'nt be
  //  a subset
  if(this.size > otherSet.size)
    return false;
  else
  {
    for(var elem of this)
    {
      // if any of the element of
      // this is not present in the
      // otherset then return false
      if(!otherSet.has(elem))
        return false;
    }
    return true;
  }
}


