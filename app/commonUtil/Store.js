import { AsyncStorage } from 'react-native';

export const DEEPLINK_URL_STRING = 'deeplink_url_string';
export const CUSTOMER_BASIC_DETAILS = 'customer_basic_details';
export const WALK_THROUGH_ACTION = 'walk_through_action';
export const BB_QUESTIONS_DATA = 'bb_questions_data';
export const DEVICE_IMEI = 'device_imei';
export const BB_QUESTIONS_VERSION = 'bb_questions_version';
export const SHOW_SCHEDULE_PICKUP_BOTTOMSHEET = 'show_schedule_pickup_bottomsheet';
export const PREF_KEY_RECOMMENDATIONS_DATA = 'recommendationsData';
export const PREF_KEY_RECOMMENDATION_FORCE_UPDATE_TIME = 'recommendation_force_update_time';
export const PREF_KEY_USER_FAV = "UserFavList";
export const PREF_KEY_FAVORITE_HINT_COUNT = "FavoriteHintCount";
export const HOME_SCREEN_DATA = "homeScreenData";
export const ISSUERS = "ISSUERS";
export const CACHE_DATA = "CACHE_DATA";
export const PREF_KEY_BUY_TAB_RED_DOT = "BuyTabRedDot";

// Common Code
export const getValueForKey = (key, callback) => {
    AsyncStorage.getItem(key, callback)
}

export const setValueForKey = (key, value) => {
    if (value) {
        AsyncStorage.setItem(key, value);
    } else {
        AsyncStorage.removeItem(key);
    }
}

export async function setValueForKeyInAsyn(key, value) {
    // Can we store here??
    try {
        await AsyncStorage.setItem(key, value);
    } catch (error) {
        // Error saving data
    }
}

// Walk-Through Action
export const getWalkthroughAction = (callback) => getValueForKey(WALK_THROUGH_ACTION, callback)
export const setWalkthroughAction = (newValue) => setValueForKey(WALK_THROUGH_ACTION, newValue)

// Show Schedule Pickup Bottom-Sheet
export const getShowSchedulePickupBottomSheet = (callback) => getValueForKey(SHOW_SCHEDULE_PICKUP_BOTTOMSHEET, callback)
export const setShowSchedulePickupBottomSheet = (newValue) => setValueForKey(SHOW_SCHEDULE_PICKUP_BOTTOMSHEET, newValue)

// Recommendations Data
export const getRecommendationsData = (callback) => getValueForKey(PREF_KEY_RECOMMENDATIONS_DATA, callback)
export const setRecommendationsData = (newValue) => setValueForKey(PREF_KEY_RECOMMENDATIONS_DATA, newValue)

// Last Recommendation Save Time
export const getLastRecommendationSaveTime = (callback) => getValueForKey(PREF_KEY_RECOMMENDATION_FORCE_UPDATE_TIME, callback)
export const setLastRecommendationSaveTime = (newValue) => setValueForKey(PREF_KEY_RECOMMENDATION_FORCE_UPDATE_TIME, newValue)

// FavoriteHintCount
export const getFavoriteHintCount = (callback) => getValueForKey(PREF_KEY_FAVORITE_HINT_COUNT, callback)
export const setFavoriteHintCount = (newValue) => setValueForKey(PREF_KEY_FAVORITE_HINT_COUNT, newValue)

// Favorite Recommendation Data
export const getFavoriteRecommendationData = (callback) => getValueForKey(PREF_KEY_USER_FAV, callback)
export const setFavoriteRecommendationData = (newValue) => setValueForKey(PREF_KEY_USER_FAV, newValue)

// Home Screen Data
export const getHomeScreenData = (callback) => getValueForKey(HOME_SCREEN_DATA, callback)
export const setHomeScreenData = (newValue) => setValueForKey(HOME_SCREEN_DATA, newValue)

// Deeplink Url
export const getDeeplinkUrl = (callback) => getValueForKey(DEEPLINK_URL_STRING, callback)
export const setDeeplinkUrl = (newValue) => setValueForKey(DEEPLINK_URL_STRING, newValue)

// IMEI
export const getIMEI = (callback) => getValueForKey(DEVICE_IMEI, callback)
export const setIMEI = (newValue) => setValueForKey(DEVICE_IMEI, newValue)

// Issuers
export const getIssuers = (callback) => getValueForKey(ISSUERS, callback)
export const setIssuers = (newValue) => setValueForKey(ISSUERS, newValue)

// Buyback Questions Data
export const getBuybackQuestionsData = (callback) => getValueForKey(BB_QUESTIONS_DATA, callback)
export const setBuybackQuestionsData = (newValue) => setValueForKey(BB_QUESTIONS_DATA, newValue)

// Buyback Questions Version
export const getBuybackQuestionsVersion = (callback) => getValueForKey(BB_QUESTIONS_VERSION, callback)
export const setBuybackQuestionsVersion = (newValue) => setValueForKey(BB_QUESTIONS_VERSION, newValue)

// Customer Basic Details
export const getCustomerBasicDetails = (callback) => getValueForKey(CUSTOMER_BASIC_DETAILS, callback)
export const setCustomerBasicDetails = (newValue) => setValueForKey(CUSTOMER_BASIC_DETAILS, newValue)


// Cache Data
export const getCacheData = (callback) => getValueForKey(CACHE_DATA, callback)
export const setCacheData = (newValue) => setValueForKey(CACHE_DATA, newValue)

//Buy Tab Red Dot
export const getBuyTabRedDot = (callback) => getValueForKey(PREF_KEY_BUY_TAB_RED_DOT, callback)
export const setBuyTabRedDot = (newValue) => setValueForKey(PREF_KEY_BUY_TAB_RED_DOT, newValue)
