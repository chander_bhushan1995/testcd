import {FormField} from "../Catalyst/components/ComponentsFactory";
import {AppStringConstants} from "../Constants/AppStringConstants";

export const isTextAndSpaceOnly = e => /^[a-zA-Z\s]+$/.test(e.toString());

export const isEmailAddressValid = e => /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(e.toString());

export const isValidMobile = e => /^[0][1-9]\d{9}$|^[1-9]\d{9}$/.test(e.toString());

export const isValidPincode = e => /^\d{6}$/.test(e.toString());

export const isValidString = (data) => {
    return (typeof (data) === 'string' && data !== null && data !== undefined && data.trim().length !== 0);
};
export const isValidUPI = data => /^\w+@\w+$/.test(data.toString());

export const validateFormFields = (displayedComponentList) => {
    let validationResult = {
        isValid: false,
        validatedComponents: [],
        userDetailsString: {}
    }

    let isValid = true;
    let formFieldsTemplate = displayedComponentList.map((item) => {
        if (item.inputValue != '') {
            switch (item.paramName) {
                case 'mobileNumber':
                    item.isValid = isValidMobile(item.inputValue);
                    break;
                case 'emailId':
                    item.isValid = isEmailAddressValid(item.inputValue);
                    break;
                case 'firstName':
                    item.isValid = isTextAndSpaceOnly(item.inputValue);
                    break;
                case 'pincode':
                    item.isValid = isValidPincode(item.inputValue);
                    break;
                default:
                    item.isValid = true;
                    break;
            }
            if (item.isValid) {
                validationResult.userDetailsString[item.paramName] = item.inputValue;
            } else {
                item.errorMessage = 'Invalid ' + item.title;
            }
        } else if (!item.isOptional) {
            item.errorMessage = 'Please enter your ' + item.title;
            item.isValid = false;
        } else {
            item.isValid = true;
        }
        isValid = isValid && item.isValid;
        return item;
    });

    validationResult.validatedComponents = formFieldsTemplate
    validationResult.isValid = isValid
    return validationResult
};

export const isFormComplete = (displayedComponentList) => {
    let isValid = displayedComponentList?.components?.every((item) => {
        return Boolean(item.inputValue)
    });
    if (displayedComponentList?.note && displayedComponentList?.note?.type === FormField.CHECK_BOX_INPUT) {
        if (!displayedComponentList?.note?.isChecked) {
            isValid = false;
        }
    }
    return isValid
};


export const setFieldError = (displayedComponentList, index, errorMsg) => {
    let fieldData = displayedComponentList[index]
    if (fieldData.type === FormField.PURCHASE_AMOUNT_RANGE_INPUT) {
        let applianceField = displayedComponentList.filter(item => {
            return item.type === FormField.GADGET_SELECTION_INPUT || item.type === FormField.BRAND_SELECTION_INPUT
        })
        if (applianceField) {
            for (let item of applianceField) {
                if (item.type === FormField.GADGET_SELECTION_INPUT && !item.inputValue) {
                    fieldData.errorMessage = AppStringConstants.ERROR_SELECT_APPLIANCE
                    break
                } else if (item.type === FormField.BRAND_SELECTION_INPUT && !item.inputValue) {
                    fieldData.errorMessage = AppStringConstants.ERROR_SELECT_BRAND;
                    break
                }
            }
        }
    } else if (fieldData.type === FormField.BRAND_SELECTION_INPUT) {
        let applianceField = displayedComponentList.find(item => {
            return item.type === FormField.GADGET_SELECTION_INPUT
        })
        if (!applianceField.inputValue) {
            fieldData.errorMessage = AppStringConstants.ERROR_SELECT_APPLIANCE
        }
    } else {
        fieldData.errorMessage = errorMsg;
    }
    return displayedComponentList
}

