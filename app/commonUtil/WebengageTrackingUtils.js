import WebEngage from "react-native-webengage";
import { NativeModules } from "react-native";
import { PlanServices, Services } from "../Constants/AppConstants";
import { parseJSON, deepCopy} from "../commonUtil/AppUtils";


const chatBrigeRef = NativeModules.ChatBridge;

const trackAppScreen = (screenName) => {
    console.info("Tracking Screen ==> ", screenName);
    let webengage = new WebEngage();
    webengage.screen(screenName);
};
const logWebEnageEvent = (eventName, eventAttr, isSystemEvent = false) => {
    if (!(eventAttr) && !(eventName)) {
        console.warn("event attributes not found for event " + eventName);
        return;
    }
    eventAttr = { ...eventAttr };
    if (eventAttr["Service"]) {
        if (eventAttr["Service"] == PlanServices.extendedWarranty) {
            eventAttr["Service"] = "EW";
        } else if (eventAttr["Service"] == PlanServices.whcInspection) {
            eventAttr["Service"] = "HomeAssist";
        }
    }
    chatBrigeRef.getWebEngageAttr((object) => {

      let dataObject = parseJSON(object) ?? {};
        if (dataObject?.link) {
            eventAttr["link"] = dataObject?.link;
        }
        if (dataObject?.userType) {
            eventAttr["User Type"] = dataObject?.userType ?? "";
        }

        if (dataObject.userGroup) {
            eventAttr["User Group"] = dataObject.userGroup;
        }
        if (dataObject.Catalyst_Product) {
            eventAttr["Catalyst_Product"] = dataObject.Catalyst_Product;
        }
        if (dataObject.Catalyst_Service) {
            eventAttr["Catalyst_Service"] = dataObject.Catalyst_Service;
        }
        if (dataObject.Catalyst_CommSubcat) {
            eventAttr["Catalyst_CommSubcat"] = dataObject.Catalyst_CommSubcat;
        }
        if (dataObject.Catalyst_RID) {
            eventAttr["recommendationId"] = dataObject.Catalyst_RID;
        }
        if (dataObject.Catalyst_UIComponentID) {
            eventAttr["uiComponentId"] = dataObject.Catalyst_UIComponentID;
        }
        if (dataObject.Catalyst_RCID) {
            eventAttr["commId"] = dataObject.Catalyst_RCID;
        }
        eventAttr["User Induced"] = isSystemEvent ? "0" : "1";

        console.info("WebEngage event log ==> \nEvent : " + eventName + ", \nattrs : " + JSON.stringify(eventAttr));
        let webengage = new WebEngage();
        webengage.track(eventName, eventAttr);
    });
};

const logAppsFlyerEvent = (eventName, eventAttr, isSystemEvent) => {
    chatBrigeRef.getWebEngageAttr((object) => {
        let dataObject = parseJSON(object);
        if (dataObject.userType) {
            eventAttr["User Type"] = dataObject.userType;
        }
        if (dataObject.userGroup) {
            eventAttr["User Group"] = dataObject.userGroup;
        }
        if (dataObject.Catalyst_Product) {
            eventAttr["Catalyst_Product"] = dataObject.Catalyst_Product;
        }
        if (dataObject.Catalyst_Service) {
            eventAttr["Catalyst_Service"] = dataObject.Catalyst_Service;
        }
        if (dataObject.Catalyst_CommSubcat) {
            eventAttr["Catalyst_CommSubcat"] = dataObject.Catalyst_CommSubcat;
        }
        if (dataObject.Catalyst_RID) {
            eventAttr["recommendationId"] = dataObject.Catalyst_RID;
        }
        if (dataObject.Catalyst_UIComponentID) {
            eventAttr["uiComponentId"] = dataObject.Catalyst_UIComponentID;
        }
        if (dataObject.Catalyst_RCID) {
            eventAttr["commId"] = dataObject.Catalyst_RCID;
        }
        eventAttr["User Induced"] = isSystemEvent ? "0" : "1";

        console.info("AppsFlyer event log ==> \nEvent : " + eventName + ", \nattrs : " + JSON.stringify(eventAttr));
        chatBrigeRef.logApppsFlyerEvent(eventName, eventAttr);
    });
};

const setWebEnageUserInfo = (userDetails) => {
    let webengage = new WebEngage();
    webengage.user.setEmail(userDetails["emailId"]);
    webengage.user.setFirstName(userDetails["firstName"]);
    webengage.user.setPhone(userDetails["mobileNumber"]);
};

const setWebEnageCustomAttrForUser = (attrName, attrValue) => {
    let webengage = new WebEngage();
    webengage.user.setAttribute(attrName, attrValue);
};

const logChatWebEngageEvent = (logChatEvent, eventName) => {
    if (logChatEvent) {
        try {
            let webengage = new WebEngage();
            let eventData = new Object();
            eventData["timestamp"] = Date.now();
            webengage.track(eventName, eventData);
        } catch (err) {
            console.warn(err);
        }
    }
};

export {
    trackAppScreen,
    logWebEnageEvent,
    setWebEnageUserInfo,
    setWebEnageCustomAttrForUser,
    logChatWebEngageEvent,
    logAppsFlyerEvent,
};
