import colors from "../../Constants/colors";
import {OAImageSource} from "../../Constants/OAImageSource";

export const OverviewCards = {
    AlertsOverview: {
        type: "AlertsOverview",
        title: null
    },
    Todo: {
        type: "TODO",
        title: "YOUR TO DO"
    },
    LinkSocial: {
        type: "LinkSocial",
        title: "LINK ACCOUNTS"
    },
    WarningCell: {
        type: "WarningCell"
    },
    WarningNoActionCell: {
        type: "WarningNoActionCell",

    },
    IDFencePremiumPlanCell: {
        type: "IDFencePremiumPlanCell"
    },
    BankAccount: {
        type: "BankAccount",
        title: "ADD BANK A/CS"
    },
    GovIDs: {
        type: "GovIDs",
        title: "ADD GOVERNMENT IDS"
    },
    Mobile: {
        type: "Mobile",
        title: "ADD MOBILE NUMBERS"
    },

    Blogs:{
        type: "Blogs",
        title: "Read more about Digital Identity"
    },

    BlogSubscription:{
        type: "BlogSubscription",
        title: "Get tips on how to protect your data on internet. Subscribe now."
    },
    ShareApp:{
        type: "ShareApp",
        title: "Tell your friends about OneAssist ID Fence"
    },
    BankSafety:{
        type: "BankSafety",
        title: "100% Secure. Bank level safety"
    }
};

export const AssetsCategories = {
    EMAIL: "Email",
    MOBILE: "Mobile",
    CREDIT_DEBIT: "creditAndDebit",
    GOVERNMENT_IDS: "GovernmentIDs",
    BANK_ACCOUNT: "BankAccount",
    SOCIAL_MEDIA_ACCOUNT: "SocialMediaAccounts"
};
export const DetailsTabRowTypes = {
    ROW_TYPE_TWO_COLUMN: "ROW_TYPE_TWO_COLUMN",
    ROW_TYPE_SINGLE_COLUMN: "ROW_TYPE_SINGLE_COLUMN",
    ROW_TYPE_TWO_COLUMN_WITH_RIGHT_BUTTON: "ROW_TYPE_TWO_COLUMN_WITH_RIGHT_BUTTON"
};

export const DetailsTabStrings = {
    title: "title",
    value: "value",
    billingDate: "billingDate",
    pdfLabel: "PDF",
    titlePaymentHistory: "Payment History",
    titlePaymentMode: "Payment Mode",
    titleCardNumber: "Card Number",
    titlePaymentDetails: "Payment Details",
    titleName: "Name",
    titleEmail: "Email",
    titlePhoneNumber: "Phone Number",
    titleBasicDetails: "Basic Details",
    titlePlanName: "Plan Name",
    titleMembershipID: "Membership ID",
    titlePlanStatus: "Plan Status",
    titleStartDate: "Start Date",
    titleEndIn: "End Date",
    titleEndOn: "Ended ",
    titlePlanDetails: "Plan Details",
    titlePlanBenefits: "Plan Benefits",
    titleFAQs: "FAQs",
    supportNumber: "8080 333 333",
    supportNumber2: "1800 123 3330",
    experianNumber: "022 6641 9000",
    actionUpdateLabel: "Update",
    msgDownloadingTaxInvoice: "Downloading Tax Invoice...",
    titlePaymentReview: "Payment Review",
    paramValueSI: "SI",
    siStatus_F: "F",
    siStatusArr: ["A", "F"],
    titleCancelMembership: "Cancel Membership",
    cancelMemConfirmTitle: "Cancel ID FENCE membership?",
    cancelMemConfirmDesc: "Sure you want to risk your personal information by cancelling the plan?",
    continueMem: "Continue Membership",
    okay: "Okay",
    cancelMem: "Cancel Membership",
    memCancelledTitle: "Membership cancellation successful",
    memCancelledDesc: "Your membership will not be auto-renewed and your card won't be charged from your next billing cycle.",
};

export const DetailsTabRowsActionType = {
    ACTION_UPDATE_PAYMENT_DETAILS: "UPDATE_PAYMENT_DETAILS",
    ACTION_OPEN_FAQ: "OPEN_FAQ",
};

export const AlertsTabConstants = {
    ALERT_TYPE_CREDIT_SCORE: "ALERT_TYPE_CREDIT_SCORE",
    ALERT_TYPE_NORMAL: "ALERT_TYPE_NORMAL",
    ACTION_OPEN_FAQ: "OPEN_FAQ",
    ACTION_RECOMMENDED_ACTIONS: "ACTION_RECOMMENDED_ACTIONS",
    ACTION_DOWNLOAD_REPORT: "ACTION_DOWNLOAD_REPORT",
    ACTION_BUY_NOW: "ACTION_BUY_NOW",
    ACTION_KNOW_MORE: "Know More",
    ALERT_TYPE_GOOD: "GOOD",
    ALERT_TYPE_BAD: "BAD",
    RECOMMENDED_ACTIONS_TITLE: "Recommended Actions",
    labels: {
        TITLE_DOWNLOAD_REPORT: "Download Report",
        TITLE_RECOMMENDED_ACTIONS: "Recommended Actions",
        DOWNLOADING_ALERT_REPORT: "Downloading alert report...",
    },
    expiredMemData: {
        title: "Your membership has expired!",
        subTitle: "Please subscribe again to continue monitoring and see details of the 3 compromises found on your personal, social or financial details.",
        actionText: 'Reactivate now',
        upgradeLabel: 'Reactivate now',
        imageUrl: require('../../images/idfence_mem_exp.webp')
    },
    trialOverMemData: {
        title: "Your ID Fence Trial is over!",
        subTitle: "Please upgrade to continue monitoring and see details of the 3 compromises found on your personal, social or financial details.",
        actionText: 'Know More',
        upgradeLabel: 'Upgrade now',
        imageUrl: require('../../images/idfence_trial_over.webp')
    },

    noAlertFoundData: {
        title: "No alerts found!",
        subTitle: "We are monitoring your personal information and social accounts 24*7 . You will get real time alerts if we detect any compromise.",
        imageUrl: require('../../images/not_alert_found.webp')
    }
};

export const getExpiredMemSubTitle = (unreadAlertCount) => `Please subscribe again to continue monitoring and see details of the ${unreadAlertCount} compromises found on your personal, social or financial details.`;
export const getTrialOverMem = (unreadAlertCount) => `Please upgrade to continue monitoring and see details of the ${unreadAlertCount} compromises found on your personal, social or financial details.`;

const membershipStatus = {
    A: "ACTIVE",
    E: "EXPIRED",
    P: "PENDING",
    X: "CANCELED"
};
export const SocialKeys = {
    FACEBOOK: "FACEBOOK",
    TWITTER: "TWITTER",
    INSTAGRAM: "INSTAGRAM",
    LINKEDIN: "LINKEDIN",
};
export const GovIdKeys = {
    DRIVING_LICENCE: "DL",
    PASSPORT: "PASSPORT",
    PAN_CARD: "PAN"
};
export const AllAssetKeys = {
    ...GovIdKeys,
    ...SocialKeys,
    EMAIL: "Email",
    MOBILE: "Mobile",
    BANK_ACCOUNT: "BankAccount",
    CREDIT_DEBIT: "creditAndDebit",

};
export const SkippableCards = {
    ...AllAssetKeys,
    WARNING: "Warning",
    TRIAL_EXPIRING_POPUP: "TrialExpiringPopup"
};
export const AllTodoCards = [
    AllAssetKeys.CREDIT_DEBIT,
    AllAssetKeys.BANK_ACCOUNT,
    AllAssetKeys.FACEBOOK,
    AllAssetKeys.TWITTER,
    AllAssetKeys.INSTAGRAM,
    AllAssetKeys.EMAIL,
    AllAssetKeys.PAN_CARD,
    AllAssetKeys.LINKEDIN,
    AllAssetKeys.PASSPORT,
    AllAssetKeys.DRIVING_LICENCE,
    AllAssetKeys.MOBILE
];
export var addMoreAssetList = {
    email: null,
    mobile: null,
    bank: null,
    creditAndDebit: null,
    pan: null,
    passport: null,
    dl: null
};

export const creditScoreBadgeViewTypeOne = {
    GOOD: {
        backgroundColor: colors.color_006C00,
        image: OAImageSource.info_indicator
    },
    FAIR: {
        backgroundColor: colors.color_028B00,
        image: OAImageSource.info_indicator
    },
    BAD: {
        backgroundColor: colors.color_E39300,
        image: OAImageSource.info_indicator
    },
    POOR: {
        backgroundColor: colors.color_F63B00,
        image: OAImageSource.info_indicator
    }
};
export const PlanStatus = {
    TRIAL: 'TRIAL',
    TRIAL_EXPIRING: 'TRIAL_EXPIRING',
    TRIAL_EXPIRED: 'TRIAL_EXPIRED',
    TRIAL_INACTIVE: 'TRIAL_INACTIVE',
    ACTIVE: 'ACTIVE',
    EXPIRING: 'EXPIRING',
    EXPIRED: 'EXPIRED',
    INACTIVE: 'INACTIVE',
};
export const DiscardedPlanStates = [
    PlanStatus.TRIAL_INACTIVE, PlanStatus.INACTIVE
];

export const scrollEventThrottle = 400;
export const MAX_ACC_NUM_TEXT = 6;

export const formatSalesPrice = (salesPrice) => `Rs ${salesPrice.toFixed(2)}`;
export const formattedFileName = (orderID) => `Tax_Invoice_${orderID}.pdf`;
export const alertReportFileName = (alertTitle, timestamp) => `IDfence_${alertTitle}_${timestamp}.pdf`;


export const inputValidationRegex = {
    EMAIL_REGEX: /[^A-Za-z0-9!#$%&'*+-/=?^_`{|}~@]/gi,
    NUMBER_REGEX: /[^0-9]/gi,
    ALPHANUMERIC_REGEX: /[^A-Za-z0-9]/gi
};


