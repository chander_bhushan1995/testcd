import {Text} from "react-native";

export const WEBENGAGE_CREDIT_SCORE = {
    EVENT_NAME: {
        CREDIT_SCORE_TREND: 'Credit Score Trend',
        CREDIT_SCORE_KNOW_MORE: 'Credit Score Know More',
        VIEW_PAYMENT_HISTORY: 'View Payment History',
        CONTACT_EXPERIAN: 'Contact Experian',
        ASSET_MIGRATION_CARD_LOADED: 'Asset Migration Card Loaded',
        ASSET_MIGRATION_CARD_TAP: 'Asset Migration Card Tap',
        SCORE_INFO: 'Score Info',
        CREDIT_SCORE_FAQ: 'Credit Score FAQ',
        ID_ASSET_MONITOR_START_NOW: 'ID Asset Monitor Start Now',
    },
    ATTR_NAME: {
        LOCATION: 'Location',
        IMPACT_ITEMS: 'Impact Items',
        BANK: 'Bank',
        MISSED: 'Missed',
        DELAYED: 'Delayed',
        MODE: 'Mode',
        ASSET_NAME: 'Asset Name'
    },
    ATTR_VALUE: {
        OVERVIEW_TAB: 'OverView Tab',
        PAYMENT_HISTORY_SCREEN: 'Payment History Screen',
        IDFENCE_DASHBOARD_OVERVIEW_TAB: 'IDFence Dashboard Overview tab',
        NUMBER_OF_MISSED_PAYMENTS: 'number of missed payments',
        CREDIT_SCORE_DASHBOARD: 'Credit Score Dashboard',
        CREDIT_SCORE_GRAPH: 'Credit Score Graph',
        PAYMENT_HISTORY: 'Payment History',
        CREDIT_UTILIZATION: 'Credit Utilization',
        ACCOUNT_AGE: 'Account Age',
        EMAIL: 'Email',
        CALL: 'Call',
    }
}
