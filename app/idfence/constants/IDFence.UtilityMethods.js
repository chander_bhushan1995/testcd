import {AlertsTabConstants} from "./Constants";
import {formatDateIn_DDMMyyyy} from "../../commonUtil/Formatter";
import {LOCATION, WEBENGAGE_IDFENCE_DASHBOARD} from "../../Constants/WebengageAttrKeys";
import {logWebEnageEvent} from "../../commonUtil/WebengageTrackingUtils";
import {CreditScoreConstants} from "../creditscore/creditscoreutils/CreditScore.constants";

export const alertsTitle = (item, alertsTypeList) => {
    if ((item?.report === true)) {
        return `First Time Report`;
    }
    let title = `${alertsTypeList[item.alertType].alertDisplayValue} Alert`;
    if (item.itemType === AlertsTabConstants.ALERT_TYPE_CREDIT_SCORE) {
        switch (item.alertType) {
            case 'NEW_ACCOUNT':
                title = `New Credit Account Alert`;
                break;
            case 'NEW_ENQUIRY':
                title = `New Credit Enquiry Alert`;
                break;
            case 'ADDRESS_CHANGE':
                title = `Registered Address Change Alert`;
                break;
            case 'SCORE_CHANGE':
                title = `New Credit Score Alert`;
                break;
            default:
                title = `Credit Score Alert`;
        }
    }
    return title;
};

export const alertsExpandedViewTitle = (item) => {
    if ((item.data?.report === true)) {
        if (item.data.alertGenre === 'GOOD') {
            return (item.alertGroup === 'SOCIAL_MEDIA' ? "Congratulations! Your ID Fence report is here and we're happy to say we've not detected any past compromises of your your linked Social account over the last 45 days." :
                "Congratulations! Your ID Fence report is here and we're happy to say that we've not detected any compromises of your added information over the last 7 years.");
        } else {
            return (item.data.alertGroup === 'SOCIAL_MEDIA' ? "Here’s the first Free detailed report with all the past compromises of your linked Social accounts in last 45 days." :
                "Here’s the free detailed report with compromises of your added information in the past 7 years.");
        }
    }
    let title = "";
    if (item.itemType === AlertsTabConstants.ALERT_TYPE_CREDIT_SCORE) {
        switch (item.alertType) {
            case 'NEW_ACCOUNT':
                let accountNumber = item.data["keyValuePairs"]["Account Number"];
                title = `We have found a new account ${accountNumber} in your Credit Report`;
                break;
            case 'NEW_ENQUIRY':
                let creditEnquirer = item.data["keyValuePairs"]["Enquirer"];
                title = `There was a new Credit Score enquiry for your Credit Report by ${creditEnquirer}`;
                break;
            case 'ADDRESS_CHANGE':
                accountNumber = item.data["keyValuePairs"]["Account Number"];
                title = `Registered Address was changed for your account ${accountNumber}`;
                break;
            case 'SCORE_CHANGE':
                let newCreditScore = item.data["keyValuePairs"]["New Credit Score"];
                title = `Your new Credit Score is ${newCreditScore}`;
                break;
            default:
                title = `Your  Credit Score alert found`;

        }
    } else {
        let category = "", displayName = "";
        let keyDataField = item.data["keyDataField"];
        if (keyDataField !== null && keyDataField !== undefined) {
            Object.keys(keyDataField).map(key => {
                category = item.data["keyDataField"][key]?.value;
                displayName = item.data["keyDataField"][key]?.displayName;
            });
        }else {
            category = item.itemTypeDisplayValue;
        }
        switch (item.data["alertGroup"]) {
            case 'SOCIAL_MEDIA':
                let source = item.data["secondaryFields"]?.["source"]?.["value"];
                if (item.data.alertGenre === 'GOOD') {
                    title = `Good News! We have been monitoring your privacy and reputation on ${category} and havn't found a threat in last 2 weeks`;
                } else {
                    title = `Your ${category} on ${source} was compromised`;
                }
                break;
            case 'CYBER_DATA':
                if (item.data.alertGenre === 'GOOD') {
                    title = `Good News! We have been monitoring your ${category} and havn't found a threat in last 14 days`;
                } else {
                    title = `Your ${displayName} ${category} was compromised`;
                }
                break
        }
    }
    return title;
};

export const isBadAlert = (item) => {
    return item.alertGenre === 'BAD';
}

export const isGoodAlert = (item) => {
    return item.alertGenre === 'GOOD';
}

export const isReportAlert = (item) => {
    return (item.alertType === null || item.alertType === undefined);
}

export const alertIndicatorIcon = (item) => {
    if ((item.alertType === null || item.alertType === undefined)) {
        return require("../../images/report.webp");
    }
    if (item.alertGenre === 'GOOD') {
        return require("../../images/no_threats.webp");
    }
    return require("../../images/compromised.webp");
};

export const getCompromisedType = (item) => {
    let compromisedType = "";
    let primaryFields = item.data["primaryFields"];
    if (primaryFields !== null && primaryFields !== undefined) {
        Object.keys(primaryFields).map(key => {
            compromisedType = item.data["primaryFields"][key]?.value;
        });
    }

    return compromisedType;
};

export const getRecommendedActions = (item) => {
    if (item.data.hasOwnProperty("recommendedActions") && item.data?.['recommendedActions'] !== null) {
        return item.data["recommendedActions"];
    }
    return [];
};

export const getUniqueRowID = () => {
    return Math.floor((Math.random() * new Date().getTime()) + 1).toString();
};


export const alertCellSubTitleDate = (createdOn) => `on ${formatCreatedOnDate(createdOn)}`;

export const expandedCellDateOpened = (createdOn) => `${formatCreatedOnDate(createdOn)}`;

function formatCreatedOnDate(createdOn) {
    let timeStampInLong = Number(createdOn)
    let createdOnDate = new Date(timeStampInLong);
    let month = String(createdOnDate.getMonth() + 1);
    let day = String(createdOnDate.getDate());
    let year = String(createdOnDate.getFullYear());
    return formatDateIn_DDMMyyyy(day, month, year);
}


export const capitalizeFLetter = param => param.charAt(0).toUpperCase() + param.slice(1);


export const isMemTrialEndOrExpired = (membershipData) => {
    let memRemainingDays = membershipData?.memRemainingDays;
    let graceDays = membershipData?.graceDays;
    if (memRemainingDays !== null && graceDays !== undefined) {
        if ((parseInt(memRemainingDays) + parseInt(graceDays)) > 0) {
            return false;
        }
    }
    return true;
};

export const memEndOn = (membershipData) => {
    let memRemainingDays = membershipData?.memRemainingDays;
    let graceDays = membershipData?.graceDays;
    return Math.abs(parseInt(memRemainingDays) + parseInt(graceDays));
};

const regexAlphabet = /[^a-zA-Z]/g;
const regexDigits = /[^0-9]/g;
export const sortAlphabetDigits = (item1, item2) => {
    let aAlphabet = item1.replace(regexAlphabet, "");
    let bAlphabet = item2.replace(regexAlphabet, "");
    if (aAlphabet === bAlphabet) {
        let aDigit = parseInt(item1.replace(regexDigits, ""), 10);
        let bDigit = parseInt(item2.replace(regexDigits, ""), 10);
        return aDigit === bDigit ? 0 : aDigit > bDigit ? 1 : -1;
    } else {
        return aAlphabet > bAlphabet ? 1 : -1;
    }
};

export const getFirstItemFromArray = (array) => array.length ? array[0] : '';

export const logWebEngageTabEvent = (membershipData, location) => {
    let eventAttr = getEventAttr(membershipData);
    eventAttr[LOCATION] = location;
    logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_TAB, eventAttr);
};
export const logIDFenceDashboardAccess = (membershipData) => {
    let eventAttr = getEventAttr(membershipData);
    logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_DASHBOARD_ACCESS, eventAttr);
};
const getEventAttr = (membershipData) => {
    let eventAttr = getIDFCommonEventAttr(membershipData);
    eventAttr[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.MOBILE_NUMBER] = membershipData?.mobileNumber;
    eventAttr[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.MEMBERSHIP_ID] = membershipData?.memId;
    eventAttr[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.SOURCE] = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.SOURCE_ATTR_VALUE;
    return eventAttr;
};

export const getIDFCommonEventAttr = (membershipData) => {
    let eventAttr = {}
    eventAttr[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.PLAN_NAME] = membershipData?.planName;
    eventAttr[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.PLAN_CODE] = membershipData?.planCode;
    eventAttr[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.MEMBERSHIP_STATUS] = membershipData?.membershipStatus;
    eventAttr[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.BP] = parseInt(membershipData?.partnerCode);
    eventAttr[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.BU] = parseInt(membershipData?.partnerBUCode);
    return eventAttr
}

export const getValidIntNumberFromString = (string) => {
    if (string === null || string === undefined || string === ".00") {
        return 0;
    }
    return string;
};

export const isNumeric = (n) => {
    return !isNaN(parseFloat(n)) && isFinite(n);
};

export const formatNumberToPercentage = (string) => {
    if (isNumeric(string)) {
        if (string === '.00' || string === '0.0' || string === '-.00') {
            string = `${CreditScoreConstants.defaultValues.INT_DEFAULT} %`;
        } else {
            string = `${string} %`;
        }
    } else {
        string = CreditScoreConstants.defaultValues.STRING_DEFAULT_2;
    }
    return string;
}

export const formatNumberToPercentageNoSpace = (string) => {
    if (isNumeric(string)) {
        if (string === '.00' || string === '0.0' || string === '-.00') {
            string = 0;
        }
    } else {
        string = CreditScoreConstants.defaultValues.INT_DEFAULT;
    }
    return string;
}
