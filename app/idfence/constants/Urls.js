export const Urls = {
    getCustomerInfo: "/idfence-service/customer/getCustomerInfo?subscriberNo=",
    addCyberData: "/idfence-service/customer/addCyberData?subscriberNo=",
    getPlanBenefitsUrl: "/recoengine/plan/planbenefits",
    alertsSummary: "/credit-score-service/alerts/alertsSummary?subscriberNo=",
    alertTodo: "/idfence-service/alerts/alertTodo?subscriberNo=",
    getIDFenceAlertTypes: "/idfence-service/alerts/alertTypes",
    getCreditCoreServiceAlertTypes: "/credit-score-service/alerts/alertTypes",
    deleteCyberData: "/idfence-service/customer/deleteCyberData?subscriberNo=",
    suggestPlans: "/recoengine/plan/suggestplans",
    disconnectSocialMedia: "/idfence-service/customer/disconnectSocialMedia?subscriberNo=",
    retrySubscription: "/credit-score-service/customer/retrySubscription",
    getScoreInfo: "/credit-score-service/customer/getScoreInfo",
    getCardDetail: "/payment-service/api/payment/getCardDetail?bin=",
    renewal:"/OASYS/webservice/rest/api/renewals",
    getAlertDetailsIDFence: "/idfence-service/alerts/alertDetail?subscriberNo=",
    getAlertDetailsCreditScore: "/credit-score-service/alerts/alertDetail?subscriberNo="
};
export const getHistoryUrl = (memId) => `/myaccount/api/membership/history?memId=${memId}`;
export const getPaymentCardUrl = (memUUID) => `/myaccount/api/customer/paymentcard?memUUID=${memUUID}`;
export const downloadFileUrl = (orderID, membershipType) => `/myaccount/api/customer/invoice/download?fileType=pdf&taxType=GST&docType=Tax%20Invoice&orderId=${orderID}&membershipType=${membershipType}`;
export const downloadAlertFileUrl = (subscriberNo, alertId) => `/idfence-service/alerts/downloadPdf?subscriberNo=${subscriberNo}&alertId=${alertId}`;
export const getExcessPaymentUniqueId = (subscriptionType, memId) => `/OASYS/doPaymentViaLink?subscriptionType=${subscriptionType}&memId=${memId}`;
export const getIDFenceFQAUrl = (baseURL) => `${baseURL}/faq/idfence/`;
export const fetchSIStatus = (memUUID) => `/myaccount/api/membership/si/data?memUUID=${memUUID}`;
export const fetchAlertsList = (subscriberNo, pageNo, pageSize, filters) => `/idfence-service/alerts/alertList?subscriberNo=${subscriberNo}&pageNo=${pageNo}&pageSize=${pageSize}&filters=${filters}`;
export const fetchCreditCoreServiceAlertsList = (subscriberNo, pageNo, pageSize, filters) => `/credit-score-service/alerts/alertList?subscriberNo=${subscriberNo}&pageNo=${pageNo}&pageSize=${pageSize}&filters=${filters}`;
export const markAlertRead = (subscriberNo, alertId) => `/idfence-service/alerts/markRead?subscriberNo=${subscriberNo}&alertId=${alertId}`;
export const markCreditScoreAlertRead = (subscriberNo, alertId) => `/credit-score-service/alerts/markRead?subscriberNo=${subscriberNo}&alertId=${alertId}`;
export const getSubscriptionStatus = (subscriberNo) => `/credit-score-service/customer/checkSubscriptionStatus?subscriberNo=${subscriberNo}`;
export const getCustomerSummary = (subscriberNo) => `/credit-score-service/customer/getCustomerSummary?subscriberNo=${subscriberNo}`;
export const getCreditScoreCustomerInfo = (subscriberNo) => `/credit-score-service/customer/getCustomerInfo?subscriberNo=${subscriberNo}`;
export const getCreditScoreTrendInfo = (subscriberNo) => `/credit-score-service/customer/getScoreTrend?subscriberNo=${subscriberNo}`;
export const getCreditScoreAssetsInfo = (subscriberNo) => `/credit-score-service/customer/getAssets?subscriberNo=${subscriberNo}`;
