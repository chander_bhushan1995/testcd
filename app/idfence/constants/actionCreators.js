// export function getOnboardData(data) {
//   return {
//     type: "getOnboardData",
//     data
//   };
// }
// export function getAssetData(data) {
//   return {
//     type: "getAssetData",
//     data
//   };
// }
import * as actions from "./actions";

function makeActionCreator(type, ...argNames) {
    return function (...args) {
        const action = {type};
        argNames.forEach((arg, index) => {
            action[argNames[index]] = args[index];
        });
        return action;
    };
}

export const getDashboardDataFromFirebase = makeActionCreator(
    actions.GET_DASHBOARD_DATA_FROM_FIREBASE,
    "firebaseData"
);

export const getDashboardData = makeActionCreator(
    actions.GET_DASHBOARD_DATA,
    "data",
    "membershipsData",
);

export const getOnboardData = makeActionCreator(
    actions.GET_ONBOARD_DATA,
    "data",
    "membershipsData"
);
export const getAssetData = makeActionCreator(
    actions.GET_ASSETS_DATA,
    "id",
    "data"
);

export const getPaymentHistoryData = makeActionCreator(
    actions.GET_PAYMENT_HISTORY_DATA,
    "data"
);
export const getPaymentCardData = makeActionCreator(
    actions.GET_PAYMENT_CARD_DATA,
    "data"
);
export const getBasicDetails = makeActionCreator(
    actions.GET_BASIC_DETAILS_DATA,
    "data"
);

export const refreshDashboard = makeActionCreator(
    actions.REFRESH_DASHBOARD,
    "data"
);

// export const getPlanBenefitsData = makeActionCreator(
//     actions.GET_PLAN_BENEFITS_DATA,
//     "data"
// );
// export const getSIStatusData = makeActionCreator(
//     actions.GET_SI_STATUS_DATA,
//     "data"
// );
export const getPreparePlanBenefitsData = makeActionCreator(
    actions.PREPARE_PLAN_BENEFITS_DATA,
    "data"
);
export const preparePlanDetailsData = makeActionCreator(
    actions.PREPARE_PLAN_DETAILS_DATA,
    "data"
);
export const getDetailsTabMergedData = makeActionCreator(
    actions.GET_DETAILS_TAB_MERGED_DATA,
    "data"
);
export const resetFlagsData = makeActionCreator(
    actions.RESET_FLAGS,
    "data"
);

export const clearAllFilterFlagsData = makeActionCreator(
    actions.CLEAR_ALL_FLAGS,
    "data"
);
export const applyFilterFlags = makeActionCreator(
    actions.APPLY_FILTER_FLAGS,
    "data"
);

export const getAlertsListData = makeActionCreator(
    actions.GET_ALERTS_LIST_DATA,
    "data"
);
export const getDashboardAndCreditScoreAlerts = makeActionCreator(
    actions.DASHBOARD_AND_CREDIT_SCORE_ALERTS,
    "data"
);
export const getCreditScoreServiceAlertsData = makeActionCreator(
    actions.GET_CREDIT_SCORE_SERVICE_ALERTS_DATA,
    "data"
);


export const getCreditScoreCustomerSummaryData = makeActionCreator(
    actions.GET_CREDIT_SCORE_CUSTOMER_SUMMARY,
    "data"
);
export const prepareAlertsListData = makeActionCreator(
    actions.PREPARE_ALERTS_LIST_DATA,
    "data"
);

export const retrySubscriptionResponse = makeActionCreator(
    actions.RETRY_SUBSCRIPTION_DATA,
    "data"
);


// export const getIdFencePlans = makeActionCreator(
//     actions.GET_ALL_IDFENCE_PLANS,
//     "data"
// );

export const showError = makeActionCreator(
    actions.SHOW_ERROR,
    "errorMessage"
);
export const resetState = makeActionCreator(
    actions.RESET_STATE,
    "data"
);
export const resetCreditScoreDashBoardState = makeActionCreator(
    actions.CREDIT_SCORE_ACTIONS.RESET_CREDIT_SCORE_DASHBOARD_STATE,
    "data"
);
export const showLoader = makeActionCreator(
    actions.SHOW_LOADER
);

export const showLoaderOnOverViewTab = makeActionCreator(
    actions.SHOW_LOADER_OVERVIEW_TAB
);

export const showLoaderOnAssetsTab = makeActionCreator(
    actions.SHOW_LOADER_ASSETS_TAB
);
export const hideLoader = makeActionCreator(
    actions.HIDE_LOADER
);
export const hideLoaderOnOverViewTab = makeActionCreator(
    actions.HIDE_LOADER_OVERVIEW_TAB
);
export const hideLoaderOnAssetsTab = makeActionCreator(
    actions.HIDE_LOADER_ASSETS_TAB
);

export const showCreditScoreAlertsError = makeActionCreator(
    actions.GET_CREDIT_SCORE_SERVICE_ALERTS_DATA_ERROR,
    "errorMessage"
);
export const showNormalAlertsError = makeActionCreator(
    actions.GET_ALERTS_LIST_DATA_ERROR,
    "errorMessage"
);

export const connectSocial = makeActionCreator(actions.CONNECT_SOCIAL, "data");
export const addCyberData = makeActionCreator(actions.ADD_CYBER_DATA, "data");
export const deleteCyberData = makeActionCreator(actions.DELETE_CYBER_DATA);
export const reset = makeActionCreator(actions.RESET_DASHBOARD_TABS);
export const resetOverViewTab = makeActionCreator(actions.RESET_OVERVIEW_TABS);
export const refreshTabBar = makeActionCreator(actions.REFRESH_TAB_BAR);

export const skipCard = makeActionCreator(actions.SKIP_CARD, "cardType");
// export const getAlertsSummary = makeActionCreator(actions.GET_ALERTS_SUMMARY, "data");
// export const getAlertTodo = makeActionCreator(actions.GET_ALERT_TODO, "data");
export const getCreditScoreCustomerData = makeActionCreator(actions.GET_CREDIT_SCORE_CUSTOMER_DATA, "data");

export const preparePaymentData = makeActionCreator(actions.CREDIT_SCORE_ACTIONS.PREPARE_PAYMENT_HISTORY_DATA, "data");
export const prepareCreditUtilisationData = makeActionCreator(actions.CREDIT_SCORE_ACTIONS.PREPARE_CREDIT_UTILISATION_DATA, "data");
export const prepareOldestOpenAccountData = makeActionCreator(actions.CREDIT_SCORE_ACTIONS.PREPARE_OLDEST_OPEN_ACCOUNT_DATA, "data");
export const prepareCardsAndAccountsData = makeActionCreator(actions.CREDIT_SCORE_ACTIONS.PREPARE_CARDS_AND_ACCOUNTS_DATA, "data");
export const prepareCreditEnquiriesData = makeActionCreator(actions.CREDIT_SCORE_ACTIONS.PREPARE_CREDIT_ENQUIRIES_DATA, "data");
export const getScoreInfoData = makeActionCreator(actions.CREDIT_SCORE_ACTIONS.GET_SCORE_INFO, "data");
export const getCreditScoreTrendData = makeActionCreator(actions.CREDIT_SCORE_ACTIONS.GET_CREDIT_SCORE_TREND_INFO, "data");
export const getCreditScoreAssetInfoData = makeActionCreator(actions.CREDIT_SCORE_ACTIONS.GET_CREDIT_SCORE_ASSET_INFO, "data");
export const prepareMonitorAssetData = makeActionCreator(actions.CREDIT_SCORE_ACTIONS.PREPARE_CREDIT_MONITOR_ASSETS_DATA, "data");
export const prepareCreditScoreCustInfoData = makeActionCreator(actions.CREDIT_SCORE_ACTIONS.PREPARE_CREDIT_SCORE_CUST_INFO_DATA, "data");
export const refreshMonitorAssets = makeActionCreator(actions.CREDIT_SCORE_ACTIONS.RESET_MONITOR_ASSET_STATE);
export const getCreditScoreDashboardData = makeActionCreator(actions.CREDIT_SCORE_ACTIONS.GET_CREDIT_SCORE_DASHBOARD_DATA, "data");
