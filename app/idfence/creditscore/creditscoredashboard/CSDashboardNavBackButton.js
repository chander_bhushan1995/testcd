import React from 'react';
import type {ColorPropType} from 'react-native';
import {I18nManager, Image, Platform, Text, TouchableOpacity, View} from 'react-native';
import colors from "../../../Constants/colors";
import {OAImageSource} from "../../../Constants/OAImageSource";
import spacing from "../../../Constants/Spacing";
import dimens from "../../../Constants/Dimens";

type Props = {
    title?: string,
    onPress: Function,
    pressColorAndroid?: ColorPropType
};

const CSDashboardNavBackButton = ({title,tintColor, onPress, pressColorAndroid}: Props) => (
    <TouchableOpacity
        onPress={onPress}
        style={styles.container}
    >
        <View style={styles.flexRowCentered}>
            <Image
                style={[styles.icon, {tintColor: tintColor}, OAImageSource.icon_back_img.dimensions]}
                source={OAImageSource.icon_back_img.source}
            />
        </View>
    </TouchableOpacity>
);

CSDashboardNavBackButton.defaultProps = {
    title: 'Back',
    pressColorAndroid: 'rgba(0, 0, 0, .2)'
};

export default CSDashboardNavBackButton;

const styles = {
    container: {
        position: 'absolute',
        left: spacing.spacing0,
        backgroundColor: 'transparent'
    },
    flexRowCentered: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    icon: {
        height: dimens.dimen30,
        width: dimens.dimen30,
        margin: spacing.spacing16,
        resizeMode: 'contain',
        transform: [{scaleX: I18nManager.isRTL ? -1 : 1}]
    }
};
