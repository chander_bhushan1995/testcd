import {NativeModules} from "react-native";
import {connect} from "react-redux";
import CreditScoreDashboard from "./CreditScoreDashboard.component";
import { promiseForGetRequest } from "../../../network/ApiManager";
import {getCreditScoreDashboardData, resetCreditScoreDashBoardState, showError} from "../../constants/actionCreators";
import {
    getCreditScoreAssetsInfo,
    getCreditScoreCustomerInfo,
    getCreditScoreTrendInfo,
    Urls
} from "../../constants/Urls";
import { parseJSON, deepCopy} from "../../../commonUtil/AppUtils";

const nativeBridgeRef = NativeModules.ChatBridge;

const mapStateToProps = state => ({
    isLoading: state.creditScoreDashboardReducer.isLoading,
    creditScoreCustInfo: state.creditScoreDashboardReducer.creditScoreCustInfo,
    scoreInfo: state.creditScoreDashboardReducer.scoreInfo,
    trendInfo: state.creditScoreDashboardReducer.trendInfo,
    isApiError: state.creditScoreDashboardReducer.isApiError,
    assetInfoData: state.creditScoreDashboardReducer.assetInfoData,
    csCustInfo: state.creditScoreDashboardReducer.csCustInfo
});

const mapDispatchToProps = dispatch => ({
    getCreditScoreDashBoardData: (subscriptionNo) => {
        nativeBridgeRef.getApiProperty(apiProperty => {
            apiProperty = parseJSON(apiProperty);
            let urlCustomerInfo =
                apiProperty.api_gateway_base_url + getCreditScoreCustomerInfo(subscriptionNo);
            let apiRequestCustomerInfo = promiseForGetRequest(
                urlCustomerInfo,
                apiProperty.apiHeader
            );

            let urlScoreInfo =
                apiProperty.api_gateway_base_url + Urls.getScoreInfo;
            let apiRequestScoreInfo = promiseForGetRequest(
                urlScoreInfo,
                apiProperty.apiHeader
            );

            let urlTrendInfo =
                apiProperty.api_gateway_base_url + getCreditScoreTrendInfo(subscriptionNo);
            let apiRequestTrendInfo = promiseForGetRequest(
                urlTrendInfo,
                apiProperty.apiHeader
            );

            let urlAssetsInfo =
                apiProperty.api_gateway_base_url + getCreditScoreAssetsInfo(subscriptionNo);
            let apiRequestAssetsInfo = promiseForGetRequest(
                urlAssetsInfo,
                apiProperty.apiHeader
            );

            let urlIDFenceCustomerInfo =
                apiProperty.api_gateway_base_url + Urls.getCustomerInfo + subscriptionNo;
            let apiRequestIDFenceCustomerInfo = promiseForGetRequest(
                urlIDFenceCustomerInfo,
                apiProperty.apiHeader
            );

            let combinedRequest = {
                "apiCreditScoreCustomerInfoRequest": {},
                "apiScoreInfoRequest": {},
                "apiTrendInfoRequest": {},
                "apiAssetsInfoRequest": {},
                "apiIDFenceCustomerInfoRequest": {}
            };

            Promise.all([apiRequestCustomerInfo,
                apiRequestScoreInfo,
                apiRequestTrendInfo,
                apiRequestAssetsInfo,
                apiRequestIDFenceCustomerInfo]).then(function (responses) {
                combinedRequest["apiCreditScoreCustomerInfoRequest"] = responses[0];
                combinedRequest["apiScoreInfoRequest"] = responses[1];
                combinedRequest["apiTrendInfoRequest"] = responses[2];
                combinedRequest["apiAssetsInfoRequest"] = responses[3];
                combinedRequest["apiIDFenceCustomerInfoRequest"] = responses[4];
                dispatch(getCreditScoreDashboardData(combinedRequest));
            }).catch(function (error) {
                dispatch(showError(error));
            });
        });
    },
    resetDataState: () => {
        dispatch(resetCreditScoreDashBoardState(""));
    },


});

export default connect(mapStateToProps, mapDispatchToProps)(CreditScoreDashboard);
