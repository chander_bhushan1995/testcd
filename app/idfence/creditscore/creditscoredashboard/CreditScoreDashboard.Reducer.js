import {CREDIT_SCORE_ACTIONS, SHOW_ERROR} from "../../constants/actions";
import {CreditScoreConstants} from "../creditscoreutils/CreditScore.constants";
import spacing from "../../../Constants/Spacing";

const initialState = {
    creditScoreCustInfo: {},
    trendInfo: {},
    scoreInfo: {scoreMapList: [], paymentHistoryMapList: [], creditUtilizationMapList: []},
    isLoading: true,
    assetInfoData: {},
    csCustInfo: {},
    isApiError: null,
}
let TAG = 'creditScoreDashboardReducer';
const creditScoreDashboardReducer = (state = initialState, action) => {

    switch (action.type) {
        case CREDIT_SCORE_ACTIONS.GET_CREDIT_SCORE_DASHBOARD_DATA:
            let data = action?.data;
            let creditScoreCustInfo = data?.apiCreditScoreCustomerInfoRequest?.data;
            let scoreData = data?.apiScoreInfoRequest?.data;
            scoreData = prepareScoreInfo(scoreData);
            let trendInfoData = data?.apiTrendInfoRequest?.data?.scoreList ?? [];
            trendInfoData = prepareTrendInfoData(trendInfoData);
            let assetInfoData = data?.apiAssetsInfoRequest?.data;
            let csCustInfo = data?.apiIDFenceCustomerInfoRequest?.data;

            return {
                ...state,
                creditScoreCustInfo: creditScoreCustInfo,
                assetInfoData: assetInfoData,
                trendInfo: trendInfoData,
                scoreInfo: scoreData,
                csCustInfo: csCustInfo,
                isApiError: null,
                isLoading: false
            };
        case SHOW_ERROR:
            let errorMsg = "Unable to process. please try again!";
            if (action.errorMessage?.message !== null && action.errorMessage?.message !== undefined) {
                errorMsg = action.errorMessage?.message;
            } else if (action.errorMessage?.hasOwnProperty('error')) {
                errorMsg = action.errorMessage?.error[0]?.message;
            }
            return {
                ...state,
                creditScoreCustInfo: {},
                isApiError: errorMsg,
                isLoading: false
            };

        case CREDIT_SCORE_ACTIONS.RESET_CREDIT_SCORE_DASHBOARD_STATE:
            return initialState;
        default:
            return {
                ...state,
                isApiError: null
            };
    }

    function prepareTrendInfoData(scoreList) {
        let scoreItems = [];
        scoreList.map(item => {
            scoreItems.push({x: item.month, y: item.score})
        });
        const dataItems = {
            scoreItems: scoreItems.reverse()
        }
        return dataItems;
    }

    function prepareScoreInfo(data) {
        let scoreMap = data?.scoreMap ?? {};
        let scoreMapList = [];
        let scoreInfo = {scoreMapList: [], paymentHistoryMapList: [], creditUtilizationMapList: []};
        Object.keys(scoreMap).forEach(key => {
            let scoreMapColors = CreditScoreConstants.creditScoreColorMap[scoreMap[key]];
            let item = {
                sColor: scoreMapColors?.sColor,
                eColor: scoreMapColors?.eColor,
                lTitle: key,
                rTitle: (scoreMap[key] === 'Immediate Action Required' ? 'Poor' : scoreMap[key])
            }
            scoreMapList.push(item)

        });
        scoreInfo.scoreMapList = scoreMapList;

        //paymentHistoryMap
        let paymentHistoryMap = data?.paymentHistoryMap ?? {};
        let paymentHistoryMapList = [];
        Object.keys(paymentHistoryMap).forEach(key => {
            let paymentMapColors = CreditScoreConstants.paymentHistoryColorMap[paymentHistoryMap[key]];
            let item = {
                sColor: paymentMapColors?.sColor,
                eColor: paymentMapColors?.eColor,
                lTitle: key,
                rTitle: paymentHistoryMap[key]
            }
            paymentHistoryMapList.push(item)

        });
        scoreInfo.paymentHistoryMapList = paymentHistoryMapList;
        //creditUtilizationMap
        let creditUtilizationMap = data?.creditUtilizationMap ?? {};
        let creditUtilizationMapList = [];
        Object.keys(creditUtilizationMap).forEach(key => {
            let creditUtilizationColors = CreditScoreConstants.paymentHistoryColorMap[creditUtilizationMap[key]];
            let item = {
                sColor: creditUtilizationColors?.sColor,
                eColor: creditUtilizationColors?.eColor,
                lTitle: key,
                rTitle: creditUtilizationMap[key]
            }
            creditUtilizationMapList.push(item)
        });
        scoreInfo.creditUtilizationMapList = creditUtilizationMapList;
        return scoreInfo;
    }
};

export default creditScoreDashboardReducer;
