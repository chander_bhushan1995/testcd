import {BackHandler, NativeModules, Platform, SafeAreaView, StatusBar, View} from 'react-native';
import React from "react";
import colors from "../../../Constants/colors";
import DashboardNavBar from "./dashboardnavbar";
import DashboardToolbar from "./dashboardtoolbar";
import DashboardContent from "./dashboardcontent";
import styles from "./CreditScoreDashboard.style";
import {getIDFenceFQAUrl} from "../../constants/Urls";
import {DetailsTabStrings} from "../../constants/Constants";
import OAActivityIndicator from "../../../CustomComponent/OAActivityIndicator";
import DialogView from "../../../Components/DialogView";
import {CreditScoreConstants} from "../creditscoreutils/CreditScore.constants";
import CreditScoreInfoBottomSheet from "./CreditScoreInfoBottomSheet.component";
import {LOCATION} from "../../../Constants/WebengageAttrKeys";
import {logWebEnageEvent} from "../../../commonUtil/WebengageTrackingUtils";
import {WEBENGAGE_CREDIT_SCORE} from "../../constants/CreditScoreWebEngage.events";
import CollapsibleToolbar from "../CollapsibleToolbar";
import {OAImageSource} from "../../../Constants/OAImageSource";
import {getIDFCommonEventAttr} from '../../constants/IDFence.UtilityMethods';
import { parseJSON, deepCopy} from "../../../commonUtil/AppUtils";

const nativeBridgeRef = NativeModules.ChatBridge;
let navigator;
let TAG = 'CreditScoreDashboard';
let cscoreInfoBottomSheetRef = React.createRef();
let scrollViewRef = React.createRef();
let subscriberNo;
const dashboardNavBarRef = React.createRef();
export default class CreditScoreDashboard extends React.Component {
    constructor(props) {
        super(props);

        let navigation = this.props?.navigation;
        this.state = {
            collapsedNavBarBackgroundColor: colors.color_0282F0

        };
    }

    static navigationOptions = ({navigation}) => {
        navigator = navigation;
        subscriberNo = navigation.getParam("subscriberNo", "");
        return {
            header: null,
            headerTintColor: colors.white
        };
    };

    componentWillMount() {
        StatusBar.setBarStyle('light-content');
        if (Platform.OS === 'android') {
            StatusBar.setTranslucent(true);
            StatusBar.setBackgroundColor('transparent', true);
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        const {navigation} = this.props;
        this.props.getCreditScoreDashBoardData(navigation.getParam("subscriberNo", ""));
    }

    //Render UI Other contents
    renderContent = () => (
        <View>
            <DashboardContent creditScoreCustInfo={this.props.creditScoreCustInfo}
                              assetInfoData={this.props.assetInfoData}
                              trendInfo={this.props.trendInfo}
                              csCustInfo={this.props.csCustInfo}
                              onPress={(e, item) => this.onAction(e, item)}
                              membershipData ={navigator.getParam('membershipsData', null)}
            />
            <CreditScoreInfoBottomSheet ref={cscoreInfoBottomSheetRef}/>
        </View>

    );

    onAction(e, item) {
        if (typeof e?.stopPropagation === "function") {
            e?.stopPropagation();
        }

        switch (item.cardType) {
            case 'FAQ_ROW':
                nativeBridgeRef.getApiProperty(apiProperty => {
                    apiProperty = parseJSON(apiProperty);
                    let tncUrlObj = getIDFenceFQAUrl(apiProperty.api_base_url);
                    navigator.navigate('TnCWebViewComponent', {
                        productTermAndCUrl: tncUrlObj,
                        enableNativeBack: false,
                        otherParam: DetailsTabStrings.titleFAQs
                    });
                });
                break;
            case 'MORE_ABOUT_IDENTITY_THEFT_ROW':
                alert("not implemented yet!")
                break;
            case 'CREDIT_DEBIT_CARDS_FOUND_CARD':
                if (item?.dataList !== null && item?.dataList.length > 0) {
                    let assetsListForWebenageEvent = [];
                    item?.dataList.map(item => {
                        assetsListForWebenageEvent.push(item?.assetType);
                    });
                    if (assetsListForWebenageEvent.length > 0) {
                        assetsListForWebenageEvent = [...new Set(assetsListForWebenageEvent)];
                        let webEngageEvents = getIDFCommonEventAttr(navigator.getParam("membershipsData", null));
                        webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.ASSET_NAME] = assetsListForWebenageEvent.join(',');
                        logWebEnageEvent(WEBENGAGE_CREDIT_SCORE.EVENT_NAME.ID_ASSET_MONITOR_START_NOW, webEngageEvents);
                    }
                }

                this.props.navigation.navigate("MonitorAssets", {
                    assetInfoData: item,
                    subscriberNo: subscriberNo
                })
                break;
            case CreditScoreConstants.dashboardCardsTemplate.paymentsDoneOnTimeCard.cardType:
                this.logKnowMoreEvent(CreditScoreConstants.dashboardCardsTemplate.paymentsDoneOnTimeCard.placeHolderOne);
                this.startDetailsScreen("CSPaymentHistory");
                break;
            case CreditScoreConstants.dashboardCardsTemplate.creditLimitUtilizationCard.cardType:
                this.logKnowMoreEvent(CreditScoreConstants.dashboardCardsTemplate.creditLimitUtilizationCard.placeHolderOne);
                this.startDetailsScreen("CSCreditUtilisation");
                break;
            case CreditScoreConstants.dashboardCardsTemplate.ageOfYourOldestOpenAccountCard.cardType:
                this.logKnowMoreEvent(CreditScoreConstants.dashboardCardsTemplate.ageOfYourOldestOpenAccountCard.placeHolderOne);
                this.startDetailsScreen("CSOldestOpenAccount");
                break;
            case CreditScoreConstants.dashboardCardsTemplate.totalNoOfCreditCardsAndLoansCard.cardType:
                this.logKnowMoreEvent(CreditScoreConstants.dashboardCardsTemplate.totalNoOfCreditCardsAndLoansCard.placeHolderOne);
                this.startDetailsScreen("CSCardsAndAccounts");
                break;
            case CreditScoreConstants.dashboardCardsTemplate.creditEnquiriesCard.cardType:
                this.logKnowMoreEvent(CreditScoreConstants.dashboardCardsTemplate.creditEnquiriesCard.placeHolderOne);
                this.startDetailsScreen("CreditEnquiries");
                break;
            case "SHOW_SCORE_INFO":
                if (cscoreInfoBottomSheetRef.current) {
                    cscoreInfoBottomSheetRef.current.setBottomSheetData(this.props.scoreInfo.scoreMapList);
                    cscoreInfoBottomSheetRef.current.setBottomSheetVisible(true);
                    let webEngageEvents = getIDFCommonEventAttr(navigator.getParam("membershipsData", null));
                    webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.LOCATION] = WEBENGAGE_CREDIT_SCORE.ATTR_VALUE.CREDIT_SCORE_GRAPH;
                    logWebEnageEvent(WEBENGAGE_CREDIT_SCORE.EVENT_NAME.SCORE_INFO, webEngageEvents);
                }
                break;
            case "SHOW_TREND_INFO":
                this.logViewTrendEvent();
                if (scrollViewRef.current) {
                    scrollViewRef.current.scrollToView();

                }
                break;
        }
    }

    startDetailsScreen = (screenName) => {
        this.props.navigation.navigate(screenName, {
            creditScoreCustInfo: this.props.creditScoreCustInfo,
            scoreInfo: this.props.scoreInfo,
            customerSummaryData: navigator.getParam("customerSummaryData", ""),
            membershipsData: navigator.getParam("membershipsData", null)
        })
    };

    //Render UI for Nav Bar contents
    renderNavBar = () => (
        <DashboardNavBar ref={dashboardNavBarRef} toolBarTitle="Your credit score" titleStyle={{
            color: (this.state.collapsedNavBarBackgroundColor === colors.color_FFFFFF ?
                colors.color_000000 : colors.color_FFFFFF)
        }}
                         onPress={this.onBackPress}/>
    );

    // Render UI for ToolBar Contains
    renderToolBar = () => (
        <DashboardToolbar scoreData={this.props.creditScoreCustInfo?.score}

                          infoCreditScorePress={(e, item) => this.onAction(e, item)}/>
    );
    logKnowMoreEvent = (impactItem) => {
        let webEngageEvents = getIDFCommonEventAttr(navigator.getParam("membershipsData", null));
        webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.IMPACT_ITEMS] = impactItem;
        logWebEnageEvent(WEBENGAGE_CREDIT_SCORE.EVENT_NAME.CREDIT_SCORE_KNOW_MORE, webEngageEvents);
    };
    logViewTrendEvent = () => {
        let webEngageEvents = getIDFCommonEventAttr(navigator.getParam("membershipsData", null));
        webEngageEvents[LOCATION] = WEBENGAGE_CREDIT_SCORE.ATTR_VALUE.OVERVIEW_TAB;
        logWebEnageEvent(WEBENGAGE_CREDIT_SCORE.EVENT_NAME.CREDIT_SCORE_TREND, webEngageEvents);
    };

    render() {
        if (this.props.isLoading) {
            return <OAActivityIndicator/>;
        }
        if (this.props.isApiError !== null) {
            this.showAlert(this.props.isApiError);
        }
        if (cscoreInfoBottomSheetRef.current) {
            cscoreInfoBottomSheetRef.current.setBottomSheetData(this.props.scoreInfo.creditUtilizationMapList);
        }
        return (
            <SafeAreaView style={styles.safeAreaStyle}>
                <CollapsibleToolbar
                    ref={scrollViewRef}
                    renderContent={this.renderContent}
                    renderNavBar={this.renderNavBar}
                    renderToolBar={this.renderToolBar}
                    collapsedNavBarBackgroundColor={this.state.collapsedNavBarBackgroundColor}
                    onContentScroll={this.onScrollChange}
                    translucentStatusBar
                    showsVerticalScrollIndicator={false}
                />
                <DialogView ref={ref => (this.DialogViewRef = ref)}/>
            </SafeAreaView>
        );
    }

    onScrollChange = (event) => {
        if (event.nativeEvent.contentOffset.y > 25) {
            if (this.state.collapsedNavBarBackgroundColor !== colors.color_FFFFFF) {
                this.setState({collapsedNavBarBackgroundColor: colors.color_FFFFFF});

            }
            /* if (dashboardNavBarRef.current) {
                 dashboardNavBarRef.current.changeTitleColorAndBackImage(true);
             }*/
        } else {
            if (this.state.collapsedNavBarBackgroundColor !== colors.color_0282F0) {
                this.setState({collapsedNavBarBackgroundColor: colors.color_0282F0});

            }
            /* if (dashboardNavBarRef.current) {
                 dashboardNavBarRef.current.changeTitleColorAndBackImage(false);
             }*/
        }
    }

    showAlert = alertMessage => {
        if (this.DialogViewRef !== null && this.DialogViewRef !== undefined) {
            this.DialogViewRef.showDailog({
                title: '',
                message: alertMessage,
                primaryButton: {
                    text: 'OK', onClick: () => {
                        navigator.pop();
                    },
                },
                cancelable: true,
                onClose: () => {
                    navigator.pop();
                },
            });
        }
    };

    onBackPress = () => {
        navigator.pop();
        return true;
    };

    componentWillUnmount() {
        StatusBar.setBarStyle('default');
        if (Platform.OS === 'android') {
            StatusBar.setTranslucent(false);
            StatusBar.setBackgroundColor(colors.color_1468e2, true);
        }
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        this.props.resetDataState();
    }
}
