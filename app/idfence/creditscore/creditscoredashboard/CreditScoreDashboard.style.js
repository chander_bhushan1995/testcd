import {StyleSheet} from "react-native";
import colors from "../../../Constants/colors";
import spacing from "../../../Constants/Spacing";

export default StyleSheet.create({
    safeAreaStyle: {
        flex: spacing.spacing1,
        backgroundColor: colors.color_004890
    }
});
