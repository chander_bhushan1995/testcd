import React, {Component} from 'react';
import {Modal, SafeAreaView, StyleSheet,StatusBar, Text, TouchableOpacity, View} from 'react-native';
import colors from "../../../Constants/colors";
import LinearGradient from "react-native-linear-gradient";
import {OATextStyle} from "../../../Constants/commonstyles";
import spacing from "../../../Constants/Spacing";
import dimens from "../../../Constants/Dimens";
import floats from "../../../Constants/Floats";

let refCreditScoreInfoBottomSheet = React.createRef();

export default class CreditScoreInfoBottomSheet extends Component {
    state = {modalVisible: false, isModalCancelable: true, isLoading: false, scoreInfo: []};


    setBottomSheetVisible(visible) {
        this.setState({modalVisible: visible});
    }

    setBottomSheetData(scoreInfo) {
        this.setState({scoreInfo: scoreInfo});
    }

    setBottomSheetCancel(status) {
        this.setState({isModalCancelable: status, isLoading: !status});
    }

    setErrorMessage(message) {
        if (refCreditScoreInfoBottomSheet.current) {
            refCreditScoreInfoBottomSheet.current.setErrorMessage(message);
        }
    }


    render() {
        return (
            <View>
                <Modal
                    animationType="fade"
                    backdropColor='transparent'
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        if (this.state.isModalCancelable) {
                            this.setBottomSheetVisible(!this.state.modalVisible);
                        }
                    }}>
                    <StatusBar backgroundColor="rgba(0,0,0,0.7)"  barStyle="light-content"/>
                    <TouchableOpacity
                        activeOpacity={1}
                        style={styles.touchableOpacityStyle}
                        onPress={() => {
                            if (this.state.isModalCancelable) {
                                this.setBottomSheetVisible(false)
                            }
                        }}>

                        <SafeAreaView style={styles.container}>
                            <View style={styles.safeAreaViewStyle}>
                                <View style={{
                                    flex: spacing.spacing1,
                                    paddingVertical: spacing.spacing12,
                                    minHeight: this.state?.scoreInfo.length * 75,
                                    paddingHorizontal: spacing.spacing26
                                }}>
                                    {gradientRow(this.state?.scoreInfo)}
                                </View>
                            </View>

                        </SafeAreaView>

                    </TouchableOpacity>
                </Modal>
            </View>
        );
    }
}

function gradientRow(scoreInfo) {
    return scoreInfo.map((item, index) => {
        return (<LinearGradient
            key={index}
            start={{x: 0.0, y: 0.5}} end={{x: 0.5, y: 1.0}}
            style={styles.linearGradientStyle}
            colors={[item.sColor, item.eColor]}>
            <View style={styles.leftTitleContainerStyle}>
                <Text style={styles.leftTitleStyle}>{item.lTitle}</Text>
            </View>
            <View style={{flex: spacing.spacing1}}>
                <Text style={styles.rightTitleStyle}>{item.rTitle}</Text>
            </View>
        </LinearGradient>);
    });

}


const styles = StyleSheet.create({
    container: {
        flex: spacing.spacing1,
    },
    touchableOpacityStyle: {
        flex: spacing.spacing1,
        flexDirection: 'row',
        marginTop:-40,
        alignItems: 'flex-end',
        backgroundColor: 'rgba(0,0,0,0.7)'
    },
    safeAreaViewStyle: {
        alignSelf: 'flex-start',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        maxHeight: dimens.height80,
        flexDirection: 'row',
        borderTopLeftRadius: spacing.spacing20,
        borderTopRightRadius: spacing.spacing20,
        backgroundColor: colors.color_FFFFFF,
        padding: spacing.spacing0
    },
    linearGradientStyle: {
        height: dimens.dimen34,
        justifyContent: 'space-between',
        flex: spacing.spacing1,
        alignItems: 'center',
        flexDirection: 'row',
        marginVertical: spacing.spacing15,
        borderRadius: spacing.spacing2
    },
    leftTitleContainerStyle: {
        flex: floats.float0_8,
        paddingHorizontal: spacing.spacing10
    },
    leftTitleStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        color: colors.color_FFFFFF
    },
    rightTitleStyle: {
        ...OATextStyle.TextFontSize_14_bold,
        color: colors.color_000000
    }
});


