import {prepareCardsAndAccountsData} from "../../../constants/actionCreators";
import {connect} from "react-redux";
import CSCardsAndAccounts from "./CSCardsAndAccounts.component";

const mapStateToProps = state => ({
    isLoading: state.csCardsAndAccountsReducer.isLoading,
    cardsAndAccountsData: state.csCardsAndAccountsReducer.cardsAndAccountsData
});
const mapDispatchToProps = dispatch => ({
    prepareCardsAndAccountsData: (accountInfo) => {
        dispatch(prepareCardsAndAccountsData(accountInfo));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(CSCardsAndAccounts);
