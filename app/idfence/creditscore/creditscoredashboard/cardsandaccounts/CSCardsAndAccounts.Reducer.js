import {CREDIT_SCORE_ACTIONS} from "../../../constants/actions";
import {CreditScoreConstants} from "../../creditscoreutils/CreditScore.constants";
import {formatDateDD_MM_YYYY} from "../../../../commonUtil/Formatter";

const initialState = {
    cardsAndAccountsData: {
        cardData: [],
        activeAccInfo: {
            accountList: [],
            loanList: [],
        },
        inActiveAccInfo: {
            accountList: [],
            loanList: [],
        }
    },
    isLoading: true
}
const csCardsAndAccountsReducer = (state = initialState, action) => {
    switch (action.type) {
        case CREDIT_SCORE_ACTIONS.PREPARE_CARDS_AND_ACCOUNTS_DATA:
            let accountsSummaryInfo = action.data?.accountsSummaryInfo || CreditScoreConstants.defaultValues.JSON_OBJECT_DEFAULT;
            state.cardsAndAccountsData.cardData.length = 0;
            state.cardsAndAccountsData.cardData.push({
                title: accountsSummaryInfo?.activeAccounts || CreditScoreConstants.defaultValues.STRING_DEFAULT_3,
                label: "Active"
            });
            state.cardsAndAccountsData.cardData.push({
                title: accountsSummaryInfo?.inactiveAccounts || CreditScoreConstants.defaultValues.STRING_DEFAULT_3,
                label: "Inactive"
            });
            state.cardsAndAccountsData.cardData.push({
                title: accountsSummaryInfo?.creditCardAccounts || CreditScoreConstants.defaultValues.STRING_DEFAULT_3,
                label: "Credit Cards"
            });
            state.cardsAndAccountsData.cardData.push({
                title: accountsSummaryInfo?.loanAccounts || CreditScoreConstants.defaultValues.STRING_DEFAULT_3,
                label: "Loans"
            });


            // get all Credit Accounts
            let accountList = action.data?.accountList || CreditScoreConstants.defaultValues.JSON_ARRAY_DEFAULT;
            let accountItemList = accountList.map(item => {
                let accNo = item.accountNo || CreditScoreConstants.defaultValues.STRING_DEFAULT_2;
                if (accNo.length > 5) {
                    item.accountNo = (accNo.substr(accNo.length - 6)).toLowerCase();
                } else {
                    item.accountNo = "--";
                }
                return item;
            });
            // cards info
            let activeAccounts = filterActiveItems(accountItemList, CreditScoreConstants.accountTypeKeys.CREDIT_CARD);
            let inActiveAccounts = filterInActiveItems(accountItemList, CreditScoreConstants.accountTypeKeys.CREDIT_CARD);

            let activeAccItemList = prepareAccData(activeAccounts, CreditScoreConstants.accountTypeKeys.CREDIT_CARD);
            let inActiveAccItemList = prepareAccData(inActiveAccounts, CreditScoreConstants.accountTypeKeys.CREDIT_CARD);
            state.cardsAndAccountsData.activeAccInfo.accountList = activeAccItemList;
            state.cardsAndAccountsData.inActiveAccInfo.accountList = inActiveAccItemList;

            // loan info
            let activeLoans = filterActiveItems(accountItemList, CreditScoreConstants.accountTypeKeys.LOAN);
            let inActiveLoans = filterInActiveItems(accountItemList, CreditScoreConstants.accountTypeKeys.LOAN);

            let activeLoanItemList = prepareAccData(activeLoans, CreditScoreConstants.accountTypeKeys.LOAN);
            let inLoanAccItemList = prepareAccData(inActiveLoans, CreditScoreConstants.accountTypeKeys.LOAN);
            state.cardsAndAccountsData.activeAccInfo.loanList = activeLoanItemList;
            state.cardsAndAccountsData.inActiveAccInfo.loanList = inLoanAccItemList;
            return {
                ...state,
                cardsAndAccountsData: state.cardsAndAccountsData,
                isLoading: false
            };
        default:
            return {
                ...state
            };
    }

    function filterActiveItems(list, type) {
        return list.filter(function (item) {
            return (item?.accountType === type &&
                item?.accountStatus === 'ACTIVE');
        });
    }

    function filterInActiveItems(list, type) {
        return list.filter(function (item) {
            return (item?.accountType === type &&
                item?.accountStatus !== 'ACTIVE');
        });
    }

    function prepareAccData(itemList, type) {
        return itemList.map(element => {
            let item = {};
            let bankName = element?.bankName || CreditScoreConstants.defaultValues.STRING_DEFAULT_2;
            item.title = bankName.trim();
            let loanType = element?.loanType || CreditScoreConstants.defaultValues.STRING_DEFAULT_2;
            item.subTitle = (type === CreditScoreConstants.accountTypeKeys.LOAN ? loanType : element?.accountNo);
            item.centerTitle = "₹ " + element?.currentBalance || CreditScoreConstants.defaultValues.INT_DEFAULT;
            let dateOpened = element?.dateOpened || CreditScoreConstants.defaultValues.INT_DEFAULT;
            if (dateOpened !== 0) {
                item.rightTitle = formatDateDD_MM_YYYY(dateOpened);
            } else {
                item.rightTitle = "--";
            }
            return item;
        });
    }

}
export default csCardsAndAccountsReducer;
