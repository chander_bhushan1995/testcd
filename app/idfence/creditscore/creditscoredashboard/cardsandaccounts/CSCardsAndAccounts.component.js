import React from "react";
import {BackHandler, Platform, SafeAreaView, ScrollView, StatusBar, Text, View} from "react-native";
import {PLATFORM_OS} from "../../../../Constants/AppConstants";
import {TextStyle} from "../../../../Constants/CommonStyle";
import {OATextStyle} from "../../../../Constants/commonstyles";
import {HeaderBackButton} from "react-navigation";
import colors from "../../../../Constants/colors";
import OAActivityIndicator from "../../../../CustomComponent/OAActivityIndicator";
import styles from "./CSCardsAndAccounts.style";
import spacing from "../../../../Constants/Spacing";
import CardTypeThree from "../../../../CustomComponent/cardviews/cardtypethree";
import ScrollableTabView, {ScrollableTabBar} from "../../../../scrollableTabView/index";
import ActiveAccounts from "./tabs/ActiveAccounts.tab";
import dimens from "../../../../Constants/Dimens";

let navigator;
let ROW_HEIGHT = 250;
let DEFAULT_ROW_HEIGHT = 430;
export default class CSCardsAndAccounts extends React.Component {
    static navigationOptions = ({navigation}) => {
        navigator = navigation;
        return {
            headerStyle: {
                height: dimens.dimen80,
            },
            headerTitle: <View
                style={Platform.OS === PLATFORM_OS.ios ? {alignItems: "center"} : {alignItems: "flex-start"}}>
                <Text style={TextStyle.text_16_bold}>Cards and Accounts</Text>
                <Text style={[OATextStyle.TextFontSize_12_normal, {marginTop: spacing.spacing4}]}>Impact on credit
                    score: Low</Text>
            </View>,
            headerLeft: <HeaderBackButton tintColor={colors.black}
                                          onPress={() => {
                                              navigation.pop();
                                          }}/>,
            headerTitleStyle: {color: colors.white},
            headerTintColor: colors.white
        };
    };

    componentWillMount() {
        StatusBar.setBarStyle('default');
        if (Platform.OS === 'android') {
            StatusBar.setTranslucent(false);
            StatusBar.setBackgroundColor(colors.color_1468e2, true);
        }
        const {navigation} = this.props;
        let creditScoreCustInfo = navigation.getParam("creditScoreCustInfo", "");
        if (creditScoreCustInfo !== null) {
            this.props.prepareCardsAndAccountsData(creditScoreCustInfo?.accountInfo);
        } else {
            navigator.pop();
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }

    render() {
        if (this.props.isLoading) {
            return <OAActivityIndicator/>;
        }
        return (<SafeAreaView style={styles.safeAreaStyle}>
            <ScrollView style={styles.scrollViewStyle}>
                <View style={styles.cardTypeThreeContainerStyle}>
                    <CardTypeThree item={this.props.cardsAndAccountsData.cardData}/>
                </View>
                <Text style={styles.titleStyle}>Your Accounts</Text>
                {this.prepareTabViewForActiveInactiveAcc()}
            </ScrollView>
        </SafeAreaView>)
    }

    prepareTabViewForActiveInactiveAcc() {
        let activeHeight = this.props.cardsAndAccountsData.activeAccInfo.accountList.length + this.props.cardsAndAccountsData.activeAccInfo.loanList.length;
        let inActiveHeight = this.props.cardsAndAccountsData.inActiveAccInfo.accountList.length + this.props.cardsAndAccountsData.inActiveAccInfo.loanList.length;

        if (activeHeight > inActiveHeight) {
            DEFAULT_ROW_HEIGHT = (activeHeight * ROW_HEIGHT) + DEFAULT_ROW_HEIGHT;
        } else if (inActiveHeight > activeHeight) {
            DEFAULT_ROW_HEIGHT = (inActiveHeight * ROW_HEIGHT) + DEFAULT_ROW_HEIGHT;
        } else {
            DEFAULT_ROW_HEIGHT = (inActiveHeight * ROW_HEIGHT);
        }
        return (<View style={{
            flex: spacing.spacing1,
            height: DEFAULT_ROW_HEIGHT,
            alignItems: 'stretch'
        }}>
            <ScrollableTabView
                style={styles.scrollableTabViewStyle}
                initialPage={0}
                ref={(tabView) => {
                    if (tabView != null) {
                        this.tabView = tabView;
                    }
                }}
                renderTabBar={() => (
                    <ScrollableTabBar
                        tabsContainerStyle={styles.tabContainerView}
                        underlineStyle={styles.tabBarUnderlineView}
                        textStyle={styles.tabBarTextStyle}
                        activeTextColor={colors.blue028}
                        inactiveTextColor={colors.grey}
                    />
                )}
            >
                <ActiveAccounts tabLabel="Active"
                                item={{
                                    data: this.props.cardsAndAccountsData.activeAccInfo,
                                    errorMessage: 'You don’t have any active card or loan',
                                    tabType: 'ACTIVE'
                                }}
                                tabView={this.tabView}
                                membershipsData ={navigator.getParam('membershipsData', null)}
                />
                <ActiveAccounts tabLabel="Inactive"
                                item={{
                                    data: this.props.cardsAndAccountsData.inActiveAccInfo,
                                    errorMessage: 'You don’t have any inactive card or loan',
                                    tabType: 'INACTIVE'
                                }}
                                tabView={this.tabView}
                                membershipsData = {navigator.getParam('membershipsData', null)}
                />
            </ScrollableTabView>
        </View>);
    }

    componentWillUnmount() {
        StatusBar.setBarStyle('light-content');
        if (Platform.OS === 'android') {
            StatusBar.setTranslucent(true);
            StatusBar.setBackgroundColor('transparent', true);
        }
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    onBackPress = () => {
        if (navigator !== null) {
            navigator.pop();
        }
        return true;
    };
}
