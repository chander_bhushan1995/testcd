import {StyleSheet} from "react-native";
import spacing from "../../../../Constants/Spacing";
import colors from "../../../../Constants/colors";
import {OATextStyle} from "../../../../Constants/commonstyles";

export default StyleSheet.create({
    safeAreaStyle: {
        flex: spacing.spacing1,
        backgroundColor: colors.color_FFFFFF
    },
    flexOneStyle: {
        flex: spacing.spacing1
    },
    scrollableTabViewStyle: {
        marginTop: spacing.spacing0,
        flex: spacing.spacing1
    },
    csContactExperianCardStyle: {
        paddingHorizontal: spacing.spacing16,
        marginTop: spacing.spacing20
    },
    separatorViewStyle: {
        backgroundColor: colors.color_F1F3F4
    },
    titleStyle: {
        ...OATextStyle.TextFontSize_14_bold,
        color: colors.color_212121,
        paddingVertical: spacing.spacing12,
        paddingHorizontal: spacing.spacing16
    },
    cardTypeThreeContainerStyle: {
        padding: spacing.spacing16
    },
    scrollViewStyle: {
        flex: spacing.spacing1,
        flexGrow: spacing.spacing1
    }
});
