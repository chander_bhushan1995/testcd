import {Text, View} from "react-native";
import React from "react";
import CellTypeThree from "../../../../../CustomComponent/customcells/celltypethree";
import ErrorViewTypeOne from "../../../../../CustomComponent/errorviews";
import styles from "./CardsAndAccountsTabs.style";
import {CreditScoreConstants} from "../../../creditscoreutils/CreditScore.constants";
import CSContactExperianCard from "../../dashboardcontent/cscontactexperiancard";
import SeparatorView from "../../../../../CustomComponent/separatorview";
import CSExperianLogo from "../../dashboardcontent/creditscoreexperianlogo";
import {getUniqueRowID} from "../../../../constants/IDFence.UtilityMethods";

let membershipsDataObj = null
const ActiveAccounts = ({item,membershipsData}) => {
    prepareUI(item)
    membershipsDataObj = membershipsData
};

function prepareUI(item) {
    if (Array.isArray(item?.data?.accountList) && item?.data?.accountList.length === 0 &&
        Array.isArray(item?.data?.loanList) && item?.data?.loanList.length === 0) {
        return (<View>
            {errorView(item?.errorMessage)}
            {showFooterViews()}
        </View>);
    }

    return (<View style={[styles.creditCardTitleStyle, styles.flexOneStyle]}>
        {rowViews(item?.data?.accountList, item?.tabType, CreditScoreConstants.textLabels.DATATYPE_CARDS, "Credit Card(s)")}
        <View style={styles.loanTitleStyle}>
            {rowViews(item?.data?.loanList, item?.tabType, CreditScoreConstants.textLabels.DATATYPE_LOANS, "Loan(s)")}
        </View>
        {showFooterViews()}
    </View>);
}

function showFooterViews() {
    return (<View>
        {<CSContactExperianCard
            webEngageLocation={CreditScoreConstants.dashboardCardsTemplate.totalNoOfCreditCardsAndLoansCard.placeHolderOne}
            customStyle={{containerStyle: styles.csContactExperianCardStyle}}
            item={CreditScoreConstants.dashboardCardsTemplate.contactExperianCard}
            membershipData ={membershipsDataObj}
        />}
        <SeparatorView separatorStyle={styles.separatorViewStyle}/>
        {<CSExperianLogo
            item={CreditScoreConstants.dashboardCardsTemplate.experianLogoFooterCard}/>}
    </View>)
}

function rowViews(accountList, tabType, dataType, title) {

    if (accountList.length === 0) {
        let errorMessage;
        if (tabType === 'ACTIVE') {
            errorMessage = (dataType === CreditScoreConstants.textLabels.DATATYPE_CARDS ? 'You don’t have any active card.' : 'You don’t have any active loan.');
        } else {
            errorMessage = (dataType === CreditScoreConstants.textLabels.DATATYPE_CARDS ? 'You don’t have any inactive card.' : 'You don’t have any inactive loan.');
        }
        return (<View>
            <Text style={styles.headerTitleStyle}>{title}</Text>
            {errorView(errorMessage)}
        </View>);
    }
    let rowViewList = accountList.map((item, index) => {

        return (<View key={getUniqueRowID()}>
            <CellTypeThree rowHeight={100}  item={item}/>
        </View>);
    });
    let rootView = <View>
        <Text style={styles.headerTitleStyle}>{title} ({accountList.length})</Text>
        <View style={styles.columnContainerStyle}>
            <View style={[styles.flexOneStyle, styles.columnLTitleStyle]}>
                <Text>BANK</Text>
            </View>
            <View style={[styles.flexOneStyle, styles.columnMTitleStyle]}>
                <Text>BALANCE</Text>
            </View>

            <View style={[styles.flexOneStyle, styles.columnRTitleStyle]}>
                <Text>AS ON</Text>
            </View>
        </View>
        {rowViewList}
    </View>;
    return rootView;
}

function errorView(errorMessage) {
    return (<View style={styles.errorViewStyle}>
        {<ErrorViewTypeOne item={{message: errorMessage}}/>}
    </View>);
}

export default ActiveAccounts;
