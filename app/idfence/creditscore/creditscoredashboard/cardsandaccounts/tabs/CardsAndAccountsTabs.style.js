import {StyleSheet} from "react-native";
import spacing from "../../../../../Constants/Spacing";
import colors from "../../../../../Constants/colors";
import {OATextStyle} from "../../../../../Constants/commonstyles";

export default StyleSheet.create({
    safeAreaStyle: {
        flex: spacing.spacing1,
        backgroundColor: colors.color_FFFFFF
    },
    flexOneStyle: {
        flex: spacing.spacing1,
    },
    errorViewStyle: {
        marginTop:-15,
        paddingHorizontal: spacing.spacing16
    },
    columnLTitleStyle: {
        alignItems: 'flex-start'
    },
    columnRTitleStyle: {
        alignItems: 'flex-end'
    },
    columnMTitleStyle: {
        alignItems: 'center'
    },
    csContactExperianCardStyle: {
        paddingHorizontal: spacing.spacing16,
        marginTop: spacing.spacing20
    },
    separatorViewStyle: {
        backgroundColor: colors.color_F1F3F4
    },
    columnContainerStyle: {
        marginTop: spacing.spacing10,
        paddingHorizontal: spacing.spacing16,
        flexDirection: 'row'
    },
    headerTitleStyle: {
        ...OATextStyle.TextFontSize_14_bold,
        color: colors.color_212121,
        paddingVertical: spacing.spacing12,
        paddingHorizontal: spacing.spacing16,
    },
    loanTitleStyle: {
        marginTop: spacing.spacing28
    },
    creditCardTitleStyle: {
        paddingVertical: spacing.spacing16
    }
});
