import {Text, View} from "react-native";
import React from "react";
import ErrorViewTypeOne from "../../../../../CustomComponent/errorviews";
import {OATextStyle} from "../../../../../Constants/commonstyles";
import colors from "../../../../../Constants/colors";
import CellTypeFour from "../../../../../CustomComponent/customcells/celltypefour";

const InActiveAccounts = () => (
    <View style={{flex: 1, paddingHorizontal: 16, paddingVertical: 16}}>
        <Text style={[OATextStyle.TextFontSize_14_bold, {
            color: colors.color_212121,
            paddingVertical: 12
        }]}>Loans (1)</Text>
        <CellTypeFour/>
        <CellTypeFour/>
        <View style={{marginTop: 28}}>
            <Text style={[OATextStyle.TextFontSize_14_bold, {
                color: colors.color_212121,
                paddingVertical: 12,
                paddingHorizontal: 16
            }]}>Loans (1)</Text>
            <CellTypeFour/>
            <CellTypeFour/>
        </View>
    </View>
);

function errorView() {
    return (<ErrorViewTypeOne item={{message: 'You don’t have any inactive card or loan'}}/>)
}

export default InActiveAccounts;
