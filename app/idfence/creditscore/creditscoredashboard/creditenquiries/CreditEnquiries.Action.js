import {prepareCreditEnquiriesData} from "../../../constants/actionCreators";
import {connect} from "react-redux";
import CreditEnquiries from "./CreditEnquiries.component";

const mapStateToProps = state => ({
    isLoading: state.csCreditEnquiriesReducer.isLoading,
    creditEnquiriesList: state.csCreditEnquiriesReducer.creditEnquiriesList
});
const mapDispatchToProps = dispatch => ({
    prepareCreditEnquiriesData: (accountInfo) => {
        dispatch(prepareCreditEnquiriesData(accountInfo));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(CreditEnquiries);
