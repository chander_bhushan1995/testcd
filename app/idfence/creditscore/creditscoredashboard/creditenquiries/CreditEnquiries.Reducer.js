import {CREDIT_SCORE_ACTIONS} from "../../../constants/actions";
import {CreditScoreConstants} from "../../creditscoreutils/CreditScore.constants";
import {formatCreditEnquiresDate, formatDateDD_MM_YYYY, formatDateIn_DDMMyyyy} from "../../../../commonUtil/Formatter";

const initialState = {
    creditEnquiriesList: [],
    isLoading: true
}
const csCreditEnquiriesReducer = (state = initialState, action) => {
    switch (action.type) {
        case CREDIT_SCORE_ACTIONS.PREPARE_CREDIT_ENQUIRIES_DATA:
            let enquirerList = action.data?.enquirerList || CreditScoreConstants.defaultValues.JSON_ARRAY_DEFAULT;
            let itemList = enquirerList.map(element => {
                let amountFinanced = element?.amountFinanced || CreditScoreConstants.defaultValues.INT_DEFAULT;
                let dateOfRequest = element?.dateOfRequest || CreditScoreConstants.defaultValues.INT_DEFAULT;
                let dateOfReq = "--";
                if (dateOfRequest !== 0) {
                    let billingDate = new Date(dateOfRequest);
                    let month = String(billingDate.getMonth() + 1);
                    let day = String(billingDate.getDate());
                    let year = String(billingDate.getFullYear());
                    dateOfReq = formatCreditEnquiresDate(day + '/' + month + '/' + year);
                }
                let item = [
                    {
                        title: "Bank",
                        value: (element?.subscriberName || CreditScoreConstants.defaultValues.STRING_DEFAULT_2)
                    },
                    {
                        title: "Date of enquiry",
                        value: dateOfReq
                    },
                    {
                        title: "Type",
                        value: (element?.enquiryReason || CreditScoreConstants.defaultValues.STRING_DEFAULT_2)
                    },
                    {
                        title: "Amount",
                        value: "₹ " + amountFinanced
                    }
                ]
                return item;
            });
            return {
                ...state,
                creditEnquiriesList: itemList,
                isLoading: false
            };
        default:
            return {
                ...state
            };
    }
}
export default csCreditEnquiriesReducer;
