import React from "react";
import {BackHandler, Platform, SafeAreaView, ScrollView, StatusBar, Text, View} from "react-native";
import {PLATFORM_OS} from "../../../../Constants/AppConstants";
import {TextStyle} from "../../../../Constants/CommonStyle";
import {OATextStyle} from "../../../../Constants/commonstyles";
import {HeaderBackButton} from "react-navigation";
import colors from "../../../../Constants/colors";
import OAActivityIndicator from "../../../../CustomComponent/OAActivityIndicator";
import styles from "./CreditEnquiries.style";
import CardTypeFour from "../../../../CustomComponent/cardviews/cardtypefour";
import CSExperianLogo from "../dashboardcontent/creditscoreexperianlogo";
import {CreditScoreConstants, EMPTY_ERROR_MESSAGES} from "../../creditscoreutils/CreditScore.constants";
import SeparatorView from "../../../../CustomComponent/separatorview";
import ErrorViewTypeOne from "../../../../CustomComponent/errorviews";
import dimens from "../../../../Constants/Dimens";
import spacing from "../../../../Constants/Spacing";
import {getUniqueRowID} from "../../../constants/IDFence.UtilityMethods";

let navigator;
export default class CreditEnquiries extends React.Component {
    static navigationOptions = ({navigation}) => {
        navigator = navigation;
        return {
            headerStyle: {
                height:dimens.dimen80,
            },
            headerTitle: <View
                style={Platform.OS === PLATFORM_OS.ios ? {alignItems: "center"} : {alignItems: "flex-start"}}>
                <Text style={TextStyle.text_16_bold}>Credit Enquiries</Text>
                <Text style={[OATextStyle.TextFontSize_12_normal,{marginTop:spacing.spacing4}]}>Impact on credit score: Low</Text>
            </View>,
            headerLeft: <HeaderBackButton tintColor={colors.black}
                                          onPress={() => {
                                              navigation.pop();
                                          }}/>,
            headerTitleStyle: {color: colors.white},
            headerTintColor: colors.white
        };
    };

    componentWillMount() {
        StatusBar.setBarStyle('default');
        if (Platform.OS === 'android') {
            StatusBar.setTranslucent(false);
            StatusBar.setBackgroundColor(colors.color_1468e2, true);
        }
        const {navigation} = this.props;
        let creditScoreCustInfo = navigation.getParam("creditScoreCustInfo", "");
        if (creditScoreCustInfo !== null) {
            this.props.prepareCreditEnquiriesData(creditScoreCustInfo?.enquiryInfo);
        } else {
            navigator.pop();
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }

    render() {
        if (this.props.isLoading) {
            return <OAActivityIndicator/>;
        }
        return (<SafeAreaView style={styles.safeAreaStyle}>
            <View >

            </View>
            <ScrollView style={styles.flexOneStyle}>
                <View style={styles.scrollViewViewContainerStyle}>
                    {this.showEnquiriesCards()}
                </View>
                <SeparatorView separatorStyle={{backgroundColor: colors.color_F1F3F4}}/>
                {<CSExperianLogo
                    item={CreditScoreConstants.dashboardCardsTemplate.experianLogoFooterCard}/>}
            </ScrollView>
        </SafeAreaView>)
    }

    showEnquiriesCards() {
        if (Array.isArray(this.props.creditEnquiriesList) && this.props.creditEnquiriesList.length) {
            return this.props.creditEnquiriesList.map((item, index) => {
                let itemValue=item.filter(function(element){
                    return element.title === "Bank";
                });
                let noteText="Note: if you didn’t apply for this loan/credit card please contact the "+itemValue[0]?.value;
                return <CardTypeFour key={getUniqueRowID()} headerPrefix={'Enquiry'} item={item} index={index}
                                     noteText={noteText}/>
            });
        }
        return this.errorView();

    }

    errorView() {
        return (<View style={styles.errorViewStyle}>
            {<ErrorViewTypeOne item={{message: EMPTY_ERROR_MESSAGES.CREDIT_ENQUIRIES}}/>}
        </View>);
    }

    componentWillUnmount() {
        StatusBar.setBarStyle('light-content');
        if (Platform.OS === 'android') {
            StatusBar.setTranslucent(true);
            StatusBar.setBackgroundColor('transparent', true);
        }
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    onBackPress = () => {
        if (navigator !== null) {
            navigator.pop();
        }
        return true;
    };
}
