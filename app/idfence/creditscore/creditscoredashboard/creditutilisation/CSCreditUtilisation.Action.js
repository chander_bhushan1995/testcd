import {connect} from "react-redux";
import CSCreditUtilisation from "./CSCreditUtilisation.component";
import {prepareCreditUtilisationData} from "../../../constants/actionCreators";


const mapStateToProps = state => ({
    isLoading: state.csCreditUtilisationReducer.isLoading,
    creditUtilisationInfo: state.csCreditUtilisationReducer.creditUtilisationInfo,
    badgeData: state.csCreditUtilisationReducer.badgeData,
});
const mapDispatchToProps = dispatch => ({
    prepareCreditUtilisationCardData: (creditUtilization) => {
        dispatch(prepareCreditUtilisationData(creditUtilization));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(CSCreditUtilisation);
