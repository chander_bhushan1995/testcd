import {CREDIT_SCORE_ACTIONS} from "../../../constants/actions";
import {creditScoreBadgeViewTypeOne} from "../../../constants/Constants";
import {CreditScoreConstants} from "../../creditscoreutils/CreditScore.constants";
import {formatDateDD_MM_YYYY} from "../../../../commonUtil/Formatter";
import {
    formatNumberToPercentage,
    formatNumberToPercentageNoSpace,
    isNumeric
} from "../../../constants/IDFence.UtilityMethods";

const initialState = {
    creditUtilisationInfo: {
        cardData: [],
        cardsInfo: []
    },
    isLoading: true,
    badgeData: {}
}
const csCreditUtilisationReducer = (state = initialState, action) => {
    switch (action.type) {
        case CREDIT_SCORE_ACTIONS.PREPARE_CREDIT_UTILISATION_DATA:
            let creditUtilization = action.data?.creditUtilization;
            let creditLimitUtilization = formatNumberToPercentage(creditUtilization?.creditLimitUtilization || CreditScoreConstants.defaultValues.STRING_DEFAULT_2);

            let creditUtlizationLevel = creditUtilization?.creditUtlizationLevel;

            let badgeData = {
                label: creditUtlizationLevel,
                isShowBadge: (creditUtlizationLevel !== "NA" && creditUtlizationLevel !== "N/A") ? true : false,
                data: creditScoreBadgeViewTypeOne[creditUtlizationLevel]
            };
            let currentBalance = creditUtilization?.currentBalance;
            if (isNumeric(currentBalance)) {
                if (currentBalance === '.00' || currentBalance === '0.0') {
                    currentBalance = CreditScoreConstants.defaultValues.INT_DEFAULT;
                }
            } else {
                currentBalance = CreditScoreConstants.defaultValues.STRING_DEFAULT_2;
            }

            let creditLimit = creditUtilization?.creditLimit;
            if (!isNumeric(creditLimit)) {
                creditLimit = CreditScoreConstants.defaultValues.STRING_DEFAULT_2;
            }

            let creditUtilisationCardData = [
                [currentBalance, creditLimit, creditLimitUtilization],
                ["Total credit balance", "Total Credit limit", "% of utilisation"]
            ];
            state.creditUtilisationInfo.cardData = creditUtilisationCardData;

            // get all credit cards
            let accountList = action.data?.accountList || CreditScoreConstants.defaultValues.JSON_ARRAY_DEFAULT;
            // cards info
            let cardItemList = accountList.filter(function (item) {
                return item?.accountType === CreditScoreConstants.accountTypeKeys.CREDIT_CARD && item?.accountStatus === 'ACTIVE';
            });

            let accountItemList = cardItemList.map(item => {
                let rowItem = {};
                let creditLimitAmount = item?.creditLimitAmount || CreditScoreConstants.defaultValues.INT_DEFAULT;
                let currentBalance = item?.currentBalance || CreditScoreConstants.defaultValues.INT_DEFAULT;
                let dateReported = item?.dateReported || CreditScoreConstants.defaultValues.STRING_DEFAULT_2
                let creditUtilizationPercentage = formatNumberToPercentageNoSpace(item?.creditUtilizationPercentage || CreditScoreConstants.defaultValues.INT_DEFAULT)
                if (dateReported !== 0) {
                    dateReported = formatDateDD_MM_YYYY(dateReported);
                }


                rowItem.title = item.bankName || CreditScoreConstants.defaultValues.STRING_DEFAULT_2;
                let accNo = item.accountNo || CreditScoreConstants.defaultValues.STRING_DEFAULT_2;
                if (accNo.length > 5) {
                    accNo = (accNo.substr(accNo.length - 6)).toLowerCase();
                } else {
                    accNo = "--";
                }
                rowItem.subTitle = accNo;
                rowItem.topRTitle = (creditUtilizationPercentage !== '--' ? creditUtilizationPercentage + "%" : creditPercentage);
                rowItem.progressValue = (creditUtilizationPercentage === '--' ? 0 : creditUtilizationPercentage);
                rowItem.bottomLTitle = "Credit Balance: ₹" + currentBalance + " | By " + dateReported;
                rowItem.bottomRTitle = "₹" + creditLimitAmount;
                return rowItem;
            });

            state.creditUtilisationInfo.cardsInfo = accountItemList;
            return {
                ...state,
                creditUtilisationInfo: state.creditUtilisationInfo,
                badgeData: badgeData,
                isLoading: false
            };
        default:
            return {
                ...state
            };
    }
}
export default csCreditUtilisationReducer;
