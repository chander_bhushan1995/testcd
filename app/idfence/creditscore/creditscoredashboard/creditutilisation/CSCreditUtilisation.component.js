import React from "react";
import {BackHandler, NativeModules, Platform, SafeAreaView, ScrollView, StatusBar, Text, View} from "react-native";
import {PLATFORM_OS} from "../../../../Constants/AppConstants";
import {TextStyle} from "../../../../Constants/CommonStyle";
import {OATextStyle} from "../../../../Constants/commonstyles";
import {HeaderBackButton} from "react-navigation";
import colors from "../../../../Constants/colors";
import styles from "./CSCreditUtilisation.style";
import spacing from "../../../../Constants/Spacing";
import CardTypeOne from "../../../../CustomComponent/cardviews/cardtypeone";
import OAActivityIndicator from "../../../../CustomComponent/OAActivityIndicator";
import CreditCardWiseStatus from "./creditcardwisestatus";
import ProTipTypeOne from "../../../../CustomComponent/protipviews";
import CSContactExperianCard from "../dashboardcontent/cscontactexperiancard";
import {
    CreditScoreConstants,
    CS_ACTION_TYPES,
    EMPTY_ERROR_MESSAGES
} from "../../creditscoreutils/CreditScore.constants";
import SeparatorView from "../../../../CustomComponent/separatorview";
import ClickableCell from "../../../../CustomComponent/customcells/clickableCell/ClickableCell";
import CSExperianLogo from "../dashboardcontent/creditscoreexperianlogo";
import {getIDFenceFQAUrl} from "../../../constants/Urls";
import {DetailsTabStrings} from "../../../constants/Constants";
import CreditScoreInfoBottomSheet from "../CreditScoreInfoBottomSheet.component";
import ErrorViewTypeOne from "../../../../CustomComponent/errorviews";
import dimens from "../../../../Constants/Dimens";
import {WEBENGAGE_CREDIT_SCORE} from "../../../constants/CreditScoreWebEngage.events";
import {logWebEnageEvent} from "../../../../commonUtil/WebengageTrackingUtils";
import {getIDFCommonEventAttr} from '../../../constants/IDFence.UtilityMethods';
import { parseJSON, deepCopy} from "../../../../commonUtil/AppUtils";

const nativeBridgeRef = NativeModules.ChatBridge;
let navigator;
let creditUtilizationMapList = [];
let cscoreInfoBottomSheetRef = React.createRef();
export default class CSCreditUtilisation extends React.Component {
    static navigationOptions = ({navigation}) => {
        navigator = navigation;
        return {
            headerStyle: {
                height: dimens.dimen80,
            },
            headerTitle: <View
                style={Platform.OS === PLATFORM_OS.ios ? {alignItems: "center"} : {alignItems: "flex-start"}}>
                <Text style={TextStyle.text_16_bold}>Credit Utilisation</Text>
                <Text style={[OATextStyle.TextFontSize_12_normal, {marginTop: spacing.spacing4}]}>Impact on credit
                    score: High</Text>
            </View>,
            headerLeft: <HeaderBackButton tintColor={colors.black}
                                          onPress={() => {
                                              navigation.pop();
                                          }}/>,
            headerTitleStyle: {color: colors.white},
            headerTintColor: colors.white
        };
    };

    componentWillMount() {
        StatusBar.setBarStyle('default');
        if (Platform.OS === 'android') {
            StatusBar.setTranslucent(false);
            StatusBar.setBackgroundColor(colors.color_1468e2, true);
        }
        const {navigation} = this.props;
        let creditScoreCustInfo = navigation.getParam("creditScoreCustInfo", "");
        creditUtilizationMapList = navigation.getParam("scoreInfo", "")?.creditUtilizationMapList;
        if (creditScoreCustInfo !== null) {
            /*let creditLimitUtilization = formatNumberToPercentage(creditScoreCustInfo?.accountInfo?.creditUtilization?.creditLimitUtilizationDifference || CreditScoreConstants.defaultValues.STRING_DEFAULT_2);
            this.logKnowMoreEvent(creditLimitUtilization+" "+CreditScoreConstants.dashboardCardsTemplate.creditLimitUtilizationCard.placeHolderOne);*/
            this.props.prepareCreditUtilisationCardData(creditScoreCustInfo?.accountInfo);
        } else {
            navigator.pop();
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }

    render() {
        if (this.props.isLoading) {
            return <OAActivityIndicator/>;
        }
        return (<SafeAreaView style={styles.safeAreaStyle}>
            <ScrollView style={styles.flexOneStyle}>
                <View style={{padding: spacing.spacing16}}>
                    <CardTypeOne item={this.props.creditUtilisationInfo.cardData}
                                 onBadgeViewPress={(e) => this.onAction(e, {actionType: CS_ACTION_TYPES.BADGE_VIEW_PRESS})}
                                 badgeData={this.props.badgeData}/>
                </View>
                <View style={{paddingVertical: 16}}>
                    {this.showCreditUtilisationData()}
                    <ProTipTypeOne item={CreditScoreConstants.proTips.CREDIT_UTILIZATION}
                                   customStyle={{containerStyle: {marginHorizontal: 16}}}/>
                    {<CSContactExperianCard
                        webEngageLocation={CreditScoreConstants.dashboardCardsTemplate.creditLimitUtilizationCard.placeHolderOne}
                        customStyle={{containerStyle: {paddingHorizontal: 16, marginTop: 20}}}
                        item={CreditScoreConstants.dashboardCardsTemplate.contactExperianCard}
                        membershipData ={this.props.navigation.getParam("membershipsData", null)}
                    />}
                </View>
                <SeparatorView separatorStyle={{backgroundColor: colors.color_F1F3F4}}/>
                {<ClickableCell item={CreditScoreConstants.dashboardCardsTemplate.faqsRow}
                                titleStyle={{color: colors.charcoalGrey}}
                                onPress={(e) => this.onAction(e, {actionType: CS_ACTION_TYPES.FAQ_PRESS})}
                                subTitleStyle={{color: colors.color_CF021B}}
                />}
                {<CSExperianLogo
                    item={CreditScoreConstants.dashboardCardsTemplate.experianLogoFooterCard}/>}

            </ScrollView>
            <CreditScoreInfoBottomSheet ref={cscoreInfoBottomSheetRef}/>
        </SafeAreaView>);
    }

    showCreditUtilisationData() {
        if (Array.isArray(this.props.creditUtilisationInfo.cardsInfo) && this.props.creditUtilisationInfo.cardsInfo.length) {
            return (<CreditCardWiseStatus item={{
                data: this.props.creditUtilisationInfo.cardsInfo,
                headerLabel: "Credit Card wise status"
            }}
                                          customStyle={{
                                              containerStyle: {paddingVertical: 16},
                                              headerStyle: {paddingHorizontal: 16}
                                          }}/>);
        }
        return this.errorView();
    }

    errorView() {
        return (<View style={styles.errorViewStyle}>
            {<ErrorViewTypeOne item={{message: EMPTY_ERROR_MESSAGES.CREDIT_UTILISATION}}/>}
        </View>);
    }

    onAction(e, item) {
        if (e !== undefined) {
            e.stopPropagation();
        }
        if (item.actionType === CS_ACTION_TYPES.FAQ_PRESS) {
            let webEngageEvents = getIDFCommonEventAttr(this.props.navigation.getParam("membershipsData", null));
            webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.LOCATION] = WEBENGAGE_CREDIT_SCORE.ATTR_VALUE.CREDIT_UTILIZATION;
            logWebEnageEvent(WEBENGAGE_CREDIT_SCORE.EVENT_NAME.CREDIT_SCORE_FAQ, webEngageEvents);
            nativeBridgeRef.getApiProperty(apiProperty => {
                apiProperty = parseJSON(apiProperty);
                let tncUrlObj = getIDFenceFQAUrl(apiProperty.api_base_url);
                navigator.navigate('TnCWebViewComponent', {
                    productTermAndCUrl: tncUrlObj,
                    enableNativeBack: false,
                    otherParam: DetailsTabStrings.titleFAQs
                });
            });
        } else if (item.actionType === CS_ACTION_TYPES.BADGE_VIEW_PRESS) {
            if (cscoreInfoBottomSheetRef.current) {
                cscoreInfoBottomSheetRef.current.setBottomSheetData(creditUtilizationMapList);
                cscoreInfoBottomSheetRef.current.setBottomSheetVisible(true);
                let webEngageEvents = getIDFCommonEventAttr(this.props.navigation.getParam("membershipsData", null));
                webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.LOCATION] = WEBENGAGE_CREDIT_SCORE.ATTR_VALUE.CREDIT_UTILIZATION;
                logWebEnageEvent(WEBENGAGE_CREDIT_SCORE.EVENT_NAME.SCORE_INFO, webEngageEvents);
            }
        }
    }

    componentWillUnmount() {
        StatusBar.setBarStyle('light-content');
        if (Platform.OS === 'android') {
            StatusBar.setTranslucent(true);
            StatusBar.setBackgroundColor('transparent', true);
        }
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    logKnowMoreEvent = (impactItem) => {
        let webEngageEvents = getIDFCommonEventAttr(this.props.navigation.getParam("membershipsData", null));
        webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.IMPACT_ITEMS] = impactItem;
        logWebEnageEvent(WEBENGAGE_CREDIT_SCORE.EVENT_NAME.CREDIT_SCORE_KNOW_MORE, webEngageEvents);
    }
    onBackPress = () => {
        if (navigator !== null) {
            navigator.pop();
        }
        return true;
    };

}
