import {StyleSheet} from "react-native";
import spacing from "../../../../Constants/Spacing";
import colors from "../../../../Constants/colors";
import dimens from "../../../../Constants/Dimens";

export default StyleSheet.create({
    safeAreaStyle: {
        flex: spacing.spacing1,
        backgroundColor: colors.color_FFFFFF
    },
    flexOneStyle: {
        flex: spacing.spacing1
    },
    errorViewStyle: {
        height: dimens.dimen200,
        paddingHorizontal: spacing.spacing16
    }
});
