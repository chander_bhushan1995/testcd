import React from "react";
import {Text, View} from "react-native";
import styles from "./CreditCardWiseStatus.style";
import {OATextStyle} from "../../../../../Constants/commonstyles";
import colors from "../../../../../Constants/colors";
import CellTypeTwo from "../../../../../CustomComponent/customcells/celltypetwo/CellTypeTwo.component";
import {getUniqueRowID} from "../../../../constants/IDFence.UtilityMethods";

const CreditCardWiseStatus = ({item, customStyle}) => (
    <View style={[styles.containerStyle, customStyle?.containerStyle]}>
        <Text
            style={[OATextStyle.TextFontSize_12_bold_212121, {
                color: colors.color_888F97,
                textTransform: 'uppercase',
                display: (item?.headerLabel === undefined ? "none" : "flex")
            }, customStyle?.headerStyle]}>{item?.headerLabel}</Text>
        <View style={{marginTop: 10, backgroundColor: '#ccee00', alignSelf: 'stretch'}}>
            {prepareUIForRow(item?.data)}
        </View>
    </View>
);

function prepareUIForRow(accountList) {
    return accountList.map((item, index) => {
        return (<CellTypeTwo key={getUniqueRowID()} item={item}/>);
    });
}

export default CreditCardWiseStatus;
