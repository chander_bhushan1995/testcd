import {StyleSheet} from "react-native";
import colors from "../../../../../Constants/colors";

export default StyleSheet.create({
    containerStyle: {
        alignItems: 'flex-start',
        backgroundColor: colors.color_FFFFFF
    }
});
