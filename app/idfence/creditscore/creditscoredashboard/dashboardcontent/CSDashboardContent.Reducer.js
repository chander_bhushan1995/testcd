import {CreditScoreConstants} from "../../creditscoreutils/CreditScore.constants";
import {
    formatNumberToPercentage,
    formatNumberToPercentageNoSpace,
    isNumeric,
    sortAlphabetDigits
} from "../../../constants/IDFence.UtilityMethods";
import {OAImageSource} from "../../../../Constants/OAImageSource";

export const PREPARE_DEFAULT_DATA = "PREPARE_DEFAULT_DATA";
export default function csDashboardContentReducer(state, action) {
    if (action.type === PREPARE_DEFAULT_DATA) {
        let creditScoreCustInfo = state.data?.creditScoreCustInfo;
        let paymentOnTimeData = CreditScoreConstants.dashboardCardsTemplate.paymentsDoneOnTimeCard;
        let paymentInfo = creditScoreCustInfo?.accountInfo?.paymentInfo || CreditScoreConstants.defaultValues.JSON_OBJECT_DEFAULT;
        let status = formatNumberToPercentage(paymentInfo?.ontimePaymentsPercentage || CreditScoreConstants.defaultValues.INT_DEFAULT);
        paymentOnTimeData["status"] = status;

        let ontimePaymentsPercentageDifference = formatNumberToPercentageNoSpace(paymentInfo?.ontimePaymentsPercentageDifference || CreditScoreConstants.defaultValues.INT_DEFAULT);
        if (ontimePaymentsPercentageDifference !== 0) {
            let imgUrl = OAImageSource.score_up_indicator;
            let scoreTitle = 'Up by ' + ontimePaymentsPercentageDifference + '%';
            if (ontimePaymentsPercentageDifference < 0) {
                imgUrl = OAImageSource.score_down_indicator;
                scoreTitle = 'Down by ' + (ontimePaymentsPercentageDifference * -1) + '%';
            }
            paymentOnTimeData["percentageDifference"] = {
                "img_source": imgUrl,
                "scoreTitle": scoreTitle
            }
            ;
        }


        let creditUtilizationData = CreditScoreConstants.dashboardCardsTemplate.creditLimitUtilizationCard;
        let creditUtilization = creditScoreCustInfo?.accountInfo?.creditUtilization || CreditScoreConstants.defaultValues.JSON_OBJECT_DEFAULT;

        status = formatNumberToPercentage(creditUtilization?.creditLimitUtilization || CreditScoreConstants.defaultValues.INT_DEFAULT);
        creditUtilizationData["status"] = status;

        let creditLimitUtilizationDifference = formatNumberToPercentageNoSpace(creditUtilization?.creditLimitUtilizationDifference || CreditScoreConstants.defaultValues.INT_DEFAULT);
        if (creditLimitUtilizationDifference !== 0) {
            let imgUrl = OAImageSource.score_up_indicator;
            let scoreTitle = 'Up by ' + creditLimitUtilizationDifference + '%';
            if (creditLimitUtilizationDifference < 0) {
                imgUrl = OAImageSource.score_down_indicator;
                scoreTitle = 'Down by ' + (creditLimitUtilizationDifference * -1) + '%';
            }
            creditUtilizationData["percentageDifference"] = {
                "img_source": imgUrl,
                "scoreTitle": scoreTitle
            }
            ;
        }


        let oldestOpenAccountData = CreditScoreConstants.dashboardCardsTemplate.ageOfYourOldestOpenAccountCard;
        let oldestActiveAccount = creditScoreCustInfo?.accountInfo?.oldestActiveAccount;
        let year = oldestActiveAccount?.oldestActiveAccountYears;
        let month = oldestActiveAccount?.oldestActiveAccountMonths;
        status = "--";
        if (isNumeric(year)) {
            status = `${year}y`;
        }
        if (isNumeric(month)) {
            if (status !== "--") {
                status = `${status}, `
            }
            status = `${status}${month}m`;
        }
        oldestOpenAccountData["status"] = status;

        let totalNoOfCreditCardsAndLoansData = CreditScoreConstants.dashboardCardsTemplate.totalNoOfCreditCardsAndLoansCard;
        let accountsSummaryInfo = creditScoreCustInfo?.accountInfo?.accountsSummaryInfo;
        status = accountsSummaryInfo?.creditAndLoanAccounts;
        if (!isNumeric(status)) {
            status = "--";
        }
        totalNoOfCreditCardsAndLoansData["status"] = status;
        let creditAndLoanAccountsDifference = formatNumberToPercentageNoSpace(accountsSummaryInfo?.creditAndLoanAccountsDifference || CreditScoreConstants.defaultValues.INT_DEFAULT);
        if (creditAndLoanAccountsDifference !== 0) {
            let imgUrl = OAImageSource.score_up_indicator;
            let scoreTitle = 'Up by ' + creditAndLoanAccountsDifference + '%';
            if (creditAndLoanAccountsDifference < 0) {
                imgUrl = OAImageSource.score_down_indicator;
                scoreTitle = 'Down by ' + (creditAndLoanAccountsDifference* -1) + '%';
            }
            totalNoOfCreditCardsAndLoansData["percentageDifference"] = {
                "img_source": imgUrl,
                "scoreTitle": scoreTitle
            };
        }

        let enquiryInfoData = CreditScoreConstants.dashboardCardsTemplate.creditEnquiriesCard;
        let creditEnquiriesCard = creditScoreCustInfo?.enquiryInfo;
        status = creditEnquiriesCard?.totalCreditEnquiries;
        if (!isNumeric(status)) {
            status = "--";
        }
        enquiryInfoData["status"] = status;

        let totalCreditEnquiriesDifference = formatNumberToPercentageNoSpace(creditEnquiriesCard?.totalCreditEnquiriesDifference || CreditScoreConstants.defaultValues.INT_DEFAULT);
        if (totalCreditEnquiriesDifference !== 0) {
            let imgUrl = OAImageSource.score_up_indicator;
            let scoreTitle = 'Up by ' + totalCreditEnquiriesDifference ;
            if (totalCreditEnquiriesDifference < 0) {
                imgUrl = OAImageSource.score_down_indicator;
                scoreTitle = 'Down by ' + (totalCreditEnquiriesDifference* -1) ;
            }
            enquiryInfoData["percentageDifference"] = {
                "img_source": imgUrl,
                "scoreTitle": scoreTitle
            };
        }
        let impactInfo = creditScoreCustInfo?.accountInfo?.impactInfo;
        if (impactInfo !== null && impactInfo !== undefined) {
            let ontimePaymentsImpactLevel = impactInfo?.ontimePaymentsImpactLevel;
            let creditLimitUtilizationImpactLevel = impactInfo?.creditLimitUtilizationImpactLevel;
            let oldestActiveAccountImpactLevel = impactInfo?.oldestActiveAccountImpactLevel;
            let totalCreditAndLoanAccountsImpactLevel = impactInfo?.totalCreditAndLoanAccountsImpactLevel;
            let totalCreditEnquiriesImpactLevel = impactInfo?.totalCreditEnquiriesImpactLevel;
            paymentOnTimeData["impact"] = ((ontimePaymentsImpactLevel !== null && ontimePaymentsImpactLevel !== undefined) ? ontimePaymentsImpactLevel : 'HIGH')


            creditUtilizationData["impact"] = ((creditLimitUtilizationImpactLevel !== null && creditLimitUtilizationImpactLevel !== undefined) ? creditLimitUtilizationImpactLevel : 'HIGH')
            oldestOpenAccountData["impact"] = ((oldestActiveAccountImpactLevel !== null && oldestActiveAccountImpactLevel !== undefined) ? oldestActiveAccountImpactLevel : 'MEDIUM')
            totalNoOfCreditCardsAndLoansData["impact"] = ((totalCreditAndLoanAccountsImpactLevel !== null && totalCreditAndLoanAccountsImpactLevel !== undefined) ? totalCreditAndLoanAccountsImpactLevel : 'LOW')
            enquiryInfoData["impact"] = ((totalCreditEnquiriesImpactLevel !== null && totalCreditEnquiriesImpactLevel !== undefined) ? totalCreditEnquiriesImpactLevel : 'LOW')


        }
        let assetInfoData = state.data?.assetInfoData || CreditScoreConstants.defaultValues.JSON_ARRAY_DEFAULT;
        let custInfo = state.data?.custInfo || CreditScoreConstants.defaultValues.JSON_OBJECT_DEFAULT;

        let cardList = [];

        let monitoringAssetsList = filterMonitoringAssets(custInfo, assetInfoData);
        if (monitoringAssetsList.length > 0) {
            let creditDebitCardsFoundCard = CreditScoreConstants.dashboardCardsTemplate.creditDebitCardsFoundCard;
            creditDebitCardsFoundCard.dataList = monitoringAssetsList;
            cardList.push(creditDebitCardsFoundCard);
        }
        cardList.push(paymentOnTimeData);
        cardList.push(creditUtilizationData);
        cardList.push(oldestOpenAccountData);
        cardList.push(totalNoOfCreditCardsAndLoansData);
        cardList.push(enquiryInfoData);
        cardList.push(CreditScoreConstants.dashboardCardsTemplate.creditScoreTrendCard);
        cardList.push(CreditScoreConstants.dashboardCardsTemplate.contactExperianCard);
        //cardList.push(CSPaymentsDoneOnTimeCardConstants.dashboardCardsTemplate.moreAboutIdentityTheftRow);
        cardList.push(CreditScoreConstants.dashboardCardsTemplate.faqsRow);
        cardList.push(CreditScoreConstants.dashboardCardsTemplate.experianLogoFooterCard);
        return {itemList: cardList}
    } else {
        return state;
    }

    function filterMonitoringAssets(custInfo, assetInfoData) {
        let cardsList = custInfo?.cardInfo?.cards || CreditScoreConstants.defaultValues.JSON_OBJECT_DEFAULT;
        let creditCardAssetsInfo = assetInfoData?.creditCardAssets || CreditScoreConstants.defaultValues.JSON_ARRAY_DEFAULT;
        let cardNeedToAdd = getEmptySlotsForCards(cardsList, creditCardAssetsInfo);
        cardNeedToAdd = cardNeedToAdd.map(item => {
            return ({assetType: 'CARD', assetValue: item});
        });
        let assetList = assetInfoData?.assets || CreditScoreConstants.defaultValues.JSON_ARRAY_DEFAULT;
        let nationalIds = custInfo?.nationalIdInfo?.nationalIds || CreditScoreConstants.defaultValues.JSON_OBJECT_DEFAULT;

        let itemNeedToAdd = [];
        itemNeedToAdd = [...itemNeedToAdd, ...cardNeedToAdd];

        let panInfo = nationalIds?.PAN;
        if (panInfo !== null && panInfo?.nationalIdNo === null) {
            let panCardAssetList = filterAssetsFromList(assetList, 'PAN');
            if (panCardAssetList.length > 0) {
                itemNeedToAdd.push(panCardAssetList[0])
            }
        }
        let dlInfo = nationalIds?.DL;
        if (dlInfo !== null && dlInfo?.nationalIdNo === null) {
            let dlAssetList = filterAssetsFromList(assetList, 'DL_NUMBER');
            if (dlAssetList.length > 0) {
                itemNeedToAdd.push(dlAssetList[0])
            }
        }

        let passportInfo = nationalIds?.PASSPORT;
        if (passportInfo !== null && passportInfo?.nationalIdNo === null) {
            let passportAssetList = filterAssetsFromList(assetList, 'PASSPORT');
            if (passportAssetList.length > 0) {
                itemNeedToAdd.push(passportAssetList[0])
            }
        }

        /*let mobileAssetList = filterAssetsFromList(assetList, 'MOBILE_NO');
        if (mobileAssetList.length > 0) {
            let mobileNeedToAdd = getEmptySlotsForMobile(mobileList, mobileAssetList);
            mobileNeedToAdd.map(item => {
                itemNeedToAdd.push(item);
            });

        }

        let emailAssetList = filterAssetsFromList(assetList, 'EMAIL_ID');
        if (emailAssetList.length > 0) {
            let emailsNeedToAdd = getEmptySlotsForEmail(emailsList, emailAssetList);
            emailsNeedToAdd.map(item => {
                itemNeedToAdd.push(item);
            });
        }*/

        return itemNeedToAdd;
    }

    function filterAssetsFromList(list, key) {
        return list.filter(function (item) {
            if (item?.assetType === key) {
                return item?.assetType === key;
            }
        });
    }


    function getEmptySlotsForCards(itemMap, creditCardAssetsInfo) {
        let emptySlotList = [];
        let cardNeedToAdd = [];
        Object.keys(itemMap).forEach(key => {
            let element = itemMap[key];
            if (element.cardNo !== null) {
                let lastCardD = element.cardNo.substr(element.cardNo.length - 4);
                creditCardAssetsInfo.map(item => {
                    let lastCardDigits = item.substr(item.length - 4);
                    if (lastCardDigits !== lastCardD) {
                        cardNeedToAdd.push(item);
                    }
                });
            } else {
                emptySlotList.push(key);
            }
        });
        cardNeedToAdd = cardNeedToAdd.filter(function (item, pos, self) {
            return self.indexOf(item) == pos;
        })

        let sortedEmptyArr = emptySlotList.sort(sortAlphabetDigits)
        if (sortedEmptyArr.length >= cardNeedToAdd.length) {
            emptySlotList = sortedEmptyArr.slice(0, cardNeedToAdd.length);
        } else {
            emptySlotList = sortedEmptyArr;
        }
        return emptySlotList;
    }

    function getEmptySlotsForMobile(itemMap, mobileAssetList) {
        let emptySlotList = [];
        Object.keys(itemMap).forEach(key => {
            let element = itemMap[key];
            if (element.telephoneNo === null) {
                emptySlotList.push(key);
            }
        });
        let sortedEmptyArr = emptySlotList.sort(sortAlphabetDigits)
        if (sortedEmptyArr.length >= mobileAssetList.length) {
            emptySlotList = sortedEmptyArr.slice(0, mobileAssetList.length);
        } else {
            emptySlotList = sortedEmptyArr;
        }
        return emptySlotList;
    }

    function getEmptySlotsForEmail(itemMap, emailAssetList) {
        let emptySlotList = [];
        Object.keys(itemMap).forEach(key => {
            let element = itemMap[key];
            if (element.emailId === null) {
                emptySlotList.push(key);
            }
        });
        let sortedEmptyArr = emptySlotList.sort(sortAlphabetDigits)
        if (sortedEmptyArr.length >= emailAssetList.length) {
            emptySlotList = sortedEmptyArr.slice(0, emailAssetList.length);
        } else {
            emptySlotList = sortedEmptyArr;
        }
        return emptySlotList;
    }

}
