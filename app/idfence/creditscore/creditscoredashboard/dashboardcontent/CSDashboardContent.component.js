import {View} from "react-native";
import React, {useLayoutEffect, useReducer, useRef} from "react";

import CSDashboardTypeOneCard from "./csdashboardcards/CSDashboardTypeOneCard";
import styles from "./CSDashboardContent.style";
import CSCardsFoundCard from "./csdashboardcards/CSCardsFoundCard";
import CSBezierChart from "./csbezierchart";
import CSContactExperianCard from "./cscontactexperiancard";
import CSExperianLogo from "./creditscoreexperianlogo";
import colors from "../../../../Constants/colors";
import ClickableCell from "../../../../CustomComponent/customcells/clickableCell/ClickableCell";
import SeparatorView from "../../../../CustomComponent/separatorview";
import csDashboardContentReducer, {PREPARE_DEFAULT_DATA} from "./CSDashboardContent.Reducer";
import OAActivityIndicator from "../../../../CustomComponent/OAActivityIndicator";
import {WEBENGAGE_CREDIT_SCORE} from "../../../constants/CreditScoreWebEngage.events";
import {logWebEnageEvent} from "../../../../commonUtil/WebengageTrackingUtils";
import {getIDFCommonEventAttr} from '../../../constants/IDFence.UtilityMethods';


export default ({onPress, creditScoreCustInfo, csCustInfo, assetInfoData, trendInfo, membershipData}) => {

    const initialState = {
        itemList: [],
        data: {creditScoreCustInfo: creditScoreCustInfo, custInfo: csCustInfo, assetInfoData: assetInfoData}
    }
    const [state, dispatchItems] = useReducer(csDashboardContentReducer, initialState);
    const isFirstUpdateRun = useRef(true);

    useLayoutEffect(() => {
        if (isFirstUpdateRun.current) {
            dispatchItems({
                type: PREPARE_DEFAULT_DATA
            });
            isFirstUpdateRun.current = false;
            return;
        }

    });

    return (<View
        style={styles.containerStyle}>
        {renderDashboardCards()}
    </View>);

    function renderDashboardCards() {
        if (state.itemList.length == 0) {
            return <OAActivityIndicator/>;
        }
        return state.itemList.map((element, index) => {

            if (index === 0) {
                if (element.cardType === 'CREDIT_DEBIT_CARDS_FOUND_CARD'){
                    let webEngageEvents = getIDFCommonEventAttr(membershipData);
                    webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.LOCATION] = WEBENGAGE_CREDIT_SCORE.ATTR_VALUE.CREDIT_SCORE_DASHBOARD;
                    logWebEnageEvent(WEBENGAGE_CREDIT_SCORE.EVENT_NAME.ASSET_MIGRATION_CARD_LOADED, webEngageEvents);
                    return (<View key={index} style={styles.csCardsFoundCardContainerStyle}>
                        <CSCardsFoundCard item={element}
                                          marginTop={-40}
                                          assetInfoData={assetInfoData?.creditCardAssets}
                                          onPress={(e) => handleAction(e, element)}
                        />
                    </View>);
                }else{
                    return (<View key={index} style={styles.csCardsFoundCardContainerStyle}>
                        {<CSDashboardTypeOneCard item={element}
                                                 marginTop={-40}
                                                 onPress={(e) => handleAction(e, element)}/>}
                    </View>);
                }

            }
            if (element.cardType === 'CREDIT_SCORE_TREND_CARD') {
                return (
                    <View key={index} style={styles.csBezierChartContainerStyle}>
                        {<CSBezierChart item={trendInfo}/>}
                    </View>);
            }
            if (element.cardType === 'CONTACT_EXPERIAN_CARD') {
                return (
                    <View key={index} style={styles.csContactExperianCardContainerStyle}>
                        {<CSContactExperianCard
                            webEngageLocation='Credit score dashboard'
                            item={element}
                            membershipData={membershipData}
                        />}
                    </View>);
            }
            if (element.cardType === 'MORE_ABOUT_IDENTITY_THEFT_ROW') {
                return (
                    <View key={index}>
                        <SeparatorView separatorStyle={styles.separatorViewStyle}/>
                        <ClickableCell item={element}
                                       onPress={(e) => handleAction(e, element)}
                                       titleStyle={{color: colors.charcoalGrey}}
                                       subTitleStyle={{color: colors.color_CF021B}}
                        />
                    </View>
                );
            }
            if (element.cardType === 'FAQ_ROW') {
                return (
                    <View key={index}>
                        <SeparatorView separatorStyle={styles.separatorViewStyle}/>
                        <ClickableCell item={element}
                                       onPress={(e) => onPress(e, element)}
                                       titleStyle={{color: colors.charcoalGrey}}
                                       subTitleStyle={{color: colors.color_CF021B}}
                        />
                    </View>
                );
            }
            if (element.cardType === 'EXPERIAN_LOGO_FOOTER_CARD') {
                return (
                    <View key={index}>
                        {<CSExperianLogo item={element}/>}
                    </View>);
            }
            return (<View key={index} style={styles.csDashboardTypeOneCardContainerStyle}>
                {<CSDashboardTypeOneCard item={element}
                                         marginTop={0}
                                         onPress={(e) => handleAction(e, element)}/>}
            </View>);
        });
    }

    function handleAction(e, element) {
        if (typeof e?.stopPropagation === "function") {
            e?.stopPropagation();
        }
        onPress(e, element);
    }
}
