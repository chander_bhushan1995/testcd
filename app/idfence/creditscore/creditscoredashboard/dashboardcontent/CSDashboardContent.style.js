import {StyleSheet} from "react-native";
import spacing from "../../../../Constants/Spacing";
import colors from "../../../../Constants/colors";

export default StyleSheet.create({
    containerStyle: {
        backgroundColor:colors.color_FFFFFF,
        justifyContent: 'space-between',
        flex: spacing.spacing1
    },
    csCardsFoundCardContainerStyle: {
        marginTop: spacing.spacing16,
        paddingRight: spacing.spacing16,
        paddingLeft: spacing.spacing16
    },
    csBezierChartContainerStyle: {
        marginTop:spacing.spacing16,


    },
    csContactExperianCardContainerStyle: {
        paddingRight: spacing.spacing16,
        paddingLeft: spacing.spacing16
    },
    csDashboardTypeOneCardContainerStyle: {
        marginTop:spacing.spacing16,
        paddingRight: spacing.spacing16,
        paddingLeft: spacing.spacing16
    },
    separatorViewStyle: {
        backgroundColor: colors.color_F6F6F6,
        height: 0.5,
        marginTop: spacing.spacing16
    }

});
