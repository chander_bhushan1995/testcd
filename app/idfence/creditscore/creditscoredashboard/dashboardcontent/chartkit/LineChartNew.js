import React from 'react'
import {LineChart} from "react-native-chart-kit";
import {Dimensions, View} from "react-native";
import {
    getChartData
} from '../csbezierchart/common'
import {bool} from "prop-types";

let defaultSelectedIndex = 0;
const onCirclePressRef = React.createRef();

class LineChartNew extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        let chartData = getChartData(this.props);
        let fromZero = chartData.labels.length > 1 ? false : true;
        return <View>
            <LineChart
                data={{
                    labels: chartData.labels,
                    datasets: [{data: chartData.dataSet}]
                }}
                width={Dimensions.get("window").width} // from react-native
                height={this.props.height}
                yAxisInterval={chartData.interval}
                fromZero={fromZero}
                chartConfig={{
                    backgroundGradientFromOpacity: 0,
                    backgroundGradientToOpacity: 0,
                    decimalPlaces: 0, // optional, defaults to 2dp
                    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    style: {
                        borderRadius: 16
                    },
                    propsForDots: {
                        r: "6",
                        strokeWidth: "2",
                        stroke: "#ffa726"
                    }
                }}
                style={{
                    marginVertical: 8,
                }}// optional, defaults to 1


            >

            </LineChart>
        </View>

    }

    onLayout = event => {
        if (this.state.dimensions) return // layout was already called
        let {width, height} = event.nativeEvent.layout
        this.setState({dimensions: {width, height}})
    }


}

export default LineChartNew
