import React from 'react'
import {Grid, LineChart, XAxis, YAxis} from "react-native-svg-charts";

import {ScrollView, View} from "react-native";
import {getChartDataForV3} from '../csbezierchart/common'
import spacing from "../../../../../Constants/Spacing";
import {Circle, G, Rect, Text} from "react-native-svg";
import colors from "../../../../../Constants/colors";
import {Dimensions} from 'react-native';


let defaultSelectedIndex = 0;
const onCirclePressRef = React.createRef();
const xLabelWidth = 50;
const chartLeftMargin = 50;

class LineChartV3 extends React.Component {
    constructor(props) {
        super(props)
        let chartData = getChartDataForV3(this.props);
        this.state = {
            selectedIndex: chartData.dataSet.length - 1,
            chartData: chartData,
            tooltip: {
                x: -1,
                y: -1,
                value: "",
                show: false
            }
        }
    }

    render() {

        const data = this.state.chartData.dataSet;
        const labels = this.state.chartData.labels;
        let chartWidth = Dimensions.get('window').width - chartLeftMargin;
        let contentWidth = xLabelWidth * labels.length;

        let finalWidth = chartWidth > contentWidth ? chartWidth : contentWidth;

        const Decorator = ({x, y, data}) => {

            return data.map((value, index) => (
                <Circle
                    key={index}
                    cx={x(index)}
                    cy={y(value)}
                    r={5}
                    strokeWidth={3}
                    stroke={(this.state.selectedIndex == index) ? colors.color_38B839 : colors.color_D8D8D8}
                    fill={'white'}
                    onPress={() => {
                        this.setState({
                            selectedIndex: index,
                            tooltip: {
                                x: x(index),
                                y: y(value),
                                value: data[index],
                                show: true
                            }
                        })
                    }}
                />
            ))
        }


        const Tooltip = () => (

            <G
                x={this.state.tooltip.x - 30}
                y={this.state.tooltip.y}
                key={'tooltip'}
            >
                <G y={8}>
                    <Rect
                        height={spacing.width_30}
                        width={spacing.width_60}
                        fill={colors.charcoalGrey}
                        ry={spacing.spacing10}
                        rx={spacing.spacing10}
                    />
                    <Text
                        x={spacing.width_60 / 2}
                        dy={15}
                        alignmentBaseline={'middle'}
                        textAnchor={'middle'}
                        stroke={'white'}
                    >
                        {this.state.tooltip.value}
                    </Text>
                </G>

            </G>
        )


        return (

            <View style={{height: 200, width: '100%'}}>
                <View style={{
                    height: 200,
                    width: "100%",
                    flexDirection: 'row',
                    paddingHorizontal: spacing.spacing12,
                }}
                >
                    <YAxis
                        data={data}
                        svg={{
                            fill: 'white',
                            fontSize: 10,
                        }}
                        contentInset={{top: 20, bottom: 35}}
                        numberOfTicks={this.props.numberOfYAxisGuideLine}
                        formatLabel={(value) => `${value}`}
                        min={this.state.chartData.yMin}
                        max={this.state.chartData.yMax}>

                    </YAxis>
                    <ScrollView style={{flex: 1}} horizontal={true} showsHorizontalScrollIndicator={false}>
                        <View style={{width: finalWidth}}>
                            <LineChart
                                style={{flex: 1, marginLeft: spacing.spacing10}}
                                data={data}
                                contentInset={{top: 20, bottom: 20, left: 30, right: 20}}
                                numberOfTicks={this.props.numberOfYAxisGuideLine}
                                yMin={this.state.chartData.yMin}
                                yMax={this.state.chartData.yMax}
                                svg={{stroke: 'rgb(255, 255, 255)'}}>
                                <Grid></Grid>
                                <Decorator/>
                                {this.state.tooltip.show && <Tooltip/>}

                            </LineChart>

                            <XAxis
                                data={labels}
                                formatLabel={(value, index) => labels[index]}
                                contentInset={{left: 40, right: 25}}
                                svg={{fontSize: 10, fill: 'white'}}
                            />
                        </View>
                    </ScrollView>

                </View>
            </View>
        )
    }


}

export default LineChartV3
