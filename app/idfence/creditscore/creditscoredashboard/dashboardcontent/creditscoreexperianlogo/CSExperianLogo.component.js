import {Image, Text, View} from "react-native";
import styles from "./CSExperianLogo.style";
import React from "react";
import {OAImageSource} from "../../../../../Constants/OAImageSource";

const CSExperianLogo = ({item}) => (
    <View style={styles.containerStyle}>
        <Text style={styles.logoTextStyle}>Powered By Experian</Text>
        <Image style={styles.logoImageStyle} source={OAImageSource.experian_credit_score}/>
    </View>
);


export default CSExperianLogo;
