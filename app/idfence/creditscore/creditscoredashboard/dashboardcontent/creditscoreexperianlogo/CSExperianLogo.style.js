import {StyleSheet} from "react-native";
import colors from "../../../../../Constants/colors";
import {OATextStyle} from "../../../../../Constants/commonstyles";

export default StyleSheet.create({
    containerStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 80,
        backgroundColor: colors.color_FFFFFF
    },
    logoTextStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        fontStyle: 'italic'
    },
    logoImageStyle: {
        width:99,
        height: 47
    }
});
