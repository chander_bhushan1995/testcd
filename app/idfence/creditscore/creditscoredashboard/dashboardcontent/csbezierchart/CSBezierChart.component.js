import {Text, View} from "react-native";
import React from "react";

import colors from "../../../../../Constants/colors";

import LinearGradient from "react-native-linear-gradient";

import {OATextStyle} from "../../../../../Constants/commonstyles";
import PureChart from "./PureChart";


const height = 175;


export default function CSBezierChart({item}) {


    return (<LinearGradient colors={[colors.color_004890, colors.color_0282F0]}>
        {drawLine()}
    </LinearGradient>)

    function drawLine() {

        if (item?.scoreItems?.length == 0) return null;

        const data = [
            {
                seriesName: 'creditScore', data: item?.scoreItems?.slice(), color: colors.color_FFFFFF
            }
        ]
        return (
            <View style={{paddingTop: 16, height: 300}}>
                <Text style={[OATextStyle.TextFontSize_14_bold, {color: colors.color_FFFFFF, paddingHorizontal: 16}]}>Your
                    credit score
                    trend</Text>
                <View style={{paddingVertical: 14}}>
                    <PureChart type={'line'}
                               data={data}
                               width={'100%'}
                               backgroundColor={'transparent'}
                               height={height}
                               primaryColor={colors.color_FFAB00}
                               xAxisColor={'rgba(255,255,255,0.1)'}
                               yAxisColor={'rgba(255,255,255,0.1)'}
                               xAxisGridLineColor={'rgba(255,255,255,0.1)'}
                               yAxisGridLineColor={'rgba(255,255,255,0.1)'}
                               minValue={10}
                               labelColor={'white'}
                               showEvenNumberXaxisLabel={false}
                               customValueRenderer={(index, point) => {
                                   if (index < 3) return null
                                   return (
                                       <View style={{
                                           width: 44,
                                           justifyContent: 'center',
                                           alignItems: 'center',
                                           height: 30,
                                           borderRadius: 4,
                                           backgroundColor: 'rgba(0,0,0,0.2)'
                                       }}>
                                           <Text style={[OATextStyle.TextFontSize_10_normal_0282F0, {
                                               textAlign: 'center',
                                               color: colors.color_FFFFFF
                                           }]}>{point.y}</Text>
                                       </View>
                                   )
                               }}
                    />
                </View>
                <View
                    style={{
                        justifyContent: 'center',
                        flexDirection: 'row',
                        alignItems: 'center',
                        height: 40,
                        marginTop: -32,
                        flex: 1
                    }}>
                    <Text style={[OATextStyle.TextFontSize_18_normal_212121, {
                        color: '#FFFFFF',
                        opacity: 0.5
                    }]}>&laquo;</Text>
                    <Text style={[OATextStyle.TextFontSize_12_normal_212121, {
                        color: '#FFFFFF',
                        opacity: 0.5
                    }]}> Swipe to
                        see more </Text>
                    <Text style={[OATextStyle.TextFontSize_18_normal_212121, {
                        color: '#FFFFFF',
                        opacity: 0.5
                    }]}>&raquo;</Text>
                </View>
            </View>)
            ;
    }
}



