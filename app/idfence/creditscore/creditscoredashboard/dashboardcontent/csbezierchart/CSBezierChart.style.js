import {StyleSheet} from "react-native";
import colors from "../../../../../Constants/colors";

export default StyleSheet.create({
    containerStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 250,
        backgroundColor: colors.color_0282F0
    }
});
