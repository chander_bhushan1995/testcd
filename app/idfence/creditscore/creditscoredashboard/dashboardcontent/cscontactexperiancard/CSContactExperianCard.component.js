import {Image, Linking, Text, View} from "react-native";
import styles from "./CSContactExperianCard.style";
import React from "react";
import {OATextStyle} from "../../../../../Constants/commonstyles";
import LinkButton from "../../../../../CustomComponent/LinkButton";
import {DetailsTabStrings} from "../../../../constants/Constants";
import {OAImageSource} from "../../../../../Constants/OAImageSource";
import {WEBENGAGE_CREDIT_SCORE} from "../../../../constants/CreditScoreWebEngage.events";
import {logWebEnageEvent} from "../../../../../commonUtil/WebengageTrackingUtils";
import {getIDFCommonEventAttr} from '../../../../constants/IDFence.UtilityMethods';

export default function CSContactExperianCard({item, customStyle, webEngageLocation, membershipData}) {
    return (<View style={[styles.containerStyle, customStyle?.containerStyle]}>
        <Text style={OATextStyle.TextFontSize_14_normal}>{item.placeHolderOne}</Text>
        <View style={styles.callViewStyle}>
            <Image style={styles.callImageStyle} resizeMode={'contain'} source={OAImageSource.call_credit_score}/>
            <LinkButton
                onPress={() => makeCall()}
                style={{
                    actionTextStyle: styles.actionTextStyle
                }}
                data={{
                    action: item.mobileNo
                }}/>
        </View>
        <View style={styles.emailViewStyle}>
            <Image style={styles.emailImageStyle} resizeMode={'contain'} source={OAImageSource.mail_credit_score}/>
            <LinkButton
                onPress={() => openEmail(item.email)}
                style={{
                    actionTextStyle: styles.actionTextStyle
                }}
                data={{
                    action: item.email
                }}/>
        </View>
    </View>);

    function logKnowMoreEvent(mode) {
        let webEngageEvents = getIDFCommonEventAttr(membershipData);
        webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.MODE] = mode;
        webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.LOCATION] = webEngageLocation;
        logWebEnageEvent(WEBENGAGE_CREDIT_SCORE.EVENT_NAME.CONTACT_EXPERIAN, webEngageEvents);
    };

    function makeCall() {
        logKnowMoreEvent(WEBENGAGE_CREDIT_SCORE.ATTR_VALUE.CALL);
        Linking.openURL(`tel:${DetailsTabStrings.experianNumber}`);
    };

    function openEmail(email) {
        logKnowMoreEvent(WEBENGAGE_CREDIT_SCORE.ATTR_VALUE.EMAIL);
        Linking.openURL(`mailto:${email}`);
    };
}


