import {StyleSheet} from "react-native";
import colors from "../../../../../Constants/colors";
import {OATextStyle} from "../../../../../Constants/commonstyles";
import spacing from "../../../../../Constants/Spacing";
import dimens from "../../../../../Constants/Dimens";

export default StyleSheet.create({
    containerStyle: {
        alignItems: 'flex-start',
        paddingVertical: spacing.spacing16,
        backgroundColor: colors.color_FFFFFF
    },
    callViewStyle: {
        flexDirection: 'row',
        marginTop: spacing.spacing20
    },
    callImageStyle: {
        width: dimens.dimen14
    },
    actionTextStyle: {
        ...OATextStyle.TextFontSize_14_bold,
        color: colors.color_0282F0
    },
    emailViewStyle: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    emailImageStyle: {
        width: dimens.dimen14,
        marginTop: spacing.spacing5
    },
});
