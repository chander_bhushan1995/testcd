import {Image, Text, View, TouchableOpacity} from "react-native";
import React from "react";
import {ccDashboardCardStyle} from "./CSDashboardCards.style";
import spacing from "../../../../../Constants/Spacing";
import SeparatorView from "../../../../../CustomComponent/separatorview";
import {OATextStyle} from "../../../../../Constants/commonstyles";
import LinkButton from "../../../../../CustomComponent/LinkButton";
import {OAImageSource} from "../../../../../Constants/OAImageSource";

const CSCardsFoundCard = ({item, assetInfoData, marginTop, onPress}) => (
    <TouchableOpacity onPress={(e) => {
        if (e !== undefined) {
            e.stopPropagation();
        }
        onPress(e, item);
    }} activeOpacity={1} style={[ccDashboardCardStyle.containerStyle, {marginTop: marginTop}]}>
        <View style={[ccDashboardCardStyle.cardViewStyle]}>
            <View style={ccDashboardCardStyle.cardViewTopContainerStyle}>
                <View style={{flex: spacing.spacing1}}>
                    <View style={ccDashboardCardStyle.cardViewTitleItemContainerStyle}>
                        <Text
                            style={OATextStyle.TextFontSize_14_bold}>
                            {item.dataList.length} {item.placeHolderOne}
                        </Text>
                        <Image style={{height: 20, width: 45, marginRight: -15, marginTop: -12}}
                               source={OAImageSource.credit_card_new_badge}/>
                    </View>
                    <View style={ccDashboardCardStyle.cardViewTitleItemValueContainerStyle}>
                        <Text style={[OATextStyle.TextFontSize_12_normal]}>
                            {item.placeHolderTwo}
                        </Text>
                    </View>
                </View>

            </View>
            <View style={ccDashboardCardStyle.cardViewActionContainerStyle}>
                <SeparatorView separatorStyle={ccDashboardCardStyle.cardViewSeparatorViewStyle}/>
                <View
                    style={ccDashboardCardStyle.cardViewActionInnerContainerStyle}>
                    <LinkButton
                        style={{actionTextStyle: ccDashboardCardStyle.cardViewLinkButtonStyle}}
                        onPress={(e) => {
                            if (e !== undefined) {
                                e.stopPropagation();
                            }
                            {
                                onPress(e, item)
                            }
                        }}
                        data={{
                            action: item.actionLabel
                        }}/>
                </View>
            </View>

        </View>
    </TouchableOpacity>
);


export default CSCardsFoundCard;
