import {StyleSheet} from "react-native";
import {CommonStyle} from "../../../../../Constants/CommonStyle";
import spacing from "../../../../../Constants/Spacing";
import OATextStyle from "../../../../../Constants/commonstyles/OAText.style";
import colors from "../../../../../Constants/colors";

export const ccDashboardCardStyle = StyleSheet.create({
    containerStyle: {
        flex: spacing.spacing1,
        height: 132
    },
    cardViewStyle: {
        flex: spacing.spacing1,
        ...CommonStyle.cardContainerWithShadow,
        borderRadius: spacing.spacing4
    },
    cardViewTopContainerStyle: {
        flex: spacing.spacing1,
        paddingTop: spacing.spacing16,
        paddingLeft: spacing.spacing16,
        paddingRight: spacing.spacing16
    },
    cardViewTitleItemContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    cardViewLabel_14_Style: {
        ...OATextStyle.TextFontSize_14_normal,
        color: colors.color_212121
    },
    cardViewLabel_12_Style: {
        ...OATextStyle.TextFontSize_10_normal_0282F0,
        color: colors.color_888F97
    },
    cardViewTitleItemValueContainerStyle: {
        marginTop: spacing.spacing4,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    cardViewActionContainerStyle: {
        height: spacing.spacing40
    },
    cardViewActionInnerContainerStyle: {
        flex: spacing.spacing1,
        justifyContent: 'center',
        alignItems: 'flex-end',
        paddingRight: spacing.spacing6
    },
    cardViewSeparatorViewStyle: {
        color: colors.color_979797,
        opacity: 0.2
    },
    cardViewLinkButtonStyle:{
        ...OATextStyle.TextFontSize_12_bold,
        color: colors.color_0282F0
    }
});


const csPaymentsDoneOnTimeCardStyle = StyleSheet.create({
    SafeArea: {
        flex: 1,
        backgroundColor: "white"
    },
    scrollViewStyle: {
        flex: 1,
        paddingBottom: 32,
        backgroundColor: "white"
    },
    cardShadow: {
        padding: 16,
        shadowColor: "black",
        borderColor: "black",
        backgroundColor: "red"
    }
});



