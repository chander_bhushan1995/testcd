import {Text, View, Image, TouchableOpacity} from "react-native";
import React from "react";
import {ccDashboardCardStyle} from "./CSDashboardCards.style";
import spacing from "../../../../../Constants/Spacing";
import SeparatorView from "../../../../../CustomComponent/separatorview";
import {OATextStyle} from "../../../../../Constants/commonstyles";
import LinkButton from "../../../../../CustomComponent/LinkButton";
import {getRandomColor} from "../../../../../commonUtil/Formatter";
import {OAImageSource} from "../../../../../Constants/OAImageSource";

const CSDashboardTypeOneCard = ({item, marginTop, onPress}) => (
    <TouchableOpacity onPress={(e) => {
        if (e !== undefined) {
            e.stopPropagation();
        }
        onPress();
    }} activeOpacity={1} style={[ccDashboardCardStyle.containerStyle, {marginTop: marginTop}]}>
        <View style={[ccDashboardCardStyle.cardViewStyle]}>
            <View style={ccDashboardCardStyle.cardViewTopContainerStyle}>
                <View style={{flex: spacing.spacing1}}>
                    <View style={ccDashboardCardStyle.cardViewTitleItemContainerStyle}>
                        <Text
                            style={ccDashboardCardStyle.cardViewLabel_14_Style}>
                            {item.placeHolderOne}
                        </Text>
                        <Text
                            style={ccDashboardCardStyle.cardViewLabel_12_Style}>
                            {item.placeHolderTwo}
                        </Text>
                    </View>
                    <View style={ccDashboardCardStyle.cardViewTitleItemValueContainerStyle}>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={[OATextStyle.TextFontSize_28_bold]}>{item.status}</Text>
                            <View style={{
                                paddingBottom: 5,
                                paddingLeft: 5,
                                alignItems: 'flex-end',
                                flexDirection: 'row',
                                display: ((item.percentageDifference !== undefined) ? 'flex' : 'none')
                            }}>
                                <Image reiszeMode={'contain'} style={{height: 12, marginBottom: 4, width: 11}}
                                       source={item.percentageDifference?.img_source}/><Text
                                style={[OATextStyle.TextFontSize_12_normal_212121, {
                                    paddingLeft: 3,
                                    fontStyle: 'italic'
                                }]}>{item.percentageDifference?.scoreTitle}</Text>
                            </View>
                        </View>
                        <Text style={[OATextStyle.TextFontSize_12_bold]}>{item.impact}</Text>
                    </View>
                </View>

            </View>
            <View style={ccDashboardCardStyle.cardViewActionContainerStyle}>
                <SeparatorView separatorStyle={ccDashboardCardStyle.cardViewSeparatorViewStyle}/>
                <View
                    style={ccDashboardCardStyle.cardViewActionInnerContainerStyle}>
                    <LinkButton
                        style={{actionTextStyle: ccDashboardCardStyle.cardViewLinkButtonStyle}}
                        onPress={() => {
                            onPress();
                        }}
                        data={{
                            action: item.actionLabel
                        }}/>
                </View>
            </View>

        </View>
    </TouchableOpacity>
);


export default CSDashboardTypeOneCard;
