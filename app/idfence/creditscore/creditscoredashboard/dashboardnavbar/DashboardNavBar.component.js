import {default as ViewPropTypes, Text, View} from "react-native";
import React from "react";
import CSDashboardNavBackButton from "../CSDashboardNavBackButton";
import styles from "./DashboardNavBar.style";

type Props = {
    toolBarTitle?: string,
    onPress: Function,
    titleStyle: ViewPropTypes.style
};
const DashboardNavBar = ({toolBarTitle, onPress, titleStyle}: Props) => (
    <View style={styles.containerStyle}>
        <CSDashboardNavBackButton title={toolBarTitle} tintColor={titleStyle.color} onPress={onPress}/>
        <Text style={[styles.titleTextStyle, titleStyle]}>{toolBarTitle}</Text>
    </View>
);
/*OAImageSource.icon_back_img.source*/
DashboardNavBar.defaultProps = {
    toolBarTitle: 'Your credit score'
};

export default DashboardNavBar;
