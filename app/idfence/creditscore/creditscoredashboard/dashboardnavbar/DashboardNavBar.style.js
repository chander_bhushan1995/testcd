import {StyleSheet} from "react-native";
import spacing from "../../../../Constants/Spacing";
import {OATextStyle} from "../../../../Constants/commonstyles";
import colors from "../../../../Constants/colors";
import dimens from "../../../../Constants/Dimens";

export default StyleSheet.create({
    containerStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flex: spacing.spacing1
    },
    titleTextStyle: {
        ...OATextStyle.TextFontSize_16_bold,
        left: dimens.dimen60,
        color: colors.color_FFFFFF
    }
});
