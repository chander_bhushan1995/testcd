import {Image, Text, TouchableOpacity, View} from "react-native";
import colors from "../../../../Constants/colors";
import LinearGradient from "react-native-linear-gradient";
import React, {useState, useEffect} from "react";
import styles from "./DashboardToolbar.style";
import {OATextStyle} from "../../../../Constants/commonstyles";
import {formatCreditScoreLastUpdatedOn} from "../../../../commonUtil/Formatter";
import {OAImageSource} from "../../../../Constants/OAImageSource";
import spacing from "../../../../Constants/Spacing";
import dimens from "../../../../Constants/Dimens";
import floats from "../../../../Constants/Floats";
import CircularSlider from "./CircularSlider";

const circularSliderGradientExcellent = [

    {
        stop: '0%',
        color: '#F52600'
    },
    {
        stop: '25%',
        color: '#F68100'
    },
    {
        stop: '50%',
        color: '#E39300'
    },
    {
        stop: '75%',
        color: '#028B00'
    },
    {
        stop: '100%',
        color: '#006C00'
    }
];
const circularSliderGradientGood = [
    {
        stop: '0%',
        color: '#F52600'
    },
    {
        stop: '25%',
        color: '#F68100'
    },
    {
        stop: '65%',
        color: '#F68100'
    },
    {
        stop: '80%',
        color: '#028B00'
    },
    {
        stop: '100%',
        color: '#006C00'
    }
];
const circularSliderGradientFair = [
    {
        stop: '0%',
        color: '#F52600'
    },
    {
        stop: '25%',
        color: '#F68100'
    },
    {
        stop: '50%',
        color: '#E39300'
    },
    {
        stop: '85%',
        color: '#028B00'
    },
    {
        stop: '100%',
        color: '#006C00'
    }
];
const circularSliderGradientDoubtful = [
    {
        stop: '0%',
        color: '#F52600'
    },
    {
        stop: '85%',
        color: '#F68100'
    },
    {
        stop: '100%',
        color: '#028B00'
    }
];
const circularSliderGradientPoor = [
    {
        stop: '0%',
        color: '#F52600'
    },
    {
        stop: '50%',
        color: '#F68100'
    },
    {
        stop: '100%',
        color: '#E39300'
    }
];
const smallCircleColor = {

    excellentColor: {
        "firstColor": "#45B448",
        "secondColor": "#D8D8D8",
    },
    goodColor: {
        "firstColor": "#028B00",
        "secondColor": "#D8D8D8",
    },
    fairColor: {
        "firstColor": "#028B00",
        "secondColor": "#D8D8D8",
    },
    doubtfulColor: {
        "firstColor": "#028B00",
        "secondColor": "#D8D8D8",
    },
    poorColor: {
        "firstColor": "#E39300",
        "secondColor": "#D8D8D8",
    },
    activeColor: {
        "firstColor": "#45B448",
        "secondColor": "#D8D8D8",
    },
    inActiveColor: {
        "firstColor": "#C8C8C8",
        "secondColor": "#C8C8C8",
    }
};
const circularSliderRef = React.createRef();
let minScore = 10, maxScore = 90;
export default function DashboardToolbar({scoreData, infoCreditScorePress}) {
    let circleValue = calCreditScoreData(scoreData?.recentScore, scoreData?.maxScore);
    const [value, setValue] = useState(10);
    const [isLoading, setIsLoading] = useState(true);

    let totalParts = 40;
    let values = [];
    let currentSum = 0;
    let firstItem = circleValue / totalParts;

    useEffect(() => {
        if (isLoading) {
            setIsLoading(false);
            if (firstItem!=='NaN'){
                for (let i = 0; i < totalParts; i++) {
                    currentSum = currentSum + firstItem;
                    values.push(currentSum);
                }
                let timerId = setInterval(() => {
                        if (values.length !== 0) {
                            let item = values.shift();
                            if (item>10){
                                setValue(item)
                            }
                        } else {
                            clearTimeout(timerId);
                        }
                    }
                    , 15);
            }
        }

    });


    return (<LinearGradient
        colors={[colors.color_004890, colors.color_0282F0]}
        style={styles.containerStyle}>
        <View style={styles.linearGradientItemContainerStyle}>
            <View style={{marginTop: spacing.spacing70}}>
                <View style={styles.circularSliderContainerStyle}>
                    <Text style={styles.minScoreLabelStyle}>{scoreData?.minScore}</Text>
                    <CircularSlider
                        ref={circularSliderRef}
                        step={dimens.dimen0}
                        min={minScore}
                        max={maxScore}
                        value={updateCircleValue()}
                        contentContainerStyle={styles.contentContainerStyle}
                        radius={dimens.dimen80}
                        strokeWidth={dimens.dimen10}
                        buttonBorderColor={getSmallCircleColor(scoreData?.recentScore).firstColor}
                        buttonFillColor={getSmallCircleColor(scoreData?.recentScore).secondColor}
                        buttonStrokeWidth={spacing.spacing8}
                        openingRadian={Math.PI / floats.float2_7}
                        buttonRadius={spacing.spacing8}
                        linearGradient={getLinearGradient(scoreData?.recentScore)}>
                    </CircularSlider>
                    <View style={styles.nonClickableStyle}>
                        <TouchableOpacity activeOpacity={.5}
                                          onPress={(e) => infoCreditScorePress(e, {cardType: 'SHOW_SCORE_INFO'})}>
                            <View style={styles.creditScoreInfoContainerStyle}>
                                <Text
                                    style={[OATextStyle.TextFontSize_34_bold, {color: '#FFFFFF'}]}>{scoreData?.recentScore}</Text>

                                <View style={styles.creditScoreInfoSubContainerStyle}>
                                    <Text
                                        style={styles.creditScoreStatusLabelStyle}>
                                        {(scoreData?.scoreLabel === 'Immediate Action Required' ? 'Poor' : scoreData?.scoreLabel)
                                        }</Text>

                                    <Image style={styles.infoCreditScoreImgStyle} resizeMode={'contain'}
                                           source={OAImageSource.info_credit_score}/>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.maxScoreLabelStyle}>{scoreData?.maxScore}</Text>
                </View>
            </View>
            <View style={styles.bottomStripContainerStyle}>
                <Text style={styles.lastUpdatedOnLabelStyle}>
                    {formatCSLastUpdatedDate(scoreData?.lastUpdatedOn)}
                </Text>
                <TouchableOpacity style={styles.trendViewStyle}
                                  onPress={(e) => infoCreditScorePress(e, {cardType: 'SHOW_TREND_INFO'})}>
                    <Image style={styles.trendImageStyle} resizeMode={'contain'}
                           source={OAImageSource.credit_score_view_trend}/>
                    <Text style={styles.trendViewLabelStyle}>View trend</Text>
                </TouchableOpacity>
            </View>

        </View>

    </LinearGradient>);

    function updateCircleValue() {
        if (circularSliderRef.current) {
            circularSliderRef.current.setValueFromParent(value);
        }
        return value;
    }
}


function calCreditScoreData(recentScore, maxScore) {
    let circleValue = (Math.round(((recentScore / maxScore) * 100))) - 10;
    return circleValue;
}


function getLinearGradient(recentScore) {
    if (recentScore > 749) {
        return circularSliderGradientExcellent;
    }
    if (recentScore > 699) {
        return circularSliderGradientGood;
    }
    if (recentScore > 649) {
        return circularSliderGradientFair;
    }
    if (recentScore > 549) {
        return circularSliderGradientDoubtful;
    }

    return circularSliderGradientPoor;
}
function getSmallCircleColor(recentScore) {

    if (recentScore > 749) {
        return smallCircleColor.excellentColor;
    }
    if (recentScore > 699) {
        return smallCircleColor.goodColor;
    }
    if (recentScore > 649) {
        return smallCircleColor.fairColor;
    }
    if (recentScore > 549) {
        return smallCircleColor.doubtfulColor;
    }
    return smallCircleColor.poorColor;
}
function formatCSLastUpdatedDate(lastUpdatedOn) {
    lastUpdatedOn = new Date(lastUpdatedOn);
    let month = String(lastUpdatedOn.getMonth() + 1);
    let day = String(lastUpdatedOn.getDate());
    let year = String(lastUpdatedOn.getFullYear());
    let formattedDate = day + "/" + month + "/" + year;
    if (formattedDate !== 'NaN/NaN/NaN') {
        return ("Last Updated on: " + formatCreditScoreLastUpdatedOn(formattedDate));
    }
    return "";

}

