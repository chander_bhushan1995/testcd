import {StyleSheet} from "react-native";
import colors from "../../../../Constants/colors";
import spacing from "../../../../Constants/Spacing";
import dimens from "../../../../Constants/Dimens";
import {OATextStyle} from "../../../../Constants/commonstyles";
import floats from "../../../../Constants/Floats";

export default StyleSheet.create({
    containerStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: dimens.dimen335,
        backgroundColor: colors.color_0282F0
    },
    titleTextStyle: {
        textAlign: 'center',
        color: colors.color_FFFFFF
    },
    nonClickableStyle: {
        position: "absolute",
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        marginTop: -50,
        height:dimens.dimen150,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor:'rgba(255,255,255,0.0)'
    },
    trendViewStyle: {
        width: spacing.width_105,
        height: dimens.height100,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        borderRadius: spacing.spacing4,
        borderWidth: spacing.spacing0,
        backgroundColor:'rgba(0,0,0,0.1)'
    },
    trendImageStyle: {
        width: spacing.spacing12
    },
    trendViewLabelStyle: {
        ...OATextStyle.TextFontSize_12_normal,
        color: colors.color_FFFFFF,
        marginLeft: spacing.spacing5
    },
    lastUpdatedOnLabelStyle: {
        ...OATextStyle.TextFontSize_12_normal,
        color: colors.color_FFFFFF,
        opacity: floats.float0_7,
        flex: spacing.spacing1,
        alignSelf: 'center'
    },
    bottomStripContainerStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: spacing.spacing20,
        width: dimens.width80,
        height: dimens.dimen40
    },
    maxScoreLabelStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        alignSelf: 'flex-end',
        marginLeft: dimens.neg_dimen12,
        color: colors.color_FFFFFF,
        opacity: floats.float0_5
    },
    minScoreLabelStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        alignSelf: 'flex-end',
        color: colors.color_FFFFFF,
        opacity: floats.float0_5,
        marginRight: dimens.neg_dimen12,
    },
    infoCreditScoreImgStyle: {
        width: dimens.dimen14,
        height: dimens.dimen14,
        marginLeft: spacing.spacing2
    },
    creditScoreStatusLabelStyle: {
        ...OATextStyle.TextFontSize_16_bold,
        color: colors.color_FFFFFF
    },
    creditScoreInfoContainerStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        opacity: 1,
    },
    creditScoreInfoSubContainerStyle: {
        flexDirection: 'row',
        alignSelf: 'center',
        opacity: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    contentContainerStyle: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    circularSliderContainerStyle: {
        flexDirection: 'row',
        height: dimens.dimen80
    },
    linearGradientItemContainerStyle: {
        flex: spacing.spacing1,
        alignItems: 'center',
        paddingTop: spacing.spacing40
    }


});
