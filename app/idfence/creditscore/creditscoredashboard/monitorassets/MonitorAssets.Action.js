import {
    prepareMonitorAssetData,
    refreshMonitorAssets,
    refreshTabBar, resetCreditScoreDashBoardState,
    showError
} from "../../../constants/actionCreators";
import {connect} from "react-redux";
import MonitorAssets from "./MonitorAssets.component";
import {getCreditScoreAssetsInfo, Urls} from "../../../constants/Urls";
import { promiseForGetRequest } from "../../../../network/ApiManager";
import {NativeModules} from "react-native";
import { parseJSON, deepCopy} from "../../../../commonUtil/AppUtils";

const mapStateToProps = state => ({
    isLoading: state.csMonitorAssetsReducer.isLoading,
    monitoringAssetsList: state.csMonitorAssetsReducer.monitoringAssetsList

});
const nativeBridgeRef = NativeModules.ChatBridge;

const mapDispatchToProps = dispatch => ({

    getMonitorAssetsData: (subscriptionNo) => {
        nativeBridgeRef.getApiProperty(apiProperty => {
            apiProperty = parseJSON(apiProperty);
            let urlAssetInfo =
                apiProperty.api_gateway_base_url + getCreditScoreAssetsInfo(subscriptionNo);

            let apiRequestAssetInfo = promiseForGetRequest(
                urlAssetInfo,
                apiProperty.apiHeader
            );
            let urlCustomerInfo =
                apiProperty.api_gateway_base_url + Urls.getCustomerInfo + subscriptionNo;
            let apiRequestCustomerInfo = promiseForGetRequest(
                urlCustomerInfo,
                apiProperty.apiHeader
            );
            let combinedRequest = {"apiAssetInfoRequest": {}, "apiCustomerInfoRequest": {}};
            Promise.all([apiRequestAssetInfo, apiRequestCustomerInfo]).then(function (responses) {
                combinedRequest["apiAssetInfoRequest"] = responses[0];
                combinedRequest["apiCustomerInfoRequest"] = responses[1];
                dispatch(prepareMonitorAssetData(combinedRequest));
            }).catch(function (error) {
                dispatch(showError(error));
            });

        });
    },
    resetAllData: () => {
        dispatch(refreshMonitorAssets());
    },
    refreshTabBarAssets: () => {
        //dispatch(resetCreditScoreDashBoardState(""));
        dispatch(refreshTabBar());
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(MonitorAssets);
