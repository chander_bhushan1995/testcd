import {CREDIT_SCORE_ACTIONS} from "../../../constants/actions";
import {CreditScoreConstants} from "../../creditscoreutils/CreditScore.constants";
import {AllAssetKeys} from "../../../constants/Constants";
import colors from "../../../../Constants/colors";
import Validate from "../../../../commonUtil/DashboardAssetsValidation";
import {sortAlphabetDigits} from "../../../constants/IDFence.UtilityMethods";

const initialState = {
    monitoringAssetsList: [],
    isLoading: true,
}

const csMonitorAssetsReducer = (state = initialState, action) => {
    switch (action.type) {
        case CREDIT_SCORE_ACTIONS.PREPARE_CREDIT_MONITOR_ASSETS_DATA:
            let apiAssetInfoRequest = action?.data.apiAssetInfoRequest.data;
            let apiCustomerInfoRequest = action?.data.apiCustomerInfoRequest.data;
            let monitoringAssetsList = filterMonitoringAssets(apiCustomerInfoRequest, apiAssetInfoRequest);

            return {
                ...state,
                monitoringAssetsList: monitoringAssetsList,
                isLoading: false,
            };
        case CREDIT_SCORE_ACTIONS.RESET_MONITOR_ASSET_STATE:
            return {
                ...state,
                monitoringAssetsList: [],
                isLoading: true,
                isResetAll: false,
            };
        default:
            return {
                ...state,

            };
    }

    function filterMonitoringAssets(custInfo, assetInfoData) {
        let cardsList = custInfo?.cardInfo?.cards || CreditScoreConstants.defaultValues.JSON_OBJECT_DEFAULT;
        let creditCardAssetsInfo = assetInfoData?.creditCardAssets || CreditScoreConstants.defaultValues.JSON_ARRAY_DEFAULT;
        let cardNeedToAdd = getEmptySlotsForCards(cardsList, creditCardAssetsInfo);
        let assetList = assetInfoData?.assets || CreditScoreConstants.defaultValues.JSON_ARRAY_DEFAULT;

        let nationalIds = custInfo?.nationalIdInfo?.nationalIds || CreditScoreConstants.defaultValues.JSON_OBJECT_DEFAULT;

        let itemNeedToAdd = [];
        itemNeedToAdd = [...itemNeedToAdd, ...cardNeedToAdd];
        let panInfo = nationalIds?.PAN;
        if (panInfo !== null && panInfo?.nationalIdNo === null) {
            let panCardAssetList = filterAssetsFromList(assetList, 'PAN');
            if (panCardAssetList.length > 0) {
                let panItem = panCardAssetList[0];
                panItem.index = "PAN";
                panItem.title = "PAN Card (" + panItem.assetValue + ")";
                panItem.subTitle = "Enter Card Number";
                panItem.type = AllAssetKeys.PAN_CARD;
                panItem.errorMessage = "PAN number is mandatory";
                panItem.placeHolder = "";
                panItem.maxLength = 15;
                panItem.autoCapitalize = 'words';
                panItem.placeHolderColor = colors.grey;
                panItem.isMultiLines = false;
                panItem.keyboardType = "default";
                panItem.btnText = "Add Now";
                panItem.assetType= 'PAN';
                panItem.validationErrorMessage = "Please enter a valid PAN number";
                panItem.validationMethodRef = Validate.pan;
                panItem.element = "NATIONAL_ID";
                panItem.dataKey = "nationalId";
                itemNeedToAdd.push(panItem)
            }
        }
        let dlInfo = nationalIds?.DL;
        if (dlInfo !== null && dlInfo?.nationalIdNo === null) {
            let dlAssetList = filterAssetsFromList(assetList, 'DL_NUMBER');
            if (dlAssetList.length > 0) {
                let dlItem = dlAssetList[0];
                dlItem.index = "DL";
                dlItem.title = "DL (" + dlItem.assetValue + ")";
                dlItem.subTitle = "Enter DL Number";
                dlItem.title = "Driving License Number";
                dlItem.type = AllAssetKeys.DRIVING_LICENCE;
                dlItem.errorMessage = "License number is mandatory";
                dlItem.placeHolder = "";
                dlItem.maxLength = 30;
                dlItem.assetType= 'DL';
                dlItem.autoCapitalize = 'words';
                dlItem.placeHolderColor = colors.grey;
                dlItem.isMultiLines = false;
                dlItem.keyboardType = "default";
                dlItem.btnText = "Add Now";
                dlItem.validationErrorMessage = "Please enter a valid license number";
                dlItem.validationMethodRef = Validate.dl;
                dlItem.element = "NATIONAL_ID";
                dlItem.dataKey = "nationalId";
                dlItem.indexKey = "DL";

                itemNeedToAdd.push(dlItem)
            }
        }

        let passportInfo = nationalIds?.PASSPORT;
        if (passportInfo !== null && passportInfo?.nationalIdNo === null) {
            let passportAssetList = filterAssetsFromList(assetList, 'PASSPORT');
            if (passportAssetList.length > 0) {
                let passportItem = passportAssetList[0];
                passportItem.index = "PASSPORT";
                passportItem.title = "Passport (" + passportItem.assetValue + ")";
                passportItem.subTitle = "Enter passport Number";
                passportItem.type = AllAssetKeys.PASSPORT;
                passportItem.errorMessage = "Passport number is mandatory";
                passportItem.placeHolder = "";
                passportItem.maxLength = 50;
                passportItem.assetType= 'PASSPORT';
                passportItem.autoCapitalize = 'words';
                passportItem.placeHolderColor = colors.grey;
                passportItem.isMultiLines = false;
                passportItem.keyboardType = "default";
                passportItem.btnText = "Add Now";
                passportItem.validationErrorMessage = "Please enter a valid passport number";
                passportItem.validationMethodRef = Validate.passport;
                passportItem.element = "NATIONAL_ID";
                passportItem.dataKey = "nationalId";
                passportItem.indexKey = "PASSPORT";
                itemNeedToAdd.push(passportItem)
            }
        }

        /*let mobileAssetList = filterAssetsFromList(assetList, 'MOBILE_NO');
        if (mobileAssetList.length > 0) {
            let mobileNeedToAdd = getEmptySlotsForMobile(mobileList, mobileAssetList);
            mobileNeedToAdd.map(item => {
                item.title = "Mobile number (" + item.assetValue + ")";
                item.subTitle = "Enter mobile Number";
                itemNeedToAdd.push(item);
            });

        }

        let emailAssetList = filterAssetsFromList(assetList, 'EMAIL_ID');
        if (emailAssetList.length > 0) {
            let emailsNeedToAdd = getEmptySlotsForEmail(emailsList, emailAssetList);
            emailsNeedToAdd.map(item => {
                item.title = "Email id (" + item.assetValue + ")";
                item.subTitle = "Enter email id";
                itemNeedToAdd.push(item);
            });
        }*/

        return itemNeedToAdd;
    }

    function filterAssetsFromList(list, key) {
        return list.filter(function (item) {
            if (item?.assetType === key) {
                return item?.assetType === key;
            }
        });
    }


    function getEmptySlotsForCards(itemMap, creditCardAssetsInfo) {
        let emptySlotList = [];
        let cardNeedToAdd = [];
        let cardListData = [];
        Object.keys(itemMap).forEach(key => {
            let element = itemMap[key];
            if (element.cardNo !== null) {
                let lastCardD = element.cardNo.substr(element.cardNo.length - 4);
                creditCardAssetsInfo.map(item => {
                    let lastCardDigits = item.substr(item.length - 4);
                    if (lastCardDigits !== lastCardD) {
                        cardNeedToAdd.push(item);
                    }
                });
            } else {
                emptySlotList.push(key);
            }
        });
        cardNeedToAdd = cardNeedToAdd.filter(function (item, pos, self) {
            return self.indexOf(item) == pos;
        })

        let sortedEmptyArr = emptySlotList.sort(sortAlphabetDigits)
        if (sortedEmptyArr.length >= cardNeedToAdd.length) {
            emptySlotList = sortedEmptyArr.slice(0, cardNeedToAdd.length);
        } else {
            emptySlotList = sortedEmptyArr;
        }
        emptySlotList.map((item, index) => {
            let cardNum = cardNeedToAdd[index];
            cardNum = cardNum.substr(cardNum.length - 6).toLowerCase();
            let element = {
                type: AllAssetKeys.CREDIT_DEBIT,
                errorMessage: "Card number is mandatory",
                placeHolder: "",
                maxLength: 21,
                placeHolderColor: colors.grey,
                isMultiLines: false,
                keyboardType: "number-pad",
                btnText: "Add Now",
                validationErrorMessage: "Please enter a valid card number",
                validationMethodRef: Validate.card,
                element: "CARD",
                dataKey: "cardNo",
                index: item,
                title: "Credit card ending " + cardNum,
                subTitle: "Enter Card Number",
                assetValue: cardNeedToAdd[index],
                assetType: 'CARD'

            };
            cardListData.push(element);
        });

        return cardListData;
    }

    function getEmptySlotsForMobile(itemMap, mobileAssetList) {
        let emptySlotList = [];
        let mobileNeedToAdd = [];
        Object.keys(itemMap).forEach(key => {
            let element = itemMap[key];
            if (element.telephoneNo === null) {
                emptySlotList.push(key);
            }
        });

        let sortedEmptyArr = emptySlotList.sort(sortAlphabetDigits)
        if (sortedEmptyArr.length >= mobileAssetList.length) {
            emptySlotList = sortedEmptyArr.slice(0, mobileAssetList.length);
        } else {
            emptySlotList = sortedEmptyArr;
        }
        emptySlotList.map((item, index) => {
            let element = {
                type: AllAssetKeys.MOBILE,
                errorMessage: "Mobile number is mandatory",
                placeHolder: "",
                maxLength: 10,
                placeHolderColor: colors.grey,
                isMultiLines: false,
                keyboardType: "number-pad",
                btnText: "Add Now",
                validationMethodRef: Validate.telephone,
                validationErrorMessage: "Please enter a valid Mobile number",
                dataKey: "telephoneNo",
                element: "TELEPHONE",
                index: item,
                assetValue: mobileAssetList[index].assetValue,
                assetType: 'TELEPHONE'
            }
            mobileNeedToAdd.push(element);
        });

        return mobileNeedToAdd;
    }

    function getEmptySlotsForEmail(itemMap, emailAssetList) {
        let emptySlotList = [];
        let emailNeedToAdd = [];

        Object.keys(itemMap).forEach(key => {
            let element = itemMap[key];
            if (element.emailId === null) {
                emptySlotList.push(key);
            }
        });

        let sortedEmptyArr = emptySlotList.sort(sortAlphabetDigits)
        if (sortedEmptyArr.length >= emailAssetList.length) {
            emptySlotList = sortedEmptyArr.slice(0, emailAssetList.length);
        } else {
            emptySlotList = sortedEmptyArr;
        }
        emptySlotList.map((item, index) => {
            let element = {
                maxLength: 100,
                type: AllAssetKeys.EMAIL,
                placeHolder: "abc@xyz.com",
                placeHolderColor: colors.grey,
                isMultiLines: false,
                keyboardType: "email-address",
                btnText: "Add Now",
                validationMethodRef: Validate.email,
                validationErrorMessage: "Please enter a valid Email Id",
                element: "EMAIL",
                dataKey: "email",
                index: item,
                assetValue: emailAssetList[index].assetValue,
                assetType: 'EMAIL'
            }
            emailNeedToAdd.push(element);
        });

        return emailNeedToAdd;
    }
}
export default csMonitorAssetsReducer;
