import React from "react";
import {
    Alert,
    BackHandler,
    Platform,
    SafeAreaView,
    ScrollView, StatusBar,
    Text,
    TouchableWithoutFeedback,
    View
} from "react-native";
import {PLATFORM_OS} from "../../../../Constants/AppConstants";
import {TextStyle} from "../../../../Constants/CommonStyle";
import {HeaderBackButton} from "react-navigation";
import colors from "../../../../Constants/colors";
import OAActivityIndicator from "../../../../CustomComponent/OAActivityIndicator";
import styles from "./MonitorAssets.style";
import BankLavelSafety from "../../../../CustomComponent/bankLevelSafety/BankLavelSafety";
import {OAImageSource} from "../../../../Constants/OAImageSource";
import AddAssetsData from "./addassetsview/AddAssetsData.view";
import {addCyberDataAPI, getCardType, SUCCESS_KEY} from "../../../dashboard/screens/IDFenceApiCalls";
import {addMoreAssetList, AllAssetKeys} from "../../../constants/Constants";
import {IDFenceErrorApiErrorMsg} from "../../../../Constants/ApiError.message";
import AlertView from "../../../../CommonComponents/alertView/AlertView";
import {getUniqueRowID} from "../../../constants/IDFence.UtilityMethods";
import MonitorAssetsError from "./MonitorAssets.error";
import {ASSET_NAME, LOCATION, WEBENGAGE_IDFENCE_DASHBOARD} from "../../../../Constants/WebengageAttrKeys";
import {logWebEnageEvent} from "../../../../commonUtil/WebengageTrackingUtils";

let navigator;
let addItemsBottomSheet = React.createRef();
let subscriberNo;
let monitorAssetsErrorRef = React.createRef();
let indexKey = "Data";
let addedAsset = "";
export default class MonitorAssets extends React.Component {
    static navigationOptions = ({navigation}) => {
        navigator = navigation;
        subscriberNo = navigation.getParam("subscriberNo", "");
        return {
            headerTitle: <View
                style={Platform.OS === PLATFORM_OS.ios ? {alignItems: "center"} : {alignItems: "flex-start"}}>
                <Text style={TextStyle.text_16_bold}>Monitor Assets</Text>
            </View>,
            headerLeft: <HeaderBackButton tintColor={colors.black}
                                          onPress={() => {

                                              navigation.pop();
                                          }}/>,
            headerTitleStyle: {color: colors.white},
            headerTintColor: colors.white
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            addMoreData: addMoreAssetList.mobile,
            showError: false,
        };
    }

    componentWillMount() {
        StatusBar.setBarStyle('default');
        if (Platform.OS === 'android') {
            StatusBar.setTranslucent(false);
            StatusBar.setBackgroundColor(colors.color_1468e2, true);
        }
        this.callApis();
    }

    callApis() {
        this.props.getMonitorAssetsData(subscriberNo);

    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }


    render() {
        if (this.props.isLoading) {
            return <OAActivityIndicator/>;
        }
        return (<SafeAreaView style={styles.safeAreaStyle}>
            <MonitorAssetsError ref={monitorAssetsErrorRef}/>
            <ScrollView style={styles.flexOneStyle} keyboardShouldPersistTaps='always'>
                <View>
                    <View style={styles.scrollViewItemContainerStyle}>
                        <Text style={styles.titleLabelStyle}>Note: </Text>
                        <Text style={styles.subTitleLabelStyle}>
                            We will not share your card number with anyone. Card number is required to monitor frauds
                            only.
                        </Text>
                        <TouchableWithoutFeedback style={styles.flexOneStyle}>

                            <View style={styles.flexOneStyle}>
                                {this.addInputBox()}
                            </View>

                        </TouchableWithoutFeedback>
                    </View>
                </View>
            </ScrollView>
            <View style={styles.bankLavelSafetyStyle}>
                <BankLavelSafety key={getUniqueRowID()} data={{
                    title: "100% Secure. Bank level safety",
                    logos: [OAImageSource.ssl, OAImageSource.pci, OAImageSource.norton]
                }}/>
            </View>
            <AlertView ref={ref => (this.AlertViewRef = ref)}/>
        </SafeAreaView>)
    }

    addInputBox() {
        if (Array.isArray(this.props.monitoringAssetsList) && this.props.monitoringAssetsList.length) {
            return this.props.monitoringAssetsList.map((item, index) => {
                return (
                    <AddAssetsData key={index} ref={addItemsBottomSheet}
                                   item={item}
                                   onPress={(textFieldData) =>
                                       this.onAddPress(textFieldData)
                                   }
                    />);
            })
        } else {
            this.onBackPress();
        }

    }

    onAddPress(textFieldData) {
        if (addItemsBottomSheet.current) {
            addItemsBottomSheet.current.setIsLoading(true);
        }

        if (textFieldData?.element === 'CARD') {
            addedAsset = textFieldData?.element;
            indexKey = textFieldData?.textFieldData;
            indexKey = indexKey.substr(indexKey.length - 4);
            indexKey = "xx" + indexKey;
        } else {
            indexKey = textFieldData?.indexKey;
            addedAsset = textFieldData?.indexKey;

        }
        addCyberDataAPI(subscriberNo, textFieldData, textFieldData?.textFieldData, this.handleCyberDataAPIResponse);
    }

    onMinTextForCredDeb(text, type) {
        if (type === AllAssetKeys.CREDIT_DEBIT) getCardType(text, this.handleOnMinTextForCredDebResponse)
    }

    handleOnMinTextForCredDebResponse = (response, status) => {
        if (status === SUCCESS_KEY) {
            let cardType = response?.data?.cardCategory;
            if (cardType === "CRC") {
                this.state.addMoreData.cardType = "CC";

            } else {
                this.state.addMoreData.cardType = "DC";

            }
        }
    };

    handleCyberDataAPIResponse = (response, status) => {
        if (addItemsBottomSheet.current) {
            addItemsBottomSheet.current.setIsLoading(false);
            if (status === SUCCESS_KEY) {
                addItemsBottomSheet.current.setIsLoading(false);
                if (monitorAssetsErrorRef.current) {
                    monitorAssetsErrorRef.current.setErrorMessage(indexKey + " added successfully for monitoring");
                    let attr = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.attrValueForAsset(addedAsset);
                    if (attr !== null && attr !== undefined) {
                        let eventAttr = new Object();
                        eventAttr[LOCATION] = "Credit Score Dashboard Start Scan";
                        eventAttr[ASSET_NAME] = attr;
                        logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_ASSET_ADDED, eventAttr);
                    }
                }

                setTimeout(() => this.showErrorAndResetData(), 2000)
                // this.showAlert("Message", "Data added successfully for monitoring");
            } else {
                if (IDFenceErrorApiErrorMsg.hasOwnProperty(response?.message)) {
                    addItemsBottomSheet.current.setErrorMessage(IDFenceErrorApiErrorMsg[response?.message]);
                    return
                }
                addItemsBottomSheet.current.setErrorMessage(IDFenceErrorApiErrorMsg.LUHN_CHECK_FAILED);
            }
        }
    };


    showAlert(title, alertMessage) {
        Alert.alert(
            title,
            alertMessage,
            [
                {
                    text: 'Ok', onPress: () => {
                        this.props.resetAllData();
                        this.callApis();
                    }
                },
            ]
        )
    }

    showErrorAndResetData() {
        if (monitorAssetsErrorRef.current) {
            monitorAssetsErrorRef.current.setErrorMessage('');
            this.props.resetAllData();
            this.callApis();
        }
    }

    componentWillUnmount() {
        StatusBar.setBarStyle('light-content');
        if (Platform.OS === 'android') {
            StatusBar.setTranslucent(true);
            StatusBar.setBackgroundColor('transparent', true);
        }
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        this.props.resetAllData();
        this.props.refreshTabBarAssets();
    }

    onBackPress = () => {
        if (navigator !== null) {
            navigator?.pop();
        }
        return true;
    };

}
