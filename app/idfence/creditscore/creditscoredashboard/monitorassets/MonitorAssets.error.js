import React, {forwardRef, useImperativeHandle, useState} from "react";
import {Text, View} from "react-native";
import {OATextStyle} from "../../../../Constants/commonstyles";
import colors from "../../../../Constants/colors";
import {getRandomColor} from "../../../../commonUtil/Formatter";

const MonitorAssetsError = forwardRef((props, ref) => {
    const [errorMessage, setErrorMessage] = useState('');
    useImperativeHandle(ref, () => {
        return {
            setErrorMessage: setErrorMessage,
        };
    });
    return (
        <View style={{
            alignItems: 'center',
            justifyContent: 'center',
            height: 40,
            backgroundColor: colors.color_45B448,
            display: (errorMessage === '' ? 'none' : 'flex')
        }}>
            <Text style={[OATextStyle.TextFontSize_12_bold_212121, {color: colors.color_FFFFFF}]}>
                {errorMessage}
            </Text>
        </View>
    );
});
export default MonitorAssetsError;
