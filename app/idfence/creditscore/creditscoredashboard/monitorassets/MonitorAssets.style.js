import {StyleSheet} from "react-native";
import colors from "../../../../Constants/colors";
import {OATextStyle} from "../../../../Constants/commonstyles";
import dimens from "../../../../Constants/Dimens";
import spacing from "../../../../Constants/Spacing";

export default StyleSheet.create({
    safeAreaStyle: {
        flex: spacing.spacing1,
        backgroundColor: colors.white
    },
    flexOneStyle: {
        flex: spacing.spacing1,
    },
    scrollViewItemContainerStyle: {
        flex: spacing.spacing1,
        paddingHorizontal: spacing.spacing16,
        justifyContent: 'space-between',
        paddingVertical: spacing.spacing16
    },
    titleLabelStyle: {
        ...OATextStyle.TextFontSize_12_bold_212121
    },
    subTitleLabelStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        paddingVertical: spacing.spacing8
    },
    bankLavelSafetyStyle: {
        height: dimens.dimen90
    }
});
