import {StyleSheet, View} from "react-native";
import InputBox from "../../../../../Components/InputBox";
import {AllAssetKeys, inputValidationRegex} from "../../../../constants/Constants";
import ButtonWithLoader from "../../../../../CommonComponents/ButtonWithLoader";
import spacing from "../../../../../Constants/Spacing";
import {OATextStyle} from "../../../../../Constants/commonstyles";
import colors from "../../../../../Constants/colors";
import React, {forwardRef, useImperativeHandle, useState} from "react";
import dimens from "../../../../../Constants/Dimens";
import {getCardType, SUCCESS_KEY} from "../../../../dashboard/screens/IDFenceApiCalls";
import {CreditScoreConstants, IDFENCE_ALERT_TYPE} from "../../../creditscoreutils/CreditScore.constants";

const AddAssetsData = forwardRef((props, ref) => {
    let item = props.item;
    const [errorMessage, setErrorMessage] = useState(CreditScoreConstants.defaultValues.STRING_DEFAULT_1);
    const [isLoading, setIsLoading] = useState(false);
    const [text, setText] = useState(CreditScoreConstants.defaultValues.STRING_DEFAULT_1);
    const [cardPrefixText, setCardPrefixText] = useState(CreditScoreConstants.defaultValues.STRING_DEFAULT_1);
    const errorPadding = errorMessage === CreditScoreConstants.defaultValues.STRING_DEFAULT_1 ? spacing.spacing0 : spacing.spacing8;
    const MAX_CARD_PREFIX = 6;
    React.useEffect(() => {
        if (cardPrefixText.length === MAX_CARD_PREFIX) {
            let cType = item.cardType || CreditScoreConstants.defaultValues.STRING_DEFAULT_1;
            if (cType === CreditScoreConstants.defaultValues.STRING_DEFAULT_1) {
                getCardType(text, handleOnMinTextForCredDebResponse)
                return
            }
        }
    }, [cardPrefixText]);
    const handleOnMinTextForCredDebResponse = (response, status) => {
        if (status === SUCCESS_KEY) {
            let cardType = response?.data?.cardCategory || CreditScoreConstants.defaultValues.STRING_DEFAULT_1;
            if ('unknown' !== cardType.toLowerCase()) {
                if (cardType === "CRC") {
                    item.cardType = "CC";
                } else {
                    item.cardType = "DC";
                }
                setCardPrefixText(text);
            } else {
                item.cardType = "";
                setErrorMessage("Invalid card type")
            }
        }
    };
    useImperativeHandle(ref, () => {
        return {
            setIsLoading: setIsLoading,
            setErrorMessage: setErrorMessage,
            getIsLoading: getIsLoading
        };
    });

    const getIsLoading = () => {
        return isLoading;
    }
    return (
        <View style={styles.bottomsheetContainer}>
            <View style={styles.inputContainerStyle}>
                <InputBox
                    maxLength={item.maxLength}
                    autoCapitalize={item.type === AllAssetKeys.EMAIL ? CreditScoreConstants.defaultValues.STRING_DEFAULT_1 : 'words'}
                    containerStyle={styles.textInputContainer}
                    inputHeading={item.title}
                    placeholder={item.subTitle}
                    value={text}
                    placeholderTextColor={item.placeHolderColor}
                    isMultiLines={item.isMultiLines}
                    keyboardType={item.keyboardType}
                    editable={true}
                    errorMessage={errorMessage}
                    onFocus={() => {
                        setErrorMessage(CreditScoreConstants.defaultValues.STRING_DEFAULT_1)

                    }}
                    onChangeText={(text) => {
                        if (errorMessage !== null || errorMessage !== CreditScoreConstants.defaultValues.STRING_DEFAULT_1) {
                            setErrorMessage(CreditScoreConstants.defaultValues.STRING_DEFAULT_1)
                        }
                        let cPrefixText = cardPrefixText || CreditScoreConstants.defaultValues.STRING_DEFAULT_1;
                        if (text.length < MAX_CARD_PREFIX &&
                            item.element === IDFENCE_ALERT_TYPE.CARD && cPrefixText !== CreditScoreConstants.defaultValues.STRING_DEFAULT_1) {
                            setCardPrefixText(CreditScoreConstants.defaultValues.STRING_DEFAULT_1);
                            //item.cardType = CreditScoreConstants.defaultValues.STRING_DEFAULT_1;
                        }
                        validateInputData(text, item.element);
                    }}

                />
            </View>
            <View style={styles.actionButtonContainer}>
                <ButtonWithLoader
                    isLoading={isLoading}
                    enable={!isLoading}
                    Button={{
                        text: (isLoading ? CreditScoreConstants.defaultValues.STRING_DEFAULT_1 : `  Add`),
                        onClick: () => {
                            setErrorMessage(CreditScoreConstants.defaultValues.STRING_DEFAULT_1);
                            if (!item.validationMethodRef(text, false)) {
                                setErrorMessage(item.validationErrorMessage);
                                return;
                            }
                            setIsLoading(true);
                            let element = {
                                element: item.element,
                                textFieldData: text,
                                indexKey: item.index,
                                dataKey: item.dataKey
                            }
                            if (item.element === IDFENCE_ALERT_TYPE.CARD) {
                                element.cardType = item.cardType;
                            }
                            props.onPress(element);
                        },
                    }}
                />
            </View>
        </View>
    );

    function validateInputData(text, element) {
        switch (element) {
            case IDFENCE_ALERT_TYPE.EMAIL:
                text = text.replace(inputValidationRegex.EMAIL_REGEX, CreditScoreConstants.defaultValues.STRING_DEFAULT_1);
                setText(text);
                break;
            case IDFENCE_ALERT_TYPE.TELEPHONE:
                text = text.replace(inputValidationRegex.NUMBER_REGEX, CreditScoreConstants.defaultValues.STRING_DEFAULT_1);
                setText(text);
                break;
            case IDFENCE_ALERT_TYPE.CARD:
                text = text.replace(inputValidationRegex.NUMBER_REGEX, CreditScoreConstants.defaultValues.STRING_DEFAULT_1);
                if (text.length === MAX_CARD_PREFIX && item.type === AllAssetKeys.CREDIT_DEBIT) {
                    setCardPrefixText(text);
                }
                setText(text);
                break;
            case IDFENCE_ALERT_TYPE.NATIONAL_ID:
                text = text.replace(inputValidationRegex.ALPHANUMERIC_REGEX, CreditScoreConstants.defaultValues.STRING_DEFAULT_1);
                setText(text);
                break;
            default:
                setText(text);
                break;
        }
    }
});
export default AddAssetsData;

const styles = StyleSheet.create({
    container: {
        flex: spacing.spacing1,
    },
    bottomsheetContainer: {
        marginTop: spacing.spacing12,
        marginBottom: spacing.spacing16,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center'
    },
    title: {
        ...OATextStyle.TextFontSize_14_normal,
        lineHeight: spacing.spacing22,
        color: colors.color_212121
    },
    inputContainerStyle: {
        flex: spacing.spacing1,
        marginRight: spacing.spacing10
    },
    textInputContainer: {
        flex: spacing.spacing1
    },
    actionButtonContainer: {
        height: dimens.dimen48,
        width: dimens.dimen80,
        marginTop: spacing.spacing10
    }
});
