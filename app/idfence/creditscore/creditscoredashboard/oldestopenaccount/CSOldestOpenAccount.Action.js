import {prepareOldestOpenAccountData} from "../../../constants/actionCreators";
import {connect} from "react-redux";
import CSOldestOpenAccount from "./CSOldestOpenAccount.component";

const mapStateToProps = state => ({
    isLoading: state.csOldestOpenAccountReducer.isLoading,
    oldestOpenAccountData: state.csOldestOpenAccountReducer.oldestOpenAccountData
});
const mapDispatchToProps = dispatch => ({
    prepareOldestActiveAccountData: (accountInfo) => {
        dispatch(prepareOldestOpenAccountData(accountInfo));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(CSOldestOpenAccount);
