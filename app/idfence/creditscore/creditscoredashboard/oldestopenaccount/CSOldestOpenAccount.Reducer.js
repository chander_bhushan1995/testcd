import {CREDIT_SCORE_ACTIONS} from "../../../constants/actions";
import {MAX_ACC_NUM_TEXT} from "../../../constants/Constants";
import {getValidIntNumberFromString} from "../../../constants/IDFence.UtilityMethods";
import {CreditScoreConstants} from "../../creditscoreutils/CreditScore.constants";
import {formatDateDD_MM_YYYY} from "../../../../commonUtil/Formatter";

const initialState = {
    oldestOpenAccountData: {
        cardData: {},
        accountsInfo: [],
        loansInfo: []
    },
    isLoading: true
}
const csOldestOpenAccountReducer = (state = initialState, action) => {
    switch (action.type) {
        case CREDIT_SCORE_ACTIONS.PREPARE_OLDEST_OPEN_ACCOUNT_DATA:
            let oldestActiveAccount = action.data?.oldestActiveAccount;
            let accountsSummaryInfo = action.data?.accountsSummaryInfo;

            let activeAccountCount = getValidIntNumberFromString(accountsSummaryInfo?.activeAccounts);
            let creditCardsCount = getValidIntNumberFromString(accountsSummaryInfo?.activeCreditCardAccounts);
            let activeLoanAccountsCount = getValidIntNumberFromString(accountsSummaryInfo?.activeLoanAccounts);

            let accountData = [
                [activeAccountCount, creditCardsCount, activeLoanAccountsCount],
                ["Active", "Credit Card(s)", "Loan(s)"]
            ];

            let oldestActiveAccountYears = getValidIntNumberFromString(oldestActiveAccount?.oldestActiveAccountYears || CreditScoreConstants.defaultValues.INT_DEFAULT);
            let oldestActiveAccountMonths = getValidIntNumberFromString(oldestActiveAccount?.oldestActiveAccountMonths || CreditScoreConstants.defaultValues.INT_DEFAULT);

            let oldestActiveAccYearAndMonth = "--";
            let ageOfOldestAccTitle = "Age of your Oldest Active Account :";
            let oldestActiveAccountNo = oldestActiveAccount?.oldestActiveAccountNo || CreditScoreConstants.defaultValues.STRING_DEFAULT_1;
            oldestActiveAccountNo = oldestActiveAccountNo.substr(oldestActiveAccountNo.length - MAX_ACC_NUM_TEXT);
            oldestActiveAccountNo = oldestActiveAccountNo.toLowerCase();
            let oldestActiveAccountBankName = oldestActiveAccount?.oldestActiveAccountBankName || CreditScoreConstants.defaultValues.STRING_DEFAULT_1;
            let ageOfOldestAccSubTitle = "";
            if (oldestActiveAccountBankName !== CreditScoreConstants.defaultValues.STRING_DEFAULT_1) {
                ageOfOldestAccSubTitle = `${oldestActiveAccountBankName} : ${oldestActiveAccountNo}`;
            }
            if (oldestActiveAccountYears === 0 && oldestActiveAccountMonths === 0) {
                ageOfOldestAccTitle = ageOfOldestAccTitle + " " + oldestActiveAccYearAndMonth;
            } else if (oldestActiveAccountYears !== 0 && oldestActiveAccountMonths === 0) {
                ageOfOldestAccTitle = ageOfOldestAccTitle + " " + oldestActiveAccountYears + "y ";
            } else if (oldestActiveAccountYears === 0 && oldestActiveAccountMonths !== 0) {
                ageOfOldestAccTitle = ageOfOldestAccTitle + " " + oldestActiveAccountMonths + "m ";
            } else {
                ageOfOldestAccTitle = ageOfOldestAccTitle + " " + oldestActiveAccountYears + "y " + oldestActiveAccountMonths + "m ";
            }
            let oldestOpenAccountData = {
                accountData: accountData,
                title: ageOfOldestAccTitle,
                subTitle: ageOfOldestAccSubTitle,

            };
            state.oldestOpenAccountData.cardData = oldestOpenAccountData;
            // get all credit accounts
            let accountList = action.data?.accountList || CreditScoreConstants.defaultValues.JSON_ARRAY_DEFAULT;

            let accountItemList = accountList.map(item => {
                let accNo = item.accountNo || CreditScoreConstants.defaultValues.STRING_DEFAULT_2;
                if (accNo.length > 5) {
                    item.accountNo = (accNo.substr(accNo.length - 6)).toLowerCase();
                } else {
                    item.accountNo = "--";
                }
                return item;
            });
            // cards info
            let cardItemList = accountItemList.filter(function (item) {
                return item?.accountType === CreditScoreConstants.accountTypeKeys.CREDIT_CARD && item?.accountStatus === 'ACTIVE';
            });

            let allAccountsData = cardItemList.map((element) => {
                let item = {};
                let bankName = element?.bankName || CreditScoreConstants.defaultValues.STRING_DEFAULT_2;
                item.title = bankName.trim();
                item.subTitle = element?.accountNo;
                let dateOpened = element?.dateOpened || CreditScoreConstants.defaultValues.INT_DEFAULT;
                if (dateOpened !== 0) {
                    item.centerTitle = formatDateDD_MM_YYYY(dateOpened);
                } else {
                    item.centerTitle = "--";
                }

                let yearsOfAge = element?.yearsOfAge || CreditScoreConstants.defaultValues.INT_DEFAULT;
                let monthsOfAge = element?.monthsOfAge || CreditScoreConstants.defaultValues.INT_DEFAULT
                let accAge = "--";
                if (yearsOfAge !== 0 && monthsOfAge !== 0) {
                    accAge = yearsOfAge + "y, " + monthsOfAge + "m";
                } else if (yearsOfAge !== 0 && monthsOfAge === 0) {
                    accAge = yearsOfAge + "y";
                } else if (yearsOfAge === 0 && monthsOfAge !== 0) {
                    accAge = monthsOfAge + "m";
                }
                item.rightTitle = accAge;

                return item;
            });
            state.oldestOpenAccountData.accountsInfo = allAccountsData;

            // Loan info
            let loanItemList = accountItemList.filter(function (item) {
                return item?.accountType === CreditScoreConstants.accountTypeKeys.LOAN && item?.accountStatus === 'ACTIVE';
            });

            let allLoansData = loanItemList.map((element) => {
                let item = {};
                let bankName = element?.bankName || CreditScoreConstants.defaultValues.STRING_DEFAULT_2;
                item.title = bankName.trim();
                item.subTitle = element?.loanType || CreditScoreConstants.defaultValues.STRING_DEFAULT_2;
                let yearsOfAge = element?.yearsOfAge || CreditScoreConstants.defaultValues.INT_DEFAULT;
                let monthsOfAge = element?.monthsOfAge || CreditScoreConstants.defaultValues.INT_DEFAULT;
                let accAge = "--";
                if (yearsOfAge !== 0 && monthsOfAge !== 0) {
                    accAge = yearsOfAge + "y, " + monthsOfAge + "m";
                } else if (yearsOfAge !== 0 && monthsOfAge === 0) {
                    accAge = yearsOfAge + "y";
                } else if (yearsOfAge === 0 && monthsOfAge !== 0) {
                    accAge = monthsOfAge + "m";
                }
                item.rightTitle = accAge;
                return item;
            });
            state.oldestOpenAccountData.loansInfo = allLoansData;
            return {
                ...state,
                oldestOpenAccountData: state.oldestOpenAccountData,
                isLoading: false
            };
        default:
            return {
                ...state
            };
    }
}

export default csOldestOpenAccountReducer;
