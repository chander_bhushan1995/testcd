import React from "react";
import {BackHandler, NativeModules, Platform, SafeAreaView, ScrollView, StatusBar, Text, View} from "react-native";
import {PLATFORM_OS} from "../../../../Constants/AppConstants";
import {TextStyle} from "../../../../Constants/CommonStyle";
import {OATextStyle} from "../../../../Constants/commonstyles";
import {HeaderBackButton} from "react-navigation";
import colors from "../../../../Constants/colors";
import OAActivityIndicator from "../../../../CustomComponent/OAActivityIndicator";
import styles from "./CSOldestOpenAccount.style";
import CardTypeTwo from "../../../../CustomComponent/cardviews/cardtypetwo";
import CSContactExperianCard from "../dashboardcontent/cscontactexperiancard";
import {CreditScoreConstants, CS_ACTION_TYPES} from "../../creditscoreutils/CreditScore.constants";
import SeparatorView from "../../../../CustomComponent/separatorview";
import ClickableCell from "../../../../CustomComponent/customcells/clickableCell/ClickableCell";
import CSExperianLogo from "../dashboardcontent/creditscoreexperianlogo";
import {getIDFenceFQAUrl} from "../../../constants/Urls";
import {DetailsTabStrings} from "../../../constants/Constants";
import CreditCardsLoans from "./creditcardsloans/CreditCardsLoans.component";
import spacing from "../../../../Constants/Spacing";
import dimens from "../../../../Constants/Dimens";
import {WEBENGAGE_CREDIT_SCORE} from "../../../constants/CreditScoreWebEngage.events";
import {logWebEnageEvent} from "../../../../commonUtil/WebengageTrackingUtils";
import {getIDFCommonEventAttr} from '../../../constants/IDFence.UtilityMethods';
import { parseJSON, deepCopy} from "../../../../commonUtil/AppUtils";

const nativeBridgeRef = NativeModules.ChatBridge;
let navigator;
export default class CSOldestOpenAccount extends React.Component {

    static navigationOptions = ({navigation}) => {
        navigator = navigation;
        return {
            headerStyle: {
                height: dimens.dimen80,
            },
            headerTitle: <View
                style={Platform.OS === PLATFORM_OS.ios ? {alignItems: "center"} : {alignItems: "flex-start"}}>
                <Text style={TextStyle.text_16_bold}>Oldest open account</Text>
                <Text style={[OATextStyle.TextFontSize_12_normal, {marginTop: spacing.spacing4}]}>Impact on credit
                    score: Medium</Text>
            </View>,
            headerLeft: <HeaderBackButton tintColor={colors.black}
                                          onPress={() => {
                                              navigation.pop();
                                          }}/>,
            headerTitleStyle: {color: colors.white},
            headerTintColor: colors.white
        };
    };

    componentWillMount() {
        StatusBar.setBarStyle('default');
        if (Platform.OS === 'android') {
            StatusBar.setTranslucent(false);
            StatusBar.setBackgroundColor(colors.color_1468e2, true);
        }
        const {navigation} = this.props;
        let creditScoreCustInfo = navigation.getParam("creditScoreCustInfo", "");
        if (creditScoreCustInfo !== null) {
            this.props.prepareOldestActiveAccountData(creditScoreCustInfo?.accountInfo);
        } else {
            navigator.pop();
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }

    render() {
        if (this.props.isLoading) {
            return <OAActivityIndicator/>;
        }
        return (<SafeAreaView style={styles.safeAreaStyle}>
            <ScrollView style={styles.flexOneStyle}>
                <View style={styles.cardTypeTwoContainerStyle}>
                    <CardTypeTwo item={this.props.oldestOpenAccountData.cardData?.accountData}
                                 title={this.props.oldestOpenAccountData.cardData?.title}
                                 subTitle={this.props.oldestOpenAccountData.cardData?.subTitle}
                    />
                </View>
                <View style={styles.creditCardsLoansContainerStyle}>
                    <CreditCardsLoans item={this.props.oldestOpenAccountData}/>
                    {<CSContactExperianCard
                        webEngageLocation={CreditScoreConstants.dashboardCardsTemplate.ageOfYourOldestOpenAccountCard.placeHolderOne}
                        customStyle={{containerStyle: styles.csContactExperianCardStyle}}
                        item={CreditScoreConstants.dashboardCardsTemplate.contactExperianCard}
                        membershipData={navigator.getParam("membershipsData", null)}
                    />}
                </View>
                <SeparatorView separatorStyle={{backgroundColor: colors.color_F1F3F4}}/>
                {<ClickableCell item={CreditScoreConstants.dashboardCardsTemplate.faqsRow}
                                titleStyle={{color: colors.charcoalGrey}}
                                onPress={(e) => this.onAction(e, {actionType: CS_ACTION_TYPES.FAQ_PRESS})}
                                subTitleStyle={{color: colors.color_CF021B}}
                />}
                {<CSExperianLogo
                    item={CreditScoreConstants.dashboardCardsTemplate.experianLogoFooterCard}/>}
            </ScrollView>
        </SafeAreaView>)
    }

    onAction(e, item) {
        if (e !== undefined) {
            e.stopPropagation();
        }
        if (item.actionType === CS_ACTION_TYPES.FAQ_PRESS) {
            let webEngageEvents = getIDFCommonEventAttr(navigator.getParam("membershipsData", null));
            webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.LOCATION] = WEBENGAGE_CREDIT_SCORE.ATTR_VALUE.PAYMENT_HISTORY;
            logWebEnageEvent(WEBENGAGE_CREDIT_SCORE.EVENT_NAME.CREDIT_SCORE_FAQ, webEngageEvents);
            nativeBridgeRef.getApiProperty(apiProperty => {
                apiProperty = parseJSON(apiProperty);
                let tncUrlObj = getIDFenceFQAUrl(apiProperty.api_base_url);
                navigator.navigate('TnCWebViewComponent', {
                    productTermAndCUrl: tncUrlObj,
                    enableNativeBack: false,
                    otherParam: DetailsTabStrings.titleFAQs
                });
            });
        }
    }

    componentWillUnmount() {
        StatusBar.setBarStyle('light-content');
        if (Platform.OS === 'android') {
            StatusBar.setTranslucent(true);
            StatusBar.setBackgroundColor('transparent', true);
        }
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    onBackPress = () => {
        if (navigator !== null) {
            navigator.pop();
        }
        return true;
    };
}
