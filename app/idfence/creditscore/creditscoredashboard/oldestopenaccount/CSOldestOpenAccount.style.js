import {StyleSheet} from "react-native";
import spacing from "../../../../Constants/Spacing";
import colors from "../../../../Constants/colors";

export default StyleSheet.create({
    safeAreaStyle: {
        flex: spacing.spacing1,
        backgroundColor: colors.color_FFFFFF
    },
    flexOneStyle: {
        flex: spacing.spacing1
    },
    cardTypeTwoContainerStyle: {
        padding: spacing.spacing16
    },
    creditCardsLoansContainerStyle: {
        paddingVertical: 16
    },
    csContactExperianCardStyle: {
        paddingHorizontal: 16, marginTop: 20
    },

});
