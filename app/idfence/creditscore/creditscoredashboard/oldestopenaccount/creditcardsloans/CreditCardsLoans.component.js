import React from "react";
import {Text, View} from "react-native";
import styles from "./CreditCardsLoans.style";
import {OATextStyle} from "../../../../../Constants/commonstyles";
import colors from "../../../../../Constants/colors";
import CellTypeThree from "../../../../../CustomComponent/customcells/celltypethree";
import CellTypeFour from "../../../../../CustomComponent/customcells/celltypefour";
import ErrorViewTypeOne from "../../../../../CustomComponent/errorviews";
import {EMPTY_ERROR_MESSAGES} from "../../../creditscoreutils/CreditScore.constants";
import {getUniqueRowID} from "../../../../constants/IDFence.UtilityMethods";

const CreditCardsLoans = ({item, customStyle}) => (
    <View style={[styles.containerStyle, customStyle?.containerStyle]}>
        <View style={{marginTop: 24, flex: 1, alignSelf: 'stretch'}}>
            <View>
                <Text style={[OATextStyle.TextFontSize_14_bold, {
                    color: colors.color_212121,
                    paddingHorizontal: 16,
                }, customStyle?.titleStyle]}>Credit Cards ({item?.accountsInfo.length})</Text>
                <View style={{
                    display: (item?.accountsInfo.length === 0 ? "none" : "flex"),
                    marginTop: 30,
                    flex: 1,
                    paddingHorizontal: 16,
                    flexDirection: 'row'
                }}>
                    <View style={{flex: 1, alignItems: 'flex-start'}}>
                        <Text>BANK</Text>
                    </View>
                    <View style={{flex: 1, alignItems: 'flex-end'}}>
                        <Text>DATE OPENED</Text>
                    </View>

                    <View style={{flex: 1, alignItems: 'flex-end'}}>
                        <Text>AGE</Text>
                    </View>
                </View>
                {showAccountItems(item?.accountsInfo)}
            </View>
            <View style={{marginTop: 28, display: (item?.loansInfo.length === 0 ? "none" : "flex")}}>
                <Text style={[OATextStyle.TextFontSize_14_bold, {
                    color: colors.color_212121,
                    paddingVertical: 12,
                    paddingHorizontal: 16
                }, customStyle?.titleStyle]}>Loans ({item?.loansInfo.length})</Text>
                {showLoanItems(item?.loansInfo)}
            </View>

        </View>
    </View>
);

function showAccountItems(accountItemList) {
    if (Array.isArray(accountItemList) && accountItemList.length) {
        return accountItemList.map((item, index) => {
            return (<CellTypeThree key={getUniqueRowID()} item={item}/>);
        });
    }
    return errorView(EMPTY_ERROR_MESSAGES.PAYMENT_HISTORY_CARDS);
}

function showLoanItems(loanItemList) {
    if (Array.isArray(loanItemList) && loanItemList.length) {
        return loanItemList.map((item, index) => {
            return (<CellTypeFour key={getUniqueRowID()} item={item}/>);
        });
    }
    return errorView(EMPTY_ERROR_MESSAGES.PAYMENT_HISTORY_LOANS);
}

function errorView(errorMessage) {
    return (<View style={styles.errorViewStyle}>
        {<ErrorViewTypeOne item={{message: errorMessage}}/>}
    </View>);
}

export default CreditCardsLoans;

