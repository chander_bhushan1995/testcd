import {connect} from "react-redux";
import CSPaymentHistory from "./CSPaymentHistory.component";
import {preparePaymentData} from "../../../constants/actionCreators";

const mapStateToProps = state => ({
    isLoading: state.csPaymentHistoryReducer.isLoading,
    paymentCardData: state.csPaymentHistoryReducer.paymentCardData,
    badgeData:state.csPaymentHistoryReducer.badgeData,
});
const mapDispatchToProps = dispatch => ({
    preparePaymentCardData: (accountInfo) => {
        dispatch(preparePaymentData(accountInfo));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(CSPaymentHistory);
