import {CREDIT_SCORE_ACTIONS} from "../../../constants/actions";
import {creditScoreBadgeViewTypeOne} from "../../../constants/Constants";
import {CreditScoreConstants, EMPTY_ERROR_MESSAGES} from "../../creditscoreutils/CreditScore.constants";
import {formatNumberToPercentage, isNumeric} from "../../../constants/IDFence.UtilityMethods";


const initialState = {
    paymentCardData: {
        summaryInfo: CreditScoreConstants.defaultValues.JSON_ARRAY_DEFAULT,
        cardsInfo: {
            missedPayments: CreditScoreConstants.defaultValues.INT_DEFAULT,
            title: CreditScoreConstants.textLabels.CREDIT_CARDS,
            dataType: CreditScoreConstants.textLabels.DATATYPE_CARDS,
            errorMessage: EMPTY_ERROR_MESSAGES.PAYMENT_HISTORY_CARDS,
            totalCards: CreditScoreConstants.defaultValues.INT_DEFAULT,
            itemList: CreditScoreConstants.defaultValues.JSON_ARRAY_DEFAULT
        },
        loanInfo: {
            missedPayments: CreditScoreConstants.defaultValues.INT_DEFAULT,
            title: CreditScoreConstants.textLabels.LOANS,
            totalCards: CreditScoreConstants.defaultValues.INT_DEFAULT,
            errorMessage: EMPTY_ERROR_MESSAGES.PAYMENT_HISTORY_LOANS,
            dataType: CreditScoreConstants.textLabels.DATATYPE_LOANS,
            itemList: CreditScoreConstants.defaultValues.JSON_ARRAY_DEFAULT
        }
    },
    isLoading: true,
    badgeData: CreditScoreConstants.defaultValues.JSON_OBJECT_DEFAULT
}

const csPaymentHistoryReducer = (state = initialState, action) => {
    switch (action.type) {
        case CREDIT_SCORE_ACTIONS.PREPARE_PAYMENT_HISTORY_DATA:
            let paymentInfo = action.data?.paymentInfo || CreditScoreConstants.defaultValues.JSON_OBJECT_DEFAULT;
            let ontimePaymentsPercentage =formatNumberToPercentage(paymentInfo?.ontimePaymentsPercentage || CreditScoreConstants.defaultValues.INT_DEFAULT)
            //let ontimePaymentsPercentage = `${paymentInfo?.ontimePaymentsPercentage || CreditScoreConstants.defaultValues.STRING_DEFAULT_2} %`;
            let paymentHistoryLevel = paymentInfo?.paymentHistoryLevel;
            let badgeData = {
                label: paymentHistoryLevel,
                isShowBadge: (paymentHistoryLevel !== "NA" && paymentHistoryLevel !== "N/A") ? true : false,
                data: creditScoreBadgeViewTypeOne?.[paymentHistoryLevel] || CreditScoreConstants.defaultValues.JSON_OBJECT_DEFAULT

            }
            let totalPayments = paymentInfo?.totalPayments || CreditScoreConstants.defaultValues.STRING_DEFAULT_2;
            if (!isNumeric(totalPayments)) {
                totalPayments = CreditScoreConstants.defaultValues.STRING_DEFAULT_2;
            }
            let delayedPayments = paymentInfo?.delayedPayments || CreditScoreConstants.defaultValues.STRING_DEFAULT_2;
            if (!isNumeric(delayedPayments)) {
                delayedPayments = CreditScoreConstants.defaultValues.STRING_DEFAULT_2;
            }

            let paymentCardData = [
                [totalPayments, delayedPayments, ontimePaymentsPercentage],
                [CreditScoreConstants.textLabels.TOTAL_PAYMENTS_IN_36_MONTHS,
                    CreditScoreConstants.textLabels.DELAYED_PAYMENTS_IN_36_MONTHS, CreditScoreConstants.textLabels.PERCENTAGE_ON_TIME_PAYMENTS]
            ];
            state.paymentCardData.summaryInfo = paymentCardData;

            let accountList = action.data?.accountList || CreditScoreConstants.defaultValues.JSON_ARRAY_DEFAULT;

            let accountItemList = accountList.map(item => {
                let accNo = item.accountNo || CreditScoreConstants.defaultValues.STRING_DEFAULT_2;
                if (accNo.length > 5) {
                    item.accountNo = (accNo.substr(accNo.length - 6)).toLowerCase();
                } else {
                    item.accountNo = CreditScoreConstants.defaultValues.STRING_DEFAULT_2;
                }
                return item;
            });
            // cards info
            let cardItemList = accountItemList.filter(function (item) {
                return item?.accountType === CreditScoreConstants.accountTypeKeys.CREDIT_CARD;
            });
            let missedPayments = cardItemList
                .map(item => item.missedPayments)
                .reduce((total, current) => total + current, 0);

            state.paymentCardData.cardsInfo.missedPayments = missedPayments;
            state.paymentCardData.cardsInfo.totalCards = cardItemList.length;
            state.paymentCardData.cardsInfo.itemList = cardItemList || CreditScoreConstants.defaultValues.JSON_ARRAY_DEFAULT;

            // loan list
            let loanItemList = accountItemList?.filter(function (item) {
                return item?.accountType === CreditScoreConstants.accountTypeKeys.LOAN;
            });
            missedPayments = loanItemList
                .map(item => item.missedPayments)
                .reduce((total, current) => total + current, 0);


            state.paymentCardData.loanInfo.missedPayments = missedPayments;
            state.paymentCardData.loanInfo.totalCards = loanItemList.length;
            state.paymentCardData.loanInfo.itemList = loanItemList || CreditScoreConstants.defaultValues.JSON_ARRAY_DEFAULT;

            return {
                ...state,
                paymentCardData: state.paymentCardData,
                badgeData: badgeData,
                isLoading: false
            };
        default:
            return {
                ...state
            };
    }
}

export default csPaymentHistoryReducer;
