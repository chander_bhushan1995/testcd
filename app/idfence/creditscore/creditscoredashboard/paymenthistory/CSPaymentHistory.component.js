import React from "react";
import colors from "../../../../Constants/colors";
import styles from "./CSPaymentHistory.style";
import {BackHandler, NativeModules, Platform, SafeAreaView, ScrollView, StatusBar, Text, View} from "react-native";
import {PLATFORM_OS} from "../../../../Constants/AppConstants";
import {TextStyle} from "../../../../Constants/CommonStyle";
import {HeaderBackButton} from "react-navigation";
import CardTypeOne from "../../../../CustomComponent/cardviews/cardtypeone";
import spacing from "../../../../Constants/Spacing";
import CSContactExperianCard from "../dashboardcontent/cscontactexperiancard";
import {CreditScoreConstants, CS_ACTION_TYPES} from "../../creditscoreutils/CreditScore.constants";
import CSExperianLogo from "../dashboardcontent/creditscoreexperianlogo";
import ClickableCell from "../../../../CustomComponent/customcells/clickableCell/ClickableCell";
import SeparatorView from "../../../../CustomComponent/separatorview";
import AccountWiseStatus from "./accountwisestatus";
import ProTipTypeOne from "../../../../CustomComponent/protipviews";
import TipsListTypeOne from "../../../../CustomComponent/tipslistview/TipsListTypeOne";
import OAActivityIndicator from "../../../../CustomComponent/OAActivityIndicator";
import {getIDFenceFQAUrl} from "../../../constants/Urls";
import {DetailsTabStrings} from "../../../constants/Constants";
import CreditScoreInfoBottomSheet from "../CreditScoreInfoBottomSheet.component";
import dimens from "../../../../Constants/Dimens";
import {WEBENGAGE_CREDIT_SCORE} from "../../../constants/CreditScoreWebEngage.events";
import {logWebEnageEvent} from "../../../../commonUtil/WebengageTrackingUtils";
import {getIDFCommonEventAttr} from '../../../constants/IDFence.UtilityMethods';
import { parseJSON, deepCopy} from "../../../../commonUtil/AppUtils";

const nativeBridgeRef = NativeModules.ChatBridge;
let navigator;
let cscoreInfoBottomSheetRef = React.createRef();
let paymentHistoryMapList = [];
export default class CSPaymentHistory extends React.Component {
    static navigationOptions = ({navigation}) => {
        navigator = navigation;
        return {
            headerStyle: {
                height: dimens.dimen80,
            },
            headerTitle: <View
                style={Platform.OS === PLATFORM_OS.ios ? {alignItems: "center"} : {alignItems: "flex-start"}}>
                <Text style={TextStyle.text_16_bold}>Payment History</Text>
                <Text style={styles.headerSubTitleStyle}>Impact on credit score: High</Text>
            </View>,
            headerLeft: <HeaderBackButton tintColor={colors.black}
                                          onPress={() => {
                                              navigation.pop();
                                          }}/>,
            headerTitleStyle: {color: colors.white},
            headerTintColor: colors.white
        };
    };

    componentWillMount() {
        StatusBar.setBarStyle('default');
        if (Platform.OS === 'android') {
            StatusBar.setTranslucent(false);
            StatusBar.setBackgroundColor(colors.color_1468e2, true);
        }
        const {navigation} = this.props;
        let creditScoreCustInfo = navigation.getParam("creditScoreCustInfo", "");
        paymentHistoryMapList = navigation.getParam("scoreInfo", "")?.paymentHistoryMapList;

        if (creditScoreCustInfo !== null) {
            /*let ontimePaymentsPercentage = formatNumberToPercentage(creditScoreCustInfo?.accountInfo?.paymentInfo?.ontimePaymentsPercentage || CreditScoreConstants.defaultValues.STRING_DEFAULT_2);
            this.logKnowMoreEvent(ontimePaymentsPercentage + " " + CreditScoreConstants.textLabels.PAYMENTS_DONE_ON_TIME);*/
            this.props.preparePaymentCardData(creditScoreCustInfo?.accountInfo);
        } else {
            navigator.pop();
        }

    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }

    render() {
        if (this.props.isLoading) {
            return <OAActivityIndicator/>;
        }
        return (<SafeAreaView style={styles.safeAreaStyle}>
            <ScrollView style={styles.flexOneStyle}>
                <View style={styles.badgeViewStyle}>
                    <CardTypeOne item={this.props.paymentCardData?.summaryInfo}
                                 onBadgeViewPress={(e) => this.onAction(e, {actionType: CS_ACTION_TYPES.BADGE_VIEW_PRESS})}
                                 badgeData={this.props.badgeData}/>
                </View>
                <View style={styles.itemViewsContainerStyle}>
                    <AccountWiseStatus
                        item={{
                            data: this.props.paymentCardData.cardsInfo,
                            headerLabel: CreditScoreConstants.textLabels.ACCOUNT_WISE_STATUS
                        }}
                        onPress={(e, item) => {
                            this.onAction(e, item)
                        }}
                        customStyle={{
                            containerStyle: {paddingVertical: spacing.spacing16},
                            headerStyle: {paddingHorizontal: spacing.spacing16}
                        }}/>
                    <AccountWiseStatus
                        item={{
                            data: this.props.paymentCardData.loanInfo,
                            /*headerLabel: CreditScoreConstants.textLabels.ACCOUNT_WISE_STATUS*/
                        }}
                        onPress={(e, item) => {
                            this.onAction(e, item)
                        }}
                        customStyle={{containerStyle: {paddingVertical: spacing.spacing2}}}/>
                    <ProTipTypeOne item={CreditScoreConstants.proTips.PAYMENT_HISTORY}
                                   customStyle={{
                                       containerStyle: {
                                           marginHorizontal: spacing.spacing16,
                                           marginTop: spacing.spacing16
                                       }
                                   }}/>
                    <TipsListTypeOne item={CreditScoreConstants.tipsToImproveData.PAYMENT_HISTORY}/>
                    {<CSContactExperianCard
                        webEngageLocation={CreditScoreConstants.dashboardCardsTemplate.paymentsDoneOnTimeCard.placeHolderOne}
                        customStyle={{
                            containerStyle: {
                                paddingHorizontal: spacing.spacing16,
                                marginTop: spacing.spacing10
                            }
                        }}
                        item={CreditScoreConstants.dashboardCardsTemplate.contactExperianCard}
                        membershipData={navigator.getParam("membershipsData", null)}
                    />}


                </View>
                <SeparatorView separatorStyle={styles.separatorViewStyle}/>
                {<ClickableCell item={CreditScoreConstants.dashboardCardsTemplate.faqsRow}
                                titleStyle={{color: colors.charcoalGrey}}
                                onPress={(e) => this.onAction(e, {actionType: CS_ACTION_TYPES.FAQ_PRESS})}
                                subTitleStyle={{color: colors.color_CF021B}}
                />}
                {<CSExperianLogo
                    item={CreditScoreConstants.dashboardCardsTemplate.experianLogoFooterCard}/>}
            </ScrollView>
            <CreditScoreInfoBottomSheet ref={cscoreInfoBottomSheetRef}/>
        </SafeAreaView>);
    }


    componentWillUnmount() {
        StatusBar.setBarStyle('light-content');
        if (Platform.OS === 'android') {
            StatusBar.setTranslucent(true);
            StatusBar.setBackgroundColor('transparent', true);
        }
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    onAction(e, item) {
        if (e !== undefined) {
            e.stopPropagation();
        }
        switch (item.actionType) {
            case CS_ACTION_TYPES.FAQ_PRESS:
                let webEngageEvents = getIDFCommonEventAttr(navigator.getParam("membershipsData", null));
                webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.LOCATION] = WEBENGAGE_CREDIT_SCORE.ATTR_VALUE.ACCOUNT_AGE;
                logWebEnageEvent(WEBENGAGE_CREDIT_SCORE.EVENT_NAME.CREDIT_SCORE_FAQ, webEngageEvents);
                nativeBridgeRef.getApiProperty(apiProperty => {
                    apiProperty = parseJSON(apiProperty);
                    let tncUrlObj = getIDFenceFQAUrl(apiProperty.api_base_url);
                    navigator.navigate('TnCWebViewComponent', {
                        productTermAndCUrl: tncUrlObj,
                        enableNativeBack: false,
                        otherParam: DetailsTabStrings.titleFAQs
                    });
                });
                break;
            case CS_ACTION_TYPES.BADGE_VIEW_PRESS:
                if (cscoreInfoBottomSheetRef.current) {
                    cscoreInfoBottomSheetRef.current.setBottomSheetData(paymentHistoryMapList);
                    cscoreInfoBottomSheetRef.current.setBottomSheetVisible(true);
                    let webEngageEvents = getIDFCommonEventAttr(navigator.getParam("membershipsData", null));
                    webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.LOCATION] = WEBENGAGE_CREDIT_SCORE.ATTR_VALUE.PAYMENT_HISTORY;
                    logWebEnageEvent(WEBENGAGE_CREDIT_SCORE.EVENT_NAME.SCORE_INFO, webEngageEvents);
                }
                break;
            case CS_ACTION_TYPES.OPEN_DETAILS_VIEW:
                this.showSelectedItemDetail(item);
                break;
            default:
                //this.showSelectedItemDetail(item);
                break;
        }
    }

    logKnowMoreEvent = (impactItem) => {
        let webEngageEvents = getIDFCommonEventAttr(navigator.getParam("membershipsData", null));
        webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.IMPACT_ITEMS] = impactItem;
        logWebEnageEvent(WEBENGAGE_CREDIT_SCORE.EVENT_NAME.CREDIT_SCORE_KNOW_MORE, webEngageEvents);
    };

    showSelectedItemDetail(item) {
        let clickedIndex = item.itemID;
        let dataType = item.dataType;
        let selectedItemData;
        if (dataType === CreditScoreConstants.textLabels.DATATYPE_CARDS) {
            selectedItemData = this.props.paymentCardData.cardsInfo.itemList[clickedIndex];
        } else if (dataType === CreditScoreConstants.textLabels.DATATYPE_LOANS) {
            selectedItemData = this.props.paymentCardData.loanInfo.itemList[clickedIndex];
        }
        if (selectedItemData === undefined) {
            return;
        }

        let webEngageEvents = getIDFCommonEventAttr(navigator.getParam("membershipsData", null));
        webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.LOCATION] = WEBENGAGE_CREDIT_SCORE.ATTR_VALUE.PAYMENT_HISTORY_SCREEN;
        webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.BANK] = "" + selectedItemData?.bankName;
        webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.MISSED] = "" + selectedItemData?.missedPayments;
        webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.DELAYED] = "" + selectedItemData?.delayedPayments;
        logWebEnageEvent(WEBENGAGE_CREDIT_SCORE.EVENT_NAME.VIEW_PAYMENT_HISTORY, webEngageEvents);
        this.props.navigation.navigate("SelectedItemDetail", {
            title: selectedItemData?.bankName || "",
            data: selectedItemData,
            subTitle: selectedItemData?.accountNo || "",
            membershipsData: navigator.getParam("membershipsData", null)
        })
    };

    onBackPress = () => {
        if (navigator !== null) {
            navigator.pop();
        }
        return true;
    };
}
