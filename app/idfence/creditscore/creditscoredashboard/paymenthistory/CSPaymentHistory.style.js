import {StyleSheet} from "react-native";
import colors from "../../../../Constants/colors";
import spacing from "../../../../Constants/Spacing";
import {OATextStyle} from "../../../../Constants/commonstyles";

export default StyleSheet.create({
    safeAreaStyle: {
        flex: spacing.spacing1,
        backgroundColor: colors.color_FFFFFF
    },
    flexOneStyle: {
        flex: spacing.spacing1
    },
    badgeViewStyle: {
        padding: spacing.spacing16
    },
    itemViewsContainerStyle: {
        paddingVertical: spacing.spacing1
    },
    separatorViewStyle: {
        backgroundColor: colors.color_F1F3F4
    },
    headerSubTitleStyle:{
        ...OATextStyle.TextFontSize_12_normal,
        marginTop:spacing.spacing4
    }
});
