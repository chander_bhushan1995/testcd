import React from "react";
import {Text, View} from "react-native";
import styles from "./AccountWiseStatus.style";
import colors from "../../../../../Constants/colors";
import CellTypeOne from "../../../../../CustomComponent/customcells/celltypeone";
import {OAImageSource} from "../../../../../Constants/OAImageSource";
import ErrorViewTypeOne from "../../../../../CustomComponent/errorviews";
import {CS_ACTION_TYPES} from "../../../creditscoreutils/CreditScore.constants";

export default function AccountWiseStatus({item, onPress, customStyle}) {
    return (<View style={[styles.containerStyle, customStyle?.containerStyle]}>
        <Text
            style={[styles.headerLabelStyle, {
                display: (item?.headerLabel ? "flex" : "none")
            }, customStyle?.headerStyle]}>{item?.headerLabel}</Text>
        <View style={styles.headerItemContainerStyle}>
            <Text
                style={[styles.titleStyle, customStyle?.titleStyle]}>{item?.data.title} ({item?.data?.totalCards})</Text>
            <Text style={[styles.missedPaymentsLabelStyle, {
                display: (item?.data?.missedPayments === 0 ? "none" : "flex")
            }, customStyle?.subTitleStyle]}>{item?.data.missedPayments} missed payments</Text>
            <View style={styles.itemContainerStyle}>
                {showItemList(item?.data?.itemList || [], item?.data?.errorMessage)}
            </View>
        </View>
    </View>);

    function showItemList(itemList, errorMessage) {
        if (Array.isArray(itemList) && itemList.length) {
            return itemList.map((element, index) => {
                let missedCount = element?.missedPayments || 0;
                let delayedCount = element?.delayedPayments || 0;
                let rightLabel = "0 delayed";
                let rightLabel2="";
                let rightLabelStyle = {color: colors.color_45B448};
                if (missedCount != 0 && delayedCount != 0) {
                    rightLabel = missedCount + " missed";
                    rightLabel2=", "+delayedCount+ " delayed"
                    rightLabelStyle = {color: colors.color_D0021B};
                } else if (delayedCount != 0) {
                    rightLabel = delayedCount + " delayed";
                    rightLabelStyle = {color: colors.color_EEAA00};
                } else if (missedCount != 0) {
                    rightLabel = missedCount + " missed";
                    rightLabelStyle = {color: colors.color_D0021B};
                }

                let subTitle = element?.accountNo;
                if (element?.accountType === 'LOAN') {
                    subTitle = element?.loanType;
                }

                return (<CellTypeOne key={index} onPress={onPress}
                                     item={{
                                         title: element?.bankName || "",
                                         subTitle: subTitle || "",
                                         rightLabel: rightLabel,
                                         rightLabel2:rightLabel2,
                                         imgRight: OAImageSource.chevron_down.source,
                                         actionType: CS_ACTION_TYPES.OPEN_DETAILS_VIEW,
                                         dataType: item?.data?.dataType,
                                         itemID: index
                                     }}
                                     customStyle={{
                                         rightLabelStyle: rightLabelStyle,
                                         rightLabel2Style: {color: colors.color_EEAA00},
                                         containerStyle: styles.cellContainerStyle,
                                         separatorStyle: styles.separatorStyle
                                     }}/>);
            });
        }
        return errorView(errorMessage);
    }

    function errorView(errorMessage) {
        return (<View style={styles.errorViewStyle}>
            {<ErrorViewTypeOne item={{message: errorMessage}}/>}
        </View>);
    }
}

