import {StyleSheet} from "react-native";
import colors from "../../../../../Constants/colors";
import {OATextStyle} from "../../../../../Constants/commonstyles";
import spacing from "../../../../../Constants/Spacing";

export default StyleSheet.create({
    containerStyle: {
        alignItems: 'flex-start',
        backgroundColor: colors.color_FFFFFF
    },
    headerLabelStyle: {
        ...OATextStyle.TextFontSize_12_bold_212121,
        color: colors.color_888F97,
        textTransform: 'uppercase'
    },
    headerItemContainerStyle: {
        marginTop: spacing.spacing24,
        flex: spacing.spacing1,
        alignSelf: 'stretch'
    },
    titleStyle: {
        ...OATextStyle.TextFontSize_14_bold,
        color: colors.color_212121,
        paddingHorizontal: spacing.spacing16
    },
    missedPaymentsLabelStyle: {
        ...OATextStyle.TextFontSize_12_bold,
        color: colors.color_CF021B,
        marginTop: spacing.spacing6,
        paddingHorizontal: spacing.spacing16,
    }
    ,
    itemContainerStyle: {
        marginTop: spacing.spacing20
    },
    separatorStyle:{
        backgroundColor: colors.color_F1F3F4
    },

    cellContainerStyle:{
        paddingHorizontal: spacing.spacing16
    },
    errorViewStyle: {
        marginTop:-20,
        paddingHorizontal: spacing.spacing16
    }

});
