import React from "react";
import {BackHandler, Platform, SafeAreaView, Text, View} from "react-native";
import {PLATFORM_OS} from "../../../../../Constants/AppConstants";
import {TextStyle} from "../../../../../Constants/CommonStyle";
import {OATextStyle} from "../../../../../Constants/commonstyles";
import {HeaderBackButton} from "react-navigation";
import colors from "../../../../../Constants/colors";
import styles from "./SelectedItemDetail.style";
import ScrollableTabView, {ScrollableTabBar} from "../../../../../scrollableTabView/index";
import MissedPayments from "./tabs/MissedPayments.tab";
import AccountDetails from "./tabs/AccountDetails.tab";
import {WEBENGAGE_CREDIT_SCORE} from "../../../../constants/CreditScoreWebEngage.events";
import {logWebEnageEvent} from "../../../../../commonUtil/WebengageTrackingUtils";
import dimens from "../../../../../Constants/Dimens";
import spacing from "../../../../../Constants/Spacing";
import {getIDFCommonEventAttr} from '../../../../constants/IDFence.UtilityMethods';

let navigator;
export default class SelectedItemDetail extends React.Component {
    static navigationOptions = ({navigation}) => {
        navigator = navigation;
        let title = navigation.getParam('title', "Payment History")
        let subTitle = navigation.getParam('subTitle', "Impact on credit score: High")
        return {
            headerStyle: {
                borderBottomWidth: 0,
                elevation: 0,
                height: dimens.dimen80,
            },
            headerTitle: <View
                style={Platform.OS === PLATFORM_OS.ios ? {alignItems: "center"} : {alignItems: "flex-start"}}>
                <Text style={TextStyle.text_16_bold}>{title}</Text>
                <Text style={[OATextStyle.TextFontSize_12_normal,{marginTop:spacing.spacing4}]}>{subTitle}</Text>
            </View>,
            headerLeft: <HeaderBackButton tintColor={colors.black}
                                          onPress={() => {
                                              navigation.pop();
                                          }}/>,
            headerTitleStyle: {color: colors.white},
            headerTintColor: colors.white
        };
    };

    componentWillMount() {
        const {navigation} = this.props;
        let itemData = navigation.getParam("data", {});
        this.logViewPaymentHistoryEvent(itemData);
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    render() {
        return (<SafeAreaView style={styles.safeAreaStyle}>
            <ScrollableTabView
                style={{marginTop: 0, color: colors.color_008DF6}}
                initialPage={0}
                ref={(tabView) => {
                    if (tabView != null) {
                        this.tabView = tabView;
                    }
                }}
                renderTabBar={() => (
                    <ScrollableTabBar
                        tabsContainerStyle={styles.tabContainerView}
                        underlineStyle={styles.tabBarUnderlineView}
                        textStyle={styles.tabBarTextStyle}
                        activeTextColor={colors.blue028}
                        inactiveTextColor={colors.grey}
                    />
                )}
            >
                <MissedPayments tabLabel="Missed Payments"
                                data={navigator.getParam("data", {})}
                                tabView={this.tabView}/>
                <AccountDetails tabLabel="Account Details"
                                data={navigator.getParam("data", {})}
                                tabView={this.tabView}/>
            </ScrollableTabView>
        </SafeAreaView>);
    }

    logViewPaymentHistoryEvent = (item) => {
        let webEngageEvents = getIDFCommonEventAttr(navigator.getParam("membershipsData", null));;
        webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.LOCATION] = WEBENGAGE_CREDIT_SCORE.ATTR_VALUE.PAYMENT_HISTORY_SCREEN;
        webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.BANK] = "" + item?.bankName;
        webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.MISSED] = "" + item?.missedPayments;
        webEngageEvents[WEBENGAGE_CREDIT_SCORE.ATTR_NAME.DELAYED] = "" + item?.delayedPayments;
        logWebEnageEvent(WEBENGAGE_CREDIT_SCORE.EVENT_NAME.CREDIT_SCORE_KNOW_MORE, webEngageEvents);
    };
    onBackPress = () => {
        if (navigator !== null) {
            navigator.pop();
        }
        return true;
    };
}
