import {StyleSheet} from "react-native";
import spacing from "../../../../../Constants/Spacing";
import colors from "../../../../../Constants/colors";
import {OATextStyle} from "../../../../../Constants/commonstyles";

export default StyleSheet.create({
    safeAreaStyle: {
        flex: spacing.spacing1,
        backgroundColor: colors.color_FFFFFF
    },
    flexOneStyle: {
        flex: spacing.spacing1
    },
    backNavButtonContainer: {
        width: spacing.spacing24,
        height: spacing.spacing24,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing12
    },
    navigationTitle: {
        ...OATextStyle.TextFontSize_18_bold_212121
    },
    tabBarTextStyle: {
        fontSize: 13,
        fontFamily: "Lato-Semibold",
        lineHeight: 20
    },
    tabBarUnderlineView: {
        backgroundColor: colors.blue028,
        height: spacing.spacing4,
        borderRadius: spacing.spacing2
    },
    tabContainerView: {
        borderBottomColor: colors.grey,
    }
});
