import {ScrollView, Text, View} from "react-native";
import React from "react";
import {CreditScoreConstants} from "../../../../creditscoreutils/CreditScore.constants";
import TipsListTypeOne from "../../../../../../CustomComponent/tipslistview/TipsListTypeOne";
import styles from "./DetialsTabs.style";
import {formatDateDD_MM_YYYY} from "../../../../../../commonUtil/Formatter";
import {getUniqueRowID} from "../../../../../constants/IDFence.UtilityMethods";

const AccountDetails = ({data}) => (
    <View style={styles.containerStyle}>
        <ScrollView style={styles.flexOneStyle}>
            <View style={styles.headerContainerStyle}>
                {getRowItems(data)}
            </View>
            <TipsListTypeOne item={CreditScoreConstants.tipsToImproveData.PAYMENT_HISTORY}/>
        </ScrollView>
    </View>
);

function getRowItems(data) {
    return prepareDataListForUI(data).map(item => {
        return (<View key={getUniqueRowID()} style={styles.rowContainerStyle}>
            <View style={styles.flexOneStyle}>
                <Text style={styles.leftTitleStyle}>{item.lLabel}</Text>
            </View>
            <View style={styles.flexOneStyle}>
                <Text style={styles.rightTitleStyle}>{item.rLabel}</Text>
            </View>
        </View>);
    });
}

function prepareDataListForUI(data) {
    let accDetailsList = [];

    accDetailsList.push({
        lLabel: 'Date Opened :',
        rLabel: (data?.dateOpened !== null ? formatDateDD_MM_YYYY(data?.dateOpened) : CreditScoreConstants.defaultValues.STRING_DEFAULT_2)
    });
    accDetailsList.push({
        lLabel: 'Date Closed :',
        rLabel: (data?.dateClosed !== null ? formatDateDD_MM_YYYY(data?.dateClosed) : CreditScoreConstants.defaultValues.STRING_DEFAULT_2)
    });
    accDetailsList.push({
        lLabel: 'Value of Collateral :',
        rLabel: '--'
    });
    accDetailsList.push({
        lLabel: 'Type of Collateral :',
        rLabel: '--'
    });
    accDetailsList.push({
        lLabel: 'Date Reported :',
        rLabel: (data?.dateReported !== null ? formatDateDD_MM_YYYY(data?.dateReported) : CreditScoreConstants.defaultValues.STRING_DEFAULT_2)
    });

    if (data?.loanType !== null) {
        accDetailsList.push({
            lLabel: 'Loan Type :',
            rLabel: data?.loanType || CreditScoreConstants.defaultValues.STRING_DEFAULT_2
        });
    }

    accDetailsList.push({
        lLabel: 'Account Status :',
        rLabel: data?.accountStatus || CreditScoreConstants.defaultValues.STRING_DEFAULT_2
    });
    accDetailsList.push({
        lLabel: 'Highest Credit :',
        rLabel: "₹ " + (data?.sanctionAmountOrHighestCredit || CreditScoreConstants.defaultValues.INT_DEFAULT)
    });
    accDetailsList.push({
        lLabel: 'Current Balance :',
        rLabel: "₹ " + (data?.currentBalance || CreditScoreConstants.defaultValues.INT_DEFAULT)
    });
    accDetailsList.push({
        lLabel: 'Amount Overdue :',
        rLabel: "₹ " + (data?.amountOverdue || CreditScoreConstants.defaultValues.INT_DEFAULT)
    });
    accDetailsList.push({
        lLabel: 'Registered Address :',
        rLabel: "" + (data?.address || CreditScoreConstants.defaultValues.STRING_DEFAULT_2)
    });
    return accDetailsList;
}

export default AccountDetails;
