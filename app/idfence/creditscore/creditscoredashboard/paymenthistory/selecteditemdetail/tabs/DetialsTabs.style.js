import {StyleSheet} from "react-native";
import colors from "../../../../../../Constants/colors";
import spacing from "../../../../../../Constants/Spacing";
import {OATextStyle} from "../../../../../../Constants/commonstyles";

export default StyleSheet.create({
    containerStyle: {
        flex: spacing.spacing1,
        justifyContent: "center",
        alignItems: 'stretch'
    },
    flexOneStyle: {
        flex: spacing.spacing1
    },
    headerContainerStyle: {
        paddingBottom: spacing.spacing16,
        paddingHorizontal: spacing.spacing16,
        alignItems: 'stretch'
    },
    rowContainerStyle: {
        flex: spacing.spacing1,
        paddingVertical: spacing.spacing16,
        flexDirection: 'row',
        alignItems: 'stretch'
    },
    leftTitleStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        color: colors.color_212121
    },
    rightTitleStyle: {
        ...OATextStyle.TextFontSize_14_bold,
        color: colors.color_212121
    }

});
