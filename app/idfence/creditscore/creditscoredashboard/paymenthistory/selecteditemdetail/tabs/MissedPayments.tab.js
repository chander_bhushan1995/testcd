import {ScrollView, Text, View} from "react-native";
import React from "react";
import SeparatorView from "../../../../../../CustomComponent/separatorview";
import {CreditScoreConstants} from "../../../../creditscoreutils/CreditScore.constants";
import colors from "../../../../../../Constants/colors";
import {OATextStyle} from "../../../../../../Constants/commonstyles";
import TipsListTypeOne from "../../../../../../CustomComponent/tipslistview/TipsListTypeOne";
import {getUniqueRowID} from "../../../../../constants/IDFence.UtilityMethods";

const MissedPayments = ({data}) => (
    <View style={{flex: 1, justifyContent: "center", alignItems: 'stretch'}}>
        <ScrollView style={{flex: 1}}>
            <View key={getUniqueRowID()} style={{paddingVertical: 24, alignItems: 'stretch'}}>
                {missedAndDelayedPaymentsCount(data)}
                <View style={{
                    flex: 1, minHeight: 250
                }}>
                    <View style={{
                        paddingHorizontal: 16,
                        marginTop: 35,
                        flexDirection: 'row'
                    }}>
                        <View style={{flex: 1}}>
                            <Text
                                style={[OATextStyle.TextFontSize_12_bold_212121, {color: colors.color_888F97}]}>YEAR</Text>
                        </View>
                        <View style={{flex: 1}}>
                            <Text
                                style={[OATextStyle.TextFontSize_12_bold_212121, {color: colors.color_888F97}]}>PAYMENT</Text>
                        </View>
                        <View style={{flex: 1}}>
                            <Text
                                style={[OATextStyle.TextFontSize_12_bold_212121, {color: colors.color_888F97}]}>MONTH</Text>
                        </View>
                    </View>
                    <View style={{marginTop: 4}}>
                        {missedAndDelayedPaymentsRow(data)}
                    </View>
                </View>
                <TipsListTypeOne item={CreditScoreConstants.tipsToImproveData.PAYMENT_HISTORY}/>
            </View>
        </ScrollView>
    </View>
);

function missedAndDelayedPaymentsCount(data) {
    let missedCount = data?.missedPayments || 0; //+ (data?.delayedPayments || "");
    let delayedCount = data?.delayedPayments || 0;
    let titleOne = "0 delayed";
    let titleTwo = "";

    let textTitleStrColor = colors.color_D0021B;
    if (missedCount !== 0 && delayedCount !== 0) {
        titleOne = missedCount + " missed,";
        titleTwo = "" + delayedCount + " delayed";
    } else if (missedCount !== 0 && delayedCount === 0) {
        titleOne = missedCount + " missed";
        titleTwo = "";
    } else if (missedCount === 0 && delayedCount !== 0) {
        titleTwo = delayedCount + " delayed";
        titleOne = "";
    } else {
        textTitleStrColor = colors.color_45B448;
    }
    //titleStr="Your have "+titleStr+"";
    return (<View style={{paddingHorizontal: 16}}>
        <Text style={[OATextStyle.TextFontSize_16_normal, {
            color: colors.color_212121
        }]}>You have <Text style={[OATextStyle.TextFontSize_16_normal, {
            color: textTitleStrColor
        }]}> {titleOne} </Text>
            <Text style={[OATextStyle.TextFontSize_16_normal, {
                color: colors.color_EEAA00
            }]}> {titleTwo} </Text>
            <Text style={[OATextStyle.TextFontSize_16_normal, {
                color: colors.color_212121
            }]}> payment(s) in last 3 years</Text>
        </Text>


    </View>);
}

function missedAndDelayedPaymentsRow(data) {

    // filter data by year

    if (data.accountPaymentHistory === undefined) {
        return null
    }
    let getAllYears = [...new Set(data.accountPaymentHistory.map(item => item.year))]
    let itemListData = getAllYears.map(year => {
        let itemList = data.accountPaymentHistory.filter(function (item) {
            return item?.year === year;
        });

        let onTimeCount = 0;
        let onTimeCountMonths = [];
        let missedCount = 0;
        let missedCountMonths = [];
        let delayedCount = 0;
        let delayedCountMonths = [];

        itemList.map(item => {

            switch (item.paymentStatus) {
                case 'ON_TIME':
                    onTimeCountMonths.push(item.month);
                    onTimeCount++;
                    break;
                case 'MISSED':
                    missedCountMonths.push(item.month);
                    missedCount++;
                    break;
                case 'DELAYED':
                    delayedCountMonths.push(item.month);
                    delayedCount++;
                    break;
            }
        });
        let paymentStatus = "0 delayed";
        let paymentMonths = "";
        let paymentStatusItems = [];
        let paymentStatusStyle = {color: colors.color_45B448};
        if (missedCount != 0 && delayedCount != 0) {
            paymentStatus = `${missedCount} missed`;
            paymentStatusItems[0] = getRowViews(colors.color_D0021B, paymentStatus);
            paymentStatusItems[1] = getRowViews(colors.color_EEAA00, `\n${delayedCount} delayed`);
            paymentMonths = `${missedCountMonths.join(', ')} ${'\n\n'}${delayedCountMonths.join(', ')}`;
            paymentStatusStyle = {color: colors.color_D0021B};
        } else if (missedCount != 0) {
            paymentStatus = `${missedCount} missed`
            paymentStatusItems[0] = getRowViews(colors.color_D0021B, paymentStatus);
            paymentStatusStyle = {color: colors.color_D0021B};
            paymentMonths = missedCountMonths.join(', ');
        } else if (delayedCount != 0) {
            paymentStatus = `${delayedCount} delayed`;
            paymentStatusItems[0] = getRowViews(colors.color_EEAA00, paymentStatus);
            paymentStatusStyle = {color: colors.color_D0021B};
            paymentMonths = delayedCountMonths.join(', ');
        }
        if (missedCount === 0 && delayedCount === 0) {
            paymentStatusItems[0] = getRowViews(colors.color_45B448, `0 delayed`);
        }

        let item = {
            year: year,
            paymentMonths: paymentMonths,
            paymentStatus: paymentStatusItems,
            paymentStatusStyle: paymentStatusStyle
        }


        return item;
    });

    function getRowViews(textColor, paymentStatus,) {
        return (<View>
            <Text style={[OATextStyle.TextFontSize_14_normal, {
                color: textColor,//colors.color_212121,
                flex: 1
            }]}>{paymentStatus}</Text>
        </View>);
    }


    return itemListData.map((item, index) => {
        // filter data by year
        return (<View key={getUniqueRowID()}>
            <View style={{
                flexDirection: 'row',
                paddingHorizontal: 16,
                paddingVertical: 20
            }}>
                <View style={{flex: 1}}>
                    <Text style={[OATextStyle.TextFontSize_14_normal, {
                        color: colors.color_212121,
                        flex: 1
                    }]}>{item?.year}</Text>
                </View>
                <View style={{flex: 1}}>
                    {item?.paymentStatus}
                </View>
                <View style={{flex: 1}}>
                    <Text style={[OATextStyle.TextFontSize_14_normal, {
                        color: colors.color_212121,
                        flex: 1
                    }]}>{item?.paymentMonths}</Text>
                </View>
            </View>
            <SeparatorView/>
        </View>);
    });
}

export default MissedPayments;
