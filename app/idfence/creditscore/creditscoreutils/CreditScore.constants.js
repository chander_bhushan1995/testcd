import React from "react";
import {OAImageSource} from "../../../Constants/OAImageSource";
import colors from "../../../Constants/colors";

export const CreditScoreConstants = {
    textLabels: {
        PAYMENTS_DONE_ON_TIME: "Payments done on time",
        IMPACT: "IMPACT",
        KNOW_MORE_ACTION_LABEL: "Know More",
        START_NOW_ACTION_LABEL: "Start Now",
        TOTAL_PAYMENTS_IN_36_MONTHS: "Total payments in 36 months",
        DELAYED_PAYMENTS_IN_36_MONTHS: "Delayed payments in 36 months",
        PERCENTAGE_ON_TIME_PAYMENTS: "% on time payments",
        CREDIT_CARDS: "Credit Cards",
        LOANS: "Loans",
        DATATYPE_CARDS: "CARDS",
        DATATYPE_LOANS: "LOANS",
        ACCOUNT_WISE_STATUS: "Account wise status"
    },

    dashboardCardsTemplate: {
        creditDebitCardsFoundCard: {
            placeHolderOne: "Credit/Debit/ID cards found",
            placeHolderTwo: "Start monitoring your credit/debit/id cards to get instant alerts of financial frauds and information leak",
            cardType: "CREDIT_DEBIT_CARDS_FOUND_CARD",
            actionLabel: "Start Now"
        },
        paymentsDoneOnTimeCard: {
            placeHolderOne: "Payments done on time",
            placeHolderTwo: "IMPACT",
            impact: "HIGH",
            cardType: "PAYMENTS_DONE_ON_TIME_CARD",
            actionLabel: "Know More"
        },
        creditLimitUtilizationCard: {
            placeHolderOne: "% of Credit limit utilization",
            placeHolderTwo: "IMPACT",
            impact: "HIGH",
            cardType: "CREDIT_LIMIT_UTILIZATION_CARD",
            actionLabel: "Know More"
        },
        ageOfYourOldestOpenAccountCard: {
            placeHolderOne: "Age of your oldest open account",
            placeHolderTwo: "IMPACT",
            impact: "MEDIUM",
            cardType: "AGE_OF_YOUR_OLDEST_OPEN_ACCOUNT_CARD",
            actionLabel: "Know More"
        },
        totalNoOfCreditCardsAndLoansCard: {
            placeHolderOne: "Total no of credit cards and loans",
            placeHolderTwo: "IMPACT",
            impact: "LOW",
            cardType: "TOTAL_NO_OF_CREDIT_CARDS_AND_LOANS_CARD",
            actionLabel: "Know More"
        },
        creditEnquiriesCard: {
            placeHolderOne: "Credit Enquiries",
            placeHolderTwo: "IMPACT",
            impact: "LOW",
            cardType: "CREDIT_ENQUIRIES_CARD",
            actionLabel: "Know More"
        },
        creditScoreTrendCard: {
            placeHolderOne: "Your credit score trend",
            placeHolderTwo: "Swipe to see more",
            cardType: "CREDIT_SCORE_TREND_CARD"
        },
        contactExperianCard: {
            placeHolderOne: "Found any error in credit score or Account? Contact experian at ",
            mobileNo: "022 6641 9000",
            email: "consumer.support@experian.com",
            cardType: "CONTACT_EXPERIAN_CARD"
        },
        moreAboutIdentityTheftRow: {
            title: "More about Identity Theft",
            imageSource: OAImageSource.chevron_down_right,
            cardType: "MORE_ABOUT_IDENTITY_THEFT_ROW"
        },
        faqsRow: {
            title: "FAQs",
            imageSource: OAImageSource.chevron_down_right,
            cardType: "FAQ_ROW"
        },
        experianLogoFooterCard: {
            title: "Powered By",
            logoImage: "",
            cardType: "EXPERIAN_LOGO_FOOTER_CARD"
        }
    },

    accountTypeKeys: {
        CREDIT_CARD: "CREDIT_CARD",
        LOAN: "LOAN",
    },

    defaultValues: {
        STRING_DEFAULT_1: "",
        STRING_DEFAULT_2: "--",
        STRING_DEFAULT_3: "-",
        INT_DEFAULT: 0,
        JSON_OBJECT_DEFAULT: {},
        JSON_ARRAY_DEFAULT: [],
    },


    creditScoreColorMap: {
        Excellent: {sColor: "#006C00", eColor: colors.color_FFFFFF},
        Good: {sColor: "#20A81E", eColor: colors.color_FFFFFF},
        Fair: {sColor: "#FDAE1E", eColor: colors.color_FFFFFF},
        Doubtful: {sColor: "#F67A00", eColor: colors.color_FFFFFF},
        "Immediate Action Required": {sColor: "#F63B00", eColor: colors.color_FFFFFF}
    },
    paymentHistoryColorMap: {
        Good: {sColor: "#006C00", eColor: colors.color_FFFFFF},
        Fair: {sColor: "#028B00", eColor: colors.color_FFFFFF},
        Bad: {sColor: "#E39300", eColor: colors.color_FFFFFF},
        Poor: {sColor: "#F63B00", eColor: colors.color_FFFFFF}
    },

    tipsToImproveData: {
        PAYMENT_HISTORY: [
            'Pay your pending dues at the earliest',
            'Set up reminders to ensure timely payments',
            'Set up AUTO-BILL pay to avoid payment failure'
        ]
    },
    proTips: {
        PAYMENT_HISTORY: {
            title: "Did you know?",
            detail: "On-time payments increase your credit score. You should avoid late payments to maintain a healthy credit score."
        },
        CREDIT_UTILIZATION: {
            title: "Did you know?",
            detail: "Keeping a low utilization rate helps to increase your credit score. Most experts recommend keeping your overall credit card utilization below 30%."
        }

    }

};
export const IDFENCE_ALERT_TYPE = {
    EMAIL: 'EMAIL',
    TELEPHONE: 'TELEPHONE',
    CARD: 'CARD',
    NATIONAL_ID: 'NATIONAL_ID',
};

export const EMPTY_ERROR_MESSAGES = {
    CREDIT_ENQUIRIES: 'No Credit Enquiry Present',
    CREDIT_UTILISATION: 'You don’t have any active credit card',
    PAYMENT_HISTORY_CARDS: 'You don’t have any active credit card',
    PAYMENT_HISTORY_LOANS: 'You don’t have any active loans'
};

export const CS_ACTION_TYPES = {
    BADGE_VIEW_PRESS: 'BADGE_VIEW_PRESS',
    FAQ_PRESS: 'FAQ_PRESS',
    OPEN_DETAILS_VIEW: 'OPEN_DETAILS_VIEW'
};

