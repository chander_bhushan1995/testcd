import React from "react";
import {Image, ImageBackground, Text, TouchableOpacity, View} from "react-native";
import CircularSlider from "rn-circular-slider";
import styles from "./CreditScoreCircularSliderView.style";
import spacing from "../../Constants/Spacing";
import SeparatorView from "../../CustomComponent/separatorview";
import {OAImageSource} from "../../Constants/OAImageSource";
import {formatCreditScoreLastUpdatedOn} from "../../commonUtil/Formatter";
import {WEBENGAGE_IDFENCE_DASHBOARD} from "../../Constants/WebengageAttrKeys";
import {logWebEnageEvent} from "../../commonUtil/WebengageTrackingUtils";
import colors from "../../Constants/colors";
import {getIDFCommonEventAttr} from '../constants/IDFence.UtilityMethods';

const circularSliderGradientExcellent = [

    {
        stop: '0%',
        color: '#F52600'
    },
    {
        stop: '25%',
        color: '#F68100'
    },
    {
        stop: '50%',
        color: '#E39300'
    },
    {
        stop: '75%',
        color: '#028B00'
    },
    {
        stop: '100%',
        color: '#006C00'
    }
];
const circularSliderGradientGood = [
    {
        stop: '0%',
        color: '#F52600'
    },
    {
        stop: '25%',
        color: '#F68100'
    },
    {
        stop: '65%',
        color: '#F68100'
    },
    {
        stop: '80%',
        color: '#028B00'
    },
    {
        stop: '100%',
        color: '#006C00'
    }
];
const circularSliderGradientFair = [
    {
        stop: '0%',
        color: '#F52600'
    },
    {
        stop: '25%',
        color: '#F68100'
    },
    {
        stop: '50%',
        color: '#E39300'
    },
    {
        stop: '85%',
        color: '#028B00'
    },
    {
        stop: '100%',
        color: '#006C00'
    }
];
const circularSliderGradientDoubtful = [
    {
        stop: '0%',
        color: '#F52600'
    },
    {
        stop: '85%',
        color: '#F68100'
    },
    {
        stop: '100%',
        color: '#028B00'
    }
];
const circularSliderGradientPoor = [
    {
        stop: '0%',
        color: '#F52600'
    },
    {
        stop: '50%',
        color: '#F68100'
    },
    {
        stop: '100%',
        color: '#E39300'
    }
];

const smallCircleColor = {

    excellentColor: {
        "firstColor": "#45B448",
        "secondColor": "#D8D8D8",
    },
    goodColor: {
        "firstColor": "#028B00",
        "secondColor": "#D8D8D8",
    },
    fairColor: {
        "firstColor": "#028B00",
        "secondColor": "#D8D8D8",
    },
    doubtfulColor: {
        "firstColor": "#028B00",
        "secondColor": "#D8D8D8",
    },
    poorColor: {
        "firstColor": "#E39300",
        "secondColor": "#D8D8D8",
    },
    activeColor: {
        "firstColor": "#45B448",
        "secondColor": "#D8D8D8",
    },
    inActiveColor: {
        "firstColor": "#C8C8C8",
        "secondColor": "#C8C8C8",
    }
};
let minScore = 10, maxScore = 90;
export default function CreditScoreCircularSliderView({membershipData, circleValue, customerSummaryData, isCustNotFound, onCreditScoreViewDetailsClick}) {

    return (
        <TouchableOpacity activeOpacity={1} onPress={(e) => onCreditScoreViewDetailsClick(e)}>
            <View style={styles.circularSliderContainerStyle}>

                {/*<Text style={styles.poweredByExperianTitleStyle}>*Powered by Experian</Text>*/}
                <Image style={styles.poweredByExperianImgStyle} source={OAImageSource.experian_credit_score}/>
                <View style={{flex: spacing.spacing1}}>
                    <CircularSlider
                        step={0}
                        min={minScore}
                        max={maxScore}
                        value={circleValue}
                        contentContainerStyle={styles.contentContainerStyle}
                        strokeWidth={10}
                        buttonBorderColor={getSmallCircleColor(customerSummaryData?.CustomerSummaryData?.score?.recentScore).firstColor}
                        buttonFillColor={getSmallCircleColor(customerSummaryData?.CustomerSummaryData?.score?.recentScore).secondColor}
                        buttonStrokeWidth={8}
                        openingRadian={Math.PI / 2.7}
                        buttonRadius={8}
                        linearGradient={getLinearGradient(customerSummaryData?.CustomerSummaryData?.score?.recentScore)}>
                        <ImageBackground style={styles.imageBackgroundStyle}
                                         source={OAImageSource.credit_score_inner_circle}>
                            <View style={styles.imageBackgroundViewStyle}>
                                <Text
                                    style={[styles.creditScoreStyle, {color: getSmallCircleColor(customerSummaryData?.CustomerSummaryData?.score?.recentScore).firstColor}]}>{customerSummaryData?.CustomerSummaryData?.score?.recentScore}</Text>
                                <Text
                                    style={[styles.scoreLabelStyle, {color: getSmallCircleColor(customerSummaryData?.CustomerSummaryData?.score?.recentScore).firstColor}]}>
                                    {(customerSummaryData?.CustomerSummaryData?.score?.scoreLabel === 'Immediate Action Required' ? 'Poor' : customerSummaryData?.CustomerSummaryData?.score?.scoreLabel)}
                                </Text>
                            </View>
                        </ImageBackground>
                    </CircularSlider>
                    <View style={styles.nonClickableStyle}/>
                </View>
                <View style={styles.bottomActionStripContainerStyle}>
                    <SeparatorView/>
                    <View style={styles.bottomActionStripViewStyle}>
                        {prepareCreditScoreMsg()}
                        {creditScoreAction()}
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    );

    function creditScoreAction() {
        let actionTitle = "View Details";
        if (customerSummaryData?.SubscriptionStatus === 'CS_CUSTOMER_IN_PROGRESS') {
            return null;
        }
        if (customerSummaryData?.SubscriptionStatus === 'CS_CUSTOMER_NOT_FOUND') {
            actionTitle = "Retry Activation";
        }

        return (<View
            style={styles.touchableOpacityStyle}>
            <Text style={styles.viewDetailsTitleStyle}>{actionTitle}</Text>
            {/*<Image style={styles.rightArrowImgStyle}
                   source={OAImageSource.right_arrow}/>*/}
        </View>);
    }

    function getLinearGradient(recentScore) {
        if (recentScore > 749) {
            return circularSliderGradientExcellent;
        }
        if (recentScore > 699) {
            return circularSliderGradientGood;
        }
        if (recentScore > 649) {
            return circularSliderGradientFair;
        }
        if (recentScore > 549) {
            return circularSliderGradientDoubtful;
        }
        return circularSliderGradientPoor;
    }

    function getSmallCircleColor(recentScore) {
        if (isCustNotFound) {
            return smallCircleColor.inActiveColor
        }
        if (recentScore > 749) {
            return smallCircleColor.excellentColor;
        }
        if (recentScore > 699) {
            return smallCircleColor.goodColor;
        }
        if (recentScore > 649) {
            return smallCircleColor.fairColor;
        }
        if (recentScore > 549) {
            return smallCircleColor.doubtfulColor;
        }
        return smallCircleColor.poorColor;
    }

    function prepareCreditScoreMsg() {
        let eventAttr = getIDFCommonEventAttr(membershipData);
        let cStatus = "";
        switch (customerSummaryData?.SubscriptionStatus) {
            case 'CS_CUSTOMER_ACTIVE':
                cStatus = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_DASHBOARD_CREDIT_SCORE_CARD_LOAD_ATTR_VALUE.ACTIVE;
                break;
            case 'CS_CUSTOMER_IN_PROGRESS':
                cStatus = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_DASHBOARD_CREDIT_SCORE_CARD_LOAD_ATTR_VALUE.IN_PROCESSING;
                break;
            default:
                cStatus = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_DASHBOARD_CREDIT_SCORE_CARD_LOAD_ATTR_VALUE.NO_SCORE;
        }
        eventAttr[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.STATUS] = cStatus;
        logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_DASHBOARD_CREDIT_SCORE_CARD_LOAD, eventAttr);
        if (customerSummaryData?.SubscriptionStatus === 'CS_CUSTOMER_NOT_FOUND') {
            return (<Text style={styles.errorMsgStyle}>
                We are unable to check your credit score due to some reasons
            </Text>);
        }
        if (customerSummaryData?.SubscriptionStatus === 'CS_CUSTOMER_IN_PROGRESS') {
            return (<Text style={styles.errorMsgStyle}>
                We are fetching your credit score
            </Text>);
        }
        let lastUpdatedOn = customerSummaryData?.CustomerSummaryData?.score?.lastUpdatedOn;
        let scoreDifference = customerSummaryData?.CustomerSummaryData?.score?.scoreDifference;
        if (lastUpdatedOn !== undefined) {
            lastUpdatedOn = new Date(lastUpdatedOn);
            let month = String(lastUpdatedOn.getMonth() + 1);
            let day = String(lastUpdatedOn.getDate());
            let year = String(lastUpdatedOn.getFullYear());
            let updatedOnDate = day + "/" + month + "/" + year;

            if (scoreDifference < 0) {
                return (<Text style={styles.errorMsgStyle}>
                    {`Your credit score decreased by ${(scoreDifference * -1)} points in last month`}
                </Text>);
            } else if (scoreDifference > 0) {
                return (<Text style={[styles.errorMsgStyle, {color: colors.color_38B839}]}>
                    {`Your credit score increased by ${scoreDifference} points in last month`}
                </Text>);
            } else {
                return (<Text style={styles.lastUpdateMsgStyle}>
                    {"Last Updated on: " + formatCreditScoreLastUpdatedOn(updatedOnDate)}
                </Text>);
            }
        }

    };
}
