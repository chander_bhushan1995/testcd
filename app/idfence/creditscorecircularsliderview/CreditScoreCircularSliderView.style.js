import {StyleSheet} from "react-native";
import {CommonStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import {OATextStyle} from "../../Constants/commonstyles";
import colors from "../../Constants/colors";

export default StyleSheet.create({
    poweredByExperianTitleStyle: {
        ...OATextStyle.TextFontSize_8_normal,
        alignSelf: 'flex-end',
        paddingRight: spacing.spacing8
    },
    poweredByExperianImgStyle: {
        alignSelf: 'flex-end',
        width:60,
        height:24,
        marginRight:8
    },
    circularSliderContainerStyle: {
        ...CommonStyle.cardContainerWithShadow,
        borderRadius: spacing.spacing4,
        height: 270,
        paddingTop: spacing.spacing8,
        paddingBottom: spacing.spacing8
    }, contentContainerStyle: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    creditScoreStyle: {
        ...OATextStyle.TextFontSize_20_bold_212121,
        color: colors.color_45B448
    },
    imageBackgroundStyle: {
        width: spacing.spacing80,
        height: spacing.spacing80, justifyContent: 'center'
    },
    imageBackgroundViewStyle: {
        flex: spacing.spacing1,
        alignItems: 'center',
        marginLeft: spacing.spacing12,
        marginRight: spacing.spacing12,
        marginBottom: spacing.spacing8,
        justifyContent: 'center'
    },
    scoreLabelStyle: {
        ...OATextStyle.TextFontSize_8_bold_808080,
        color: colors.color_45B448,
        marginTop: spacing.spacing2
    },
    nonClickableStyle: {
        position: "absolute",
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        justifyContent: "center",
        alignItems: "center",
        opacity: 0.1,
    },
    bottomActionStripContainerStyle: {
        height: spacing.spacing70,
        paddingTop: spacing.spacing10,
        alignItems: 'flex-start'
    },
    bottomActionStripViewStyle: {
        justifyContent: 'center',
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        paddingTop:spacing.spacing5,
        flex: spacing.spacing1,
        alignSelf: 'center',
        flexDirection: 'row'
    },
    touchableOpacityStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    viewDetailsTitleStyle: {
        ...OATextStyle.TextFontSize_12_bold_212121,
        color: colors.color_0282F0
    },
    rightArrowImgStyle: {
        alignSelf: 'center',
        marginLeft: spacing.spacing5,
        marginTop: spacing.spacing2
    },
    errorMsgStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        color: colors.color_D0021B,
        alignSelf: 'center',
        paddingRight: spacing.spacing5,
        flex: spacing.spacing1
    },
    lastUpdateMsgStyle: {
        ...OATextStyle.TextFontSize_12_normal,
        color: colors.color_808080,
        paddingRight: spacing.spacing5,
        alignSelf: 'center',
        flex: spacing.spacing1
    }

});
