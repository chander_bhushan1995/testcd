import DashboardComponent from "./DashboardComponent";
import {connect} from "react-redux";
import {NativeModules} from "react-native";
import {fetchSIStatus, getCustomerSummary, getSubscriptionStatus, Urls} from "../constants/Urls";
import {
    CHECK_CREDIT_SCORE_SUBSCRIPTION_STATUS,
    GET_CREDIT_SCORE_SERVICE_ALERTS_DATA,
    GET_ALERT_TODO,
    GET_ALERTS_SUMMARY,
    GET_ALERTS_TYPE_LIST_DATA,
    GET_ALL_IDFENCE_PLANS,
    GET_ONBOARD_DATA,
    GET_PLAN_BENEFITS_DATA,
    GET_SI_STATUS_DATA, GET_CREDIT_SCORE_ALERTS_TYPE_LIST_DATA,
    GET_CREDIT_SCORE_CUSTOMER_SUMMARY
} from '../constants/actions';
import {
    getDashboardData,
    getOnboardData,
    getDashboardDataFromFirebase,
    showError,
    getDashboardAndCreditScoreAlerts,
    getCreditScoreCustomerSummaryData

} from '../constants/actionCreators';
import { makeGetRequest, makePostRequest } from "../../network/ApiManager";
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

const nativeBridgeRef = NativeModules.ChatBridge;

const mapStateToProps = state => ({
    siData: state.dashboardReducer.siData,
    idFencePlansData: state.dashboardReducer.idFencePlansData,
    dataSource: state.dashboardReducer.data,
    checkSubscriptionStatusData: state.dashboardReducer.checkSubscriptionStatusData,
    creditScoreCustomerSummaryData: state.dashboardReducer.creditScoreCustomerSummaryData,
    alertsData: state.dashboardReducer.alertsData,
    assetData: state.dashboardReducer.assetData,
    detailsData: state.dashboardReducer.detailsData,
    isLoading: state.dashboardReducer.isLoading,
    checkSubscriptionStatusLoading: state.dashboardReducer.checkSubscriptionStatusLoading,
    isCustomerSummaryLoading: state.dashboardReducer.isCustomerSummaryLoading,
    refreshNeeded: state.dashboardReducer.refreshNeeded,
    skippedCards: state.dashboardReducer.skippedCards,
    errorMessage: state.dashboardReducer.errorMessage,
    planBenefitsData: state.dashboardReducer.planBenefitsData,
    bankLevelSafetyData: state.dashboardReducer.bankLevelSafetyData,
    unreadAlertCount: state.dashboardReducer.unreadAlertCount,
    topAlerts: state.dashboardReducer.topAlerts,
    userData: state.dashboardReducer.userData,
    premiumPopupData: state.dashboardReducer.premiumPopupData,
    initialProps: state.dashboardReducer.initialProps ? state.dashboardReducer.initialProps : state.appData.params,
    refreshMembership: state.dashboardReducer.refreshMembership,


});
const mapDispatchToProps = dispatch => ({
    getAllDashboardData: (membershipsData) => {
      let getUrls = [];
      let postUrls = [];

      //get
      let getCustomerInfoUrl =
        Urls.getCustomerInfo +
        membershipsData?.subscriberNo;
      let fetchSIStatusUrl =
        fetchSIStatus(membershipsData?.subscriberNo);
      let getIDFenceAlertTypesUrl =
        Urls.getIDFenceAlertTypes;
      let getCreditCoreServiceAlertTypesUrl =
        Urls.getCreditCoreServiceAlertTypes;
      let alertsSummaryUrl =
        Urls.alertsSummary +
        membershipsData?.subscriberNo;
      let alertTodoUrl =
        Urls.alertTodo +
        membershipsData?.subscriberNo;

      getUrls.push({url: getCustomerInfoUrl, type: GET_ONBOARD_DATA});
      getUrls.push({url: fetchSIStatusUrl, type: GET_SI_STATUS_DATA});
      getUrls.push({url: getIDFenceAlertTypesUrl, type: GET_ALERTS_TYPE_LIST_DATA});
      getUrls.push({url: getCreditCoreServiceAlertTypesUrl, type: GET_CREDIT_SCORE_ALERTS_TYPE_LIST_DATA});
      getUrls.push({url: alertsSummaryUrl, type: GET_ALERTS_SUMMARY});
      getUrls.push({url: alertTodoUrl, type: GET_ALERT_TODO});

        // post
      let getPlanBenefitsUrl =
        Urls.getPlanBenefitsUrl;
            let requestData = {planCode: [membershipsData?.planCode]};

      let suggestPlansUrl =
        Urls.suggestPlans;
      let requestBody = {
        activity: "S",
        businessUnitCode: 0,
        businessUnitType: ["App"],
        disjunctive: true,
        noOfCustomer: 1,
        numberOfPlan: 10,
        priceType: "S",
        recommendationParam: [
          {
            categoryCode: "F",
            serviceList: [
              {
                name: "DARK_WEB_MONITORING",
                value: ""
              }
            ]
          }
        ],
        requireAllPlans: true
      };
            postUrls.push({url: getPlanBenefitsUrl, body: requestData, type: GET_PLAN_BENEFITS_DATA});
            postUrls.push({url: suggestPlansUrl, body: requestBody, type: GET_ALL_IDFENCE_PLANS});

      let promises = [];
      getUrls.map(url => {
        const promise = new Promise((resolve, reject) => {
        makeGetRequest(url.url, (response, error) => {
            if (response) {
                resolve({data: response?.data, type: url.type})
            } else {
                resolve({data: error?.data, type: url.type})
                // reject(error)
            }
          });
        });
        promises.push(promise);
      });

      postUrls.map(url => {
        const promise = new Promise((resolve, reject) => {
            makePostRequest(url.url, url.body, (response, error) => {
                if (response) {
                                resolve({data: response?.data, type: url.type})
                } else {
                  reject(error)
              }
            }
          );
        });
        promises.push(promise);
      });

      Promise.all(promises).then((allData) => {
          // dispatch(getDashboardData(allData, membershipsData));
          let subscriptionStatusUrl =
              getSubscriptionStatus(membershipsData?.subscriberNo);
          let customerSummaryUrl = getCustomerSummary(membershipsData?.subscriberNo);
          makeGetRequest(subscriptionStatusUrl, (response, error) => {
              if (response) {
                  //get customer summary
                  let subscriptionStatus = response.data;
                  makeGetRequest(customerSummaryUrl, (customerSummaryResponse, error) => {
                      if (customerSummaryResponse) {
                          let summaryData = {
                              "SubscriptionStatus": subscriptionStatus,
                              "CustomerSummaryData": customerSummaryResponse.data
                          };
                                let customerSummary = {data: summaryData, type: GET_CREDIT_SCORE_CUSTOMER_SUMMARY};
                          allData.push(customerSummary);
                          dispatch(getDashboardData(allData, membershipsData));
                      } else {
                          //no customer
                          // dispatch(showError(error?.message));
                          let summaryData = {
                              "SubscriptionStatus": subscriptionStatus,
                              "CustomerSummaryData": error.data
                          };
                                let customerSummary = {data: summaryData, type: GET_CREDIT_SCORE_CUSTOMER_SUMMARY};
                          allData.push(customerSummary);
                          dispatch(getDashboardData(allData, membershipsData));
                      }
                  });
              } else {
                  dispatch(showError(error?.message));
              }
          });
      }).catch(error => {
          dispatch(showError(error));
      })
  },
    getOnboardData: membershipsData => {
            let url =
                Urls.getCustomerInfo + membershipsData?.subscriberNo;
            makeGetRequest(url, (response, error) => {
                if (response) {
                    dispatch(getOnboardData(response?.data, membershipsData));
                } else {
                    dispatch(showError(error));
                }
            });
    },
    fetchDashboardAndCreditScoreAlerts: (membershipsData) => {
        let alertsSummaryUrl =
            Urls.alertsSummary +
            membershipsData?.subscriberNo;
        let alertTodoUrl =
            Urls.alertTodo +
            membershipsData?.subscriberNo;
        let alertUrls = [];
        alertUrls.push({url: alertsSummaryUrl, type: GET_ALERTS_SUMMARY});
        alertUrls.push({url: alertTodoUrl, type: GET_ALERT_TODO});

        let promises = [];
        alertUrls.map(url => {
            const promise = new Promise((resolve, reject) => {
                makeGetRequest(url.url, (response, error) => {
                    if (response) {
                        resolve({data: response?.data, type: url.type})
                    } else {
                        reject(error)
                    }
                });
            });
            promises.push(promise);
        });
        Promise.all(promises).then((allAlerts) => {
            dispatch(getDashboardAndCreditScoreAlerts(allAlerts));
        }).catch(error => {
            dispatch(showError(error));
        })
    },

    getCreditScoreCustomerSummary: (membershipsData) => {
            let subscriptionStatusUrl =
                getSubscriptionStatus(membershipsData?.subscriberNo);
            let customerSummaryUrl = getCustomerSummary(membershipsData?.subscriberNo);

            makeGetRequest(subscriptionStatusUrl, (response, error) => {
                if (response) {
                    //get customer summary
                    let subscriptionStatus = response.data;
                    makeGetRequest(customerSummaryUrl, (customerSummaryResponse, error) => {
                        if (customerSummaryResponse) {
                            let summaryData = {
                                "SubscriptionStatus": subscriptionStatus,
                                "CustomerSummaryData": customerSummaryResponse.data
                            };
                            dispatch(getCreditScoreCustomerSummaryData(summaryData));
                        } else {
                            //no customer
                            // dispatch(showError(error?.message));
                        }
                    });
                } else {
                    dispatch(showError(error?.message));
                }
            });
    },

    getDashboardDataFromFirebase: () => {
        nativeBridgeRef.getIDFenceDashboardDetails(json => {
            let jsonObject = parseJSON(json);
            dispatch(getDashboardDataFromFirebase(jsonObject));
        });
    }


});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DashboardComponent);


