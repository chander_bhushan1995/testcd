import React from "react";
import {
    BackHandler,
    Image,
    NativeModules,
    Platform,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import colors from "../../Constants/colors";
import OverviewTab from "./screens/overviewTab/OverviewTabAction";
import AssetsTab from "./screens/assetsTab/AssetsTabAction";
import AlertsTab from "./screens/alertTab/AlertsTab.Action";
import spacing from "../../Constants/Spacing";
import OAActivityIndicator from "../../CustomComponent/OAActivityIndicator";
import DetailsTab from "./screens/detailsTab/DetailsTab.Action";
import ScrollableTabView, {OAScrollableTabBar} from "./tabbar/tabIndex"
import {OATextStyle} from "../../Constants/commonstyles";
import {PLATFORM_OS} from "../../Constants/AppConstants";
import {logIDFenceDashboardAccess} from "../constants/IDFence.UtilityMethods";
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";


import AlertBadgeView from '../../CustomComponent/alertbadgeview';
import {TextStyle} from '../../Constants/CommonStyle';

const nativeBrigeRef = NativeModules.ChatBridge;
let navigator;
let dashboardLoaded: false;
let defaultRadius = 4;
let initialPage = 0;

export default class DashboardComponent extends React.Component {
    static navigationOptions = ({navigation}) => {
        navigator = navigation;
        return {
            headerStyle: {
                borderBottomWidth: 0,
                elevation: 0
            },
            headerTitle: (
                <View style={Platform.OS === 'ios' ? {alignItems: 'center'} : {alignItems: 'flex-start'}}>
                    <Text style={styles.navigationTitle}>ID Fence Dashboard</Text>
                </View>
            ),
            headerLeft: (
                <View style={{flexDirection: "row", alignItems: "center"}}>
                    <TouchableOpacity
                        onPress={() => nativeBrigeRef.goBack()}
                    >
                        <Image
                            source={require("../../../app/images/back_arrow.webp")}
                            style={styles.backNavButtonContainer}
                        />
                    </TouchableOpacity>
                </View>
            ),
            headerTitleStyle: {color: colors.color_FFFFFF},
            headerTintColor: colors.color_FFFFFF
        };
    };

    constructor(props) {
        super(props);

        let navigation = this.props?.navigation;
        this.state = {
            params: navigation?.state?.params,
            membershipData: parseJSON(this.props.initialProps?.membershipData),
            renewalData: parseJSON(this.props.initialProps?.renewalData),
            deeplinkData: parseJSON(this.props.initialProps?.deeplinkData),
        };
        if (this.state.deeplinkData?.relativePath?.includes("/idfence/alerts")) {
            initialPage = 1 //alerts tab
        }
    }

    componentWillMount() {
        logIDFenceDashboardAccess(this.state.membershipData);
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        this.props.getDashboardDataFromFirebase();
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        console.ignoredYellowBox = true;
        console.disableYellowBox = ["WARNING:"];
        this.props.getAllDashboardData(this.state.membershipData);
    }

    render() {
        if (this.props.refreshNeeded) {
            this.props.fetchDashboardAndCreditScoreAlerts(this.state.membershipData);
            this.props.getOnboardData(this.state.membershipData);
            this.props.getCreditScoreCustomerSummary(this.state.membershipData);
        }
        if (this.props.isLoading && !dashboardLoaded) {
            return <OAActivityIndicator/>;
        }
        if (this.props.refreshMembership) {
            if(this.props.initialProps?.membershipData !== null && this.props.initialProps?.membershipData !== undefined) {
                this.props.getAllDashboardData(parseJSON(this.props.initialProps.membershipData));
                return <OAActivityIndicator/>;
            }
        }
        dashboardLoaded = true;
        // if(this.props.errorMessage){
        //     alert(this.props.errorMessage)
        // }
        return (
            <SafeAreaView style={styles.SafeArea}>
                <ScrollableTabView
                    style={{marginTop: 0,}}
                    initialPage={initialPage}
                    ref={(tabView) => {
                        if (tabView != null) {
                            this.tabView = tabView;
                        }
                    }}
                    renderTabBar={() => (
                        <OAScrollableTabBar
                            tabsContainerStyle={styles.tabContainerView}
                            underlineStyle={styles.tabBarUnderlineView}
                            textStyle={styles.tabBarTextStyle}
                            activeTextColor={colors.blue028}
                            inactiveTextColor={colors.grey}
                            containerStyle={styles.containerStyle}
                            badgeStyle={styles.badgeStyle}
                            pageWithBadge={1}
                            alertCount={this.getAlertBadgeCount()}

                        />
                    )}
                >
                    <OverviewTab
                        tabLabel="Overview"
                        gotoTab={(pageNumber) => this.gotoTab(pageNumber)}
                        navigation={this.props.navigation}
                        renewalData={this.state.renewalData}
                        deeplinkData={this.state.deeplinkData}
                        alertTypeList={this.props.alertsData?.alertsTypeList}

                    />
                    <AlertsTab
                        tabLabel="Alerts"
                        idFencePlansData={this.props.idFencePlansData}
                        dataSource={this.props.alertsData}
                        initialProps={this.props.initialProps}
                        renewalData={this.state.renewalData}
                    />
                    <AssetsTab
                        tabLabel="Assets"
                        initialProps={this.props.initialProps}
                        navigation={this.props.navigation}
                        renewalData={this.state.renewalData}
                    />
                    <DetailsTab tabLabel="Details"
                                initialProps={this.props.initialProps}
                                dataSource={this.props.detailsData}
                                otherData={{
                                    planBenefitsData: this.props.planBenefitsData,
                                    siData: this.props.siData,
                                    renewalData: this.state.renewalData
                                }}
                                navigation={this.props.navigation}/>

                </ScrollableTabView>
            </SafeAreaView>
        );
    }
    gotoTab = (pageNumber) => {
        this.tabView?.goToPage(pageNumber);
    };
    onBackPress = () => {
        nativeBrigeRef.goBack();
    };


    getAlertBadgeCount = () => {
        let unreadAlertCount = this.props.unreadAlertCount;
        if (unreadAlertCount !== null && unreadAlertCount !== undefined && unreadAlertCount !== 0) return unreadAlertCount;
        return 0;
    }
}


const styles = StyleSheet.create({
    SafeArea: {
        flex: 1,
        backgroundColor: "white"
    },
    backNavButtonContainer: {
        width: spacing.spacing16,
        height: spacing.spacing16,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing12,
        bottom: spacing.spacing0
    },
    navigationTitle: {
        ...OATextStyle.TextFontSize_18_bold_212121
    },
    tabBarTextStyle: {
        fontSize: 13,
        fontFamily: "Lato-Semibold",
        lineHeight: 20,
        position: 'absolute'
    },
    tabBarUnderlineView: {
        backgroundColor: colors.blue028,
        height: spacing.spacing2,
        borderRadius: spacing.spacing2
    },
    tabContainerView: {
        borderBottomColor: colors.grey,
    },
    containerStyle: {
        shadowColor: colors.grey,
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowRadius: defaultRadius,
        shadowOpacity: 0.72,
        backgroundColor: colors.color_FFFFFF,
        borderRadius: defaultRadius,
        elevation: defaultRadius,
    },
    badgeStyle: {
        margin: spacing.spacing1,
        borderRadius: defaultRadius,
        elevation: defaultRadius,
    }
});
