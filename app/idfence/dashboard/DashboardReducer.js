import {
    CHECK_CREDIT_SCORE_SUBSCRIPTION_STATUS,
    GET_ALERT_TODO,
    GET_ALERTS_SUMMARY,
    DASHBOARD_AND_CREDIT_SCORE_ALERTS,
    GET_ALERTS_TYPE_LIST_DATA,
    GET_ALL_IDFENCE_PLANS,
    GET_CREDIT_SCORE_ALERTS_TYPE_LIST_DATA,
    GET_CREDIT_SCORE_CUSTOMER_SUMMARY,
    GET_DASHBOARD_DATA_FROM_FIREBASE,
    GET_ONBOARD_DATA,
    GET_PLAN_BENEFITS_DATA,
    GET_SI_STATUS_DATA,
    REFRESH_TAB_BAR,
    SHOW_ERROR,
    SKIP_CARD,
    GET_DASHBOARD_DATA, REFRESH_DASHBOARD
} from '../constants/actions';
import React from "react";
import * as DashboardUtils from "./DashboardUtils"
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

const initialState = {
    skippedCards: new Set([]),
    isLoading: true,
    planBenefitsData: {},
    siData: {},
    alertsData: {},
    checkSubscriptionStatusData: "",
    checkSubscriptionStatusLoading: true,
    isCustomerSummaryLoading: true,
    creditScoreCustomerSummaryData: {"SubscriptionStatus": "", "CustomerSummaryData": {}},
    idFencePlansData: {"trialPlan": {}, "premiumPlan": {}},
    subscriberNo: null,
    errorMessage: null,
    userData: {},
    dataFromFirebase: {},
    blogsData: {}
};

const dashboardReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_DASHBOARD_DATA:

            let allData = action.data;
            let membershipsData = action?.membershipsData;
            let userData = null;
            //data to be updated
            let dashboardData = null;
            let siData = null;

            let alertsSummary = null;
            let unreadAlertCountCreditScore = null;

            let alertCards = null;
            let unreadAlertCountDashboard = null;

            let planBenefitsData = null;
            let idFencePlansData = {"trialPlan": {}, "premiumPlan": {}};
            let alertsData = {"alertsTypeList": {}};
            let creditScoreCustomerSummaryData = null;
            allData.filter(data => {
                if (data.type === GET_SI_STATUS_DATA) {
                    siData = data.data;
                }
            });
            allData.filter(data => {
                if (data.type === GET_ALL_IDFENCE_PLANS) {
                    data.data.map(item => {
                        if (item["trial"]) {
                            idFencePlansData["trialPlan"] = item;
                        }
                        if (!item["trial"]) {
                            idFencePlansData["premiumPlan"] = item;
                        }
                    });
                }
            });

            allData.map((data) => {
                switch (data.type) {
                    case GET_ONBOARD_DATA:
                        dashboardData = DashboardUtils.getOnboardData(state.dataFromFirebase, data.data, state.skippedCards,
                            membershipsData, siData, idFencePlansData, state.blogsData);
                        userData = data.data;
                        break;
                    case GET_SI_STATUS_DATA:
                        siData = data.data;
                        break;
                    case GET_ALERTS_TYPE_LIST_DATA:
                    case GET_CREDIT_SCORE_ALERTS_TYPE_LIST_DATA:
                        data.data.map(item => {
                            if (data.type === GET_CREDIT_SCORE_ALERTS_TYPE_LIST_DATA) {
                                item.alertCategory = "CREDIT_SCORE";
                            } else {
                                item.alertCategory = "NORMAL";
                            }
                            alertsData.alertsTypeList[item.alertType] = item;
                        });
                        break;
                    case GET_ALERTS_SUMMARY:
                        alertsSummary = data?.data?.unreadTopFiveAlerts;
                        unreadAlertCountCreditScore = data?.data?.unreadAlertCount ?? 0;
                        break;
                    case GET_ALERT_TODO:
                        alertCards = data?.data?.alertCards;
                        unreadAlertCountDashboard = data?.data?.unreadAlertCount ?? 0;
                        break;
                    case GET_PLAN_BENEFITS_DATA:
                        planBenefitsData = data.data;
                        break;
                    case GET_ALL_IDFENCE_PLANS:
                        data.data.map(item => {
                            if (item["trial"]) {
                                idFencePlansData["trialPlan"] = item;
                            }
                            if (!item["trial"]) {
                                idFencePlansData["premiumPlan"] = item;
                            }
                        });
                        break;
                    case GET_CREDIT_SCORE_CUSTOMER_SUMMARY:
                        creditScoreCustomerSummaryData = data.data;
                        break;
                }
            });
            return {
                ...state,
                data: dashboardData?.data,
                assetData: dashboardData?.assetData,
                detailsData: dashboardData?.detailsData,
                bankLevelSafetyData: dashboardData?.bankLevelSafetyData,
                premiumPopupData: dashboardData?.premiumPopupData,
                idFencePlansData: idFencePlansData,
                planBenefitsData: planBenefitsData,
                topAlerts: DashboardUtils.getTopAlerts(alertCards, alertsSummary),
                unreadAlertCount: unreadAlertCountDashboard + unreadAlertCountCreditScore,
                siData: siData,
                isLoading: false,
                alertsData: alertsData,
                userData: userData,
                membershipsData: membershipsData,
                creditScoreCustomerSummaryData: creditScoreCustomerSummaryData,
                refreshMembership: false
            };


        case GET_DASHBOARD_DATA_FROM_FIREBASE:
            return {
                ...state,
                dataFromFirebase: parseJSON(action.firebaseData?.id_fence_dashboard_details),
                blogsData: parseJSON(action.firebaseData?.blogsData),
                errorMessage: null
            };
        case GET_ONBOARD_DATA:
            let onboardData = DashboardUtils.getOnboardData(state.dataFromFirebase, action.data, state.skippedCards,
                action.membershipsData, state.siData, state.idFencePlansData, state.blogsData);

            return {
                ...state,
                data: onboardData?.data,
                assetData: onboardData?.assetData,
                detailsData: onboardData?.detailsData,
                bankLevelSafetyData: onboardData?.bankLevelSafetyData,
                premiumPopupData: onboardData?.premiumPopupData,
                isLoading: false,
                refreshNeeded: false,
                userData: action.data,
                membershipsData: action.membershipsData,
                refreshMembership: false
            };
        case GET_CREDIT_SCORE_ALERTS_TYPE_LIST_DATA:
        case GET_ALERTS_TYPE_LIST_DATA:
            if (state.alertsData.alertsTypeList === undefined) {
                state.alertsData.alertsTypeList = {};
            }
            action.data.map(item => {
                if (action.type === GET_CREDIT_SCORE_ALERTS_TYPE_LIST_DATA) {
                    item.alertCategory = "CREDIT_SCORE";
                } else {
                    item.alertCategory = "NORMAL";
                }
                state.alertsData.alertsTypeList[item.alertType] = item;
            });
            return {
                ...state,
                alertsData: state.alertsData,
                errorMessage: null
            };
        case SHOW_ERROR:
            return {
                ...state,
                errorMessage: action.errorMessage,
                refreshNeeded: false,
                isLoading: false
            };
        case REFRESH_TAB_BAR: {
            return {
                ...state,
                isLoading: true,
                refreshNeeded: true,
                errorMessage: null
            };
        }
        case SKIP_CARD: {
            if (state.skippedCards.has(action.cardType)) {
                state.skippedCards = new Set([]);
            }
            state.skippedCards.add(action.cardType);
            let onboardData = DashboardUtils.getOnboardData(state.dataFromFirebase, state.userData, state.skippedCards,
                state.membershipsData, state.siData, state.idFencePlansData, state.blogsData);

            return {
                ...state,
                refreshNeeded: false,
                errorMessage: null,
                data: onboardData?.data,
            };
        }
        // case CHECK_CREDIT_SCORE_SUBSCRIPTION_STATUS: {
        //     state.creditScoreCustomerSummaryData.SubscriptionStatus = action.data;
        //     return {
        //         ...state,
        //         checkSubscriptionStatusLoading: false,
        //         isCustomerSummaryLoading: (action.data === 'CS_CUSTOMER_ACTIVE'),
        //         checkSubscriptionStatusData: action.data,
        //         creditScoreCustomerSummaryData: state.creditScoreCustomerSummaryData,
        //         errorMessage: null
        //     };
        // }
        case GET_CREDIT_SCORE_CUSTOMER_SUMMARY: {
            return {
                ...state,
                creditScoreCustomerSummaryData: action.data,
            };
        }

        case DASHBOARD_AND_CREDIT_SCORE_ALERTS:
            let alertsSummaryRefreshed = null;
            let unreadAlertCountCreditScoreRefreshed = null;

            let alertCardsRefreshed = null;
            let unreadAlertCountDashboardRefreshed = null;

            action.data?.map((data) => {
                switch (data.type) {
                    case GET_ALERTS_SUMMARY:
                        alertsSummaryRefreshed = data?.data?.unreadTopFiveAlerts;
                        unreadAlertCountCreditScoreRefreshed = data?.data?.unreadAlertCount ?? 0;
                        break;
                    case GET_ALERT_TODO:
                        alertCardsRefreshed = data?.data?.alertCards;
                        unreadAlertCountDashboardRefreshed = data?.data?.unreadAlertCount ?? 0;
                        break;
                }
            });

            return {
                ...state,
                topAlerts: DashboardUtils.getTopAlerts(alertCardsRefreshed, alertsSummaryRefreshed),
                unreadAlertCount: unreadAlertCountDashboardRefreshed + unreadAlertCountCreditScoreRefreshed,
            };

        case REFRESH_DASHBOARD:
            return {
                ...state,
                isLoading: true,
                initialProps: action.data,
                refreshMembership: true
            };
        default:
            return {...state};
    }
};

export default dashboardReducer;
