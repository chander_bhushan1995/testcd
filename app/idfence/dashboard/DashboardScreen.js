import React from "react";
import {Platform, SafeAreaView, ScrollView, StyleSheet, Text, View} from "react-native";
import {PLATFORM_OS} from "../../Constants/AppConstants";
import {TextStyle} from "../../Constants/CommonStyle";
import {HeaderBackButton} from "react-navigation";
import TabBar from "../../CustomComponent/tabbar/TabBar";
import AlertBadgeView from '../../CustomComponent/alertbadgeview';
import WarningViewWithAction from '../../CustomComponent/warningviewwithaction';
import IDFencePremiumPlanView from '../../CustomComponent/idfencepremiumplanview';

const idVieww=3;
export default class DashboardScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        <WarningViewWithAction />
        return {
            headerTitle: (
                <View
                    style={
                        Platform.OS === PLATFORM_OS.ios
                            ? { alignItems: "center" }
                            : { alignItems: "flex-start" }
                    }
                >
                    <Text style={TextStyle.text_16_bold}>Okay</Text>
                </View>
            ),
            headerLeft: (
                <HeaderBackButton
                    tintColor={"black"}
                    onPress={() => {
                        navigation.pop();
                    }}
                />
            ),
            headerTitleStyle: { color: "white" },
            headerTintColor: "white"
        };
    };
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <SafeAreaView style={styles.SafeArea}>
                <TabBar/>
                <ScrollView style={styles.scrollViewStyle}>
                    <AlertBadgeView count="5"/>
                    <IDFencePremiumPlanView/>
                    {/*<IDFencePremiumPlanView/>*/}
                    {/*<WarningViewWithAction  label="Connect Now" onClick={() => onBtnClick(idVieww)}/>*/}
                    {/*<View style={styles.cardShadow}>
                        <AlertsOverviewCard data={data} />
                    </View>*/}
                </ScrollView>
            </SafeAreaView>
        );
    }
}
const onBtnClick = (id) => {

}

const styles = StyleSheet.create({
    SafeArea: {
        flex: 1,
        backgroundColor: "white"
    },
    scrollViewStyle: {
        flex: 1,
        paddingBottom: 32,
        backgroundColor: "white"
    },
    cardShadow: {
        padding: 16,
        shadowColor: "black",
        borderColor: "black",
        backgroundColor: "red"
    }
});

// import * as React from "react";
// import { View, StyleSheet, Dimensions } from "react-native";
// import { TabView, SceneMap } from "react-native-tab-view";

// const FirstRoute = () => (
//   <View style={[styles.scene, { backgroundColor: "#ff4081" }]} />
// );

// const SecondRoute = () => (
//   <View style={[styles.scene, { backgroundColor: "#673ab7" }]} />
// );

// export default class DashboardScreen extends React.Component {
//   state = {
//     index: 0,
//     routes: [
//       { key: "first", title: "First" },
//       { key: "second", title: "Second" }
//     ]
//   };

//   render() {
//     return (
//       <TabView
//         navigationState={this.state}
//         renderScene={SceneMap({
//           first: FirstRoute,
//           second: SecondRoute
//         })}
//         onIndexChange={index => this.setState({ index })}
//         initialLayout={{ width: Dimensions.get("window").width }}
//       />
//     );
//   }
// }

// const styles = StyleSheet.create({
//   scene: {
//     flex: 1
//   }
// });
