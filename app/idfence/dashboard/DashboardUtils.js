import {
    addMoreAssetList,
    AllAssetKeys,
    AssetsCategories,
    GovIdKeys,
    OverviewCards,
    SkippableCards,
    SocialKeys,
    AllTodoCards,
    PlanStatus
} from "../constants/Constants";

import colors from "../../Constants/colors";
import Validate from "../../commonUtil/DashboardAssetsValidation";
import {OAImageSource} from "../../Constants/OAImageSource";
import {getFirstItemFromArray, sortAlphabetDigits} from "../constants/IDFence.UtilityMethods";


export const getOnboardData = (
    dashboardDataFirebase,
    dashboardDataRemote,
    skippedCards,
    membershipsData,
    siData,
    idFencePlansData,
    blogsData
) => {

    /*
    *
    * Trial expiring popup
    *
    *
    */
    let idFencePremiumPlan = idFencePlansData.premiumPlan;
    let planDetail = {
        customerName: dashboardDataRemote?.customerInfo?.firstName,
        offerPrice: idFencePremiumPlan?.price,
        oldPrice: idFencePremiumPlan?.anchorPrice,
        daysRemaining: parseInt(membershipsData?.memRemainingDays),
        isTrial: membershipsData?.isTrial,
        planStatus: getPlanStatus(parseInt(membershipsData?.memRemainingDays), membershipsData?.isTrial,
            siData, membershipsData?.isSIEnabled)
    };
    let premiumPopupData = getTrialExpiringAlertData(planDetail);


    /*
     *
     *  Warning cell
     *
     *
     */
    //Todo: create other warning cards too
    let warningData = null;

    warningData = dashboardDataFirebase.warningData;
    warningData.type = SkippableCards.WARNING;

    if (
        dashboardDataFirebase?.socialMediaData?.facebook?.status !==
        "DISCONNECTED_BY_USER" ||
        skippedCards.includes(SkippableCards.WARNING)
    ) {
        warningData = null;
    }

    /*
     *
     * IDFencePremiumPlanCell
     *
     * */
    let benefitPointsArr = [
        "Check if someone took a loan on behalf of you",
        "Track credit score enquiries"
    ];
    let idfencePremiumPlanCellData = {
        benefitPoints: benefitPointsArr,
        linkButtonActionLabel: "Know more",
        strikeThroughAmount: "₹ 480",
        cellTitle: "Save upto ₹ 480 yearly on premium plan",
        actionType: "OPEN_KNOWN_MORE",
        cellSubTitle:
            "Buy ID Fence premium plan to avail free credit score monitoring"
    };


    /*
     *
     *
     * Assets
     *
     *
     * */

    let assetData = [];
    let addMoreAssetsData = [];
    /*
     *
     * asset email
     *
     * */
    let numTotalEmails = dashboardDataRemote?.emailInfo?.numTotalEmails;
    let numAddedEmails = dashboardDataRemote?.emailInfo?.numAddedEmails;
    let emails = dashboardDataRemote?.emailInfo?.emails;
    let nonEmptyEmails = [];
    let userWarningData = [];
    let emailWarnings = [];

    let userPrimaryEmail = dashboardDataRemote?.customerInfo?.email;


    let emptyEmail = [];
    Object.keys(emails).map(key => {
        if (emails?.[key]?.status) {
            let addedEmail = {
                title: emails?.[key]?.emailId,
                imageSource: OAImageSource.delete,
                webEngageValue: 'Email',
                cyberDataElement: "EMAIL",
                indexKey: key
            };
            if (userPrimaryEmail === emails?.[key]?.emailId) {
                addedEmail["subtitle"] = "(Primary)";
            }
            nonEmptyEmails.push(addedEmail);
            if (emails?.[key]?.status == "VERIFICATION_PENDING") {
                let warningData = {
                    "key": "Email",
                    "webEngageValue": "Verify Email",
                    "status": emails?.[key]?.status,
                    "displayName": emails?.[key]?.emailId
                };
                userWarningData.push(warningData);
                emailWarnings.push(warningData);
            }
        }
        if (emails?.[key]?.status === null) {
            emptyEmail.push(key);
        }
    });

    let firstEmptyEmail = getFirstItemFromArray(emptyEmail.sort(sortAlphabetDigits));

    if (numAddedEmails < numTotalEmails && firstEmptyEmail !== '') {

        addMoreAssetList.email = {
            title: "Email ID",
            type: AllAssetKeys.EMAIL,
            errorMessage: "Email ID is mandatory",
            maxLength: 100,
            placeHolder: "abc@xyz.com",
            placeHolderColor: colors.grey,
            isMultiLines: false,
            keyboardType: "email-address",
            btnText: "Add Now",
            validationMethodRef: Validate.email,
            validationErrorMessage: "Please enter a valid Email Id",
            element: "EMAIL",
            dataKey: "email",
            indexKey: firstEmptyEmail,
            cardType: null,
            planStatus: planDetail.planStatus
        };
    }

    let assetEmail = {
        type: AssetsCategories.EMAIL,
        title: "Email Address (" + numAddedEmails + "/" + numTotalEmails + ")",
        imageSource: OAImageSource.chevron_down,
        emails: nonEmptyEmails,
        emailWarnings:emailWarnings,
        numTotalEmails: numTotalEmails,
        numAddedEmails: numAddedEmails,
        addMore: (firstEmptyEmail !== '' ? addMoreAssetList.email : null)
    };
    assetData.push(assetEmail);

    /*
     *
     * asset Mobile
     *
     *
     * */
    let numTotalTelephones =
        dashboardDataRemote?.telephoneInfo?.numTotalTelephones;
    let numAddedTelephones =
        dashboardDataRemote?.telephoneInfo?.numAddedTelephones;
    let telephones = dashboardDataRemote?.telephoneInfo?.telephones;
    let nonEmptyTelephones = [];
    let emptyTelephones = [];

    Object.keys(telephones).map(key => {
        if (telephones?.[key]?.status) {
            let addedPhone = {
                title: "+91 " + telephones?.[key]?.telephoneNo,
                imageSource: OAImageSource.delete,
                webEngageValue: 'Mobile',
                cyberDataElement: "TELEPHONE",
                indexKey: key
            };
            nonEmptyTelephones.push(addedPhone);
        }
        if (telephones?.[key]?.status === null) {
            emptyTelephones.push(key);
        }
    });
    let firstEmptyTelephone = getFirstItemFromArray(emptyTelephones.sort(sortAlphabetDigits));

    if (numAddedTelephones < numTotalTelephones && firstEmptyTelephone !== '')
        addMoreAssetList.mobile = {
            title: "Mobile Number",
            type: AllAssetKeys.MOBILE,
            errorMessage: "Mobile number is mandatory",
            placeHolder: "",
            maxLength: 10,
            placeHolderColor: colors.grey,
            isMultiLines: false,
            keyboardType: "number-pad",
            btnText: "Add Now",
            validationMethodRef: Validate.telephone,
            validationErrorMessage: "Please enter a valid Mobile number",
            element: "TELEPHONE",
            dataKey: "telephoneNo",
            indexKey: firstEmptyTelephone,
            cardType: null,
            planStatus: planDetail.planStatus

        };

    let assetMobile = {
        type: AssetsCategories.MOBILE,
        title:
            "Mobile number (" + numAddedTelephones + "/" + numTotalTelephones + ")",
        imageSource: OAImageSource.chevron_down,
        telephones: nonEmptyTelephones,
        numTotalTelephones: numTotalTelephones,
        numAddedTelephones: numAddedTelephones,
        addMore: (firstEmptyTelephone !== '' ? addMoreAssetList.mobile : null)
    };
    assetData.push(assetMobile);

    // Add more Mobile asset

    let mobileData = dashboardDataFirebase.mobileData;
    mobileData.imageSource = OAImageSource.mobile;
    mobileData.addMore = assetMobile.addMore;

    /*
     *
     * asset credit & debit cards
     *
     *
     * */
    let numTotalCards = dashboardDataRemote?.cardInfo?.numTotalCards;
    let numAddedCards = dashboardDataRemote?.cardInfo?.numAddedCards;
    let cards = dashboardDataRemote?.cardInfo?.cards;
    let nonEmptyCards = [];
    let emptyCards = [];

    Object.keys(cards).map(key => {
        let card = cards?.[key];
        if (card?.status === "ACTIVE") {
            let addedCard = {
                title: card?.cardNo,
                imageSource: OAImageSource.delete,
                //logoImageSource: card?.cardType === "CC" ? OAImageSource.MasterCard : OAImageSource.MasterCard,
                webEngageValue: card?.cardType,
                cyberDataElement: "CARD",
                indexKey: key
            };
            nonEmptyCards.push(addedCard);
        }
        if (card?.status === null) {
            emptyCards.push(key);
        }
    });
    let firstEmptyCard = getFirstItemFromArray(emptyCards.sort(sortAlphabetDigits));


    if (numAddedCards < numTotalCards && firstEmptyCard !== '')
        addMoreAssetList.creditAndDebit = {
            title: "Card Number",
            type: AllAssetKeys.CREDIT_DEBIT,
            errorMessage: "Card number is mandatory",
            placeHolder: "",
            maxLength: 21,
            placeHolderColor: colors.grey,
            isMultiLines: false,
            keyboardType: "number-pad",
            btnText: "Add Now",
            validationErrorMessage: "Please enter a valid card number",
            validationMethodRef: Validate.card,
            element: "CARD",
            dataKey: "cardNo",
            indexKey: firstEmptyCard,
            cardType: "CC",
            planStatus: planDetail.planStatus
        };
    let assetCCDB = {
        type: AssetsCategories.CREDIT_DEBIT,
        title: "Credit & Debit Cards (" + numAddedCards + "/" + numTotalCards + ")",
        imageSource: OAImageSource.chevron_down,
        cards: nonEmptyCards,
        numTotalCards: numTotalCards,
        numAddedCards: numAddedCards,
        addMore: (firstEmptyCard !== '' ? addMoreAssetList.creditAndDebit : null)
    };
    assetData.push(assetCCDB);

    /*
     *
     * assets Government IDs
     *
     *
     * */

    addMoreAssetList.pan = {
        title: "PAN Number",
        type: AllAssetKeys.PAN_CARD,
        errorMessage: "PAN number is mandatory",
        placeHolder: "",
        maxLength: 15,
        autoCapitalize: 'words',
        placeHolderColor: colors.grey,
        isMultiLines: false,
        keyboardType: "default",
        btnText: "Add Now",
        validationErrorMessage: "Please enter a valid PAN number",
        validationMethodRef: Validate.pan,
        element: "NATIONAL_ID",
        dataKey: "nationalId",
        indexKey: "PAN",
        cardType: null,
        planStatus: planDetail.planStatus
    };

    addMoreAssetList.dl = {
        title: "Driving License Number",
        type: AllAssetKeys.DRIVING_LICENCE,
        errorMessage: "License number is mandatory",
        placeHolder: "",
        maxLength: 30,
        autoCapitalize: 'words',
        placeHolderColor: colors.grey,
        isMultiLines: false,
        keyboardType: "default",
        btnText: "Add Now",
        validationErrorMessage: "Please enter a valid license number",
        validationMethodRef: Validate.dl,
        element: "NATIONAL_ID",
        dataKey: "nationalId",
        indexKey: "DL",
        cardType: null,
        planStatus: planDetail.planStatus
    };
    addMoreAssetList.passport = {
        title: "Passport Number",
        type: AllAssetKeys.PASSPORT,
        errorMessage: "Passport number is mandatory",
        placeHolder: "",
        maxLength: 8,
        autoCapitalize: 'words',
        placeHolderColor: colors.grey,
        isMultiLines: false,
        keyboardType: "default",
        btnText: "Add Now",
        validationErrorMessage: "Please enter a valid passport number",
        validationMethodRef: Validate.passport,
        element: "NATIONAL_ID",
        dataKey: "nationalId",
        indexKey: "PASSPORT",
        cardType: null,
        planStatus: planDetail.planStatus
    };

    // Gov IDs carousel on overview tab

    let govIdsData = dashboardDataFirebase.govIds;
    let pan = govIdsData?.pan;
    pan.imageSource = OAImageSource.pan_logo;
    pan.addMore = addMoreAssetList.pan;

    let passport = govIdsData?.passport;
    passport.imageSource = OAImageSource.passport_logo;
    passport.addMore = addMoreAssetList.passport;

    let drivingLicence = govIdsData?.drivingLicence;
    drivingLicence.imageSource = OAImageSource.dl_logo;
    drivingLicence.addMore = addMoreAssetList.dl;
    let govIDsCarouselData = [];


    //Gov assets
    let numTotalNationalIds =
        dashboardDataRemote?.nationalIdInfo?.numTotalNationalIds;
    let numAddedNationalIds =
        dashboardDataRemote?.nationalIdInfo?.numAddedNationalIds;
    let nationalIds = dashboardDataRemote?.nationalIdInfo?.nationalIds;
    let nonEmptyNationalIds = [];
    Object.keys(nationalIds).map(key => {
        switch (key) {
            case GovIdKeys.PAN_CARD:
                let panAsset = null;
                if (nationalIds?.[key]?.status) {
                    //already added
                    addMoreAssetList.pan = null;
                    panAsset = {
                        title: nationalIds?.[key]?.nationalIdNo + " (PAN Card)",
                        isAlreadyAdded: true,
                        imageSource: OAImageSource.delete,
                        webEngageValue: "PAN",
                        cyberDataElement: "NATIONAL_ID",
                        indexKey: key

                    };
                } else {
                    govIDsCarouselData.push(pan);
                    panAsset = {
                        title: "PAN Card",
                        isAlreadyAdded: false,
                        addMore: addMoreAssetList.pan
                    };
                }
                nonEmptyNationalIds.push(panAsset);
                break;
            case GovIdKeys.DRIVING_LICENCE:
                let dlAsset = null;
                if (nationalIds?.[key]?.status) {
                    addMoreAssetList.dl = null;
                    //already added
                    dlAsset = {
                        title: nationalIds?.[key]?.nationalIdNo + " (Driving License)",
                        isAlreadyAdded: true,
                        webEngageValue: "DL",
                        imageSource: OAImageSource.delete,
                        cyberDataElement: "NATIONAL_ID",
                        indexKey: key
                    };
                } else {
                    govIDsCarouselData.push(drivingLicence);
                    dlAsset = {
                        title: "Driving License",
                        isAlreadyAdded: false,
                        addMore: addMoreAssetList.dl
                    };
                }
                nonEmptyNationalIds.push(dlAsset);
                break;
            case GovIdKeys.PASSPORT:
                let passportAsset = null;
                if (nationalIds?.[key]?.status) {
                    //already added
                    addMoreAssetList.passport = null;
                    passportAsset = {
                        title: nationalIds?.[key]?.nationalIdNo + " (Passport)",
                        isAlreadyAdded: true,
                        webEngageValue: "Passport",
                        imageSource: OAImageSource.delete,
                        cyberDataElement: "NATIONAL_ID",
                        indexKey: key
                    };
                } else {
                    govIDsCarouselData.push(passport);
                    passportAsset = {
                        title: "Passport",
                        isAlreadyAdded: false,
                        addMore: addMoreAssetList.passport
                    };
                }
                nonEmptyNationalIds.push(passportAsset);
                break;
        }
    });

    let assetGovIds = {
        type: AssetsCategories.GOVERNMENT_IDS,
        title:
            "Government IDs (" +
            numAddedNationalIds +
            "/" +
            numTotalNationalIds +
            ")",
        imageSource: OAImageSource.chevron_down,
        nationalIds: nonEmptyNationalIds,
        numTotalNationalIds: numTotalNationalIds,
        numAddedNationalIds: numAddedNationalIds
    };
    assetData.push(assetGovIds);


    /*
     *
     * Bank cards
     *
     *
     * */
    let numTotalBankAccounts =
        dashboardDataRemote?.bankAccInfo?.numTotalBankAccounts;
    let numAddedBankAccounts =
        dashboardDataRemote?.bankAccInfo?.numAddedBankAccounts;
    let bankAccounts = dashboardDataRemote?.bankAccInfo?.bankAccounts;
    let nonEmptyBankAccounts = [];
    let emptyBankAccounts = [];
    Object.keys(bankAccounts).map(key => {

        if (bankAccounts?.[key]?.status) {
            let addedBankAcc = {
                title: bankAccounts?.[key]?.bankAccountNo,
                imageSource: OAImageSource.delete,
                webEngageValue: "Bank Accounts",
                cyberDataElement: "BANK",
                indexKey: key
            };
            nonEmptyBankAccounts.push(addedBankAcc);
        }
        if (bankAccounts?.[key]?.status === null) {
            emptyBankAccounts.push(key);
        }
    });
    let firstEmptyBankAccount = getFirstItemFromArray(emptyBankAccounts.sort(sortAlphabetDigits));

    if (numAddedBankAccounts < numTotalBankAccounts && firstEmptyBankAccount !== '')
        addMoreAssetList.bank = {
            title: "Account Number",
            type: AllAssetKeys.BANK_ACCOUNT,
            errorMessage: "Bank account number is mandatory",
            placeHolder: "",
            maxLength: 21,
            placeHolderColor: colors.grey,
            isMultiLines: false,
            keyboardType: "number-pad",
            btnText: "Add Now",
            validationErrorMessage: "Please enter a valid bank account number",
            validationMethodRef: Validate.bankAccount,
            element: "BANK",
            dataKey: "bankAccountNo",
            indexKey: firstEmptyBankAccount,
            cardType: null,
            planStatus: planDetail.planStatus
        };
    let assetBank = {
        type: AssetsCategories.BANK_ACCOUNT,
        title:
            "Bank Accounts (" +
            numAddedBankAccounts +
            "/" +
            numTotalBankAccounts +
            ")",
        imageSource: OAImageSource.chevron_down,
        bankAccounts: nonEmptyBankAccounts,
        numTotalBankAccounts: numTotalBankAccounts,
        numAddedBankAccounts: numAddedBankAccounts,
        addMore: (firstEmptyBankAccount !== '' ? addMoreAssetList.bank : null)
    };
    assetData.push(assetBank);


//bank assset at overview card
    let bankAccCard = dashboardDataFirebase.bankAccCard;
    bankAccCard.imageSource = OAImageSource.bank_logo;
    bankAccCard.addMore = assetBank.addMore;


    /*
    /*
     *
     * Social media
     *
     *
     * */
    let numTotalSocialMediaAccounts =
        dashboardDataRemote?.socialMediaInfo?.numTotalSocialMediaAccounts ?? 0;
    let numAddedSocialMediaAccounts =
        dashboardDataRemote?.socialMediaInfo?.numAddedSocialMediaAccounts ?? 0;
    let socialMediaInfo = dashboardDataRemote?.socialMediaInfo?.socialMediaInfo ?? {};
    let nonEmptySocialMediaInfo = [];
    Object.keys(socialMediaInfo).map(key => {
        let socialKey = key.toUpperCase();
        switch (socialKey) {
            case SocialKeys.FACEBOOK:
                let facebookAsset = {
                    title: "Facebook Account",
                    linked: socialMediaInfo?.[key]?.status == "ACTIVE",
                    imageSource: OAImageSource.icon_fb, // logoName: key, //FIXME:- provide social icon image
                    linkUrl: socialMediaInfo?.[key]?.url,
                    socialKey: socialKey,
                    planStatus: planDetail.planStatus
                };
                nonEmptySocialMediaInfo.push(facebookAsset);
                break;

            case SocialKeys.TWITTER:
                let twitterAsset = {
                    title: "Twitter Account",
                    linked: socialMediaInfo?.[key]?.status == "ACTIVE",
                    imageSource: OAImageSource.icon_twitter, // logoName: key, //FIXME:- provide social icon image
                    linkUrl: socialMediaInfo?.[key]?.url,
                    socialKey: socialKey,
                    planStatus: planDetail.planStatus
                };
                nonEmptySocialMediaInfo.push(twitterAsset);

                break;
            case SocialKeys.INSTAGRAM:
                let instagramAsset = {
                    title: "Instagram Account",
                    linked: socialMediaInfo?.[key]?.status == "ACTIVE",
                    imageSource: OAImageSource.icon_insta, // logoName: key, //FIXME:- provide social icon image
                    linkUrl: socialMediaInfo?.[key]?.url,
                    socialKey: socialKey,
                    planStatus: planDetail.planStatus
                };
                nonEmptySocialMediaInfo.push(instagramAsset);
                break;
            case SocialKeys.LINKEDIN:
                let linkedInAsset = {
                    title: "LinkedIn Account",
                    linked: socialMediaInfo?.[key]?.status == "ACTIVE",
                    imageSource: OAImageSource.icon_linkedin, // logoName: key, //FIXME:- provide social icon image
                    linkUrl: socialMediaInfo?.[key]?.url,
                    socialKey: socialKey,
                    planStatus: planDetail.planStatus
                };
                nonEmptySocialMediaInfo.push(linkedInAsset);
                break;
        }
    });
    let assetSocialAccount = {
        type: AssetsCategories.SOCIAL_MEDIA_ACCOUNT,
        title:
            "Social Media Accounts (" +
            numAddedSocialMediaAccounts +
            "/" +
            numTotalSocialMediaAccounts +
            ")",
        imageSource: OAImageSource.chevron_down,
        socialAccounts: nonEmptySocialMediaInfo,
        numAddedSocialMediaAccounts: numAddedSocialMediaAccounts,
        numTotalSocialMediaAccounts: numTotalSocialMediaAccounts
    };
    assetData.push(assetSocialAccount);

    //Social media carousel on overview tab

    let socialMediaCarousalData = [];

    let socialMediaData = dashboardDataRemote?.socialMediaInfo?.socialMediaInfo ?? {};
    let socialKeys = Object.keys(socialMediaData);

    socialKeys.map(key => {
        let socialData = socialMediaData[key];
        let socialKey = key.toLowerCase();
        if (socialData.status !== "ACTIVE") {
            let iconSource = OAImageSource.icon_camera;
            let firebaseKey = SocialKeys.FACEBOOK;
            //key map
            if (SocialKeys.FACEBOOK.toLowerCase() === socialKey.toLowerCase()) {
                iconSource = OAImageSource.fb_logo;
                firebaseKey = SocialKeys.FACEBOOK;
            }
            if (SocialKeys.TWITTER.toLowerCase() === socialKey.toLowerCase()) {
                iconSource = OAImageSource.twitter_logo;
                firebaseKey = SocialKeys.TWITTER;
            }
            if (SocialKeys.INSTAGRAM.toLowerCase() === socialKey.toLowerCase()) {
                iconSource = OAImageSource.instagram_logo;
                firebaseKey = SocialKeys.INSTAGRAM;
            }
            if (SocialKeys.LINKEDIN.toLowerCase() === socialKey.toLowerCase()) {
                iconSource = OAImageSource.linkedin_logo;
                firebaseKey = SocialKeys.LINKEDIN;
            }

            let firebaseSocialData =
                dashboardDataFirebase?.socialMediaData[socialKey];
            if (firebaseSocialData !== null && firebaseSocialData !== undefined) {
                firebaseSocialData.linkUrl = socialData?.url;
                firebaseSocialData.imageSource = iconSource;
                firebaseSocialData.socialKey = firebaseKey;
                firebaseSocialData.planStatus = planDetail.planStatus;
                socialMediaCarousalData.push(firebaseSocialData);
            }
        }
        if (socialData.status === "DISCONNECTED_FROM_SOCIAL_MEDIA") {
            userWarningData.push({
                "key": key,
                "webEngageValue": "Social Media",
                "status": socialData.status,
                "displayName": key,
                "data": {planStatus: planDetail.planStatus, linkUrl: socialData?.url},
                "actionText": "Connect Now"
            })
        }
    });

    let blogSubscriptionData = {};
    blogSubscriptionData.subtitle = "Short and insightful tips on your email.";
    blogSubscriptionData.actionText = "Subscribe";

    /*
     *
     * bankLevelSafetyData
     *
     *
     * */
    let bankLevelSafetyData = {
        title: "100% Secure. Bank level safety",
        logos: [OAImageSource.ssl, OAImageSource.pci, OAImageSource.norton]
    };

    let data = [

        {
            type: OverviewCards.WarningCell.type,
            data: userWarningData
        },
        {
            type: OverviewCards.AlertsOverview.type,
            data: null
        },
        {
            type: OverviewCards.Todo.type,
            data: getToDoData(
                dashboardDataRemote,
                dashboardDataFirebase.todoCardData,
                skippedCards,
                socialMediaData,
                planDetail
            )
        },
        {
            type: OverviewCards.IDFencePremiumPlanCell.type,
            data: idfencePremiumPlanCellData
        },
        {
            type: OverviewCards.LinkSocial.type,
            data: socialMediaCarousalData
        },
        {
            type: OverviewCards.BankAccount.type,
            data: bankAccCard
        },
        {
            type: OverviewCards.GovIDs.type,
            data: govIDsCarouselData
        },
        {
            type: OverviewCards.Mobile.type,
            data: mobileData
        },
        {
            type: OverviewCards.Blogs.type,
            data: blogsData,
        },
        {
            type: OverviewCards.BlogSubscription.type,
            data: blogSubscriptionData,
        },
        {
            type: OverviewCards.ShareApp.type,
            data: {
                title: "Tell your friends about OneAssist ID Fence",
                actionText: "Share App"
            }
        },
        {
            type: OverviewCards.BankSafety.type,
            data: bankLevelSafetyData
        }
    ];

    /*
     *
     * Details tab data
     *
     *
     * */
    let detailsData = [dashboardDataRemote?.customerInfo];

    return {
        data,
        assetData,
        detailsData,
        bankLevelSafetyData,
        premiumPopupData
    };
};

//get todoCard and doneAssetsCount

const getToDoData = (
    dashboardDataRemote,
    todoCardData,
    skippedCards,
    socialMediaData,
    planDetail
) => {
    let totalAssets = 6;
    let assetsDone = 0;

    // Done asset %
    if (dashboardDataRemote.cardInfo?.numAddedCards > 0) assetsDone += 1;
    if (dashboardDataRemote.bankAccInfo?.numAddedBankAccounts > 0) assetsDone += 1;
    if (dashboardDataRemote.socialMediaInfo?.numAddedSocialMediaAccounts > 0) assetsDone += 1;
    if (dashboardDataRemote.emailInfo?.numAddedEmails > 0) assetsDone += 1;
    if (dashboardDataRemote.nationalIdInfo?.numAddedNationalIds > 0) assetsDone += 1;
    if (dashboardDataRemote.telephoneInfo?.numAddedTelephones > 0) assetsDone += 1;
    let completed = Math.round((assetsDone / totalAssets) * 100);

    let progressBarData = {
        title: completed + "% completed",
        subtitle: assetsDone + "/" + totalAssets + " done",
        completed: completed
    };

    let actionData = {
        action1: "Dismiss",
        action2: "Add"
    };

    //TODO card logic

    let alreadyAddedCards = [];

    if (dashboardDataRemote.cardInfo?.numAddedCards > 0) alreadyAddedCards.push(AllAssetKeys.CREDIT_DEBIT);
    if (dashboardDataRemote.bankAccInfo?.numAddedBankAccounts > 0) alreadyAddedCards.push(AllAssetKeys.BANK_ACCOUNT);
    if (socialMediaData?.FACEBOOK?.status === "ACTIVE") alreadyAddedCards.push(AllAssetKeys.FACEBOOK);
    if (socialMediaData?.TWITTER?.status === "ACTIVE") alreadyAddedCards.push(AllAssetKeys.TWITTER);
    if (socialMediaData?.INSTAGRAM?.status === "ACTIVE") alreadyAddedCards.push(AllAssetKeys.INSTAGRAM);
    if (dashboardDataRemote.emailInfo?.numAddedEmails > 0) alreadyAddedCards.push(AllAssetKeys.EMAIL);
    if (dashboardDataRemote.nationalIdInfo.nationalIds.PAN.status === "ACTIVE") alreadyAddedCards.push(AllAssetKeys.PAN_CARD);
    if (socialMediaData?.LINKEDIN?.status === "ACTIVE") alreadyAddedCards.push(AllAssetKeys.LINKEDIN);
    if (dashboardDataRemote.nationalIdInfo.nationalIds.PASSPORT.status === "ACTIVE") alreadyAddedCards.push(AllAssetKeys.PASSPORT);
    if (dashboardDataRemote.nationalIdInfo.nationalIds.DL.status === "ACTIVE") alreadyAddedCards.push(AllAssetKeys.DRIVING_LICENCE);
    if (dashboardDataRemote.telephoneInfo?.numAddedTelephones > 0) alreadyAddedCards.push(AllAssetKeys.MOBILE);

    let addList = AllTodoCards.filter(x => !alreadyAddedCards.includes(x));
    if (addList.length === 0) return null;
    if (addList.length === 1) {
        actionData.action1 = null
    }
    addList = addList.filter(x => !skippedCards.has(x));
    if (addList.length === 0) {
        addList = Array.from(skippedCards);
        skippedCards = new Set([])
    }

    let firstCard = addList[0];
    let infoCardData = getTodoInfoCardData(firstCard, todoCardData, socialMediaData, planDetail);

    return {
        type: firstCard,
        progressBarData: progressBarData,
        infoCardData: infoCardData.card,
        actionData: actionData,
        degree: infoCardData.gradientData.gradientDegree,
        colors: infoCardData.gradientData.gradientColors
    };
};

const getTodoInfoCardData = (type, todoCardData, socialMediaData, planDetail) => {
    let card = null;
    let gradientData = {
        gradientDegree: 225.65,
        gradientColors: [colors.color_FFFFFF, colors.color_FFFFFF],
    };
    switch (type) {
        case AllAssetKeys.CREDIT_DEBIT:
            card = todoCardData?.creditAndDebit;
            card.imageSource = null;
            card.addMore = addMoreAssetList.creditAndDebit;
            break;

        case AllAssetKeys.BANK_ACCOUNT:
            card = todoCardData?.bankAccount;
            card.imageSource = null;
            card.addMore = addMoreAssetList.bank;

            break;

        case AllAssetKeys.FACEBOOK:
            card = todoCardData?.facebook;
            card.linkUrl = socialMediaData?.FACEBOOK?.url;
            card.imageSource = OAImageSource.icon_fb;
            card.planStatus = planDetail.planStatus;
            gradientData.gradientColors = card.gradient?.colors;
            break;

        case AllAssetKeys.TWITTER:
            card = todoCardData?.twitter;
            card.linkUrl = socialMediaData?.TWITTER?.url;
            card.imageSource = OAImageSource.icon_twitter;
            card.planStatus = planDetail.planStatus;
            gradientData.gradientColors = card.gradient?.colors;
            break;

        case AllAssetKeys.INSTAGRAM:
            card = todoCardData?.instagram;
            card.linkUrl = socialMediaData?.INSTAGRAM?.url;
            card.imageSource = OAImageSource.icon_insta;
            card.planStatus = planDetail.planStatus;
            gradientData.gradientColors = card.gradient?.colors;
            break;

        case AllAssetKeys.EMAIL:
            card = todoCardData?.email;
            card.imageSource = null;
            card.addMore = addMoreAssetList.email;
            break;

        case AllAssetKeys.PAN_CARD:
            card = todoCardData?.panCard;
            card.imageSource = null;
            card.addMore = addMoreAssetList.pan;
            break;

        case AllAssetKeys.LINKEDIN:
            card = todoCardData?.linkedin;
            card.linkUrl = socialMediaData?.LINKEDIN?.url;
            card.imageSource = OAImageSource.icon_linkedin;
            card.planStatus = planDetail.planStatus;
            gradientData.gradientColors = card.gradient?.colors;
            break;
        case AllAssetKeys.PASSPORT:
            card = todoCardData?.passport;
            card.imageSource = null;
            card.addMore = addMoreAssetList.passport;

            break;
        case AllAssetKeys.DRIVING_LICENCE:
            card = todoCardData?.drivingLicence;
            card.imageSource = null;
            card.addMore = addMoreAssetList.dl;

            break;
        case AllAssetKeys.MOBILE:
            card = todoCardData?.mobile;
            card.imageSource = null;
            card.addMore = addMoreAssetList.mobile;
            break;
    }
    return {card, gradientData};
};

const alertDict = {
    IDCCN: "Credit Card/Debit card Alert",
    IDBANK: "Bank Account Alert",
    IDEMAILADDR: "Email Address Alert"
};

export const getPlanStatus = (daysRemaining, isTrial, siData, memSIEnabled) => {
    let planStatus = PlanStatus.TRIAL;
    if (daysRemaining >= 20) planStatus = isTrial ? PlanStatus.TRIAL : PlanStatus.ACTIVE;
    if (daysRemaining <= 9 && daysRemaining >= 0) planStatus = isTrial ? PlanStatus.TRIAL_EXPIRING : PlanStatus.EXPIRING;
    if (daysRemaining >= -7 && daysRemaining <= -1) planStatus = isTrial ? PlanStatus.TRIAL_EXPIRED : PlanStatus.EXPIRED;
    if (daysRemaining < -7) planStatus = isTrial ? PlanStatus.TRIAL_INACTIVE : PlanStatus.INACTIVE;

    if (planStatus === PlanStatus.EXPIRED || (planStatus === PlanStatus.TRIAL_EXPIRED && memSIEnabled)) {
        if (siData.status === "A") planStatus = PlanStatus.ACTIVE;
        else if (siData.status === "F") planStatus = PlanStatus.EXPIRED;
        else planStatus = PlanStatus.INACTIVE;
    }

    return planStatus;
};
const getTrialExpiringAlertData = (
    planDetail,
) => {
    let description1 = null;
    let buttonText1 = `Upgrade to Premium @ ₹${planDetail.offerPrice}/month`;
    let buttonText2 = `₹${planDetail.oldPrice}`;
    let footerText = `Save ₹${planDetail.oldPrice - planDetail.offerPrice} per month`;
    let updatePaymentOrRenew = false;
    switch (planDetail.planStatus) {
        case PlanStatus.TRIAL_EXPIRING:
            description1 = `Your trial membership is expiring in ${planDetail.daysRemaining} days. Upgrade it to premium keep monitoring identity thefts and free monthly credit reports`;

            break;
        case PlanStatus.TRIAL_EXPIRED:
            description1 = `Your trial membership is expired. Upgrade now to keep monitoring your assets and your Credit Score with our Premium Plan within ${-planDetail.daysRemaining} days`;
            break;
        case PlanStatus.TRIAL_INACTIVE:
            description1 = `Your trial membership is inactive. Upgrade now to keep monitoring your assets and your Credit score.`;
            break;
        case PlanStatus.EXPIRING:
            description1 = null;
            break;
        case PlanStatus.EXPIRED:
            description1 = `Your Last Payment failed, please update your payment info to continue availing the benefits within ${-planDetail.daysRemaining} days`;
            buttonText1 = `Update Payment Method`;
            buttonText2 = ``;
            footerText = ``;
            updatePaymentOrRenew = true;
            break;
        case PlanStatus.INACTIVE:
            description1 = `Your membership has been inactivated. Please reactivate now to continue availing the benefits`;
            buttonText1 = `Reactivate Now `;
            buttonText2 = ``;
            footerText = ``;
            updatePaymentOrRenew = true;
            break;
    }
    if (!description1) return null;
    return {
        title: `Hi ${planDetail.customerName} 👋`,
        description: description1,
        checkList: [
            "Check if someone took a loan on half of you",
            "Track credit score enquiries",
            "See if your address is changed",
            "Track all your accounts at one place"
        ],
        note:
            "Note : We will never share your information with anyone and spam you with any promotion",
        offerPrice: buttonText1,
        oldPrice: buttonText2,
        savedAmount: footerText,
        updatePaymentOrRenew: updatePaymentOrRenew

    };
};

export const getTopAlerts = (alertCards, alertSummary) => {
    let allAlerts = [];
    if (alertCards !== null && allAlerts !== undefined) allAlerts = alertCards;
    if (alertSummary !== null && alertSummary !== undefined) allAlerts = allAlerts?.concat(alertSummary);
    let filteredAlerts = Array.from(allAlerts).sort(
        (a, b) => b.createdOn - a.createdOn
    );
    filteredAlerts = filteredAlerts.slice(0, 5);
    let top5AlertsMessages = getAlertMessages(filteredAlerts);

    if (top5AlertsMessages.length === 0) return null;
    return {
        title: top5AlertsMessages.length + " alerts that need your attention",
        alertList: top5AlertsMessages,
        imageSource: require("../../images/right_arrow.webp"),
        totalUnreadCount: alertCards
    };
};

const getAlertMessages = arr => {
    let messageArray = arr.map((item, index) => {
        if (item.alertType === null && item.alertGroup !== null) {
            //report
            return index + 1 + ". " + "First Time report";
        }
        return index + 1 + ". " + alertMap[item.alertType];
    });
    // alert(messageArray);
    return messageArray.filter(item => {
        return item !== ""
    });

};

let alertMap = {
    //credit score: Alert summary
    NEW_ACCOUNT: "New Credit Account Alert",
    NEW_ENQUIRY: "New Credit Enquiry Alert",
    ADDRESS_CHANGE: "Registered Address Change Alert",
    SCORE_CHANGE: "New Credit Score Alert",

    //Dashboard: AlertTodo
    EMAIL: "Email Address Alert",
    TELEPHONE: "Phone Number Alert",
    CARD: "Credit Card/Debit card Alert",
    BANK: "Bank Account Alert",
    NATIONAL_ID: "National ID Alert",
    FACEBOOK: "Facebook Alert",
    LINKEDIN: "LinkedIn Alert",
    TWITTER: "Twitter Alert",
    INSTAGRAM: "Instagram Alert",
    REPORT: "First Time report"
};
