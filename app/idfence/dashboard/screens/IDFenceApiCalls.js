import {markAlertRead, markCreditScoreAlertRead, Urls} from '../../constants/Urls';
import { makeGetRequest, makePostRequest, makePutRequest } from "../../../network/ApiManager";
import {AlertsTabConstants} from '../../constants/Constants';

export const SUCCESS_KEY = "SUCCESS";
export const FAIL_KEY = "FAIL";
export const addCyberDataAPI = (subscriberNo, addMoreData, textFieldData, callBack) => {
    let {element, dataKey, indexKey, cardType} = addMoreData;
        let data = {
            indexKey: indexKey
        };
        data[dataKey] = textFieldData;
        if (cardType !== null && cardType !== undefined) data.cardType = cardType;
        let body = [{element: element, data: JSON.stringify(data)}];
    makePostRequest(Urls.addCyberData + subscriberNo, body, (response, error) => {
                if (response) {
                    callBack(response, SUCCESS_KEY);
                } else {
                    callBack(error, FAIL_KEY)
                }
    });
};

export const getCardType = (cardPrefix, callback) => {
    makeGetRequest(Urls.getCardDetail + cardPrefix, (response, error) => {
            if (response) {
                callback(response, SUCCESS_KEY);
            } else {
                callback(error, FAIL_KEY)
            }
        });
};

export const renewMembership = (renewalData, callback) => {
    makePostRequest(Urls.renewal, renewalData, (response, error) => {
                if (response) {
                    callback(response, SUCCESS_KEY);
                } else {
                    callBack(error, FAIL_KEY)
                }
    });
};

export const getAlertDetails = (subscriberNo, alertId, isCreditScore, callBack) => {
    let url = isCreditScore ? Urls.getAlertDetailsCreditScore : Urls.getAlertDetailsIDFence;
    url += subscriberNo + "&alertId=" + alertId;
    makeGetRequest(url, (response, error) => {
            if (response) {
                callBack(response, SUCCESS_KEY);
            } else {
                callBack(error, FAIL_KEY)
            }
            });
};

export const markAlertReadAPICall = (alertType, memUUID, alertID) => {
        if (alertType == AlertsTabConstants.ALERT_TYPE_NORMAL) {
            makePutRequest(markAlertRead(memUUID, alertID), (response, error) => {
                    if (response) {
                    } else {
                        alert("failure 0")
                    }
                }
            );
        } else {
            let requestData = "";
            makePostRequest(markCreditScoreAlertRead(memUUID, alertID), requestData, (response, error) => {
                    if (response) {
                    } else {
                        //alert("failure 1")
                    }
                }
            );
        }
}
