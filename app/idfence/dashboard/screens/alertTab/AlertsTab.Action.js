import {connect} from "react-redux";
import AlertsTab from "./AlertsTab";
import {
    applyFilterFlags,
    clearAllFilterFlagsData,
    getAlertsListData,
    getCreditScoreServiceAlertsData,
    prepareAlertsListData, refreshTabBar,
    resetFlagsData,
    showCreditScoreAlertsError,
    showNormalAlertsError
} from "../../../constants/actionCreators";
import {
    fetchAlertsList,
    fetchCreditCoreServiceAlertsList,
    markAlertRead,
    markCreditScoreAlertRead
} from "../../../constants/Urls";
import { makeGetRequest, makePostRequest, makePutRequest } from  "../../../../network/ApiManager";
import {AlertsTabConstants} from "../../../constants/Constants";

const mapStateToProps = state => ({
    alertsData: state.alertsTabReducer.alertsData,
    alertsListData: state.alertsTabReducer.alertsListData,
    alertsTypeList: state.alertsTabReducer.alertsTypeList,
    pageNoNormalAlerts: state.alertsTabReducer.pageNoNormalAlerts,
    pageNoCreditScoreAlerts: state.alertsTabReducer.pageNoCreditScoreAlerts,
    isAllNormalAlertFetched: state.alertsTabReducer.isAllNormalAlertFetched,
    isAllCreditScoreAlertFetched: state.alertsTabReducer.isAllCreditScoreAlertFetched,
    isLoading: state.alertsTabReducer.isLoading,
    isClearAllFlags: state.alertsTabReducer.isClearAllFlags,
    isApplyFilters: state.alertsTabReducer.isApplyFilters,
    isFiltersApplied: state.alertsTabReducer.isFiltersApplied,
    creditScoreFilters: state.alertsTabReducer.creditScoreFilters,
    normalFilters: state.alertsTabReducer.normalFilters,
    isOnScrollLoading: state.alertsTabReducer.isOnScrollLoading,
    isLoadingAlertsList: state.alertsTabReducer.isLoadingAlertsList,
    isLoadingCreditScoreAlertsList: state.alertsTabReducer.isLoadingCreditScoreAlertsList,
    isAllAlertDataLoaded: state.alertsTabReducer.isAllAlertDataLoaded,
    isResetFlags: state.alertsTabReducer.isResetFlags,
    unreadAlertCount: state.dashboardReducer.unreadAlertCount,
    premiumPopupData: state.dashboardReducer.premiumPopupData,
    siData:state.dashboardReducer.siData,
});
const mapDispatchToProps = dispatch => ({
    prepareAlertsListData: (dataSource) => {
        setTimeout(() => {
            dispatch(prepareAlertsListData(dataSource));
        }, 30)
    },
    fetchAlertsListData: (memUUID, pageNo, normalFilters) => {
        setTimeout(() => {
            makeGetRequest(fetchAlertsList(memUUID, pageNo, 10, normalFilters), (response, error) => {
                        if (response) {
                            dispatch(getAlertsListData(response?.data));
                        } else {
                            dispatch(showNormalAlertsError(error));
                        }
            });
        }, 20)
    },
    fetchCreditCoreServiceAlertsData: (memUUID, pageNo, creditScoreFilters) => {
        makeGetRequest(fetchCreditCoreServiceAlertsList(memUUID, pageNo, 10, creditScoreFilters), (response, error) => {
                    if (response) {
                        dispatch(getCreditScoreServiceAlertsData(response?.data));
                    } else {
                        dispatch(showCreditScoreAlertsError(error));
                    }
        });
    },
    markAlertRead: (alertType, memUUID, alertID) => {
            if (alertType == AlertsTabConstants.ALERT_TYPE_NORMAL) {
                makePutRequest(markAlertRead(memUUID, alertID), (response, error) => {
                        if (response) {
                        } else {
                            alert("failure 0")
                        }
                    }
                );
            } else {
                makePostRequest(markCreditScoreAlertRead(memUUID, alertID), "", (response, error) => {
                        if (response) {
                        } else {
                            //alert("failure 1")
                        }
                    }
                );
            }
    },
    resetFlagsData: () => {
        dispatch(resetFlagsData());
    },
    applyFilters: (normalFilters, creditScoreFilters) => {
        dispatch(applyFilterFlags({normalFilters, creditScoreFilters}));
    },
    clearAllFlagsData: (normalFilters, creditScoreFilters) => {
        dispatch(clearAllFilterFlagsData({normalFilters, creditScoreFilters}));
    },
    refreshTabBarAssets: () => {
        dispatch(refreshTabBar());
    },

});
export default connect(mapStateToProps, mapDispatchToProps)(AlertsTab);