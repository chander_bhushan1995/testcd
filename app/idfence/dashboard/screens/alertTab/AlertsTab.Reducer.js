import {
    APPLY_FILTER_FLAGS,
    CLEAR_ALL_FLAGS,
    GET_ALERTS_LIST_DATA,
    GET_ALERTS_LIST_DATA_ERROR,
    GET_CREDIT_SCORE_SERVICE_ALERTS_DATA,
    GET_CREDIT_SCORE_SERVICE_ALERTS_DATA_ERROR,
    PREPARE_ALERTS_LIST_DATA,
    RESET_FLAGS
} from "../../../constants/actions";
import {AlertsTabConstants} from "../../../constants/Constants";
import {alertCellSubTitleDate, alertIndicatorIcon, alertsTitle} from "../../../constants/IDFence.UtilityMethods";
import {OAImageSource} from "../../../../Constants/OAImageSource";

const initialState = {
    alertsData: {alertsList: [], creditScoreAlertsList: []},
    alertsTypeList: {},
    alertsListData: [],
    isLoadingAlertsList: true,
    isLoadingCreditScoreAlertsList: true,
    isOnScrollLoading: false,
    isLoading: true,
    pageNoNormalAlerts: 1,
    pageNoCreditScoreAlerts: 1,
    isAllNormalAlertFetched: false,
    isAllCreditScoreAlertFetched: false,
    isAllAlertDataLoaded: false,
    isClearAllFlags: false,
    isApplyFilters: false,
    isFiltersApplied: false,
    creditScoreFilters: "",
    normalFilters: "",
    isResetFlags: false,
    unreadAlertCount: 0
}

const alertsTabReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALERTS_LIST_DATA:
            state.alertsData.alertsList = action.data;
            let normalAlertsList = [];
            if (action.data && action.data.length > 0) {
                action.data.map(item => {
                    if (state.alertsData.alertsList.filter(function (e) {
                        return e.itemID === item?.id;
                    }).length <= 0) {
                        item["itemType"] = AlertsTabConstants.ALERT_TYPE_NORMAL;
                        normalAlertsList.push(item);
                    }
                });
            } else {
                state.isAllNormalAlertFetched = true;
            }
            state.alertsData.alertsList = normalAlertsList;
            return {
                ...state,
                isLoadingAlertsList: false,
                isClearAllFlags: false,
                isResetFlags: false,
                isApplyFilters: false,
                pageNoNormalAlerts: state.pageNoNormalAlerts + 1,
                isAllNormalAlertFetched: state.isAllNormalAlertFetched,
                alertsData: state.alertsData
            };
        case GET_CREDIT_SCORE_SERVICE_ALERTS_DATA:
            let creditScoreAlertsList = [];
            if (action.data && action.data.length > 0) {
                action.data.map(item => {
                    if (state.alertsData.creditScoreAlertsList.filter(function (e) {
                        return e.itemID === item?.id;
                    }).length <= 0) {
                        item["itemType"] = AlertsTabConstants.ALERT_TYPE_CREDIT_SCORE;
                        creditScoreAlertsList.push(item);
                    }
                });
            } else {
                state.isAllCreditScoreAlertFetched = true;
            }
            state.alertsData.creditScoreAlertsList = creditScoreAlertsList;
            return {
                ...state,
                isResetFlags: false,
                isClearAllFlags: false,
                isApplyFilters: false,
                pageNoCreditScoreAlerts: state.pageNoCreditScoreAlerts + 1,
                isLoadingCreditScoreAlertsList: false,
                isAllCreditScoreAlertFetched: state.isAllCreditScoreAlertFetched,
                alertsData: state.alertsData
            };
        case PREPARE_ALERTS_LIST_DATA:
            let allAlertsList = [];
            let allAlertsWithCreditScore = [...state.alertsData.alertsList, ...state.alertsData.creditScoreAlertsList];

            /* Note:- below code is disabled just because  there are no alert found or count is zero state is not getting update
             like :-  state is not getting update to hide loader and show no alerts found message.
             */
            // if (allAlertsWithCreditScore.length===0){
            //     return {...state};
            // }

            allAlertsWithCreditScore.map(item => {
                let createdOnLabel = alertCellSubTitleDate(item?.createdOn);
                /* Note:- below code is written by hemlata by i have commented because of this first time report alert is not showing
                    where alert type is getting null from backend. but web team is showing.
                 */
                // if(item.alertType === null || item.alertType == undefined) {
                //     return;
                // }

                allAlertsList.push({
                    data: item,
                    itemID: item?.id,
                    isClearAllFlags: false,
                    alertType: item?.alertType,
                    itemType: item?.itemType,
                    subTitle: createdOnLabel,
                    title: alertsTitle(item, action.data.alertsTypeList),
                    createdOn: item?.createdOn,
                    readByUser: (item?.readDate !== null && item?.readDate !== undefined),
                    leftImageSource: alertIndicatorIcon(item),
                    imageSource: OAImageSource.chevron_down,
                    itemTypeDisplayValue: action.data?.alertsTypeList[item?.alertType]?.alertDisplayValue ?? ""
                });
            });

            state.isAllAlertDataLoaded = allAlertsList.length == 0;
            allAlertsList.sort(function (item1, item2) {
                return (item2.createdOn - item1.createdOn);
            });
            allAlertsList.map(item => {
                if (state.alertsListData.filter(function (e) {
                    return e.itemID === item.itemID;
                }).length <= 0) {
                    state.alertsListData.push(item);
                }
            });

            return {
                ...state,
                isLoading: false,
                isClearAllFlags: false,
                isApplyFilters: false,
                isAllAlertDataLoaded: state.isAllAlertDataLoaded,
                alertsListData: state.alertsListData
            }
        case GET_ALERTS_LIST_DATA_ERROR:
            return {
                ...state,
                isResetFlags: false,
                isLoadingAlertsList: false,
                isClearAllFlags: false,
                isApplyFilters: false,
                pageNoNormalAlerts: state.pageNoNormalAlerts - 1,
            };
        case GET_CREDIT_SCORE_SERVICE_ALERTS_DATA_ERROR:
            return {
                ...state,
                isResetFlags: false,
                isLoadingCreditScoreAlertsList: false,
                isClearAllFlags: false,
                isApplyFilters: false,
                pageNoCreditScoreAlerts: state.pageNoCreditScoreAlerts + 1,
            };
        case RESET_FLAGS:
            return {
                ...state,
                isClearAllFlags: false,
                isResetFlags: true,
                isApplyFilters: false,
                isOnScrollLoading: true,
                isLoadingAlertsList: true,
                isLoadingCreditScoreAlertsList: true,
                isLoading: true
            };

        case CLEAR_ALL_FLAGS:

            return {
                ...state,
                isResetFlags: false,
                alertsData: {alertsList: [], creditScoreAlertsList: []},
                alertsTypeList: {},
                alertsListData: [],
                isLoadingAlertsList: (action.data.normalFilters.length > 0),
                isLoadingCreditScoreAlertsList: (action.data.creditScoreFilters.length > 0),
                isOnScrollLoading: false,
                isLoading: true,
                isClearAllFlags: true,
                isApplyFilters: false,
                isFiltersApplied: false,
                pageNoNormalAlerts: 1,
                pageNoCreditScoreAlerts: 1,
                creditScoreFilters: action.data.creditScoreFilters,
                normalFilters: action.data.normalFilters,
                isAllNormalAlertFetched: false,
                isAllCreditScoreAlertFetched: false,
                isAllAlertDataLoaded: false
            };
        case APPLY_FILTER_FLAGS:
            return {
                ...state,
                isResetFlags: false,
                alertsData: {alertsList: [], creditScoreAlertsList: []},
                alertsTypeList: {},
                alertsListData: [],
                isLoadingAlertsList: (action.data.normalFilters.length > 0),
                isLoadingCreditScoreAlertsList: (action.data.creditScoreFilters.length > 0),
                isOnScrollLoading: false,
                isLoading: true,
                isClearAllFlags: false,
                isApplyFilters: true,
                isFiltersApplied: true,
                pageNoNormalAlerts: 1,
                pageNoCreditScoreAlerts: 1,
                creditScoreFilters: action.data.creditScoreFilters,
                normalFilters: action.data.normalFilters,
                isAllNormalAlertFetched: false,
                isAllCreditScoreAlertFetched: false,
                isAllAlertDataLoaded: false
            };
        default:
            return {...state}
    }
};


export default alertsTabReducer;
