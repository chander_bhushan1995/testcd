import React from 'react';
import {NativeModules, Platform, SafeAreaView, ScrollView, View} from 'react-native';
import {CellWithLeftImgAndTitle} from '../../../../CustomComponent/customcells';
import styles from './AlertsTab.style';
import AlertsTabCellExpandedView from './cellexpandedview/AlertsTab.CellExpandedView';
import {isScrollViewCloseToBottom} from '../../../../commonUtil/CommonMethods.utility';
import OAActivityIndicator from '../../../../CustomComponent/OAActivityIndicator';
import OAActivityIndicatorShowHide from '../../../../CustomComponent/OAActivityIndicatorShowHide';
import {
    AlertsTabConstants,
    getExpiredMemSubTitle,
    getTrialOverMem,
    scrollEventThrottle,
} from '../../../constants/Constants';
import colors from '../../../../Constants/colors';
import FilterBadgeView from '../../../../CustomComponent/filterbadgeview/FilterBadgeView.compoment';
import AlertFiltersBottomSheet from './alertfiltersview/AlertFiltersBottomSheet.component';
import MembershipExpired from './alertfiltersview/MembershipExpired.component';
import {
    getIDFCommonEventAttr,
    isMemTrialEndOrExpired,
    logWebEngageTabEvent,
} from '../../../constants/IDFence.UtilityMethods';
import {WEBENGAGE_IDFENCE_DASHBOARD} from '../../../../Constants/WebengageAttrKeys';
import {logWebEnageEvent} from '../../../../commonUtil/WebengageTrackingUtils';
import {prepareAlertsListData} from "../../../constants/actionCreators";
import {renewMembership, SUCCESS_KEY} from '../IDFenceApiCalls';
import {PLATFORM_OS} from '../../../../Constants/AppConstants';
import { parseJSON, deepCopy} from "../../../../commonUtil/AppUtils";

const TAG = 'AlertsTab ';
let markedReadAlertsList = [];
const nativeBridgeRef = NativeModules.ChatBridge;
export default class AlertsTab extends React.Component {

    constructor(props) {
        super(props);
        this.bottomProgressCircle = React.createRef();
        this.alertFiltersBottomSheetRef = React.createRef();
        this.state = {
            expanded: false,
            selectedIndex: -1,
            membershipData: parseJSON(this.props.initialProps?.membershipData),
        };
    }

    componentWillMount() {
        logWebEngageTabEvent(this.state.membershipData, WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.LOCATION_ATTR_VALUE.ALERTS);
        if (!isMemTrialEndOrExpired(this.state.membershipData)) {
            this.callAlertDataApis();
        }

        //this.callAlertDataApis();
    }

    callAlertDataApis = () => {
        this.props.resetFlagsData();
        this.callApis();
    };

    callApis = () => {
        if (!this.props.isAllNormalAlertFetched) {
            if (this.props.isFiltersApplied) {
                if (this.props.normalFilters.length > 0) {
                    this.props.fetchAlertsListData(this.state.membershipData?.subscriberNo, this.props.pageNoNormalAlerts,
                        this.props.normalFilters);
                }
            } else {
                this.props.fetchAlertsListData(this.state.membershipData?.subscriberNo, this.props.pageNoNormalAlerts,
                    this.props.normalFilters);
            }

        }
        if (!this.props.isAllCreditScoreAlertFetched) {
            if (this.props.isFiltersApplied) {
                if (this.props.creditScoreFilters.length > 0) {
                    this.props.fetchCreditCoreServiceAlertsData(this.state.membershipData?.subscriberNo, this.props.pageNoCreditScoreAlerts,
                        this.props.creditScoreFilters);
                }
            } else {
                this.props.fetchCreditCoreServiceAlertsData(this.state.membershipData?.subscriberNo, this.props.pageNoCreditScoreAlerts,
                    this.props.creditScoreFilters);
            }
        }
    };

    applyFiltersApiCalls = () => {
        if (!this.props.isAllNormalAlertFetched && this.props.normalFilters.length > 0) {
            this.props.fetchAlertsListData(this.state.membershipData?.subscriberNo, this.props.pageNoNormalAlerts,
                this.props.normalFilters);
        }
        if (!this.props.isAllCreditScoreAlertFetched && this.props.creditScoreFilters.length > 0) {
            this.props.fetchCreditCoreServiceAlertsData(this.state.membershipData?.subscriberNo, this.props.pageNoCreditScoreAlerts,
                this.props.creditScoreFilters);
        }
    };


    render() {
        /*if (this.props.isResetFlags){
            return <OAActivityIndicator/>;
        }*/

        if (isMemTrialEndOrExpired(this.state.membershipData)) {

            if (this.state.membershipData?.isTrial) {
                return <MembershipExpired data={AlertsTabConstants.trialOverMemData}
                                          membershipData={this.state.membershipData}
                                          subTitle={getTrialOverMem(this.props.unreadAlertCount)}
                                          premiumPLan={this.props.idFencePlansData.premiumPlan}
                                          actionType={AlertsTabConstants.ACTION_KNOW_MORE} isNoAlertFound={false}
                                          siData={this.props.siData}
                                          renewalData={this.props.renewalData}
                />;
            }
            return <MembershipExpired data={AlertsTabConstants.expiredMemData}
                                      membershipData={this.state.membershipData}
                                      subTitle={getExpiredMemSubTitle(this.props.unreadAlertCount)}
                                      premiumPLan={this.props.idFencePlansData.premiumPlan}
                                      actionType={AlertsTabConstants.ACTION_BUY_NOW} isNoAlertFound={false}
                                      siData={this.props.siData}
                                      renewalData={this.props.renewalData}
                                      renewalAction={() => {
                                          renewMembership(this.props.renewalData, (response, status)=>{
                                                  if (status === SUCCESS_KEY) {
                                                      let updatePaymentOrRenew = this.props.premiumPopupData.updatePaymentOrRenew;
                                                      if (Platform.OS === PLATFORM_OS.android) {
                                                          this.openIdFence(updatePaymentOrRenew, response.data?.payNowLink).then(value => {
                                                          }).catch(onerror => {
                                                          });
                                                      } else {
                                                          nativeBridgeRef.openIDFencePlanDetails(updatePaymentOrRenew, response.data?.payNowLink);
                                                      }
                                                  }
                                          });
                                      }}
            />;
        }
        if (this.props.isClearAllFlags) {
            this.callApis();
            return <OAActivityIndicator/>;
        }
        if (this.props.isApplyFilters) {
            this.applyFiltersApiCalls();
            return <OAActivityIndicator/>;
        }
        if ((this.props.isLoadingAlertsList || this.props.isLoadingCreditScoreAlertsList) && this.props.alertsListData.length == 0) {
            return <OAActivityIndicator/>;
        }

        if (this.props.isLoading) {
            this.props.prepareAlertsListData(this.props.dataSource);
            if (this.props.alertsListData.length===0){
                return <OAActivityIndicator/>;
            }
        }

        return (
            <SafeAreaView style={styles.safeArea}>
                <View style={styles.scrollItemContainerViewStyle}>
                    <View style={styles.mainContainerViewStyle}>
                        <View style={styles.topHeaderViewStyle}><FilterBadgeView
                            status={(this.props.creditScoreFilters.length > 0 || this.props.normalFilters.length > 0)}
                            onFilterBadgePress={(e) => this.onFilterBadgePress(e)}/></View>
                    </View>
                    <ScrollView
                        style={styles.scrollViewStyle}
                        onScroll={({nativeEvent}) => {
                            if (isScrollViewCloseToBottom(nativeEvent) &&
                                (!this.props.isLoadingAlertsList || !this.props.isLoadingCreditScoreAlertsList)) {
                                if (!this.props.isAllAlertDataLoaded && this.bottomProgressCircle.current) {
                                    this.bottomProgressCircle.current.show();
                                    this.callAlertDataApis();
                                } else if (this.bottomProgressCircle.current) {
                                    this.bottomProgressCircle.current.hide();
                                }
                            } else {
                                if (this.props.isAllAlertDataLoaded) {
                                    if (this.bottomProgressCircle.current) {
                                        this.bottomProgressCircle.current.hide();
                                    }
                                }
                            }
                        }}
                        scrollEventThrottle={scrollEventThrottle}>
                        {this.getAlertsList()}
                        <OAActivityIndicatorShowHide ref={this.bottomProgressCircle}/>
                    </ScrollView>
                    <AlertFiltersBottomSheet ref={this.alertFiltersBottomSheetRef}
                                             alertsTypeList={this.props.dataSource.alertsTypeList}
                                             onApplyFilterClick={(assetFilterList, alertTypeFilterList) => this.onApplyFilterClick(assetFilterList, alertTypeFilterList)}
                                             clearAllFilters={() => this.clearAllFilters()}/>
                </View>

            </SafeAreaView>
        );
    }

    onApplyFilterClick = (assetFilterList, alertTypeFilterList) => {
        logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_ALERT_FILTER_APPLY, getIDFCommonEventAttr(this.state.membershipData));
        let creditScoreFilterList = [], normalAlertFilterList = [];
        let allFiltersList = assetFilterList.concat(alertTypeFilterList);
        allFiltersList.map(item => {
            if (item.item.alertType === AlertsTabConstants.ALERT_TYPE_GOOD || item.item.alertType === AlertsTabConstants.ALERT_TYPE_BAD) {
                creditScoreFilterList.push(item.item.alertType);
            }
            normalAlertFilterList.push(item.item.alertType);
        });

        this.props.applyFilters(normalAlertFilterList.join(','), creditScoreFilterList.join(','));
    };
    clearAllFilters = () => {
        logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_ALERT_FILTER_DISMISS, getIDFCommonEventAttr(this.state.membershipData));
        if (this.alertFiltersBottomSheetRef.current) {
            this.props.clearAllFlagsData('', '');
            this.alertFiltersBottomSheetRef.current.resetSelectedFilters();
            this.alertFiltersBottomSheetRef.current.setModalVisible(false);
        }
    };
    getAlertsList = () => {
        if (this.props.alertsListData.length > 0) {
            let list = this.props.alertsListData.map((item, index) => {
                return this.renderItem(item, index);
            });
            return list;
        } else {
            return <MembershipExpired data={AlertsTabConstants.noAlertFoundData} isNoAlertFound={true} membershipData={this.state.membershipData}/>;
        }

    };


    renderItem = (item, index) => {
        let alertGenre = item?.data?.alertGenre;
        let alertReadColor = (alertGenre === 'GOOD' ? colors.color_45B448 : colors.color_D0021B);
        return (
            <View style={styles.rowItemContainerStyle}>
                <CellWithLeftImgAndTitle item={item}
                                         index={index}
                                         cellTitleOnPressColor={(!item.readByUser ? alertReadColor : null)}
                                         onPress={(e) => this.handleAction(e, item, index)}/>
                <AlertsTabCellExpandedView index={index} item={item} expanded={!this.state.expanded}
                                           selectedIndex={this.state.selectedIndex}
                                           subscriberNo={this.state.membershipData?.subscriberNo} {...this.props}
                                           membershipData = {this.state.membershipData}
                />
            </View>
        );
    };

    handleAction = (e, item, index) => {
        e.stopPropagation();

        if (!item.readByUser) {
            if (!markedReadAlertsList.includes(item.data.id)) {
                this.props.markAlertRead(item.itemType, this.state.membershipData?.subscriberNo, item.data.id);
                markedReadAlertsList.push(item.data.id);
                this.props.refreshTabBarAssets();
            }
        }
        if (index === this.state.selectedIndex) {
            this.setState({
                selectedIndex: -1,
            });
        } else {
            let eventAttr = getIDFCommonEventAttr(this.state.membershipData);
            eventAttr[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.ASSET_NAME] = '' + (WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_ASSET_NAME_ATTR_VALUE[item?.data?.alertType] || 'Credit Score');
            logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_ALERT_VIEW, eventAttr);
            this.setState({
                selectedIndex: index,
            });
        }
    };

    onFilterBadgePress = (e) => {
        logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_ALERT_FILTER, getIDFCommonEventAttr(this.state.membershipData));
        if (this.alertFiltersBottomSheetRef.current) {
            this.alertFiltersBottomSheetRef.current.setModalVisible(true);
        }
    };


}
