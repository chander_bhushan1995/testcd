import {StyleSheet} from "react-native";
import spacing from "../../../../Constants/Spacing";
import Dimens from "../../../../Constants/Dimens";
import colors from "../../../../Constants/colors";

export default StyleSheet.create({
    safeArea: {
        flex: spacing.spacing1,
        backgroundColor: colors.color_FFFFFF
    },
    scrollViewStyle: {
        flex: Dimens.dimen1
    },
    scrollItemContainerViewStyle: {
        flex: spacing.spacing1,
        flexDirection: 'column'
    },
    mainContainerViewStyle: {
        flexDirection: 'row',
        height: Dimens.dimen60,
        justifyContent: 'center',
        alignItems: 'center',
        paddingRight: spacing.spacing16,
        paddingLeft: spacing.spacing16
    },
    topHeaderViewStyle: {
        flexDirection: 'row',
        flex: spacing.spacing1,
        justifyContent: 'flex-end',
        alignSelf: 'center'
    },
    rowItemContainerStyle:{
        flex: spacing.spacing1
    }

});
