import React, {Component} from 'react';
import {
    Image,
    Modal,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View
} from 'react-native';
import colors from "../../../../../Constants/colors";
import {OATextStyle} from "../../../../../Constants/commonstyles";
import SeparatorView from "../../../../../CustomComponent/separatorview";
import LinkButton from "../../../../../CustomComponent/LinkButton";
import SelectableTextWithIndicatorView from "../../../../../CustomComponent/selectabletextwithindicatorview";
import FilterListView from "./FilterListView.component";
import dimens from "../../../../../Constants/Dimens";
import ButtonWithEnableDisable from "../../../../../CommonComponents/ButtonWithEnableDisable";

const alertAssetRef = React.createRef();
const alertAlertTypeRef = React.createRef();
const filterListRef = React.createRef();
const applyFilterButtonRef = React.createRef();
const TAG = "AlertFiltersBottomSheet";
const checkedAssetItemList = [], checkedAlertTypeItemList = [];
export default class AlertFiltersBottomSheet extends Component {
    state = {modalVisible: false, recommendedActionsList: []};

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    resetSelectedFilters() {
        checkedAssetItemList.length = 0;
        checkedAlertTypeItemList.length = 0;
    }

    componentDidMount(): void {
    }

    render() {
        return (

            <View>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }}>
                    <TouchableOpacity
                        activeOpacity={1}
                        style={{
                            flex: 1,
                            flexDirection: 'row',
                            alignItems: 'flex-end',
                            backgroundColor: 'rgba(0,0,0,0.7)'
                        }}
                        onPress={() => {
                            this.setModalVisible(false)
                        }}>

                        <SafeAreaView style={styles.container}>
                            <View style={{
                                alignSelf: 'flex-start',
                                alignItems: 'flex-end',
                                justifyContent: 'flex-end',
                                flexDirection: 'row',
                                backgroundColor: 'white',
                            }}>
                                <TouchableWithoutFeedback style={{flex: 1}}>
                                    <View style={{
                                        flex: 1,
                                        flexDirection: 'column',
                                    }}>
                                        <View style={{flexDirection: 'row', justifyContent: 'center', margin: 16}}>
                                            <View style={{
                                                alignItems: 'center',
                                                justifyContent: 'center'
                                            }}>
                                                <Text
                                                    style={OATextStyle.TextFontSize_20_bold_212121}>
                                                    Filters
                                                </Text>
                                            </View>
                                            <View style={{
                                                flex: 1,
                                                alignSelf: 'center',
                                                justifyContent: 'center',
                                                alignItems: 'flex-end'
                                            }}>
                                                <TouchableOpacity onPress={(e) => this.setModalVisible(false)}>
                                                    <Image style={{width: 16, height: 16}} resizeMode={'contain'}
                                                           source={require('../../../../../images/cross_idfence.webp')}/>
                                                </TouchableOpacity>

                                            </View>
                                        </View>
                                        <SeparatorView/>
                                        <View style={{flexDirection: 'row', maxHeight: '68%'}}>
                                            <View style={{flex: 1, backgroundColor: colors.color_F6F6F6}}>
                                                <SelectableTextWithIndicatorView
                                                    ref={alertAssetRef}
                                                    label="Asset"
                                                    setDefaultSelected={true}
                                                    onRowPress={() => onAlertTypeClick(0)}/>
                                                <SelectableTextWithIndicatorView
                                                    ref={alertAlertTypeRef}
                                                    setDefaultSelected={false}
                                                    label="Alert Type"
                                                    onRowPress={() => onAlertTypeClick(1)}/>
                                            </View>
                                            <View style={{backgroundColor: "#E0E0E0", width: 1}}/>
                                            <View style={{flex: 2, paddingTop: 16}}>
                                                <TouchableWithoutFeedback style={{flex: 1}}>
                                                    <FilterListView
                                                        ref={filterListRef}
                                                        onItemCheckListener={() => onItemCheckListener()}
                                                        alertsTypeList={this.props.alertsTypeList}
                                                        checkedAssetItemList={checkedAssetItemList}
                                                        checkedAlertTypeItemList={checkedAlertTypeItemList}/>
                                                </TouchableWithoutFeedback>
                                            </View>
                                        </View>
                                        <View style={{
                                            flexDirection: 'column',
                                            height: 80,
                                            backgroundColor: 'white',
                                            borderColor: "transparent",
                                            shadowColor: '#000',
                                            shadowOffset: {
                                                width: 0,
                                                height: -5,
                                            },
                                            shadowOpacity: 0.3,
                                            shadowRadius: 5,
                                            elevation: 4,
                                        }}>
                                            <SeparatorView/>
                                            <View style={{flexDirection: 'row', flex: 1}}>
                                                <View style={{
                                                    flex: 1,
                                                    padding: 12,
                                                    justifyContent: 'center',
                                                    alignItems: 'center'
                                                }}>
                                                    <LinkButton
                                                        style={{
                                                            actionTextStyle: [OATextStyle.TextFontSize_14_bold, {
                                                                color: colors.color_0282F0
                                                            }]
                                                        }}
                                                        onPress={() => {

                                                            this.props.clearAllFilters();
                                                        }}
                                                        data={{
                                                            action: "Clear All"
                                                        }}/>
                                                </View>
                                                <View style={{
                                                    flex: 1,
                                                    padding: 12,
                                                    justifyContent: 'flex-end'
                                                }}>
                                                    <ButtonWithEnableDisable
                                                        ref={applyFilterButtonRef}
                                                        isLoading={false}
                                                        enable={(checkedAssetItemList.length > 0 || checkedAlertTypeItemList.length > 0)}
                                                        Button={{
                                                            text: "Apply filters",
                                                            onClick: () => {
                                                                if (filterListRef.current) {
                                                                    this.props.onApplyFilterClick(checkedAssetItemList, checkedAlertTypeItemList);
                                                                }
                                                            }
                                                        }}
                                                    />
                                                </View>
                                            </View>
                                        </View>


                                    </View>
                                </TouchableWithoutFeedback>
                            </View>

                        </SafeAreaView>

                    </TouchableOpacity>
                </Modal>
            </View>
        );

        function onAlertTypeClick(index) {
            if (index === 0) {
                if (alertAssetRef.current) {
                    alertAssetRef.current.resetRowSelection(true);
                    alertAlertTypeRef.current.resetRowSelection(false);
                }
            } else {
                if (alertAlertTypeRef.current) {
                    alertAssetRef.current.resetRowSelection(false);
                    alertAlertTypeRef.current.resetRowSelection(true);
                }
            }
            filterListRef.current.refreshListData(index);
        }

        function onItemCheckListener() {
            if (applyFilterButtonRef.current) {
                applyFilterButtonRef.current.enableDisableButton((checkedAssetItemList.length > 0 || checkedAlertTypeItemList.length > 0));
            }
        }
    }


}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        maxHeight: '80%',
    },
    title: {
        ...OATextStyle.TextFontSize_14_normal,
        lineHeight: dimens.dimen22,
        color: colors.color_212121
    },
    item: {
        backgroundColor: colors.color_f9c2ff,
        height: dimens.dimen55
    }
});


