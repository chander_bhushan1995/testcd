import React, {forwardRef, useImperativeHandle, useState} from "react";
import {FlatList, TouchableWithoutFeedback} from "react-native";
import CellWithLeftCheckboxAndTitle
    from "../../../../../CustomComponent/customcells/cellwithleftcheckboxandtitle/CellWithLeftCheckboxAndTitle.component";
import {getUniqueRowID} from "../../../../constants/IDFence.UtilityMethods";

const FilterListView = forwardRef((props, ref) => {
        const [LIST_DATA, setUIData] = useState({data: filterForAlertAssetData(), filterType: 0});

        function filterForAlertAssetData() {
            let onlyNormalAlertsType = [];
            Object.values(props.alertsTypeList).forEach((item) => {
                if (item.alertCategory === "NORMAL") {
                    item["id"] = getUniqueRowID();
                    item["title"] = item["alertDisplayValue"];
                    onlyNormalAlertsType.push(item);
                }
            });
            return onlyNormalAlertsType;
        };

        function prepareAlertTypeList() {
            let alertsTypeList = [
                {
                    id: getUniqueRowID(),
                    title: 'All good alerts',
                    alertType: 'GOOD',

                },
                {
                    id: getUniqueRowID(),
                    title: 'All bad alerts',
                    alertType: 'BAD',
                },
                {
                    id: getUniqueRowID(),
                    title: 'All reports',
                    alertType: 'REPORT',
                },
            ];

            return alertsTypeList;
        };

        useImperativeHandle(ref, () => {
            return {
                refreshListData: refreshListData
            };
        });
        const refreshListData = (type) => {
            let data = ((type == 0) ? filterForAlertAssetData() : prepareAlertTypeList());
            setUIData(prev => ({
                ...prev,
                data: data,
                filterType: type
            }))
        }

        return (
            <TouchableWithoutFeedback style={{flex: 1}}>
                <FlatList
                    data={LIST_DATA.data}
                    renderItem={({item, index}) => prepareListItems(item, index)}
                    keyExtractor={item => item.id}
                />
            </TouchableWithoutFeedback>
        );

        function prepareListItems(item, index) {
            return (<CellWithLeftCheckboxAndTitle onItemCheckListener={props.onItemCheckListener} item={item} index={index}
                                                  checkItemList={LIST_DATA.filterType === 0 ? props.checkedAssetItemList : props.checkedAlertTypeItemList}/>);
        }
    });
export default FilterListView;
