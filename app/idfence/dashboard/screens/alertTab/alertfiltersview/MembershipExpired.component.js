import React from "react";
import {Image, ScrollView, Text, View} from "react-native";
import memExpiredStyles from "./MembershipExpired.style";
import IDFenceBuyStickyView from "../../../../../CustomComponent/idfencebuystickyview";
import {LOCATION, WEBENGAGE_IDFENCE_DASHBOARD} from "../../../../../Constants/WebengageAttrKeys";
import {logWebEnageEvent} from "../../../../../commonUtil/WebengageTrackingUtils";
import {getIDFCommonEventAttr} from '../../../../constants/IDFence.UtilityMethods';
import {getPlanStatus} from '../../../DashboardUtils';
import {PlanStatus} from '../../../../constants/Constants';

export default ({data,subTitle, actionType,membershipData, premiumPLan, isNoAlertFound, siData, renewalData, renewalAction}) => {
    return (
        <View style={memExpiredStyles.mainContainer}>
            <View style={memExpiredStyles.scrollViewContainer}>
                <ScrollView>
                    <View>
                        <View style={memExpiredStyles.imageContainer}>
                            <Image style={memExpiredStyles.imageStyle} resizeMode={'cover'}
                                   source={data.imageUrl}/>
                        </View>
                        <View style={memExpiredStyles.textContainer}>
                            <Text style={memExpiredStyles.titleTextStyle}>{data.title}</Text>
                            <Text style={memExpiredStyles.subTitleTextStyle}>{data.subTitle}</Text>
                        </View>
                    </View>
                </ScrollView>

            </View>
            {showActionView()}
        </View>
    );

    function showActionView() {
        if (isNoAlertFound) {
            return null;
        }
        let eventAttr = getIDFCommonEventAttr(membershipData);
        eventAttr[LOCATION] = 'Alerts Tab';
        logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_DASHBOARD_BUY_PREMIUM_FOOTER_LOAD, eventAttr);
        return (<IDFenceBuyStickyView data={premiumPLan}
                                      membershipData={membershipData}
                                      from={'Alerts Tab'}
                                      upgradeLabel={data.upgradeLabel}
                                      actionText={data.actionText}
                                      isRenewal={getPlanStatus(parseInt(membershipData?.memRemainingDays), membershipData?.isTrial,
                                          siData, membershipData?.isSIEnabled) === PlanStatus.INACTIVE}
                                      planPrice={renewalData?.orderInfo?.planPrice}
                                      webEngageData={{
                                          'memStatus': membershipData?.membershipStatus,
                                          'planName': membershipData?.planName,
                                      }}
                                      renewalAction={renewalAction}

        />);
    }
}
