import {StyleSheet} from "react-native";
import spacing from "../../../../../Constants/Spacing";
import {OATextStyle} from "../../../../../Constants/commonstyles";
import dimens from "../../../../../Constants/Dimens";

export default StyleSheet.create({
    mainContainer: {
        flex: spacing.spacing1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    scrollViewContainer: {
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        flex: spacing.spacing1
    },
    imageContainer: {
        flex: spacing.spacing1,
        height: 230,
        flexDirection: 'row'
    },
    imageStyle: {
        alignSelf: 'center',
        height: dimens.height100,
        width: dimens.width100
    },
    textContainer: {
        flex: spacing.spacing1,
    },
    titleTextStyle: {
        ...OATextStyle.TextFontSize_20_bold_212121,
        marginTop: spacing.spacing16
    },
    subTitleTextStyle: {
        ...OATextStyle.TextFontSize_16_normal,
        color: 'rgba(0, 0, 0, 0.87)',
        marginTop: spacing.spacing12
    }
});
