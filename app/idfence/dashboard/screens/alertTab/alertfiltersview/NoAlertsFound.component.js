import React from "react";
import {Image, Text, View} from "react-native";
import spacing from "../../../../../Constants/Spacing";
import styles from "./NoAlertsFound.style";
import {OAImageSource} from "../../../../../Constants/OAImageSource";

export default function NoAlertsFound() {
    return (
        <View style={styles.mainContainer}>
            <View style={styles.imageContainerStyle}>
                <Image style={styles.imageStyle} resizeMode={'cover'} source={OAImageSource.not_alert_found}/>
            </View>
            <View style={{flex: spacing.spacing1}}>
                <Text style={styles.noAlertsFoundTitleStyle}>No alerts found!</Text>
                <Text style={styles.noAlertsFoundSubTitleStyle}>We are monitoring
                    your personal information and social accounts 24*7 . You will get real time alerts if we detect any
                    compromise.</Text>
            </View>
        </View>
    );
}
