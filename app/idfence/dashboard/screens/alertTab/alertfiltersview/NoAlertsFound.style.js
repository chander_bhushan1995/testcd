import {StyleSheet} from "react-native";
import spacing from "../../../../../Constants/Spacing";
import {OATextStyle} from "../../../../../Constants/commonstyles";

export default StyleSheet.create({
    mainContainer: {
        flex: spacing.spacing1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        margin: spacing.spacing16
    },
    imageContainerStyle: {
        flex: spacing.spacing1,
        height: 230,
        flexDirection: 'row'
    },
    imageStyle: {
        alignSelf: 'center',
        height: '100%',
        width: '100%'
    },
    noAlertsFoundTitleStyle: {
        ...OATextStyle.TextFontSize_20_bold_212121,
        marginTop: spacing.spacing16
    },
    noAlertsFoundSubTitleStyle: {
        ...OATextStyle.TextFontSize_16_normal,
        marginTop: spacing.spacing12
    }
});
