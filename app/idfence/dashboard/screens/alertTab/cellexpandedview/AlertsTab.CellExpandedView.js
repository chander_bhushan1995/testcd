import {NativeModules, Platform, Text, View} from "react-native";
import React from "react";

import ViewWithLeftRightLinkButton from "../../../../../CustomComponent/viewwithleftrightlinkbuttons";
import styles from "./AlertsTab.CellExpandedView.style";
import {alertReportFileName, AlertsTabConstants} from "../../../../constants/Constants";
import {
    alertsExpandedViewTitle,
    getCompromisedType,
    getRecommendedActions,
    isBadAlert,
    isReportAlert,
    expandedCellDateOpened, getIDFCommonEventAttr,
} from '../../../../constants/IDFence.UtilityMethods';
import colors from "../../../../../Constants/colors";
import SeparatorView from "../../../../../CustomComponent/separatorview";
import LinkButton from "../../../../../CustomComponent/LinkButton";
import {downloadAlertFileUrl} from "../../../../constants/Urls";
import RecommendedActionsView from "../recommendedactionsview/RecommendedActions.component";


import {PLATFORM_OS} from "../../../../../Constants/AppConstants";
import {LOCATION, WEBENGAGE_IDFENCE_DASHBOARD} from "../../../../../Constants/WebengageAttrKeys";
import {logWebEnageEvent} from "../../../../../commonUtil/WebengageTrackingUtils";
import { parseJSON, deepCopy} from "../../../../../commonUtil/AppUtils";

const nativeBridgeRef = NativeModules.ChatBridge;
const STR_TAG = "AlertsTabCellExpandedView ";
export default function AlertsTabCellExpandedView({membershipData,item, index, selectedIndex, subscriberNo, expanded, ...props}) {
    let expandedCell = null;
    this.bottomSheetRef = React.createRef();

    if (index === selectedIndex && expanded) {
        if (item.itemType === AlertsTabConstants.ALERT_TYPE_NORMAL) {
            if (isReportAlert(item.data)) {
                expandedCell = <View style={styles.expandedMainContainer}>
                    {getViewForReportInfo(item, index)}
                </View>
                return expandedCell;
            }

            if (isBadAlert(item.data)) {
                expandedCell = <View style={styles.expandedMainContainer}>
                    <SeparatorView separatorStyle={{height: 0.2}}/>
                    {getViewForNormalBadAlertInfo(item)}
                    {actionViewForBadAlerts(item, index)}
                </View>
                return expandedCell;
            }

            expandedCell = <View style={styles.expandedMainContainer}>
                {getViewForNormalBadAlertInfo(item)}
            </View>


            return expandedCell;
        }
        if (isBadAlert(item.data)) {
            expandedCell = <View>{getViewForCreditScoreAlertInfo(item)}
                {actionViewForBadAlerts(item, index)}
            </View>
        } else {
            expandedCell = <View>{getViewForCreditScoreAlertInfo(item)}</View>
        }

    }
    expandedCell = <View>
        {expandedCell}
        <RecommendedActionsView ref={this.bottomSheetRef} membershipData = {membershipData}/>
    </View>;
    return expandedCell;


    function getViewForCreditScoreAlertInfo(item) {
        return (<View style={styles.mainContainerWithMinH170}>
            <View>
                <Text
                    style={styles.expandedViewTitleStyle}>{alertsExpandedViewTitle(item)}</Text>
            </View>
            <View style={styles.expandedViewAddInfoContainerStyle}>
                {creditScoreAlertAdditionalInfo(item.data["keyValuePairs"])}
            </View>

        </View>);

    }

    function creditScoreAlertAdditionalInfo(keyValuePairs) {
        let addInfoArr = [];
        if ((keyValuePairs !== null || keyValuePairs !== undefined)) {
            Object.keys(keyValuePairs)
                .forEach(function eachKey(key) {
                    let viewItem = <View style={styles.creditScoreAlertAddInfoContainerStyle}>
                        <View style={styles.compromisedTypeLabelContainerStyle}>
                            <Text style={styles.compromisedTypeLabelStyle}>{key}: </Text>
                        </View>
                        {getViewForKeyValuePair(key, keyValuePairs[key])}

                    </View>;
                    addInfoArr.push(viewItem);
                });
            return addInfoArr;
        }
        return null;

    }

    function getViewForKeyValuePair(key, value) {
        if (key !== null && key !== undefined && key.includes('Date')) {
            value = expandedCellDateOpened(value);
        }
        return <Text
            style={styles.expandedViewTitleStyle}>{value}</Text>;
    }

    function getViewForNormalBadAlertInfo(item) {
        return (<View style={styles.alertDescContainer}>
            <Text
                style={styles.expandedViewTitleStyle}>{alertsExpandedViewTitle(item)}</Text>
            {getViewForAlertCompromisedType(item)}
        </View>);
    }


    function getViewForAlertCompromisedType(item) {
        if (getCompromisedType(item) === "") {
            return null;
        }
        let viewItem = <View style={styles.creditScoreAlertAddInfoContainerStyle}>
            <View style={styles.compromisedTypeLabelContainerStyle}>
                <Text style={styles.compromisedTypeLabelStyle}>Type: </Text>
            </View>
            <Text
                style={styles.expandedViewTitleStyle}>{getCompromisedType(item)}</Text>
        </View>;
        return viewItem;
    }


    function getViewForReportInfo(item, index) {
        if (isBadAlert(item.data)) {
            return (<View>
                <View>
                    <SeparatorView separatorStyle={{height: 0.2}}/>
                    <View style={styles.alertDescContainer}>
                        <Text
                            style={styles.expandedViewTitleStyle}>{alertsExpandedViewTitle(item)}</Text>
                    </View>
                </View>

                {getViewForSingleAction(item, index)}
            </View>);
        }
        return (<View>
            <SeparatorView separatorStyle={{height: 0.2}}/>
            <View style={styles.alertDescContainer}>
                <Text
                    style={styles.expandedViewTitleStyle}>{alertsExpandedViewTitle(item)}</Text>
            </View>

        </View>);
    }


    function actionViewForBadAlerts(item, index) {
        if (getRecommendedActions(item).length > 0 && item.itemType !== AlertsTabConstants.ALERT_TYPE_CREDIT_SCORE) {
            return getViewForActions(item, index);
        }
        if (item.itemType !== AlertsTabConstants.ALERT_TYPE_CREDIT_SCORE) {
            item["action"] = AlertsTabConstants.ACTION_DOWNLOAD_REPORT;
            item["actionTitle"] = AlertsTabConstants.labels.TITLE_DOWNLOAD_REPORT;
            return getViewForSingleAction(item, index);
        }
        item["action"] = AlertsTabConstants.ACTION_RECOMMENDED_ACTIONS;
        item["actionTitle"] = AlertsTabConstants.labels.TITLE_RECOMMENDED_ACTIONS;
        return getViewForSingleAction(item, index);
    }

    function getViewForSingleAction(item, index) {
        return (<View style={styles.actionContainerStyle}>
            <SeparatorView separatorStyle={{backgroundColor: colors.color_F6F6F6}}/>
            <View style={styles.singleActionButtonContainerStyle}>
                <LinkButton
                    style={{actionTextStyle: styles.singleActionButtonStyle}}
                    data={{
                        action: item["actionTitle"]
                    }}
                    onPress={() => actionCallBack(item, index, item["action"])}/>
            </View>
        </View>);
    }

    function getViewForActions(item, index) {
        return (<View style={styles.actionContainerStyle}>
            <ViewWithLeftRightLinkButton
                leftActionTitle={AlertsTabConstants.labels.TITLE_DOWNLOAD_REPORT}
                leftActionType={AlertsTabConstants.ACTION_DOWNLOAD_REPORT}
                rightActionTitle={AlertsTabConstants.labels.TITLE_RECOMMENDED_ACTIONS}
                rightActionType={AlertsTabConstants.ACTION_RECOMMENDED_ACTIONS}
                index={index}
                item={item}
                actionCallBack={actionCallBack}/>
        </View>);
    }

    function actionCallBack(item, index, action) {
        if (action === AlertsTabConstants.ACTION_DOWNLOAD_REPORT) {
            downloadAlertFile(subscriberNo, item?.data?.id, item.title);
            return;
        } else if (action === AlertsTabConstants.ACTION_RECOMMENDED_ACTIONS) {
            recommendedActionsWebEngageEvents();
            if (item.data.hasOwnProperty("recommendedActions")) {
                if (this.bottomSheetRef.current) {
                    this.bottomSheetRef.current.setModalVisible(true, item.data["recommendedActions"], item?.alertType);
                }
            }
        }
    }

    function recommendedActionsWebEngageEvents() {
        let webEngageEvents = getIDFCommonEventAttr(membershipData);
        webEngageEvents[LOCATION] = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_ALERT_DOWNLOAD_REPORT_ATTR_VALUE.ALERT_DETAIL_PAGE;
        webEngageEvents[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.ASSET_NAME] = item?.data?.alertType;
        logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_ALERT_RECOMMENDATION, webEngageEvents);

    }

    function downloadAlertFile(subscriberNo, alertID, title) {
        let webEngageEvents = getIDFCommonEventAttr(membershipData);
        let assetName = item?.data?.alertType;
        webEngageEvents[LOCATION] = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_ALERT_DOWNLOAD_REPORT_ATTR_VALUE.ALERT_DETAIL_PAGE;
        webEngageEvents[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.ASSET_NAME] = assetName;
        logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_ALERT_DOWNLOAD_REPORT, webEngageEvents);

        let currentDate = new Date();
        let formattedDate = `${currentDate.getDate()}-${currentDate.getMonth()}-${currentDate.getFullYear()}_${currentDate.getHours()}:${currentDate.getMinutes()}`;
        nativeBridgeRef.getApiProperty(apiProperty => {
            apiProperty = parseJSON(apiProperty);
            let urlToDownloadTaxInvoiceFile = apiProperty.api_gateway_base_url +
                downloadAlertFileUrl(subscriberNo, alertID);
            nativeBridgeRef.downloadDocument(urlToDownloadTaxInvoiceFile,
                alertReportFileName(title.replace(/ /g, "_"), formattedDate),
                AlertsTabConstants.labels.DOWNLOADING_ALERT_REPORT);
        });
    }


}

