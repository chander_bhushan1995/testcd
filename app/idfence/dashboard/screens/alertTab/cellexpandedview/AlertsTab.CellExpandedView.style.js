import {StyleSheet} from "react-native";
import Dimens from "../../../../../Constants/Dimens";
import colors from "../../../../../Constants/colors";
import spacing from "../../../../../Constants/Spacing";
import {OATextStyle} from "../../../../../Constants/commonstyles";

export default StyleSheet.create({
    mainContainerWithMinH170: {
        flex: Dimens.dimen1,
        minHeight: Dimens.dimen70,
        backgroundColor: colors.color_FAFAFA,
        justifyContent: 'flex-start',
        padding: spacing.spacing16,
    },
    alertDescContainer: {
        flex: Dimens.dimen1,
        minHeight: Dimens.dimen70,
        justifyContent: 'flex-start',
        padding: spacing.spacing16,
    },
    expandedMainContainer: {
        minHeight: Dimens.dimen70,
        backgroundColor: colors.color_FAFAFA
    },
    dataContainerStyle: {
        flex: Dimens.dimen1,
        justifyContent: 'flex-start'
    },
    actionContainerStyle: {
        backgroundColor: colors.color_FAFAFA,
        justifyContent: 'flex-end',
    },
    singleActionButtonStyle: {
        ...OATextStyle.TextFontSize_14_bold,
        color: colors.color_0282F0,
        alignSelf: 'flex-end'
    },
    singleActionButtonContainerStyle: {
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignSelf: 'flex-end'
    },
    expandedViewTitleStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        color: colors.color_888F97,
        flex: spacing.spacing1,
        paddingTop: spacing.spacing4
    },
    expandedViewAddInfoContainerStyle: {
        marginTop: spacing.spacing4
    },
    creditScoreAlertAddInfoContainerStyle:{
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: spacing.spacing5
    },
    compromisedTypeLabelContainerStyle:{
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    compromisedTypeLabelStyle: {
        ...OATextStyle.TextFontSize_14_bold,
        color: colors.color_888F97
    }


});
