import React, {Component,} from 'react';
import {Modal, SafeAreaView, StyleSheet, TouchableOpacity, TouchableWithoutFeedback, View} from 'react-native';
import colors from "../../../../../Constants/colors";
import {OATextStyle} from "../../../../../Constants/commonstyles";
import spacing from "../../../../../Constants/Spacing";
import IDFenceAddBottomSheet from "./IDFenceAddBottomSheet.component";

let refIDFenceAddBottomSheet = React.createRef();
export default class AddDataView extends Component {
    state = {modalVisible: false, isModalCancelable: true, isLoading: false};


    setBottomSheetVisible(visible) {
        this.setState({modalVisible: visible});
    }

    setBottomSheetCancel(status) {
        this.setState({isModalCancelable: status, isLoading: !status});
    }
    setErrorMessage(message) {
        if (refIDFenceAddBottomSheet.current) {
            refIDFenceAddBottomSheet.current.setErrorMessage(message);
        }
    }


    render() {
        if (this.state.isLoading) {
            if (refIDFenceAddBottomSheet.current && !refIDFenceAddBottomSheet.current.getIsLoading()) {
                refIDFenceAddBottomSheet.current.setIsLoading(true);
            }
        }else{
            if (refIDFenceAddBottomSheet.current && refIDFenceAddBottomSheet.current.getIsLoading()) {
                refIDFenceAddBottomSheet.current.setIsLoading(false);
            }
        }
        return (
            <View>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        if (this.state.isModalCancelable) {
                            this.setBottomSheetVisible(!this.state.modalVisible);
                        }
                    }}>
                    <TouchableOpacity
                        activeOpacity={1}
                        style={{
                            flex: 1,
                            flexDirection: 'row',
                            alignItems: 'flex-end',
                            backgroundColor: "#00000077"
                        }}
                        onPress={() => {
                            if (this.state.isModalCancelable) {
                                this.setBottomSheetVisible(false)
                            }
                        }}>

                        <SafeAreaView style={styles.container}>
                            <View style={{
                                alignSelf: 'flex-start',
                                alignItems: 'flex-start',
                                justifyContent: 'flex-start',
                                maxHeight: '80%',
                                flexDirection: 'row',
                                backgroundColor: colors.color_FFFFFF,
                                padding: 0
                            }}>
                                <TouchableWithoutFeedback style={{flex: 1}}>
                                    <View style={{flex: 1}}>
                                        <IDFenceAddBottomSheet
                                            ref={refIDFenceAddBottomSheet}
                                            {...this.props}
                                            onButtonPress={state => {
                                                this.setBottomSheetVisible(state)
                                            }}/>

                                    </View>
                                </TouchableWithoutFeedback>
                            </View>

                        </SafeAreaView>

                    </TouchableOpacity>
                </Modal>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    bottomsheetContainer: {
        marginHorizontal: spacing.spacing20,
        marginTop: spacing.spacing24,
        marginBottom: spacing.spacing16
    },
    title: {
        ...OATextStyle.TextFontSize_14_normal,
        lineHeight: 22,
        color: colors.color_212121
    },
    textInputContainer: {
        width: '100%',
    },
    ActionButtonContainer: {
        height: 48,
    }
});
