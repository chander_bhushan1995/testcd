import spacing from "../../../../../Constants/Spacing";
import {StyleSheet, View, KeyboardAvoidingView, Platform} from "react-native";
import InputBox from "../../../../../Components/InputBox";
import ButtonWithLoader from "../../../../../CommonComponents/ButtonWithLoader";
import React, {forwardRef, useImperativeHandle, useState} from "react";
import {OATextStyle} from "../../../../../Constants/commonstyles";
import colors from "../../../../../Constants/colors";
import {AllAssetKeys} from "../../../../constants/Constants";
import {PLATFORM_OS} from "../../../../../Constants/AppConstants";

const IDFenceAddBottomSheet = forwardRef((props, ref) => {
    let item = props.item;
    const [errorMessage, setErrorMessage] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [text, setText] = useState('');
    const errorPadding = errorMessage === '' ? spacing.spacing0 : spacing.spacing8;
    const keyboardVOffset = (Platform.OS === PLATFORM_OS.android ? 0 : 300);
    useImperativeHandle(ref, () => {
        return {
            setIsLoading: setIsLoading,
            setErrorMessage: setErrorMessage,
            getIsLoading: getIsLoading
        };
    });

    const getIsLoading = () => {
        return isLoading;
    }
    return (
        <KeyboardAvoidingView
            keyboardVerticalOffset={keyboardVOffset} behavior={"padding"}>
            <View style={styles.bottomsheetContainer}>
                <InputBox
                    maxLength={item.maxLength}
                    autoCapitalize={item.type === AllAssetKeys.EMAIL ? '' : 'words'}
                    containerStyle={styles.textInputContainer}
                    inputHeading={item.title}
                    placeholder={item.placeholder}
                    placeholderTextColor={item.placeHolderColor}
                    isMultiLines={item.isMultiLines}
                    keyboardType={item.keyboardType}
                    editable={true}
                    errorMessage={errorMessage}
                    onFocus={() => {
                        setErrorMessage('')
                    }}
                    onChangeText={(text) => {
                        if (errorMessage !== null || errorMessage !== '') {
                            setErrorMessage('')
                        }
                        setText(text);
                        if (text.length === 6) {
                            props.setMinTextForCredDeb(text);
                        }
                    }}
                />
                <View style={[styles.ActionButtonContainer, {marginTop: errorPadding}]}>
                    <ButtonWithLoader
                        isLoading={isLoading}
                        enable={!isLoading}
                        Button={{
                            text: item.btnText,
                            onClick: () => {
                                setErrorMessage('');
                                if (!item.validationMethodRef(text, false)) {
                                    setErrorMessage(item.validationErrorMessage);
                                    return;
                                }
                                if (item?.cardType !== 'CC' || item?.cardType !== 'DC') {
                                    item.cardType = 'DC'
                                }
                                setIsLoading(true);
                                props.onPress(text);
                                props.onButtonPress(true)
                                // this.setBottomSheetVisible(false)

                            },
                        }}
                    />
                </View>
            </View>
        </KeyboardAvoidingView>
    );
});
export default IDFenceAddBottomSheet;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    bottomsheetContainer: {
        marginHorizontal: spacing.spacing20,
        marginTop: spacing.spacing24,
        marginBottom: spacing.spacing16
    },
    title: {
        ...OATextStyle.TextFontSize_14_normal,
        lineHeight: 22,
        color: colors.color_212121
    },
    textInputContainer: {
        width: '100%',
    },
    ActionButtonContainer: {
        height: 48,
    }
});
