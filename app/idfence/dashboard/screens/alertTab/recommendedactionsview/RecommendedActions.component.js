import React, {Component} from 'react';
import {
    FlatList,
    Linking,
    Modal,
    SafeAreaView,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View
} from 'react-native';
import Hyperlink from 'react-native-hyperlink';
import ImageViewFromUrl from "../../../../../CustomComponent/imageviewfromurl";
import {WEBENGAGE_IDFENCE_DASHBOARD} from "../../../../../Constants/WebengageAttrKeys";
import {logWebEnageEvent} from "../../../../../commonUtil/WebengageTrackingUtils";
import styles from "./RecommendedActions.style"
import spacing from "../../../../../Constants/Spacing";
import FullScreenImageView from "../../../../../CustomComponent/fullscreenimageview";
import {getIDFCommonEventAttr} from '../../../../constants/IDFence.UtilityMethods';

const hyperLinksItems = ["click here", "Click here", "Check here"];
let clickedAlertType = '';
let fullScreenImageViewRef = React.createRef();
export default class RecommendedActions extends Component {
    state = {modalVisible: false, recommendedActionsList: []};

    setModalVisible(visible, recommendedActionsList, alertType) {
        clickedAlertType = alertType;
        if (visible) {
            recommendedActionsList.sort(function (item1, item2) {
                return (item2.priority - item1.priority);
            });
            // add unique id
            recommendedActionsList.forEach((element, index) => {
                let uniqueID = new Date().getTime();
                element["id"] = Math.floor((Math.random() * uniqueID) + 1);
                recommendedActionsList[index] = element;
            });
            this.setState({modalVisible: visible, recommendedActionsList: recommendedActionsList});
        }
        this.setState({modalVisible: visible});
    }

    render() {
        function externalLinkWebEngageEvent() {
            let webEngageEvents = getIDFCommonEventAttr(this.props.membershipData);
            if (clickedAlertType !== '') {
                webEngageEvents[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.ASSET_NAME] = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_ASSET_NAME_ATTR_VALUE[clickedAlertType];
                logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_EXTENER_LINK_RECOMMENDED_ACTION, webEngageEvents);
            }
        }

        function handleLinkClick(clickedURL) {
            externalLinkWebEngageEvent();
            Linking.canOpenURL(clickedURL).then(supported => {
                if (supported) {
                    Linking.openURL(clickedURL);
                }
            });
        };

        function renderItem(item, index) {
            let actionPoint = (index + 1) + ". " + item.action;
            let externalUrl = item["externalUrl"];
            let mediaUrl = item["mediaUrl"];// "https://sit6.1atesting.in/static/idfence/shared/images/oneassist-logo.1581059283569.png";
            let replacedSubString = "click here";
            if (externalUrl !== null && externalUrl !== undefined) {
                hyperLinksItems.map(item => {
                    if (actionPoint.indexOf(item) > -1) {
                        replacedSubString = item;
                    }
                });
                actionPoint = actionPoint.split(replacedSubString).join(externalUrl);
            }


            return (
                <View style={styles.hyperlinkContainerStyle}>
                    <Hyperlink
                        onPress={(url) => handleLinkClick(url)}
                        linkStyle={styles.hyperlinkTitleStyle}
                        linkText={url => url === externalUrl ? replacedSubString : url}>
                        <Text style={styles.hyperlinkSubTitleStyle}>{actionPoint}</Text>
                    </Hyperlink>
                    {addActionMedia(mediaUrl)}
                </View>);
        }

        function addActionMedia(mediaUrl) {
            if (mediaUrl !== null && mediaUrl !== undefined) {
                return (<View style={styles.imageViewFromUrlContainerStyle}>
                    <ImageViewFromUrl imageUrl={mediaUrl} onPress={(mediaUrl) => {
                        let webEngageEvents = getIDFCommonEventAttr(this.props.membershipData);
                        webEngageEvents[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.ASSET_NAME] = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_ASSET_NAME_ATTR_VALUE[clickedAlertType];
                        logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_GIF_ICON, webEngageEvents);
                        onImageFullView(mediaUrl)
                    }}/>
                </View>);
            }
            return null;
        }

        function onImageFullView(mediaUrl) {
            if (fullScreenImageViewRef.current) {
                fullScreenImageViewRef.current.setModalVisible(true, mediaUrl);
            }
        }

        return (
            <View>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }}>
                    <TouchableOpacity
                        activeOpacity={1}
                        style={styles.touchableOpacityStyle}
                        onPress={() => {
                            this.setModalVisible(false)
                        }}>

                        <SafeAreaView style={styles.container}>
                            <View style={styles.listContainerStyle}>
                                <TouchableWithoutFeedback style={{flex: spacing.spacing1}}>
                                    <View style={{flex: spacing.spacing1}}>
                                        <Text
                                            style={styles.recommendedActionsTitleStyle}>
                                            Recommended Actions
                                        </Text>
                                        <FlatList
                                            contentContainerStyle={{
                                                flexGrow: 1,
                                            }}
                                            data={this.state.recommendedActionsList}
                                            renderItem={({item, index}) => renderItem(item, index)}
                                            keyExtractor={item => item.id.toString()}/>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                            <FullScreenImageView ref={fullScreenImageViewRef}/>
                        </SafeAreaView>

                    </TouchableOpacity>
                </Modal>
            </View>
        );
    }
}
