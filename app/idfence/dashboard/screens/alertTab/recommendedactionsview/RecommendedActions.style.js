import {StyleSheet} from "react-native";
import {OATextStyle} from "../../../../../Constants/commonstyles";
import colors from "../../../../../Constants/colors";
import spacing from "../../../../../Constants/Spacing";

export default StyleSheet.create({
    container: {
        flex: spacing.spacing1
    },
    hyperlinkTitleStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        lineHeight: spacing.spacing22,
        color: colors.color_2980b9
    },
    hyperlinkContainerStyle: {
        marginTop: spacing.spacing8,
        marginBottom: spacing.spacing8
    },

    imageViewFromUrlContainerStyle: {
        marginTop: spacing.spacing8
    },

    hyperlinkSubTitleStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        lineHeight: spacing.spacing22,
        color: colors.color_212121
    },
    touchableOpacityStyle: {
        flex: spacing.spacing1,
        flexDirection: 'row',
        alignItems: 'flex-end',
        backgroundColor: colors.color_07000000
    },
    listContainerStyle: {
        alignSelf: 'flex-start',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        maxHeight: '80%',
        flexDirection: 'row',
        backgroundColor: colors.color_FFFFFF,
        padding: spacing.spacing16

    },
    recommendedActionsTitleStyle: {
        ...OATextStyle.TextFontSize_10_bold_888F97,
        textTransform: 'uppercase'
    },
});
