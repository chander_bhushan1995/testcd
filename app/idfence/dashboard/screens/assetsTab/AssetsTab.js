import React from "react";
import {
    NativeModules,
    Platform,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import spacing from "../../../../Constants/Spacing";
import colors from "../../../../Constants/colors";

import {CellWithLeftTitleAndSubtitle, ClickableCell} from "../../../../CustomComponent/customcells";
import {addMoreAssetList, AllAssetKeys, AssetsCategories, DiscardedPlanStates} from '../../../constants/Constants';
import SocialCard from "../../../../CustomComponent/socialCard/SocialCard";
import AlertView from "../../../../CommonComponents/alertView/AlertView";
import CreditDebitCard from "../../../../CustomComponent/ccdbCard/CreditDebitCard";
import BankLavelSafety from "../../../../CustomComponent/bankLevelSafety/BankLavelSafety";
import {OATextStyle} from "../../../../Constants/commonstyles";
import OAActivityIndicator from '../../../../CustomComponent/OAActivityIndicator';
import AddDataView from '../alertTab/recommendedactionsview/AddDataView.component';
import {addCyberDataAPI, getCardType, renewMembership, SUCCESS_KEY} from '../IDFenceApiCalls';
import {IDFenceErrorApiErrorMsg} from "../../../../Constants/ApiError.message";
import {ASSET_NAME, LOCATION, WEBENGAGE_IDFENCE_DASHBOARD} from '../../../../Constants/WebengageAttrKeys';
import {logWebEnageEvent} from "../../../../commonUtil/WebengageTrackingUtils";
import {getIDFCommonEventAttr, logWebEngageTabEvent} from '../../../constants/IDFence.UtilityMethods';
import {MoreAboutPremiumPopup, openMoreAboutPremiumPopup} from "../../../../CustomComponent/MoreAboutPremiumPopup";
import {PLATFORM_OS} from "../../../../Constants/AppConstants";
import {TextStyle} from "../../../../Constants/CommonStyle";
import { parseJSON, deepCopy} from "../../../../commonUtil/AppUtils";

const nativeBridgeRef = NativeModules.ChatBridge;
let CCDCType = "CC";
let addItemsBottomSheet = React.createRef();
export default class AssetsTab extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedIndex: -1,
            addMoreData: addMoreAssetList.mobile,
            membershipData: parseJSON(this.props.initialProps?.membershipData),
            renewalData: parseJSON(this.props.initialProps?.renewalData)
        };
    }

    componentWillMount() {
        logWebEngageTabEvent(this.state.membershipData, WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.LOCATION_ATTR_VALUE.ASSETS);
        // BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        // this.props.getOnboardData();
    }

    render() {
        if (this.props.isLoading) {
            return <OAActivityIndicator/>;
        }
        if (this.props.refreshNeeded) {
            this.props.reset();
        }
        return (
            <SafeAreaView style={styles.SafeArea}>
                <ScrollView style={{flex: 1}} keyboardShouldPersistTaps='always'>
                    {this.getAssetList()}
                    <BankLavelSafety data={this.props?.bankLevelSafetyData}/>
                </ScrollView>
                {this.addTrialExpiringPopup()}
                <AlertView ref={ref => (this.AlertViewRef = ref)}/>
            </SafeAreaView>
        );
    }

    addTrialExpiringPopup = () => {
        if (!this.props.premiumPopupData) return null;
        return <MoreAboutPremiumPopup
            height={spacing.spacing56 * 7 + spacing.spacing36}
            data={this.props.premiumPopupData}
            membershipData={this.state.membershipData}
            onPress={() => {
                if (this.props.premiumPopupData.updatePaymentOrRenew) renewMembership(this.props.renewalData, this.handleRenewal);
                else this.openIDFencePlanDetails("");
            }}
        />
    };
    getAssetList = () => {
        let list = this.props.dataSource?.map((item, index) => {
            return this.renderItem(item, index);
        });
        return list;
    };

    renderItem = (item, index) => {
        let warningBadgeText = "";
        if (item?.emailWarnings?.length > 0) {
            let actionText = (item.emailWarnings.length > 1) ? "Pending Actions" : "Pending Action";
            warningBadgeText = `${item.emailWarnings.length} ${actionText}`;
        }
        return (
            <View style={{flex: 1}}>
                <ClickableCell
                    item={item}
                    index={index}
                    onPress={() => this.selectSection(item, index)}
                    titleStyle={{color: colors.charcoalGrey}}
                    subTitleStyle={{color: "#eeaa00"}}
                    badgeStyle={styles.badgeStyle} badgeTextStyle={styles.badgeTextStyle}
                    badgeText={warningBadgeText}

                />
                {this.getExpandedViewOfCell(item, index)}
                <AddDataView ref={addItemsBottomSheet}
                             item={this.state.addMoreData}
                             setMinTextForCredDeb={(text) =>
                                 this.onMinTextForCredDeb(text, this.state?.addMoreData?.type)
                             }
                             onPress={(textFieldData) =>
                                 this.onAddPress(textFieldData)
                             }
                />
            </View>
        );
    };

    onMinTextForCredDeb(text, type) {
        if (type === AllAssetKeys.CREDIT_DEBIT) getCardType(text, this.handleOnMinTextForCredDebResponse)
    }

    handleOnMinTextForCredDebResponse = (response, status) => {
        if (status === SUCCESS_KEY) {
            let cardType = response?.data?.cardCategory;
            if (cardType === "CRC") {
                CCDCType = "CC"
            } else {
                CCDCType = "DC"
            }
        }
    };
    handleRenewal = (response, status) => {
        if (status === SUCCESS_KEY) {
            this.openIDFencePlanDetails(response.data?.payNowLink);
        }
    };
    openIDFencePlanDetails = (link) => {
        let updatePaymentOrRenew = this.props.premiumPopupData.updatePaymentOrRenew;
        if (Platform.OS === PLATFORM_OS.android) {
            this.openIdFence(updatePaymentOrRenew, link).then(value => {

            }).catch(onerror => {
            })
        } else {
            //    open payment page: renewal
            nativeBridgeRef.openIDFencePlanDetails(updatePaymentOrRenew, link);
        }
    };

    async openIdFence(updatePaymentOrRenew, payNowLink) {
        await nativeBridgeRef.openIDFencePlanDetails(updatePaymentOrRenew, payNowLink);
        // TODO: KAG Check if needed?
        nativeBridgeRef.goBack();
    }

    onAddPress(textFieldData) {
        if (addItemsBottomSheet.current) {
            addItemsBottomSheet.current.setBottomSheetCancel(false);
        }
        this.state.addMoreData.cardType = CCDCType;
        addCyberDataAPI(this.state.membershipData?.subscriberNo, this.state.addMoreData, textFieldData, this.handleCyberDataAPIResponse);
    }

    handleCyberDataAPIResponse = (response, status) => {
        if (addItemsBottomSheet.current) {
            addItemsBottomSheet.current.setBottomSheetCancel(true);
            if (status === SUCCESS_KEY) {
                addItemsBottomSheet.current.setBottomSheetVisible(false);
                this.props.refreshTabBarAssets();
                //event
                let attr = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.attrValueForAsset(this.state.addMoreData?.type);
                if (attr !== null && attr !== undefined) {
                    let eventAttr = getIDFCommonEventAttr(this.state.membershipData);
                    eventAttr[LOCATION] = "Asset Bucket";
                    eventAttr[ASSET_NAME] = attr;
                    logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_ASSET_ADDED, eventAttr);
                }
            } else {
                if (response?.message in IDFenceErrorApiErrorMsg) {
                    addItemsBottomSheet.current.setErrorMessage(IDFenceErrorApiErrorMsg[response?.message]);
                    return
                }
                addItemsBottomSheet.current.setErrorMessage(IDFenceErrorApiErrorMsg.DEFAULT);
            }
        }
    };

    selectSection = (item, index) => {
        if (index === this.state.selectedIndex) {
            this.setState({
                selectedIndex: -1
            });
        } else {
            this.setState({
                selectedIndex: index
            });
        }
    };

    deleteCyberData = (item, index) => {
        this.showAlert(
            "Are you sure you want to delete?",
            "This will stop further monitoring of your information",
            item
        );
    };

    openAddAssetPopup = (addMoreData) => {
        if (DiscardedPlanStates.includes(addMoreData.planStatus)) {
            openMoreAboutPremiumPopup(0);
            return
        }
        this.setState({addMoreData: addMoreData});
        if (addItemsBottomSheet.current) {
            addItemsBottomSheet.current.setBottomSheetVisible(true);
        }
        //event
        let attr = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.attrValueForAsset(addMoreData?.type);
        if (attr !== null && attr !== undefined) {
            let eventAttr = getIDFCommonEventAttr(this.state.membershipData);
            eventAttr[LOCATION] = "Asset Bucket";
            eventAttr[ASSET_NAME] = attr;
            logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_ASSET_OPEN_TO_ADD, eventAttr);
        }
    };
    getExpandedViewOfCell = (item, index) => {
        let expandedCell = null;
        if (index === this.state.selectedIndex) {
            switch (item.type) {
                case AssetsCategories.MOBILE:
                    let contactsList = item.telephones?.map(item => {
                        return (
                            <CellWithLeftTitleAndSubtitle
                                item={item}
                                index={index}
                                onPress={() => this.deleteCyberData(item, index)}
                                titleStyle={{color: colors.charcoalGrey}}
                                subTitleStyle={{color: colors.grey}}
                                containerStyle={{backgroundColor: colors.color_FAFAFA}}
                            />
                        );
                    });
                    if (item.addMore !== null && item.addMore !== undefined) {
                        contactsList.push(
                            this.getAddMoreCell(null, "Add more", item.addMore)
                        );
                    }

                    expandedCell = contactsList;
                    break;
                case AssetsCategories.EMAIL:
                    let userPrimaryEmail = this.props.userData?.customerInfo?.email;
                    if (userPrimaryEmail !== null && userPrimaryEmail !== undefined) {
                        let addedPrimaryEmail = {
                            title: userPrimaryEmail,
                            webEngageValue: 'Email',
                            cyberDataElement: "EMAIL",
                            indexKey: 'PRIMARY_EMAIL',
                            subTitle: "(Primary)"
                        };
                        if (item?.emails?.filter(function (element) {
                            return element.indexKey === 'PRIMARY_EMAIL';
                        }).length === 0) {
                            item.emails.unshift(addedPrimaryEmail);
                        }

                    }
                    let emailList = item.emails?.map(emailItem => {
                        let isPendingEmail = item?.emailWarnings?.some(pendingEmail =>{
                            return emailItem.title === pendingEmail.displayName;
                        });
                        emailItem.subTitle = (isPendingEmail) ? "Email Verification Pending" : "";
                        return (
                            <CellWithLeftTitleAndSubtitle
                                item={emailItem}
                                index={index}
                                onPress={() => this.deleteCyberData(emailItem, index)}
                                titleStyle={{color: colors.color_DE000000}}
                                subTitleStyle={isPendingEmail ? {color: colors.salmonRed} : {color: colors.grey}}
                                containerStyle={{backgroundColor: colors.color_FAFAFA}}
                            />
                        );
                    });
                    if (item.addMore !== null && item.addMore !== undefined) {
                        emailList.push(this.getAddMoreCell(null, "Add more", item.addMore));
                    }
                    expandedCell = emailList;
                    break;
                case AssetsCategories.GOVERNMENT_IDS:
                    let govIdsList = item.nationalIds?.map(item => {
                        if (item.isAlreadyAdded) {
                            return (
                                <CellWithLeftTitleAndSubtitle
                                    item={item}
                                    index={index}
                                    onPress={() => this.deleteCyberData(item, index)}
                                    titleStyle={{color: colors.color_DE000000, ...OATextStyle.TextFontSize_16_normal}}
                                    subTitleStyle={{color: colors.grey}}
                                    containerStyle={{backgroundColor: colors.color_FAFAFA}}
                                />
                            );
                        } else {
                            return this.getAddMoreCell(item.title, "Add", item.addMore);
                        }
                    });
                    expandedCell = govIdsList;
                    break;
                case AssetsCategories.BANK_ACCOUNT:
                    let bankAccList = item.bankAccounts?.map(item => {
                        return (
                            <CellWithLeftTitleAndSubtitle
                                item={item}
                                index={index}
                                onPress={() => this.deleteCyberData(item, index)}
                                titleStyle={{color: colors.color_DE000000}}
                                subTitleStyle={{color: colors.grey}}
                                containerStyle={{backgroundColor: colors.color_FAFAFA}}
                            />
                        );
                    });
                    if (item.addMore !== null && item.addMore !== undefined) {
                        bankAccList.push(this.getAddMoreCell(null, "Add more", item.addMore));
                    }

                    expandedCell = bankAccList;
                    break;
                case AssetsCategories.SOCIAL_MEDIA_ACCOUNT:
                    let socialAccList = item.socialAccounts?.map(item => {
                        return (
                            <SocialCard
                                item={item}
                                onPress={value => this.linkDelinkSocial(item, index, value)}
                            />
                        );
                    });
                    expandedCell = socialAccList;
                    break;
                case AssetsCategories.CREDIT_DEBIT:
                    let cardList = item.cards?.map(item => {
                        return (
                            <CreditDebitCard
                                item={item}
                                onPress={() => this.deleteCyberData(item, index)}
                            />
                        );
                    });
                    if (item.addMore !== null && item.addMore !== undefined) {
                        cardList.push(this.getAddMoreCell(null, "Add more", item.addMore));
                    }
                    expandedCell = cardList;
                    break;
                default:
                    expandedCell = (
                        <View
                            style={{
                                marginLeft: 16,
                                width: 50,
                                height: 50,
                                backgroundColor: colors.grey
                            }}
                        />
                    );
            }
        }
        return expandedCell;
    };
    getAddMoreCell = (title, actionTitle, addMoreData) => {
        return (
            <View style={styles.addMoreCellContainer}>
                <Text style={styles.titleStyle}>{title}</Text>
                <View style={styles.addMoreButton}>
                    <TouchableOpacity
                        style={{flex: 1}}
                        onPress={() => {
                            this.openAddAssetPopup(addMoreData);
                        }}
                    >
                        <Text style={{color: colors.blue028}}>{actionTitle}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    };

    linkDelinkSocial = (item, index, value) => {
        if (DiscardedPlanStates.includes(item.planStatus)) {
            openMoreAboutPremiumPopup(0);
            return
        }
        if (value == true) {
            this.props.navigation.navigate("OAWebView", {
                url: item.linkUrl,
                asset: item.socialKey,
                location: "Asset Bucket"
            });
        } else {
            this.props.disconnectSocialMedia(this.state.membershipData?.subscriberNo, item.socialKey)
        }

        //delink

    };

    showAlert(title, alertMessage, item) {
        if (this.AlertViewRef !== null && this.AlertViewRef !== undefined) {
            this.AlertViewRef.showDailog({
                title: title,
                message: alertMessage,
                primaryButton: {
                    text: "Yes",
                    onClick: () => {
                        this.logDeleteAssetEvent(item);
                        this.props?.deleteCyberData(this.state.membershipData?.subscriberNo, item);
                    }
                },
                secondaryButton: {
                    text: "No",
                    onClick: () => {
                        //dismiss
                    }
                },
                cancelable: true,
                onClose: () => {
                }
            });
        }
    }

    logDeleteAssetEvent(item) {
        let webEngageEvents = getIDFCommonEventAttr(this.state.membershipData);
        webEngageEvents[LOCATION] = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ASSET_TAB_ATTR_VALUE;
        webEngageEvents[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.ASSET_NAME] = item?.webEngageValue;
        logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_ASSET_DELETE, webEngageEvents);
    }
}

const handleAction = id => {
};

const styles = StyleSheet.create({
    SafeArea: {
        flex: 1,
        backgroundColor: "white"
    },
    addMoreCellContainer: {
        backgroundColor: colors.color_FAFAFA,
        flexDirection: "row",
        justifyContent: "space-between",
        padding: spacing.spacing16
    },
    addMoreButton: {
        alignItems: "flex-end",
        justifyContent: "center"
    },
    titleStyle: {
        ...OATextStyle.TextFontSize_16_normal
    },
    badgeStyle: {
        alignSelf: 'center',
        marginRight: spacing.spacing10,
        justifyContent: 'flex-end',
        paddingHorizontal: spacing.spacing4,
    },
    badgeTextStyle: {
        ...TextStyle.text_12_regular,
        color: colors.salmonRed,
    },
});
