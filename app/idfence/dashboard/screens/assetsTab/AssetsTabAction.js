import AssetsTab from "./AssetsTab";
import {connect} from "react-redux";
import {Urls} from "../../../constants/Urls";
import { makePostRequest } from "../../../../network/ApiManager";
import {
    hideLoader,
    refreshTabBar,
    reset,
    showLoader,
    showLoaderOnAssetsTab,
    showLoaderOnOverViewTab,
} from '../../../constants/actionCreators';

const mapStateToProps = state => ({
    // dataSource: state.assetsTabReducer.data,
    // subscriberNo: state.dashboardReducer.subscriberNo,
    isLoading: state.dashboardReducer.isLoading,
    dataSource: state.dashboardReducer.assetData,
    bankLevelSafetyData: state.dashboardReducer.bankLevelSafetyData,
    refreshNeeded: state.assetsTabReducer.refreshNeeded,
    userData: state.dashboardReducer.userData,
    premiumPopupData: state.dashboardReducer.premiumPopupData,
});

const mapDispatchToProps = dispatch => ({

    reset: () => {
        dispatch(reset())
    },
    deleteCyberData: (subscriberNo, cyberData) => {
        dispatch(showLoader());
            let body = [{cyberDataElement: cyberData.cyberDataElement, //TELEPHONE
                indexKey:cyberData.indexKey}]; //TELEPHONE1
        makePostRequest(Urls.deleteCyberData + subscriberNo, body, (response, error) => {
                    if (response) {
                        dispatch(hideLoader());
                        dispatch(refreshTabBar());
                    } else {
                        dispatch(hideLoader());
                        dispatch(refreshTabBar()); //dispatch(showError(error));
                    }
        });
    },
    addCyberDataAPI: (subscriberNo, addMoreData, textFieldData) => {
        dispatch(showLoader());
        let {element, dataKey, indexKey, cardType} = addMoreData;
            let data = {
                indexKey: indexKey
            };
            data[dataKey] = textFieldData;
            if(cardType !== null && cardType !== undefined)  data.cardType = cardType;
            let body = [{element: element, data: JSON.stringify(data)}];

        makePostRequest(Urls.addCyberData + subscriberNo, body, (response, error) => {
                    if (response) {
                        dispatch(hideLoader());
                        dispatch(refreshTabBar());
                    } else {
                        dispatch(hideLoader());
                        //dispatch(showError(error));
                    }
        });
    },
    refreshTabBarAssets: () =>{
        dispatch(refreshTabBar());
    },
    disconnectSocialMedia: (subscriberNo, media) => {
        dispatch(showLoader());
            let body = [{}]; //TELEPHONE1
        makePostRequest(Urls.disconnectSocialMedia + subscriberNo + '&socialMediaType=' + media, body, (response, error) => {
                    if (response) {
                        dispatch(hideLoader());
                        dispatch(refreshTabBar());
                    } else {
                        dispatch(hideLoader());
                        dispatch(refreshTabBar()); //dispatch(showError(error));
                    }
        });
    },
    showLoader: () => {
        dispatch(showLoaderOnAssetsTab());
    }

});

export default connect(mapStateToProps, mapDispatchToProps)(AssetsTab);
