import {
  DELETE_CYBER_DATA,
  HIDE_LOADER_ASSETS_TAB,
  RESET_DASHBOARD_TABS,
  SHOW_ERROR,
  SHOW_LOADER_ASSETS_TAB,
} from '../../../constants/actions';

const initialState = {
  isLoading: false,
  refreshNeeded: false,
};

const assetsTabReducer = (state = initialState, action) => {
  switch (action.type) {
    case DELETE_CYBER_DATA:
      return {
        ...state,
        refreshNeeded: true
      };
    case SHOW_ERROR:
      return {
        ...state,
        refreshNeeded: false
      };
    case RESET_DASHBOARD_TABS:
      return {
        ...state,
        refreshNeeded: false
      };
    case SHOW_LOADER_ASSETS_TAB:
      return {
        ...state,
        isLoading: true
      };
    case HIDE_LOADER_ASSETS_TAB:
      return {
        ...state,
        isLoading: false
      };
    default:
      return { ...state };
  }
};
export default assetsTabReducer;
