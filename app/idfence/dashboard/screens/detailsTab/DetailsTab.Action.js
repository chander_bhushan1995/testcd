import {connect} from "react-redux";
import DetailsTab from "./DetailsTab";
import {
    getBasicDetails,
    getDetailsTabMergedData,
    getOnboardData,
    getPaymentCardData,
    getPaymentHistoryData,
    getPreparePlanBenefitsData,
    preparePlanDetailsData,
    refreshDashboard,
    resetFlagsData
} from "../../../constants/actionCreators";
import { makeGetRequest, makePutRequest } from "../../../../network/ApiManager";
import {getHistoryUrl, getPaymentCardUrl} from "../../../constants/Urls";
import {CANCEL_MEMBERSHIP_URL} from "../../../../Constants/ApiUrls";

const mapStateToProps = state => ({
    basicDetails: state.detailsTabReducer.basicDetails,
    paymentHistoryData: state.detailsTabReducer.paymentHistoryData,
    paymentCardData: state.detailsTabReducer.paymentCardData,
    planBenefitsData: state.dashboardReducer.planBenefitsData,
    planDetailsData: state.dashboardReducer.planDetailsData,
    mergedData: state.detailsTabReducer.mergedData,
    isPaymentHistoryLoading: state.detailsTabReducer.isPaymentHistoryLoading,
    isPaymentCardDetailsLoading: state.detailsTabReducer.isPaymentCardDetailsLoading,
    isLoading: state.detailsTabReducer.isLoading,
    showLoader: state.detailsTabReducer.showLoader
});
const mapDispatchToProps = dispatch => ({

    getPaymentHistoryData: (membershipID) => {
        makeGetRequest(getHistoryUrl(membershipID), (response, error) => {
                    if (response) {
                        dispatch(getPaymentHistoryData(response?.data));
                    } else {
                        dispatch(getOnboardData());
                    }
        });
    },
    getPaymentCardData: (memUUID, siData) => {
        makeGetRequest(getPaymentCardUrl(memUUID), (response, error) => {
                    if (response) {
                        let siStatus = siData.status;
                        let responseData = {siStatus: siStatus, response: response?.data};
                        dispatch(getPaymentCardData(responseData));
                    } else {
                        dispatch(getOnboardData());
                    }
        });
    },
    getBasicDetailsData: (customerInfo) => {
        dispatch(getBasicDetails(customerInfo));
    },
    getPreparePlanBenefitsData: (planBenefits) => {
        dispatch(getPreparePlanBenefitsData(planBenefits));
    },
    preparePlanDetailsData: (membership, siStatus) => {
        dispatch(preparePlanDetailsData({membership: membership, siStatus: siStatus}));
    },

    resetFlagsData: () => {
        dispatch(resetFlagsData());
    },
    getMergedData: () => {
        dispatch(getDetailsTabMergedData());
    },
    cancelMembership: (memData, callback) => {
        // dispatch(showLoader());
        makePutRequest(CANCEL_MEMBERSHIP_URL(memData?.memId), (data, error) => {
                    // dispatch(hideLoader);
                    callback(data, error);
        });
    },

    refreshDashboard: (memData) => {
        dispatch(refreshDashboard(memData))
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(DetailsTab);
