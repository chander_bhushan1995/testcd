import {NativeModules, Platform, Text, TouchableOpacity, View} from "react-native";
import React, {useState} from "react";
import {
    DetailsTabRowsActionType,
    DetailsTabRowTypes,
    DetailsTabStrings,
    formattedFileName
} from "../../../constants/Constants";
import MultiColumnCell from "../../../../CustomComponent/customcells/MultiColumnCell";
import LinkButton from "../../../../CustomComponent/LinkButton";
import RightTickWithLabelView from "../../../../CustomComponent/righttickwithlabelview";
import SeparatorView from "../../../../CustomComponent/separatorview";
import styles from "./DetailsTab.style";
import {downloadFileUrl, getExcessPaymentUniqueId} from "../../../constants/Urls";
import {PLATFORM_OS} from "../../../../Constants/AppConstants";
import {WEBENGAGE_IDFENCE_DASHBOARD} from "../../../../Constants/WebengageAttrKeys";
import {logWebEnageEvent} from "../../../../commonUtil/WebengageTrackingUtils";
import {renewMembership, SUCCESS_KEY} from "../IDFenceApiCalls";
import OAActivityIndicator from "../../../../CustomComponent/OAActivityIndicator";
import {getIDFCommonEventAttr} from '../../../constants/IDFence.UtilityMethods';
import {TextStyle} from "../../../../Constants/CommonStyle";
import spacing from "../../../../Constants/Spacing";
import colors from "../../../../Constants/colors";
import { parseJSON, deepCopy} from "../../../../commonUtil/AppUtils";

const nativeBridgeRef = NativeModules.ChatBridge;
export default function DetailsTabExpandedView({item, index, selectedIndex, callback, ...props}) {
    const [showLoading, setShowLoading] = useState(false);
    let expandedCell = null;
    let membershipData = parseJSON(props.initialProps?.membershipData);
    if (index === selectedIndex) {
        let rowData = [];
        item.expandedListData.map((item, index) => {
            let itemArr = [item?.title, item?.value];
            rowData.push(itemArr);
        });
        switch (item?.rowType) {
            case DetailsTabRowTypes.ROW_TYPE_TWO_COLUMN:
                expandedCell =
                    <View>
                        <MultiColumnCell data={rowData}/>
                        {
                            (item.title === DetailsTabStrings.titlePlanDetails && membershipData?.isSIEnabled && membershipData?.isTrial) &&
                            <TouchableOpacity onPress={() => {
                                callback();
                            }}>
                                <View style={{
                                    height: 56,
                                    backgroundColor: '#FAFAFA',
                                    flexDirection: 'row',
                                    paddingHorizontal: spacing.spacing16
                                }}>
                                    <Text style={{
                                        ...TextStyle.text_14_bold,
                                        color: colors.blue,
                                        alignSelf: 'center'
                                    }}>{DetailsTabStrings.titleCancelMembership}</Text>
                                </View>
                            </TouchableOpacity>
                        }

                    </View>
                break;
            case DetailsTabRowTypes.ROW_TYPE_TWO_COLUMN_WITH_RIGHT_BUTTON:
                if (DetailsTabRowsActionType.ACTION_UPDATE_PAYMENT_DETAILS == item?.actionType) {
                    rowData.length = 0;
                    item.expandedListData.map((item, index) => {
                        let itemArr = [item?.title, item?.value, item?.imageSource];
                        rowData.push(itemArr);
                    });
                    let updatePaymentDetails = <View style={styles.updatePaymentDetailsContainerStyle}>
                        <View style={{display: showLoading ? 'none' : 'flex'}}>
                            <LinkButton
                                onPress={e => {
                                    onUpdatePaymentDetails(item);
                                }}
                                style={{
                                    actionTextStyle: styles.linkButtonTextStyle
                                }}
                                data={{
                                    action: DetailsTabStrings.actionUpdateLabel
                                }}/>

                        </View>
                        <View style={{display: showLoading ? 'flex' : 'none'}}>
                            <View style={{width: 12, height: 12, paddingRight: 16}}>
                                <OAActivityIndicator/>
                            </View>
                        </View>
                    </View>

                    if (DetailsTabStrings.siStatusArr.includes(props.otherData?.siData?.status)) {
                        expandedCell = <View>
                            <MultiColumnCell data={rowData}/>
                            {updatePaymentDetails}
                        </View>;
                    } else {
                        expandedCell = <View>
                            <MultiColumnCell data={rowData}/>
                        </View>;
                    }

                } else {
                    rowData.length = 0;
                    item.expandedListData.map((item, index) => {
                        let itemArr = [item?.paymentDate, item?.salesPrice, item?.imageSource];
                        rowData.push(itemArr);
                    });
                    expandedCell = <MultiColumnCell data={rowData}
                                                    styles={{
                                                        TextStyle0: styles.multiColumnCellStyle,
                                                        TextStyle1: styles.multiColumnCellStyle
                                                    }}
                                                    onRightClick={(e) => onDownloadPdfClick(e)}

                    />
                }
                break;
            case DetailsTabRowTypes.ROW_TYPE_SINGLE_COLUMN:
                rowData.length = 0;
                item.expandedListData.map((item, index) => {
                    rowData.push(
                        <View style={styles.singleRowStyle}>
                            <RightTickWithLabelView label={item}/>
                        </View>
                    );
                });
                expandedCell = <View>
                    <View style={styles.expandedCellSingleRowStyle}>{rowData}</View>
                    <SeparatorView separatorStyle={styles.SeparatorViewStyle}/>
                </View>
                break;
            default:
                expandedCell = <MultiColumnCell data={rowData}/>
        }
    }
    return expandedCell;

    function onDownloadPdfClick(item) {
        logUpdatePdfDownloadEvent();
        nativeBridgeRef.getApiProperty(apiProperty => {
            apiProperty = parseJSON(apiProperty);
            let urlToDownloadTaxInvoiceFile = apiProperty.api_gateway_base_url + downloadFileUrl(item?.orderId, item?.subActivity);
            nativeBridgeRef.downloadDocument(urlToDownloadTaxInvoiceFile,
                formattedFileName(item?.orderId),
                DetailsTabStrings.msgDownloadingTaxInvoice);
        });
    };


    function logUpdatePaymentEvent() {
        let membershipData = parseJSON(props.initialProps?.membershipData)
        let webEngageEvents = getIDFCommonEventAttr(membershipData);
        webEngageEvents[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.PLAN_NAME] = membershipData?.planName;
        webEngageEvents[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.SI_STATUS] = props.otherData?.siData?.status;
        webEngageEvents[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.MEMBERSHIP_STATUS] = membershipData?.membershipStatus;
        logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_PAYMENT_DETAILS_UPDATE, webEngageEvents);
    }

    function logUpdatePdfDownloadEvent() {
        let membershipData = parseJSON(props.initialProps?.membershipData)
        let webEngageEvents = getIDFCommonEventAttr(membershipData);
        webEngageEvents[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.PLAN_NAME] = membershipData?.planName;
        webEngageEvents[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.MEMBERSHIP_STATUS] = membershipData?.membershipStatus;
        logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_PAYMENT_DETAILS_DOWNLOAD_INVOICE, webEngageEvents);
    }

    function onUpdatePaymentDetails(item) {
        logUpdatePaymentEvent();
        if (props.otherData?.siData?.status === 'F') {
            setShowLoading(true);
            renewMembership(props.otherData?.renewalData, handleRenewal);
        } else {
            nativeBridgeRef.getApiProperty(apiProperty => {
                apiProperty = parseJSON(apiProperty);
                let paymentURL = apiProperty.api_gateway_base_url + getExcessPaymentUniqueId(DetailsTabStrings.paramValueSI, item?.memId);
                props.navigation.navigate("PaymentWebViewComponent", {
                    paymentLink: paymentURL,
                    isFromIDFence: true,
                    otherParam: DetailsTabStrings.titlePaymentReview
                });

            });
        }
    };

    function handleRenewal(response, status) {

        if (Platform.OS === PLATFORM_OS.android) {
            //FIXME:- : send <updatePaymentOrRenew> in param
            openIdFence(true, response.data?.payNowLink).then(value => {
                setShowLoading(false);
            }).catch(onerror => {
            })
        } else {
            if (status === SUCCESS_KEY) {
                //    open payment page: renewal
                nativeBridgeRef.openIDFencePlanDetails(true, response.data?.payNowLink);
            }
        }
    };

    async function openIdFence(updatePaymentOrRenew, payNowLink) {
        await nativeBridgeRef.openIDFencePlanDetails(updatePaymentOrRenew, payNowLink);
    }
}

