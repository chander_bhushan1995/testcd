import {
    CANCEL_MEMBERSHIP,
    GET_BASIC_DETAILS_DATA,
    GET_DETAILS_TAB_MERGED_DATA,
    GET_PAYMENT_CARD_DATA,
    GET_PAYMENT_HISTORY_DATA, HIDE_LOADER, MEMBERSHIP_CANCELLED,
    PREPARE_PLAN_BENEFITS_DATA,
    PREPARE_PLAN_DETAILS_DATA,
    RESET_FLAGS, SHOW_LOADER
} from "../../../constants/actions";
import {formatDateIn_DD_MMM_yyyy, formatDateIn_MMM_DD_yyyy} from "../../../../commonUtil/Formatter";
import {
    DetailsTabRowsActionType,
    DetailsTabRowTypes,
    DetailsTabStrings,
    formatSalesPrice,
} from "../../../constants/Constants";

import {OAImageSource} from "../../../../Constants/OAImageSource";
import {isMemTrialEndOrExpired, memEndOn} from "../../../constants/IDFence.UtilityMethods";

const initialState = {
    basicDetails: {},
    paymentHistoryData: {},
    paymentCardData: {},
    planBenefitsData: {},
    planDetailsData: {},
    mergedData: {},
    isPaymentHistoryLoading: true,
    isPaymentCardDetailsLoading: true,
    isLoading: true,
    isCancellingMembership: false,
    showLoader:false
}
const detailsTabReducer = (state = initialState, action) => {

    switch (action.type) {

        case GET_PAYMENT_HISTORY_DATA:
            let paymentListFromApi = action.data;
            let expandedListData = [];
            Object.keys(paymentListFromApi)
                .forEach(function eachKey(key) {
                    let billingDate = new Date(paymentListFromApi[key]?.billingDate);
                    let month = String(billingDate.getMonth() + 1);
                    let day = String(billingDate.getDate());
                    let year = String(billingDate.getFullYear());
                    let formattedBillingDate = formatDateIn_MMM_DD_yyyy(day + "/" + month + "/" + year);

                    let item = {
                        orderId: paymentListFromApi[key]?.orderId,
                        paymentDate: formattedBillingDate,
                        subActivity: paymentListFromApi[key]?.orderId,
                        salesPrice: formatSalesPrice(paymentListFromApi[key]?.salesPrice),
                        imageSource: {
                            url: require("../../../../images/pdf_download.webp"),
                            label: DetailsTabStrings.pdfLabel,
                            orderId: paymentListFromApi[key]?.orderId,
                            subActivity: paymentListFromApi[key]?.subActivity
                        }

                    };
                    expandedListData.push(item);
                });
            let paymentHistoryData = {
                title: DetailsTabStrings.titlePaymentHistory,
                rowType: DetailsTabRowTypes.ROW_TYPE_TWO_COLUMN_WITH_RIGHT_BUTTON,
                expandedListData: expandedListData,
                imageSource: OAImageSource.chevron_down

            };
            return {
                ...state,
                paymentHistoryData: paymentHistoryData,
                isPaymentHistoryLoading: false
            };
        case GET_PAYMENT_CARD_DATA:
            let paymentCardData = {};
            let apiResponseData = action?.data?.response;
            if (apiResponseData && apiResponseData.length > 0) {
                let actionData = apiResponseData[0];
                let expandedListData = [];
                expandedListData.push({
                    title: DetailsTabStrings.titlePaymentMode,
                    value: actionData?.cardTypeName,
                });
                expandedListData.push({
                    title: DetailsTabStrings.titleCardNumber,
                    value: actionData?.instrumentNo,
                   // imageSource: {url: require("../../../../images/idf_master_card.webp")}
                });
                paymentCardData = {
                    title: DetailsTabStrings.titlePaymentDetails,
                    subTitle: (action?.data?.siStatus === DetailsTabStrings.siStatus_F ? "Last Payment Failed " : ""),
                    siStatus: action?.data?.siStatus,
                    memId: actionData?.memId,
                    actionType: DetailsTabRowsActionType.ACTION_UPDATE_PAYMENT_DETAILS,
                    rowType: DetailsTabRowTypes.ROW_TYPE_TWO_COLUMN_WITH_RIGHT_BUTTON,
                    expandedListData: expandedListData,
                    imageSource: OAImageSource.chevron_down
                };

            }
            return {
                ...state,
                paymentCardData: paymentCardData,
                isPaymentCardDetailsLoading: false
            };
        case GET_BASIC_DETAILS_DATA:
            //Basic Details
            let basicDetails = {};
            if (action.data && action.data.length > 0) {
                let actionData = action.data[0];
                let expandedListData = [];
                expandedListData.push({
                    title: DetailsTabStrings.titleName,
                    value: actionData?.firstName
                });
                expandedListData.push({
                    title: DetailsTabStrings.titleEmail,
                    value: actionData?.email
                });
                expandedListData.push({
                    title: DetailsTabStrings.titlePhoneNumber,
                    value: actionData?.mobileNo
                });
                basicDetails = {
                    title: DetailsTabStrings.titleBasicDetails,
                    subTitle: "",
                    rowType: DetailsTabRowTypes.ROW_TYPE_TWO_COLUMN,
                    expandedListData: expandedListData,
                    imageSource: OAImageSource.chevron_down
                };
            }
            return {
                ...state,
                basicDetails: basicDetails
            };
        case PREPARE_PLAN_DETAILS_DATA:
            //Basic Details
            let planDetails = {};
            if (Object.keys(action.data).length !== 0) {
                planDetails = action.data?.membership;
                let planName = planDetails?.planName;
                let memId = planDetails?.memId;
                // formatted start date
                let startDate = +planDetails?.startDate;
                let endDate = +planDetails?.endDate;
                let date = new Date(startDate);
                let month = String(date.getMonth() + 1);
                let day = String(date.getDate());
                let year = String(date.getFullYear());
                let formattedStartDate = formatDateIn_DD_MMM_yyyy((day + "/" + month + "/" + year));
                // end date
                date = new Date(endDate);
                month = String(date.getMonth() + 1);
                day = String(date.getDate());
                year = String(date.getFullYear());
                let formattedEndDate = formatDateIn_DD_MMM_yyyy((day + "/" + month + "/" + year));
                // membership status
                let membershipStatus = "INACTIVE";
                let siStatus = action.data?.siStatus;
                let isMemExpired = isMemTrialEndOrExpired(planDetails);
                let memEndDate=formattedEndDate;
                let memEndTitle=DetailsTabStrings.titleEndIn;

                if (planDetails?.memRemainingDays > 0) {
                    membershipStatus = "ACTIVE";
                } else if (!isMemExpired) {
                    if (siStatus === 'A') {
                        membershipStatus = "ACTIVE";
                    }else{
                        membershipStatus = "EXPIRED";
                    }
                }else{
                    memEndDate=memEndOn(planDetails)+" day(s) ago";
                    memEndTitle=DetailsTabStrings.titleEndOn;
                }

                let expandedListData = [];
                expandedListData.push({
                    title: DetailsTabStrings.titlePlanName,
                    value: planName
                });
                expandedListData.push({
                    title: DetailsTabStrings.titleMembershipID,
                    value: memId
                });
                expandedListData.push({
                    title: DetailsTabStrings.titlePlanStatus,
                    value: membershipStatus
                });
                expandedListData.push({
                    title: DetailsTabStrings.titleStartDate,
                    value: formattedStartDate
                });
                expandedListData.push({
                    title: memEndTitle,
                    value: memEndDate
                });


                planDetails = {
                    title: DetailsTabStrings.titlePlanDetails,
                    rowType: DetailsTabRowTypes.ROW_TYPE_TWO_COLUMN,
                    expandedListData: expandedListData,
                    imageSource: OAImageSource.chevron_down
                }
            }
            return {
                ...state,
                planDetailsData: planDetails
            };
        case PREPARE_PLAN_BENEFITS_DATA:
            let formattedPlanList = [];
            let planBenefitsData = {};
            if (Object.keys(action.data).length !== 0) {
                let benefitList = action.data[0]?.benefitList;
                benefitList.map(item => {
                    formattedPlanList.push(item?.benefit);
                });
                planBenefitsData = {
                    title: DetailsTabStrings.titlePlanBenefits,
                    rowType: DetailsTabRowTypes.ROW_TYPE_SINGLE_COLUMN,
                    expandedListData: formattedPlanList,
                    imageSource: OAImageSource.chevron_down

                };
            }
            return {
                ...state,
                planBenefitsData: planBenefitsData
            };
        case GET_DETAILS_TAB_MERGED_DATA:
            let mergedData = [];
            if (Array.isArray(state.planDetailsData?.expandedListData) && state.planDetailsData?.expandedListData.length) {
                mergedData.push(state.planDetailsData);
            }
            if (Array.isArray(state.paymentCardData?.expandedListData) && state.paymentCardData?.expandedListData.length) {
                mergedData.push(state.paymentCardData);
            }
            if (Array.isArray(state.basicDetails?.expandedListData) && state.basicDetails?.expandedListData.length) {
                mergedData.push(state.basicDetails);
            }
            if (Array.isArray(state.planBenefitsData?.expandedListData) && state.planBenefitsData?.expandedListData.length) {
                mergedData.push(state.planBenefitsData);
            }
            if (Array.isArray(state.paymentHistoryData?.expandedListData) && state.paymentHistoryData?.expandedListData.length) {
                mergedData.push(state.paymentHistoryData);
            }


            let planFAQsData = {
                title: DetailsTabStrings.titleFAQs,
                actionType: DetailsTabRowsActionType.ACTION_OPEN_FAQ,
                imageSource: OAImageSource.chevron_down

            };
            mergedData.push(planFAQsData);
            return {
                ...state,
                mergedData: mergedData,
                isLoading: false
            };
        case RESET_FLAGS:
            return {
                ...state,
                isPaymentCardDetailsLoading: true,
                isPaymentHistoryLoading: true,
                isLoading: true
            };

        case SHOW_LOADER:
            return {
                ...state,
                showLoader: true
            };
        case HIDE_LOADER:
            return {
                ...state,
                showLoader: false
            };

        default:
            return {...state};
    }
};
export default detailsTabReducer;
