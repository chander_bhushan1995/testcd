import React from "react";
import {Linking, NativeModules, SafeAreaView, ScrollView, Text, TouchableOpacity, View} from "react-native";
import colors from "../../../../Constants/colors";
import {OATextStyle} from "../../../../Constants/commonstyles";
import OAActivityIndicator from "../../../../CustomComponent/OAActivityIndicator";
import {DetailsTabRowsActionType, DetailsTabStrings} from "../../../constants/Constants";
import LinkButton from "../../../../CustomComponent/LinkButton";
import styles from "./DetailsTab.style";
import DetailsTabExpandedView from "./DetailsTab.ExpandedView";
import {getIDFenceFQAUrl} from "../../../constants/Urls";
import ClickableCell from "../../../../CustomComponent/customcells/clickableCell/ClickableCell";
import {ASSET_NAME, LOCATION, WEBENGAGE_IDFENCE_DASHBOARD} from "../../../../Constants/WebengageAttrKeys";
import {getIDFCommonEventAttr, logWebEngageTabEvent} from '../../../constants/IDFence.UtilityMethods';
import {logWebEnageEvent} from "../../../../commonUtil/WebengageTrackingUtils";
import {TextStyle} from "../../../../Constants/CommonStyle";
import {MEMBERSHIP_DETAILS} from "../../../../Constants/AppConstants";
import spacing from '../../../../Constants/Spacing';
import AlertView from '../../../../CommonComponents/alertView/AlertView';
import DialogView from '../../../../Components/DialogView';
import PlatformActivityIndicator from '../../../../CustomComponent/PlatformActivityIndicator';
import { parseJSON, deepCopy} from "../../../../commonUtil/AppUtils";


const nativeBridgeRef = NativeModules.ChatBridge;
let checkItemList = [];
export default class DetailsTab extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: -1,
            membershipData: parseJSON(this.props.initialProps?.membershipData),
            siStatus: this.props.otherData?.siData?.status,
            showLoader: false,
        };
    }

    componentWillMount() {
        logWebEngageTabEvent(this.state.membershipData, WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.LOCATION_ATTR_VALUE.DETAILS);
        this.callApiForDetailsTab();
        this.props.navigation.addListener(
            'didFocus', payload => {
                this.props.resetFlagsData();
                this.callApiForDetailsTab();
            }
        );
    }

    callApiForDetailsTab() {
        checkItemList = [];
        this.props.getPaymentHistoryData(this.state.membershipData?.memId);
        this.props.getPreparePlanBenefitsData(this.props.otherData?.planBenefitsData);
        if (this.state.siStatus !== 'NA') {
            this.props.getPaymentCardData(this.state.membershipData?.subscriberNo, this.props.otherData?.siData);
        }
        this.props.getBasicDetailsData(this.props.dataSource);
        this.props.preparePlanDetailsData(this.state.membershipData, this.state.siStatus);
    }

    render() {
        if (this.state.siStatus !== 'NA' && (this.props.isPaymentHistoryLoading || this.props.isPaymentCardDetailsLoading)) {
            return <OAActivityIndicator/>;
        } else if (this.props.isLoading) {
            this.props.getMergedData();
            return <OAActivityIndicator/>;
        }
        return (
            <SafeAreaView style={styles.SafeArea}>
                <ScrollView style={{flex: 1}}>
                    <View>
                        <View>
                            <Text style={{
                                ...TextStyle.text_10_bold,
                                paddingHorizontal: spacing.spacing20,
                                paddingVertical: spacing.spacing16
                            }}>
                                {MEMBERSHIP_DETAILS}
                            </Text>
                            <View style={{height: 0.2, backgroundColor: colors.greyf2}}/>
                        </View>
                        {this.getAssetList()}
                        <View style={styles.needHelpContainerStyle}>
                            <Text style={OATextStyle.TextFontSize_14_normal}>Need help? Contact us
                                at </Text>
                            <TouchableOpacity onPress={() => this.makeCall(DetailsTabStrings.supportNumber)}>
                                <Text
                                    style={[OATextStyle.TextFontSize_14_bold, {color: colors.color_0282F0}]}>{DetailsTabStrings.supportNumber}</Text>
                            </TouchableOpacity>
                            <Text>,</Text>
                            <TouchableOpacity onPress={() => this.makeCall(DetailsTabStrings.supportNumber2)}>
                                <Text style={[OATextStyle.TextFontSize_14_bold, {
                                    color: colors.color_0282F0,
                                    paddingLeft: 5
                                }]}>{DetailsTabStrings.supportNumber2}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
                <DialogView ref={ref => (this.AlertViewRef = ref)}/>
                {this.state.showLoader ? <View style={styles.loaderContainer}><PlatformActivityIndicator /></View> : null}
            </SafeAreaView>
        );
    }

    getAssetList = () => {
        let list = this.props.mergedData.map((item, index) => {
            return this.renderItem(item, index)
        })
        return list
    }

    showMemCancelAlert = () => {
        if (this.AlertViewRef !== null && this.AlertViewRef !== undefined) {
            this.AlertViewRef.showDailog({
                title: DetailsTabStrings.cancelMemConfirmTitle,
                message: DetailsTabStrings.cancelMemConfirmDesc,
                primaryButton: {
                    text: DetailsTabStrings.continueMem,
                    onClick: () => {
                    }
                },
                secondaryButton: {
                    text: DetailsTabStrings.cancelMem,
                    onClick: () => {
                        let eventAttr = getIDFCommonEventAttr(this.state.membershipData);
                        eventAttr[LOCATION] = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.MEMBERSHIP_DETAIL_TAB;
                        logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_FENCE_MEM_CONFIRM_CANCEL,eventAttr)
                        this.setState({showLoader: true})
                        let memData = parseJSON(this.props.initialProps?.membershipData);
                        if (memData !== null && memData !== undefined) {
                            this.props.cancelMembership(memData, (data, error) => {
                                this.setState({showLoader: false})
                                if (data?.status === 'success') {
                                    memData.isSIEnabled = false;
                                    this.showMemCancelled(memData);
                                    let eventAttr = getIDFCommonEventAttr(memData);
                                    eventAttr[LOCATION] = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.MEMBERSHIP_DETAIL_TAB;
                                    logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_FENCE_CANCEL_MEMBERSHIP_COMPLETE, eventAttr);
                                }else{
                                }
                            });
                        }
                    }
                },
                cancelable: false,
                onClose: () => {
                }
            });
        }
    }

    refreshDashboardAndMemTab = (memData)=>{
        let initialProps = {...this.props.initialProps};
        initialProps.membershipData = JSON.stringify(memData);
        nativeBridgeRef.refreshMemberships();
        this.props.refreshDashboard(initialProps);
    }

    showMemCancelled = (memData) => {
        if (this.AlertViewRef !== null && this.AlertViewRef !== undefined) {
            this.AlertViewRef.showDailog({
                title: DetailsTabStrings.memCancelledTitle,
                message: DetailsTabStrings.memCancelledDesc,
                primaryButton: {
                    text: DetailsTabStrings.okay,
                    onClick: () => {
                        this.refreshDashboardAndMemTab(memData)
                    }
                },
                cancelable: false,
                onClose: ()=>{this.refreshDashboardAndMemTab(memData)}
            });
        }
    }

    onCancelMembershipClick = () => {
        let eventAttr = getIDFCommonEventAttr(this.state.membershipData);
        eventAttr[LOCATION] = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.MEMBERSHIP_DETAIL_TAB;
        logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_FENCE_MEM_START_CANCEL,eventAttr)
        this.showMemCancelAlert()
    }

    renderItem = (item, index) => {
        return (
            <View style={styles.CellWithLeftTitleAndSubtitleStyle}>
                <ClickableCell item={item}
                               index={index}
                               onPress={(e) => this.handleAction(e, item, index)}
                               titleStyle={{color: colors.charcoalGrey}}
                               subTitleStyle={{color: colors.color_CF021B}}
                />
                <DetailsTabExpandedView item={item} index={index}
                                        selectedIndex={this.state.selectedIndex}
                                        callback={this.onCancelMembershipClick} {...this.props}/>
            </View>
        );
    };

    handleAction = (e, item, index) => {
        e.stopPropagation();
        let webEngageEvents = getIDFCommonEventAttr(this.state.membershipData);
        webEngageEvents[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.BUCKET_NAME] = item?.title;
        logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_DETAIL_BUCKET, webEngageEvents);
        if (DetailsTabRowsActionType.ACTION_OPEN_FAQ === item.actionType) {
            nativeBridgeRef.getApiProperty(apiProperty => {
                apiProperty = parseJSON(apiProperty);
                let tncUrlObj = getIDFenceFQAUrl(apiProperty.api_base_url);
                this.props.navigation.navigate('TnCWebViewComponent', {
                    productTermAndCUrl: tncUrlObj,
                    enableNativeBack: false,
                    otherParam: DetailsTabStrings.titleFAQs
                });
            });
        } else {
            if (index === this.state.selectedIndex) {
                this.setState({
                    selectedIndex: -1,
                })
            } else {
                this.setState({
                    selectedIndex: index,
                });
            }
        }
    };


    makeCall(supportNumber) {
        Linking.openURL(`tel:${supportNumber}`);
    };
}
