import {StyleSheet} from 'react-native';
import colors from '../../../../Constants/colors';
import spacing from '../../../../Constants/Spacing';
import Dimens from '../../../../Constants/Dimens';
import {OATextStyle} from '../../../../Constants/commonstyles';


export default StyleSheet.create({
    SafeArea: {
        flex: spacing.spacing1,
        backgroundColor: 'white',
    },
    cellContainerStyle: {
        marginVertical: spacing.spacing16,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    needHelpContainerStyle: {
        margin: spacing.spacing16,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
    },
    singleRowStyle: {
        paddingTop: spacing.spacing6,
        paddingBottom: spacing.spacing6,

    },
    SeparatorViewStyle: {
        backgroundColor: colors.color_F6F6F6,
    },
    imgCross: {
        width: Dimens.dimen16,
        height: Dimens.dimen16,
    },
    updatePaymentDetailsContainerStyle: {
        height: Dimens.dimen56,
        paddingLeft: spacing.spacing16,
        paddingRight: spacing.spacing16,
        backgroundColor: colors.color_FAFAFA,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    linkButtonTextStyle: {
        ...OATextStyle.TextFontSize_14_bold,
        color: colors.color_0282F0,
    },
    multiColumnCellStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        flex: spacing.spacing1,
    },
    CellWithLeftTitleAndSubtitleStyle: {
        flex: spacing.spacing1,
    },
    spinnerTextStyle: {
        color: '#FFF',
    },
    expandedCellSingleRowStyle: {
        margin: spacing.spacing16,
    },
    loaderContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        height: '100%',
        width: '100%',
        backgroundColor: colors.transparent,
        flex: 1,
    },
});
