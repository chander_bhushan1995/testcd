import React from 'react';

import {Image, NativeModules, Platform, SafeAreaView, ScrollView, Text, View} from 'react-native';

import AlertsOverviewCard from '../../../../CustomComponent/alertsOverviewCard/AlertsOverviewCard';
import spacing from '../../../../Constants/Spacing';
import {ID_FENCE_SHARE_LINK} from '../../../../Constants/AppConstants';
import TodoCard from '../../../../CustomComponent/todoCard/TodoCard';
import OACarousel from '../../../../CommonComponents/OACarousel';
import AccountsCard from '../../../../CustomComponent/AccountsCard';
import OALinearGradient from '../../../../CommonComponents/OALinearGradient';
import OAActivityIndicator from '../../../../CustomComponent/OAActivityIndicator';
import {CommonStyle, TextStyle} from '../../../../Constants/CommonStyle';
import {ID_FENCE_BLOG, PLATFORM_OS, THANK_YOU_FOR_BLOG_SUBSCRIPTION} from '../../../../Constants/AppConstants';
import {
    addMoreAssetList,
    AlertsTabConstants, AllAssetKeys as ID_ASSET_NAME_ATTR_VALUE,
    AllAssetKeys,
    DiscardedPlanStates,
    OverviewCards,
    PlanStatus,
    SkippableCards,
} from '../../../constants/Constants';
import OverviewTabWarningView from './OverviewTabWarningView.component';
import {alertsOverviewStyle, IDCardStyle, SocialCardStyle, styles} from './styles';
import IDFenceBuyStickyView from '../../../../CustomComponent/idfencebuystickyview';
import {
    getIDFCommonEventAttr,
    isMemTrialEndOrExpired,
    logWebEngageTabEvent,
} from '../../../constants/IDFence.UtilityMethods';

import AddDataView from '../alertTab/recommendedactionsview/AddDataView.component';
import DialogView from '../../../../Components/DialogView';
import {addCyberDataAPI, getCardType, renewMembership, SUCCESS_KEY} from '../IDFenceApiCalls';
import {IDFenceErrorApiErrorMsg} from '../../../../Constants/ApiError.message';
import {logWebEnageEvent} from '../../../../commonUtil/WebengageTrackingUtils';
import {ASSET_NAME, EMAIL_ID, LOCATION, WEBENGAGE_IDFENCE_DASHBOARD} from '../../../../Constants/WebengageAttrKeys';
import CreditScoreCircularSliderView from '../../../creditscorecircularsliderview';
import colors from '../../../../Constants/colors';
import {MoreAboutPremiumPopup, openMoreAboutPremiumPopup} from '../../../../CustomComponent/MoreAboutPremiumPopup';
import BlogCard from '../../../../CustomComponent/BlogCard';
import OAHorizontalFlatList from '../../../../CommonComponents/OAHorizontalFlatList';
import ButtonWithLoader from '../../../../CommonComponents/ButtonWithLoader';
import WhiteButtonComponent from '../../../../CommonComponents/WhiteButtonComponent';
import BankLavelSafety from '../../../../CustomComponent/bankLevelSafety/BankLavelSafety';
import {getPlanStatus} from '../../DashboardUtils';
import {WEBENGAGE_CREDIT_SCORE} from '../../../constants/CreditScoreWebEngage.events';

const chatBridgeRef = NativeModules.ChatBridge;

let CCDCType = 'CC';
const nativeBridgeRef = NativeModules.ChatBridge;
let circleValue = 80, recentScore = 0;
let addItemsBottomSheet = React.createRef();
let deeplink=null;
export default class OverviewTab extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            addMoreData: addMoreAssetList.mobile,
            creditScore: {
                activeColor: {
                    'color1': '#FFAB00',
                    'color2': '#45B448',
                    'color3': '#45B448',
                    'color4': '#45B448',
                    'smallFirstCircleColor': '#45B448',
                    'smallSecondCircleColor': '#D8D8D8',
                },
                inActiveColor: {
                    'color1': '#FFDFC8',
                    'color2': '#FFDFC8',
                    'color3': '#FFDFC8',
                    'color4': '#FFDFC8',
                    'smallFirstCircleColor': '#C8C8C8',
                    'smallSecondCircleColor': '#C8C8C8',
                },
            },
            loadedCards: [],
            blogsSubscribed: false,
        };
    }

    componentWillMount() {
        // BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        // this.props.getAssetsData();
        logWebEngageTabEvent(this.props.membershipsData, WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.LOCATION_ATTR_VALUE.OVERVIEW);
    }


    updateCreditScoreStatus = () => {
        recentScore = this.props.customerSummaryData?.CustomerSummaryData?.score?.recentScore;
        let mScore = this.props.customerSummaryData?.CustomerSummaryData?.score?.maxScore;
        circleValue = (Math.round(((recentScore / mScore) * 100))) - 10;
    };


    dismiss(data, index) {
        this.props.dismissCard(data?.type);
        let attr = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.attrValueForAsset(data?.type);
        if (attr !== null && attr !== undefined) {
            let eventAttr = getIDFCommonEventAttr(this.props.membershipsData);
            eventAttr[LOCATION] = attr;
            logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_TODO_CARD_DISMISS, eventAttr);
        }
    }

    addTodo(data) {
        if (data.type === SkippableCards.LINKEDIN || data.type === SkippableCards.TWITTER || data.type === SkippableCards.FACEBOOK || data.type === SkippableCards.INSTAGRAM) {
            //Link social
            data.infoCardData['socialKey'] = data.type;
            this.linkSocial(data?.infoCardData, 'Overview Tab Static Card');
        } else {
            //add cyber data
            this.openAddAssetPopup(data?.infoCardData?.addMore);
        }
        let attr = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.attrValueForAsset(data?.type);
        if (attr !== null && attr !== undefined) {
            let eventAttr = getIDFCommonEventAttr(this.props.membershipsData);
            eventAttr[LOCATION] = attr;
            logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_TODO_CARD_TAP, eventAttr);
        }
    }

    openAddAssetPopup = (addMoreData) => {
        if (DiscardedPlanStates.includes(addMoreData.planStatus)) {
            openMoreAboutPremiumPopup(0);
            return;
        } else {
            this.setState({addMoreData: addMoreData});
            if (addItemsBottomSheet.current) {
                addItemsBottomSheet.current.setBottomSheetVisible(true);
            }
        }
    };

    componentDidMount() {
        //open auto popup if any
        openMoreAboutPremiumPopup(0);

        setTimeout(() => {
            switch (this.props.deeplinkData?.relativePath) {
                case '/idfence/alert-details':
                case '/idfence/alert-details/':
                    this.navigateToAlertDetails();
                    break;
                case '/idfence/credit-score':
                case '/idfence/credit-score/':
                    this.onCreditScoreViewDetailsClick();
                    break;
                case '/idfence/updatePayment':
                case '/idfence/updatePayment/':
                    renewMembership(this.props.renewalData, this.handleUpdatePayment);
                    break;
                case '/idfence':
                case '/idfence/':
                    if (this.props.deeplinkData?.reactivate) {
                        this.onCreditScoreViewDetailsClick();
                    }
                    break;
            }
        }, 2000)

    }

    render() {
        if (this.props.isLoading) {
            return <OAActivityIndicator/>;
        }
        return (
            <SafeAreaView style={styles.SafeArea}>
                <View style={{flex: 1}}>
                    <ScrollView style={{flex: 1}} keyboardShouldPersistTaps='always'>
                        {this.getAssetList()}
                    </ScrollView>
                    {
                        (!this.props.membershipsData.isSIEnabled || (this.props.membershipsData.membershipStatus == 'E')) ? this.showStickyBuyPlanFooter() : null
                    }
                    {this.addTrialExpiringPopup()}
                    <AddDataView ref={addItemsBottomSheet}
                                 item={this.state.addMoreData}
                                 setMinTextForCredDeb={(text) =>
                                     this.onMinTextForCredDeb(text, this.state?.addMoreData?.type)
                                 }
                                 onPress={(textFieldData) =>
                                     this.onAddPress(textFieldData)
                                 }
                    />
                </View>
                <DialogView ref={ref => (this.DialogViewRef = ref)}/>
            </SafeAreaView>
        );
    }

    onMinTextForCredDeb(text, type) {
        if (type === AllAssetKeys.CREDIT_DEBIT) {
            getCardType(text, this.handleOnMinTextForCredDebResponse);
        }
    }

    handleOnMinTextForCredDebResponse = (response, status) => {
        if (status === SUCCESS_KEY) {
            let cardType = response?.data?.cardCategory;
            if (cardType === 'CRC') {
                CCDCType = 'CC';

            } else {
                CCDCType = 'DC';

            }
        }
    };
    handleRenewal = (response, status) => {
        if (status === SUCCESS_KEY) {
            this.openIDFencePlanDetails(response.data?.payNowLink);
        }

    };

    handleUpdatePayment = (response, status) => {
        if (status === SUCCESS_KEY) {
            nativeBridgeRef.openIDFencePlanDetails(true, response.data?.payNowLink);
        }
    };

    openIDFencePlanDetails = (link) => {
        let updatePaymentOrRenew = this.props.premiumPopupData.updatePaymentOrRenew;
        if (Platform.OS === PLATFORM_OS.android) {
            this.openIdFence(updatePaymentOrRenew, link).then(value => {
            }).catch(onerror => {
            });
        } else {
            // nativeBridgeRef.goBack();
            //    open payment page: renewal
            nativeBridgeRef.openIDFencePlanDetails(updatePaymentOrRenew, link);
        }
    };

    onAddPress(textFieldData) {
        if (addItemsBottomSheet.current) {
            addItemsBottomSheet.current.setBottomSheetCancel(false);
        }
        this.state.addMoreData.cardType = CCDCType;
        addCyberDataAPI(this.props.membershipsData?.subscriberNo, this.state.addMoreData, textFieldData, this.handleCyberDataAPIResponse);
    }

    handleCyberDataAPIResponse = (response, status) => {
        if (addItemsBottomSheet.current) {
            addItemsBottomSheet.current.setBottomSheetCancel(true);
            if (status === SUCCESS_KEY) {
                addItemsBottomSheet.current.setBottomSheetVisible(false);
                this.props.refreshTabBarAssets();
                //event
                let attr = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.attrValueForAsset(this.state.addMoreData?.type);
                if (attr !== null && attr !== undefined) {
                    let eventAttr = getIDFCommonEventAttr(this.props.membershipsData);
                    eventAttr[LOCATION] = 'Overview Tab Static Card';
                    eventAttr[ASSET_NAME] = attr;
                    logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_ASSET_ADDED, eventAttr);
                }
            } else {
                if (response?.message in IDFenceErrorApiErrorMsg) {
                    addItemsBottomSheet.current.setErrorMessage(IDFenceErrorApiErrorMsg[response?.message]);
                    return;
                }
                addItemsBottomSheet.current.setErrorMessage(IDFenceErrorApiErrorMsg.DEFAULT);
            }
        }
    };

    showAlert(alertMessage) {
        if (this.DialogViewRef !== null && this.DialogViewRef !== undefined) {
            this.DialogViewRef.showDailog({
                title: '',
                message: alertMessage,
                primaryButton: {
                    text: 'OK', onClick: () => {
                        this.props.resetOverViewTab();
                    },
                },
                cancelable: true,
                onClose: () => {
                    this.props.resetOverViewTab();
                },
            });
        }
    }


    showStickyBuyPlanFooter = () => {
        if (this.props.membershipsData?.isTrial || isMemTrialEndOrExpired(this.props.membershipsData)) {
            let locationAttVal = 'Overview Tab';
            //event
            if (!this.state.loadedCards.includes('footerPlan')) {
                let eventAttr = getIDFCommonEventAttr(this.props.membershipsData);
                eventAttr[LOCATION] = locationAttVal;
                logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_DASHBOARD_BUY_PREMIUM_FOOTER_LOAD, eventAttr);
                this.state.loadedCards.push('footerPlan');
            }

            let upgradeLabel = AlertsTabConstants.expiredMemData.upgradeLabel;
            let btnActionText = AlertsTabConstants.expiredMemData.actionText;
            if (this.props.membershipsData?.isTrial) {
                btnActionText = AlertsTabConstants.trialOverMemData.actionText;
                upgradeLabel = AlertsTabConstants.trialOverMemData.upgradeLabel;
            }
            return <IDFenceBuyStickyView data={this.props.idFencePlansData.premiumPlan}
                                         upgradeLabel={upgradeLabel}
                                         from={locationAttVal}
                                         renewalAction={() => {
                                             renewMembership(this.props.renewalData, this.handleRenewal);
                                         }}
                                         membershipData={this.props.membershipsData ?? {}}
                                         planPrice={this.props.renewalData.orderInfo.planPrice}
                                         isRenewal={getPlanStatus(parseInt(this.props.membershipsData?.memRemainingDays), this.props.membershipsData?.isTrial,
                                             this.props.siData, this.props.membershipsData?.isSIEnabled) === PlanStatus.INACTIVE}
                                         webEngageData={{
                                             'memStatus': this.props.membershipsData?.membershipStatus,
                                             'planName': this.props.membershipsData?.planName,
                                         }}
                                         actionText={btnActionText} />;
        }
        return null;
    };

    addTrialExpiringPopup = () => {
        if (!this.props.premiumPopupData) {
            return null;
        }
        return <MoreAboutPremiumPopup
            height={spacing.spacing56 * 7 + spacing.spacing36}
            data={this.props.premiumPopupData}
            membershipData={this.props.membershipsData ?? {}}
            onPress={() => {
                this.sendBottonPopupActionclickEvent()
                if (this.props.premiumPopupData.updatePaymentOrRenew) {
                    renewMembership(this.props.renewalData, this.handleRenewal);
                } else {
                    this.openIDFencePlanDetails('');
                }
            }}
        />;
    };

    sendBottonPopupActionclickEvent = () => {
        let webEngageEvents = getIDFCommonEventAttr(this.props.membershipsData);
        webEngageEvents[LOCATION] = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_FENCE_DASHBOARD;
        logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.DASHBOARD_BOTTOM_SHEET_CTA_TAB, webEngageEvents);
    };

    async openIdFence(updatePaymentOrRenew, payNowLink) {
        await nativeBridgeRef.openIDFencePlanDetails(updatePaymentOrRenew, payNowLink);
        // TODO: KAG Check if needed?
        nativeBridgeRef.goBack();
    }

    getAssetList = () => {
        let list = this.props.dataSource?.map((item, index) => {
            return this.renderItem(item, index);
        });
        return list;
    };
    renderItem = (item, index) => {
        const {type, data} = item;
        let cell = null;
        let headerTitle = null;
        let headerStyle = styles.cellHeaderTitleStyle;
        switch (type) {
            case OverviewCards.AlertsOverview.type:
                if (!this.props.topAlerts) {
                    return null;
                }
                cell = (
                    <AlertsOverviewCard
                        data={this.props.topAlerts}
                        style={alertsOverviewStyle}
                        onPress={() => {
                            let eventAttr = getIDFCommonEventAttr(this.props.membershipsData);
                            eventAttr[LOCATION] = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_ALERTS_OVERVIEW_NOTIFICATION_TRAY_VALUE;
                            logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_ALERTS, eventAttr);
                            this.props.gotoTab(1);
                        }}
                    />
                );
                break;
            case OverviewCards.Todo.type:
                headerTitle = OverviewCards.Todo.title;
                if (data == null) {
                    return null;
                }
                let attrTodo = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.attrValueForAsset(data?.type);
                if (attrTodo !== null && attrTodo !== undefined && !this.state.loadedCards.includes(data?.type)) {
                    let eventAttr = getIDFCommonEventAttr(this.props.membershipsData);
                    eventAttr[LOCATION] = attrTodo;
                    logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_TODO_LOAD, eventAttr);
                    this.state.loadedCards.push(data?.type);
                }
                ;
                let theme = null;
                if (data.colors !== null && data.colors !== undefined && data.colors[0] == data.colors[1] && data.colors[0] === colors.white) {
                    theme = colors.white;
                }
                cell = (
                    <TodoCard
                        data={data}
                        onLeftClick={() => {
                            this.dismiss(data, index);
                        }}
                        onRightClick={() => {
                            this.addTodo(data);
                        }}
                        theme={theme}
                    />
                );
                if (theme !== null) {
                    cell = <View style={{
                        justifyContent: 'space-around',
                        backgroundColor: colors.white,
                        borderRadius: spacing.spacing4, ...CommonStyle.cardContainerWithShadow,
                    }}>{cell}</View>;
                }
                break;
            case OverviewCards.LinkSocial.type:
                if (data.length == 0) {
                    return null;
                }
                data.map(item => {
                    let assetName = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.attrValueForAsset(item?.id);
                    this.sendLoadEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_CARD_LOAD, item?.id, 'Social_Media_Link_Card', assetName);
                });
                headerTitle = OverviewCards.LinkSocial.title;
                let cards = this.getSocialCards(data);
                cell = <OACarousel data={cards}/>;
                break;
            case OverviewCards.BankAccount.type:
                if (data === null || data?.addMore === null) {
                    return null;
                }

                let assetName = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.attrValueForAsset(data?.type);
                this.sendLoadEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_CARD_LOAD, item?.id, 'Overview Tab Static Card', assetName);

                headerTitle = OverviewCards.BankAccount.title;
                cell = this.getIDCard(data);
                break;
            case OverviewCards.GovIDs.type:
                if (data.length == 0) {
                    return null;
                }
                data.map(item => {
                    let assetName = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.attrValueForAsset(item?.type);
                    this.sendLoadEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_CARD_LOAD, item.id, 'Overview Tab Static Card', assetName);
                });
                headerTitle = OverviewCards.GovIDs.title;
                let govIDs = this.getIDCards(data);
                cell = <OACarousel data={govIDs}/>;
                break;
            case OverviewCards.Mobile.type:
                if (data === null || data?.addMore === null) {
                    return null;
                }
                this.sendLoadEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_CARD_LOAD, data?.type, 'Overview Tab Static Card', WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.attrValueForAsset(data?.type));

                headerTitle = OverviewCards.Mobile.title;
                cell = this.getIDCard(data);
                break;

            case OverviewCards.Blogs.type:
                if (data !== null && data !== undefined) {
                    headerStyle = styles.cellBoldHeaderTitleStyle;
                    headerTitle = data.title ? data.title : OverviewCards.Blogs.title;
                    cell = this.getBlogCard(data.list);
                    break;
                }
                break;

            case OverviewCards.BlogSubscription.type:
                if (data !== null && data !== undefined) {
                    headerStyle = styles.cellBoldHeaderTitleStyle;
                    headerTitle = !this.state.blogsSubscribed ? OverviewCards.BlogSubscription.title : '';
                    cell = this.getBlogSubscriptionCard(data);
                    break;
                }
                break;

            case OverviewCards.ShareApp.type:
                if (data !== null && data !== undefined) {
                    headerStyle = styles.cellBoldHeaderTitleStyle;
                    cell = this.getShareAppCad(data);
                    break;
                }
                break;
            case OverviewCards.BankSafety.type:
                if (data !== null && data !== undefined) {
                    cell = this.getBankSafetyCard(data);
                    break;
                }
                break;
            case OverviewCards.IDFencePremiumPlanCell.type:
                if (this.props.membershipsData?.isTrial) {
                    cell = <View style={{width: 0, height: 0}}/>;
                    break;
                }
                this.updateCreditScoreStatus();

                // Note:- as per discussed with akash now will credit score for inactive membership and these conditions will commented.
                // if (this.props.customerSummaryData?.SubscriptionStatus === 'CS_CUSTOMER_UNSUBSCRIBED' ||
                //     this.props.customerSummaryData?.SubscriptionStatus === 'CS_CUSTOMER_DOES_NOT_EXIST') {
                //     cell = <View style={{width: 0, height: 0}}/>;
                //     break;
                // }

                const isCustNotFound = (this.props.customerSummaryData?.SubscriptionStatus === 'CS_CUSTOMER_NOT_FOUND');
                headerTitle = 'YOUR CREDIT SCORE';
                cell = (
                    <View>
                        <CreditScoreCircularSliderView
                            circleValue={circleValue}
                            customerSummaryData={this.props.customerSummaryData}
                            isCustNotFound={isCustNotFound}
                            membershipData={this.props.membershipsData}
                            onCreditScoreViewDetailsClick={e => {
                                this.onCreditScoreViewDetailsClick();
                            }}
                        />
                    </View>);
                break;
            case OverviewCards.WarningCell.type:
                if (data.length == 0) {
                    return null;
                }
                if ('F' === this.props.siData?.status) {
                    if (data.filter(function (itemKey) {
                        return itemKey.key === 'UPDATE_PAYMENT';
                    }).length === 0) {
                        data.push({
                            'webEngageValue': 'Update Payment Method',
                            'key': 'UPDATE_PAYMENT',
                            'memId': this.props.membershipsData?.subscriberNo,
                            'actionText': 'Update Now',
                        });
                    }
                }
                //event
                data.map(item => {
                    if (!this.state.loadedCards.includes(item?.type)) {
                        let eventAttr = getIDFCommonEventAttr(this.props.membershipsData);
                        eventAttr[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.TYPE] = item.webEngageValue;
                        logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_DASHBOARD_PROMPT_LOAD, eventAttr);
                        this.state.loadedCards.push(item?.type);
                    }
                });

                cell = (
                    <OverviewTabWarningView
                        onSocialActionClick={e => {
                            this.linkSocial(e, '');
                            //event
                            let assetName = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.attrValueForAsset(item?.type);
                            let eventAttr = getIDFCommonEventAttr(this.props.membershipsData);
                            eventAttr[ASSET_NAME] = assetName;
                            eventAttr[LOCATION] = 'Top_Prompt';
                            logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_ASSET_OPEN_TO_ADD, eventAttr);
                        }}
                        onRenewalActionClick={e => renewMembership(this.props.renewalData, this.handleRenewal)}
                        siStatus={this.props.siData?.status} warningList={data} navigation={this.props.navigation}
                        membershipsData={this.props.membershipsData}
                    />
                );
                break;
            default:
                cell = null;
        }

        if (cell !== null) {
            let marginTop = spacing.spacing16;
            let marginBottom = spacing.spacing16;
            if (type !== OverviewCards.WarningCell.type && type !== OverviewCards.ShareApp.type) {
                cell = <View style={styles.defaultHorizontalPaddingStyle}>{cell}</View>;
            } else if (type === OverviewCards.WarningCell.type) {
                marginBottom = -spacing.spacing16;
            }
            return (
                <View key={index.toString()}
                    style={[
                        styles.cellContainerStyle,
                        {marginTop: marginTop, marginBottom: marginBottom},
                    ]}
                >
                    <Text
                        style={[headerStyle, {
                            display: ((headerTitle !== null && headerTitle !== '') ? null : 'none'),
                        }]}>{headerTitle}</Text>
                    {cell}
                </View>
            );
        }
        return null;
    };

    activateCreditScore = () => {
        alert('activating...');
    };

    openActivationScreen() {
        this.props.navigation.navigate('ActivateCreditScoreComponent', {
            activateCreditScore: this.activateCreditScore,
            subscriberNo: this.props.membershipsData?.subscriberNo,
            otherParam: 'Activate Credit Score',
            membershipsData: this.props.membershipsData,
        });
    }

    onCreditScoreViewDetailsClick() {
        let eventAttr = getIDFCommonEventAttr(this.props.membershipsData);
        let membershipsData = this.props.membershipsData;
        if (getPlanStatus(parseInt(membershipsData?.memRemainingDays), membershipsData?.isTrial,
            this.props.siData, membershipsData?.isSIEnabled) === PlanStatus.INACTIVE) {
            openMoreAboutPremiumPopup(0);
            return;
        }

        if (this.props.customerSummaryData?.SubscriptionStatus === 'CS_CUSTOMER_NOT_FOUND') {
            logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_DASHBOARD_CREDIT_SCORE_RETRY_ACTIVATION, eventAttr);
            this.openActivationScreen();
            return;
        }

        logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.GO_TO_CREDIT_SCORE_DASHBOARD, eventAttr);
        this.props.navigation.navigate('CreditScoreDashboard', {
            title: 'Your credit score',
            subscriberNo: this.props.membershipsData?.subscriberNo,
            customerSummaryData: this.props.customerSummaryData,
            membershipsData: this.props.membershipsData,
        });
    }

    navigateToAlertDetails() {
        this.props.navigation.navigate('AlertDetails', {
            title: 'Alert Details',
            deeplinkData: this.props.deeplinkData,
            alertTypeList: this.props.alertTypeList,
            membershipData: this.props.membershipsData,

        });
    }

    handleAction = id => {


    };

    getIDCard = data => {
        return (
            <View style={styles.roundedCornerContainerWithShadow}>
                <AccountsCard
                    data={data}
                    style={IDCardStyle}
                    onClick={id => {
                        this.openAddAssetPopup(data?.addMore);

                        //event
                        let assetName = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.attrValueForAsset(data?.type);
                        let eventAttr = getIDFCommonEventAttr(this.props.membershipsData);
                        eventAttr[ASSET_NAME] = assetName;
                        eventAttr[LOCATION] = 'Overview Tab Static Card';
                        logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_ASSET_OPEN_TO_ADD, eventAttr);
                    }
                    }
                />
            </View>
        );
    };

    getIDCards = data => {
        let cards = data.map(item => {
            return this.getIDCard(item);
        });
        return cards;
    };

    linkSocial = (item, location) => {
        if (DiscardedPlanStates.includes(item.planStatus)) {
            openMoreAboutPremiumPopup(0);
            return;
        } else {
            //open link in webview
            this.props.navigation.navigate('OAWebView', {
                url: item.linkUrl,
                asset: item.socialKey,
                location: location,
            });
        }
    };
    getSocialCards = data => {
        let cards = data.map(item => {
            let card = (
                <View style={styles.roundedCornerContainer}>
                    <AccountsCard
                        data={item}
                        style={SocialCardStyle}
                        onClick={link => {
                            this.linkSocial(item, 'Social_Media_Link_Card');

                            //event
                            let assetName = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.attrValueForAsset(item?.id);
                            let eventAttr = getIDFCommonEventAttr(this.props.membershipsData);
                            eventAttr[ASSET_NAME] = assetName;
                            eventAttr[LOCATION] = 'Social_Media_Link_Card';
                            logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_ASSET_OPEN_TO_ADD, eventAttr);

                        }}
                    />
                </View>
            );
            return (
                <OALinearGradient
                    child={card}
                    colors={item.gradient?.colors}
                    degree={item.gradient?.degree}
                    style={{
                        borderRadius: spacing.spacing8,
                    }}
                />
            );
        });
        return cards;
    };

    getBlogCard = data => {
        const renderItem = ({item}) => {
            let itemView = (
                <View style={styles.roundedCornerContainer}>
                    <BlogCard
                        data={item}
                        onClick={(title, link) => {
                            this.props.navigation.navigate('OAWebView', {
                                url: link,
                                title: ID_FENCE_BLOG,
                            });
                            let eventAttr = getIDFCommonEventAttr(this.props.membershipsData);
                            eventAttr[LOCATION] = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.DASHBOARD;
                            logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_FENCE_BLOG_CARD_CLICK, eventAttr);
                        }}
                    />
                </View>
            );
            return itemView;
        };
        let listCard = (
            <OAHorizontalFlatList data={data} renderItem={renderItem}/>
        );
        return listCard;
    };

    getBlogSubscriptionCard = data => {
        let card = (
            <View style={{marginTop: spacing.spacing16}}>
                {
                    !this.state.blogsSubscribed ?
                        (<View>
                            <Text style={{...TextStyle.text_16_regular, color: colors.grey}}>
                                {data.subtitle}
                            </Text>
                            <View style={{marginTop: spacing.spacing16}}>
                                <ButtonWithLoader
                                    isLoading={false}
                                    enable={true}
                                    Button={{
                                        text: data.actionText,
                                        onClick: () => {
                                            let eventAttr = getIDFCommonEventAttr(this.props.membershipsData);
                                            eventAttr[LOCATION] = 'IDFence_Dashboard';
                                            eventAttr[EMAIL_ID] = this.props.membershipsData?.email;
                                            logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.IDFence_Blog_Subscription, eventAttr);
                                            this.setState({blogsSubscribed: true});
                                        },
                                    }}
                                />
                            </View>
                        </View>) :
                        (
                            <View style={{flexDirection: 'row'}}>
                                <Image source={require('../../../../images/ic_green_tick.webp')}
                                       style={{width: 24, height: 24, marginTop: spacing.spacing8}}/>
                                <Text style={{...TextStyle.text_18_bold, marginLeft: spacing.spacing16, flex: 1}}>
                                    {THANK_YOU_FOR_BLOG_SUBSCRIPTION}
                                </Text>

                            </View>
                        )

                }
            </View>
        );
        return card;
    };


    getShareAppCad = (data) => {
        let card = (
            <View style={{
                paddingRight: spacing.spacing16,
                paddingVertical: spacing.spacing32,
                backgroundColor: colors.soulitudeGrey,
                flexDirection: 'row',
            }}>
                <Text style={{...styles.cellBoldHeaderTitleStyle, marginRight: spacing.spacing16, flex: 2}}>
                    {data.title}
                </Text>
                <WhiteButtonComponent
                    Button={{
                        text: data.actionText,
                        onClick: () => {
                            nativeBridgeRef.shareLink(ID_FENCE_SHARE_LINK);
                            let eventAttr = getIDFCommonEventAttr(this.props.membershipsData);
                            eventAttr[LOCATION] = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.DASHBOARD;
                            logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_FENCE_SHARE_CARD_CLICK, eventAttr);
                        },
                    }}
                />
            </View>
        );
        return card;
    };

    getBankSafetyCard = data => {
        return <BankLavelSafety data={data}/>;
    };


    sendLoadEvent = (eventName, cardType, location, assetName) => {
        if (!this.state.loadedCards.includes(cardType)) {
            let eventAttr = getIDFCommonEventAttr(this.props.membershipsData);
            if (location !== null && location !== undefined) {
                eventAttr[LOCATION] = location;
            }
            if (assetName !== null && assetName !== undefined) {
                eventAttr[ASSET_NAME] = assetName;
            }
            logWebEnageEvent(eventName, eventAttr);
            this.state.loadedCards.push(cardType);
        }
    };
}
