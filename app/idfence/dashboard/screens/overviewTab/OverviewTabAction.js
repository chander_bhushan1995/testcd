import {connect} from "react-redux";
import OverviewTab from "./OverviewTab";
import {
    hideLoader, hideLoaderOnOverViewTab,
    refreshTabBar,
    resetOverViewTab,
    showError,
    showLoaderOnOverViewTab,
    skipCard,
} from '../../../constants/actionCreators';
import {Urls} from '../../../constants/Urls';
import { makePostRequest } from '../../../../network/ApiManager';

const mapStateToProps = state => ({
    isLoading: state.dashboardReducer.isLoading,
    dataSource: state.dashboardReducer.data,
    premiumPopupData: state.dashboardReducer.premiumPopupData,
    errorMessage: state.dashboardReducer.errorMessage,
    initialProps:  state.appData.params,
    idFencePlansData:state.dashboardReducer.idFencePlansData,
    membershipsData: state.dashboardReducer.membershipsData,
    customerSummaryData:state.dashboardReducer.creditScoreCustomerSummaryData,
    siData:state.dashboardReducer.siData,
    unreadAlertCount:state.dashboardReducer.unreadAlertCount,
    topAlerts:state.dashboardReducer.topAlerts,
});
const mapDispatchToProps = dispatch => ({

    addCyberDataAPI: (subscriberNo, addMoreData, textFieldData) => {
        dispatch(showLoaderOnOverViewTab());
        let {element, dataKey, indexKey, cardType} = addMoreData;
            let data = {
                indexKey: indexKey,
                [dataKey]: textFieldData
            };
            data[dataKey] = textFieldData;
            if (cardType !== null && cardType !== undefined) data.cardType = cardType;
            let body = [{element: element, data: JSON.stringify(data)}];

        makePostRequest(Urls.addCyberData + subscriberNo, body, (response, error) => {
                    if (response) {
                        dispatch(hideLoaderOnOverViewTab());
                        dispatch(refreshTabBar());
                    } else {
                        dispatch(hideLoaderOnOverViewTab());
                        dispatch(showError(error.message));
                    }
        });
    },
    dismissCard: (type) => {
        dispatch(skipCard(type))
    },
    refreshTabBarAssets: () =>{
        dispatch(refreshTabBar());
    },
  resetOverViewTab: () => {
        dispatch(resetOverViewTab())
    },
  showLoader: () => {
      dispatch(showLoaderOnOverViewTab());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(OverviewTab);
