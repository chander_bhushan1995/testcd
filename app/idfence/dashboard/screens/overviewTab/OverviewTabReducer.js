import React from "react";
import {
    HIDE_LOADER,
    HIDE_LOADER_OVERVIEW_TAB,
    RESET_OVERVIEW_TABS,
    SHOW_ERROR,
    SHOW_LOADER_OVERVIEW_TAB,
} from '../../../constants/actions';

const initialState = {
    isLoading: false,
    isAlertsTodoFetched: false,
    isAlertsSummaryFetched: false,
    alertSummary: [],
    alertCards: []
};

const overviewTabReducer = (state = initialState, action) => {

    switch (action.type) {
        /*case DISPLAY_OVERVIEW_TAB:
            return {...state};*/
        case SHOW_ERROR:
            return {
                ...state,
                errorMessage: action.errorMessage,
                isLoading: false
            };

        case RESET_OVERVIEW_TABS:
            return {
                ...state,
                errorMessage: null
            };
        case SHOW_LOADER_OVERVIEW_TAB:
            return {
                ...state,
                isLoading: true
            };
        case HIDE_LOADER_OVERVIEW_TAB:
            return {
                ...state,
                isLoading: false
            };
        default:
            return {...state};
    }
};
export default overviewTabReducer;
