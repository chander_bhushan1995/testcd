import React, {useState, useEffect} from "react";
import {NativeModules, View} from "react-native";
import WarningViewNoAction from "../../../../CustomComponent/warningviewnoaction";
import WarningViewWithAction from "../../../../CustomComponent/warningviewwithaction";
import {capitalizeFLetter, getIDFCommonEventAttr} from '../../../constants/IDFence.UtilityMethods';
import spacing from "../../../../Constants/Spacing";
import {getExcessPaymentUniqueId} from "../../../constants/Urls";
import {DetailsTabStrings} from "../../../constants/Constants";
import {WEBENGAGE_IDFENCE_DASHBOARD} from '../../../../Constants/WebengageAttrKeys';
import {logWebEnageEvent} from '../../../../commonUtil/WebengageTrackingUtils';
import { parseJSON, deepCopy} from "../../../../commonUtil/AppUtils";

const nativeBridgeRef = NativeModules.ChatBridge;
export default ({warningList,siStatus,onRenewalActionClick,onSocialActionClick, navigation, membershipsData}) => {

    const [LIST_DATA, setUIData] = useState({warningList: [], removedWarningList: []});


    useEffect(() => {
        setUIData({warningList: prepareWarningData(), removedWarningList: LIST_DATA.removedWarningList});
    }, []);

    function filterOnlyEmailWarnings(item) {
        return 'Email' === item.key;
    }

    function filterOnlyNonEmailWarnings(item) {
        return 'Email' !== item.key;
    }

    function prepareWarningData() {
        let filteredWList = warningList;
        if (LIST_DATA.removedWarningList.length > 0) {
            filteredWList = [];
            filteredWList = warningList.filter(ar => !LIST_DATA.removedWarningList.find(rm => (rm.name === ar.name && ar.key === rm.key) ))
        }

        let allEmailsWarnings = filteredWList.filter(filterOnlyEmailWarnings);
        let allNonEmailsWarnings = filteredWList.filter(filterOnlyNonEmailWarnings);
        let finalWarningArr = [];
        if (allEmailsWarnings !== null && allEmailsWarnings.length > 0) {
            finalWarningArr.push({
                "key": "Email",
                "count": allEmailsWarnings.length,
                "displayName": allEmailsWarnings[0]?.displayName,
                "webEngageValue": allEmailsWarnings[0]?.webEngageValue
            });
        }
        return [...finalWarningArr, ...allNonEmailsWarnings];
    }

    const prepareWarningViewList = () => {
        let template = <View/>;
        let filteredWList = LIST_DATA.warningList.filter(ar => !LIST_DATA.removedWarningList.find(rm => (rm.name === ar.name && ar.key === rm.key) ))
        template = filteredWList.map(function (item, index) {
            if ('Email' === item.key) {
                let title = "Email verification pending";
                let subTitle = "";
                if (item.count === 1) {
                    subTitle = `Open verification email on "${item.displayName}" and click on verify now button to start monitoring`;
                } else {
                    subTitle = `${item.count} of the added emails are not verified yet. Please open verification email and click on verification link to start monitoring your emails. Check your Spam Folder too.`;
                }
                return (
                    <View style={[{marginBottom: spacing.spacing16},{marginTop: index === 0 ? -spacing.spacing16:0 }]}>
                        <WarningViewNoAction title={title} subTitle={subTitle}
                                             onCrossPress={() => handleCrossPress(item, index)}/>
                    </View>
                );
            } else if ('UPDATE_PAYMENT' === item.key) {
                let title = "Update Payment Method";
                let subTitle = "Payment failed due to card expiry or other reasons will come here";
                return (
                    <View style={[{marginBottom: spacing.spacing16}, {marginTop: index === 0 ? -spacing.spacing16:0 }]}>
                        <WarningViewWithAction title={title} subTitle={subTitle} label={item.actionText}
                                               onCrossPress={() => handleCrossPress(item, index)}
                                               onMainActionPress={() => handleMainActionPress(item, index)}/>
                    </View>
                );
            } else {
                let title = `${capitalizeFLetter(item?.displayName?.toLowerCase())} account disconnected`;
                let subTitle = "Social media disconnection and other info message will come here";
                return (
                    <View style={[{marginBottom: spacing.spacing16}, {marginTop: index === 0 ? -spacing.spacing16:0 }]}>
                        <WarningViewWithAction title={title} subTitle={subTitle} label={item.actionText}
                                               onCrossPress={() => handleCrossPress(item, index)}
                                               onMainActionPress={() => handleMainActionPress(item, index)}/>
                    </View>
                );
            }
        });
        return template;
    };
    return (
        <View>{prepareWarningViewList()}</View>
    );

    function handleCrossPress(item, index) {
        let removedItem = LIST_DATA.warningList[index];
        LIST_DATA.removedWarningList.push(removedItem);

        let filteredWarningList = [];
        LIST_DATA.removedWarningList.map(element => {
            LIST_DATA.warningList.map(function (item) {
                if (item.key !== element.key) {
                    filteredWarningList.push(item);
                }
            });
        })
        setUIData(prev => ({
            ...prev,
            warningList: filteredWarningList,
            removedWarningList: LIST_DATA.removedWarningList
        }));
        let eventAttr = getIDFCommonEventAttr(membershipsData);
        eventAttr[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.TYPE] = item.webEngageValue;
        logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_DASHBOARD_DISMISS_PROMPT, eventAttr);
    }

    function handleMainActionPress(item, index) {

        if (item.key === 'UPDATE_PAYMENT') {
            if (siStatus==='F'){
                onRenewalActionClick();
                return;
            }else{
                nativeBridgeRef.getApiProperty(apiProperty => {
                    apiProperty = parseJSON(apiProperty);
                    let paymentURL = apiProperty.api_gateway_base_url + getExcessPaymentUniqueId(DetailsTabStrings.paramValueSI, item?.memId);
                    navigation.navigate("PaymentWebViewComponent", {
                        paymentLink: paymentURL,
                        isFromIDFence: true,
                        otherParam: DetailsTabStrings.titlePaymentReview
                    });

                });
            }

            return;
        }
        onSocialActionClick(item?.data);
        /*navigation.navigate("OAWebView", {
            url: item?.data.url,
            title: capitalizeFLetter(item.displayName.toLowerCase()) + " connect",
        })*/

    }
}
