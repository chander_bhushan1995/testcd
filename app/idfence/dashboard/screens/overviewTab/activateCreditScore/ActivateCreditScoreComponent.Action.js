import {connect} from "react-redux";
import ActivateCreditScoreComponent from "./ActivateCreditScoreComponent";
import {Urls} from "../../../../constants/Urls";
import { makePostRequest } from "../../../../../network/ApiManager";
import {refreshTabBar, resetState, retrySubscriptionResponse, showError} from "../../../../constants/actionCreators";

const mapStateToProps = state => ({
    retrySubscriptionData: state.activateCreditScoreComponentReducer.retrySubscriptionData,
    isRetrySubscriptionApiSuccess: state.activateCreditScoreComponentReducer.isRetrySubscriptionApiSuccess,
    isErrorApiError: state.activateCreditScoreComponentReducer.isErrorApiError,
    errorMessage: state.activateCreditScoreComponentReducer.errorMessage,
    resetState: state.activateCreditScoreComponentReducer.resetState,
    enableButton: state.activateCreditScoreComponentReducer.enableButton,
    isLoading: state.activateCreditScoreComponentReducer.isLoading,
    userData: state.dashboardReducer.userData
});

const mapDispatchToProps = dispatch => ({
    retrySubscription: (sNo, fName, lName) => {
            let requestData = {
                "subscriberNo": sNo,
                "firstName": fName,
                "lastName": lName
            };
        makePostRequest(Urls.retrySubscription, requestData, (response, error) => {
                    if (response) {
                        dispatch(retrySubscriptionResponse(response?.data));
                    } else {
                        dispatch(showError(error));
                    }
        });
    },
    resetDataState: () => {
        dispatch(resetState(""));
    },
    refreshTabBarAssets: () =>{
        dispatch(refreshTabBar());
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(ActivateCreditScoreComponent);
