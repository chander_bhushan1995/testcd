import {RESET_STATE, RETRY_SUBSCRIPTION_DATA, SHOW_ERROR} from "../../../../constants/actions";

const initialState = {
    retrySubscriptionData: {},
    isRetrySubscriptionApiSuccess: false,
    isErrorApiError: false,
    errorMessage: "",
    resetState: false,
    enableButton: false,
    isLoading: false
}
const activateCreditScoreComponentReducer = (state = initialState, action) => {
    switch (action.type) {
        case RETRY_SUBSCRIPTION_DATA:

            return {
                ...state,
                retrySubscriptionData: action.data,
                isRetrySubscriptionApiSuccess: true,
                enableButton:false,
                isLoading:false,
                isErrorApiError: false
            };
        case SHOW_ERROR:

            return {
                ...state,
                errorMessage: action.errorMessage?.message,
                isRetrySubscriptionApiSuccess: false,
                retrySubscriptionData: {},
                resetState: true,
                enableButton:false,
                isLoading:false,
                isErrorApiError: true
            };
        case RESET_STATE:
            return {
                ...state,
                isRetrySubscriptionApiSuccess: false,
                retrySubscriptionData: {},
                resetState: false,
                isLoading:false,
                enableButton:false,
                isErrorApiError: false
            };
        default:
            return {...state};
    }
};
export default activateCreditScoreComponentReducer;
