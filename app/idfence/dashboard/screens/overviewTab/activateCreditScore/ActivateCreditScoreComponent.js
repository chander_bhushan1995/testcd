import React from "react";
import {BackHandler, NativeModules, Platform, SafeAreaView, ScrollView, Text, View,} from 'react-native';
import DialogView from '../../../../../Components/DialogView';
import colors from '../../../../../Constants/colors';
import InputBox from '../../../../../Components/InputBox';
import {TextStyle} from '../../../../../Constants/CommonStyle';
import paymentFlowStrings from '../../../../../PaymentFlow/Constants/Constants';
import {PLATFORM_OS} from "../../../../../Constants/AppConstants";
import {HeaderBackButton} from "react-navigation";
import TnCCheckedBox from "./TnCCheckedBox.component";
import styles from "./ActivateCreditScoreComponent.style";
import {logWebEnageEvent} from "../../../../../commonUtil/WebengageTrackingUtils";
import {WEBENGAGE_IDFENCE_DASHBOARD} from "../../../../../Constants/WebengageAttrKeys";
import ButtonWithEnableDisable from "../../../../../CommonComponents/ButtonWithEnableDisable";
import {getIDFCommonEventAttr} from '../../../../constants/IDFence.UtilityMethods';

let navigator;
const nativeBrigeRef = NativeModules.ChatBridge;
export default class ActivateCreditScoreComponent extends React.Component {

    static navigationOptions = ({navigation}) => {
        navigator = navigation;
        let componentTitle = navigation.getParam('otherParam', null)
        if (componentTitle == null || componentTitle == undefined) {
            componentTitle = paymentFlowStrings.paymentFlowComponentTitles.tnCWebViewComponentTitle;
        }
        return {
            headerTitle: <View
                style={Platform.OS === PLATFORM_OS.ios ? {alignItems: "center"} : {alignItems: "flex-start"}}>
                <Text style={TextStyle.text_16_bold}>
                    {componentTitle}

                </Text>
            </View>,
            headerLeft: <HeaderBackButton tintColor={colors.black}
                                          onPress={() => {
                                              navigation.pop();
                                          }}/>,
            headerTitleStyle: {color: colors.white},
            headerTintColor: colors.white
        };
    };

    constructor(props) {
        super(props);
        this.actionButtonRef = React.createRef();
        this.state = {
            subscriberNo: navigator.getParam('subscriberNo', null),
            membershipData: navigator.getParam('membershipsData', null),
            enableButton: false,
            firstName: "",
            lastName: "",
            isLoading: false,
            errorMessage: "",
            showErrorDialog: false,
            isTnCChecked: false
        };
    }

    componentDidMount() {

        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    render() {
        if (this.props.isErrorApiError && this.props.resetState) {
            this.props.resetDataState();
            this.setState({showErrorDialog: true});
        }
        if (this.props.isRetrySubscriptionApiSuccess) {
            this.showAlert(this.props.errorMessage, true);
        }
        if (this.state.showErrorDialog) {
            this.showAlert(this.props.errorMessage, false);
        }

        this.setActionButtonState();
        return (
            <SafeAreaView style={styles.SafeArea}>
                <ScrollView style={styles.scrollViewStyle}>
                    <View style={styles.container}>
                        <Text style={styles.creditScoreTextStyle}>YOUR CREDIT SCORE</Text>
                        <Text style={styles.noteTextStyle1}>1. Make sure mobile number and full name are same as per
                            your bank details </Text>
                        <Text style={styles.noteTextStyle2}>2. Credit score usually generated when you took a loan or
                            credit card. Your credit score will be shown here once its generated </Text>
                        <Text style={styles.verifyTextStyle}>Verify your details</Text>
                        <Text style={styles.contactDetailsTextStyle}>Contact
                            details: {this.props.userData?.customerInfo?.mobileNo},
                            {this.props.userData?.customerInfo?.email}</Text>
                        <InputBox containerStyle={styles.addressTxtInputContainer}
                                  inputHeading="First Name as per PAN"
                                  keyboardType={'default'}
                                  isMultiLines={false}
                                  value={this.state.firstName}
                                  placeholderTextColor={colors.black}
                                  maxLength={100}
                                  errorMessage={this.state.errorMessage}
                                  onChangeText={(text) => {
                                      this.validateInputData(text, 0);
                                  }}/>
                        <InputBox containerStyle={styles.addressTxtInputContainer}
                                  isMultiLines={false}
                                  keyboardType={'default'}
                                  value={this.state.lastName}
                                  inputHeading="Last Name as per PAN"
                                  placeholderTextColor={colors.black}
                                  maxLength={100}
                                  errorMessage={this.state.errorMessage}
                                  onChangeText={(text) => {

                                      this.validateInputData(text, 1);
                                  }}

                        />

                        {this.getCheckboxComponent()}
                    </View>
                </ScrollView>
                <Text style={styles.stickyTextStyle}>Please contact us to change your email and phone number</Text>
                <View style={styles.primaryButtonContainer}>
                    <ButtonWithEnableDisable
                        ref={this.actionButtonRef}
                        isLoading={this.state.isLoading}
                        enable={this.state.enableButton}
                        Button={{
                            text: "Activate Now",
                            onClick: () => {
                                let eventAttr = getIDFCommonEventAttr(this.state.membershipData);
                                logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_RETRY_ACTIVATION_SUBMIT, eventAttr);
                                if (this.state.subscriberNo !== null) {
                                    this.setState({isLoading: true, enableButton: false});
                                    this.props.retrySubscription(this.state.subscriberNo, this.state.firstName,
                                        this.state.lastName);
                                    if (this.actionButtonRef.current) {
                                        this.actionButtonRef.current.enableDisableButton(false);
                                    }
                                }
                            },
                        }}
                    />
                </View>
                <DialogView ref={ref => (this.DialogViewRef = ref)}/>
            </SafeAreaView>
        );
    }

    showAlert(alertMessage, successAlert) {
        if (this.DialogViewRef !== null && this.DialogViewRef !== undefined) {
            this.DialogViewRef.showDailog({
                title: '',
                message: (alertMessage === "" ? "Request submitted successfully." : alertMessage),
                primaryButton: {
                    text: 'OK', onClick: () => {
                        this.setState({showErrorDialog: false});
                        if (!successAlert) {
                            if (this.actionButtonRef.current) {
                                this.actionButtonRef.current.enableDisableButton(true);
                            }
                            this.setState({isLoading: false, enableButton: true, showErrorDialog: false});
                            return
                        }
                        navigator.pop();
                        this.props.resetDataState();
                        this.props.refreshTabBarAssets();

                    },
                },
                cancelable: true,
                onClose: () => {
                    this.setState({showErrorDialog: false});
                    if (!successAlert) {
                        if (this.actionButtonRef.current) {
                            this.actionButtonRef.current.enableDisableButton(true);
                        }
                        this.setState({isLoading: false, enableButton: true, showErrorDialog: false});
                        return
                    }
                    this.props.resetDataState();
                    navigator.pop();
                    this.props.refreshTabBarAssets();
                },
            });
        }
    }


    validateInputData(text, index) {
        text = text.replace(/[^A-Za-z]/gi, "");
        if (index == 0) {
            this.setState({firstName: text});
            return
        }
        this.setState({lastName: text});
    }

    setActionButtonState() {
        if (!this.state.isTnCChecked) {
            this.disableActionButton();
            return
        }
        if (this.state.firstName.length > 3 && this.state.lastName.length > 3) {
            this.enableActionButton();
            return;
        }
        this.disableActionButton();
    }

    enableActionButton() {
        if (!this.state.enableButton) {
            if (this.actionButtonRef.current) {
                this.actionButtonRef.current.enableDisableButton(true);
            }
            this.setState({enableButton: true});
        }
    }

    disableActionButton() {
        if (this.state.enableButton) {
            if (this.actionButtonRef.current) {
                this.actionButtonRef.current.enableDisableButton(false);
            }
            this.setState({enableButton: false});
        }
    }

    getCheckboxComponent = () => {
        return (
            <TnCCheckedBox navigation={this.props.navigation} onCheckBoxChecked={(isChecked) => {
                this.onCheckBoxChecked(isChecked)
            }}/>
        );
    };

    onCheckBoxChecked(isChecked) {
        this.setState({isTnCChecked: isChecked});

    }

    onBackPress = () => {
        const {navigation} = this.props;
        let enableNativeBack = navigation.getParam('enableNativeBack', false);
        if ((enableNativeBack == null || enableNativeBack == undefined)) {
            nativeBrigeRef.goBack();
        } else {
            navigator.pop();
            return true;
        }
        return false;
    };
}

