import {StyleSheet} from "react-native";
import spacing from "../../../../../Constants/Spacing";
import colors from "../../../../../Constants/colors";
import {TextStyle} from "../../../../../Constants/CommonStyle";

export default StyleSheet.create({
    SafeArea: {
        flex: spacing.spacing1,
        backgroundColor: colors.white,
    },
    container: {
        flex: spacing.spacing1,
        backgroundColor: colors.white,
        paddingTop: spacing.spacing16,
        paddingHorizontal: spacing.spacing16
    },
    formFieldscontainer: {
        flexDirection: 'column',
        flex: spacing.spacing1,
    },
    primaryButtonContainer: {
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        marginBottom: spacing.spacing16,
        justifyContent: 'flex-end',
        alignSelf: 'stretch',
    },
    addressTxtInputContainer: {},
    tncViewStyle: {
        flexDirection: 'row',
        marginRight: spacing.spacing16,
    },
    termsContainer: {
        padding: spacing.spacing16,
    },
    agreeTermsViewStyle: {
        flexDirection: 'row',
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing32,
        marginTop: spacing.spacing16,
    },
    termsStyle: {
        flexDirection: 'row',
        paddingTop: spacing.spacing16,
    },

    scrollViewStyle: {
        flex: spacing.spacing1,
        paddingBottom: spacing.spacing32,
        backgroundColor: colors.white,
    },
    checkboxStyle: {
        width: spacing.spacing20,
        height: spacing.spacing20,
        marginRight: spacing.spacing4,
    },
    tncTextStyle: {
        marginLeft: spacing.spacing4,
        marginRight: spacing.spacing4,
        color: colors.blue028,
    },

    warrantyTextStyle: {
        padding: spacing.spacing16,
        flex: spacing.spacing1,
        paddingLeft: spacing.spacing5
    },
    termsTextStyle: {
        flex: spacing.spacing1,
        paddingLeft: spacing.spacing5
    },
    warrantyContainer: {
        marginTop: spacing.spacing8,
    },
    loaderContainer: {
        justifyContent: 'center',
        flex: spacing.spacing1
    },
    creditScoreTextStyle: {
        ...TextStyle.text_12_normal
    },
    noteTextStyle1: {
        marginTop: spacing.spacing16,
        marginBottom: spacing.spacing12,

    },
    noteTextStyle2: {
        marginVertical: spacing.spacing12,

    },
    verifyTextStyle: {
        marginTop: spacing.spacing16,
        ...TextStyle.text_14_bold

    },
    contactDetailsTextStyle: {
        marginTop: spacing.spacing6,
        marginBottom: spacing.spacing32,
        ...TextStyle.text_12_normal
    },
    stickyTextStyle: {
        marginHorizontal: spacing.spacing16,
        paddingTop: spacing.spacing8,
        ...TextStyle.text_12_normal
    }

});
