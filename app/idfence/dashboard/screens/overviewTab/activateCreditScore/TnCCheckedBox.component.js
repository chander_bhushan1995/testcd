import React, {useState} from "react";
import {Image, NativeModules, StyleSheet, Text, View} from "react-native";
import CheckBox from "react-native-check-box";
import {TextStyle} from "../../../../../Constants/CommonStyle";
import spacing from "../../../../../Constants/Spacing";
import colors from "../../../../../Constants/colors";
import { parseJSON, deepCopy} from "../../../../../commonUtil/AppUtils";

const nativeBridgeRef = NativeModules.ChatBridge;
export default function TnCCheckedBox({navigation, onCheckBoxChecked}) {
    const [checked, isChecked] = useState(false);

    return (<View style={styles.tncViewStyle}>
        <CheckBox
            style={styles.checkboxStyle}
            checkedImage={<Image source={require('../../../../../images/ic_check_box.webp')}
                                 style={styles.checkboxStyle}/>}
            unCheckedImage={<Image source={require('../../../../../images/ic_check_box_outline_blank.webp')}
                                   style={styles.checkboxStyle}/>}
            isChecked={checked}
            onClick={(e) => {
                onCheckBoxClick(e);
            }
            }
        />
        <Text>
            <Text style={[TextStyle.text_12_bold]} onPress={(e) => {
                onCheckBoxClick(e);
            }}>I agree with </Text>
            <Text style={[TextStyle.text_12_normal, styles.tncTextStyle]} onPress={() => {
                navigateToTnCScreen();
            }}>Terms and Conditions</Text>
        </Text>
    </View>);
    function onCheckBoxClick(e) {
        isChecked(!checked);
        onCheckBoxChecked(!checked);
    }

    function navigateToTnCScreen() {
        //TODO:- t&c
        nativeBridgeRef.getApiProperty(apiProperty => {
            apiProperty = parseJSON(apiProperty);
            let tncUrl = apiProperty.api_base_url + "ProductTerms/idfence/";
            navigation.navigate('TnCWebViewComponent', {
                productTermAndCUrl: tncUrl,
                otherParam: 'T&C'
            });
        });
    }

}
const styles = StyleSheet.create({
    tncViewStyle: {
        flexDirection: 'row',
        marginRight: spacing.spacing16,
    },
    termsContainer: {
        padding: spacing.spacing16,
    },
    agreeTermsViewStyle: {
        flexDirection: 'row',
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing32,
        marginTop: spacing.spacing16,
    },
    termsStyle: {
        flexDirection: 'row',
        paddingTop: spacing.spacing16,
    },
    checkboxStyle: {
        width: spacing.spacing20,
        height: spacing.spacing20,
        marginRight: spacing.spacing4,
    },
    tncTextStyle: {
        marginLeft: spacing.spacing4,
        marginRight: spacing.spacing4,
        color: colors.blue028,
    }
});
