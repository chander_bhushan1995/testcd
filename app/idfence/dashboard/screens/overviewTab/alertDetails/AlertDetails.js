import React from "react";
import {
    BackHandler,
    FlatList,
    Image, Linking, NativeModules,
    Platform,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import colors from '../../../../../Constants/colors';
import {TextStyle} from '../../../../../Constants/CommonStyle';
import {PLATFORM_OS} from "../../../../../Constants/AppConstants";
import {OAImageSource} from '../../../../../Constants/OAImageSource';
import spacing from '../../../../../Constants/Spacing';
import {OATextStyle} from '../../../../../Constants/commonstyles';
import dimens from '../../../../../Constants/Dimens';
import Hyperlink from 'react-native-hyperlink';
import ImageViewFromUrl from "../../../../../CustomComponent/imageviewfromurl";
import AlertDetailsDescription from './AlertDetailsDescription'
import {
    alertCellSubTitleDate,
    alertsExpandedViewTitle,
    alertsTitle,
    alertIndicatorIcon,
    getCompromisedType,
    getRecommendedActions,
    isBadAlert,
    isReportAlert, getIDFCommonEventAttr
} from '../../../../constants/IDFence.UtilityMethods';
import ButtonWithLoader from '../../../../../CommonComponents/ButtonWithLoader';
import OAActivityIndicator from '../../../../../CustomComponent/OAActivityIndicator';
import { markAlertReadAPICall, getAlertDetails, SUCCESS_KEY} from "../../../../dashboard/screens/IDFenceApiCalls";
import {AlertsTabConstants, formattedFileName} from '../../../../constants/Constants';
import {downloadAlertFileUrl} from '../../../../constants/Urls';
import SeparatorView from '../../../../../CustomComponent/separatorview';
import FullScreenImageView from "../../../../../CustomComponent/fullscreenimageview";
import {WEBENGAGE_IDFENCE_DASHBOARD} from '../../../../../Constants/WebengageAttrKeys';
import {logWebEnageEvent} from '../../../../../commonUtil/WebengageTrackingUtils';
import { parseJSON, deepCopy} from "../../../../../commonUtil/AppUtils";

const nativeBridgeRef = NativeModules.ChatBridge;
let navigator;

let alertCardItem = {
    title: "Test",
    subTitle: "Test.me",
    leftImageSource: OAImageSource.checkBox.source,
};
let alertData = null;
let recommendedActionsList = [
    "1. Monitor your Social Media accounts 24/7",
    "2. Get real time suggestions to minimize the breach effect",
    "3. Keep a track of your government IDs for any probable breach on Dark Web",
    "4. Monitor all your email accounts, phone numbers and even the transactional cards on Dark Web",
    "5. Lorem Ipsum should come here eventually"
];
let isAvailableForDownload = false;
let deeplinkData = null;
let alertTypeList = null;
let membershipData = null;
const hyperLinksItems = ["click here", "Click here", "Check here"];
let fullScreenImageViewRef = React.createRef();
let compromisedType = "";
const nativeBrigeRef = NativeModules.ChatBridge;
export default class AlertDetails extends React.Component {

    static navigationOptions = ({navigation}) => {
        navigator = navigation;
        return {
            headerStyle: {
                borderBottomWidth: 0,
                elevation: 0
            },
            headerTitle: null,
            headerLeft: (
                <View style={{flexDirection: "row", alignItems: "flex-start"}}>
                    <TouchableOpacity
                        onPress={() => {
                            // nativeBrigeRef.goBack();
                            navigation.pop()
                        }}
                    >
                        <Image
                            source={OAImageSource.back_arrow.source}
                            style={[OAImageSource.back_arrow.dimensions, {
                                marginLeft: spacing.spacing16,
                                marginRight: spacing.spacing16
                            }]}
                        />
                    </TouchableOpacity>
                    <Text style={styles.navigationTitle}>{navigator?.getParam('title', null)}</Text>
                </View>
            ),
            headerTitleStyle: {color: colors.color_FFFFFF},
            headerTintColor: colors.color_FFFFFF
        };
    };

    constructor(props) {
        super(props);
        deeplinkData = navigator.getParam('deeplinkData', null);
        alertTypeList = navigator.getParam('alertTypeList', null);
        membershipData = navigator.getParam('membershipData', null);

        this.state = {
            isLoading: true,
            primaryButtonEnabled: true,

        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        let isCreditScore = false;
        if (deeplinkData?.type === "credit") isCreditScore = true;
        getAlertDetails(deeplinkData?.subscriberNo, deeplinkData?.alertId, isCreditScore, this.handleAlertDetailsResponse)
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    render() {
        if (this.state.isLoading) {
            return <OAActivityIndicator/>;
        }

        return (
            <SafeAreaView style={styles.SafeArea}>
                <ScrollView style={styles.scrollViewStyle}>
                    <View style={styles.RowContainer}>
                        <Image resizeMode={"contain"} source={alertCardItem.leftImageSource}
                               style={[styles.ImageLeft]}/>
                        <View style={styles.ColumnContainer}>
                            <Text style={[OATextStyle.TextFontSize_14_bold]}
                                  numberOfLines={1}
                                  ellipsizeMode='tail'>
                                {alertCardItem.title}
                            </Text>
                            <Text style={styles.SubTitleText}
                                  numberOfLines={1}
                                  ellipsizeMode='tail'>
                                {alertCardItem.subTitle}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.alertDetailsContainer}>
                        <AlertDetailsDescription item={alertData}/>
                    </View>

                    <View style={styles.recommendedActionsContainer}>
                        <Text
                            style={[styles.alertDetailsTitleStyle, {display: recommendedActionsList?.length > 0 ? 'flex' : 'none'}]}>
                            RECOMMENDED ACTIONS
                        </Text>
                        <FlatList
                            contentContainerStyle={{
                                flexGrow: 1,
                            }}
                            data={recommendedActionsList}
                            renderItem={({item, index}) => this.renderItem(item, index)}
                            keyExtractor={item => item.toString()}/>
                    </View>

                </ScrollView>
                {this.getPrimaryButtonComponent()}
                <FullScreenImageView ref={fullScreenImageViewRef}/>
            </SafeAreaView>
        );
    }

    //  renderItem = (item, index) => {
    //      return <View style = {styles.recommendedActionsTextContainer}><Text style={TextStyle.text_14_normal_charcoalGrey}>{item}</Text></View>
    // };

    renderItem = (item, index) => {
        let actionPoint = (index + 1) + ". " + item.action;
        let externalUrl = item["externalUrl"];
        let mediaUrl = item["mediaUrl"];// "https://sit6.1atesting.in/static/idfence/shared/images/oneassist-logo.1581059283569.png";
        let replacedSubString = "click here";
        if (externalUrl !== null && externalUrl !== undefined) {
            hyperLinksItems.map(item => {
                if (actionPoint.indexOf(item) > -1) {
                    replacedSubString = item;
                }
            });
            actionPoint = actionPoint.split(replacedSubString).join(externalUrl);
        }


        return (
            <View style={styles.hyperlinkContainerStyle}>
                <Hyperlink
                    onPress={(url) => this.handleLinkClick(url)}
                    linkStyle={styles.hyperlinkTitleStyle}
                    linkText={url => url === externalUrl ? replacedSubString : url}>
                    <Text style={styles.hyperlinkSubTitleStyle}>{actionPoint}</Text>
                </Hyperlink>
                {this.addActionMedia(mediaUrl)}
            </View>);
    };

    getPrimaryButtonComponent = () => {
        if (!isAvailableForDownload) return null;
        let buttonTitle = "Download Report";
        return (
            <View style={styles.actionViewContainerStyle}>
                <View style={styles.actionMainViewStyle}>
                    <SeparatorView/>
                    <View style={styles.primaryButtonContainer}>
                        <ButtonWithLoader
                            isLoading={!this.state.primaryButtonEnabled}
                            enable={this.state.primaryButtonEnabled}
                            Button={{
                                text: buttonTitle,
                                onClick: () => {
                                    this.downloadReport();
                                },
                            }}/>
                    </View>
                </View>
            </View>
        );
    };

    downloadReport = () => {
        let subscriberNo = deeplinkData?.subscriberNo;
        let alertID = deeplinkData?.alertId;
        // navigator.pop();
        // this.setState({primaryButtonEnabled: false});
        //  let webEngageEvents = new Object();
        // let assetName = item?.data?.alertType;
        // webEngageEvents[LOCATION] = WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_ALERT_DOWNLOAD_REPORT_ATTR_VALUE.ALERT_DETAIL_PAGE;
        // webEngageEvents[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.ASSET_NAME] = assetName;
        // logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_ALERT_DOWNLOAD_REPORT, webEngageEvents);

        nativeBridgeRef.getApiProperty(apiProperty => {
            apiProperty = parseJSON(apiProperty);
            let urlToDownloadTaxInvoiceFile = apiProperty.api_gateway_base_url +
                downloadAlertFileUrl(subscriberNo, alertID);
            nativeBridgeRef.downloadDocument(urlToDownloadTaxInvoiceFile,
                formattedFileName(alertID),
                AlertsTabConstants.labels.DOWNLOADING_ALERT_REPORT);
        });
    };

    handleAlertDetailsResponse
        = (response, status) => {
        if (status === SUCCESS_KEY) {
            response["itemType"] = AlertsTabConstants.ALERT_TYPE_NORMAL;
            response["alertType"] = response.data?.alertType;
            if (deeplinkData?.type === "credit") {
                response["itemType"] = AlertsTabConstants.ALERT_TYPE_CREDIT_SCORE;
            }
            if (response.itemType === AlertsTabConstants.ALERT_TYPE_NORMAL && response.data?.alertGenre === 'BAD') {
                isAvailableForDownload = true;
            }
            alertCardItem.title = alertsTitle(response.data, alertTypeList);
            alertCardItem.subTitle = alertCellSubTitleDate(response.data?.createdOn);
            alertCardItem.leftImageSource = alertIndicatorIcon(response?.data);
            alertData = response;
            recommendedActionsList = response.data?.recommendedActions || [];
            recommendedActionsList.sort(function (item1, item2) {
                return (item2.priority - item1.priority);
            });
            compromisedType = getCompromisedType(response)
            // let eventAttr = new Object();
            let eventAttr = getIDFCommonEventAttr(membershipData);
            eventAttr[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.ALERT_TYPE] = response?.data?.alertType
            eventAttr[WEBENGAGE_IDFENCE_DASHBOARD.ATTR_NAME.ASSET_NAME] = '' + (WEBENGAGE_IDFENCE_DASHBOARD.ATTR_VALUE.ID_ASSET_NAME_ATTR_VALUE[response?.data?.alertType] || 'Credit Score');
            logWebEnageEvent(WEBENGAGE_IDFENCE_DASHBOARD.EVENT_NAME.ID_ALERT_VIEW, eventAttr);
            if (!(response?.data?.readDate)){
                markAlertReadAPICall(response?.itemType,deeplinkData?.subscriberNo,deeplinkData?.alertId)
            }
        }
        this.setState({isLoading: false});
    };

    handleLinkClick = (clickedURL) => {
        // externalLinkWebEngageEvent();
        Linking.canOpenURL(clickedURL).then(supported => {
            if (supported) {
                Linking.openURL(clickedURL);
            }
        });
    };

    addActionMedia = (mediaUrl) => {
        if (mediaUrl !== null && mediaUrl !== undefined) {
            return (<View style={styles.imageViewFromUrlContainerStyle}>
                <ImageViewFromUrl imageUrl={mediaUrl} onPress={(mediaUrl) => {
                    this.onImageFullView(mediaUrl)
                }}/>
            </View>);
        }
        return null;
    };

    onImageFullView = (mediaUrl) => {
        if (fullScreenImageViewRef.current) {
            fullScreenImageViewRef.current.setModalVisible(true, mediaUrl);
        }
    }
    onBackPress = () => {
        const {navigation} = this.props;
        let enableNativeBack = navigation.getParam('enableNativeBack', false);
        if ((enableNativeBack == null || enableNativeBack == undefined)) {
            nativeBrigeRef.goBack();
        } else {
            navigator.pop();
            return true;
        }
        return false;
    };
}

const styles = StyleSheet.create({
    scrollViewStyle: {
        flex: spacing.spacing1,
        paddingBottom: spacing.spacing32,
        backgroundColor: colors.color_FAFAFA,

    },
    SafeArea: {
        flex: spacing.spacing1,
        backgroundColor: colors.white
    },
    MainContainer: {
        height: 72,
        flexDirection: "column"
    },
    RowContainer: {
        flexDirection: "row",
        backgroundColor: colors.white,
        paddingVertical: spacing.spacing16
    },
    ColumnContainer: {
        paddingRight: spacing.spacing16,
        paddingLeft: spacing.spacing16,
        paddingTop: spacing.spacing12,
        flexDirection: "column"
    },
    alertDetailsTitleStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        color: colors.color_888F97,
        flex: spacing.spacing1,
        paddingTop: spacing.spacing4,
        marginBottom: spacing.spacing16
    },
    alertDetailsContainer: {
        justifyContent: 'flex-start',
        paddingVertical: spacing.spacing16,
    },

    recommendedActionsContainer: {
        justifyContent: 'flex-start',
        paddingHorizontal: spacing.spacing16,
        paddingVertical: spacing.spacing32,
    },
    recommendedActionsTextContainer: {
        paddingHorizontal: spacing.spacing16,
        paddingVertical: spacing.spacing4,
    },
    primaryButtonContainer: {
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing16,
        marginRight: spacing.spacing16,
        marginBottom: spacing.spacing16,
        justifyContent: 'flex-end',
        alignSelf: 'stretch',
    },
    SubTitleText: {
        ...OATextStyle.TextFontSize_12_normal,
        marginTop: spacing.spacing6,
    },
    ImageLeft: {
        marginTop: spacing.spacing16,
        marginLeft: spacing.spacing16,
        height: dimens.dimen24,
        maxWidth: 30
    },
    ImageRight: {
        alignSelf: 'center',
        marginTop: spacing.spacing12,
        marginRight: spacing.spacing16,
        justifyContent: 'flex-end',
        width: 12,
        height: 7
    },
    RightImageContainer: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'flex-end'
    },
    navigationTitle: {
        ...OATextStyle.TextFontSize_18_bold_212121
    },
    SeparatorViewStyle: {backgroundColor: colors.color_F6F6F6},
    hyperlinkTitleStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        lineHeight: spacing.spacing22,
        color: colors.color_2980b9
    },
    hyperlinkContainerStyle: {
        marginTop: spacing.spacing8,
        marginBottom: spacing.spacing8
    },

    imageViewFromUrlContainerStyle: {
        marginTop: spacing.spacing8
    },

    hyperlinkSubTitleStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        lineHeight: spacing.spacing22,
        color: colors.color_212121
    },
    touchableOpacityStyle: {
        flex: spacing.spacing1,
        flexDirection: 'row',
        alignItems: 'flex-end',
        backgroundColor: colors.color_07000000
    },
    listContainerStyle: {
        alignSelf: 'flex-start',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        maxHeight: '80%',
        flexDirection: 'row',
        backgroundColor: colors.color_FFFFFF,
        padding: spacing.spacing16

    },
    recommendedActionsTitleStyle: {
        ...OATextStyle.TextFontSize_10_bold_888F97,
        textTransform: 'uppercase'
    },
    expandedViewTitleStyle: {
        ...OATextStyle.TextFontSize_14_normal,
        color: colors.color_888F97,
        flex: spacing.spacing1,
        paddingTop: spacing.spacing4
    },
    actionViewContainerStyle: {
        flexDirection: 'row',
        height: dimens.dimen75,
        width: dimens.width100,
        backgroundColor: colors.color_FFFFFF,
        borderColor: "transparent",
        shadowColor: colors.color_000000,
        shadowOffset: {
            width: dimens.dimen0,
            height: -5,
        },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 4,
    },
    actionMainViewStyle: {
        width: dimens.width100,
    },
});
