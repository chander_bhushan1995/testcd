import { Text, View } from "react-native";
import { AlertsTabConstants } from "../../../../constants/Constants";
import {
  alertsExpandedViewTitle,
  getCompromisedType,
  isBadAlert,
  isReportAlert
} from "../../../../constants/IDFence.UtilityMethods";
import styles from "../../alertTab/cellexpandedview/AlertsTab.CellExpandedView.style";
import React from "react";

export default function AlertDetailsDescription({ item }) {
  return getExpendedCell(item);
}

function getExpendedCell(item) {
  if (!item) return null;
  let expandedCell = null;
  if (item.itemType === AlertsTabConstants.ALERT_TYPE_NORMAL) {
    if (isReportAlert(item.data)) {
      expandedCell = (
        <View style={styles.expandedMainContainer}>
          {getViewForReportInfo(item)}
        </View>
      );
      return expandedCell;
    }

    if (isBadAlert(item.data)) {
      expandedCell = (
        <View style={styles.expandedMainContainer}>
          {getViewForNormalBadAlertInfo(item)}
        </View>
      );
      return expandedCell;
    }

    expandedCell = (
      <View style={styles.expandedMainContainer}>
        {getViewForNormalBadAlertInfo(item)}
      </View>
    );

    return expandedCell;
  }
  if (isBadAlert(item.data)) {
    expandedCell = <View>{getViewForCreditScoreAlertInfo(item)}</View>;
  } else {
    expandedCell = <View>{getViewForCreditScoreAlertInfo(item)}</View>;
  }

  return expandedCell;
}

function getViewForCreditScoreAlertInfo(item) {
  return (
    <View style={styles.mainContainerWithMinH170}>
      <View>
        <Text style={styles.expandedViewTitleStyle}>
          {alertsExpandedViewTitle(item)}
        </Text>
      </View>
      <View style={styles.expandedViewAddInfoContainerStyle}>
        {creditScoreAlertAdditionalInfo(item.data["keyValuePairs"])}
      </View>
    </View>
  );
}
function creditScoreAlertAdditionalInfo(keyValuePairs) {
  let addInfoArr = [];
  if (keyValuePairs !== null || keyValuePairs !== undefined) {
    Object.keys(keyValuePairs).forEach(function eachKey(key) {
      let viewItem = (
        <View style={styles.creditScoreAlertAddInfoContainerStyle}>
          <View style={styles.compromisedTypeLabelContainerStyle}>
            <Text style={styles.compromisedTypeLabelStyle}>{key}: </Text>
          </View>
          <Text style={styles.expandedViewTitleStyle}>
            {keyValuePairs[key]}
          </Text>
        </View>
      );
      addInfoArr.push(viewItem);
    });
    return addInfoArr;
  }
  return null;
}

function getViewForNormalBadAlertInfo(item) {
  return (
    <View style={styles.alertDescContainer}>
      <Text style={styles.expandedViewTitleStyle}>
        {alertsExpandedViewTitle(item)}
      </Text>
      {getViewForAlertCompromisedType(item)}
    </View>
  );
}

function getViewForAlertCompromisedType(item) {
  if (getCompromisedType(item) === "") {
    return null;
  }
  let viewItem = (
    <View style={styles.creditScoreAlertAddInfoContainerStyle}>
      <View style={styles.compromisedTypeLabelContainerStyle}>
        <Text style={styles.compromisedTypeLabelStyle}>Type: </Text>
      </View>
      <Text style={styles.expandedViewTitleStyle}>
        {getCompromisedType(item)}
      </Text>
    </View>
  );
  return viewItem;
}

function getViewForReportInfo(item) {
  if (isBadAlert(item.data)) {
    return (
      <View>
        <View>
          <View style={styles.alertDescContainer}>
            <Text style={styles.expandedViewTitleStyle}>
              {alertsExpandedViewTitle(item)}
            </Text>
          </View>
        </View>
      </View>
    );
  }
  return (
    <View>
      <View style={styles.alertDescContainer}>
        <Text style={styles.expandedViewTitleStyle}>
          {alertsExpandedViewTitle(item)}
        </Text>
      </View>
    </View>
  );
}
