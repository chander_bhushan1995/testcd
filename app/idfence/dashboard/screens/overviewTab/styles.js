import {Platform, StyleSheet} from "react-native";
import spacing from "../../../../Constants/Spacing";
import colors from "../../../../Constants/colors";
import {CommonStyle, TextStyle} from "../../../../Constants/CommonStyle";
import {OATextStyle} from "../../../../Constants/commonstyles";

export const styles = StyleSheet.create({
    SafeArea: {
        flex: 1,
        backgroundColor: colors.white
    },
    scrollViewStyle: {
        flex: 1,
        paddingBottom: spacing.spacing32,
        backgroundColor: colors.white
    },
    container: {
        marginTop: 0,
    },
    cellContainerStyle: {
        marginVertical: 0
    },
    cellHeaderTitleStyle: {
        marginBottom: spacing.spacing8,
        ...TextStyle.text_10_bold,
        paddingRight: spacing.spacing16,
        paddingLeft: spacing.spacing16,
    },
    cellBoldHeaderTitleStyle: {
        marginBottom: spacing.spacing8,
        ...TextStyle.text_18_bold,
        paddingRight: spacing.spacing16,
        paddingLeft: spacing.spacing16,
    },
    defaultHorizontalPaddingStyle:{
        paddingHorizontal: spacing.spacing16,
    },
    roundedCornerContainerWithShadow: {
        justifyContent: "space-around",
        backgroundColor: colors.white,
        borderRadius: spacing.spacing4, ...CommonStyle.cardContainerWithShadow
    },
    circularSliderContainerStyle: {
        ...CommonStyle.cardContainerWithShadow,
        borderRadius: spacing.spacing4,
        height: 270,
        paddingTop: spacing.spacing8,
        paddingBottom: spacing.spacing8
    },
    roundedCornerContainer: {
        borderRadius: spacing.spacing4
    },
    poweredByExperianTitleStyle: {
        ...OATextStyle.TextFontSize_8_normal,
        alignSelf: 'flex-end',
        paddingRight: spacing.spacing8
    },
    contentContainerStyle: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    creditScoreStyle: {
        ...OATextStyle.TextFontSize_20_bold_212121,
        color: colors.color_45B448
    },
    circleWidthShadow: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: {width: 0, height: 2},
                shadowOpacity: 0.2,
            },
            android: {
                elevation: 1

            },
        }),
    }
});

//*********** child components customization  ***************//

export const alertsOverviewStyle = {
    titleText: {
        ...TextStyle.text_16_bold,
        color: colors.salmonRed
    },
    descriptionText: {...TextStyle.text_14_normal}
};

export const IDCardStyle = {
    buttonStyle: {
        borderRadius: 0,
        borderColor: "transparent",
        marginTop: spacing.spacing10
    },
    actionTextStyle: {
        color: colors.blue
    }
};
export const waringViewStyle = {};
export const SocialCardStyle = {
    titleStyle: {
        color: colors.white
    }
};
