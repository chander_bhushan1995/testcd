import { createStackNavigator } from "react-navigation";
import OAWebView from "../../CustomComponent/webview/OAWebViewAction";
import AlertDetails from '../dashboard/screens/overviewTab/alertDetails/AlertDetails';
import DashboardAction from "../../idfence/dashboard/DashboardAction";
import activateCreditScoreComponentAction
    from "../dashboard/screens/overviewTab/activateCreditScore/ActivateCreditScoreComponent.Action";
import CreditScoreDashboard from "../../idfence/creditscore/creditscoredashboard/CreditScoreDashboard.Action";
import CSPaymentHistory from "../../idfence/creditscore/creditscoredashboard/paymenthistory/CSPaymentHistory.Action";
import SelectedItemDetail
    from "../creditscore/creditscoredashboard/paymenthistory/selecteditemdetail/SelectedItemDetail.component";
import CSCreditUtilisation
    from "../../idfence/creditscore/creditscoredashboard/creditutilisation/CSCreditUtilisation.Action";
import CSOldestOpenAccount
    from "../../idfence/creditscore/creditscoredashboard/oldestopenaccount/CSOldestOpenAccount.Action";
import CSCardsAndAccounts
    from "../../idfence/creditscore/creditscoredashboard/cardsandaccounts/CSCardsAndAccounts.Action";
import CreditEnquiries
    from "../../idfence/creditscore/creditscoredashboard/creditenquiries/CreditEnquiries.Action";
import MonitorAssets
    from "../../idfence/creditscore/creditscoredashboard/monitorassets/MonitorAssets.Action";
import TnCWebViewComponent from "../../PaymentFlow/Components/TnCWebViewComponent";
import PaymentWebViewComponent from "../../PaymentFlow/Components/Payment/PaymentWebViewComponent";

const AppNavigator = createStackNavigator(
    {
        DashboardComponent: {
            screen: DashboardAction,
            navigationOptions: {
                title: "dashboard",
                gesturesEnabled: false
            }
        },
        OAWebView: {
            screen: OAWebView,
            navigationOptions: {
                gesturesEnabled: false
            }
        },
        AlertDetails: {
            screen: AlertDetails,
            navigationOptions: {
                gesturesEnabled: false
            }
        },
        ActivateCreditScoreComponent: {
            screen: activateCreditScoreComponentAction,
            navigationOptions: {
                gesturesEnabled: false
            }
        }, CSCreditUtilisation: {
            screen: CSCreditUtilisation,
            navigationOptions: {
                title: "Credit Utilisation",
                gesturesEnabled: false
            }
        },
        SelectedItemDetail: {
            screen: SelectedItemDetail,
            navigationOptions: {
                title: "Payment details",
                gesturesEnabled: false
            }
        },
        CreditScoreDashboard: {
            screen: CreditScoreDashboard,
            navigationOptions: {
                title: "Your credit score",
                gesturesEnabled: false
            }
        },
        CSPaymentHistory: {
            screen: CSPaymentHistory,
            navigationOptions: {
                title: "Payment History",
                gesturesEnabled: false
            }
        },
        CSOldestOpenAccount: {
            screen: CSOldestOpenAccount,
            navigationOptions: {
                title: "Oldest Open Account",
                gesturesEnabled: false
            }
        },
        CSCardsAndAccounts: {
            screen: CSCardsAndAccounts,
            navigationOptions: {
                title: "Cards and Accounts",
                gesturesEnabled: false
            }
        },
        CreditEnquiries: {
            screen: CreditEnquiries,
            navigationOptions: {
                title: "Credit Enquiries",
                gesturesEnabled: false
            }
        },
        MonitorAssets: {
            screen: MonitorAssets,
            navigationOptions: {
                title: "Monitor Assets",
                gesturesEnabled: false
            }
        },
        TnCWebViewComponent: {
            screen: TnCWebViewComponent,
            navigationOptions: {
                title: "Terms & Conditions",
                gesturesEnabled: false,
            }
        },
        PaymentWebViewComponent: {
            screen: PaymentWebViewComponent,
            navigationOptions: {
                title: "Payment Review",
                gesturesEnabled: false,
            }
        },
    },

    {
        mode: "modal"
    }
);

export default AppNavigator;
