import { APIData } from '../../index';
import { executeApi, HTTP_GET, HTTP_POST, promiseForGetRequest } from "./ApiManager";
import {getCDNDataVersioning, getFirebaseData} from '../commonUtil/AppUtils';
import {getTodayDate} from "../commonUtil/DateUtils";
import {Category, PlanServices, TaskTypes} from "../Constants/AppConstants";
import {REQUEST_TYPE} from "../Constants/MembershipConstants";
import {getServiceRequestParams} from "../MembershipTab/Helpers/MembershipTabUtils";

import {
    API_HEADERS,
    GET_BUYBACK,
    ALLCARDS,
    SPOTLIGHT,
    RECOMMENDAION_SPOTLIGHT,
    CARD_OFFERS,
    HOME_SCREEN_DATA,
    GET_SERVICE_REQUEST,
    Get_Pending_SR_Orders,
    GET_CATALYST_RECOMMENDED_UI_COMPONENTS_SUB_URL,
    UPDATE_FAVORITE_RECOMMENDATIONS_URL,
    UPDATE_ONGOING_BUYBACK,
    GET_BUYBACK_REQUEST_FEEDBACK_STATUS_URL,
    BUY_TAB_SPLIT_URL,
    GET_BRANDS,
    GET_OFFER_DATA,
    GET_PRICE_RANGES,
    GET_PRODUCT_SPECIFICATIONS,
    GET_SOD_PRODUCTS,
    GET_TESTIMONIALS,
    GET_TIME_SLOTS,
    RENEW_MEMBERSHIP,
    REPORT_FOR_PINCODE,
    SERVICE_TASKS,
    SUGGEST_PLANS,
    WHC_NOTIFY,
    WHC_PINCODE_SERVICEABILITY,
    MEMBERSHIP_TAB_API_URLS,
    GET_CUSTOMER_DETAILS,
    GET_QUESTIONS,
    SEND_EMAIL_VERIFICATION_LINK
} from '../Constants/ApiUrls';

import {
    getRatingReviewRequest,
    getRenewalRequest,
    getRenewalRequestForHA,
    getSuggestPlanRequest,
    parseTestimonialResponse,
} from "../BuyPlan/Helpers/BuyPlanUtils";


/***
 * Api Helper class
 * @param apiParams
 * @returns {*}
 */

const mobileNumber = () => { return APIData.mobile_number }
const custId = () => { return APIData?.customer_id }


const getDataFromApiGateway = (apiParams) => {
    apiParams.apiProps = { baseUrl: APIData?.api_gateway_base_url, header: APIData?.apiHeader };
    return apiParams;
};

const getDataFromApi = (apiParams) => {
    apiParams.apiProps = {baseUrl: APIData?.api_base_url, header: APIData?.apiHeader};
    return apiParams;
};

export const getMembershipArrayForLoggedInUserWith = (callback) => {
    let requestData = {
        assetRequired: "true",
        claimRequired: "true",
        mobileNo: mobileNumber(),
        membershipType: "A,P,E,X",
        assetDocRequired: "true",
        assetAttributesRequired: "true",
        serviceRequestTypes: "HA_EW,HA_BD,HA_AD,HA_FR,HA_BR,PE_ADLD,PE_THEFT,PE_EW,PMS",
        renewalEligibilityRequired: "true",
        customerInfoRequired: "true",
        checkClaimEligibility: "true",
        assetScopeRequired: "true",
        assetServicesRequired: "true",
        assetRenewalEligibilityRequired: "true",
        leadDataRequired: "true",
        customerAddressRequired: "true",
    }
    let apiParams = {
        httpType: HTTP_GET,
        url: MEMBERSHIP_TAB_API_URLS.GET_MEMBERSHIPS + new URLSearchParams(requestData).toString(),
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams));
};

// MARK: - HOME SCREEN APIs

export const getAllCards = (callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: ALLCARDS(custId()),
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getCardOffers = (cardsData, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: CARD_OFFERS,
        requestBody: {
            query: {
                addedCards: cardsData.length > 0 ? cardsData : [],
                otherCards: [],
                customerId: custId() ?? null,
                offerType: 'Online',
                pageNo: 1,
                pageSize: 10,
            },
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getBuyBackData = (callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: GET_BUYBACK(custId(), APIData?.modelName, APIData?.RAM, APIData?.ROM, APIData?.deviceIdentifier),
        callback: callback,
    };
    // TODO:add rerty count here
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getSpotLightData = (isCatalyst, callback) => {

    getCDNDataVersioning((data) => {
        let url = APIData?.cdn_base_url;

        if (isCatalyst) {
            url += RECOMMENDAION_SPOTLIGHT
        } else {
            url += SPOTLIGHT;
            if (data?.spotlightData) {
                url += '?' + data?.spotlightData;
            }
        }

        let apiParams = {
            httpType: HTTP_GET,
            url: url,
            callback: callback,
        };
        apiParams.apiProps = {baseUrl: '', header: APIData?.apiHeader};
        executeApi(apiParams).done();
    });
};

export const getHomeScreenData = (callback) => {

    getCDNDataVersioning((data) => {

        let url = APIData.cdn_base_url + HOME_SCREEN_DATA;

        if (data?.homeScreenData) {
            url += '?' + data?.homeScreenData;
        }

        let apiParams = {
            httpType: HTTP_GET,
            url: url,
            callback: callback,
        };
        apiParams.apiProps = {baseUrl: '', header: APIData?.apiHeader};
        executeApi(apiParams).done();
    });
};

export const getServiceRequests = (callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: GET_SERVICE_REQUEST(custId(), 'showAssets,serviceOrderDetails'),
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getPendingSODServices = (callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: Get_Pending_SR_Orders(custId(), 'SO'),
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getCatalystRecommendedUIComponents = (isOnlyFavorite, callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: GET_CATALYST_RECOMMENDED_UI_COMPONENTS_SUB_URL(custId(), isOnlyFavorite),
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const updateFavoriteRecommendation = (rcId, favoriteData, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: UPDATE_FAVORITE_RECOMMENDATIONS_URL(custId()),
        requestBody: {
            rcId: rcId,
            components: favoriteData,
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
}

export const cancelRequest = (quoteId,customerId,callback) => {
    let body = {quoteId: quoteId, modifiedBy: customerId, status:"EXPIRED"};
    let apiParams = {
        httpType: HTTP_POST,
        url: UPDATE_ONGOING_BUYBACK(quoteId),
        requestBody: body,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
}

export const getBuybackRequestFeedbackStatus = (refPrimaryTrackingNos, callback) => {
    let apiParams = {
        httpType: HTTP_GET,
        url: GET_BUYBACK_REQUEST_FEEDBACK_STATUS_URL(refPrimaryTrackingNos),
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
}

// MARK:- BUY SCREEN APIs

export const getDataFromCDN = (url, params, callback) => {
    let finalURL = BUY_TAB_SPLIT_URL;
    let pageVariationId = ""
    getCDNDataVersioning((data) => {
        if (data.apiConfiguration) {
            if (data.apiConfiguration?.buyTab && data.apiConfiguration?.buyTab[url]) {
                finalURL += "/" + data.apiConfiguration?.buyTab[url]?.folderPath + `/${url}.json?` + data.apiConfiguration?.buyTab[url]?.dataVersion;
                pageVariationId = data.apiConfiguration?.buyTab[url]?.folderPath
            } else if (callback) {
                callback(null, {message: `${url} not found.`});
                return;
            }
        }

        let apiParams = {
            httpType: HTTP_GET,
            url: finalURL,
            callback: callback,
            isCacheEnabled: params?.isCacheEnabled,
            cacheValidity: 10, // in days
            pageVariationId: pageVariationId
        };
        apiParams.apiProps = {baseUrl: APIData.cdn_base_url, header: APIData.apiHeader};
        executeApi(apiParams).done();
    });
};

export const getSODProductPage = (category, callback) => {
    let url = `${(category ?? "").toLowerCase()}_product_screen`;
    getDataFromCDN(url, {isCacheEnabled: true}, callback);
};

export const getSODProductServicePage = (category, selectedService, callback) => {
    let service = `${selectedService.replace(/ /g, "_").toLowerCase()}`;
    let categoryString = `${category.replace(/ /g, "_").toLowerCase()}`;
    let url = `${categoryString}_${service}_product_service_screen`;
    getDataFromCDN(url, {isCacheEnabled: true}, callback);
};

export const getDataFromCDNWithURL = (url, callback) => {
    let pageVariationId = ""
    let splitUrl = url?.split('/') ?? [];
    if (splitUrl.length > 1){
        splitUrl.pop();
    }
    pageVariationId =  splitUrl.last()

    let apiParams = {
        httpType: HTTP_GET,
        url: url,
        callback: callback,
        pageVariationId: pageVariationId
    };
    apiParams.apiProps = {baseUrl: "", header: APIData.apiHeader};
    executeApi(apiParams).done();
};

export const fetchTestimonialData = (tabCategory, service, product, page, pageSize, callback) => {
    let categoryNService = "";
    let category = tabCategory;
    let productName = product;
    if (productName) {
        if(productName === "Mobile") {
            productName = "MobileAssist";
        }else if(productName.toLowerCase() === "wallet"){
            productName = "WalletAssist";
        }
    }
    if (tabCategory === Category.personalElectronics && service) {
        categoryNService = category + "_" + (service === PlanServices.extendedWarranty ? "EW" : service);
    } else if (tabCategory === Category.homeAppliances && service) {
        if (service === PlanServices.extendedWarranty) {
            categoryNService = category + "_EW"
        } else {
            categoryNService = service
        }
    } else {
        categoryNService = "";
    }
    let url = GET_TESTIMONIALS(tabCategory, categoryNService, productName);
    let requestObj = getRatingReviewRequest(page, pageSize);
    let apiParams = {
        httpType: HTTP_POST,
        url: url,
        requestBody: requestObj,
        callback: (response, error) => {
            if (!error && response) {
                callback(parseTestimonialResponse(response));
            } else {
                // let parsedRes = parseTestimonialResponse(NEW_TEST_DATA)
                callback(null);
            }
        },
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getOffersData = (callback) => {
    let url = GET_OFFER_DATA;
    getDataFromCDN(url, {isCacheEnabled: false}, callback);
};

export const getTabsData = (tabCategory, callback) => {
    let url = `${tabCategory.toLowerCase()}_tabpage`;
    console.log("getTabsData ",url);
    getDataFromCDN(url, {isCacheEnabled: true}, callback);
};

export const getServicePageFromCDN = (isForTrial, category, service, productName, callback) => {
    let url = `${category?.toLowerCase()}`;
    if (productName && category != Category.finance) {
        url += `_${productName.replace(/ /g, "_").toLowerCase()}`;
    }
    if (service) {
        url += `_${service.replace(/ /g, "_").toLowerCase()}`;
    }
    url += isForTrial ? "_trial_detailpage" : "_detailpage";
    getDataFromCDN(url, {isCacheEnabled: true}, callback);
};

export const getPlanListingFromCDN = (category, service, productName, callback) => {
    let url = `${category?.toLowerCase()}`;
    if (productName && category !== Category.homeAppliances && category !== Category.finance) {
        url += `_${productName.replace(/ /g, "_").toLowerCase()}`;
    }
    if (service) {
        url += `_${service.replace(/ /g, "_").toLowerCase()}`;
    }
    url += "_planlistingpage";
    getDataFromCDN(url, {isCacheEnabled: true}, callback);
};

export const verifyForPin = (pincode, serviceRequestSourceType, callback) => {
    let url = WHC_PINCODE_SERVICEABILITY + pincode;
    if (serviceRequestSourceType) {
        url += `?serviceRequestSourceType=${serviceRequestSourceType}`;
    }
    let apiParams = {
        httpType: HTTP_GET,
        url: url,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const notifyMe = (name, mobile, pincode, email, campaignId, callback) => {
    let url = WHC_NOTIFY + `last_name=${name ?? ""}&email_c=${email ?? APIData.user_Email ?? ""}&phone_mobile=${mobile}&whc_postalcode=${pincode ?? ""}&lead_source=Assist_app&lead_source_description=Mobile_description&BUSS_PARTNER_NAME_C=${APIData.BP_NAME}&BUS_UNIT_NAME_C=${APIData.BU_NAME}&customer_status_c=lead&campaign_id=${campaignId ?? ""}&assigned_user_id=1&req_id=last_name`;

    let apiParams = {
        httpType: HTTP_GET,
        url: url,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getSRsReportForPinCode = (pinCode, serviceRequestType, serviceCategory, callback) => {
    let url = REPORT_FOR_PINCODE(pinCode);

    if (serviceRequestType && serviceCategory) {
        url += `?serviceRequestSourceType=${serviceRequestType}&serviceCategory=${serviceCategory}`;
    }

    let apiParams = {
        httpType: HTTP_GET,
        url: url,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};


export const loadIDFenceTrialPlans = (category, service, callback) => {
    let requestObj = getSuggestPlanRequest(category, service, true, null);
    let apiParams = {
        httpType: HTTP_POST,
        url: SUGGEST_PLANS,
        requestBody: requestObj,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const loadIDFencePremiumPlans = (category, service, trialPlanCode, callback) => {
    let requestObj = getSuggestPlanRequest(category, service, false, null, trialPlanCode);
    let apiParams = {
        httpType: HTTP_POST,
        url: SUGGEST_PLANS,
        requestBody: requestObj,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const renewMembership = (planCode, membership, callback) => {
    let requestObj = getRenewalRequest(planCode, membership, APIData);
    let apiParams = {
        httpType: HTTP_POST,
        url: RENEW_MEMBERSHIP,
        requestBody: requestObj,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const renewHAMembership = (planCode, buyPlanJourney, callback) => {
    let requestObj = getRenewalRequestForHA(planCode, buyPlanJourney, APIData);
    let apiParams = {
        httpType: HTTP_POST,
        url: RENEW_MEMBERSHIP,
        requestBody: requestObj,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getSODProductsList = (productCodes = [], serviceRequestTypes = [], callback) => {
    let pc = productCodes ?? [];
    let srTypes = serviceRequestTypes ?? [];
    let url = GET_SOD_PRODUCTS + "?productCodes=" + pc.join(",") + "&serviceRequestTypes=" + srTypes.join(",") + "&pageNo=0&pageSize=100&sortBy=serviceId&sortType=DESC";
    let apiParams = {
        httpType: HTTP_GET,
        url: url,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getSODProductsOffers = (callback) => {
    getFirebaseData("sod_appliance_offers", callback);
};

export const getPlansForSOD = (productCode, serviceRequestTypes = [], callback) => { // need to be develop
    let requestObj = getSuggestPlanRequest(Category.quickRapairs, serviceRequestTypes, false, {productCode: productCode});
    let apiParams = {
        httpType: HTTP_POST,
        url: SUGGEST_PLANS,
        requestBody: requestObj,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getTimeSlotsForToday = (serviceRequestType, serviceRequestSourceType, pinCode, bpCode, buCode, productCode, callback) => {
    let url = GET_TIME_SLOTS(serviceRequestType, serviceRequestSourceType, getTodayDate(), pinCode, bpCode, buCode, productCode);
    let apiParams = {
        httpType: HTTP_GET,
        url: url,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getProductsSpecs = (productCodes = [], callback) => {
    let pc = productCodes ?? [];
    let url = GET_PRODUCT_SPECIFICATIONS + "?productCodes=" + pc.join(",") + "&isSubscription=false";
    let apiParams = {
        httpType: HTTP_GET,
        url: url,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getServiceTask = (productCode, taskType = TaskTypes.ISSUE, productVariantIds, callback) => {
    let url = SERVICE_TASKS(productCode, taskType, "A");
    if (taskType === TaskTypes.ISSUE) {
        url += "&options=distinctByName";
    } else if (taskType === TaskTypes.LABOUR_COST && Array.isArray(productVariantIds) && productVariantIds.length > 0) {
        url += `&productVariantIds=${productVariantIds.join(",")}`;
    }
    let apiParams = {
        httpType: HTTP_GET,
        url: url,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const fetchBrands = (category, productCode, serviceType, callback) => {
    let url = GET_BRANDS();
    if (!productCode) {
        callback("No product selected")
        return
    }
    let service = category == Category.personalElectronics ? "Default" : serviceType
    let requestBody = {
        serviceType: service,
        subCategoryCodeList: [productCode]
    }
    let apiParams = {
        httpType: HTTP_POST,
        url: url,
        requestBody: requestBody,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getPlansForWHC = (callback) => { // need to be develop
    let requestObj = getSuggestPlanRequest(Category.homeAppliances, PlanServices.whcInspection, false, null);
    let apiParams = {
        httpType: HTTP_POST,
        url: SUGGEST_PLANS,
        requestBody: requestObj,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const fetchSuggestedPlans = (buyPlanJourney, callback) => { // need to be develop
    let extraObj = {
        productCode: buyPlanJourney?.product?.code,
        brandName: buyPlanJourney?.brandCode ?? buyPlanJourney?.brandName,
        invoiceRange: buyPlanJourney?.invoiceRange,
        invoiceDate: buyPlanJourney?.invoiceDate,
        invoiceValue: buyPlanJourney?.invoiceValue,
    };
    let requestObj = getSuggestPlanRequest(buyPlanJourney?.category, buyPlanJourney?.service,
        false, extraObj);

    let apiParams = {
        httpType: HTTP_POST,
        url: SUGGEST_PLANS,
        requestBody: requestObj,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const suggestPlans = (category, service, callback) => {
    let requestObj = getSuggestPlanRequest(category, service, false, null);
    let apiParams = {
        httpType: HTTP_POST,
        url: SUGGEST_PLANS,
        requestBody: requestObj,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const fetchPriceRanges = (service, product, brand, callback) => {
    let url = GET_PRICE_RANGES(service, product, brand);
    let apiParams = {
        httpType: HTTP_GET,
        url: url,
        callback: (response, error) => {
            if (!error && response) {
                callback(response);
            } else {
                callback(null);
            }
        },
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

// MARK:- MEMBERSHIP SCREEN APIs

/**
 *  Method to get all require api's response before membershipTab Rendering
 * @param membershipIDs
 * @param buyBackRequestData
 * @param activationCodeList
 * @param idFenceMembershipsUUIDs
 * @param callback
 */

/**
 * Enum for api call response.
 * @readonly
 * @enum {{name: string, hex: string}}
 */
export const INITIAL_API_RESPONSE_TAGS = Object.freeze({
    SR_DRAFT_DATA: 'SR_DRAFT_DATA',
    SERVICE_REQUEST_DATA: 'SERVICE_REQUEST_DATA',
    BUY_BACK_DATA: 'BUY_BACK_DATA',
    TEMP_CUST_INFO_LIST:'TEMP_CUST_INFO_LIST',
    TEMP_DOC_TO_UPLOAD_LIST:'TEMP_DOC_TO_UPLOAD_LIST',
    IDFENCE_SI_DATA_LIST:'IDFENCE_SI_DATA_LIST',

});
export const initialMembershipApiCalls = (membershipIDs, buyBackRequestData, activationCodeList,
                                          idFenceMembershipsUUIDs, callback) => {
    let apiRequestList = [];
    let params = APIData;
    let apiHeader = params?.apiHeader;
    let responseArr = [];

    if (membershipIDs?.length > 0) {
        let membershipCommaSeparatedStr = membershipIDs?.join(",");
        //SRDraftData
        let urlSRDraft = params?.api_gateway_base_url + MEMBERSHIP_TAB_API_URLS.GET_INTIMATION(membershipCommaSeparatedStr);
        const promiseSRDraftData = promiseForGetRequest(urlSRDraft, apiHeader);
        responseArr.push(INITIAL_API_RESPONSE_TAGS.SR_DRAFT_DATA);
        apiRequestList.push([promiseSRDraftData]);

        //ServiceRequests
        let srRequestParams = getServiceRequestParams(REQUEST_TYPE.REQUEST_MEMBERSHIP_TAB, {membershipIDs: membershipCommaSeparatedStr})
        let urlServiceRequest = params?.api_gateway_base_url + MEMBERSHIP_TAB_API_URLS.GET_SERVICE_REQUESTS + new URLSearchParams(srRequestParams).toString();
        const promiseServiceRequest = promiseForGetRequest(urlServiceRequest, apiHeader);
        responseArr.push(INITIAL_API_RESPONSE_TAGS.SERVICE_REQUEST_DATA);
        apiRequestList.push([promiseServiceRequest]);
    }


    if (buyBackRequestData) {
        //getBuybackStatus
        let urlBuybackStatus = params?.api_gateway_base_url + MEMBERSHIP_TAB_API_URLS.GET_BUYBACK_STATUS + new URLSearchParams(buyBackRequestData).toString();
        const promiseBuybackStatus = promiseForGetRequest(urlBuybackStatus, apiHeader);
        responseArr.push(INITIAL_API_RESPONSE_TAGS.BUY_BACK_DATA);
        apiRequestList.push([promiseBuybackStatus]);
    }
    if (activationCodeList?.length > 0) {
        // GET_TEMP_CUSTOMER_INFO
        let apiRequestTempCustInfoList = [];
        activationCodeList?.forEach(activationCode => {
                // Add getTempInfoCall Calls
                apiRequestTempCustInfoList.push(promiseForGetRequest(
                    params?.api_gateway_base_url + MEMBERSHIP_TAB_API_URLS.GET_TEMP_CUSTOMER_INFO(activationCode),
                    params?.apiHeader
                ));
            }
        );
        responseArr.push(INITIAL_API_RESPONSE_TAGS.TEMP_CUST_INFO_LIST);
        apiRequestList.push(apiRequestTempCustInfoList);
        //GET_DOCS_TO_UPLOAD
        apiRequestTempCustInfoList = [];
        activationCodeList?.forEach(activationCode => {
                // Add getDocsToUpload Calls
                let apiHeader = { ...params?.apiHeader, 'Content-Type': API_HEADERS.CONTENT_TYPE_TEXT_HTML_UTF_8 }
                apiRequestTempCustInfoList.push(promiseForGetRequest(
                    params?.api_gateway_base_url + MEMBERSHIP_TAB_API_URLS.GET_DOCS_TO_UPLOAD(activationCode),
                    apiHeader
                ));
            }
        );
        responseArr.push(INITIAL_API_RESPONSE_TAGS.TEMP_DOC_TO_UPLOAD_LIST);
        apiRequestList.push(apiRequestTempCustInfoList);
    }

    if (idFenceMembershipsUUIDs?.length > 0) {
        // IdFence getIdFenceSIData
        let apiSIDataRequestList = [];
        idFenceMembershipsUUIDs?.forEach(uuid => {
                // Add getIdFenceSIData Calls
                let apiHeader = params?.apiHeader;
                apiSIDataRequestList.push(promiseForGetRequest(
                    params?.api_gateway_base_url + MEMBERSHIP_TAB_API_URLS.GET_IDFENCE_SI_DATA(uuid),
                    apiHeader
                ));
            }
        );
        responseArr.push(INITIAL_API_RESPONSE_TAGS.IDFENCE_SI_DATA_LIST);
        apiRequestList.push(apiSIDataRequestList);
    }
    Promise.all(apiRequestList.map(Promise.all.bind(Promise))).then(function (responses) {
        let responseMap = new Map();
        responses?.forEach((response, index) => {
            // map single response
            switch (responseArr[index]) {
                case INITIAL_API_RESPONSE_TAGS.SR_DRAFT_DATA:
                    responseMap.set(responseArr[index], response?.[0]);
                    break;
                case INITIAL_API_RESPONSE_TAGS.BUY_BACK_DATA:
                    responseMap.set(responseArr[index], response?.[0])
                    break;
                case INITIAL_API_RESPONSE_TAGS.SERVICE_REQUEST_DATA:
                    responseMap.set(responseArr[index], response?.[0])
                    break;
                default:
                    responseMap.set(responseArr[index], response)
            }

        })
        callback(responseMap, null);
    }).catch(function (error) {
        callback(null, error);
    });
}

export const getTempCustomerInfo = (activationCodeList, callback) => {
    let apiRequestList = [];
    let params = APIData;
    activationCodeList?.forEach(activationCode => {
            // Add getTempInfoCall Calls
            apiRequestList.push(promiseForGetRequest(
                params?.api_gateway_base_url + MEMBERSHIP_TAB_API_URLS.GET_TEMP_CUSTOMER_INFO(activationCode),
                params?.apiHeader
            ));
        }
    );
    Promise.all(apiRequestList).then(function (responses) {
        callback(responses, null);
    }).catch(function (error) {
        callback(null, error);
    });
};

export const getDocsToUpload = (activationCodeList, callback) => {
    let apiRequestList = [];
    let params = APIData;
    activationCodeList?.forEach(activationCode => {
            // Add getDocsToUpload Calls
            let apiHeader = { ...params?.apiHeader, 'Content-Type': API_HEADERS.CONTENT_TYPE_TEXT_HTML_UTF_8 }
            apiRequestList.push(promiseForGetRequest(
                params?.api_gateway_base_url + MEMBERSHIP_TAB_API_URLS.GET_DOCS_TO_UPLOAD(activationCode),
                apiHeader
            ));
        }
    );
    Promise.all(apiRequestList).then(function (responses) {
        callback(responses, null);
    }).catch(function (error) {
        callback(null, error);
    });
};

export const getSRDraftData = (membershipIDs, callback) => {
    let membershipCommaSeparatedStr = membershipIDs?.join(",");
    let url = MEMBERSHIP_TAB_API_URLS.GET_INTIMATION(membershipCommaSeparatedStr);
    let apiParams = {
        httpType: HTTP_GET,
        url: url,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};
export const getMembershipServiceRequests = (params, callback) => {
    let memCommaSeparatedStr = params?.membershipIDs?.join(",");
    let requestParams = getServiceRequestParams(REQUEST_TYPE.REQUEST_MEMBERSHIP_TAB, {membershipIDs: memCommaSeparatedStr})

    let url = MEMBERSHIP_TAB_API_URLS.GET_SERVICE_REQUESTS + new URLSearchParams(requestParams).toString();
    ;
    let apiParams = {
        httpType: HTTP_GET,
        url: url,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};
export const getBuybackStatus = (requestData, callback) => {

    let url = MEMBERSHIP_TAB_API_URLS.GET_BUYBACK_STATUS + new URLSearchParams(requestData).toString();
    let apiParams = {
        httpType: HTTP_GET,
        url: url,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
}

export const checkMembershipEligibility = (requestData, callback) => {
    let url = MEMBERSHIP_TAB_API_URLS.GET_CLAIM_CHECK_ELIGIBILITY + new URLSearchParams(requestData).toString();
    let apiParams = {
        httpType: HTTP_GET,
        url: url,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
}
export const getMemberShipDetail = (requestData, callback) => {

    let url = MEMBERSHIP_TAB_API_URLS.GET_CUSTOMER_MEMBERSHIPS + new URLSearchParams(requestData).toString();
    ;
    let apiParams = {
        httpType: HTTP_GET,
        url: url,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
}

export const membershipRenewalRequest = (requestBody, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: MEMBERSHIP_TAB_API_URLS.POST_RENEWALS_REQUEST,
        requestBody: requestBody,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getIdFenceSIData = (memUUIDList, callback) => {

    let apiSIDDataRequestList = [];
    let params = APIData;
    memUUIDList?.forEach(uuid => {
            // Add getIdFenceSIData Calls
            let apiHeader = params?.apiHeader;
            apiSIDDataRequestList.push(promiseForGetRequest(
                params?.api_gateway_base_url + MEMBERSHIP_TAB_API_URLS.GET_IDFENCE_SI_DATA(uuid),
                apiHeader
            ));
        }
    );
    Promise.all(apiSIDDataRequestList).then(function (responses) {
        let allResponse = memUUIDList?.map((uuid, index) => {
            return {uuid: uuid, response: responses?.[index]};
        });
        callback(allResponse, null);
    }).catch(function (error) {
        callback(null, error);
    });
};

export const checkSubscriptionStatus = (subscriberNo, callback) => {
    let url = MEMBERSHIP_TAB_API_URLS.GET_CHECK_SUBSCRIPTION_STATUS(subscriberNo);
    let apiParams = {
        httpType: HTTP_GET,
        url: url,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

// MARK:- ACCOUNT SCREEN APIs

export const getQuestions = (callback) => {
    let url = GET_QUESTIONS(custId());
    let apiParams = {
        httpType: HTTP_GET,
        url: url,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const getCustomerDetails = (callback) => {
    let url = GET_CUSTOMER_DETAILS(custId());
    let apiParams = {
        httpType: HTTP_GET,
        url: url,
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};

export const sendVerificationEmail = (email, callback) => {
    let apiParams = {
        httpType: HTTP_POST,
        url: SEND_EMAIL_VERIFICATION_LINK,
        requestBody: {
            emailId: email,
            custId: custId()
        },
        callback: callback,
    };
    executeApi(getDataFromApiGateway(apiParams)).done();
};
