import { NativeModules } from "react-native";
import { cacheData } from "../commonUtil/APIDataUtil";
import { parseJSON, deepCopy } from "../commonUtil/AppUtils";

const FAILURE = "failure";
const SUCCESS = "success";
export const HTTP_GET = "http_get";
export const HTTP_POST = "http_post";
export const HTTP_PUT = "http_put";

const nativeBridgeRef = NativeModules.ChatBridge;

export const executeApi = async (apiParams) => {
    switch (apiParams.httpType) {
        case HTTP_GET:
            executeGetRequest(apiParams);
            break;
        case HTTP_POST:
            executePostRequest(apiParams);
            break;
        case HTTP_PUT:
            executePutRequest(apiParams);
            break;
    }
};

function executePostRequest(apiParams) {
    const url = (apiParams?.apiProps?.baseUrl ?? "") + apiParams.url;
    const header = apiParams?.apiProps?.header;
    const requestBody = apiParams.requestBody;
    const callback = apiParams.callback;
    fetch(url, { method: "POST", headers: header, body: JSON.stringify(requestBody) })
        .then((response) => response.json())
        .then((data) => {
            console.log(`\n==============================================\nmethod : POST \nurl : ${url} \nbody : ${JSON.stringify(requestBody, null, 4)} \nheaders : ${JSON.stringify(header, null, 4)} \ndata : ${JSON.stringify(data, null, 4)} \n==============================================\n`);
            if (data?.status?.toLowerCase() === SUCCESS || data?.status === undefined)
                callback(data, null);
            else
                callback(null, data);
        })
        .catch(error => {
            console.log(`\n==============================================\nmethod : POST \nurl : ${url} \nbody : ${JSON.stringify(requestBody, null, 4)} \nheaders : ${JSON.stringify(header, null, 4)} \nerror : ${JSON.stringify(error, null, 4)} \n==============================================\n`);
            callback(null, error);
        });
}

function fetchGetRequest(apiParams, apiCallback) {
    const url = (apiParams?.apiProps?.baseUrl ?? "") + apiParams.url;
    const header = apiParams?.apiProps?.header;
    fetch(url, { method: "GET", headers: header })
        .then((response) => response.json())
        .then((data) => {
            console.log(`\n==============================================\nmethod : GET \nurl : ${url} \nheaders : ${JSON.stringify(header, null, 4)} \ndata : ${JSON.stringify(data, null, 4)} \n==============================================\n`);
            if (data?.status?.toLowerCase() === SUCCESS || data?.status === undefined) {// check for undefined only for autosuggestion API.
                if(apiParams?.pageVariationId){
                    data.pageVariationId = apiParams?.pageVariationId;
                }
                apiCallback(data, null);
            } else {
                apiCallback(null, data);
            }
        })
        .catch(error => {
            console.log(`\n==============================================\nmethod : GET \nurl : ${url} \nheaders : ${JSON.stringify(header, null, 4)} \nerror : ${JSON.stringify(error, null, 4)} \n==============================================\n`);
            apiCallback(null, error);
        });
}

function executeGetRequest(apiParams) {
    let apiCallback = apiParams.callback;
    if (apiParams?.isCacheEnabled ?? false) {
        cacheData(apiParams, (apiCallback) => {
            if (apiCallback) {
                fetchGetRequest(apiParams, apiCallback);
            }
        });
    } else {
        fetchGetRequest(apiParams, apiCallback);
    }
}

function executePutRequest(apiParams) {
    const url = (apiParams?.apiProps?.baseUrl ?? "") + apiParams.url;
    const header = apiParams?.apiProps?.header;
    const callback = apiParams.callback;
    fetch(url, { method: "PUT", headers: header })
        .then((response) => response.json())
        .then((data) => {
            console.log(`\n==============================================\nmethod : PUT \nurl : ${url} \nheaders : ${JSON.stringify(header, null, 4)} \ndata : ${JSON.stringify(data, null, 4)} \n==============================================\n`);
            if (data?.status?.toLowerCase() === SUCCESS || data?.status === undefined)
                callback(data, null);
            else
                callback(null, data);
        })
        .catch(error => {
            console.log(`\n==============================================\nmethod : PUT \nurl : ${url} \nheaders : ${JSON.stringify(header, null, 4)} \nerror : ${JSON.stringify(error, null, 4)} \n==============================================\n`);
            callback(null, error);
        });
}

export function executeDocUploadRequest(apiParams) {
    const url = (apiParams?.apiProps?.baseUrl ?? "") + apiParams.url;
    const header = apiParams?.apiProps?.header;
    const requestBody = apiParams.requestBody;
    const callback = apiParams.callback;
    fetch(url, { method: "POST", headers: header, body: requestBody })
        .then((response) => response.json())
        .then((data) => {
            console.log(`\n==============================================\nmethod : POST \nurl : ${url} \nbody : ${JSON.stringify(requestBody, null, 4)} \nheaders : ${JSON.stringify(header, null, 4)} \ndata : ${JSON.stringify(data, null, 4)} \n==============================================\n`);
            if (data?.status?.toLowerCase() === SUCCESS)
                callback(data, null);
            else
                callback(null, data);
        })
        .catch(error => {
            console.log(`\n==============================================\nmethod : POST \nurl : ${url} \nbody : ${JSON.stringify(requestBody, null, 4)} \nheaders : ${JSON.stringify(header, null, 4)} \nerror : ${JSON.stringify(error, null, 4)} \n==============================================\n`);
            callback(null, error);
        });
}

export function makeGetRequestDOC(url, header, callback) {
    fetch(url, { method: "GET", headers: header })
        .then((response) => response.blob())
        .then((data) => {
            console.log(`\n==============================================\nmethod : GET \nurl : ${url} \nheaders : ${JSON.stringify(header, null, 4)} \ndata : ${data} \n==============================================\n`);
            if (data?.status?.toLowerCase() === SUCCESS || data?.status === undefined)
                callback(data, null);
            else
                callback(null, data);
        })
        .catch(error => {
            console.log(`\n==============================================\nmethod : GET \nurl : ${url} \nheaders : ${JSON.stringify(header, null, 4)} \nerror : ${JSON.stringify(error, null, 4)} \n==============================================\n`);
            callback.onError(error);
        });
}

export function makeGetRequest(url, callback) {
    nativeBridgeRef.getApiProperty(apiProperty => {
        const params = parseJSON(apiProperty);
        executeApi({
            apiProps: { baseUrl: params?.api_gateway_base_url, header: params?.apiHeader },
            httpType: HTTP_GET,
            url: url,
            callback: callback,
        });
    });
}

export function makePostRequest(url, body, callback) {
    nativeBridgeRef.getApiProperty(apiProperty => {
        const params = parseJSON(apiProperty);
        executeApi({
            apiProps: { baseUrl: params?.api_gateway_base_url, header: params?.apiHeader },
            httpType: HTTP_POST,
            url: url,
            requestBody: body,
            callback: callback,
        });
    });
}

export function makePutRequest(url, callback) {
    nativeBridgeRef.getApiProperty(apiProperty => {
        const params = parseJSON(apiProperty);
        executeApi({
            apiProps: { baseUrl: params?.api_gateway_base_url, header: params?.apiHeader },
            httpType: HTTP_PUT,
            url: url,
            callback: callback,
        });
    });
}

export function promiseForGetRequest(url, header) {
    return fetch(url, { method: "GET", headers: header })
        .then((response) => response.json())
        .then(res => res)
        .catch(error => error);
}

const timeout = (ms, promise) => new Promise((resolve, reject) => {
    setTimeout(() => reject(new Error("timeout")), ms);
    promise.then(resolve, reject);
});
