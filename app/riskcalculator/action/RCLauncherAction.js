import {connect} from 'react-redux';
import * as Actions from '../../Constants/ActionTypes';
import {GET_RC_QUESTIONS} from '../../Constants/ActionTypes';
import RCLauncherComponent from '../components/RCLauncherComponent';
import {RISK_CALCULATOR_QUESTIONS} from '../../Constants/ApiUrls';
import { executeApi, HTTP_GET } from "../../network/ApiManager";
import {NativeModules} from 'react-native';
import { parseJSON, deepCopy} from "../../commonUtil/AppUtils";

const nativeBridgeRef = NativeModules.ChatBridge;

const mapStateToProps = state => ({
    isLoading: state.RCLauncherReducer.isLoading,
    questionData: state.RCLauncherReducer.questionData,
    type: state.RCLauncherReducer.type,
    isProd: state.appData.params.isProd,
});

const mapDispatchToProps = dispatch => ({

    getRiskCalculatorQuestions: () => {
        dispatch({type: Actions.RC_LAUNCHER_LOADER_UPDATE, isLoading: true});
        executeGetRCQuestionsApi(dispatch);
    },
});

function executeGetRCQuestionsApi(dispatch) {
    // let url = isProd ? RISK_CALCULATOR_QUESTIONS_PROD : RISK_CALCULATOR_QUESTIONS_NON_PROD
    nativeBridgeRef.getApiProperty(apiProperty => {
        let apiPropert = parseJSON(apiProperty);
        executeApi({
        httpType: HTTP_GET,
        url: apiPropert.api_base_url + RISK_CALCULATOR_QUESTIONS,
        callback: (response, error) => {
        if (response) {
                dispatch({type: GET_RC_QUESTIONS, data: response, isLoading: false});
        } else {
                dispatch({type: GET_RC_QUESTIONS, data: error, isLoading: false});
            }
            }
        })
    });
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(RCLauncherComponent);
