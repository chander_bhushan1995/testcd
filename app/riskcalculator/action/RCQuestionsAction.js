import {connect} from "react-redux";
import * as Actions from "../../Constants/ActionTypes";
import RCQuestionsComponent from "../components/RCQuestionsComponent";
import { executeApi, HTTP_POST } from '../../network/ApiManager';
import {IDFENCE_RISK_CALCULATOR_NON_PROD, IDFENCE_RISK_CALCULATOR_PROD} from '../../Constants/ApiUrls';

const xApiKey = "d8YeNrcn1l6lLdB8y5RZ0aNbhsT33xAt7G0I3OQy";
const xApiKey_non_prod = "IyQYnYOcYtWn6jp9kXfh83YedjpffXEa9djDD018";


const mapStateToProps = state => ({
    // Store has access to all the Reducers
    isLoading: state.RCQuestionsReducer.isLoading,
    index: state.RCQuestionsReducer.index,
    askQuestion: state.RCQuestionsReducer.askQuestion,
    radioItems: state.RCQuestionsReducer.radioItems,
    questions: state.RCLauncherReducer.questionData,
    noOfQuestionsGotWrong: state.RCQuestionsReducer.noOfQuestionsGotWrong,
    isRight: state.RCQuestionsReducer.isRight,
    custId: state.appData.params.custId, // fetching it from store
    docId: state.RCQuestionsReducer.docId,
    type: state.RCQuestionsReducer.type,
    hasTrial: state.appData.params.hasATrial,
    hasPremium: state.appData.params.hasAPremium,
    forPlan: state.appData.params.forPlan,
    isProd: state.appData.params.isProd,
    isSIEnabled: state.appData.params.isSIEnabled,
    isIDFenceForRenewal: state.appData.params.isIDFenceForRenewal
});

const mapDispatchToProps = dispatch => ({
    verifyQuestionCorrectness: (questionNo, questions, answer, noOfQuestionsGotWrong) => {
        dispatch({
            type: Actions.VERIFY_ANSWERED_QUESTION, questionNo: questionNo, questions: questions, answer: answer,
            noOfQuestionsGotWrong: noOfQuestionsGotWrong
        })
    },
    getNextQuestion: (index) => {
        dispatch({type: Actions.GET_NEXT_QUESTION, index: index})
    },
    startRiskCalculator: (custID, isProd) => {
        executeStartRiskCalculator(custID, dispatch, isProd);
    },
    endRiskCalculator: (docId, noOfQuestionsAnswered, score, isProd) => {
        dispatch({type: Actions.RC_QUESTIONS_LOADER_UPDATE, isLoading: true});
        submitRiskCalculatorScore(docId, noOfQuestionsAnswered, score, dispatch, isProd)
    },
});

function executeStartRiskCalculator(custID, dispatch, isProd) {
    let bodyData = {
        "userId": custID
    };
    const url = isProd ? IDFENCE_RISK_CALCULATOR_PROD : IDFENCE_RISK_CALCULATOR_NON_PROD;
    executeApi({
        apiProps: { header: getHeader(isProd) },
        httpType: HTTP_POST,
        url: url,
        requestBody: bodyData,
        callback: (response, error) => {
        if (response) {
            let status = response.status;
            if (status === 'success') {
                let docId = response.data.documentId;
                dispatch({type: Actions.CHECK_RISK_CALCULATOR_START_SUCCESS, docId: docId})
            }
        } else {
        }
        }
    });
}

function submitRiskCalculatorScore(docId, noOfQuestionsAnswered, score, dispatch, isProd) {
    let bodyData = {
        "score": score,
        "answeredQuestions": noOfQuestionsAnswered
    };
    const baseUrl = isProd ? IDFENCE_RISK_CALCULATOR_PROD : IDFENCE_RISK_CALCULATOR_NON_PROD;
    const url = baseUrl + "/" + docId;
    executeApi({
        apiProps: { header: getHeader(isProd) },
        httpType: HTTP_POST,
        url: url,
        requestBody: bodyData,
        callback: (response, error) => {
        if (response) {
            dispatch({type: Actions.CHECK_RISK_CALCULATOR_END_SUCCESS, data: response, isLoading: false})
        } else {
            dispatch({type: Actions.CHECK_RISK_CALCULATOR_END_FAILURE, data: error, isLoading: false})
        }
        }
    });
}

function getHeader(isProd) {

    const apiHeader = {
        "x-api-key": isProd ? xApiKey : xApiKey_non_prod
    };
    return apiHeader
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RCQuestionsComponent);
