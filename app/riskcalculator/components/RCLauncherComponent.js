import React from 'react';
import {
    BackHandler,
    Image,
    NativeModules,
    Platform,
    SafeAreaView,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import {TextStyle} from "../../Constants/CommonStyle";
import colors from "../../Constants/colors";
import spacing from "../../Constants/Spacing";
import ButtonWithLoader from "../../CommonComponents/ButtonWithLoader";
import * as Actions from "../../Constants/ActionTypes"
import {PLATFORM_OS} from "../../Constants/AppConstants";
import {rcFlowStrings} from "../constants/RcConstants"
import {RISK_CALCULATOR_GET_STARTED} from '../../Constants/WebengageEvents';
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';

const nativeBrigeRef = NativeModules.ChatBridge;

const HeaderComponent = props => {
    return (<View style={styles.headerStyle}>
        <Text style={TextStyle.text_16_bold}>{rcFlowStrings.titleRiskCalculator}</Text>
        <TouchableOpacity hitSlop={{top: 50, bottom: 20, left: 30, right: 50}} onPress={() => {
            onBackPress();
        }}>
            <Image source={require("../../images/icon_cross.webp")} style={styles.crossIconStyle}/>
        </TouchableOpacity>
    </View>)
}

const IntermediateContentComponent = props => {
    return (
        <View style={styles.centerViewStyle}>
            <Image style={{width: 120, height: 120}} source={require("../../images/ic_fingerprint_launcher.webp")}/>
            <Text
                style={[TextStyle.text_20_bold, {marginTop: spacing.spacing24}]}>{rcFlowStrings.textLauncherText1}</Text>
            <Text
                style={[TextStyle.text_14_normal, styles.descriptionTextStyle]}>{rcFlowStrings.textLauncherText2}</Text>
        </View>
    )
};

const RCLauncherComponent = props => {
    BackHandler.addEventListener('hardwareBackPress', onBackPress);
    if (props.type === Actions.GET_RC_QUESTIONS && props?.questionData?.length) {
        navigateToRCQuestionsScreen(props);
    }
    return (
        <SafeAreaView style={styles.parentViewStyle}>
            <StatusBar
                backgroundColor={colors.white}
                barStyle="dark-content"
            />
            <HeaderComponent props={props}/>
            <IntermediateContentComponent/>
            <View style={styles.buttonStyle}>
                <ButtonWithLoader
                    isLoading={props.isLoading}
                    enable={!props.isLoading}
                    Button={{
                        text: rcFlowStrings.textGetStarted,
                        onClick: () => {
                            logWebEnageEvent(RISK_CALCULATOR_GET_STARTED, new Object());
                            fetchRiskCalculatorQuestions(props);
                        }
                    }}/>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    crossIconStyle: {
        tintColor: colors.black,
        height: 16,
        width: 16,
        resizeMode: "contain",
        alignSelf: 'center',
    },
    parentViewStyle: {
        backgroundColor: colors.white,
        flexDirection: 'column',
        flex: 1,
        justifyContent: 'space-between',
    },
    headerStyle: {
        backgroundColor: colors.white,
        padding: spacing.spacing20,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    centerViewStyle: {
        flexDirection: 'column',
        alignItems: 'center'
    },
    descriptionTextStyle: {
        color: colors.charcoalGrey,
        marginTop: spacing.spacing8,
        marginHorizontal: spacing.spacing16,
        textAlign: 'center'
    },
    buttonStyle: {
        marginHorizontal: spacing.spacing16,
        marginBottom: spacing.spacing24
    }
});

function fetchRiskCalculatorQuestions(props) {
    props.getRiskCalculatorQuestions(props.isProd);
}

function navigateToRCQuestionsScreen(props) {
    props.navigation.navigate('RCQuestionsScreen', {});
}

function onBackPress() {
    nativeBrigeRef.goBack();
};

export default RCLauncherComponent;
