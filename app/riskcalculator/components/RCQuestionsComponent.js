import React from 'react';
import {
    BackHandler,
    Image,
    NativeModules,
    Platform,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import colors from '../../Constants/colors';
import {TextStyle} from '../../Constants/CommonStyle';
import spacing from '../../Constants/Spacing';
import ButtonWithLoader from '../../CommonComponents/ButtonWithLoader';
import RadioButton from '../../Components/RadioButton';
import {HeaderBackButton} from 'react-navigation';
import * as ActionTypes from '../../Constants/ActionTypes';
import DialogView from '../../Components/DialogView';
import {PLATFORM_OS} from '../../Constants/AppConstants';
import RiskMeterComponent from './RiskMeterComponent';
import {rcFlowStrings} from '../constants/RcConstants';
import {SHARE_ICON} from '../../Constants/WebengageAttributes';
import {RISK_CALCULATOR_SUBMIT, SHARE_RISK_CALCULATOR} from '../../Constants/WebengageEvents';
import {logWebEnageEvent} from '../../commonUtil/WebengageTrackingUtils';

let DialogViewRef;
const nativeBrigeRef = NativeModules.ChatBridge;

const QuestionView = props => {
    return (
        <View style={props.style}>
            <Text
                style={[
                    TextStyle.text_10_regular,
                    {marginHorizontal: spacing.spacing16},
                ]}
            >
                QUESTION {props.index + 1}/10
            </Text>
            <Text
                style={[
                    TextStyle.text_16_regular,
                    {
                        marginHorizontal: spacing.spacing16,
                        marginTop: spacing.spacing8,
                    },
                ]}
            >
                {props.questionText}
            </Text>
        </View>
    );
};

const RadioGroupView = props => {
    const callback = index => {
        if (props.index == 0) {
            props.apiCallback(props.custId, props.isProd);
        }
        props.callback(
            props.index,
            props.questions,
            props.radioItems[index].label,
            props.noOfQuestionsGotWrong,
        );
    };

    return (
        <View style={props.style}>
            <View style={styles.horizontalLineCompleteView}/>
            <TouchableOpacity
                onPress={() => {
                    callback(0);
                }}
            >
                <View style={styles.radioButtonContainer}>
                    <RadioButton
                        data={props.radioItems[0]}
                        onClick={() => {
                            callback(0);
                        }}
                    />
                </View>
            </TouchableOpacity>
            <View style={styles.horizontalLineCompleteView}/>
            <TouchableOpacity
                onPress={() => {
                    callback(1);
                }}
            >
                <View style={[styles.radioButtonContainer]}>
                    <RadioButton
                        data={props.radioItems[1]}
                        onClick={() => {
                            callback(1);
                        }}
                    />
                </View>
            </TouchableOpacity>
        </View>
    );
};

const AnswerView = props => {
    const question = props.questions[props.index];
    const answerHeader = props.isCorrect
        ? question.correctHeader
        : question.wrongHeader;
    const answerText = props.isCorrect
        ? question.correctResponse
        : question.badResponse;
    const color = props.isCorrect ? colors.seaGreen : colors.salmonRed;

    return (
        <View style={props.style}>
            <Text
                style={[
                    TextStyle.text_16_bold_24,
                    {color: color, marginHorizontal: spacing.spacing16}
                ]}
            >
                {answerHeader}
            </Text>
            <Text
                style={[
                    TextStyle.text_16_regular,
                    {
                        marginHorizontal: spacing.spacing16,
                        marginTop: spacing.spacing4
                    }
                ]}
            >
                {answerText}
            </Text>
        </View>
    );
};

const RCQuestionsComponent = props => {
    BackHandler.addEventListener('hardwareBackPress', showExitAlert);
    switch (props.type) {
        case ActionTypes.CHECK_RISK_CALCULATOR_END_SUCCESS:
            navigateToRCResultScreen(props);
            break;
        case ActionTypes.CHECK_RISK_CALCULATOR_END_FAILURE:
            showAlertForAPI(
                'Alert', 'Something Went Wrong!!!',
            );
            break;
        default:
            break;
    }
    const getPrimaryButtonComponent = () => {
        let buttonTitle = 'Next';
        if (props.index == props.questions.length - 1) {
            buttonTitle = 'Submit';
        }
        return (
            <View style={styles.buttonStyle}>
                <ButtonWithLoader
                    isLoading={props.isLoading}
                    enable={!props.isLoading}
                    Button={{
                        text: buttonTitle,
                        onClick: () => {
                            if (props.index == props.questions.length - 1) {
                                let eventDataRCSubmit = new Object();
                                eventDataRCSubmit['Risk Meter'] = (((props.noOfQuestionsGotWrong / props.questions.length) * 100) + '% Risk');
                                logWebEnageEvent(RISK_CALCULATOR_SUBMIT, eventDataRCSubmit);

                                if (props.docId === undefined) {
                                    navigateToRCResultScreen(props);
                                } else {
                                    props.endRiskCalculator(
                                        props.docId,
                                        props.questions.length,
                                        (props.noOfQuestionsGotWrong / props.questions.length) * 100,
                                        props.isProd
                                    );
                                }
                            } else {
                                props.getNextQuestion(props.index);
                            }
                        }
                    }}
                />
            </View>
        );
    };

    let radioGroupView = props.askQuestion ? (
        <RadioGroupView
            radioItems={props.radioItems}
            style={{marginTop: spacing.spacing20}}
            index={props.index}
            questions={props.questions}
            custId={props.custId}
            isProd={props.isProd}
            callback={props.verifyQuestionCorrectness}
            apiCallback={props.startRiskCalculator}
            noOfQuestionsGotWrong={props.noOfQuestionsGotWrong}
        />
    ) : null;
    let answerView = props.askQuestion ? null : (
        <AnswerView
            style={styles.answerView}
            index={props.index}
            isCorrect={props.isRight}
            questions={props.questions}
        />
    );
    let buttonView = props.askQuestion ? null : getPrimaryButtonComponent();

    return (
        <SafeAreaView style={[styles.MainContainer]}>
            <RiskMeterComponent
                style={styles.riskMeterView}
                noOfQuestionsGotWrong={props.noOfQuestionsGotWrong}
                questions={props.questions}
            />
            <QuestionView
                index={props.index}
                questionText={props?.questions?.[props.index]?.question}
            />
            {radioGroupView}
            {answerView}
            {buttonView}
            <DialogView ref={ref => (DialogViewRef = ref)}/>
        </SafeAreaView>
    );
};

RCQuestionsComponent.navigationOptions = navigationData => {
    var shareImage = (
        <Image
            source={require('../../images/share.webp')}
            style={styles.riskCalculatorShare}
        />
    );

    return {
        headerTitle: (
            <View
                style={
                    Platform.OS === 'ios'
                        ? {alignItems: 'center'}
                        : {alignItems: 'flex-start'}
                }
            >
                <Text style={TextStyle.text_16_bold_24}>{rcFlowStrings.titleRiskCalculator}</Text>
            </View>
        ),
        headerLeft: (
            <HeaderBackButton
                tintColor={colors.black}
                onPress={() => {
                    showExitAlert();
                }}
            />
        ),
        headerRight: (
            <TouchableOpacity
                style={styles.NavigationBarRightButton}
                onPress={() => {
                    let eventDataShare = new Object();
                    eventDataShare['Location'] = SHARE_ICON;
                    logWebEnageEvent(SHARE_RISK_CALCULATOR, eventDataShare);
                    nativeBrigeRef.shareLink(rcFlowStrings.rcShareMessage);
                }}
            >
                {shareImage}
            </TouchableOpacity>
        ),
        headerTitleStyle: {color: colors.white},
        headerTintColor: colors.white,
    };
};

function showExitAlert() {
    if (DialogViewRef !== null && DialogViewRef !== undefined) {
        DialogViewRef.showDailog({
            title: rcFlowStrings.rcExitDialogTitle,
            message: rcFlowStrings.rcExitDialogDescription,
            primaryButton: {
                text: 'Yes',
                onClick: () => {
                    onBackPress();
                },
            },
            secondaryButton: {
                text: 'No',
            },
            cancelable: true
        });
    }
    return true;
}

function showAlertForAPI(title, alertMessage) {
    if (DialogViewRef !== null && DialogViewRef !== undefined) {
        DialogViewRef.showDailog({
            title: title,
            message: alertMessage,
            primaryButton: {
                text: 'Ok',
            },
            cancelable: true,
        });
    }
}

function navigateToRCResultScreen(props) {
    props.navigation.navigate('RCResultScreen', {
        riskPercent: (props.noOfQuestionsGotWrong / props.questions.length) * 100,
        hasATrial: props.hasTrial,
        hasAPremium: props.hasPremium,
        forPlan: props.forPlan,
        isSIEnabled: props.isSIEnabled,
        isIDFenceForRenewal: props.isIDFenceForRenewal
    });
}

function onBackPress() {
    if (Platform.OS === PLATFORM_OS.ios) {
        setTimeout(() => nativeBrigeRef.goBack(), 40);
    } else {
        nativeBrigeRef.goBack();
    }
}

const styles = StyleSheet.create({
    MainContainer: {
        backgroundColor: colors.white,
        flex: 1
    },
    riskCalculatorShare: {
        width: 24,
        height: 24
    },
    riskCalculatorBack: {
        width: 24,
        height: 24,
        justifyContent: 'center',
        alignItems: 'center',
        resizeMode: 'contain',
    },
    riskMeterView: {
        marginBottom: spacing.spacing48
    },
    answerView: {
        marginTop: spacing.spacing28,
    },
    NavigationBarLeftButton: {
        paddingLeft: spacing.spacing16,
    },
    NavigationBarRightButton: {
        paddingRight: spacing.spacing16,
    },
    buttonStyle: {
        marginHorizontal: spacing.spacing16,
        marginBottom: spacing.spacing16,
        flex: spacing.spacing1,
        justifyContent: 'flex-end',
        alignSelf: 'stretch',
    },
    horizontalLineCompleteView: {
        flexDirection: 'row',
        borderBottomColor: colors.color_E0E0E0,
        borderBottomWidth: StyleSheet.hairlineWidth,
        alignSelf: 'stretch',
        height: spacing.spacing1,
    },
    radioButtonContainer: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        marginLeft: spacing.spacing16,
        marginTop: spacing.spacing12,
        marginRight: spacing.spacing12,
        marginBottom: spacing.spacing12,
    },
});

export default RCQuestionsComponent;
