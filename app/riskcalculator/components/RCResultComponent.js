import React, { useEffect, useState, useRef } from "react";
import {
    Alert,
    BackHandler,
    Image,
    NativeEventEmitter,
    NativeModules,
    Platform,
    SafeAreaView,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from "react-native";
import { CommonStyle, TextStyle } from "../../Constants/CommonStyle";
import colors from "../../Constants/colors";
import spacing from "../../Constants/Spacing";
import { HeaderBackButton } from "react-navigation";
import { Category, PLATFORM_OS, IDFencePlanCode } from "../../Constants/AppConstants";
import ButtonWithLoader from "../../CommonComponents/ButtonWithLoader";
import { rcFlowStrings } from "../constants/RcConstants";
import DialogView from "../../Components/DialogView";
import { SHARE_BUTTON } from "../../Constants/WebengageAttributes";
import { FIND_A_PLAN, RISK_CALCULATOR_SUBMIT, SHARE_RISK_CALCULATOR } from "../../Constants/WebengageEvents";
import { logWebEnageEvent } from "../../commonUtil/WebengageTrackingUtils";
import { getPlansCategoryData } from "../../commonUtil/AppUtils";

const nativeBrigeRef = NativeModules.ChatBridge;
let DialogViewRef;
let tagText = "";
const IDFenceCardComponent = props => {

    const hasPremium = props.navigation.getParam("hasAPremium");
    const hasTrial = props.navigation.getParam("hasATrial");
    const forPlan = props.navigation.getParam("forPlan")
    const isSIEnabled = props.navigation.getParam("isSIEnabled");
    const isIDFenceForRenewal = props.navigation.getParam("isIDFenceForRenewal");
    const [idFencePlanData, setIDFencePlanData] = useState(null);

    const buttonText = () => {
        if (hasPremium || (hasTrial && isSIEnabled) || (!isIDFenceForRenewal && !forPlan?.trial)) {
            return "Buy"
        } else {
            if (isIDFenceForRenewal) {
                return "Upgrade to Premium"
            } else {
                return "Get Free Trial"
            }
        }
    }

    useEffect(() => {
        getPlansCategoryData((plansData) => {
            let categories = plansData.buyPlans ?? [];
            let financeCategoryPlans = categories.filter((currentCategory) => {
                return currentCategory.category == Category.finance;
            })[0].productCodes ?? [];
            let idFencePlanDataObj = financeCategoryPlans.filter((product) => {
                return product.productCode == IDFencePlanCode;
            })[0];
            tagText = (hasTrial && !hasPremium)
                ? (isIDFenceForRenewal ? "UPGRADE TO PREMIUM" : null)
                : ((!hasPremium && forPlan?.trial)
                    ? `FREE ${(forPlan?.planDuration ?? "").toUpperCase()} TRIAL`
                    : null);
            setIDFencePlanData(idFencePlanDataObj);
        });
    }, []);

    if (idFencePlanData == null) {
        return null;
    }

    return (
        <View style={[CommonStyle.cardContainerWithShadow, props.style]}>
            <Text style={[TextStyle.text_10_bold, {
                marginTop: spacing.spacing20,
                marginLeft: spacing.spacing12,
            }]}>{idFencePlanData.heading ?? ""}</Text>
            <View style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginTop: spacing.spacing8,
                marginLeft: spacing.spacing12,
            }}>
                <View style={{ alignItems: "flex-start", flex: 1 }}>
                    <Text style={TextStyle.text_16_bold_24}>{idFencePlanData.title ?? ""}</Text>
                    {!(tagText) ? null
                        : <Text style={[TextStyle.text_10_bold, {
                            lineHeight: 18,
                            color: colors.white,
                            backgroundColor: colors.saffronYellow,
                            marginTop: spacing.spacing4,
                            paddingHorizontal: spacing.spacing4,
                            paddingTop: spacing.spacing1,
                        }]}>{tagText}</Text>
                    }
                    <Text style={[TextStyle.text_14_normal, {
                        lineHeight: 22,
                        marginTop: spacing.spacing8,
                        marginRight: spacing.spacing96,
                    }]}>{idFencePlanData.subtitle ?? ""}</Text>
                </View>
                <Image
                    source={require("../../images/id_fence_logo.webp")}
                    style={{
                        marginRight: spacing.spacing16,
                        height: 46,
                        width: 46,
                    }}
                />
            </View>
            <Text style={[TextStyle.text_12_normal, {
                lineHeight: 20,
                color: colors.charcoalGrey,
                marginTop: spacing.spacing18,
                marginLeft: spacing.spacing12,
                marginRight: spacing.spacing32,
            }]}>{idFencePlanData.note ?? ""}</Text>
            <View style={{ marginTop: 16, marginHorizontal: 12, height: 48, marginBottom: 16 }}>
                <ButtonWithLoader
                    isLoading={props.buttonLoader}
                    enable={true}
                    Button={{
                        text: buttonText(),
                        onClick: () => {
                            if(Platform.OS === PLATFORM_OS.ios){
                                nativeBrigeRef.goBackWithAmnimatedParam(false);
                                nativeBrigeRef.sendBuyIDFenceEventFromRiskCalculator();
                            }else{
                                nativeBrigeRef.openIdFencePaymentFlow()
                            }
                            logWebEnageEvent(FIND_A_PLAN, { Location: "Risk Calculator Success" });
                        },
                    }}
                />
            </View>
        </View>
    );
};

const ResultContentComponent = props => {
    const percent = props.riskPercent;
    const resultColor = percent === 0 ? colors.seaGreen : colors.salmonRed;
    const resultText = percent === 0 ? `At ${percent}%, you’re at low risk but a cyber thief may steal identity in many ways.` :
        `At ${percent}%, you’re at a high risk of identity theft.`;

    return (<View style={styles.resultContentStyle}>
        <Text style={[TextStyle.text_16_bold, {
            color: resultColor,
            fontSize: spacing.spacing18,
        }]}>{resultText}</Text>
        <Text style={[TextStyle.text_16_regular, { marginTop: spacing.spacing16 }]}>Share risk calculator quiz with your
            friends & family too.</Text>
        <TouchableOpacity onPress={() => {

            let eventDataShare = new Object();
            eventDataShare["Location"] = SHARE_BUTTON;
            logWebEnageEvent(SHARE_RISK_CALCULATOR, eventDataShare);
            nativeBrigeRef.shareLink(rcFlowStrings.rcShareMessage);
        }}>
            <Text style={[TextStyle.text_14_bold, styles.shareRiskCalculatorStyle]}>Share Risk Calculator</Text>
        </TouchableOpacity>
        <IDFenceCardComponent buttonLoader={props.buttonLoader} setButtonLoader={props.setButtonLoader}
                              navigation={props.navigation} style={{
            marginTop: spacing.spacing36,
            marginHorizontal: spacing.spacing_n_4,
            backgroundColor: colors.white,
        }} />
    </View>);
};

const RCResultComponent = props => {

    const [buttonLoader, setButtonLoader] = useState(false);

    function receiveMessageFromRenewalAPI(params) {
        if (params == "fail") {
            showAlertForAPI(
                "Alert", "Something Went Wrong!!!",
            );
        }
        setButtonLoader(false);
    }

    const eventEmitter = new NativeEventEmitter(nativeBrigeRef);

    useEffect(() => {
        if (Platform.OS === "ios") {
            eventEmitter.addListener("IDFenceRenewalEvent", params => receiveMessageFromRenewalAPI(params));
            return () => {
                eventEmitter.removeListener("IDFenceRenewalEvent", () => {
                });
            };
        }
    }, []);

    BackHandler.addEventListener("hardwareBackPress", onBackPress);
    return (
        <SafeAreaView style={styles.parentViewStyle}>
            <StatusBar
                backgroundColor={colors.white}
                barStyle="dark-content"
            />
            <ResultContentComponent buttonLoader={buttonLoader} setButtonLoader={setButtonLoader}
                                    navigation={props.navigation}
                                    riskPercent={props.navigation.getParam("riskPercent")} />
            <DialogView ref={ref => (DialogViewRef = ref)} />
        </SafeAreaView>
    );
};

RCResultComponent.navigationOptions = (navigationData) => {
    return {
        headerTitle: <View style={Platform.OS === "ios" ? { alignItems: "center" } : { alignItems: "flex-start" }}>
            <Text style={TextStyle.text_16_bold_24}>{rcFlowStrings.titleRiskCalculator}</Text>
        </View>,
        headerLeft: <HeaderBackButton tintColor={colors.black}
                                      onPress={() => {
                                          onBackPress();
                                      }} />,
        headerTitleStyle: { color: colors.white },
        headerTintColor: colors.white,
    };
};

function showAlertForAPI(title, alertMessage) {
    if (DialogViewRef !== null && DialogViewRef !== undefined) {
        DialogViewRef.showDailog({
            title: title,
            message: alertMessage,
            primaryButton: {
                text: "Ok",
            },
            cancelable: true,
        });
    }
}

const styles = StyleSheet.create({
    resultContentStyle: {
        marginHorizontal: spacing.spacing16,
        marginVertical: spacing.spacing24,
        flexDirection: "column",
    },
    shareRiskCalculatorStyle: {
        marginTop: spacing.spacing8,
        paddingVertical: spacing.spacing14,
        color: colors.blue028,
    },
    parentViewStyle: {
        backgroundColor: colors.white,
        flexDirection: "column",
        flex: 1,
    },
    headerStyle: {
        backgroundColor: colors.white,
        padding: spacing.spacing20,
        flexDirection: "row",
        justifyContent: "space-between",
    },
    centerViewStyle: {
        flexDirection: "column",
        alignItems: "center",
    },
    descriptionTextStyle: {
        color: colors.charcoalGrey,
        marginTop: spacing.spacing8,
        marginHorizontal: spacing.spacing16,
        textAlign: "center",
    },
    buttonStyle: {
        marginHorizontal: spacing.spacing16,
        marginBottom: spacing.spacing24,
    },
});

function onBackPress() {
    nativeBrigeRef.goBack();
};

export default RCResultComponent;
