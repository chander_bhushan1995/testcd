import {Animated, Image, StyleSheet, Text, View} from "react-native";
import {TextStyle} from "../../Constants/CommonStyle";
import spacing from "../../Constants/Spacing";
import React from "react";
import colors from "../../Constants/colors";

let initialPercent = 100;
let targetPercent = 100;

const RiskMeterComponent = props => {
    const {noOfQuestionsGotWrong, questions} = props;
    const questionLength = (questions?.length ?? 1)
    const percentage = (noOfQuestionsGotWrong / questionLength) * 100;
    
    return (
        <View style={[{flexDirection: "row"}, props.style]}>
            <View style={{height: 120, width: 120}}>
                <View style={styles.negativeLayerView}/>
                <PercentageAnimatedView percentage={percentage} style={styles.positiveLayerView}/>
                <Image
                    source={require("../../images/biometric.webp")}
                    style={styles.biometricImage}
                />
            </View>

            <View style={styles.riskMeterSubView}>
                <Text style={TextStyle.text_14_normal_charcoalGrey}>Risk Meter</Text>
                <Text style={[TextStyle.text_36_bold, {marginTop: spacing.spacing12}]}>
                    {percentage}%
                </Text>
            </View>
        </View>
    )
        ;
};

const PercentageAnimatedView = props => {
    const updatedPercent = 100 - props.percentage;
    let animatedValue = new Animated.Value(0);
    if (updatedPercent !== targetPercent) {
        initialPercent = targetPercent;
        targetPercent = updatedPercent;
        Animated.timing(
            animatedValue,
            {
                toValue: 1,
                duration: 400,
            }
        ).start();
    }

    return (
        <Animated.View
            style={[
                props.style,
                {
                    height: animatedValue.interpolate({
                        inputRange: [0, 1],
                        outputRange: [initialPercent + '%', targetPercent + '%']
                    })
                }]}>
        </Animated.View>
    );
};

const styles = StyleSheet.create({
    negativeLayerView: {
        marginLeft: spacing.spacing16,
        marginTop: spacing.spacing24,
        height: 120,
        width: 120,
        backgroundColor: colors.salmonRed,
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 120,
    },
    positiveLayerView: {
        marginLeft: spacing.spacing16,
        marginTop: spacing.spacing24,
        height: 120,
        width: 120,
        backgroundColor: colors.blue,
        position: 'absolute',
        top: 0,
        left: 0,
    },
    biometricImage: {
        marginLeft: spacing.spacing16,
        marginTop: spacing.spacing24,
        height: 120,
        width: 120,
        position: 'absolute',
        top: 0,
        left: 0,
    },
    riskMeterSubView: {
        marginLeft: spacing.spacing36,
        marginTop: spacing.spacing48
    }
});

export default RiskMeterComponent;