export const rcFlowStrings = {
    titleRiskCalculator: "Risk Calculator",
    textGetStarted: "Get Started",
    textLauncherText1:"Is your Online Identity at risk?",
    textLauncherText2:"Try our Identity Risk Calculator to see where you stand.",
    rcExitDialogTitle:"Are you sure you want to exit from Risk Calculator?",
    rcExitDialogDescription:"You will lose your current score and have to begin from start the next time.",
    rcShareMessage:"Identity theft is on rise in India. Is you online Identity at a Risk of theft? Check it now for free https://bit.ly/2UbByzr"
};
