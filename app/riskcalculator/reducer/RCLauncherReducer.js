import * as ActionTypes from "../../Constants/ActionTypes";

const initialState = {
    isLoading: false,
    questionData: [],
    type: ""
};

const rcLauncherReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.RC_LAUNCHER_LOADER_UPDATE:
            return {...state, type: action.type, isLoading: action.isLoading};

        case ActionTypes.GET_RC_QUESTIONS:
            if (action.data?.questions === undefined) {
                return {...state, type: action.type, isLoading: action.isLoading};
            }
            return {...state, type: action.type, isLoading: action.isLoading, questionData: action.data.questions};

        default:
            return {...state};
    }
};
export default rcLauncherReducer;
