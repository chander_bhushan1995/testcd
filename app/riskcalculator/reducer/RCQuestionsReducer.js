import * as ActionTypes from "../../Constants/ActionTypes";
import fontSizes from '../../Constants/FontSizes';
import colors from '../../Constants/colors';

const initialState = {
    isLoading: false,
    index: 0,
    isRight: true,
    askQuestion: true,
    noOfQuestionsGotWrong: 0,
    radioItems:
        [
            {
                label: 'Yes',
                size: fontSizes.fontSize20,
                color: colors.grey,
                selected: false,
            },
            {
                label: 'No',
                color: colors.grey,
                size: fontSizes.fontSize20,
                selected: false,
            }
        ],
    type: "",
    docId: undefined,
};

const rcQuestionsReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.GET_NEXT_QUESTION:
            return {...state, type: action.type, askQuestion: true, index: action.index + 1};
        case ActionTypes.VERIFY_ANSWERED_QUESTION:
            let isCorrect = action.answer.toUpperCase() === action.questions[action.questionNo].correctAnswer.toUpperCase();

            return {...state, type: action.type, askQuestion: false, isRight: isCorrect, noOfQuestionsGotWrong:
                    isCorrect ? action.noOfQuestionsGotWrong: action.noOfQuestionsGotWrong + 1 }
        case ActionTypes.CHECK_RISK_CALCULATOR_START_SUCCESS:
            return {...state, type: action.type, docId: action.docId}
        case ActionTypes.RC_QUESTIONS_LOADER_UPDATE:
        case ActionTypes.CHECK_RISK_CALCULATOR_END_SUCCESS:
        case ActionTypes.CHECK_RISK_CALCULATOR_END_FAILURE:
            return {...state, type: action.type, isLoading: action.isLoading};
        default:
            return {...state};
    }
};

export default rcQuestionsReducer;
