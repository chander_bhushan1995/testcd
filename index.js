import { AppRegistry, Text, TextInput, Platform } from 'react-native';

import Chat from './app/Navigation/index';
import OneAssistBuyback from './app/Navigation/index.buyback'
import PaymentFlow from './app/Navigation/index.paymentflow'
import IDFenceFlow from './app/Navigation/IDFenceIndex';
import RiskCalculatorFlow from './app/Navigation/index.riskcalculator'
import SplashScreen from './app/Navigation/index.appforall'
import MyProfileFlow from './app/AppForAll/MyProfile/Navigation/index.myProfile'
import CatalystFlow from './app/Navigation/index.catalystflow'
import CardManagement from './app/CardManagement/Navigation/index.cardmanagement';
import MyAccountSetting from './app/AppForAll/MyAccountSettings/Screens/index.myaccountsettings'
import UserRegisterationFlow from './app/Navigation/index.userregistration'
import TabBarTutorialFlow from './app/AppForAll/TabBarTutorial/Screens/TabBarTutorialScreen'
import MyRewardsScreen from './app/AppForAll/MyRewards/Navigation/MyRewardsNavigationStack'
import MyServiceRequests from './app/AppForAll/MyServiceRequest/Navigation/index.myservicerequests';
import EditDetails from './app/EditDetails/index.editprofile';
import FavoriteRecommendations from './app/AppForAll/FavoriteRecommendations/Navigation/index.favoriterecommendations';
import CommonIndexModule from "./app/CommonComponents/Index/index.common";
import TabBar from "./app/TabBar/index.tabbar"
import Rating from './app/AppForAll/Rating/Navigation/index.rating';

import * as PrototypeExtensions from "./app/commonUtil/PrototypeExtentions";
// Note:- The above import is mandatory don't remove go through with file
import { removeCacheData } from "./app/commonUtil/APIDataUtil";
import { parseJSON } from './app/commonUtil/AppUtils';

export let APIData;

export function setAPIData(data) {
    if (APIData) {
        // This check is needed so that APIData is only set once for a module
        // and any change to it will be done with updateAPIData method
        return
    }
    let params = data

    if (params?.onBoardingQA && (typeof (params?.onBoardingQA) === "string")) {
        params = { ...params, onBoardingQA: parseJSON(params.onBoardingQA ?? "{}").data ?? [] }
    }

    if (params?.apiHeader && (typeof (params?.apiHeader) === "string")) {
        params = { ...params, apiHeader: parseJSON(params.apiHeader) }
    }

    if (params?.data && (typeof (params?.data) === "string")) {
        params = { ...params, data: parseJSON(params.data) }
    }

    if (params?.eventDict && (typeof (params?.eventDict) === "string")) {
        params = { ...params, eventDict: parseJSON(params.eventDict) }
    }

    APIData = params;
    console.info("Set APIData ===>\n", JSON.stringify(APIData, null, 4));
}

export function updateAPIData(data) {
    let params = data

    if (params?.onBoardingQA && (typeof (params?.onBoardingQA) === "string")) {
        params = { ...params, onBoardingQA: parseJSON(params.onBoardingQA ?? "{}").data ?? [] }
    }

    if (params?.apiHeader && (typeof (params?.apiHeader) === "string")) {
        params = { ...params, apiHeader: parseJSON(params.apiHeader) }
    }

    if (params?.data && (typeof (params?.data) === "string")) {
        params = { ...params, data: parseJSON(params.data) }
    }

    if (params?.eventDict && (typeof (params?.eventDict) === "string")) {
        params = { ...params, eventDict: parseJSON(params.eventDict) }
    }
    console.info("Updating APIData with ===>\n", JSON.stringify(params, null, 4));

    APIData = {
        ...APIData,
        ...params
    }

    console.info("Updated APIData ===>\n", JSON.stringify(APIData, null, 4));
}

let DISABLE_CONSOLE_OUTPUT = true

if (Platform.OS === 'ios') { // prevents font scalling while user changes fonts from settings
    //default props for Text
    Text.defaultProps = Text.defaultProps || {};
    Text.defaultProps = { allowFontScaling: false };
    //default props for TextInput
    TextInput.defaultProps = Text.defaultProps || {};
    TextInput.defaultProps = { allowFontScaling: false };
}

if (DISABLE_CONSOLE_OUTPUT) {
    console = console || {};
    console.log = function(){};
    console.warn = function(){};
}

removeCacheData(); //We are assuming async storage and file system will remove cache data when app initized //TODO: - will change after discussion

AppRegistry.registerComponent("OneAssistBuyback", () => OneAssistBuyback);
AppRegistry.registerComponent("OneAssistChat", () => Chat);
AppRegistry.registerComponent("PaymentFlow", () => PaymentFlow);
AppRegistry.registerComponent("OAIDFenceFlow", () => IDFenceFlow);
AppRegistry.registerComponent("RiskCalculatorFlow", () => RiskCalculatorFlow);
AppRegistry.registerComponent("CardManagement", () => CardManagement);
AppRegistry.registerComponent("SplashScreen", () => SplashScreen);
AppRegistry.registerComponent("UserRegistrationFlow", () => UserRegisterationFlow);
AppRegistry.registerComponent("CatalystFlow", () => CatalystFlow);
AppRegistry.registerComponent("MyProfileFlow", () => MyProfileFlow);
AppRegistry.registerComponent("TabBarTutorialScreen", () => TabBarTutorialFlow);
AppRegistry.registerComponent("MyRewardsScreen", () => MyRewardsScreen);
AppRegistry.registerComponent("MyServiceRequests", () => MyServiceRequests);
AppRegistry.registerComponent("MyAccountSettings", () => MyAccountSetting);
AppRegistry.registerComponent("EditDetails", () => EditDetails);
AppRegistry.registerComponent('FavoriteRecommendations', () => FavoriteRecommendations);
AppRegistry.registerComponent('TabBar', () => TabBar);
AppRegistry.registerComponent('CommonIndexModule', () => CommonIndexModule);
AppRegistry.registerComponent('Rating', () => Rating);
